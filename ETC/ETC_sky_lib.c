//----------------------------------------------------------------------------------
//--
//--    Filename: ETC_sky_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.1 $"
//#define CVS_DATE     "$Date: 2015/11/23 15:42:02 $"
//--    Note: Definitions are in the corresponding header file ETC_sky_lib.h
//--    Description:
//--      Used to handle sky emission and transmission structures
//--


#include <string.h>
#include <math.h>
#include <float.h>
#include <fitsio.h>

#include "ETC_sky_lib.h"

//a few defintions that we do not want to escape beyond the scope of this file
#define COMMENT_PREFIX "#-ETC_sky_lib:Comment: "
#define WARNING_PREFIX "#-ETC_sky_lib:Warning: "
#define ERROR_PREFIX   "#-ETC_sky_lib:Error:   "
#define DEBUG_PREFIX   "#-ETC_sky_lib:Debug:   "
#define DATA_PREFIX    ""

////////////////////////////////////////////////////////////////////////
//functions
int ETC_sky_lib_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
int sky_trans_struct_init ( sky_trans_struct *pTrans )
{
  if ( pTrans == NULL ) return 1;
  strncpy(pTrans->str_name,     "", STR_MAX);
  strncpy(pTrans->str_filename, "", STR_MAX);
  
  pTrans->num_curves = 0;
  pTrans->pairmass = NULL;
  pTrans->pCurve = NULL;
  pTrans->airmass_min = NAN;
  pTrans->airmass_max = NAN; 
  return 0;
}

int sky_trans_struct_free ( sky_trans_struct *pTrans )
{
  if ( pTrans == NULL ) return 1;
  if ( pTrans->pairmass ) {free (pTrans->pairmass ); pTrans->pairmass  = NULL;}
  if ( pTrans->pCurve    )
  {
    int i = 0;
    for(i=0;i<pTrans->num_curves;i++)
    {
      if ( curve_struct_free ( (curve_struct*) &(pTrans->pCurve[i]) )) return 1;
    }
    free (pTrans->pCurve );
    pTrans->pCurve    = NULL;
  }
  return 0;
}

int sky_emiss_struct_init ( sky_emiss_struct *pEmiss )
{
  if ( pEmiss == NULL ) return 1;
  strncpy(pEmiss->str_name,     "", STR_MAX);
  strncpy(pEmiss->str_filename, "", STR_MAX);
  
  pEmiss->num_curves   = 0;
  pEmiss->pairmass      = NULL;
  pEmiss->pskybright    = NULL;
  pEmiss->pzenbright    = NULL;
  pEmiss->pCurve        = NULL;

  pEmiss->airmass_min = NAN;
  pEmiss->airmass_max = NAN; 

  pEmiss->skybright_min = NAN;
  pEmiss->skybright_max = NAN;

  pEmiss->zenbright_min = NAN;
  pEmiss->zenbright_max = NAN;


  return 0;
}

int sky_emiss_struct_free ( sky_emiss_struct *pEmiss )
{
  if ( pEmiss == NULL ) return 1;
  if ( pEmiss->pairmass  ) free (pEmiss->pairmass  ); pEmiss->pairmass  = NULL;
  if ( pEmiss->pskybright) free (pEmiss->pskybright); pEmiss->pskybright= NULL;
  if ( pEmiss->pzenbright) free (pEmiss->pzenbright); pEmiss->pzenbright= NULL;
  if ( pEmiss->pCurve    )
  {
    int i = 0;
    for(i=0;i<pEmiss->num_curves;i++)
    {
      if ( curve_struct_free ( (curve_struct*) &(pEmiss->pCurve[i]) )) return 1;
    }
    free (pEmiss->pCurve );
    pEmiss->pCurve    = NULL;
  }
  return 0;
}

int sky_struct_init ( sky_struct **ppSky )
{
  if ( ppSky == NULL ) return 1;
  if ( *ppSky == NULL )
  {
    //make space for the struct
    if ( (*ppSky = (sky_struct*) malloc(sizeof(sky_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  if ( sky_trans_struct_init( (sky_trans_struct*) &((*ppSky)->Trans))) return 1;
  if ( sky_emiss_struct_init( (sky_emiss_struct*) &((*ppSky)->Emiss))) return 1;
  return 0;
}

int sky_struct_free ( sky_struct **ppSky )
{
  if ( ppSky == NULL ) return 1;
  if ( *ppSky )
  {
    if ( sky_trans_struct_free( (sky_trans_struct*) &((*ppSky)->Trans))) return 1;
    if ( sky_emiss_struct_free( (sky_emiss_struct*) &((*ppSky)->Emiss))) return 1;
    free(*ppSky);
    *ppSky = NULL;
  }
  return 0;
}


//expect the sky transmission to be stored as vector columns
int sky_trans_struct_read_from_file ( sky_trans_struct *pTrans, BOOL verbose)
{
  fitsfile *pFile = NULL;
  int num_cols, colnum_am, colnum_trans;
  long i,j;
  int status = 0;
  int coltype_trans;
  long colrepeat_trans, colwidth_trans;
  if ( pTrans == NULL ) return 1;
  

  //open the file for reading
  if ( fits_open_table((fitsfile**) &pFile, pTrans->str_filename, READONLY, &status))
  {
    fits_report_error(stderr, status);  //DEBUG
    return 1;
  }

  if ( verbose)
  {
    fprintf(stdout, "%s %s Reading sky transmission from file\n", DEBUG_PREFIX, pTrans->str_filename); fflush(stdout);
  }
  
  if ( fits_get_num_rows ((fitsfile*) pFile, &(pTrans->num_curves), &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( verbose)
  {
    fprintf(stdout, "%s %s contains %ld rows\n", DEBUG_PREFIX, pTrans->str_filename, pTrans->num_curves); fflush(stdout);
  }
  if ( pTrans->num_curves <= 0 )
  {
    fprintf(stderr, "%s I found zero valid rows in the fits file: %s\n", ERROR_PREFIX, pTrans->str_filename);fflush(stderr);
    return 1;
  }

  
  //work out the index of the columns we need to read
  if ( fits_get_num_cols ((fitsfile*) pFile, &num_cols, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "AIRMASS", &colnum_am, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "SKYTRANS", &colnum_trans, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }


  //check that colnums are sensible
  if ( colnum_am > num_cols || colnum_trans > num_cols ||
       colnum_am < 0        || colnum_trans < 0 )
  {
    fprintf(stderr, "%s Strange column numbers returned when reading from: %s\n",
            ERROR_PREFIX, pTrans->str_filename);  fflush(stderr);
    return 1;
  }

  //allocate space for data
  if ((pTrans->pairmass = (double*)       malloc(sizeof(double)       * pTrans->num_curves)) == NULL ||
      (pTrans->pCurve   = (curve_struct*) malloc(sizeof(curve_struct) * pTrans->num_curves)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }


  //get the dimensions of the SKYTRANS column
  if ( fits_get_coltype((fitsfile*) pFile, colnum_trans, &coltype_trans, &colrepeat_trans, &colwidth_trans, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  
  //init the curve structs
  for(i=0;i<pTrans->num_curves;i++)
  {
    curve_struct *pCurve = (curve_struct*) &(pTrans->pCurve[i]);
    if ( curve_struct_init((curve_struct*) pCurve)) return 1;
    //allocate some space for each curve
    pCurve->num_rows = colrepeat_trans;
    if ((pCurve->px = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL ||
        (pCurve->py = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }

  }

  
  //now read the data - do each airmass in turn
  for ( i=0; i<pTrans->num_curves; i++ )
  {
    curve_struct *pCurve = (curve_struct*) &(pTrans->pCurve[i]);
    pCurve->colnum_y = colnum_trans;

    //fits_read_col*fptr,datatyp, colnum,    1strow, 1stelem,         nelem, *nulval, *array,           *anynul, *status)

    fits_read_col(pFile, TDOUBLE, colnum_am,    i+1,       1,                1, NULL, &(pTrans->pairmass[i]), NULL, &status);
    fits_read_col(pFile, TDOUBLE, colnum_trans, i+1,       1, pCurve->num_rows, NULL, pCurve->py,             NULL, &status);
   
    if ( status)
    {
      fits_report_error(stderr, status);
      return 1;
    }
    sprintf(pCurve->str_name, "SKYTRANS_AM%3.1f", pTrans->pairmass[i]);  
    strncpy(pCurve->str_unit_x, "nm", STR_MAX);  //this is a fudge
    strncpy(pCurve->str_unit_y, "fraction", STR_MAX);  
    if ( curve_interpret_unit_string((char*) pCurve->str_unit_x, (int*) &(pCurve->unit_type_x), (double*)&(pCurve->unit_x))) return 1;
    if ( curve_interpret_unit_string((char*) pCurve->str_unit_y, (int*) &(pCurve->unit_type_y), (double*)&(pCurve->unit_y))) return 1;
  }

  //now decifer the wcs solution of the vector columns
   //now get the wcs...
  {
    char buffer[STR_MAX];
    double crval, crpix, cdelt, cunit;
    char str_ctype[STR_MAX];
    curve_struct *pCurve = NULL;
    sprintf(buffer, "1CTYP%d", colnum_trans);
    fits_read_key ((fitsfile*) pFile, (int) TSTRING, buffer, str_ctype,  NULL, &status);
    sprintf(buffer, "1CRVL%d", colnum_trans);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &crval,  NULL, &status);
    sprintf(buffer, "1CRPX%d", colnum_trans);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &crpix,  NULL, &status);
    sprintf(buffer, "1CDLT%d", colnum_trans);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &cdelt,  NULL, &status);
    sprintf(buffer, "1CUNI%d", colnum_trans);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &cunit,  NULL, &status);
    if ( status)
    {
      fits_report_error(stderr, status);
      return 1;
    }
    //TODO add some more error checking here
    
    pCurve = (curve_struct*) &(pTrans->pCurve[0]);
    for(j=0;j<pCurve->num_rows;j++)
    {
      pCurve->px[j] = (double) (1e9 * cunit * (crval + cdelt * ((double) (j + 1) - crpix))); 
    }

    for ( i=1; i<pTrans->num_curves; i++ )
    {
      curve_struct *pCurvei = (curve_struct*) &(pTrans->pCurve[i]);
      for(j=0;j<pCurve->num_rows;j++)  pCurvei->px[j] = pCurve->px[j];
    }
  }

  //tidy up
  status = 0;
  fits_close_file(pFile, &status);

  //now calc some stats
  for ( i=0; i<pTrans->num_curves; i++ )
  {
    curve_struct *pCurve = (curve_struct*) &(pTrans->pCurve[i]);
    
    //and record some metadata
    pCurve->min_x = (double) DBL_MAX;
    pCurve->max_x = (double) -DBL_MAX;
    pCurve->min_y  = (double) DBL_MAX;
    pCurve->max_y  = (double) -DBL_MAX;
    for(j=0;j<pCurve->num_rows;j++)
    {
      if ( pCurve->px[j] < pCurve->min_x ) pCurve->min_x = pCurve->px[j];
      if ( pCurve->px[j] > pCurve->max_x ) pCurve->max_x = pCurve->px[j];
      if ( pCurve->py[j] < pCurve->min_y ) pCurve->min_y = pCurve->py[j];
      if ( pCurve->py[j] > pCurve->max_y ) pCurve->max_y = pCurve->py[j];
    }
//    
//    fprintf(stdout, "%s %s data ranges: x=[%10g:%10g]%s y=[%10g:%10g]%s nrows=%ld\n", DEBUG_PREFIX,
//            pCurve->str_filename,
//            pCurve->min_x*pCurve->unit_x,
//            pCurve->max_x*pCurve->unit_x,
//            CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_x),
//            pCurve->min_y*pCurve->unit_y,
//            pCurve->max_y*pCurve->unit_y,
//            CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_y),
//            pCurve->num_rows); fflush(stdout);
  }

  
  //work out max+min of the sky parameters
  pTrans->airmass_min   =  DBL_MAX; 
  pTrans->airmass_max   = -DBL_MAX; 
  for ( i=0; i<pTrans->num_curves; i++ )
  {
    if ( pTrans->pairmass[i]   > pTrans->airmass_max   ) pTrans->airmass_max   = pTrans->pairmass[i];
    if ( pTrans->pairmass[i]   < pTrans->airmass_min   ) pTrans->airmass_min   = pTrans->pairmass[i];
  }

  return 0;
}


//expect the sky transmission to be stored as vector columns
int sky_emiss_struct_read_from_file ( sky_emiss_struct *pEmiss, BOOL verbose )
{
  fitsfile *pFile = NULL;
  int num_cols, colnum_am, colnum_skybright,colnum_zenbright, colnum_emiss;
  long i,j;
  int status = 0;
  int coltype_emiss;
  long colrepeat_emiss, colwidth_emiss;
  if ( pEmiss == NULL ) return 1;
  

  //open the file for reading
  if ( fits_open_table((fitsfile**) &pFile, pEmiss->str_filename, READONLY, &status))
  {
    fits_report_error(stderr, status);  //DEBUG
    return 1;
  }

  if ( verbose) {
    fprintf(stdout, "%s %s Reading sky emission from file\n", DEBUG_PREFIX, pEmiss->str_filename); fflush(stdout);
  }
  if ( fits_get_num_rows ((fitsfile*) pFile, &(pEmiss->num_curves), &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( verbose) {
    fprintf(stdout, "%s %s contains %ld rows\n", DEBUG_PREFIX, pEmiss->str_filename, pEmiss->num_curves); fflush(stdout);
  }
  if ( pEmiss->num_curves <= 0 )
  {
    fprintf(stderr, "%s I found zero valid rows in the fits file: %s\n", ERROR_PREFIX, pEmiss->str_filename);fflush(stderr);
    return 1;
  }

  
  //work out the index of the columns we need to read
  if ( fits_get_num_cols ((fitsfile*) pFile, &num_cols, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "AIRMASS", &colnum_am, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "SKY_V", &colnum_skybright, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "SKY_ZEN_V", &colnum_zenbright, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "SKYEMISS", &colnum_emiss, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }


  //check that colnums are sensible
  if ( colnum_am        < 0 || colnum_am        > num_cols ||
       colnum_skybright < 0 || colnum_skybright > num_cols ||
       colnum_zenbright < 0 || colnum_zenbright > num_cols ||
       colnum_emiss     < 0 || colnum_emiss     > num_cols )
              
  {
    fprintf(stderr, "%s Strange column numbers returned when reading from: %s\n",
            ERROR_PREFIX, pEmiss->str_filename);  fflush(stderr);
    return 1;
  }

  //allocate space for data
  if ((pEmiss->pairmass   = (double*)       malloc(sizeof(double)       * pEmiss->num_curves)) == NULL ||
      (pEmiss->pskybright = (double*)       malloc(sizeof(double)       * pEmiss->num_curves)) == NULL ||
      (pEmiss->pzenbright = (double*)       malloc(sizeof(double)       * pEmiss->num_curves)) == NULL ||
      (pEmiss->pCurve     = (curve_struct*) malloc(sizeof(curve_struct) * pEmiss->num_curves)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }


  //get the dimensions of the SKYEMISS column
  if ( fits_get_coltype((fitsfile*) pFile, colnum_emiss, &coltype_emiss, &colrepeat_emiss, &colwidth_emiss, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  
  ///////////////////////////////////////////////GOT TO HERE
  //init the curve structs
  for(i=0;i<pEmiss->num_curves;i++)
  {
    curve_struct *pCurve = (curve_struct*) &(pEmiss->pCurve[i]);
    if ( curve_struct_init((curve_struct*) pCurve)) return 1;
    //allocate some space for each curve
    pCurve->num_rows = colrepeat_emiss;
    if ((pCurve->px = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL ||
        (pCurve->py = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  
  //now read the data - do each curve in turn
  for ( i=0; i<pEmiss->num_curves; i++ )
  {
    curve_struct *pCurve = (curve_struct*) &(pEmiss->pCurve[i]);

    //fits_read_col*fptr,datatyp, colnum,    1strow, 1stelem,         nelem, *nulval, *array,           *anynul, *status)

    //TODO read in chunks

    fits_read_col(pFile,TDOUBLE,colnum_am,       i+1, 1,               1, NULL,&(pEmiss->pairmass[i]),  NULL,&status);
    fits_read_col(pFile,TDOUBLE,colnum_emiss,    i+1, 1,pCurve->num_rows, NULL,pCurve->py,              NULL,&status);
    fits_read_col(pFile,TDOUBLE,colnum_skybright,i+1, 1,               1, NULL,&(pEmiss->pskybright[i]),NULL,&status);
    fits_read_col(pFile,TDOUBLE,colnum_zenbright,i+1, 1,               1, NULL,&(pEmiss->pzenbright[i]),NULL,&status);
    pCurve->colnum_y = colnum_emiss;
    if ( status)
    {
      fits_report_error(stderr, status);
      return 1;
    }
    sprintf(pCurve->str_name, "SKYEMISS_AM%3.1f_ZEN%.2f", pEmiss->pairmass[i], pEmiss->pzenbright[i]);  
    strncpy(pCurve->str_unit_x, "nm", STR_MAX);  //this is a fudge
    if ( curve_interpret_unit_string((char*) pCurve->str_unit_x, (int*) &(pCurve->unit_type_x), (double*)&(pCurve->unit_x))) return 1;
    //read the y-units properly
    if ( curve_read_unit_for_numbered_column((fitsfile*) pFile, (int) pCurve->colnum_y,
                                            (char*) pCurve->str_unit_y,
                                            (int*) &(pCurve->unit_type_y),
                                            (double*) &(pCurve->unit_y))) return 1;
    
  }

  //now decifer the wcs solution of the vector columns
  //now get the wcs...
  {
    char buffer[STR_MAX];
    double crval, crpix, cdelt, cunit;
    char str_ctype[STR_MAX], str_tunit[STR_MAX];
    sprintf(buffer, "1CTYP%d", colnum_emiss);
    fits_read_key ((fitsfile*) pFile, (int) TSTRING, buffer, str_ctype,  NULL, &status);
    sprintf(buffer, "1CRVL%d", colnum_emiss);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &crval,  NULL, &status);
    sprintf(buffer, "1CRPX%d", colnum_emiss);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &crpix,  NULL, &status);
    sprintf(buffer, "1CDLT%d", colnum_emiss);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &cdelt,  NULL, &status);
    sprintf(buffer, "1CUNI%d", colnum_emiss);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, buffer, &cunit,  NULL, &status);
    sprintf(buffer, "TUNIT%d", colnum_emiss);
    fits_read_key ((fitsfile*) pFile, (int) TSTRING, buffer, str_tunit,  NULL, &status);

    for ( i=0; i<pEmiss->num_curves; i++ )
    {
      curve_struct *pCurve = (curve_struct*) &(pEmiss->pCurve[i]);
      strncpy(pCurve->str_unit_y, str_tunit, STR_MAX);  
      for(j=0;j<pCurve->num_rows;j++)
      {
        if ( i == 0 )
          pCurve->px[j] = (double) (1e9 * cunit * (crval + cdelt * ((double) (j + 1) - crpix))); 
        else
          pCurve->px[j] = (double) pEmiss->pCurve[0].px[j];
      }
    }
  }

  //tidy up
  status = 0;
  fits_close_file(pFile, &status);

  for ( i=0; i<pEmiss->num_curves; i++ )
  {
    curve_struct *pCurve = (curve_struct*) &(pEmiss->pCurve[i]);
    
    //and record some metadata
    pCurve->min_x = (double) DBL_MAX;
    pCurve->max_x = (double) -DBL_MAX;
    pCurve->min_y  = (double) DBL_MAX;
    pCurve->max_y  = (double) -DBL_MAX;
    for(j=0;j<pCurve->num_rows;j++)
    {
      if ( pCurve->px[j] < pCurve->min_x ) pCurve->min_x = pCurve->px[j];
      if ( pCurve->px[j] > pCurve->max_x ) pCurve->max_x = pCurve->px[j];
      if ( pCurve->py[j] < pCurve->min_y ) pCurve->min_y = pCurve->py[j];
      if ( pCurve->py[j] > pCurve->max_y ) pCurve->max_y = pCurve->py[j];
    }
    
//    fprintf(stdout, "%s %s data ranges: x=[%10g:%10g]%s y=[%10g:%10g]%s nrows=%ld\n", DEBUG_PREFIX,
//            pCurve->str_filename,
//            pCurve->min_x*pCurve->unit_x,
//            pCurve->max_x*pCurve->unit_x,
//            CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_x),
//            pCurve->min_y*pCurve->unit_y,
//            pCurve->max_y*pCurve->unit_y,
//            CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_y),
//            pCurve->num_rows); fflush(stdout);
  }


  //work out max+min of the sky parameters
  pEmiss->airmass_min   =     DBL_MAX; 
  pEmiss->airmass_max   =    -DBL_MAX; 
  pEmiss->skybright_min =     DBL_MAX; 
  pEmiss->skybright_max =    -DBL_MAX;
  pEmiss->zenbright_min =     DBL_MAX; 
  pEmiss->zenbright_max =    -DBL_MAX;
  for ( i=0; i<pEmiss->num_curves; i++ )
  {
 
    if ( pEmiss->pairmass[i]   > pEmiss->airmass_max   ) pEmiss->airmass_max   = pEmiss->pairmass[i];
    if ( pEmiss->pairmass[i]   < pEmiss->airmass_min   ) pEmiss->airmass_min   = pEmiss->pairmass[i];
    if ( pEmiss->pskybright[i] > pEmiss->skybright_max ) pEmiss->skybright_max = pEmiss->pskybright[i];
    if ( pEmiss->pskybright[i] < pEmiss->skybright_min ) pEmiss->skybright_min = pEmiss->pskybright[i];
    if ( pEmiss->pzenbright[i] > pEmiss->zenbright_max ) pEmiss->zenbright_max = pEmiss->pzenbright[i];
    if ( pEmiss->pzenbright[i] < pEmiss->zenbright_min ) pEmiss->zenbright_min = pEmiss->pzenbright[i];
  }

  pEmiss->index_max_airmass_max_skybright = -1;
  pEmiss->index_max_airmass_min_skybright = -1;
  pEmiss->index_min_airmass_max_skybright = -1;
  pEmiss->index_min_airmass_min_skybright = -1;
  pEmiss->index_max_airmass_max_zenbright = -1;
  pEmiss->index_max_airmass_min_zenbright = -1;
  pEmiss->index_min_airmass_max_zenbright = -1;
  pEmiss->index_min_airmass_min_zenbright = -1;
   //we can rely on the airmasses to be evenly gridded, but not the sky brightnesses
  for ( i=0; i<pEmiss->num_curves; i++ )
  {
    if ( pEmiss->pairmass[i]   >= pEmiss->airmass_max  )
    {
      //first for local sky brightnesses
      if ( pEmiss->index_max_airmass_max_skybright < 0 )
        pEmiss->index_max_airmass_max_skybright = i;
      else if ( pEmiss->pskybright[i] >= pEmiss->pskybright[pEmiss->index_max_airmass_max_skybright] )
        pEmiss->index_max_airmass_max_skybright = i;

      if ( pEmiss->index_max_airmass_min_skybright < 0 )
        pEmiss->index_max_airmass_min_skybright = i;
      else if ( pEmiss->pskybright[i] <= pEmiss->pskybright[pEmiss->index_max_airmass_max_skybright] )
        pEmiss->index_max_airmass_min_skybright = i;

      //now repeat for zenithal values
      if ( pEmiss->index_max_airmass_max_zenbright < 0 )
        pEmiss->index_max_airmass_max_zenbright = i;
      else if ( pEmiss->pzenbright[i] >= pEmiss->pzenbright[pEmiss->index_max_airmass_max_zenbright] )
        pEmiss->index_max_airmass_max_zenbright = i;

      if ( pEmiss->index_max_airmass_min_zenbright < 0 )
        pEmiss->index_max_airmass_min_zenbright = i;
      else if ( pEmiss->pzenbright[i] <= pEmiss->pzenbright[pEmiss->index_max_airmass_max_zenbright] )
        pEmiss->index_max_airmass_min_zenbright = i;
    }

    if ( pEmiss->pairmass[i]   <= pEmiss->airmass_min  )
    {
      //first for local sky brightnesses
      if ( pEmiss->index_min_airmass_max_skybright < 0 )
        pEmiss->index_min_airmass_max_skybright = i;
      else if ( pEmiss->pskybright[i] >= pEmiss->pskybright[pEmiss->index_min_airmass_max_skybright] )
        pEmiss->index_min_airmass_max_skybright = i;

      if ( pEmiss->index_min_airmass_min_skybright < 0 )
        pEmiss->index_min_airmass_min_skybright = i;
      else if ( pEmiss->pskybright[i] <= pEmiss->pskybright[pEmiss->index_min_airmass_max_skybright] )
        pEmiss->index_min_airmass_min_skybright = i;

      //now repeat for zenithal values
      if ( pEmiss->index_min_airmass_max_zenbright < 0 )
        pEmiss->index_min_airmass_max_zenbright = i;
      else if ( pEmiss->pzenbright[i] >= pEmiss->pzenbright[pEmiss->index_min_airmass_max_zenbright] )
        pEmiss->index_min_airmass_max_zenbright = i;

      if ( pEmiss->index_min_airmass_min_zenbright < 0 )
        pEmiss->index_min_airmass_min_zenbright = i;
      else if ( pEmiss->pzenbright[i] <= pEmiss->pzenbright[pEmiss->index_min_airmass_max_zenbright] )
        pEmiss->index_min_airmass_min_zenbright = i;
    }
  }
  
  return 0;
}

//---------------------------------------------------------------------------------------------------
