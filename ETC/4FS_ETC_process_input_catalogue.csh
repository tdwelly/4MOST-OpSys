#!/bin/tcsh -f


# Scan through an input catalogue and extract the range of magnitude space 
# required for each combination of template+ruleset 
# also build a magnitude histogram
set OUTDIR  = ""
set TEMPLATE_MAG = 15
set MAG_STEP = 0.5

if ( $#argv == 1 ) then
  set CATALOGUE    = "$argv[1]"
else if ( $#argv == 2 ) then
  set CATALOGUE    = "$argv[1]"
  set TEMPLATE_MAG = "$argv[2]"
else if ( $#argv == 3 ) then
  set CATALOGUE    = "$argv[1]"
  set TEMPLATE_MAG = "$argv[2]"
  set MAG_STEP     = "$argv[3]"
else if ( $#argv == 4 ) then
  set CATALOGUE    = "$argv[1]"
  set TEMPLATE_MAG = "$argv[2]"
  set MAG_STEP     = "$argv[3]"
  set OUTDIR       = "$argv[4]"
else
  echo "Error, wrong number of input parameters"
  echo "Correct usage: ${0:t} CATALOGUE [TEMPLATE_MAG] [MAG_STEP] [OUTDIR]"
  exit 1
endif

set HISTO_MAG_MIN   =  10 
set HISTO_MAG_STEP  =  1
set HISTO_MAG_NSTEP =  15

if ( ! -e "$CATALOGUE" ) then
  echo "Error, could not find catalogue: $CATALOGUE" 
  exit 1
endif

if ( "$OUTDIR" == "" ) then
  if ( "${CATALOGUE:h}" == "${CATALOGUE}" ) then
    set OUTDIR = "."
  else
    set OUTDIR = "${CATALOGUE:h}"
  endif
endif

set OUTFILE = "${OUTDIR}/${CATALOGUE:t:r}_template_list.txt"
set OUTFILE_STEM_ETC = "${OUTFILE:r}_ETC"


set TEMPDIR = /tmp/${USER}/4FS_ETC_process_input_catalogue/
mkdir -p ${TEMPDIR}
if ( ! -d ${TEMPDIR} ) then
  echo "Error: Cannot create temporary directory: $TEMPDIR"
  exit 1
endif




echo "Processing $CATALOGUE -> ${OUTDIR}/"

if ( "TRUE" == "TRUE" ) then 
  if ( -e $OUTFILE ) mv -f $OUTFILE ${OUTFILE}~ 
  
  echo "Processing $CATALOGUE -> $OUTFILE -> ${OUTFILE:r}.fits"
  
  echo "# Summary of range of magnitudes probed by each Ruleset+Template combination in ${CATALOGUE}" >> ${OUTFILE} 
  echo "# Full catalogue file listing: " >> ${OUTFILE} 
  echo "# `/bin/ls --full-time $CATALOGUE`" >> ${OUTFILE} 
  echo "# Total entries in catalogue: `nrows $CATALOGUE`" >> ${OUTFILE} 
  echo "# This file created at `date` "  >> ${OUTFILE} 
  echo "# HISTOGRAM in magnitude follows this binnning:" >> ${OUTFILE} 
  echo "# HISTOGRAM MAG_MIN   = $HISTO_MAG_MIN" >> ${OUTFILE} 
  echo "# HISTOGRAM MAG_STEP  = $HISTO_MAG_STEP" >> ${OUTFILE} 
  echo "# HISTOGRAM MAG_NSTEP = $HISTO_MAG_NSTEP" >> ${OUTFILE} 
  #echo "# CLASS TEMPLATE NTARGETS MAG_MIN MAG_MAX MAG_HISTO" >> ${OUTFILE} 
  printf '#%-11s %-32s %4s %8s %8s %8s %8s %s\n'  'RULESET' 'TEMPLATE' 'RES' 'REDSHIFT' 'NTARGETS' 'MAG_MIN' 'MAG_MAX' 'MAG_HISTO' >> ${OUTFILE} 

#  gump ${CATALOGUE} "TEMPLATE RESOLUTION R_MAG RULESET REDSHIFT" no |& gawk -v histo_min=${HISTO_MAG_MIN} -v histo_step=${HISTO_MAG_STEP} -v histo_nstep=${HISTO_MAG_NSTEP}
  if ( `ftlist "${CATALOGUE}[1]" C mode=q | gawk 'BEGIN{n=0}$2=="REDSHIFT" {n++} END{print n}'` > 0 ) then
    set COL_EXPR = "A=TEMPLATE,B=RESOLUTION,C=R_MAG,D=RULESET,E=REDSHIFT" 
  else
    set COL_EXPR = "A=TEMPLATE,B=RESOLUTION,C=R_MAG,D=RULESET" 
  endif
  ftlist "${CATALOGUE}[1][col $COL_EXPR]" T rownum=no colheader=no mode=q |& gawk -v histo_min=${HISTO_MAG_MIN} -v histo_step=${HISTO_MAG_STEP} -v histo_nstep=${HISTO_MAG_NSTEP} '\
$0!~/good_dump: Error/{\
  res=$2;mag=$3;redshift=$4;\
  sr=$4 "," $1 "," res;\
  if(NF>=5){redshift=$5}else{redshift=0.0};\
  n[sr]++;\
  z[sr]+=redshift;\
  if(n[sr]==1||mag<magmin[sr]){magmin[sr]=mag}; \
  if(n[sr]==1||mag>magmax[sr]){magmax[sr]=mag}; \
  smr=sprintf("%s,%d",sr,int((mag-histo_min)/histo_step));\
  nm[smr]++; \
}\
END {for(sr in n) {\
  z[sr] = z[sr] / n[sr];\
  split(sr,a,","); \
  printf("%-12s %-32s %3d %8.4g %8d %8.6g %8.6g ",\
    a[1],a[2],int(a[3]),z[sr],n[sr],magmin[sr],magmax[sr]); \
  for(m=0;m<histo_nstep;m++){\
    smr=sprintf("%s,%d",sr,m);\
    printf("%6d ", nm[smr])};\
  printf("\n");}} ' | sort -n -r -k 4 >> ${OUTFILE} 


  
  #look for longest ruleset name and template names
  set LONGEST_RULESET  = `gawk '$1!~/^#/{l=length($1);if(l>max_l){max_l=l};} END {print max_l} ' ${OUTFILE}` 
  set LONGEST_TEMPLATE = `gawk '$1!~/^#/{l=length($2);if(l>max_l){max_l=l};} END {print max_l} ' ${OUTFILE}` 
  
  set CDFILE   = ${TEMPDIR}/cdfile_${HOST}_$$
  set HEADFILE = ${TEMPDIR}/headfile_${HOST}_$$
  
  echo "RULESET ${LONGEST_RULESET}A\
TEMPLATE ${LONGEST_TEMPLATE}A\
RESOLUTION 1B\
REDSHIFT 1E\
NTARGETS 1J\
MAG_MIN  1E mag\
MAG_MAX  1E mag\
MAG_HISTO ${HISTO_MAG_NSTEP}J " > ${CDFILE}

  set COL_HISTO = 7
  # for WCS keywords for vector column, see http://www.aanda.org/articles/aa/full/2002/45/aah3859/aah3859.right.html 
  echo "CREATOR = "'"${0:t}"'"\
MAG_MIN = $HISTO_MAG_MIN\
MAG_STEP  = $HISTO_MAG_STEP\
MAG_NSTEP = $HISTO_MAG_NSTEP\
WCAX${COL_HISTO}  = 1           'Number of axes'\
1CTYP${COL_HISTO} = 'LINEAR'    'Coordinate type'\
1CUNI${COL_HISTO} = 'mag'       'Coordinate units'\
1CRVL${COL_HISTO} = ${HISTO_MAG_MIN}  'Reference value'\
1CDLT${COL_HISTO} = ${HISTO_MAG_STEP} 'Coordinate increment'\
1CRPX${COL_HISTO} = -0.5        'Reference point'" >   ${HEADFILE}
  #

  ftcreate ${CDFILE} ${OUTFILE} ${OUTFILE:r}.fits headfile=${HEADFILE} clobber=yes mode=q 

  rm ${CDFILE} ${HEADFILE}
endif




if ( "TRUE" == "TRUE" ) then 
  echo "Making template description file for 4FS_ETC:"
  echo "Processing ${OUTFILE:r}.fits -> ${OUTFILE_STEM_ETC}_??.txt"
  # Count the number of LR and HR spectra"
  set RES_NAME_LIST = (  LR HR ) 
  set NSPECTRA = ( `nrows "${OUTFILE:r}.fits[1][RESOLUTION==1]"` `nrows "${OUTFILE:r}.fits[1][RESOLUTION==2]"`) 

  if ( "${CATALOGUE:h}" == "${CATALOGUE}" ) then
    set PREFIX = "templates_latest/"
  else
    set PREFIX = "${CATALOGUE:h}/templates_latest/"
  endif

  set R = 1
  while ( $R <= $#RES_NAME_LIST ) 
    if ( ${NSPECTRA[$R]} > 0 ) then
      set OUTFILE_ETC = "${OUTFILE_STEM_ETC}_${RES_NAME_LIST[$R]}.txt"
      echo "Working on ${RES_NAME_LIST[$R]} -> ${OUTFILE_ETC} -> ${OUTFILE_ETC:r}.fits"
#      gump  "${OUTFILE:r}.fits[1][RESOLUTION==$R]" 'TEMPLATE RULESET REDSHIFT MAG_MIN MAG_MAX' | gawk -v template_mag=${TEMPLATE_MAG} -v mag_step=${MAG_STEP} -v prefix="${PREFIX}" -v size=0.0 '
      ftlist "${OUTFILE:r}.fits[1][RES==$R][col A=TEMPLATE,B=RULESET,C=REDSHIFT,D=MAG_MIN,E=MAG_MAX,RES=RESOLUTION]" T rownum=no colheader=no mode=q | gawk -v template_mag=${TEMPLATE_MAG} -v mag_step=${MAG_STEP} -v prefix="${PREFIX}" -v size=0.0 '\
BEGIN {printf("#%-54s %-70s %-18s %5s %8s %5s %s\n",\
  "OBJECTNAME","FILENAME","RULESET","SIZE","REDSHIFT","MAG","MAG_RANGE")} \
$1!~/^#/{path_template=sprintf("%s%s",prefix,$1);\
  name=sprintf("%s_%s",$2,substr($1,1,length($1)-5));\
  mag_range=sprintf("%4.1f:%.1f:%g",$4-0.5,$5+0.5,mag_step); \
  printf("%-55s %-70s %-18s %5.1f %8.3g %5.2f %s\n", \
    name, path_template, $2, size, z, template_mag, mag_range);\
  }' > ${OUTFILE_ETC}

      #look for longest ruleset name and template names
      set LONGEST_IDENTIFIER = `gawk '$1!~/^#/{l=length($1);if(l>max_l){max_l=l};} END {print max_l} ' ${OUTFILE_ETC}` 
      set LONGEST_TEMPLATE   = `gawk '$1!~/^#/{l=length($2);if(l>max_l){max_l=l};} END {print max_l} ' ${OUTFILE_ETC}` 
      set LONGEST_RULESET    = `gawk '$1!~/^#/{l=length($3);if(l>max_l){max_l=l};} END {print max_l} ' ${OUTFILE_ETC}` 

      # convert to fits
      set HEADFILE = ${TEMPDIR}/headfile_${HOST}_$$
      set CDFILE = ${TEMPDIR}/cdfile_${HOST}_$$
      echo "IDENTIFIER ${LONGEST_IDENTIFIER}A\
TEMPLATE ${LONGEST_TEMPLATE}A\
RULESET ${LONGEST_RULESET}A\
SIZE 1E arcsec\
REDSHIFT 1E\
TEMPLATE_MAG 1E ABmag\
MAG_RANGE 24A ABmag" > ${CDFILE}

      echo "CREATOR = "'"${0:t}"'"" >  ${HEADFILE}

      ftcreate ${CDFILE} ${OUTFILE_ETC} ${OUTFILE_ETC:r}.fits headfile=${HEADFILE} clobber=yes mode=q 
      rm ${CDFILE} ${HEADFILE}

    else
      echo "Skipping ${RES_NAME_LIST[$R]}"
    endif
    @ R ++
  end

endif




if ( "TRUE" == "TRUE" ) then 
  set CHECK_LOG = ${OUTFILE:r}_check_templates_exist.log
  set CHECK_DIR = "${CATALOGUE:h}"
  if ( "${CHECK_DIR}" == "${CATALOGUE}" ) then
    set CHECK_DIR = "."
  endif
  set CHECK_DIR = "${CHECK_DIR}/templates_latest"
  if ( -e $CHECK_LOG ) mv -f $CHECK_LOG ${CHECK_LOG}~

  echo "# Check Templates: Verifying that all the necessary templates exist (assume that can find them in $CHECK_DIR)" | tee -a $CHECK_LOG
  echo "# Check Templates: Log of results is here: $CHECK_LOG" | tee -a $CHECK_LOG
#  foreach TEMPLATE ( `gump ${OUTFILE:r}.fits TEMPLATE no | sort -u` ) 
  foreach TEMPLATE ( `ftlist "${OUTFILE:r}.fits[1][col TEMPLATE]" T rownum=no colheader=no mode=q | sort -u` ) 
    set FILENAME = "${CHECK_DIR}/$TEMPLATE"
    if ( -e "$FILENAME" ) then
      echo "Ok:    this file exists:     $FILENAME" >> $CHECK_LOG
    else
      echo "Error: this file is missing: $FILENAME" >> $CHECK_LOG
    endif
  end
  set N = `grep -c 'this file is missing' $CHECK_LOG`
  echo "# Check Templates: There were $N missing template files" | tee -a $CHECK_LOG
endif


if ( "TRUE" == "TRUE" && -X cat2hpx ) then 
  echo "Making HEALPix maps:"
  foreach RES ( 7 9 ) 
    cat2hpx infile=${CATALOGUE}+1 outstem=${CATALOGUE:r}_res${RES} long=RA lat=DEC res=${RES} clobber=yes
    gzip -vf ${CATALOGUE:r}_res${RES}_???.fits
  end
endif



exit
