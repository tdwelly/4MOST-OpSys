//----------------------------------------------------------------------------------
//--
//--    Filename: ETC_sky_lib.h
//--    Use: This is a header for the ETC_sky_lib.c C code file
//--
//--    Notes:
//--


#ifndef ETC_SKY_LIB_H
#define ETC_SKY_LIB_H

//standard header files - need this for FILE* pointers
#include <stdio.h>

#include "define.h"
#include "curve_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.1 $"
//#define CVS_DATE_H     "$Date: 2015/11/23 15:42:03 $"
inline static int ETC_sky_lib_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX        DEF_STRLEN

//------------------------------------------
//this struct holds a 1-D array of transmission spectra
//calculated for a number of discrete airmasses 
typedef struct {
  char str_name[STR_MAX]; //identifier
  char str_filename[STR_MAX]; //identifier

  long num_curves;
  double *pairmass;
  curve_struct *pCurve;  

  double airmass_min; 
  double airmass_max; 

} sky_trans_struct;

//------------------------------------------
//this struct holds a 2-D array of emission spectra
//calculated for a number of discrete airmasses and zenithal sky brightnesses 
typedef struct {
  char str_name[STR_MAX]; //identifier
  char str_filename[STR_MAX]; //identifier

  long num_curves;
  double *pairmass;
  double *pskybright;   //the V-band sky brightness locally at the given airmass (mag/arcsec2)
  double *pzenbright;   //the V-band sky brightness at the zenith (mag/arcsec2)

  double airmass_min; 
  double airmass_max; 

  double skybright_min; 
  double skybright_max;

  double zenbright_min; 
  double zenbright_max;

  int index_min_airmass_min_skybright;
  int index_min_airmass_max_skybright;
  int index_max_airmass_min_skybright;
  int index_max_airmass_max_skybright;
  
  int index_min_airmass_min_zenbright;
  int index_min_airmass_max_zenbright;
  int index_max_airmass_min_zenbright;
  int index_max_airmass_max_zenbright;

  curve_struct *pCurve;  
} sky_emiss_struct;

////////////////////////////////////////////////////////////////
//this describes a general model of the sky emission/transmission
typedef struct {
  //  int type_code; 
  //add more types of skymodel
  sky_emiss_struct Emiss;
  sky_trans_struct Trans;

} sky_struct;
////////////////////////////////////////////////////////////////

int    ETC_sky_lib_print_version              (FILE* pFile);


/////////////////////////////////////////////////////////////////////////////
int sky_struct_init       ( sky_struct **ppSky );
int sky_struct_free       ( sky_struct **ppSky );
int sky_trans_struct_init ( sky_trans_struct *pTrans );
int sky_trans_struct_free ( sky_trans_struct *pTrans );
int sky_emiss_struct_free ( sky_emiss_struct *pEmiss );
int sky_emiss_struct_init ( sky_emiss_struct *pEmiss );
int sky_trans_struct_read_from_file ( sky_trans_struct *pTrans, BOOL verbose );
int sky_emiss_struct_read_from_file ( sky_emiss_struct *pEmiss, BOOL verbose );
/////////////////////////////////////////////////////////////////////////////


#endif
