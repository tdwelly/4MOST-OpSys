//----------------------------------------------------------------------------------
//--
//--    Filename: ETC_input_params_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.9 $"
//#define CVS_DATE     "$Date: 2015/10/28 15:32:00 $"
//--    Note: Definitions are in the corresponding header file ETC_input_params_lib.h
//--    Description:
//--      This module is used to control all input parameter operations
//--

#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <ctype.h>  //for toupper
#include <assert.h>   //needed for debugging

#include "ETC_input_params_lib.h"


#define ERROR_PREFIX   "#-ETC_input_params_lib.c : Error   :"
#define WARNING_PREFIX "#-ETC_input_params_lib.c : Warning :"
#define COMMENT_PREFIX "#-ETC_input_params_lib.c : Comment :"
#define DEBUG_PREFIX   "#-ETC_input_params_lib.c : DEBUG   :"


////////////////////////////////////////////////////////////////////////
//functions
int ETC_input_params_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

int ETC_input_params_struct_init (ETC_input_params_struct **ppIpar)
{
  if ( ppIpar == NULL ) return 1;
  if ( (*ppIpar) == NULL )
  {
    //make space for the struct
    if ( ((*ppIpar) = (ETC_input_params_struct*) malloc(sizeof(ETC_input_params_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  (*ppIpar)->SIM_RANDOM_SEED = 0;
  (*ppIpar)->debug = (BOOL) FALSE;
  return 0;
}

int ETC_input_params_struct_free (ETC_input_params_struct **ppIpar)
{
  if ( ppIpar == NULL ) return 1;
  if ( *ppIpar )
  {
    //now free the struct itself
    free(*ppIpar);
    *ppIpar = NULL;
  }
  return 0;
}



int ETC_input_params_print_usage_information(FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile, "\nUsage: \n\
  4FS_ETC [SWITCHES] PARAM_FILENAME=filename [PARAM1=val1] [PARAM2=val2] ...\n\
\n\
[SWITCHES] are taken from the following list:\n\n\
-d,--debug    Turn on debugging mode (expert use only)\n\n\
-h,--help     Print full help listing (and then exit)\n\
-u,--usage    Print this short usage listing\n\
-v,--version  Print full version information (and then exit)\n\
-x,--example  Print an example input parameter file \n\
              (listing all possible input parameters)\n\
\n\
Parameter values are typically supplied in the file indicated by PARAM_FILENAME.\n\
Additional input parameters can be supplied on the command line and will override \n\
any values set in the parameter file.\n\
For a full description of input parameters consult the interface documentation.\n\
The format for input parameters is 'identifier=value', strings and arrays must\n\
be supplied surrounded by quotes, and the token separator for arrays must be one\n\
of the following characters: ',' '|' ';' ' '.\n\
\n");

  return 0;
}


//ways to expand macros to strings
#define xstringify_macro(s) stringify_macro(s)
#define stringify_macro(s) #s


//add all the TS input params to the ParamList structure
int ETC_input_params_setup_ParamList ( ETC_input_params_struct *pIpar,
                                      param_list_struct* pParamList,
                                      int argc, char** argv) 
{
  param_list_struct *pPL = (param_list_struct*) pParamList;  //just an alias
  if ( pIpar == NULL || pParamList == NULL) return 1;

  //make a copy of the param_list_struct pointer
  pIpar->pParamList =  (param_list_struct*) pParamList;
  
  if(ParLib_setup_param(pPL,"PARAM_FILENAME", PARLIB_ARGTYPE_STRING, 1,pIpar->PARAM_FILENAME, "",FALSE, "parfile" ,"Input parameters filename")) return 1;
  //  ParLib_print_param_list (stdout, (param_list_struct*) pPL);

  //immediately try to read the value of this param from the command line:
  {
    param_struct *pParam = NULL;
    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL, (const char*) "PARAM_FILENAME")) == NULL) return 1;
    //    fprintf(stdout, "DEBUG: line=%d name= >%s<\n", __LINE__, pParam->name); fflush(stdout);

    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;
    //    fprintf(stdout, "DEBUG: line=%d name= >%s< value= >%s<\n", __LINE__, pParam->name, pParam->str_value); fflush(stdout);
  }

  //now set up all the other possible input parameters

  //first set up the switches
  if(ParLib_setup_param(pPL,"--help"    ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-h","Print help information and exit")) return 1;
  if(ParLib_setup_param(pPL,"--usage"   ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-u","Print short usage instructions and exit")) return 1;
  if(ParLib_setup_param(pPL,"--version" ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-v","Print version information and exit")) return 1;
  if(ParLib_setup_param(pPL,"--example" ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-x","Print example input params file and exit")) return 1;
  if(ParLib_setup_param(pPL,"--debug"   ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-d","Run in debug mode (verbose output etc)")) return 1;

  if(ParLib_setup_param(pPL,"SIM.CODE_NAME", PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_CODE_NAME,"testrun_v0.1"         ,
                        FALSE,"CODE_NAME","Human readable codename for this run of the 4FS_TS")) return 1;
  if(ParLib_setup_param(pPL,"SIM.OUTDIR",    PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_OUTDIR   ,"./"                   ,
                        FALSE,"OUTDIR", "Where should we put the output files?")) return 1;
  if(ParLib_setup_param(pPL,"SIM.MODE",      PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_MODE     ,"CALC_SNR"             ,
                        FALSE,"MODE",   "Should we calculate SNR from given TEXP, or TEXP/MAG from given SNR? (CALC_SNR,CALC_TEXP,CALC_MAG)")) return 1;
  if(ParLib_setup_param(pPL,"SIM.OUTPUT",    PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_OUTPUT   ,"SUMMARY,SPECTRA_SNR"  ,
                        FALSE,"OUTPUT","Which output types to produce? (ADD LIST OF OPTIONS HERE)")) return 1;
  if(ParLib_setup_param(pPL,"SIM.SPECFORMAT",PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_SPECFORMAT,"TABLE,NATIVE"        ,
                        FALSE,"SPECFORMAT","Which output spectral formats should be produced? (IMAGE,TABLE;NATIVE,RESAMPLED)")) return 1;
  if(ParLib_setup_param(pPL,"SIM.CLOBBER",   PARLIB_ARGTYPE_BOOL,   1,  &pIpar->SIM_CLOBBER  ,"no"                  ,
                        FALSE,"clobber", "Run in clobber mode? (existing output files will be overwritten)")) return 1;
                              
     
  if(ParLib_setup_param(pPL,"TEMPLATES.FILENAME",  PARLIB_ARGTYPE_STRING,1,pIpar->TEMPLATES_FILENAME  ,"templates.txt"  ,
                        TRUE,  "TEMPLATES",   "Name of file containing the list of spectral templates")) return 1;
  if(ParLib_setup_param(pPL,"RULELIST.FILENAME",   PARLIB_ARGTYPE_STRING,1,pIpar->RULELIST_FILENAME   ,"NONE"   ,
                        FALSE, "RULELIST",    "Name of file containing the list of spectral success rules")) return 1;
  if(ParLib_setup_param(pPL,"RULESETLIST.FILENAME",PARLIB_ARGTYPE_STRING,1,pIpar->RULESETLIST_FILENAME,"NONE",
                        FALSE, "RULESETLIST", "Name of file containing the list of spectral success rulesets")) return 1;

  // this controls how we calculate the science fibre input losses
  // NONE   - no losses (assume that already in Thoughput curves)
  // FIXED  - fixed multiplier giving fraction of object light that is transmitted
  // SEEING - Fraction of light tranmitted is calculated from the delivered seeing, the object profile and the fibre diameter
  // MATRIX - fraction of light lost is calculated from a lookup table giving the dependence on airmass,zenith seeing,tilt angle and wavelength.
  if(ParLib_setup_param(pPL,"FIBRECOUPLING.TYPE",PARLIB_ARGTYPE_STRING, 1,  pIpar->FIBRECOUPLING_TYPE,"FIXED",
                        FALSE, "FIBERCOUPLING.TYPE", "Method by which fibre entrance losses are calculated (FIXED,SEEING,MATRIX)")) return 1;
  if(ParLib_setup_param(pPL,"FIBRECOUPLING.FILENAME", PARLIB_ARGTYPE_STRING,1,pIpar->FIBRECOUPLING_FILENAME  ,"NONE"  ,
                        FALSE,  "FIBERCOUPLING.FILENAME",   "Name of file containing the matrix of fibre-coupling values (used when FIBRECOUPLING.TYPE='MATRIX')")) return 1;
  if(ParLib_setup_param(pPL,"FIBRECOUPLING.FRAC_SCIENCE",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->FIBRECOUPLING_FRAC_SCIENCE,"1.0" ,
                        FALSE, "FIBERCOUPLING.FRAC_SCIENCE" ,"Fraction of science light transmitted down fibre (used when FIBRECOUPLING.TYPE='FIXED')")) return 1;
  if(ParLib_setup_param(pPL,"FIBRECOUPLING.FRAC_SKY",    PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->FIBRECOUPLING_FRAC_SKY    ,"1.0" ,
                        FALSE, "FIBERCOUPLING.FRAC_SKY" ,"Fraction of sky transmitted down fibre (used when FIBRECOUPLING.TYPE='*')")) return 1;
  if(ParLib_setup_param(pPL,"FIBRECOUPLING.SEEING_PROFILE",PARLIB_ARGTYPE_STRING, 1,  pIpar->FIBRECOUPLING_SEEING_PROFILE,"GAUSSIAN",
                        FALSE, "FIBERCOUPLING.SEEING_PROFILE", "Functional form of seeing profile (GAUSSIAN or MOFFAT, used when FIBRECOUPLING.TYPE='SEEING')")) return 1;
  /// .... TODO add more seeing params
  
  //fibre size - in arcsec
  if(ParLib_setup_param(pPL,"SPECTRO.FIBRE_DIAM",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_FIBRE_DIAM  ,"1.45" ,
                        FALSE, "SPECTRO.FIBER_DIAM" ,"Fibre diameter (arcsec)")) return 1;
  //
  if(ParLib_setup_param(pPL,"SPECTRO.EFFECTIVE_AREA",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_EFFECTIVE_AREA,"10.0" ,
                        FALSE, "" ,"Effective collecting area of telescope (m^2)")) return 1;
  if(ParLib_setup_param(pPL,"SPECTRO.SKYSUB_RESIDUAL",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_SKYSUB_RESIDUAL,"0.0" ,
                        FALSE, "" ,"Fractional uncertaintity on sky subtraction")) return 1;

//  //-----------------resampling params---------------------//                          
//  //TODO add units options for x-axis, y-axis??
//  if(ParLib_setup_param(pPL,"RESAMPLE.MODE", PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_RESAMPLE_MODE,  "LINEAR_LAMBDA",
//                        FALSE, "RESAMPLE_MODE", "Method by which resampled spectra are binned (LINEAR_LAMBDA,LOG_LAMBDA,FREQ...)")) return 1;
//  if(ParLib_setup_param(pPL,"RESAMPLE.MIN",  PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SIM_RESAMPLE_MIN,   "3000.0" ,
//                        FALSE, "RESAMPLE_MIN" ,"Minimum wavelength/frequency coordinate at centre of 1st pixel of resampled output")) return 1;
//  if(ParLib_setup_param(pPL,"RESAMPLE.DELTA",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SIM_RESAMPLE_DELTA, "10.0" ,
//                        FALSE, "RESAMPLE_DELTA" ,"Pixel width in wavelength/frequency/log(wavelength)/log(freq) in resampled output")) return 1;
//  if(ParLib_setup_param(pPL,"RESAMPLE.NUM_PIX",PARLIB_ARGTYPE_LONG,  1, &pIpar->SIM_RESAMPLE_NUM_PIX,"700" ,
//                        FALSE, "RESAMPLE_NUM_PIX" ,"Number of pixels in resampled output")) return 1;
//  //-----------------resampling params---------------------//                          


  ///////////////////////////////////////////////////////
  //special case - accept a variable number of filters
  {
    int f;
    param_struct *pParam = NULL;

    if(ParLib_setup_param(pPL,"SIM.NUM_FILTERS" ,PARLIB_ARGTYPE_INT,  1, &pIpar->SIM_NUM_FILTERS ,"1" ,
                          FALSE, "NUM_FILTERS" ,"Number of filters (for calculating synthetic mags)")) return 1;
    //    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL,
    //                                      (const char*) "SIM.NUM_FILTERS")) == NULL) return 1;
    assert((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL,
                                                              (const char*) "SIM.NUM_FILTERS")) != NULL);

    //check to see if the parameter is supplied in the "PARAM_FILENAME=xx" file
    if ( strlen(pIpar->PARAM_FILENAME) > 0 )
    {
      if(ParLib_read_param_from_file ((param_struct*)pParam, (char*) pIpar->PARAM_FILENAME ) > 0)
      {
        fprintf(stderr, "%s Problem reading from input params file: >%s<\n", ERROR_PREFIX, pIpar->PARAM_FILENAME);
        return 1;
      }
    }
    //check to see if the parameter is supplied on the command line
    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;

    if ( pIpar->SIM_NUM_FILTERS > SIM_MAX_FILTERS )
    {
      fprintf(stderr, "%s Too many filters for synthetic magnitudes: %d (>%d)\n",
              ERROR_PREFIX, pIpar->SIM_NUM_FILTERS, SIM_MAX_FILTERS);
      return 1;
    }

    for (f=0; f<pIpar->SIM_NUM_FILTERS; f++)
    {
      char str_par_name[STR_MAX];
      char str_filter_code[STR_MAX];
      //add the standard params for each filter
      snprintf(str_filter_code, STR_MAX,"SIM.NORM_FILTER%d",f+1);

      //filter code name
      snprintf(str_par_name, STR_MAX, "%s.NAME",str_filter_code );
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SIM_NORM_FILTER_NAME[f] ,"-" ,
                            FALSE, "" ,"Short name for filter (e.g. 'r','B','u*')")) return 1;

      //filter file name
      snprintf(str_par_name, STR_MAX, "%s.FILENAME",str_filter_code );
      if(ParLib_setup_param(pPL,str_par_name, PARLIB_ARGTYPE_STRING,1,pIpar->SIM_NORM_FILTER_FILENAME[f],"NONE"  ,
                            FALSE,  "","Name of file containing the normalising filter bandpass")) return 1;
      
    }
  }

  if(ParLib_setup_param(pPL,"SIM.NORM_FILTER.MAGSYS",   PARLIB_ARGTYPE_STRING,1,pIpar->SIM_NORM_FILTER_MAGSYS,"AB",
                        FALSE, "NORM_FILTER.MAGSYS",   "Magnitude system for normailsing templates (AB,Vega)")) return 1;

  ///////////////////////////////////////////////////////
  //special case - accept a variable number of spectrograph arms
  {
    int a;
    param_struct *pParam = NULL;

    if(ParLib_setup_param(pPL,"SPECTRO.NUM_ARMS" ,PARLIB_ARGTYPE_INT,  1, &pIpar->SPECTRO_NUM_ARMS ,"1" ,
                          TRUE, "NUM_ARMS" ,"Number of spectrograph arms")) return 1;
    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL,
                                                           (const char*) "SPECTRO.NUM_ARMS")) == NULL) return 1;

    //check to see if the "SPECTRO.NUM_ARMS" is supplied in the "PARAM_FILENAME=xx" file
    if ( strlen(pIpar->PARAM_FILENAME) > 0 )
    {
      if(ParLib_read_param_from_file ((param_struct*)pParam, (char*) pIpar->PARAM_FILENAME ) > 0)
      {
        fprintf(stderr, "%s Problem reading from input params file: >%s<\n", ERROR_PREFIX, pIpar->PARAM_FILENAME);
        return 1;
      }
    }
    //check to see if the "SPECTRO.NUM_ARMS" is supplied on the command line
    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;

    if ( pIpar->SPECTRO_NUM_ARMS > MAX_SPECTRO_ARMS )
    {
      fprintf(stderr, "%s Too many spectrograph arms: %d (>%d)\n", ERROR_PREFIX, pIpar->SPECTRO_NUM_ARMS, MAX_SPECTRO_ARMS);
      return 1;
    }
    
    for (a=0; a<pIpar->SPECTRO_NUM_ARMS; a++)
    {
      char str_par_name[STR_MAX];
      char str_arm_code[STR_MAX];
      //add the standard params for each spectrograph
      snprintf(str_arm_code, STR_MAX,"SPECTRO.ARM%d",a+1);

      //arm name
      snprintf(str_par_name, STR_MAX, "%s.CODENAME",str_arm_code );
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SPECTRO_ARM_CODENAME[a] ,"Arm1"            ,
                            FALSE, "" ,"Codename for spectrograph arm")) return 1;

      //resolution filename
      snprintf(str_par_name, STR_MAX, "%s.RES_FILENAME", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SPECTRO_ARM_RES_FILENAME[a]  ,"NONE"       ,
                            TRUE, "" ,"Filename describing spectral resolution")) return 1;

      //throughput filename
      snprintf(str_par_name, STR_MAX, "%s.TPUT_FILENAME", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SPECTRO_ARM_TPUT_FILENAME[a] ,"NONE"       ,
                            TRUE, "" ,"Filename describing spectral throughput")) return 1;

      //number of pixels to add up per object in x-dispersion direction
      snprintf(str_par_name, STR_MAX, "%s.APER_SIZE", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_APER_SIZE[a]        ,"5.0"       ,
                            FALSE, "" ,"Effective size of software aperture in the cross-dispersion direction (unbinned pix)")) return 1;

      //fraction of flux contained in aperture
      snprintf(str_par_name, STR_MAX, "%s.APER_EEF", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_APER_EEF[a]         ,"1.0"       ,
                            FALSE, "" ,"Fraction of flux contained within the software aperture")) return 1;

      //fraction of flux contained in the peak pixel
      snprintf(str_par_name, STR_MAX, "%s.PEAK_PIX_FRAC", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_PEAK_PIX_FRAC[a]    ,"0.5"       ,
                            FALSE, "" ,"The maximum fraction of the flux that is contained within a single pixel (in the cross-dispersion direction, after on-chip binning)")) return 1;

      //      //FWHM of spectrum in spatial direction, in um  
      //      snprintf(str_par_name, STR_MAX, "%s.SPATIAL_FWHM", str_arm_code);
      //      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_SPATIAL_FWHM[a]  ,"30.0"   ,
      //                            FALSE, "" ,"FWHM of spectra in the cross-dispersion direction (um)")) return 1;

      //pixel size in um
      snprintf(str_par_name, STR_MAX, "%s.PIXEL_SIZE", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_PIXEL_SIZE[a]    ,"15"     ,
                            FALSE, "" ,"Pixel size (um, in the dispersion direction)")) return 1;
      
      //readnoise (e-/pix)
      snprintf(str_par_name, STR_MAX, "%s.READ_NOISE", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_READ_NOISE[a]    ,"3.0"    ,
                            FALSE, "" ,"CCD read noise (e-/pix)")) return 1;

      //dark current (e-/hr/pix)
      snprintf(str_par_name, STR_MAX, "%s.DARK_CURRENT", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_DARK_CURRENT[a]  ,"3.0"   ,
                            FALSE, "" ,"CCD dark current (e-/hr/pix)")) return 1;

//      //ADC gain (e-/ADU)
//      snprintf(str_par_name, STR_MAX, "%s.GAIN", str_arm_code);
//      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_GAIN[a]          ,"1.0"   ,
//      FALSE, "" ,"CCD readout gain (e-/ADU)")) return 1;

      //Full well
      snprintf(str_par_name, STR_MAX, "%s.FULL_WELL", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_FULL_WELL[a]     ,"1.5e5" ,
                            FALSE, "" ,"Full well capacity of the CCD (e-/pix)")) return 1;

      //on chip binning 
      snprintf(str_par_name, STR_MAX, "%s.BINNING.DISP", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT, 1, &pIpar->SPECTRO_ARM_BINNING_DISP[a]     ,"1"   ,
                            FALSE, "" ,"On-chip binning in dispersion direction")) return 1;
      snprintf(str_par_name, STR_MAX, "%s.BINNING.CROSS", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT, 1, &pIpar->SPECTRO_ARM_BINNING_CROSS[a]     ,"1"   ,
                            FALSE, "" ,"On-chip binning in cross-dispersion direction")) return 1;
   

      //dispersion type
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.TYPE",str_arm_code );
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING,1, pIpar->SPECTRO_ARM_LAMBDA_TYPE[a]    ,"LINEAR",
                            FALSE,"","Type of dispersion description, LINEAR, from DISPFILE, or FULLFILE")) return 1;
      //dispersion filename
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.FILENAME", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING,1, pIpar->SPECTRO_ARM_LAMBDA_FILENAME[a],"NONE"     ,
                            TRUE ,"","Filename describing wavelength solution (dispersion scale or full lambda solution)")) return 1;
      //number of pixels on ccd in dispersion direction
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.NUMPIX", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT   ,1,&pIpar->SPECTRO_ARM_LAMBDA_NUMPIX[a]  ,"4096"  ,
                            FALSE,"","Number of CCD pixels in the dispersion direction")) return 1;
      //reference pixel coord
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.REFPIX", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE,1,&pIpar->SPECTRO_ARM_LAMBDA_REFPIX[a]  ,"2048.0",
                            FALSE,"","Coordinate of the reference pixel")) return 1;
      //wavelength of ref pixel, in AA
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.REFVAL", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE,1,&pIpar->SPECTRO_ARM_LAMBDA_REFVAL[a]  ,"400.0" ,
                            FALSE,"","Wavelength at the reference pixel (AA)")) return 1;
      //dispersion pixel scale, in AA/pix
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.DISP", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE,1,&pIpar->SPECTRO_ARM_LAMBDA_DISP[a]    ,"0.1"   ,
                            FALSE,"","Linear dispersion scale (AA/pix)")) return 1;


    }
  }

  
  if(ParLib_setup_param(pPL,"SKY.TRANSMISSION.FILENAME",PARLIB_ARGTYPE_STRING, 1, pIpar->SKY_TRANSMISSION_FILENAME ,"sky_trans.fits" ,
                        TRUE, "SKY_TRANS", "Name of file containing the sky transmission info")) return 1;
  if(ParLib_setup_param(pPL,"SKY.EMISSION.FILENAME"    ,PARLIB_ARGTYPE_STRING, 1, pIpar->SKY_EMISSION_FILENAME     ,"sky_emiss.fits" ,
                        TRUE, "SKY_EMISS", "Name of file containing the sky emission info")) return 1;
  
  
  //observational parameters to simulate
  if(ParLib_setup_param(pPL,"OBS_PARAMS.INTERP_METHOD", PARLIB_ARGTYPE_STRING, 1, pIpar->OBS_PARAMS_INTERP_METHOD , "NEAREST",
                        FALSE, "OBS_PARAMS_INTERP", "Method to use when interpolating obs params grid: NEAREST,LINEAR,SPLINE")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.SKYBRIGHT_TYPE",PARLIB_ARGTYPE_STRING, 1, pIpar->OBS_PARAMS_SKYBRIGHT_TYPE, "ZENITH" ,
                        FALSE, "SKYBRIGHT_TYPE" ,"Is the specified sky brightness to be measured at ZENITH or LOCALly?")) return 1;

  if(ParLib_setup_param(pPL,"OBS_PARAMS.AIRMASS",      PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_AIRMASS     ,"1.0"  ,
                        FALSE, "" ,"List of airmasses to simulate")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.IQ",           PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_IQ          ,"1.0"  ,
                        FALSE, "" ,"List of delivered image quality values to simulate (V-band,FWHM,arcsec)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.SKYBRIGHT",    PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_SKYBRIGHT   ,"22.0" ,
                        FALSE, "" ,"List of sky brightnesses to simulate (V-band,ABmag/arcsec2)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.TILT",         PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_TILT        ,"0.0"  ,
                        FALSE, "" ,"List of tilts to simulate (mm)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.MISALIGNMENT", PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_MISALIGN    ,"0.0"  ,
                        FALSE, "" ,"List of fibre->target misalignments to simulate (arcsec)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.TEXP",         PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_TEXP        ,"1200.",
                        FALSE, "" ,"List of total exposure times to simulate (sec)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.NSUB",         PARLIB_ARGTYPE_INT,    MAX_OBS_PARAMS, (double*) pIpar->pOBS_PARAMS_NSUB        ,"1"    ,
                        FALSE, "" ,"List of numbers of sub-exposures to simulate")) return 1;

  return 0;
}


//this does everything related to reading command line and input files
//return < 0 if error
//return = 0 if we should proceed as normal
//return = 1 if need to exit (without error)
int ETC_input_params_handler ( ETC_input_params_struct **ppIpar,
                              param_list_struct **ppParamList,
                              int argc, char** argv)
{
  //  int status;
  param_struct *pP = NULL; 
  if ( ppIpar == NULL || ppParamList == NULL ) return -1;
  
  //make space for and init the TS input params struct 
  if ( ETC_input_params_struct_init ((ETC_input_params_struct**) ppIpar)) return -1;
  /////////////////////////////////////////////////////////

  //make space for and init the param_list structure
  if ( ParLib_init_param_list((param_list_struct**) ppParamList))  return -1;

  //now add all the input parameters
  if ( ETC_input_params_setup_ParamList ( (ETC_input_params_struct*) *ppIpar,
                                         (param_list_struct*) *ppParamList,
                                         (int) argc, (char**) argv))
  {
    fprintf(stderr, "%s Problem setting up the input parameter list...\n", ERROR_PREFIX);
    fflush(stderr);
    return -1;
  }

  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "PARAM_FILENAME" )) == NULL)
  {
    fprintf(stderr, "%s You do not have PARAM_FILENAME added as a parameter!\n", ERROR_PREFIX);
    fflush(stderr);
    return -1;
  }
  if ( ParLib_read_param_from_command_line ((param_struct*) pP, (int) argc, (char**) argv))
  {
    //    fprintf(stderr, "%s Problem reading the input parameter file\n", ERROR_PREFIX);
  }
  if ( pP->supplied && strlen(pP->pCval))
  {
    //    status = 0;
    if (ParLib_read_params_from_file ((param_list_struct*) *ppParamList, (*ppIpar)->PARAM_FILENAME))
    {
      ETC_input_params_print_usage_information(stderr);
      return -1;
    }
  }
  //  else status = 1;
  
  if ( ParLib_read_params_from_command_line ((param_list_struct*) *ppParamList, (int) argc, (char**) argv))
  {
    return -1;
  }

  //interpret any supplied command line switches here
  ////////////////////////////////////////////////////////
  //first see if we should print out the version information
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--VERSION" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( print_full_version_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the help file
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--help" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( print_full_help_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the useage info
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--usage" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( ETC_input_params_print_usage_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the example input params file
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--example" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( ParLib_print_example_params_file((FILE*) stdout, (param_list_struct*) *ppParamList)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if the debug flag is set
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--debug" )) == NULL)  return -1;
  if ( pP->supplied ) (*ppIpar)->debug = TRUE;
  else                (*ppIpar)->debug = FALSE;
  ////////////////////////////////////////////////////////


  //special check - if nothing is supplied on the command line
  if ( argc == 1 )
  {
    fprintf(stdout, "%s Printing help information because you didn't supply any input parameters:\n", COMMENT_PREFIX);
    if ( ETC_input_params_print_usage_information((FILE*) stdout)) return -1;
    return -1;
  }
 
  if ( ParLib_check_param_list ((param_list_struct*) *ppParamList))
  {
    ParLib_print_param_list (stdout, (param_list_struct*) *ppParamList);
    return -1;
  }

  //  if ( (*ppIpar)->debug )
  ParLib_print_param_list (stdout, (param_list_struct*) *ppParamList);

  //seed the random number generator(s)
  if ( util_srand((*ppIpar)->SIM_RANDOM_SEED) == 0)
  {
    fprintf(stderr, "%s Problem seeding the random number generator\n", ERROR_PREFIX);
    return 1;
  }

  return 0;
}




///////////////////////////////////////////////////////////////////////////
////these funcs interpret the input values in a sensible way

int ETC_input_params_interpret_spectrograph (ETC_input_params_struct *pIpar,
                                             spectrograph_struct    *pSpec,
                                             BOOL verbose )
{
  int i;
  if ( pIpar        == NULL ||
       pSpec        == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting spectrograph parameters\n", COMMENT_PREFIX); fflush(stdout);
  //  fprintf(stdout, "%s (at file %s line %d)\n", DEBUG_PREFIX, __FILE__, __LINE__); fflush(stdout); //DEBUG//

  //first we need to know how many arms our spectrograph has
  pSpec->num_arms = pIpar->SPECTRO_NUM_ARMS;
  if ( pSpec->num_arms > MAX_SPECTRO_ARMS ||
       pSpec->num_arms <= 0 )
  {
    fprintf(stderr, "%s Number of spectrograph arms (%d) exceeds the maximum (%d) or is < 1\n",
            ERROR_PREFIX, pSpec->num_arms , MAX_SPECTRO_ARMS);
    return 1;
  }
    

  
  //make some space for the arms
  if ((pSpec->pArm = (arm_struct*) malloc(pSpec->num_arms * sizeof(arm_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for (i=0; i<pSpec->num_arms; i++ )
  {
    arm_struct    *pArm    = (arm_struct*)    &(pSpec->pArm[i]);
    lambda_struct *pLambda = (lambda_struct*) &(pArm->Lambda);
    if ( arm_struct_init ( (arm_struct*) pArm ))  return 1;

    strncpy(pArm->str_name, pIpar->SPECTRO_ARM_CODENAME[i], STR_MAX);

    if ( verbose) {
      fprintf(stdout, "%s Interpreting spectrograph arm parameters: %s\n", DEBUG_PREFIX, pArm->str_name); fflush(stdout);
    }
    pArm->aper_size      = (double) pIpar->SPECTRO_ARM_APER_SIZE[i]; 
    pArm->aper_eef       = (double) pIpar->SPECTRO_ARM_APER_EEF[i]; 
    pArm->peak_pix_frac  = (double) pIpar->SPECTRO_ARM_PEAK_PIX_FRAC[i]; 
    //    pArm->spatial_fwhm   = (double) pIpar->SPECTRO_ARM_SPATIAL_FWHM[i]; 
    pArm->pixel_size     = (double) pIpar->SPECTRO_ARM_PIXEL_SIZE[i];
    pArm->read_noise     = (double) pIpar->SPECTRO_ARM_READ_NOISE[i];
    pArm->dark_current_h = (double) pIpar->SPECTRO_ARM_DARK_CURRENT[i];
    pArm->dark_current_s = (double) pArm->dark_current_h / HOURS_TO_SECS;
    pArm->full_well      = (double) pIpar->SPECTRO_ARM_FULL_WELL[i];
    pArm->binning_disp   = (int)    pIpar->SPECTRO_ARM_BINNING_DISP[i];
    pArm->binning_cross  = (int)    pIpar->SPECTRO_ARM_BINNING_CROSS[i];
    pArm->effective_area = (double) pIpar->SPECTRO_EFFECTIVE_AREA; 
    pArm->fiber_diam     = (double) pIpar->SPECTRO_FIBRE_DIAM; 
    pArm->fiber_area     = (double) M_PI * SQR((double)0.5*pArm->fiber_diam);   // fiber area = pi r^2
    pArm->skysub_residual= (double) pIpar->SPECTRO_SKYSUB_RESIDUAL;

    // noise total = noise per pix * sqrt(num pix)
    //pArm->sq_read_noise = (double) pArm->aper_size*SQR(pArm->read_noise); 
    //take account of binning in cross dispersion direction
    // but ignore issue that aper_size may not be an integer multiple of binning_cross
    //in such cases can imagine aper_size to be the 'effective' number of pixels considered (after weighting) 
    pArm->sq_read_noise = SQR(pArm->read_noise)* (double) pArm->aper_size /(double) MAX(1,pArm->binning_cross);  

    
    strncpy(pLambda->str_type, pIpar->SPECTRO_ARM_LAMBDA_TYPE[i], STR_MAX);
    //determine the dispersion description type
    {
      const char *plist_names[]  = {"LINEAR",
                                    "DISPFILE",
                                    "FULLFILE"};
      const int   plist_values[] = {LAMBDA_TYPE_CODE_DISP_LINEAR,
                                    LAMBDA_TYPE_CODE_DISP_CURVE,
                                    LAMBDA_TYPE_CODE_LAMBDA_CURVE};
      int len = (int) (sizeof(plist_values)/sizeof(int));
      if (util_select_option_from_string(len, plist_names, plist_values,
                                         pIpar->SPECTRO_ARM_LAMBDA_TYPE[i],  &(pLambda->type)))
      {
        fprintf(stderr, "%s I do not understand this SPECTRO.ARM%d.LAMBDA.TYPE: >%s<\n",
                ERROR_PREFIX, (i+1), pIpar->SPECTRO_ARM_LAMBDA_TYPE[i]);fflush(stderr);
        return 1;
      }
    }
    
    pLambda->num_pix = (long)   pIpar->SPECTRO_ARM_LAMBDA_NUMPIX[i]; 
    pLambda->refpix  = (double) pIpar->SPECTRO_ARM_LAMBDA_REFPIX[i]; 
    pLambda->refval  = (double) pIpar->SPECTRO_ARM_LAMBDA_REFVAL[i]; 
    pLambda->disp    = (double) pIpar->SPECTRO_ARM_LAMBDA_DISP[i]; 
    strncpy(pLambda->crvFile.str_filename, pIpar->SPECTRO_ARM_LAMBDA_FILENAME[i], STR_MAX);

    if ( pLambda->type == LAMBDA_TYPE_CODE_DISP_CURVE   ||
         pLambda->type == LAMBDA_TYPE_CODE_LAMBDA_CURVE )       
    {

      if ( pLambda->type == LAMBDA_TYPE_CODE_DISP_CURVE  )
      {
        strncpy(pLambda->crvFile.str_colname_x, "LAMBDA", STR_MAX);
        strncpy(pLambda->crvFile.str_colname_y, "DISPERSION", STR_MAX);
      }
      else if ( pLambda->type == LAMBDA_TYPE_CODE_LAMBDA_CURVE )
      {
        strncpy(pLambda->crvFile.str_colname_x, "PIXEL", STR_MAX);
        strncpy(pLambda->crvFile.str_colname_y, "LAMBDA", STR_MAX);
      }
      //read the file from disk
      if ( curve_struct_read_from_file ( (curve_struct*) &(pLambda->crvFile), (BOOL) verbose))
      {
        fprintf(stderr, "%s I had problems reading curve: %s\n",
                ERROR_PREFIX, pLambda->crvFile.str_filename);
        return 1;
      }
      if ( pLambda->type == LAMBDA_TYPE_CODE_LAMBDA_CURVE )       
      {
        pLambda->num_pix = (long) pLambda->crvFile.num_rows;
      }
    }

    //make some space for the lambda array
    if ((pLambda->pl  = (double*) malloc(pLambda->num_pix * sizeof(double))) == NULL ||
        (pLambda->pdl = (double*) malloc(pLambda->num_pix * sizeof(double))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }

    switch ( pLambda->type )
    {
      long j;
    case LAMBDA_TYPE_CODE_DISP_LINEAR :
      for(j=0;j<pLambda->num_pix;j++)
      {
        pLambda->pl[j]  = (double) pLambda->refval + ((double) j - pLambda->refpix) * pLambda->disp;
        pLambda->pdl[j] = (double) pLambda->disp;
      }
      break;

    case LAMBDA_TYPE_CODE_DISP_CURVE :
      {
        long refpix = (long) (0.5 + pLambda->refpix);  //TODO proper rounding?? Check this rounding is ok
        if ( refpix < 0 || refpix >= pLambda->num_pix )
        {
          fprintf(stderr, "%s Error - refpix value (=%g) is outside valid pixel range [%ld:%ld]\n",
                  ERROR_PREFIX, pLambda->refpix, (long) 0, (long) pLambda->num_pix);
          return 1;
        }
        //start at the reference pixel
        pLambda->pl[refpix]  = (double) pLambda->refval;
        //now work up to end of CCD
        j=refpix+1;
        while ( j<pLambda->num_pix )
        {
          long jmin,jmax;
          double disp, dl;
          //do a binary search to get the location in the dispersion file
          if ( util_binary_search_double ((long) pLambda->crvFile.num_rows,
                                          (double*) pLambda->crvFile.px,
                                          (double) pLambda->pl[j-1]/pLambda->crvFile.unit_x,
                                          (long*) &jmin, (long*) &jmax) < 0 ) return 1;
          disp = 0.5 * ( pLambda->crvFile.py[jmin] + pLambda->crvFile.py[jmax]); //TODO : proper interpolation
          //scale the dispersion units to internal standard of AA/um, then mult by pixsize (in um) to get AA/pix
          dl = disp * pLambda->crvFile.unit_y * (pArm->pixel_size);
          pLambda->pl[j] = (double) pLambda->pl[j-1] + dl;

          //          fprintf(stdout, "%s DEBUG1 ARM %s %6ld  [%4ld,%4ld,%12.5f,%12.5f,%12.5f,%12.5f,%12.5f] %8.5f %8.5f %12.5f\n",
          //                  COMMENT_PREFIX, pArm->str_name, j,
          //                  jmin,jmax,pLambda->crvFile.px[jmin],pLambda->crvFile.px[jmax],
          //                  pLambda->crvFile.py[jmin],pLambda->crvFile.py[jmax], pLambda->pl[j-1]/pLambda->crvFile.unit_x,
          //                  disp, dl, pLambda->pl[j]);

          j ++;
        }
        //now work down to beginning of CCD
        j=refpix-1;
        while ( j>=0 )
        {
          long jmin,jmax;
          double disp, dl;
          //do a binary search to get the location in the dispersion file
          if ( util_binary_search_double ((long) pLambda->crvFile.num_rows,
                                          (double*) pLambda->crvFile.px,
                                          (double) pLambda->pl[j+1]/pLambda->crvFile.unit_x,
                                          (long*) &jmin, (long*) &jmax) < 0 ) return 1;
          disp = 0.5 * ( pLambda->crvFile.py[jmin] + pLambda->crvFile.py[jmax]); //TODO : proper interpolation
          dl = disp * pLambda->crvFile.unit_y * (pArm->pixel_size);
          pLambda->pl[j] = (double) pLambda->pl[j+1] - dl;

//          fprintf(stdout, "%s DEBUG1 ARM %s %6ld  [%4ld,%4ld,%12.5f,%12.5f,%12.5f,%12.5f,%12.5f] %8.5f %8.5f %12.5f\n",
//                  COMMENT_PREFIX, pArm->str_name, j,
//                  jmin,jmax,pLambda->crvFile.px[jmin],pLambda->crvFile.px[jmax],
//                  pLambda->crvFile.py[jmin],pLambda->crvFile.py[jmax], pLambda->pl[j-1]/pLambda->crvFile.unit_x,
//                  disp, dl, pLambda->pl[j]);
          j --;
        }
        // now calc the dl for each pixel 
        pLambda->pdl[0] = pLambda->pl[1] - pLambda->pl[0];
        for(j=1;j<pLambda->num_pix;j++) pLambda->pdl[j] = pLambda->pl[j] - pLambda->pl[j-1];
        
        //        fprintf(stdout, "\n\n%s DEBUG0 ARM %s\n", COMMENT_PREFIX, pArm->str_name) ;
        //        for(j=0;j<pLambda->num_pix;j++)
        //        {
        //          fprintf(stdout, "%s DEBUG0 ARM %s %6ld %12.5f %12.5f\n", COMMENT_PREFIX, pArm->str_name, j, pLambda->pl[j],pLambda->pdl[j]);
        //        }

      }
      //      fprintf(stderr, "%s Error - I have not yet implemented this option (at file %s line %d)\n",
      //              ERROR_PREFIX, __FILE__, __LINE__);
      //      return 1;
      break;
      
    case LAMBDA_TYPE_CODE_LAMBDA_CURVE :
      for(j=0;j<pLambda->num_pix;j++)
      {
        pLambda->pl[j] = (double) pLambda->crvFile.py[j];
        if ( j >= 1 ) {pLambda->pdl[j] = pLambda->pl[j] - pLambda->pl[j-1];};
      }
      pLambda->pdl[0] = pLambda->pl[1] - pLambda->pl[0];
      break;
      
    default:
      return 1;
    }

    pLambda->min = (double) pLambda->pl[0];
    pLambda->max = (double) pLambda->pl[pLambda->num_pix-1];

    if ( verbose) {
      fprintf(stdout, "%s Spectrograph arm %s: wavelength range: [%8g:%8g]AA npixels=%ld\n",
              DEBUG_PREFIX,
              pArm->str_name,
              pLambda->min, pLambda->max,
              pLambda->num_pix); fflush(stdout);
    }
    //read the throughput curve from file
    strncpy(pArm->crvTput.str_filename, pIpar->SPECTRO_ARM_TPUT_FILENAME[i], STR_MAX);
    strncpy(pArm->crvTput.str_colname_x, "LAMBDA", STR_MAX);
    strncpy(pArm->crvTput.str_colname_y, "TRANSMISSION", STR_MAX);
    if ( verbose) {
      fprintf(stdout, "%s Reading spectrograph arm throughput file: %s\n",
              DEBUG_PREFIX, pArm->crvTput.str_filename); fflush(stdout);
    }
    //read the file from disk
    if ( curve_struct_read_from_file ( (curve_struct*) &(pArm->crvTput), (BOOL) verbose))
    {
      fprintf(stderr, "%s I had problems reading throughput curve: %s\n",
              ERROR_PREFIX, pArm->crvTput.str_filename);
      return 1;
    }

    
    //read the resolution curve from file
    strncpy(pArm->crvRes.str_filename, pIpar->SPECTRO_ARM_RES_FILENAME[i], STR_MAX);
    strncpy(pArm->crvRes.str_colname_x, "LAMBDA", STR_MAX);
    strncpy(pArm->crvRes.str_colname_y, "RESOLUTION", STR_MAX);
    if ( verbose) {
      fprintf(stdout, "%s Reading spectrograph arm resolution file: %s\n",
              DEBUG_PREFIX, pArm->crvRes.str_filename); fflush(stdout);
    }
    //read the file from disk
    if ( curve_struct_read_from_file ( (curve_struct*) &(pArm->crvRes), (BOOL) verbose))
    {
      fprintf(stderr, "%s I had problems reading resolution curve: %s\n",
              ERROR_PREFIX, pArm->crvRes.str_filename);
      return 1;
    }

  }

  
  return 0;
}



int ETC_input_params_interpret_templatelist (ETC_input_params_struct *pIpar,
                                            templatelist_struct    *pTL )
{
  if ( pIpar   == NULL ||
       pTL     == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting templatelist parameters\n", COMMENT_PREFIX); fflush(stdout);
  
  strncpy(pTL->str_filename, pIpar->TEMPLATES_FILENAME, STR_MAX);

  return 0;
}

int ETC_input_params_interpret_rules (ETC_input_params_struct *pIpar,
                                     rulelist_struct        *pRuleList,
                                     rulesetlist_struct     *pRuleSetList)
                                     
{
  if ( pIpar        == NULL ||
       pRuleList    == NULL ||
       pRuleSetList == NULL) return 1;
  //
  fprintf(stdout, "%s Interpreting spectral success rules parameters\n", COMMENT_PREFIX); fflush(stdout);
  
  strncpy(pRuleList->str_filename, pIpar->RULELIST_FILENAME, STR_MAX);
  strncpy(pRuleSetList->str_filename, pIpar->RULESETLIST_FILENAME, STR_MAX);

  return 0;
}


int ETC_input_params_interpret_sky (ETC_input_params_struct *pIpar,
                                   sky_struct   *pSky )
{
  if ( pIpar   == NULL ||
       pSky    == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting sky parameters\n", COMMENT_PREFIX); fflush(stdout);
  
  strncpy(pSky->Trans.str_filename, pIpar->SKY_TRANSMISSION_FILENAME, STR_MAX);
  strncpy(pSky->Emiss.str_filename, pIpar->SKY_EMISSION_FILENAME,     STR_MAX);

  return 0;
}


int ETC_input_params_interpret_conditions (ETC_input_params_struct *pIpar,
                                           condlist_struct      *pCondList,
                                           BOOL verbose )
{
  int i,nsub;
  if ( pIpar == NULL ||
       pCondList == NULL ) return 1;

  if ( pIpar->pParamList == NULL ) return 1;
  
  //
  fprintf(stdout, "%s Interpreting observing conditions parameters\n", COMMENT_PREFIX); fflush(stdout);

  //determine the interpolation method
  strncpy(pCondList->str_interp_method, pIpar->OBS_PARAMS_INTERP_METHOD, STR_MAX);
  {
    const char *plist_names[]  = {"NEAREST",
                                  "LINEAR",
                                  "SPLINE"};
    const int   plist_values[] = {INTERP_METHOD_NEAREST,
                                  INTERP_METHOD_LINEAR,
                                  INTERP_METHOD_SPLINE};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pCondList->str_interp_method,  &(pCondList->interp_method)))
    {
      fprintf(stderr, "%s I do not understand this OBS_PARAMS.INTERP_METHOD: >%s<\n",
                ERROR_PREFIX, pCondList->str_interp_method);fflush(stderr);
      return 1;
    }
  }

  //determine the type of sky brightness measure to use
  strncpy(pCondList->str_skybright_type, pIpar->OBS_PARAMS_SKYBRIGHT_TYPE, STR_MAX);
  {
    const char *plist_names[]  = {"ZENITH", "ZENITHAL", "ZEN", 
                                  "LOCAL",  "DELIVERED", "LOCALLY"};

    const int   plist_values[] = {SKYBRIGHT_TYPE_ZENITH,SKYBRIGHT_TYPE_ZENITH,SKYBRIGHT_TYPE_ZENITH,
                                  SKYBRIGHT_TYPE_LOCAL, SKYBRIGHT_TYPE_LOCAL, SKYBRIGHT_TYPE_LOCAL};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pCondList->str_skybright_type,  &(pCondList->skybright_type)))
    {
      fprintf(stderr, "%s I do not understand this OBS_PARAMS.SKYBRIGHT_TYPE: >%s<\n",
                ERROR_PREFIX, pCondList->str_skybright_type);fflush(stderr);
      return 1;
    }
  }


  //determine the fibrecoupling type
  {
    const char *plist_names[]  = {"NONE",
                                  "FIXED",
                                  "SEEING",
                                  "FILE", "MATRIX"};
    const int   plist_values[] = {FIBRECOUPLING_TYPE_CODE_NONE,
                                  FIBRECOUPLING_TYPE_CODE_FIXED,
                                  FIBRECOUPLING_TYPE_CODE_SEEING,
                                  FIBRECOUPLING_TYPE_CODE_MATRIX, FIBRECOUPLING_TYPE_CODE_MATRIX};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pIpar->FIBRECOUPLING_TYPE, &(pCondList->Fibrecoupling.type_code)))
    {
      fprintf(stderr, "%s I do not understand this FIBRECOUPLING.TYPE: >%s<\n",
              ERROR_PREFIX, pIpar->FIBRECOUPLING_TYPE);fflush(stderr);
      return 1;
    }
  }
  
  pCondList->Fibrecoupling.frac_sky    = (double) pIpar->FIBRECOUPLING_FRAC_SKY; 
  if ( pCondList->Fibrecoupling.type_code == FIBRECOUPLING_TYPE_CODE_FIXED )
    pCondList->Fibrecoupling.frac_science = (double) pIpar->FIBRECOUPLING_FRAC_SCIENCE; 
  else
    pCondList->Fibrecoupling.frac_science = (double) 1.0;
  
  //determine the seeing profile type
  {
    const char *plist_names[]  = {"GAUSSIAN", "GAUSS",
                                  "MOFFAT"};
    const int   plist_values[] = {SEEING_PROFILE_CODE_GAUSSIAN,SEEING_PROFILE_CODE_GAUSSIAN,
                                  SEEING_PROFILE_CODE_MOFFAT};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pIpar->FIBRECOUPLING_SEEING_PROFILE, &(pCondList->Fibrecoupling.seeing_profile_code)))
    {
      fprintf(stderr, "%s I do not understand this FIBRECOUPLING.SEEING_PROFILE: >%s<\n",
              ERROR_PREFIX, pIpar->FIBRECOUPLING_SEEING_PROFILE);fflush(stderr);
      return 1;
    }
  }

  if ( pCondList->Fibrecoupling.type_code == FIBRECOUPLING_TYPE_CODE_MATRIX )
  {
    //take a copy of the filename which contains the fibrecoupling matrix
    //then attempt to read in the fibrecoupling matrix
    strncpy(pCondList->Fibrecoupling.str_filename, pIpar->FIBRECOUPLING_FILENAME, STR_MAX);
    if ( fibrecoupling_struct_read_from_file ( (fibrecoupling_struct*) &(pCondList->Fibrecoupling), (BOOL) verbose))
    {
      fprintf(stderr, "%s I had problems reading the fibre coupling matrix file: %s\n",
              ERROR_PREFIX, pIpar->FIBRECOUPLING_FILENAME);
      return 1;
    }
  }


  
  pCondList->num_airmass   = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.AIRMASS");
  pCondList->num_IQ        = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.IQ");
  pCondList->num_skybright = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.SKYBRIGHT");
  pCondList->num_tilt      = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.TILT");
  pCondList->num_misalign  = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.MISALIGNMENT");


  pCondList->num_exp       = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.TEXP");
  //check that number of nsub is same length as texp
  nsub = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.NSUB");
  if ( nsub !=  pCondList->num_exp )
  {
    fprintf(stderr, "%s Mismatched dimensions for OBS_PARAMS.TEXP(%d) and OBS_PARAMS.NSUB(%d) parameters\n",
            ERROR_PREFIX, pCondList->num_exp, nsub);
    return 1;
  }

  if ( pCondList->num_airmass <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of airmass values to sample: %d\n", ERROR_PREFIX, pCondList->num_airmass);
    return 1;
  }
  if ( pCondList->num_IQ <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of IQ values to sample: %d\n", ERROR_PREFIX, pCondList->num_IQ);
    return 1;
  }
  if ( pCondList->num_skybright <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of sky brightness values to sample: %d\n", ERROR_PREFIX, pCondList->num_skybright);
    return 1;
  }
  if ( pCondList->num_tilt <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of spine tilt values to sample: %d\n", ERROR_PREFIX, pCondList->num_tilt);
    return 1;
  }
  if ( pCondList->num_misalign <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of spine misalignment values to sample: %d\n", ERROR_PREFIX, pCondList->num_misalign);
    return 1;
  }
  if ( pCondList->num_exp <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of exposure times/sub exposures to sample: %d\n", ERROR_PREFIX, pCondList->num_exp);
    return 1;
  }

  
 //make some space 
  if ((pCondList->pairmass   = (double*) malloc(pCondList->num_airmass   * sizeof(double))) == NULL ||
      (pCondList->pIQ        = (double*) malloc(pCondList->num_IQ        * sizeof(double))) == NULL ||
      (pCondList->pskybright = (double*) malloc(pCondList->num_skybright * sizeof(double))) == NULL ||
      (pCondList->ptilt      = (double*) malloc(pCondList->num_tilt      * sizeof(double))) == NULL ||
      (pCondList->pmisalign  = (double*) malloc(pCondList->num_misalign  * sizeof(double))) == NULL ||
      (pCondList->ptexp      = (double*) malloc(pCondList->num_exp       * sizeof(double))) == NULL ||
      (pCondList->pnsub      = (int*)    malloc(pCondList->num_exp       * sizeof(int)))    == NULL )
  {
    fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //copy the arrays
  //  for(i=0;i<pCondList->num_airmass;  i++) pCondList->pairmass[i]   = (double) pIpar->pOBS_PARAMS_AIRMASS[i];
  //  for(i=0;i<pCondList->num_IQ;       i++) pCondList->pIQ[i]        = (double) pIpar->pOBS_PARAMS_IQ[i];
  //  for(i=0;i<pCondList->num_skybright;i++) pCondList->pskybright[i] = (double) pIpar->pOBS_PARAMS_SKYBRIGHT[i];
  //  for(i=0;i<pCondList->num_tilt;     i++) pCondList->ptilt[i]      = (double) pIpar->pOBS_PARAMS_TILT[i];
  //  for(i=0;i<pCondList->num_misalign; i++) pCondList->pmisalign[i]  = (double) pIpar->pOBS_PARAMS_MISALIGN[i];
  //  for(i=0;i<pCondList->num_exp;      i++)
  //  {
  //    pCondList->ptexp[i] = (double) pIpar->pOBS_PARAMS_TEXP[i];
  //    pCondList->pnsub[i] = (int)    pIpar->pOBS_PARAMS_NSUB[i];
  //  }

////  //and make sure that the values are sorted
//  util_dsort  ((unsigned long) pCondList->num_airmass,   (double*) pCondList->pairmass);
//  util_dsort  ((unsigned long) pCondList->num_IQ,        (double*) pCondList->pIQ);
//  util_dsort  ((unsigned long) pCondList->num_skybright, (double*) pCondList->pskybright);
//  util_dsort  ((unsigned long) pCondList->num_tilt,      (double*) pCondList->ptilt);
//  util_dsort  ((unsigned long) pCondList->num_misalign,  (double*) pCondList->pmisalign);
//  util_disort ((unsigned long) pCondList->num_texp,      (double*) pCondList->ptexp, (int*) pCondList->pnsub);

  // No, better to avoid sorting (allow the user to specifiy their own order)
  // What we actually want is the min and max of each parameter
  for(i=0;i<pCondList->num_airmass;  i++)
  {
    pCondList->pairmass[i]   = (double) pIpar->pOBS_PARAMS_AIRMASS[i];
    if ( pCondList->pairmass[i] > pCondList->airmass_max )  pCondList->airmass_max = pCondList->pairmass[i];
    if ( pCondList->pairmass[i] < pCondList->airmass_min )  pCondList->airmass_min = pCondList->pairmass[i];
  }
  for(i=0;i<pCondList->num_IQ;  i++)
  {
    pCondList->pIQ[i]   = (double) pIpar->pOBS_PARAMS_IQ[i];
    if ( pCondList->pIQ[i] > pCondList->IQ_max )  pCondList->IQ_max = pCondList->pIQ[i];
    if ( pCondList->pIQ[i] < pCondList->IQ_min )  pCondList->IQ_min = pCondList->pIQ[i];
  }
  for(i=0;i<pCondList->num_skybright;  i++)
  {
    pCondList->pskybright[i]   = (double) pIpar->pOBS_PARAMS_SKYBRIGHT[i];
    if ( pCondList->pskybright[i] > pCondList->skybright_max )  pCondList->skybright_max = pCondList->pskybright[i];
    if ( pCondList->pskybright[i] < pCondList->skybright_min )  pCondList->skybright_min = pCondList->pskybright[i];
  }
  for(i=0;i<pCondList->num_tilt;  i++)
  {
    pCondList->ptilt[i]   = (double) pIpar->pOBS_PARAMS_TILT[i];
    if ( pCondList->ptilt[i] > pCondList->tilt_max )  pCondList->tilt_max = pCondList->ptilt[i];
    if ( pCondList->ptilt[i] < pCondList->tilt_min )  pCondList->tilt_min = pCondList->ptilt[i];
  }
  for(i=0;i<pCondList->num_misalign;  i++)
  {
    pCondList->pmisalign[i]   = (double) pIpar->pOBS_PARAMS_MISALIGN[i];
    if ( pCondList->pmisalign[i] > pCondList->misalign_max )  pCondList->misalign_max = pCondList->pmisalign[i];
    if ( pCondList->pmisalign[i] < pCondList->misalign_min )  pCondList->misalign_min = pCondList->pmisalign[i];
  }
  for(i=0;i<pCondList->num_exp;  i++)
  {
    pCondList->ptexp[i]   = (double) pIpar->pOBS_PARAMS_TEXP[i];
    pCondList->pnsub[i]   = (int)    pIpar->pOBS_PARAMS_NSUB[i];
    if ( pCondList->ptexp[i] > pCondList->texp_max )  pCondList->texp_max = pCondList->ptexp[i];
    if ( pCondList->ptexp[i] < pCondList->texp_min )  pCondList->texp_min = pCondList->ptexp[i];
    if ( pCondList->pnsub[i] > pCondList->nsub_max )  pCondList->nsub_max = pCondList->pnsub[i];
    if ( pCondList->pnsub[i] < pCondList->nsub_min )  pCondList->nsub_min = pCondList->pnsub[i];
  }


  
  return 0;
}




int ETC_input_params_interpret_sim (ETC_input_params_struct *pIpar,
                                   sim_struct    *pSim )
{
  if ( pIpar   == NULL ||
       pSim     == NULL ) return 1;
  //

  pSim->clobber = (BOOL) pIpar->SIM_CLOBBER;
  pSim->debug = (BOOL) pIpar->debug;
  pSim->random_seed = (BOOL) pIpar->SIM_RANDOM_SEED;
  
  strncpy(pSim->str_code_name, pIpar->SIM_CODE_NAME, STR_MAX);

  fprintf(stdout, "%s Interpreting simulation control parameters for sim run: \"%s\"\n",
          COMMENT_PREFIX,pSim->str_code_name); fflush(stdout);
  strncpy(pSim->str_mode, pIpar->SIM_MODE, STR_MAX);

  //------------------determine the simulation mode------------------//
  {
    const char *plist_names[]  = {"CALC_SNR",  "SNR", "CALC_SPEC", "SPEC", 
                                  "CALC_TEXP", "TEXP",
                                  "CALC_MAG",  "MAG"};
    const int   plist_values[] = {SIM_MODE_CODE_CALC_SNR,  SIM_MODE_CODE_CALC_SNR, SIM_MODE_CODE_CALC_SNR,  SIM_MODE_CODE_CALC_SNR,
                                  SIM_MODE_CODE_CALC_TEXP, SIM_MODE_CODE_CALC_TEXP,
                                  SIM_MODE_CODE_CALC_MAG, SIM_MODE_CODE_CALC_MAG};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pSim->str_mode,  &(pSim->mode)))
    {
      fprintf(stderr, "%s I do not understand this SIM.MODE: >%s<\n",
                ERROR_PREFIX, pSim->str_mode);fflush(stderr);
      return 1;
    }
  }
  //------------------determine the simulation mode------------------//

  //------------------Determine the output products--------------------//
  strncpy(pSim->str_output, pIpar->SIM_OUTPUT, STR_MAX);
  //tokenise the list of output options, and then test each token against the list
  {
    const char *plist_names[]  = {"SUMMARY",         "SUMMARY_ASCII",
                                  "SUMMARY_FITS",
                                  "SPECTRA_SNR",     "SNR", 
                                  "SPECTRA_FLUX",    "FLUX",
                                  "SPECTRA_FLUENCE", "FLUENCE",
                                  "SPECTRA_NOISE",   "NOISE",
                                  "SPECTRA_SKY",     "SKY",
                                  "SPECTRA_TOTAL",   "TOTAL",
                                  "SPECTRA_POISSON", "POISSON",    "SPECTRA_REALISATION", "REALISATION",
                                  "SPECTRA",         "SPECTRA_ALL",
                                  "ARF",
                                  "RMF"};
    const int plist_values[] = {SIM_OUTPUT_CODE_SUMMARY_ASCII,   SIM_OUTPUT_CODE_SUMMARY_ASCII,
                                SIM_OUTPUT_CODE_SUMMARY_FITS,
                                SIM_OUTPUT_CODE_SPECTRA_SNR,     SIM_OUTPUT_CODE_SPECTRA_SNR,     
                                SIM_OUTPUT_CODE_SPECTRA_FLUX,    SIM_OUTPUT_CODE_SPECTRA_FLUX,     
                                SIM_OUTPUT_CODE_SPECTRA_FLUENCE, SIM_OUTPUT_CODE_SPECTRA_FLUENCE, 
                                SIM_OUTPUT_CODE_SPECTRA_NOISE,   SIM_OUTPUT_CODE_SPECTRA_NOISE,   
                                SIM_OUTPUT_CODE_SPECTRA_SKY,     SIM_OUTPUT_CODE_SPECTRA_SKY,     
                                SIM_OUTPUT_CODE_SPECTRA_TOTAL,   SIM_OUTPUT_CODE_SPECTRA_TOTAL,     
                                SIM_OUTPUT_CODE_SPECTRA_POISSON, SIM_OUTPUT_CODE_SPECTRA_POISSON, SIM_OUTPUT_CODE_SPECTRA_POISSON, SIM_OUTPUT_CODE_SPECTRA_POISSON, 
                                SIM_OUTPUT_CODE_SPECTRA,         SIM_OUTPUT_CODE_SPECTRA,
                                SIM_OUTPUT_CODE_ARF,
                                SIM_OUTPUT_CODE_RMF};
    int len = (int) (sizeof(plist_values)/sizeof(int));\
    
    char str_buffer[STR_MAX];
    char *pstr1 = NULL;
    char *pstr2 = NULL;
    //        int counter = 0;
    pSim->output = (int) SIM_OUTPUT_CODE_NONE;

    strncpy(str_buffer, pSim->str_output, STR_MAX);
    pstr1 = (char*) str_buffer;
    
    //        fprintf(stdout, "%s %2d \"%s\"\n", DEBUG_PREFIX, counter, pstr1); fflush(stdout);
    while ((pstr2 = (char*) strtok(pstr1, " ,|;")) != NULL)
    {
      int this_output = 0;
    	pstr1 = NULL;
      if (util_select_option_from_string(len, plist_names, plist_values,
                                         pstr2,  (int*) &(this_output)))
      {
        fprintf(stderr, "%s I do not understand this SIM.OUTPUT: >%s<\n",
                ERROR_PREFIX, pstr2);fflush(stderr);
        return 1;
      }
      pSim->output = (int) pSim->output | (int) this_output ;
      //      counter ++;
      //      fprintf(stdout, "%s %2d \"%s\" = 0x%08x => 0x%08x\n", DEBUG_PREFIX, counter, pstr2, this_output, pSim->output); fflush(stdout);
    }   

  }
  //------------------Determine the output products--------------------//


  //-------------------Determine the spectral formats to output-------------//
  strncpy(pSim->str_specformat, pIpar->SIM_SPECFORMAT, STR_MAX);
  //tokenise the list of specformat options, and then test each token against the list
  {
    const char *plist_names[]  = {"IMAGE",
                                  "TABLE",
                                  "NATIVE",
                                  "RESAMPLED" };
    const int plist_values[] = {SIM_SPECFORMAT_CODE_IMAGE,
                                SIM_SPECFORMAT_CODE_TABLE,
                                SIM_SPECFORMAT_CODE_NATIVE,
                                SIM_SPECFORMAT_CODE_RESAMPLED};
    int len = (int) (sizeof(plist_values)/sizeof(int));\
    
    char str_buffer[STR_MAX];
    char *pstr1 = NULL;
    char *pstr2 = NULL;
    //    int counter = 0;
    pSim->specformat = (int) SIM_SPECFORMAT_CODE_NONE;

    strncpy(str_buffer, pSim->str_specformat, STR_MAX);
    pstr1 = (char*) str_buffer;
    
    //    fprintf(stdout, "%s %2d \"%s\"\n", DEBUG_PREFIX, counter, pstr1); fflush(stdout);
    while ((pstr2 = (char*) strtok(pstr1, " ,|;")) != NULL)
    {
      int this_specformat;
    	pstr1 = NULL;
      if (util_select_option_from_string(len, plist_names, plist_values,
                                         pstr2,  &(this_specformat)))
      {
        fprintf(stderr, "%s I do not understand this SIM.SPECFORMAT: >%s<\n",
                ERROR_PREFIX, pstr2);fflush(stderr);
        return 1;
      }
      pSim->specformat = (int) pSim->specformat | (int) this_specformat ;
      //      counter ++;
      //      fprintf(stdout, "%s %2d \"%s\" = 0x%08x => 0x%08x\n", DEBUG_PREFIX, counter, pstr2, this_specformat, pSim->specformat); fflush(stdout);
    }   
  }
  //-------------------Determine the spectral formats to output-------------//



//  //------------------determine the resampling mode and params------------------//
//  strncpy(pSim->Resample.str_mode, pIpar->SIM_RESAMPLE_MODE, STR_MAX);
//  {
//    const char *plist_names[]  = {"LINEAR_LAMBDA", "LINEAR",  "LAMBDA",
//                                  "LOG_LAMBDA",    "LOG",     
//                                  "LINEAR_FREQ",   "FREQ",
//                                  "LOG_FREQ"};
//    const int   plist_values[] = {RESAMPLE_MODE_LINEAR_LAMBDA, RESAMPLE_MODE_LINEAR_LAMBDA, RESAMPLE_MODE_LINEAR_LAMBDA,
//                                  RESAMPLE_MODE_LOG_LAMBDA,    RESAMPLE_MODE_LOG_LAMBDA,
//                                  RESAMPLE_MODE_LINEAR_FREQ,   RESAMPLE_MODE_LINEAR_FREQ,
//                                  RESAMPLE_MODE_LOG_FREQ};
//    int len = (int) (sizeof(plist_values)/sizeof(int));
//    if (util_select_option_from_string(len, plist_names, plist_values,
//                                       pSim->Resample.str_mode,  &(pSim->Resample.mode)))
//    {
//      fprintf(stderr, "%s I do not understand this SIM.RESAMPLE_MODE: >%s<\n",
//                ERROR_PREFIX, pSim->Resample.str_mode);fflush(stderr);
//      return 1;
//    }
//  }
//  pSim->Resample.min     = (double) pIpar->SIM_RESAMPLE_MIN;
//  pSim->Resample.delta   = (double) pIpar->SIM_RESAMPLE_DELTA;
//  pSim->Resample.num_pix = (double) pIpar->SIM_RESAMPLE_NUM_PIX;
//
//  //------------------determine the resampling mode------------------//


  
  //------------------determine the normalising filter mode------------------//
  strncpy(pSim->str_norm_filter_magsys, pIpar->SIM_NORM_FILTER_MAGSYS, STR_MAX);
  {
    const char *plist_names[]  = {"AB",  
                                  "VEGA"};
    const int   plist_values[] = {MAGSYS_AB, 
                                  MAGSYS_VEGA};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pSim->str_norm_filter_magsys,  &(pSim->norm_filter_magsys)))
    {
      fprintf(stderr, "%s I do not understand this SIM.NORM_FILTER.MAGSYS: >%s<\n",
                ERROR_PREFIX, pSim->str_norm_filter_magsys);fflush(stderr);
      return 1;
    }
  }
  //------------------determine the normalising filter mode------------------//

  
  //------------------read the normalising filters into curve structs------------------//
  pSim->num_filters = (int) pIpar->SIM_NUM_FILTERS;
  {
    int f;
    for (f=0; f<pSim->num_filters; f++)
    {
      strncpy(pSim->crvNormFilterList[f].str_name, pIpar->SIM_NORM_FILTER_NAME[f], STR_MAX);
     
      if ( strlen(pIpar->SIM_NORM_FILTER_FILENAME[f]) > 0 &&
           strcmp(pIpar->SIM_NORM_FILTER_FILENAME[f], "NONE") != 0 )
      {
        strncpy(pSim->crvNormFilterList[f].str_filename, pIpar->SIM_NORM_FILTER_FILENAME[f], STR_MAX);
        //attempt to read in the filter from file
        if ( curve_struct_read_from_file ( (curve_struct*) &(pSim->crvNormFilterList[f]), (BOOL) pSim->debug))
        {
          fprintf(stderr, "%s I had problems reading the normalising filter bandpass from: %s\n",
                  ERROR_PREFIX, pIpar->SIM_NORM_FILTER_FILENAME[f]);
          return 1;
        }
      }
    }
  }

  //this is the output directory
  strncpy(pSim->str_outdir, pIpar->SIM_OUTDIR, STR_MAX);
  

  // make sure that the output directory exists
  if ( strlen(pSim->str_outdir) )
  {
    if ( util_make_directory ((const char*) pSim->str_outdir))
    {
      fprintf(stderr, "%s Problem finding or creating output directory: \"%s\"\n",
              ERROR_PREFIX, pSim->str_outdir);
      return 1;
    }
  }
  else
  {
    strncpy(pSim->str_outdir, ".", STR_MAX);
  }


  //prepare the output summary file early so that can exit on any errors before doing main calculations  
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SUMMARY_ASCII )
  {
    snprintf(pSim->str_summary_filename,STR_MAX, "%s/%s", pSim->str_outdir, "4FS_ETC_summary.txt");
    if ((pSim->pSummaryFile = (FILE*) fopen(pSim->str_summary_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s  I had problems opening the file: %s\n",
              ERROR_PREFIX, pSim->str_summary_filename);fflush(stderr);
      return 1;
    }
  }




  return 0;
}







int    ETC_input_params_interpret_everything (ETC_input_params_struct *pIpar,
                                             spectrograph_struct    *pSpec,
                                             sky_struct             *pSky,
                                             templatelist_struct    *pTL,
                                             condlist_struct        *pCondList,
                                             sim_struct             *pSim,
                                             rulelist_struct        *pRuleList,
                                             rulesetlist_struct     *pRuleSetList)
{
  if ( pIpar        == NULL ||
       pSpec        == NULL ||
       pSky         == NULL ||
       pTL          == NULL ||
       pCondList    == NULL ||
       pSim         == NULL ||
       pRuleList    == NULL ||
       pRuleSetList == NULL ) return 1;
  //

  if ( ETC_input_params_interpret_spectrograph ((ETC_input_params_struct*) pIpar,
                                                (spectrograph_struct*)    pSpec,
                                                (BOOL) pIpar->debug )) return 1; 

  
  
  if ( ETC_input_params_interpret_sky          ((ETC_input_params_struct*) pIpar,
                                               (sky_struct*)             pSky)) return 1; 


  if ( ETC_input_params_interpret_templatelist ((ETC_input_params_struct*) pIpar,
                                               (templatelist_struct*)    pTL)) return 1; 

  
  if ( ETC_input_params_interpret_conditions   ((ETC_input_params_struct*) pIpar,
                                               (condlist_struct*)        pCondList,
                                                (BOOL) pIpar->debug )) return 1; 

  if ( ETC_input_params_interpret_sim          ((ETC_input_params_struct*) pIpar,
                                               (sim_struct*)             pSim)) return 1;
  
  if ( ETC_input_params_interpret_rules        ((ETC_input_params_struct*) pIpar,
                                               (rulelist_struct*)        pRuleList,
                                               (rulesetlist_struct*)     pRuleSetList)) return 1; 


  
  //check that when running in CALC_TEXP or CALC_MAG mode we only have one supplied TEXP param
  // and that NSUB = 1 for this TEXP
  if ( pSim->mode == SIM_MODE_CODE_CALC_TEXP)
  {
    if ( pCondList->num_exp != 1 )
    {
      fprintf(stderr, "%s When SIM.MODE=CALC_TEXP I expect OBS_PARAMS.TEXP to contain exactly one exposure time\n",
              ERROR_PREFIX);
      return 1;
    }
    if ( pCondList->pnsub[0] != 1 )
    {
      fprintf(stderr, "%s When SIM.MODE=CALC_TEXP then OBS_PARAMS.NSUB[0] must equal 1 - I'll set it to 1 for you\n",
              WARNING_PREFIX);
      pCondList->pnsub[0] = 1;
    }
  }
  
  return 0;
}
