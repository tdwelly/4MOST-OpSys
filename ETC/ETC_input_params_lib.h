//----------------------------------------------------------------------------------
//--
//--    Filename: ETC_input_params_lib.h
//--    Use: This is a header for the ETC_input_params_lib.c C code file
//--
//--    Notes:
//--

#ifndef ETC_INPUT_PARAMS_H
#define ETC_INPUT_PARAMS_H

//#define CVS_REVISION_ETC_INPUT_PARAMS_LIB_H "$Revision: 1.7 $"
//#define CVS_DATE_ETC_INPUT_PARAMS_LIB_H     "$Date: 2015/10/26 15:26:42 $"


#include <stdio.h>

#include "define.h"
#include "4FS_ETC.h"
#include "params_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.7 $"
//#define CVS_DATE_H     "$Date: 2015/10/26 15:26:42 $"
inline static int ETC_input_params_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

//----------------------------------
//----------------------------------


//the input parameters list handling  has got too long, so separate it to this
//new separate module

typedef struct {
  param_list_struct  *pParamList; //pointer to the params_list struct
  
  ////////////////////////////////////////////////////////////////////////////////////
  ///////////////Start by listing all the possible input parameters //////////////////
  ///////////////that are defined in the FSIFD document                 //////////////
  char PARAM_FILENAME[STR_MAX];  //the name of the parameter file
  //general simulation params

  char   SIM_CODE_NAME[STR_MAX];       //name of the run

  //the following controls which output products to compute+write out
  char   SIM_OUTPUT[STR_MAX];       //

  //the following controls the format of the output products
  char   SIM_SPECFORMAT[STR_MAX];       //

  //the following controls whether we calculate SNR from a given exptime or exptime from a given SNR
  char   SIM_MODE[STR_MAX];       //
  
  //the following controls where we write the output files
  char   SIM_OUTDIR[STR_MAX];       //

  //the file listing the object templates
  char   TEMPLATES_FILENAME[STR_MAX];       //

  //the file listing the rules
  char   RULELIST_FILENAME[STR_MAX];       //

  //the file listing the rulesets
  char   RULESETLIST_FILENAME[STR_MAX];       //

  char   FIBRECOUPLING_TYPE[STR_MAX];       // 
  char   FIBRECOUPLING_FILENAME[STR_MAX];       //
  //  double FIBRECOUPLING_FIBER_DIAM; //arcsec
  double FIBRECOUPLING_FRAC_SCIENCE; //transmitted fraction
  char   FIBRECOUPLING_SEEING_PROFILE[STR_MAX];       // 
  // .... TODO rest of options
  double FIBRECOUPLING_FRAC_SKY; //efficiency of coupling of sky to fiber (relative to treating fiber as circular hole)

  char   SIM_RESAMPLE_MODE[STR_MAX];       // 
  double SIM_RESAMPLE_MIN;                 //
  double SIM_RESAMPLE_DELTA;               //
  long   SIM_RESAMPLE_NUM_PIX;              //

  int    SIM_NUM_FILTERS;                      // Number of filters to consider 
  char   SIM_NORM_FILTER_NAME[SIM_MAX_FILTERS][STR_MAX];        // Name of filter (g,r,B,....) 
  char   SIM_NORM_FILTER_FILENAME[SIM_MAX_FILTERS][STR_MAX];    // Filename of file containing bandpass info: expect two columns: lambda,throughput
  char   SIM_NORM_FILTER_MAGSYS[STR_MAX];                     // Magnitude system (AB, VEGA etc) 

  double SPECTRO_FIBRE_DIAM; //arcsec
  double SPECTRO_EFFECTIVE_AREA; //m^2
  double SPECTRO_SKYSUB_RESIDUAL; //fraction of sky which is not subtracted 
  //
  int    SPECTRO_NUM_ARMS;                                      //number of spectrograph arms

  //each arm is described by a set of the following params
  char   SPECTRO_ARM_CODENAME[MAX_SPECTRO_ARMS][STR_MAX];       //human readable identifier
  char   SPECTRO_ARM_RES_FILENAME[MAX_SPECTRO_ARMS][STR_MAX];
  char   SPECTRO_ARM_TPUT_FILENAME[MAX_SPECTRO_ARMS][STR_MAX];

  double SPECTRO_ARM_APER_SIZE[MAX_SPECTRO_ARMS];    // in pix
  //  double SPECTRO_ARM_SPATIAL_FWHM[MAX_SPECTRO_ARMS]; // in um
  double SPECTRO_ARM_APER_EEF[MAX_SPECTRO_ARMS]; // fractional
  double SPECTRO_ARM_PEAK_PIX_FRAC[MAX_SPECTRO_ARMS]; // fractional
  double SPECTRO_ARM_PIXEL_SIZE[MAX_SPECTRO_ARMS];   // in um
  double SPECTRO_ARM_READ_NOISE[MAX_SPECTRO_ARMS];   // in e-/pix/read 
  double SPECTRO_ARM_DARK_CURRENT[MAX_SPECTRO_ARMS]; // in e-/hr/pix
  //  double SPECTRO_ARM_GAIN[MAX_SPECTRO_ARMS];         // in e-/ADU
  double SPECTRO_ARM_FULL_WELL[MAX_SPECTRO_ARMS];    // in e-

  int    SPECTRO_ARM_BINNING_DISP[MAX_SPECTRO_ARMS];
  int    SPECTRO_ARM_BINNING_CROSS[MAX_SPECTRO_ARMS];

  char   SPECTRO_ARM_LAMBDA_TYPE[MAX_SPECTRO_ARMS][STR_MAX];  //
  char   SPECTRO_ARM_LAMBDA_FILENAME[MAX_SPECTRO_ARMS][STR_MAX];  //
  int    SPECTRO_ARM_LAMBDA_NUMPIX[MAX_SPECTRO_ARMS];
  double SPECTRO_ARM_LAMBDA_REFPIX[MAX_SPECTRO_ARMS];
  double SPECTRO_ARM_LAMBDA_REFVAL[MAX_SPECTRO_ARMS];
  double SPECTRO_ARM_LAMBDA_DISP[MAX_SPECTRO_ARMS];


  char   SKY_TRANSMISSION_FILENAME[STR_MAX];       //
  char   SKY_EMISSION_FILENAME[STR_MAX];       //

  char   OBS_PARAMS_INTERP_METHOD[STR_MAX];       //
  char   OBS_PARAMS_SKYBRIGHT_TYPE[STR_MAX];       //

  double pOBS_PARAMS_AIRMASS[MAX_OBS_PARAMS];
  double pOBS_PARAMS_IQ[MAX_OBS_PARAMS];
  double pOBS_PARAMS_SKYBRIGHT[MAX_OBS_PARAMS];
  double pOBS_PARAMS_TILT[MAX_OBS_PARAMS];
  double pOBS_PARAMS_MISALIGN[MAX_OBS_PARAMS];
  double pOBS_PARAMS_TEXP[MAX_OBS_PARAMS];
  int    pOBS_PARAMS_NSUB[MAX_OBS_PARAMS];


  //the random number generator seed value, as supplied on the command line
  long SIM_RANDOM_SEED;
  BOOL SIM_CLOBBER;

  BOOL debug;
  ///////////////End of the list of all the possible input parameters //////////////
  ////////////////////////////////////////////////////////////////////////////////////
}  ETC_input_params_struct;



//----------------------------------------------------------------------------------------------
//-- public function declarations



int    ETC_input_params_print_version              (FILE* pFile);
int    ETC_input_params_print_usage_information    (FILE *pFile);

int    ETC_input_params_struct_init                (ETC_input_params_struct **ppIpar);
int    ETC_input_params_struct_free                (ETC_input_params_struct **ppIpar);
int    ETC_input_params_handler                    (ETC_input_params_struct **ppIpar, param_list_struct **ppParamList, int argc, char** argv);
int    ETC_input_params_setup_ParamList            (ETC_input_params_struct *pIpar, param_list_struct* pParamList,  int argc, char** argv); 


int    ETC_input_params_interpret_everything (ETC_input_params_struct *pIpar,
                                             spectrograph_struct    *pSpec,
                                             sky_struct             *pSky,
                                             templatelist_struct    *pTL,
                                             condlist_struct        *pCondList,
                                             sim_struct             *pSim,
                                             rulelist_struct        *pRuleList,
                                             rulesetlist_struct     *pRuleSetList);

int    ETC_input_params_interpret_spectrograph (ETC_input_params_struct *pIpar,
                                                spectrograph_struct    *pSpec,
                                                BOOL verbose);
int    ETC_input_params_interpret_templatelist (ETC_input_params_struct *pIpar,
                                               templatelist_struct    *pTL);
int    ETC_input_params_interpret_sky          (ETC_input_params_struct *pIpar,
                                               sky_struct             *pSky);
int    ETC_input_params_interpret_conditions   (ETC_input_params_struct *pIpar,
                                                condlist_struct        *pCondList,
                                                BOOL verbose);
int    ETC_input_params_interpret_sim          (ETC_input_params_struct *pIpar,
                                               sim_struct             *pSim);
int    ETC_input_params_interpret_rules        (ETC_input_params_struct *pIpar,
                                               rulelist_struct        *pRuleList,
                                               rulesetlist_struct     *pRuleSetList);

#endif
