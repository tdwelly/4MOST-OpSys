#!/bin/tcsh -f


# Transfer the exposure times from the summary file output of the 4FS_ETC
# to a catalogue
set RULESET  = "RULESET"
set OUTFILE  = ""

if ( $#argv == 2 ) then
  set CATALOGUE   = "$argv[1]"
  set ETC_RESULTS = "$argv[2]"
else if ( $#argv == 3 ) then
  set CATALOGUE   = "$argv[1]"
  set ETC_RESULTS = "$argv[2]"
  set OUTFILE     = "$argv[3]"
else
  echo "Error, wrong number of input parameters"
  echo "Correct usage: $0 CATALOGUE ETC_RESULTS [OUTFILE]"
  exit 1
endif

if ( ! -e "$CATALOGUE" ) then
  echo "Error, could not find catalogue file: $CATALOGUE" 
  exit 1
endif

if ( ! -e "$ETC_RESULTS" ) then
  echo "Error, could not find ETC results file: $ETC_RESULTS" 
  exit 1
endif

if ( "$OUTFILE" == "" ) then
  set OUTFILE = "${CATALOGUE:r}_with_exptimes.fits"
endif


set TEMPDIR = /tmp/${USER}/4FS_ETC_associate_exptimes_with_catalogue
mkdir -p ${TEMPDIR}
if ( ! -d ${TEMPDIR} ) then
  echo "Error: Cannot create temporary directory: $TEMPDIR"
  exit 1
endif



echo "Associating ${CATALOGUE} + ${ETC_RESULTS} -> $OUTFILE"
if ( -e $OUTFILE ) mv $OUTFILE ${OUTFILE}~ 

# make this independent of gump -> use ftlist instead
#set TRIM_CHARS = `gump ${ETC_RESULTS} TEMPLATE no | head -n 1 | gawk '//{n=split($1,s,"/");l=length($1)-length(s[n]);print l}'`
set TRIM_CHARS = `ftlist "${ETC_RESULTS}[1][col TEMPLATE][#row==1]" T rownum=no colheader=no mode=q | gawk '//{n=split($1,s,"/");l=length($1)-length(s[n]);print l}'`
set FMT = `ftlist $CATALOGUE C mode=q | gawk '$2=="TEMPLATE" {print $3;exit}'`


# Process the ETC output file:
set SKY_LIST     = ( D     G     B     )
set SKY_MAG_LIST = ( 21.77 20.77 18.77 )

set TEMP_ALL = ${TEMPDIR}/temp_all_$$.fits
if ( -e ${TEMP_ALL} ) rm ${TEMP_ALL}
set S = 1
while ( $S <= $#SKY_LIST )
  set TEMPFILE = ${TEMPDIR}/temp_${SKY_LIST[$S]}_$$.fits
  ftcopy "${ETC_RESULTS}[1][abs(SKYBRIGHT-${SKY_MAG_LIST[$S]})<0.02]" ${TEMPFILE} clobber=yes mode=q
  ftcalc ${TEMPFILE}+1 ${TEMPFILE} TEXP_${SKY_LIST[$S]} "TEXP/60.0" tform=1E clobber=yes mode=q
  renamecol ${TEMPFILE}+1 TEXP_${SKY_LIST[$S]} TEXP_${SKY_LIST[$S]} min
  if ( ! -e ${TEMP_ALL} ) then
    cp ${TEMPFILE} ${TEMP_ALL}
  else
    faddcol ${TEMP_ALL}+1 ${TEMPFILE}+1 colname=TEXP_${SKY_LIST[$S]} mode=q
  endif
  rm ${TEMPFILE}
  @ S ++
end  
fdelcol ${TEMP_ALL}+1 TEXP      N Y mode=q
fdelcol ${TEMP_ALL}+1 SKYBRIGHT N Y mode=q


 
echo "Adding exposure times using stilts ..."
echo "##------------------stilts chatter---------------------------------##"
stilts -memory tmatch2 matcher=exact+exact+1d params=0.51 fixcols=all ifmt1=fits in1="${CATALOGUE}" ifmt2=fits in2="${TEMP_ALL}" values1="TEMPLATE RULESET R_MAG" values2="TEMPLATE_TEMP RULESET_TEMP MAG"  icmd2="addcol -utype ${FMT} TEMPLATE_TEMP substring(TEMPLATE,$TRIM_CHARS);colmeta -name RULESET_TEMP RULESET; keepcols 'TEMPLATE_TEMP RULESET_TEMP MAG TEXP_D TEXP_G TEXP_B';" out="${OUTFILE}" ofmt=fits find=best1 join=all1 omode=out suffix1="" suffix2="" ocmd="delcols 'TEMPLATE_TEMP RULESET_TEMP MAG GroupID GroupSize'" progress=profile
echo "##------------------stilts chatter---------------------------------##"


rm  ${TEMP_ALL}


exit







cp ${CATALOGUE} $OUTFILE

set SKY_LIST     = ( D     G     B     )
set SKY_MAG_LIST = ( 21.77 20.77 18.77 )

set S = 1
while ( $S <= $#SKY_LIST ) 
  echo "Adding exposure times for moon phase: ${SKY_LIST[$S]}"
  echo "##------------------stilts chatter---------------------------------##"
  stilts -memory tmatch2 matcher=exact+exact+1d params=0.51 fixcols=all ifmt1=fits in1="${OUTFILE}" ifmt2=fits in2="${ETC_RESULTS}" values1="TEMPLATE RULESET R_MAG" values2="TEMPLATE_TEMP RULESET_TEMP MAG"  icmd2="addcol -utype ${FMT} TEMPLATE_TEMP substring(TEMPLATE,$TRIM_CHARS);colmeta -name RULESET_TEMP RULESET; select (abs(SKYBRIGHT-${SKY_MAG_LIST[$S]})<0.02);keepcols 'TEMPLATE_TEMP RULESET_TEMP MAG SKYBRIGHT TEXP'; addcol -utype E -units min TEXP_${SKY_LIST[$S]} ((float)(TEXP/60.0));" out="${OUTFILE}" ofmt=fits find=best1 join=all1 omode=out suffix1="" suffix2="" ocmd="delcols 'TEMPLATE_TEMP RULESET_TEMP MAG SKYBRIGHT TEXP GroupID GroupSize'" 
  echo "##------------------stilts chatter---------------------------------##"
  
  @ S ++
end


echo "... Done!"




exit

# #testing:
# ### This script works, but need to pre-filter the inputs so that template names match exactly etc.
# ftcopy "info_2016-01-24/haloHR_06_mock.fits[1][#row<=100000][col *;RULESET='GalHaloHR']" temp1.fits clobber=yes
# ftcopy "/home/tdwelly/4most/sim/round9/a/demo/outdir_DRS2/4FS_ETC_summary.fits[1][col *,TEMPLATE=strmid(TEMPLATE,25,36)]" temp_etc.fits clobber=yes
# ~/gitwork/4MOST-OpSys/ETC/4FS_ETC_associate_exptimes_with_catalogue.csh temp1.fits temp_etc.fits temp2.fits
