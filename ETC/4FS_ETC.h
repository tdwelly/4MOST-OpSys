//----------------------------------------------------------------------------------
//--
//--    Filename: 4FS_ETC.h
//--    Use: This is a header for the 4FS_ETC.c C code file
//--
//--    Notes:
//--


#ifndef FOURFS_ETC_H
#define FOURFS_ETC_H

//standard header files - need this for FILE* pointers
#include <stdio.h>
#include <fitsio.h>


//my header files which include structure definitions come next
#include "define.h"
#include "utensils_lib.h"
#include "params_lib.h"
#include "fits_helper_lib.h"
#include "curve_lib.h"
#include "ETC_sky_lib.h"
#include "ETC_rules_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.14 $"
//#define CVS_DATE_H     "$Date: 2015/11/24 09:02:29 $"
inline static int FourFS_ETC_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//


////////////////////////////////////
#define STR_MAX        DEF_STRLEN
#define LONG_STR_MAX   DEF_LONGSTRLEN
#define LSTR_MAX       DEF_LONGSTRLEN

#define ETC_BRANDING_STRING      "4MOST Facility Simulator - ETC"
#define ETC_BRANDING_PLOTSTRING_GRAPH  \
  "set label 9999 \"4MOST Facility Simulator - ETC\" at graph  0.98,0.02 font \"Times-Roman,14\" tc lt 3 right" 
#define ETC_BRANDING_PLOTSTRING_SCREEN \
  "set label 9999 \"4MOST Facility Simulator - ETC\" at screen 0.98,0.02 font \"Times-Roman,14\" tc lt 3 right" 
#define ETC_BRANDING_PLOTSTRING_UNSET  \
  "unset label 9999"
#define ETC_FITS_CREATOR_KEYWORD "4FS_ETC " GIT_VERSION " " GIT_DATE
#define CONTACT_EMAIL_ADDRESS "dwelly@mpe.mpg.de"

#define MAX_THREADS_LIMIT              8   // We don't want to go crazy and squash the system
                                           // Multi-thread handling is not really implemented yet

#define MAX_SPECTRO_ARMS               4   //max number of spectrograph arms
#define MAX_OBS_PARAMS                20

#define VERSION_INFO_FILENAME               "4FS_ETC_version_info.txt"
#define INTERPRETED_PARAMETER_FILENAME      "4FS_ETC_interpreted_parameters.txt"

#define FITS_SUMMARY_FILENAME               "4FS_ETC_summary.fits"



//things that we can output
#define SIM_MODE_CODE_NONE             0
#define SIM_MODE_CODE_CALC_SNR         1
#define SIM_MODE_CODE_CALC_TEXP        2
#define SIM_MODE_CODE_CALC_MAG         3

#define SIM_OUTPUT_CODE_NONE             (int) 0x00000000
#define SIM_OUTPUT_CODE_SUMMARY_ASCII    (int) 0x00000001
#define SIM_OUTPUT_CODE_SUMMARY_FITS     (int) 0x00000002
#define SIM_OUTPUT_CODE_SPECTRA_SNR      (int) 0x00000100
#define SIM_OUTPUT_CODE_SPECTRA_FLUX     (int) 0x00000200
#define SIM_OUTPUT_CODE_SPECTRA_FLUENCE  (int) 0x00000400
#define SIM_OUTPUT_CODE_SPECTRA_NOISE    (int) 0x00000800
#define SIM_OUTPUT_CODE_SPECTRA_SKY      (int) 0x00001000
#define SIM_OUTPUT_CODE_SPECTRA_TOTAL    (int) 0x00002000
#define SIM_OUTPUT_CODE_SPECTRA_POISSON  (int) 0x00004000
//#define SIM_OUTPUT_CODE_SPECTRA_MODEL    (int) 0x00008000 //not used 
//the following is used to see if any spectral products are needed
#define SIM_OUTPUT_CODE_SPECTRA          (int)(SIM_OUTPUT_CODE_SPECTRA_SNR|SIM_OUTPUT_CODE_SPECTRA_FLUX|SIM_OUTPUT_CODE_SPECTRA_FLUENCE|SIM_OUTPUT_CODE_SPECTRA_NOISE|SIM_OUTPUT_CODE_SPECTRA_SKY|SIM_OUTPUT_CODE_SPECTRA_TOTAL|SIM_OUTPUT_CODE_SPECTRA_POISSON) 
#define SIM_OUTPUT_CODE_ARF              (int) 0x00010000
#define SIM_OUTPUT_CODE_RMF              (int) 0x00020000
#define SIM_OUTPUT_MAX_CODES             32

#define SIM_SPECFORMAT_CODE_NONE         (int) 0x00000000
#define SIM_SPECFORMAT_CODE_IMAGE        (int) 0x00000001   //output as image extensions 
#define SIM_SPECFORMAT_CODE_TABLE        (int) 0x00000002   //output as table extensions 
#define SIM_SPECFORMAT_CODE_NATIVE       (int) 0x00001000   //produce output in native pixel format of spectrographs
#define SIM_SPECFORMAT_CODE_RESAMPLED    (int) 0x00002000   //produce resampled output across all spectrograph arms

//currently unneccessary
//#define SKY_TYPE_CODE_UNKNOWN      0
//#define SKY_TYPE_CODE_ESO_SKYMODEL 1

#define NORM_VAL_TYPE_CODE_UNKNOWN 0
#define NORM_VAL_TYPE_CODE_ABMAG   1
#define NORM_VAL_TYPE_CODE_UJY     2

//#define   NORM_BAND_TYPE_CODE_UNKNOWN 0
//#define   NORM_BAND_TYPE_CODE_FILTER  1
//#define   NORM_BAND_TYPE_CODE_MONO    2

#define LAMBDA_TYPE_CODE_UNKNOWN      -1
#define LAMBDA_TYPE_CODE_DISP_LINEAR   1
#define LAMBDA_TYPE_CODE_DISP_CURVE    2
#define LAMBDA_TYPE_CODE_LAMBDA_CURVE  3

#define FIBRECOUPLING_TYPE_CODE_UNKNOWN -1
#define FIBRECOUPLING_TYPE_CODE_NONE     0
#define FIBRECOUPLING_TYPE_CODE_FIXED    1
#define FIBRECOUPLING_TYPE_CODE_SEEING   2
#define FIBRECOUPLING_TYPE_CODE_MATRIX   3


#define SEEING_PROFILE_CODE_UNKNOWN -1
#define SEEING_PROFILE_CODE_GAUSSIAN 0
#define SEEING_PROFILE_CODE_MOFFAT 1

#define RESAMPLE_MODE_UNKNOWN        0
#define RESAMPLE_MODE_LINEAR_LAMBDA  1
#define RESAMPLE_MODE_LOG_LAMBDA     2
#define RESAMPLE_MODE_LINEAR_FREQ    3
#define RESAMPLE_MODE_LOG_FREQ       4


#define RSP_MIN_WAVELENGTH_STEP_AA ((double) 1e-3)
#define RSP_MIN_RESPONSE_VALUE     ((double) 1e-20)

#define SIM_MAX_FILTERS 10
#define MAGSYS_UNKNOWN   0
#define MAGSYS_AB   1
#define MAGSYS_VEGA 2

#define MIN_MAG_STEP 1e-4

#define CALC_MAX_ITERS          ((int) 100)            // 

#define CALC_TEXP_MAX_TEXP           ((double) 360000.0)    // seconds (100hrs,~4 days)
#define CALC_TEXP_MIN_TEXP           ((double) 10.0)        // seconds
#define CALC_TEXP_CONVERGE_CRITERION ((double)  2.0)        // seconds

#define CALC_MAG_MAX_MAG            ((double)  30.0)        // 
#define CALC_MAG_MIN_MAG            ((double) -10.0)        // mags
#define CALC_MAG_CONVERGE_CRITERION ((double)  0.01)        // mags


#define CALC_SNR_STATUS_START          0

#define CALC_TEXP_STATUS_START        10
#define CALC_TEXP_STATUS_IMPOSSIBLE   11
#define CALC_TEXP_STATUS_CONVERGED    12
#define CALC_TEXP_STATUS_SEARCH_IN    13   //texp_lo is unsuccessful, texp_hi is successful
#define CALC_TEXP_STATUS_SEARCH_UP    14   //                         texp_hi is unsuccessful
#define CALC_TEXP_STATUS_SEARCH_DOWN  15   //texp_lo is successful

#define CALC_MAG_STATUS_START         20
#define CALC_MAG_STATUS_IMPOSSIBLE    21
#define CALC_MAG_STATUS_CONVERGED     22
#define CALC_MAG_STATUS_SEARCH_IN     23   //mag_lo is unsuccessful, mag_hi is successful
#define CALC_MAG_STATUS_SEARCH_UP     24   //                        mag_hi is unsuccessful
#define CALC_MAG_STATUS_SEARCH_DOWN   25   //mag_lo is successful

#define CALC_STATUS_STRING(x) \
(((x)==CALC_SNR_STATUS_START || (x)==CALC_TEXP_STATUS_START || (x)==CALC_MAG_STATUS_START ) ? "START      ": \
(((x)==CALC_TEXP_STATUS_IMPOSSIBLE  || (x)==CALC_MAG_STATUS_IMPOSSIBLE ) ? "IMPOSSIBLE ":\
(((x)==CALC_TEXP_STATUS_CONVERGED   || (x)==CALC_MAG_STATUS_CONVERGED  ) ? "CONVERGED  ":\
(((x)==CALC_TEXP_STATUS_SEARCH_IN   || (x)==CALC_MAG_STATUS_SEARCH_IN  ) ? "SEARCH_IN  ":\
(((x)==CALC_TEXP_STATUS_SEARCH_UP   || (x)==CALC_MAG_STATUS_SEARCH_UP  ) ? "SEARCH_UP  ":\
(((x)==CALC_TEXP_STATUS_SEARCH_DOWN || (x)==CALC_MAG_STATUS_SEARCH_DOWN) ? "SEARCH_DOWN":\
 "Error"))))))


#define INTERP_METHOD_UNKNOWN  0
#define INTERP_METHOD_NEAREST  1
#define INTERP_METHOD_LINEAR   2
#define INTERP_METHOD_SPLINE   3
//#define INTERP_METHOD_BILINEAR 4
#define INTERP_METHOD_STRING(x) ((x)==INTERP_METHOD_UNKNOWN ? "UNKNOWN": \
((x)==INTERP_METHOD_NEAREST ? "NEAREST": \
((x)==INTERP_METHOD_LINEAR  ? "LINEAR" : \
((x)==INTERP_METHOD_SPLINE  ? "SPLINE" : \
 "Error"))))

#define TINY_DELTA_AIRMASS 0.001   //used when interpolating:
//treat airmasses that are closer tegether than this amount as being identical 

#define SKYBRIGHT_TYPE_UNKNOWN -1
#define SKYBRIGHT_TYPE_ZENITH   0
#define SKYBRIGHT_TYPE_LOCAL    1
#define SKYBRIGHT_TYPE_STRING(x) ((x)==SKYBRIGHT_TYPE_UNKNOWN ? "UNKNOWN": \
((x)==SKYBRIGHT_TYPE_ZENITH ? "ZENITH": \
((x)==SKYBRIGHT_TYPE_LOCAL  ? "LOCAL" : \
 "Error")))


#define RESPONSE_MATRIX_OVERSAMPLING 2

////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////
//this describes a matrix of fibrecoupling values
typedef struct {
  int type_code;

  double frac_science;
  double frac_sky;
  int seeing_profile_code;

  char str_filename[STR_MAX]; //file name
  //  long    nTilt;    // number of tilts (mm) in the matrix
  double *ptilt;    // array  of tilt values
  double tilt_min;
  double tilt_max;

  //  long    nIQ;      // number of IQ values in the matrix
  double *pIQ;      // array  of IQ values (FWHM,arcsec) 
  double IQ_min;
  double IQ_max;

  //  long    nMis;     // number of Misalignment values in the matrix
  double *pmisalign;     // array  of Misalignment values (arcsec)  
  double misalign_min;
  double misalign_max;

  double *pTrans;  //array which holds the transmission at each point in the 3D space
  //indexed by (nTilt + t)
  long    num_curves;     // total number of transmission entries 

} fibrecoupling_struct;
////////////////////////////////////////////////////////////////






//describes how to rebin the pixel data to measure e.g a rule 
//one of these is used for each rule
typedef struct {
  int     num_bins;     //number of wavelength bins for this rule
  int    *pNumPix;      //dimensions [num_bins]                  array of intgers giving (variable) num_pix_per_bin
  int    **ppPixIndex;  //dimensions [num_bins][num_pix_per_bin] array of pointers to arrays of pixels indices
  int    **ppArmIndex;  //dimensions [num_bins][num_pix_per_bin] array of pointers to arrays of arm indices
  double **ppOverlap;   //dimensions [num_bins][num_pix_per_bin] array of pointers to arrays of weights
  double *pLmin;        //dimensions [num_bins]                  array of pointers to wavelengths
  double *pLmax;        //dimensions [num_bins]                  array of pointers to wavelengths
} rebin_struct;


////////////////////////////////////////////////////////////////
//this describes a template spectrum
typedef struct {
  char str_name[STR_MAX];     //template+ruleset identifier (combination of template and ruleset)
  char str_filename[STR_MAX]; //
  int index;
  
  char str_ruleset[STR_MAX];  //name of spectral ruleset to use
  //  int fom_type;               //encoded version of the above
  ruleset_struct *pRuleSet;
  
  double object_fwhm;     // arcsec - set to 0 for point source
  double redshift;        // reshift of template
  double mag0;            // user-supplied r-band AB mag for template
  double mag_norm[SIM_MAX_FILTERS];  // mags of template calculated through pSim->crvNormFilterList

  char str_magsteps[LSTR_MAX];  //description of magnitude steps to simulate
  double mag_min;              //derived from str_magsteps
  double mag_max;              //
  double mag_step;             //
  int    num_mag;
  double *pmag_list;           //

  
  curve_struct crvModel;  //contains the spectral template curve
  
//  //these describe the normalisation 
//  int    norm_val_type;
//  double norm_val;
//  int    norm_band_type;
//  double norm_lambda_val;
//  curve_struct *pcrvFilter;  
  int num_rebin;
  rebin_struct *pRebin;  

} template_struct;

typedef struct {
  char str_name[STR_MAX];     //templatelist identifier
  char str_filename[STR_MAX]; //

  int num_templates;
  long array_length;  //size of assigned array
  template_struct *pTemplate; 

  //  int longest_template_name;
} templatelist_struct;
////////////////////////////////////////////////////////////////

//this structure is used to describe a wavelength scale
//all values in AA unless otherwise stated
typedef struct {

  char   str_type[STR_MAX];      // Use a linear dispersion value,take it from a dispersion curve or ?
  int    type;                   // Integer version of above
  long   num_pix;                // number of wavelength steps in array
  double refpix;                 // Pixel coordinate at which lambda = crval
  double refval;                 // Reference lambda (in AA)
  double disp;                   // Constant pixel scale of spectrograph arm AA/pix
  curve_struct crvFile;          // Dispersion of instrument or wavelength solution
 
  //derived params
  double *pl;                    // Wavelength array, with length num_pix,
                                 // giving the wavelength at the midpoint of each pixel
  double *pdl;                   // Delta wavelength array, with length num_pix,
                                 // giving the width of each pixel in AA
  double *pres_pix;              // resolution array, with length num_pix,
                                 // giving the width of a resolution element (in pixels) at each pixel
  
  double min;                    // Derived from the wavelength description
  double max;                    //

} lambda_struct;


//a response matrix
// 
typedef struct {
  //1st axis is wavelength direction
  //2nd axis is pixel direction
  long    num_rows;// number of steps in wavelength dimension of array
  long    num_cols;// number of steps in pixel dimension of array
  double *plmid;   // array length=num_rows - gives wavelength at middle of each slice
  double *pdl;     // array length=num_rows - gives wavelength step for each slice
  long   *pfirst;  // array length=num_rows - gives pixel coordinate of first significant pixel for each wavelength slice   
  long   *plast;   // array length=num_rows - gives pixel coordinate of last pixel for each wavelength slice   
  double *pres;    // array length=num_rows - gives the effective resolution, (deltal/l), as fn of wavelength, dimensionless
  double *parf;    // array length=num_rows - gives the effective area as fn of wavelength, units cm2
  double **pprmf;  // array length=[num_rows]*[plast[row] - pfirst[row]-(var)]
                   //              - gives the response matrix data, normalised to 1 per wavelength slice
  //  double *prmf;    // array length=num_rows*num_cols - gives the response matrix data, normalised to 1 per wavelength slice

} response_struct;


////////////////////////////////////////////////////////////////
//this describes a spectrograph arm
typedef struct {
  char str_name[STR_MAX]; //spectrograph arm identifier
  
  double aper_size;    // size of software aperture in the cross dispersion direction (unbinned pix)
  double aper_eef;    // Fraction of flux contained within the software aperture
  double peak_pix_frac;   // The maximum fraction of the flux that is contained within a single pixel (in the cross-dispersion direction, after on-chip binning)
  //  double spatial_fwhm; // FWHM of spectra in the cross dispersion direction  (um)
  double pixel_size;   // physical pixel size (um)
  double read_noise;   // e-/binned pix
  double sq_read_noise; // SQR(e-/pix) = aper_size*SQR(read_noise) (in unbinned case);
  double dark_current_h;  // e-/hr/pix
  double dark_current_s;   // e-/s/pix
  //  double gain;         // e-/ADU
  double full_well;    // e-/pix
  int    binning_disp; //
  int    binning_cross;

  double fiber_diam;     //in arcsec
  double fiber_area;     //in arcsec2 - derived from fiber_diam
  double effective_area; //effective collecting area of the telescope, in m2

  //  int fibrecoupling_type_code;
  //  double fibrecoupling_frac_science;
  //  double fibrecoupling_frac_sky;
  //  int seeing_profile_code;
  //  fibrecoupling_struct Fibrecoupling;  // a structure holding the fibrecoupling matrix
  

  curve_struct crvTput;   //effective thoughput of system (fn of lambda)
  curve_struct crvRes;    //resolution of instrument (fn of lambda) - dimensionless dlambda/lambda

  //derived structures
  lambda_struct Lambda;   //describes the wavelength scale of the CCD
  response_struct Rsp;    //describes the response matrix of the arm

 
  int      num_sky;       //number of sky emission vectors - same as pCondList->num_cond
  double  *psky_emiss;    //array of size [num_sky * Lambda.num_pix] - resampled to CCD grid
  

  double skysub_residual; //fraction
  
//  char   str_lambda_disp_type[STR_MAX]; //linear disperion or from taken dispersion curve?
//  int    lambda_disp_type;     // code giving linear disperion or from taken dispersion curve?
//  double lambda_refpix;   // pixel coordinate at which lambda = crval
//  double lambda_refval;   // reference lambda (in nm)
//  double lambda_disp;     // constant pixel scale of spectrograph arm nm/pix
//  curve_struct lambda_crvDisp;   // dispersion of instrument (fn of lambda) - nm/um
//
//  //derived params
//  double *plambda;        //wavelength array, with length num_pix_disp, giving the wavelength at the midpoint of each pixel
//  double lambda_min;      //derived from the wavelength description
//  double lambda_max;      //

} arm_struct;
////////////////////////////////////////////////////////////////

//this describes a spectrograph
typedef struct {
  char str_name[STR_MAX]; //spectrograph identifier

  int num_arms;           //number of separate arms in this spectrograph
  arm_struct *pArm;       //pointer to array of arm structs
  
  //derived params
  double lambda_min; //
  double lambda_max;


} spectrograph_struct;
////////////////////////////////////////////////////////////////



typedef struct {
  int index_airmass;
  int index_skybright;
  int index_IQ;
  int index_tilt;
  int index_misalign;
  int index_fibrecoupling;

  double airmass;          // ~sec(Z)
  double skybright;        // in mag/arcsec2
  double IQ;               // delivered FWHM arcsec, Vband
  double tilt;             // Tilt offset, mm at fibre tip
  double misalign;         // misalignment, arcsec
  
  double fibrecoupling_factor; //scalar factor that encodes the fibrecoupling losses at the given [IQ,tilt,misalign]

  curve_struct *pcrvEmiss;   // pointer to interpolated sky flux curve at this airmass,skybright step
  curve_struct *pcrvTrans;   // pointer to interpolated sky trans curve at this airmass step
} cond_struct;
////////////////////////////////////////////////////////////////

//this describes a grid of observational conditions
//through which each template spectrum should be simulated
//currently a 3-D parameter space
typedef struct {
 
  int num_airmass;     //number of airmasses
  double *pairmass;    //
  double airmass_min;
  double airmass_max;
  
  int num_skybright;   //num zenithal sky brightnesses
  double *pskybright;   //in mag/arcsec2
  double skybright_min;   
  double skybright_max;   

  int num_IQ;          //number of IQ values
  double *pIQ;         //delivered FWHM arcsec, Vband
  double IQ_min;
  double IQ_max;
  
  int num_tilt;        //number of tilt values
  double *ptilt;       //fibre tilt deflection (mm at tip)
  double tilt_min;   
  double tilt_max;   

  int num_misalign;    //number of misalignment values
  double *pmisalign;   //fibre->target misalignment (arcsec)
  double misalign_min;  
  double misalign_max;  

  int   num_exp;       //number of exposure times to simulate
  double *ptexp;       //total exposure time  
  int    *pnsub;       //number of sub-exposures in the above
  //ptexp[] and pnsubexp[] are arrays of same length
  double texp_min; 
  double texp_max; 
  int nsub_min;
  int nsub_max;

  
  char str_interp_method[STR_MAX];  // a code
  int interp_method;  // a code

  char str_skybright_type[STR_MAX];  // a code
  int skybright_type;  // a code
  
  int num_sky_emiss;       // = num_airmass*num_skybright
  curve_struct *pcrvEmiss;   //array of pointers [one per airmass,skybright step] to interpolated sky flux

  int num_sky_trans; // = num_airmass
  curve_struct *pcrvTrans;   //array of pointers [one per airmass step]] to interpolated sky trans

  //list of conditions to simulate
  int num_cond;
  cond_struct *pCond;

  fibrecoupling_struct Fibrecoupling;  // a structure holding the fibrecoupling matrix etc
  
} condlist_struct;
////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////
// typedef struct {
//   char   str_mode[STR_MAX];
//   int    mode;    //linear or log steps in wavelength/frequency
//   double min;     //minimum wavelength/frequency coordinate at centre of 1st pixel 
//   double delta;   //delta wavelength/frequency per pixel 
//   long   num_pix; //number of pixels in resampled output
// } resample_struct;
// ////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
typedef struct {
  char str_code_name[STR_MAX];

  char str_mode[STR_MAX];
  int  mode;

  char str_output[STR_MAX];
  int output;

  char str_specformat[STR_MAX];
  int specformat;

  char str_outdir[STR_MAX];

  char str_summary_filename[STR_MAX];
  FILE *pSummaryFile;

  char str_summary_filename_fits[STR_MAX];
  fitsfile *pSummaryFile_fits;
  BOOL SummaryFile_init_flag;
  long SummaryFile_num_rows;
  long SummaryFile_current_row;

  //  resample_struct Resample;

  int num_filters;
  char str_norm_filter_magsys[STR_MAX];
  int norm_filter_magsys;
  curve_struct crvNormFilterList[SIM_MAX_FILTERS];   // bandpass curves for the normalising filters

  int longest_template_name;
  int longest_template_filename;
  int longest_ruleset_name;

  
  BOOL clobber;
  BOOL debug;
  long random_seed;

} sim_struct;
////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////
//this is a set of data arrays which contain simulated spectral data
//each target array must be at least num_pix long
typedef struct {
  int num_arms; //number of spectrograph arms
  int num_spec; //number of spectra per arm
  long *pnum_pix;  //number of pixels per spectrum per arm
  double **ppFlux0;         //array dimensions=[num_arms][num_cond*num_pix] units e-/s/pix  -- for pTemplate->mag0
  double **ppFlux;          //array dimensions=[num_arms][num_spec*num_pix] units e-/s/pix
  double **ppFluence;       //array dimensions=[num_arms][num_spec*num_pix] units e-/pix
  double **ppSky    ;       //array dimensions=[num_arms][num_spec*num_pix] units e-/pix
  double **ppTotal  ;       //array dimensions=[num_arms][num_spec*num_pix] units e-/pix
  double **ppNoise  ;       //array dimensions=[num_arms][num_spec*num_pix] units e-/pix
  double **ppSNR    ;       //array dimensions=[num_arms][num_spec*num_pix] units dimensionless
  condlist_struct *pCondList; //shortcut 
  spectrograph_struct *pSpec; //shortcut 
  template_struct *pTemplate; //shortcut 

  int num_rules;
  double **ppSubResult;    //array dimensions=[num_spec][num_rules] 
  double *pResult;         //array dimensions=[num_spec] 
  double *ptexp;           //array dimensions=[num_spec], units s
  double *pmag;            //array dimensions=[num_spec], units mag
  int    *pnsub;           //array dimensions=[num_spec], number of exposures
  double *plo;             //array dimensions=[num_spec], units s or mag
  double *phi;             //array dimensions=[num_spec], units s or mag
  int    *pstatus;         //array dimensions=[num_spec]  flag


//  //resampled versions of the above, delta is either dlambda or dnu
//  resample_struct *pResample; //shortcut 
//  double *pResFlux   ;  //array dimensions=[num_spec*resample_num_pix] units e-/s/pix/delta
//  double *pResFluence;  //array dimensions=[num_spec*resample_num_pix] units e-/pix/delta
//  double *pResSky    ;  //array dimensions=[num_spec*resample_num_pix] units e-/pix/delta
//  double *pResTotal  ;  //array dimensions=[num_spec*resample_num_pix] units e-/pix/delta
//  double *pResNoise  ;  //array dimensions=[num_spec*resample_num_pix] units e-/pix/delta
//  double *pResSNR    ;  //array dimensions=[num_spec*resample_num_pix] units dimensionless
//  double *pResNorm   ;  //array dimensions=[num_spec*resample_num_pix] units (erg/cm2/s)/(e-/pix)
//                        //                                             converts observed e-/pix/delta into a physical flux density 
  
} simspec_struct;
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
//this is a collection of pointers to data arrays which allow easy passing of a single simulated spectrum's properties
//each target array must be at least num_pix long
typedef struct {
  int num_arms;             //number of spectrograph arms
  long *pnum_pix;           //[num_arms] number of pixels per spectrum per arm 
  double **ppFlux;          //[num_arms][num_pix] units e-/s/pix
  double **ppFluence;       //[num_arms][num_pix] units e-/pix
  double **ppSky    ;       //[num_arms][num_pix] units e-/pix
  double **ppTotal  ;       //[num_arms][num_pix] units e-/pix
  double **ppNoise  ;       //[num_arms][num_pix] units e-/pix
  double **ppSNR    ;       //[num_arms][num_pix] units dimentionless

  spectrograph_struct *pSpec; //shortcut 
  template_struct *pTemplate; //shortcut 
  cond_struct *pCond;         //shortcut 
  double texp;
  int    nsub;
  double mag;
} simspec_single_struct;
//////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///Here comes a list of function prototypes
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
int print_version_and_inputs                 (FILE* pstream, int argc, char **argv);
int FourFS_ETC_print_version                 (FILE *pFile);
int print_full_version_information_to_file   (const char* str_filename);
int print_full_version_information           (FILE *pFile);
int print_full_help_information              (FILE *pFile);

int write_summary_page_html                  (param_list_struct *pParamList,
                                              const char* str_filename);

/////////////////////////////////////////////////////////////////////////////
int template_struct_init ( template_struct *pTemplate );
int template_struct_free ( template_struct *pTemplate );
int templatelist_struct_init ( templatelist_struct **ppTL );
int templatelist_struct_free ( templatelist_struct **ppTL );
int templatelist_struct_read_from_file ( templatelist_struct *pTL , BOOL verbose);
int template_struct_calc_mag_through_filter(template_struct* pTemplate, curve_struct* pcrvFilter, double* presult);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int lambda_struct_init ( lambda_struct *pLambda );
int lambda_struct_free ( lambda_struct *pLambda );
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int response_struct_init ( response_struct *pResponse );
int response_struct_free ( response_struct *pResponse );
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
int fibrecoupling_struct_init ( fibrecoupling_struct *pFibrecoupling );
int fibrecoupling_struct_free ( fibrecoupling_struct *pFibrecoupling );
int fibrecoupling_struct_read_from_file ( fibrecoupling_struct *pFibrecoupling, BOOL verbose );
int fibrecoupling_struct_find_nearest ( fibrecoupling_struct *pFibrecoupling,
                                        cond_struct          *pCond,
                                        BOOL                  print_flag);

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int arm_struct_init ( arm_struct *pArm );
int arm_struct_free ( arm_struct *pArm );
int arm_struct_setup ( arm_struct *pArm);
int arm_struct_write_rmf_to_file ( arm_struct *pArm , char* str_filename );
int arm_struct_write_arf_to_file ( arm_struct *pArm , char* str_filename );
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int spectrograph_struct_init ( spectrograph_struct **ppSpec );
int spectrograph_struct_write_response ( spectrograph_struct *pSpec, sim_struct* pSim );
int spectrograph_struct_free ( spectrograph_struct **ppSpec );
int spectrograph_struct_setup ( spectrograph_struct *pSpec);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int sim_struct_init ( sim_struct **ppSim );
int sim_struct_free ( sim_struct **ppSim );
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int cond_struct_init      ( cond_struct *pCond );

int condlist_struct_init  ( condlist_struct **ppCondList );

int condlist_struct_free  ( condlist_struct **ppCondList );

int condlist_struct_setup ( condlist_struct  *pCondList,
                            sky_struct *pSky,
                            BOOL verbose);

int condlist_struct_write_to_chdu (condlist_struct *pCondList,
                                   template_struct *pTemplate,
                                   simspec_struct* pSimSpec,
                                   fitsfile* pFile );
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int convolve_curve_with_response           (arm_struct *pArm,
                                            curve_struct *pCurve,
                                            BOOL mult_by_arf,
                                            double* presult );

int convolve_spectrum_with_rmf             (arm_struct *pArm,
                                            template_struct *pTemplate,
                                            double* presult );

int convolve_spectrum_with_rmf_arf         (arm_struct *pArm,
                                            template_struct *pTemplate,
                                            double* presult );

int convolve_curve_with_rmf_arf            (arm_struct *pArm,
                                            curve_struct *pCurve,
                                            double* presult );

int pass_spectrum_through_cond             (template_struct *pTemplate,
                                            cond_struct *pCond,
                                            double* presult );

int pass_curve_through_cond                (curve_struct    *pCurve,
                                            cond_struct *pCond,
                                            double* pResult );

int work_on_templatelist                   (templatelist_struct *pTL,
                                            spectrograph_struct *pSpec,
                                            condlist_struct     *pCondList,
                                            sim_struct          *pSim);

int work_on_templatelist_calc_snr          (templatelist_struct *pTL,
                                            spectrograph_struct *pSpec,
                                            condlist_struct     *pCondList,
                                            sim_struct          *pSim);

int work_on_templatelist_iter              (templatelist_struct *pTL,
                                            spectrograph_struct *pSpec,
                                            condlist_struct     *pCondList,
                                            sim_struct          *pSim);

int calc_flux_fluence_sky_noise_snr_spectra(simspec_struct* pSimSpec,
                                            template_struct* pTemplate,
                                            condlist_struct* pCondList,
                                            spectrograph_struct* pSpec);

int write_summary_to_file_ascii            (FILE* pFile,
                                            template_struct* pTemplate,
                                            condlist_struct* pCondList,
                                            simspec_struct* pSimSpec);

int write_summary_to_file_fits             (sim_struct* pSim,
                                            template_struct* pTemplate,
                                            condlist_struct* pCondList,
                                            simspec_struct* pSimSpec);

int fold_template_through_spectrograph     (template_struct     *pTemplate,
                                            spectrograph_struct *pSpec,
                                            condlist_struct     *pCondList);

int fold_template_through_arm_cond         (template_struct     *pTemplate,
                                            arm_struct          *pArm,
                                            cond_struct         *pCond,
                                            double              *pResult,
                                            double               mag);

int fold_template_through_arm_condlist     (template_struct     *pTemplate,
                                            arm_struct          *pArm,
                                            condlist_struct     *pCondList,
                                            double              *pResult,
                                            double               mag);

int fold_sky_through_spectrograph          (spectrograph_struct *pSpec,
                                            condlist_struct     *pCondList );

int fold_sky_through_arm                   (arm_struct          *pArm,
                                            condlist_struct     *pCondList );


int associate_templatelist_with_rulesetlist (templatelist_struct *pTL,
                                             rulesetlist_struct *pRuleSetList);
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
int write_wavelength_solution_table_to_chdu (fitsfile* pFile,
                                             lambda_struct *pLambda);
int write_wavelength_solution_vector_to_chdu (fitsfile* pFile,
                                              lambda_struct *pLambda);

int write_spec_image_extension_to_chdu (fitsfile* pFile,
                                        condlist_struct *pCondList,
                                        template_struct *pTemplate,
                                        arm_struct *pArm,
                                        double *pData,
                                        double *pData2,
                                        int extension_type);

int write_spec_table_extension_to_chdu (fitsfile *pFile,
                                        sim_struct *pSim,
                                        condlist_struct *pCondList,
                                        template_struct *pTemplate,
                                        arm_struct *pArm,
                                        simspec_struct  *pSimSpec,
                                        int a);

int write_spec_data_to_files          (spectrograph_struct *pSpec,
                                       condlist_struct *pCondList,
                                       template_struct *pTemplate,
                                       simspec_struct  *pSimSpec,
                                       sim_struct      *pSim);

/////////////////////////////////////////////////////////////////////////////
int simspec_struct_init                     (simspec_struct **ppSimSpec);
int simspec_struct_free                     (simspec_struct **ppSimSpec);
int simspec_struct_alloc                    (simspec_struct *pSimSpec,
                                             spectrograph_struct *pSpec,
                                             condlist_struct *pCondList);

int simspec_struct_test_success       ( simspec_struct *pSimSpec);
int simspec_struct_spec_index         ( simspec_struct *pSimSpec,
                                        int mag_index,
                                        int exp_index,
                                        int cond_index);

int simspec_struct_update_texp_status ( simspec_struct *pSimSpec,
                                        int *pnum_search_spec);
int simspec_struct_update_mag_status ( simspec_struct *pSimSpec,
                                       int *pnum_search_spec);

int test_success_of_simspec_single    ( simspec_single_struct *pSSS,
                                        double                *pResult,
                                        double                *pSubResult);

int test_rule_on_simspec_single       (int r, 
                                       simspec_single_struct *pSSS,
                                       double                *presult);
   
int simspec_single_struct_init   ( simspec_single_struct *pSSS );
int simspec_single_struct_free   ( simspec_single_struct *pSSS );
int simspec_single_struct_alloc  ( simspec_single_struct *pSSS,
                                   spectrograph_struct *pSpec );
int simspec_single_struct_set    ( simspec_single_struct *pSSS,
                                   simspec_struct *pSimSpec,
                                   int m,
                                   int c,
                                   int e );

  
int simspec_single_struct_rebin_for_rule ( simspec_single_struct *pSSS,
                                           int r, 
                                           curve_struct *pCurve);
        

/////////////////////////////////////////////////////////////////////////////


int rebin_struct_init ( rebin_struct *pRebin);
int rebin_struct_free ( rebin_struct *pRebin);
int fold_templatelist_through_norm_filters ( sim_struct      *pSim,
                                             templatelist_struct *pTL,
                                             int num_filters,
                                             curve_struct* pcrvNormFilterList);

int templatelist_struct_prepare_rebinning ( templatelist_struct *pTL,
                                            spectrograph_struct *pSpec);

int template_struct_prepare_rebinning ( template_struct *pTemplate,
                                        spectrograph_struct *pSpec);
                                       
int rebin_struct_prepare_rebinning   (rebin_struct        *pRebin,
                                      rule_struct         *pRule,
                                      spectrograph_struct *pSpec,
                                      double               redshift);
double calc_noise_gehrels_style (double signal,
                                 double bkgnd,
                                 double sq_read_noise,
                                 double skysub_residual);


//int resample_struct_init ( resample_struct *pResample );
//int simspec_struct_resample (simspec_struct* pSimSpec);

#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
