//----------------------------------------------------------------------------------
//--
//--    Filename: ETC_rules_lib.h
//--    Use: This is a header for the ETC_rules_lib.c C code file
//--
//--    Notes:
//--


#ifndef ETC_RULES_LIB_H
#define ETC_RULES_LIB_H

//standard header files - need this for FILE* pointers
#include <stdio.h>

#include "define.h"
#include "curve_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.2 $"
//#define CVS_DATE_H     "$Date: 2015/12/03 11:31:34 $"
inline static int ETC_rules_lib_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX        DEF_STRLEN
#define LONG_STR_MAX   DEF_LONGSTRLEN
#define LSTR_MAX       DEF_LONGSTRLEN


enum rule_variable_code {
  RULE_VARIABLE_CODE_UNKNOWN       =0,
  RULE_VARIABLE_CODE_FLUX            ,   //background-subtracted signal rate in detector units: (e-/s/lambda_bin)
  RULE_VARIABLE_CODE_FLUENCE         ,   //total background-subtracted signal in detector units: (e-/lambda_bin)
  RULE_VARIABLE_CODE_TOTAL_FLUENCE   ,   //total signal+background in detector units: (e-/lambda_bin)
  RULE_VARIABLE_CODE_NOISE           ,   //noise in detector units: (e-/lambda_bin)
  RULE_VARIABLE_CODE_NOISE_CGS       ,   //noise in physical units: (erg/cm2/s/A)
  RULE_VARIABLE_CODE_SKY_FLUX        ,   //background rate in detector units: (e-/s/lambda_bin)
  RULE_VARIABLE_CODE_SKY_FLUENCE     ,   //total background in detector units: (e-/lambda_bin)
  RULE_VARIABLE_CODE_SNR             ,   //signal-to-noise ratio: (dimensionless)
  RULE_VARIABLE_CODE_RESOLUTION      ,   //spectral resolution, R=lambda/delta_lambda: (dimensionless)
  RULE_VARIABLE_CODE_TEXP            ,   //exposure time: (seconds)
  RULE_VARIABLE_CODE_NSUB                //number of sub-exposures: (dimensionless)
};

#define RULE_VARIABLE_STRING(x) ((x)==RULE_VARIABLE_CODE_UNKNOWN ? "UNKNOWN": \
((x)==RULE_VARIABLE_CODE_FLUX           ? "FLUX":\
((x)==RULE_VARIABLE_CODE_FLUENCE        ? "FLUENCE":\
((x)==RULE_VARIABLE_CODE_TOTAL_FLUENCE  ? "TOTAL_FLUENCE":\
((x)==RULE_VARIABLE_CODE_NOISE          ? "NOISE_PHOTONS":\
((x)==RULE_VARIABLE_CODE_NOISE_CGS      ? "NOISE_CGS":\
((x)==RULE_VARIABLE_CODE_SKY_FLUX       ? "SKY_FLUX":\
((x)==RULE_VARIABLE_CODE_SKY_FLUENCE    ? "SKY_FLUENCE":\
((x)==RULE_VARIABLE_CODE_SNR            ? "SNR":\
((x)==RULE_VARIABLE_CODE_RESOLUTION     ? "RESOLUTION":\
((x)==RULE_VARIABLE_CODE_TEXP           ? "TEXP":\
((x)==RULE_VARIABLE_CODE_NSUB           ? "NSUB":\
 "Error"))))))))))))

#define RULE_METRIC_CODE_UNKNOWN  0
#define RULE_METRIC_CODE_MEAN     1
#define RULE_METRIC_CODE_MIN      2
#define RULE_METRIC_CODE_MAX      3
#define RULE_METRIC_CODE_SUM      4
#define RULE_METRIC_CODE_NSAMPLE  5
//#define RULE_METRIC_CODE_STDDEV 6
#define RULE_METRIC_CODE_PC      10
#define RULE_METRIC_CODE_MEDIAN  11

#define RULE_METRIC_STRING(x) ((x)==RULE_METRIC_CODE_UNKNOWN ? "UNKNOWN": \
((x)==RULE_METRIC_CODE_MEAN    ? "MEAN":\
((x)==RULE_METRIC_CODE_MIN     ? "MIN":\
((x)==RULE_METRIC_CODE_MAX     ? "MAX":\
((x)==RULE_METRIC_CODE_SUM     ? "SUM":\
((x)==RULE_METRIC_CODE_NSAMPLE ? "NSAMPLE":\
((x)==RULE_METRIC_CODE_PC      ? "PERCENTILE":\
((x)==RULE_METRIC_CODE_MEDIAN  ? "MEDIAN":\
 "Error"))))))))


#define RULE_OPERATOR_CODE_UNKNOWN 0
#define RULE_OPERATOR_CODE_EQ      1
#define RULE_OPERATOR_CODE_GT      2
#define RULE_OPERATOR_CODE_GE      3
#define RULE_OPERATOR_CODE_LT      4
#define RULE_OPERATOR_CODE_LE      5
#define RULE_OPERATOR_CODE_DIV    10
#define RULE_OPERATOR_CODE_MULT   11

#define RULE_OPERATOR_STRING(x) ((x)==RULE_OPERATOR_CODE_UNKNOWN ? "UNKNOWN": \
((x)==RULE_OPERATOR_CODE_EQ    ? "EQ":\
((x)==RULE_OPERATOR_CODE_GT    ? "GT":\
((x)==RULE_OPERATOR_CODE_GE    ? "GE":\
((x)==RULE_OPERATOR_CODE_LT    ? "LT":\
((x)==RULE_OPERATOR_CODE_LE    ? "LE":\
((x)==RULE_OPERATOR_CODE_DIV   ? "DIV":\
((x)==RULE_OPERATOR_CODE_MULT  ? "MULT":\
 "Error"))))))))


#define RULE_LAMBDA_UNIT_CODE_UNKNOWN 0
#define RULE_LAMBDA_UNIT_CODE_AA      1
#define RULE_LAMBDA_UNIT_CODE_NM      2
#define RULE_LAMBDA_UNIT_CODE_UM      3
#define RULE_LAMBDA_UNIT_CODE_REST_AA 4
#define RULE_LAMBDA_UNIT_CODE_REST_NM 5
#define RULE_LAMBDA_UNIT_CODE_REST_UM 6

#define RULE_LAMBDA_UNIT_STRING(x) ((x)==RULE_LAMBDA_UNIT_CODE_UNKNOWN ? "UNKNOWN": \
((x)==RULE_LAMBDA_UNIT_CODE_AA       ? "AA":\
((x)==RULE_LAMBDA_UNIT_CODE_NM       ? "NM":\
((x)==RULE_LAMBDA_UNIT_CODE_UM       ? "UM":\
((x)==RULE_LAMBDA_UNIT_CODE_REST_AA  ? "REST_AA":\
((x)==RULE_LAMBDA_UNIT_CODE_REST_NM  ? "REST_NM":\
((x)==RULE_LAMBDA_UNIT_CODE_REST_UM  ? "REST_UM":\
 "Error")))))))

#define RULE_DELTAL_UNIT_CODE_UNKNOWN 0
#define RULE_DELTAL_UNIT_CODE_PIX     1
#define RULE_DELTAL_UNIT_CODE_RES     2
#define RULE_DELTAL_UNIT_CODE_AA      3
#define RULE_DELTAL_UNIT_CODE_NM      4
#define RULE_DELTAL_UNIT_CODE_UM      5
#define RULE_DELTAL_UNIT_CODE_REST_AA 6
#define RULE_DELTAL_UNIT_CODE_REST_NM 7
#define RULE_DELTAL_UNIT_CODE_REST_UM 8

#define RULE_DELTAL_UNIT_STRING(x) ((x)==RULE_DELTAL_UNIT_CODE_UNKNOWN ? "UNKNOWN": \
((x)==RULE_DELTAL_UNIT_CODE_PIX      ? "PIX":\
((x)==RULE_DELTAL_UNIT_CODE_RES      ? "RES":\
((x)==RULE_DELTAL_UNIT_CODE_AA       ? "AA":\
((x)==RULE_DELTAL_UNIT_CODE_NM       ? "NM":\
((x)==RULE_DELTAL_UNIT_CODE_UM       ? "UM":\
((x)==RULE_DELTAL_UNIT_CODE_REST_AA  ? "REST_AA":\
((x)==RULE_DELTAL_UNIT_CODE_REST_NM  ? "REST_NM":\
((x)==RULE_DELTAL_UNIT_CODE_REST_UM  ? "REST_UM":\
 "Error")))))))))



#define RULESET_COLNUM_RESULT      1
#define RULESET_COLNUM_REDSHIFT    2
#define RULESET_COLNUM_FIRST_RULE  3
#define RULESET_COLNAME_RESULT      "RULESET_RESULT"
#define RULESET_COLNAME_REDSHIFT    "REDSHIFT"
//#define RULESET_RESULT_COLUMN_NAME "RULESET_RESULT"
//#define RULESET_REDSHIFT_COLUMN_NAME "RULESET_REDSHIFT"
#define RULESET_EXPR_FILENAME      "mem://rulesetlist_expr_evaluation_mem_file.fits"


//This is the structure holding the definition of a single Rule 
typedef struct {
  char str_name[STR_MAX];
  
  int variable;   //FLUX,FLUENCE,NOISE,SKY,SNR etc
  
  int metric;     // MEAN,MEDIAN,MIN,MAX,SUM etc
  double percentile;  //derived param
  
  int operator; // GT, LT, GE, LE, EQ -> these return "weight" if true, 0 if false
                // DIV,MULT                -> these return weight*(MAX(evaluated_result(/,*)comparison_value),1)

  double comparison_value; //what number to compare against

  double lmin; //wavelength range, in AA

  double lmax;
  int lambda_unit; //what type of lambda, pix,AA,nm,um,rest_AA,rest_nm etc

  double deltal;   // multiple in wavelength scale
  int deltal_unit; //what type of delta lambda, pix,RES,AA,nm,um,rest_AA etc

  BOOL special;   //set to TRUE if this is a "special" rule

  int expr_file_col_num;  //column number for this rule in the expression evaluation file
} rule_struct;




//this is just a list of rules, read in from a file
////////////////////////////////////////////////////////////////
//expect the following format
// RULE_NAME  VARIABLE METRIC OPERATOR COMP_VALUE  LAMBDA_MIN  LAMBDA_MAX LAMBDA_UNIT DELTAL DELTAL_UNIT
// e.g.
// QSOBlueCont     SNR MEDIAN       GE        5.0      2000.0      3000.0          AA    1.0          AA
// QSOHbeta        SNR MEDIAN       GE       10.0      4851.3      4871.3          AA    3.0          AA
// all fields are required

typedef struct {
  char str_filename[STR_MAX];
  int num_rules;
  rule_struct *pRule;
} rulelist_struct;


//this describes the rules associated with a ruleset
typedef struct {
  char str_name[STR_MAX];
  int num_rules;
  rule_struct **ppRule;  //a list of pointers to the rule structs (stored separately in a rulelist_struct)
  //  double *pweight;       //a list of weights for each rule, length [num_rules]
  double required_value;  //the threshold that the result of the expression must equal or exceed for success  
  char *str_expression;  //contains expression to be evaluated
  fitsfile *pExprFile;  //! just a pointer to the fitsfile* pExprFile hosted by rulesetlist_struct
} ruleset_struct;

// this is just a list of rulesets, read in from a file
// Rulesetlist file format:
//old// // RULESET_NAME REQUIRED_WEIGHT RULENAME1[*WEIGHT1] [RULENAME2[*WEIGHT2]] [RULENAME3[*WEIGHT3]] ....
// RULESET_NAME REQUIRED_WEIGHT EXPRESSION
typedef struct {
  char str_filename[STR_MAX];
  int num_rulesets;
  ruleset_struct *pRuleSet;

  fitsfile *pExprFile;   //memory file used to compute expressions using cfitsio machinery
} rulesetlist_struct;
////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
int rule_struct_init                        (rule_struct *pRule );
int rulelist_struct_init                    (rulelist_struct **ppRuleList );
int rulelist_struct_free                    (rulelist_struct **ppRuleList );
int rule_struct_print                       (rule_struct *pRule, FILE* pFile);
int rulelist_struct_print                   (rulelist_struct *pRuleList, FILE* pFile);
int ruleset_struct_init                     (ruleset_struct  *pRuleSet );
int ruleset_struct_free                     (ruleset_struct  *pRuleSet );
int rulesetlist_struct_init                 (rulesetlist_struct **ppRuleSetList );
int rulesetlist_struct_free                 (rulesetlist_struct **ppRuleSetList );
int rulesetlist_struct_print                (rulesetlist_struct *pRuleSetList, FILE* pFile);
int rulesetlist_struct_setup_expressions    (rulesetlist_struct *pRuleSetList, rulelist_struct* pRuleList);
int rulelist_struct_read_from_file          (rulelist_struct *pRuleList );
int rulesetlist_struct_read_from_file       (rulesetlist_struct *pRuleSetList, rulelist_struct *pRuleList);
int rulelist_struct_add_specials            (rulelist_struct* pRuleList);
/////////////////////////////////////////////////////////////////////////////

#endif
