#------------------------------------
# This is the makefile for 4FS_ETC
# CVS_REVISION "$Revision: 1.10 $"
# CVS_DATE     "$Date: 2015/11/24 08:47:13 $"
#------------------------------------


include ../arch.make

#the following allows you to have an include file 
#that lives outside the CVS tree:
#include ~/code/arch.make

# If for some reason you need to override the settings 
# in these arch.make files then do so below: 
#OPTIMISE = -O2
#DEBUG =
#BINARY_DIR = 
#ARCH =
#CFLAGS = $(ARCH) -Wall -Wno-parentheses -Wl,--no-as-needed $(OPTIMISE) $(DEBUG)
#CXX = gcc

COMMON_DIR = ../common


LDIR = -L./ -L$(COMMON_DIR)/ -L$(CFITSIO_PATH)/lib 

IDIR = -I./ -I$(COMMON_DIR)/ -I$(CFITSIO_PATH)/include 

# #for paralel processing
# now included in ../arch.make
# OMP_FLAGS = -fopenmp

LIBS =  -lm -lcfitsio $(OMP_FLAGS) $(IDIR) $(LDIR)


OUTSTEM = 4FS_ETC
OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))
OUTFILE = $(OUTSTEM)

DEP_LIBS = $(COMMON_DIR)/utensils_lib $(COMMON_DIR)/params_lib $(COMMON_DIR)/fits_helper_lib $(COMMON_DIR)/curve_lib ETC_input_params_lib ETC_sky_lib ETC_rules_lib 

DEP_OFILES  = $(OFILE) $(addsuffix .o,$(DEP_LIBS)) 

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) $(COMMON_DIR)/define.h

DEP_CFILES  = $(CFILE) $(addsuffix .c,$(DEP_LIBS)) 


$(OUTFILE)	:	$(DEP_OFILES)
	$(CXX) $(CFLAGS) $(LIBS) -o $(OUTFILE) $(DEP_OFILES)

$(OFILE)	:	$(DEP_HFILES) $(DEP_CFILES)
	$(CXX) $(CFLAGS) $(OMP_FLAGS) $(IDIR) -c -o $(OFILE) $(CFILE) 

#define the rules to make the library files
%_lib.o	:	%_lib.[ch]
	make -f $*_lib.make

install   : 
	cp -pv $(OUTFILE) $(BINARY_DIR)

#clean   : 
#	rm -f $(OUTFILE) ./*.o
##	rm -f $(OFILE) $(OUTFILE) ./*.o

clean   : 
	rm -f $(OFILES) ./*.o $(OUTFILE) $(DEP_OFILES) $(BINARY_DIR)/$(OUTFILE)

.PHONY:	doc
doc	:	
	doxygen $(OUTSTEM).doxyfile


#end

