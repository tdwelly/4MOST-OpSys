//standard header files
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <time.h>
#include <sys/syscall.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/utsname.h>
//#include <pthread.h>
#include <unistd.h>   //needed for fork(), getcwd etc
//#include <signal.h>   //needed for process handling

#if defined(_OPENMP)
#include <omp.h>   //needed for parallel operation info routines
#endif

//#include <fitsio.h>    //needed for fits catalogues etc
#include <assert.h>   //needed for debugging

//my header files come next
#include "4FS_ETC.h"



//now add the included libraries that depend on definitions/data-structures in 4FS_ETC.h
#include "ETC_input_params_lib.h"

//#define CVS_REVISION "$Revision: 1.21 $"
//#define CVS_DATE     "$Date: 2015/12/03 11:31:34 $"

//apart from a few defintions that we do not want to escape beyond the scope of this file
#define COMMENT_PREFIX "#-4FS_ETC.c:Comment: "
#define WARNING_PREFIX "#-4FS_ETC.c:Warning: "
#define ERROR_PREFIX   "#-4FS_ETC.c:Error:   "
#define DEBUG_PREFIX   "#-4FS_ETC.c:Debug:   "
#define DATA_PREFIX    ""

//used as a dummy for when there is no ruleset for a template
const ruleset_struct dummy_ruleset = {
  .str_name       = "NONE",  //char str_name[STR_MAX];
  .num_rules      = 0,       // int num_rules;
  .ppRule         = NULL,    // rule_struct **ppRule;  //a list of pointers to the rule structs (stored separately in a rulelist_struct)
  .required_value = NAN,     // double required_value;  //the threshold that the result of the expression must equal or exceed for success  
  .str_expression = "",      // char *str_expression;  //contains expression to be evaluated
  .pExprFile      = NULL};   // fitsfile *pExprFile;  //! just a pointer to the fitsfile* pExprFile hosted by rulesetlist_struct


// Notes ///
//
// Inputs will be supplied with a range of resolutions
// We require that the following are expressed at linear steps in wavelength:
// * template spectra
// * sky transmission
// * sky emission
// But the following will be specified as tabulated lists, with possibly non-uniform wavelength steps
// * throughput
// * resolution
// * dispersion
// 
// All derived curves will have linear steps in wavelength  
//
// Order of steps taken
// Object spectrum: 
//  -> multiply by sky transmission curve
//  -> convolve with spectral response kernel and resample to CCD pixels
//    --> kernel includes telescope throughput and resolution
//  -> multiply by exposure time
//
//
//
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////MAIN STARTS HERE////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////
// now we start the main program
int main (int argc, char* argv[])
{
  /////////////////////////////////////
  //internal record of start of execution time
  clock_t clocks_main_start = clock();
  //////////////////////////////////
  
  //this structure deals with the reading in of input parameters
  param_list_struct *pParamList = NULL;

  //this structure holds the input parameters themselves
  ETC_input_params_struct *pIpar = NULL;

  //these are the main data structures
  sky_struct           *pSky  = NULL;
  templatelist_struct  *pTL   = NULL;
  spectrograph_struct  *pSpec = NULL;
  condlist_struct      *pCondList = NULL;
  sim_struct           *pSim = NULL;
  rulelist_struct      *pRuleList = NULL;
  rulesetlist_struct   *pRuleSetList = NULL;

  //  //DEBUG
  //  curve_struct         crvTest;

  
//  /////////////////////////
//  // Prevent zombie child processes by setting a behaviour for signal handling 
//  struct sigaction sa;
//  sa.sa_handler = SIG_IGN;
//  sa.sa_flags   = SA_NOCLDWAIT;
//  if ( sigaction(SIGCHLD, &sa, NULL) == -1 )
//  {
//    perror("sigaction");
//    return 1;
//  }
//  /////////////////////////

  /////////////////////////////////////////////////////////
  //emit the command line and program details
  if ( print_version_and_inputs(stdout, (int) argc, (char **) argv)) return 1;

#if defined(_OPENMP)
  //  fprintf(stdout, "%s Number of OMP threads= %d\n", DEBUG_PREFIX, omp_get_num_threads());
#endif
  /////////////////////////////////////////////////////////

  //make space for and init the data structures
  if ( sky_struct_init          ((sky_struct**)          &pSky ))        return 1;
  if ( templatelist_struct_init ((templatelist_struct**) &pTL  ))        return 1;
  if ( spectrograph_struct_init ((spectrograph_struct**) &pSpec))        return 1;
  if ( condlist_struct_init     ((condlist_struct**)     &pCondList))    return 1;
  if ( sim_struct_init          ((sim_struct**)          &pSim ))        return 1;
  if ( rulelist_struct_init     ((rulelist_struct**)     &pRuleList))    return 1;
  if ( rulesetlist_struct_init  ((rulesetlist_struct**)  &pRuleSetList)) return 1;

  //DEBUG
  //  if ( curve_struct_init ((curve_struct*) &crvTest)) return 1;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //set up and interpret the input params
  {
    int status; 
    if ( (status = ETC_input_params_handler ((ETC_input_params_struct**) &pIpar,
                                             (param_list_struct**) &pParamList,
                                             (int) argc, (char**) argv)) != 0 )
    {
      return 1; //DEBUG
      if ( status < 0 ) return 1; //this is due to a real error
      else              exit(0);  //not a real error, but we should exit
    }
  }

  //now interpret all the input parameters
  if ( ETC_input_params_interpret_everything ((ETC_input_params_struct*) pIpar,
                                              (spectrograph_struct*)    pSpec,
                                              (sky_struct*)             pSky,
                                              (templatelist_struct*)    pTL,
                                              (condlist_struct*)        pCondList,
                                              (sim_struct*)             pSim,
                                              (rulelist_struct*)        pRuleList,
                                              (rulesetlist_struct*)     pRuleSetList))  return 1; 

  //write some standard output files
  {
    char str_buffer[STR_MAX];
    snprintf(str_buffer, STR_MAX, "%s/%s", pSim->str_outdir, VERSION_INFO_FILENAME);
    if ( print_full_version_information_to_file ((const char*) str_buffer)) return 1;

    snprintf(str_buffer, STR_MAX, "%s/%s", pSim->str_outdir, INTERPRETED_PARAMETER_FILENAME);
    if ( ParLib_print_param_list_to_file ((const char*) str_buffer,
                                          (param_list_struct*)pParamList)) return 1;
  }


  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//  {
//    int a;
//    for(a=0;a<pSpec->num_arms;a++)
//    {
//      arm_struct *pArm = (arm_struct*) &(pSpec->pArm[a]);
//      fprintf(stderr, "AAAA a=%d num_pix = %ld\n", a, (pArm->Lambda).num_pix);
//    }
//  }

  
//  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  ///DEBUGGERY
//  strncpy(crvTest.str_filename, pIpar->SKY_TRANSMISSION_FILENAME, STR_MAX);
//  strncpy(crvTest.str_name, "testcurve", STR_MAX);
//  strncpy(crvTest.str_colname_x, "LAMBDA", STR_MAX);
//  strncpy(crvTest.str_colname_y, "TRANS_AM1p0", STR_MAX);
//  if ( curve_struct_read_from_file ( (curve_struct*) &crvTest ))
//  {
//    fprintf(stderr, "%s I had problems reading curve: %s\n",
//            ERROR_PREFIX, crvTest.str_name);
//    return 1;
//  }
//  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  //  fprintf(stdout, "%s (at file %s line %d)\n", DEBUG_PREFIX, __FILE__, __LINE__); fflush(stdout); //DEBUG//
  
  //---------------------------------------------------------------------------------------
  //read in the sky transmission description
  if ( sky_trans_struct_read_from_file ( (sky_trans_struct*) &(pSky->Trans), (BOOL) pSim->debug ))
  {
    fprintf(stderr, "%s I had problems reading sky transmission: %s\n",
            ERROR_PREFIX, pSky->Trans.str_filename);
    return 1;
  }

  //read in the sky emission description
  if ( sky_emiss_struct_read_from_file ( (sky_emiss_struct*) &(pSky->Emiss) , (BOOL) pSim->debug ))
  {
    fprintf(stderr, "%s I had problems reading sky emission: %s\n",
            ERROR_PREFIX, pSky->Emiss.str_filename);
    return 1;
  }

  //interpolate the sky vectors in (airmass,skybright) space to the requested conditions
  if ( condlist_struct_setup ( (condlist_struct*) pCondList, (sky_struct*) pSky , (BOOL) pSim->debug ))
  {
    fprintf(stderr, "%s I had problems setting up the observing conditions\n",
            ERROR_PREFIX);
    return 1;
  }

  //set up the spectrograph+arm structures
  if ( spectrograph_struct_setup ( (spectrograph_struct*) pSpec)) //, (condlist_struct*) pCondList ))
  {
    fprintf(stderr, "%s I had problems setting up the spectrograph: %s\n",
            ERROR_PREFIX, pSpec->str_name);
    return 1;
  }

  //prepare the sky flux vectors and sky transmission vectors for each condition and arm
  if ( fold_sky_through_spectrograph ((spectrograph_struct*) pSpec,  (condlist_struct*) pCondList))
  {
    fprintf(stderr, "%s I had problems calculating sky background photon flux vectors\n", ERROR_PREFIX);
    return 1;
  }

  if ( spectrograph_struct_write_response ( (spectrograph_struct*) pSpec, (sim_struct*) pSim ))
  {
    fprintf(stderr, "%s I had problems writing out the spectrograph setup files\n",
            ERROR_PREFIX);
    return 1;
  }

  //---------------------------------------------------------------------------------------


  //---------------------------------------------------------------------------------------
  //todo - only read the rulelist when necessary
  //read the file listing the rules
  if ( rulelist_struct_read_from_file ( (rulelist_struct*) pRuleList))
  {
    fprintf(stderr, "%s I had problems reading rule list: %s\n",
            ERROR_PREFIX, pRuleList->str_filename);
    return 1;
  }

  //todo - only read the rulesetlist when necessary
  //read the file listing the rulesets
  if ( rulesetlist_struct_read_from_file ( (rulesetlist_struct*) pRuleSetList, (rulelist_struct*) pRuleList))
  {
    fprintf(stderr, "%s I had problems reading ruleset list: %s\n",
            ERROR_PREFIX, pRuleSetList->str_filename);
    return 1;
  }

  //prepare the machinery to evaluate user-defined rulset expressions 
  if ( rulesetlist_struct_setup_expressions ( (rulesetlist_struct*) pRuleSetList, (rulelist_struct*) pRuleList))
  {
    fprintf(stderr, "%s I had problems setting up ruleset list expressions\n", ERROR_PREFIX);
    return 1;
  }



  
  //read the file listing the model templates
  if ( templatelist_struct_read_from_file ( (templatelist_struct*) pTL , (BOOL) pSim->debug))
  {
    fprintf(stderr, "%s I had problems reading template list: %s\n",
            ERROR_PREFIX, pTL->str_filename);
    return 1;
  }



  {
    int i;
    //find the longest template name etc
    for ( i = 0; i<pTL->num_templates; i++)
    {
      template_struct *pTemplate = (template_struct*) &(pTL->pTemplate[i]);
      if ( strlen(pTemplate->str_name)     > pSim->longest_template_name )
      {
        pSim->longest_template_name     = (int) strlen(pTemplate->str_name) ;
      }
      if ( strlen(pTemplate->str_filename) > pSim->longest_template_filename )
      {
        pSim->longest_template_filename = (int) strlen(pTemplate->str_filename) ;
      }
      if ( strlen(pTemplate->str_ruleset)  > pSim->longest_ruleset_name )
      {
        pSim->longest_ruleset_name      = (int) strlen(pTemplate->str_ruleset) ;
      }
    }
  }



  
  //fold the templates through the normalising filter
  if ( fold_templatelist_through_norm_filters ( (sim_struct*) pSim, (templatelist_struct*) pTL,
                                                (int) pSim->num_filters, (curve_struct*) pSim->crvNormFilterList ))
  {
    fprintf(stderr, "%s I had problems folding the templates through the normalising filters\n", ERROR_PREFIX);
    return 1;
  }
  
  
  //match the templates with their corresponding rulesets
  if ( associate_templatelist_with_rulesetlist( (templatelist_struct*) pTL, (rulesetlist_struct*) pRuleSetList ))
  {
    fprintf(stderr, "%s I had problems associating templatelist with rulesetlist\n", ERROR_PREFIX);
    return 1;
  }
  //---------------------------------------------------------------------------------------

  //---------------------------------------------------------------------------------------
  if ( templatelist_struct_prepare_rebinning ( (templatelist_struct*) pTL,
                                               (spectrograph_struct*) pSpec))
  {
    fprintf(stderr, "%s I had problems preparing the rule-dependent rebinning parameters for templatelist\n", ERROR_PREFIX);
    return 1;
  }
  //---------------------------------------------------------------------------------------

  
//  ///DEBUGGERY
//  strncpy(pTL->pTemplate[0].crvModel.str_filename, "!test.fits", STR_MAX); 
//  if ( curve_struct_write_to_fits_table ( (curve_struct*) &(pTL->pTemplate[0].crvModel) ))
//  {
//    fprintf(stderr, "%s I had problems writing curve: %s\n",
//            ERROR_PREFIX, pTL->pTemplate[0].str_filename);
//    return 1;
//  }

  fprintf(stdout, "%s PROCESSING_TIME End of setup phase. It took %.3f seconds\n", COMMENT_PREFIX,
          (double) (clock() - clocks_main_start)/((double) CLOCKS_PER_SEC)); 

  if ( work_on_templatelist ((templatelist_struct*) pTL,
                             (spectrograph_struct*) pSpec,
                             (condlist_struct*)     pCondList,
                             (sim_struct*)          pSim))

  {
    fprintf(stderr, "%s I had problems when working on templatelist\n", ERROR_PREFIX);
    return 1;
  }

  
  
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  //html summary pages not yet implemented
//  if ( write_summary_page_html ((param_list_struct*) pParamList, 
//                                (const char*) "index.html") )
//  {
//    fprintf(stderr, "%s Problem writing html summary page\n", ERROR_PREFIX);
//    return 1;
//  }


  //do some tidying up
  if ( ParLib_free_param_list((param_list_struct**) &pParamList))
    fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);

  
  if (ETC_input_params_struct_free((ETC_input_params_struct**) &pIpar         ) )
  {
    fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
  }
  if (spectrograph_struct_free((spectrograph_struct**) &pSpec       ) ||
      sky_struct_free         ((sky_struct**)          &pSky        ) ||
      condlist_struct_free    ((condlist_struct**)     &pCondList   ) ||
      rulelist_struct_free    ((rulelist_struct**)     &pRuleList   ) ||
      rulesetlist_struct_free ((rulesetlist_struct**)  &pRuleSetList) ||
      templatelist_struct_free((templatelist_struct**) &pTL         ))
  {
    fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
  }
  

  if ( pSim->pSummaryFile )
  {
    fflush(pSim->pSummaryFile);
    fclose(pSim->pSummaryFile);
    pSim->pSummaryFile = NULL;
    
    fprintf(stdout, "%s Output ASCII summary file can be found here: \"%s\"\n",
            COMMENT_PREFIX,pSim->str_summary_filename); fflush(stdout);

  }
  if ( pSim->pSummaryFile_fits )
  {
    int status = 0;
    fits_close_file(pSim->pSummaryFile_fits, &status);
    fprintf(stdout, "%s Output FITS summary file can be found here: \"%s\"\n",
            COMMENT_PREFIX,pSim->str_summary_filename_fits); fflush(stdout);

  }
  
  if ( sim_struct_free ((sim_struct**) &pSim ))
    fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);

  
  fflush(stdout);
  fprintf(stdout, "%s PROCESSING_TIME End of processing. It took %.3lf seconds\n", COMMENT_PREFIX,
          (double) ((clock_t) clock() - (clock_t) clocks_main_start)/((double) CLOCKS_PER_SEC)); 
  //  fprintf(stdout, "%s Please wait a few seconds for background processes to finish\n", COMMENT_PREFIX);
  fflush(stdout);
  return 0;
}//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////end main end_main endmain end of main////////////////////////////////////
/////////////////////////////////// END OF MAIN /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////






/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// FUNCTIONS //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
int FourFS_ETC_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

int print_version_and_inputs( FILE* pstream, int argc, char **argv)
{
  int i;
  //print out the command line and program details
  //some of these values are set by CVS
  if ( pstream == NULL ) return 1;
  //  if ( _4FS_ETC_print_version ((FILE*) pstream)) return 1;

  fprintf(stdout, "%s The command line (%d args) was: ", COMMENT_PREFIX, argc );
  for ( i=0;i<argc;i++) 
  {
    fprintf(stdout, "%s ", argv[i]);
  }
  fprintf(stdout, "\n"); fflush(stdout);
  return 0;
}


int print_full_version_information_to_file(const char* str_filename)
{
  FILE *pFile = NULL;

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  if ( print_full_version_information((FILE*) pFile))
  {
    fprintf(stderr, "%s  I had problems writing to the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }
 
  fflush(pFile);
  if ( pFile ) fclose(pFile); pFile = NULL;
  return 0;
}


int print_full_version_information(FILE *pFile)
{

  if ( pFile == NULL ) return 1;
  fprintf(pFile, "%s Full software version information:\n", COMMENT_PREFIX);
  if ( FourFS_ETC_print_version              ((FILE*) pFile)) return 1;
  if ( FourFS_ETC_print_version_header       ((FILE*) pFile)) return 1;
  if ( ETC_input_params_print_version        ((FILE*) pFile)) return 1;
  if ( ETC_input_params_print_version_header ((FILE*) pFile)) return 1;
  if ( ParLib_print_version                  ((FILE*) pFile)) return 1;
  if ( ParLib_print_version_header           ((FILE*) pFile)) return 1;
  if ( util_print_version                    ((FILE*) pFile)) return 1;
  if ( util_print_version_header             ((FILE*) pFile)) return 1;

  return 0;
}


int print_full_help_information(FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile, "%s Printing full help information as requested:\n", COMMENT_PREFIX);
  fflush(pFile);
  if ( ETC_input_params_print_usage_information(pFile)) return 1;
  return 0;
}


//write a handy html summary page
//This is currently empty of content
int write_summary_page_html (param_list_struct *pParamList,
                             const char* str_filename)
{
  FILE *pFile = NULL;
  char str_cwd[STR_MAX];
  if ( pParamList == NULL ) return 1;

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  //get the working directory path
  if ( getcwd((char *) str_cwd, (size_t) STR_MAX) == NULL )
  {
    fprintf(stderr, "%s Could not get CWD at (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    (void) fflush(stderr); 
    return 1;
  }

  fprintf(stdout, "%s Writing HTML summary file to: file://%s/%s\n",
          COMMENT_PREFIX, str_cwd, str_filename); fflush(stdout);
  //write HTML header inc style sheet
  fprintf(pFile, "<!DOCTYPE html>\n\
<html>\n\
<head>\n\
  <title>4FS TS results summary page</title>\n\
  <style type='text/css'>\n\
    body {font-size:80%%;}\n\
    h1 {font-size:2.5em;}\n\
    h2 {font-size:1.875em;}\n\
    p  {font-size:0.875em;}\n\
    tr {background-color:#F0F0F0;}\n\
    td {font-size:0.800em;}\n\
    #header-row  {background-color:#A0A0A0; font-size:1.20em;}\n\
    #special-row {background-color:#E0E0E0;}\n\
  </style>\n\
  <meta name='description' content='A simple summary page detailing one run of the 4MOST Facility Simulator Throughput Simulator (4FS TS)' />\n\
  <link rel='icon' type='img/gif' href='http://www.mpe.mpg.de/~tdwelly/4MOSTlogo_icon.gif' />\n\
</head>\n");

  fprintf(pFile, "<body>\n\
<img src='http://www.aip.de/en/research/research-area-ea/research-groups-and-projects/milky-way/overview-1/4MOSTLogo.png' height='120' alt='4MOST logo' style='float:left;' />\n\
<img src='http://www2011.mpe.mpg.de/PIFICONS/logo-mpe-transparent.gif' height='100' alt='4MOST logo' style='float:right;' />\n\
<h1>4FS TS results summary page</h1>\n");

  {
    time_t rawtime;
    struct tm * timeinfo;
    struct utsname unameinfo;
    char str_hostname[STR_MAX] = {'\0'};
    //    char str_domainname[STR_MAX] = {'\0'};
    
    if ( time ( &rawtime ) < 0) return 1;
    timeinfo = localtime ( &rawtime );

    if ( gethostname((char*) str_hostname, (size_t) STR_MAX)) return 1;
    //    if ( getdomainname((char*) str_domainname, (size_t) STR_MAX)) return 1;
    if ( uname((struct utsname*) &unameinfo)) return 1;

    fprintf(pFile, "<h3>Simulation finish date/time: %s</h3>\n\
<h3>Simulation run on host: %s (%s %s %s %s) </h3>\n",
            asctime(timeinfo),
            str_hostname, //str_domainname,
            unameinfo.sysname, unameinfo.release, unameinfo.version, unameinfo.machine);
  }

  {
    char buffer[STR_MAX] = {'\0'};
    if ( ParLib_get_param_str_value ((param_list_struct*) pParamList,
                                     (const char*) "PARAM_FILENAME", (char*) buffer) > 0 ) return 1;

    fprintf(pFile,"<h2> Input files/parameters</h2>\n\
<a href='%s' target='_blank' type='text/plain' title='The input parameter file that was supplied to the TS'>Full Input Parameter file</a><br>\n\
<a href='%s' target='_blank' type='text/plain' title='The array of input parameters after interpretation by the TS'>Input parameter array after interpretation</a>\n\
<br><br>\n",
            buffer,  "ETC_interpreted_parameters.txt");
  }


  fprintf(pFile, "<table border=0>\n\
  <tr id='header-row'>\n\
    <td>Selected Parameter Name</td>\n\
    <td>Value</td>\n\
    <td>Units</td>\n\
  </tr>\n");
  
  //  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "TELESCOPE.FOV",             "",  "deg<sup>2</sup>")) return 1; 

  fprintf(pFile, "</table>\n");

   {
    time_t rawtime;
    struct tm *timeinfo;
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, " <hr>\n\
\n\
A standard product of the <span style='color:blue;'>%s</span>. Written at %s. \n\
Contact <a href='mailto:%s?Subject=4FS%%20TS'>%s</a> for further information.</p>\n\
</body>\n\
</html>\n\n\n",
            ETC_BRANDING_STRING,
            asctime(timeinfo), CONTACT_EMAIL_ADDRESS, CONTACT_EMAIL_ADDRESS);
  }
  fflush(pFile);

  if ( pFile ) fclose(pFile); pFile = NULL;
  
  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------
//template_struct functions
int template_struct_init ( template_struct *pTemplate )
{
  int f;
  if ( pTemplate == NULL ) return 1;
  
  strncpy(pTemplate->str_name,     "", STR_MAX);
  strncpy(pTemplate->str_filename, "", STR_MAX);
  strncpy(pTemplate->str_ruleset,  "", STR_MAX);
  //  pTemplate->fom_type = FOM_TYPE_CODE_UNKNOWN;
  pTemplate->pRuleSet = NULL;
  
  pTemplate->object_fwhm = (double) NAN;
  pTemplate->mag0        = (double) NAN;
  for(f=0;f<SIM_MAX_FILTERS;f++)
  {
    pTemplate->mag_norm[f]  = (double) NAN;
  }
  strncpy(pTemplate->str_magsteps,  "", LSTR_MAX);
  pTemplate->mag_min     = (double) NAN;
  pTemplate->mag_max     = (double) NAN;
  pTemplate->mag_step    = (double) NAN;
  pTemplate->pmag_list   = NULL;

  pTemplate->redshift    = (double) NAN;
  if ( curve_struct_init ( (curve_struct*) &(pTemplate->crvModel))) return 1;

  pTemplate->num_rebin = 0;
  pTemplate->pRebin = NULL;

  return 0;
}

int template_struct_free ( template_struct *pTemplate )
{
  int r;
  if ( pTemplate == NULL ) return 1;
  if ( pTemplate->pmag_list ) free(pTemplate->pmag_list); pTemplate->pmag_list = NULL;
  if ( curve_struct_free ( (curve_struct*) &(pTemplate->crvModel))) return 1;

  if ( pTemplate->pRebin )
  {
    for(r=0;r<pTemplate->num_rebin;r++)
    {
      if ( rebin_struct_free((rebin_struct*) &(pTemplate->pRebin[r]))) return 1;
    }
    free(pTemplate->pRebin);
    pTemplate->pRebin = NULL;
  }
  return 0;
}

int templatelist_struct_init ( templatelist_struct **ppTL )
{
  if ( ppTL == NULL ) return 1;
  if ( *ppTL == NULL )
  {
    //make space for the struct
    if ( (*ppTL = (templatelist_struct*) malloc(sizeof(templatelist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  strncpy((*ppTL)->str_name,     "", STR_MAX);
  strncpy((*ppTL)->str_filename, "", STR_MAX);
  (*ppTL)->num_templates = (int) 0;
  (*ppTL)->array_length  = (int) 0;
  (*ppTL)->pTemplate  = NULL;
  //  (*ppTL)->longest_template_name = (int) 0;

  return 0;
}

int templatelist_struct_free ( templatelist_struct **ppTL )
{
  if ( ppTL == NULL ) return 1;
  if ( *ppTL )
  {
    if ( (*ppTL)->pTemplate )
    {
      int i;
      for ( i=0; i < (*ppTL)->num_templates; i++)
      {
        if ( template_struct_free ( (template_struct*) &((*ppTL)->pTemplate[i]))) return 1;
      }
      
      free((*ppTL)->pTemplate);
      (*ppTL)->pTemplate = NULL;
    }
    free(*ppTL);
    *ppTL = NULL;
  }
  return 0;
}

// go through the input 
// Format of input file is:
// OBJNAME  FILENAME  FOMcode   FWHM(arcsec) redshift mag0(r-band,AB) [mag_range (min:max:step)] 
// all fields are required
int templatelist_struct_read_from_file ( templatelist_struct *pTL , BOOL verbose )
{
  FILE *pFile = NULL;
  long i;
  char buffer[STR_MAX];
  if ( pTL == NULL ) return 1;
  
  //open the file for reading
  if ((pFile = fopen(pTL->str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the file: %s\n", ERROR_PREFIX, pTL->str_filename);
    fflush(stderr);
    return 1;
  }

  if(verbose)
  {
    fprintf(stdout, "%s Reading template list from file: \"%s\"\n", DEBUG_PREFIX, pTL->str_filename);
    fflush(stdout);
  }
    
  //scan through the file to determine the number of interesting lines
  i = 0;
  while (fgets((char*) buffer, (int) STR_MAX, (FILE*) pFile))
  {
    int j = 0;
    buffer[STR_MAX - 1] = '\0'; //safety stop, but this should be added by fgets()
    while( isspace(buffer[j])) j ++; //scan past leading white space
    if ( buffer[j] != '#' &&
         buffer[j] != '\0' &&
         strlen(&(buffer[j])) )  //ignore comment and blank lines
    {
      i ++;
    }
  }
  //go back to the start
  rewind((FILE*)pFile);
  pTL->num_templates = i;
 
  if(verbose)
  {
    fprintf(stdout, "%s Template list file contains %d potentially interesting line%s:\n",
            DEBUG_PREFIX, pTL->num_templates, N2S(pTL->num_templates)); fflush(stdout);
  }
  if ( pTL->num_templates <= 0 )
  {
    fprintf(stderr, "%s I found zero valid lines in the template list file: %s\n",
            ERROR_PREFIX, pTL->str_filename);
    fflush(stderr);
    return 1;
  }

  
  //allocate and init space for templates
  if ((pTL->pTemplate = (template_struct*) malloc(sizeof(template_struct) * pTL->num_templates)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  for ( i = 0; i< pTL->num_templates; i++)
  {
    if ( template_struct_init ( (template_struct*) &(pTL->pTemplate[i])))
    {
      return 1;
    }
  }

  

  //now read in the data
  {
    i = 0;
    if(verbose)
    {
      fprintf(stdout, "%s #TEMPLATE %-24s %-80s %-12s %5s %5s %5s (%5s:%5s:%5s;%4s)\n", 
              DEBUG_PREFIX, 
              (char*)  "NAME",
              (char*)  "FILENAME",
              (char*)  "RULESET",
              (char*)  "FWHM\"",
              (char*)  "z",
              (char*)  "mag0",
              (char*)  "magLo",
              (char*)  "magHi",
              (char*)  "magSt",
              (char*)  "Nmag");
    }
    
    
    int line = 0;
    while (fgets((char*) buffer, (int) STR_MAX, (FILE*) pFile))
    {
      int j = 0;
      template_struct *pTemplate = (template_struct*)  &(pTL->pTemplate[i]);
      line ++;
      buffer[STR_MAX - 1] = '\0'; //safety stop
      while( isspace(buffer[j])) j ++; //scan past leading white space
      if ( j < STR_MAX )
      {
        if ( buffer[j] != '#' && strlen(&(buffer[j])) )  //ignore comment and blank lines
        {
          const char* str_format = "%s %s %s %lf %lf %lf %s"; 
          int n_tokens1 = 0;
          int n_tokens2 = -1;
          n_tokens1 = sscanf(buffer, (const char*) str_format,
                             (char*)     pTemplate->str_name,
                             (char*)     pTemplate->str_filename,
                             (char*)     pTemplate->str_ruleset,
                             (double*) &(pTemplate->object_fwhm),
                             (double*) &(pTemplate->redshift),
                             (double*) &(pTemplate->mag0),
                             (char*)     pTemplate->str_magsteps);

          
          if ( n_tokens1 == 6 || n_tokens1 == 7 ) 
          {
            pTemplate->index = i;
            if ( n_tokens1 == 7 ) //need to interpret the supplied mag range
            {
              char str_format[LSTR_MAX];
//              //need to be able to interpret both of these formats:
//              // 1) min:max:step
//              // 2) mag1,mag2,mag3...magn
//              // count the number of commas and colons
//              int ncommas = 0;
//              int ncolons = 0;
//              for(i=0;i<strlen(pTemplate->str_magsteps);i++)
//              {
//                if      (pTemplate->str_magsteps[i] == (char) ',') ncommas++;
//                else if (pTemplate->str_magsteps[i] == (char) ':') ncolons++;
//              }
//              if ( ncolons == 0 && ncommas == 0 )
//              {
//                sprintf(str_format, "%lf");
//              }
//              else if ( ncommas > 0 )
//              {
//                for(i=0;i<ncommas;i++)
//                {
//                  sprintf(str_format+4*i, (i>0?",":" "), "%lf");
//                }
//              }
//              else
//              {
                sprintf(str_format, "%%lf:%%lf:%%lf");
//              }

              n_tokens2 = sscanf(pTemplate->str_magsteps,
                                 (const char*) str_format,
                                 &(pTemplate->mag_min),
                                 &(pTemplate->mag_max),
                                 &(pTemplate->mag_step));
            }

            if ( n_tokens2 == -1 ) // no range given, so simulate just one magnitude
            {
              pTemplate->mag_min  = pTemplate->mag0;
              pTemplate->mag_max  = pTemplate->mag0;
              pTemplate->mag_step = (double) 1.0;
            }
            else if ( n_tokens2 == 1 ) //simulate at just one magnitude
            {
              pTemplate->mag_max  = pTemplate->mag_min;
              pTemplate->mag_step = (double) 1.0;
            }
            else if ( n_tokens2 == 2 ) //simulate at between mag_min,mag_max with 1 mag steps
            {
              pTemplate->mag_step = (double) 1.0;
            }
            else if ( n_tokens2 == 3 ) //do nothing, as we have all the required info
            {
            }
            else                       //bad number of tokens
            {
              fprintf(stderr, "%s Problem reading line %d of template file, bad magsteps: \"%s\"\n",
                      WARNING_PREFIX, line, pTemplate->str_magsteps); 
            }

            //check that we don't end up in an infinite loop
            if ( fabs(pTemplate->mag_step) <= MIN_MAG_STEP )
            {
              fprintf(stdout, "%s Tweaking a suspitiously small mag_step value (%g) on line %d of template file\n",
                      WARNING_PREFIX, pTemplate->mag_step, line); 
              if ( pTemplate->mag_step < 0.0 ) pTemplate->mag_step = -1.0 * MIN_MAG_STEP;
              else                             pTemplate->mag_step = MIN_MAG_STEP;
            }
            
            //check if this was a valid row
            if ( n_tokens2 == -1 || n_tokens2 == 1 || n_tokens2 == 2 || n_tokens2 == 3 )
            {
              double mag;
              int m;

              //count the number of mags that we need
              pTemplate->num_mag = 0;
              for(mag  = pTemplate->mag_min;
                  mag <= pTemplate->mag_max;
                  mag += pTemplate->mag_step) 
                pTemplate->num_mag ++;
              pTemplate->num_mag = MAX(1, pTemplate->num_mag);
              
              if ( (pTemplate->pmag_list = (double*) malloc(sizeof(double) * pTemplate->num_mag)) == NULL )
              {
                fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
                return 1;    
              }
              mag = pTemplate->mag_min;
              for(m=0;m<pTemplate->num_mag;m++)
              {
                pTemplate->pmag_list[m] = (double) mag;
                mag += pTemplate->mag_step; 
              }

              
              if(verbose)
              {
                fprintf(stdout, "%s  TEMPLATE %-24s %-80s %-12s %5g %5g %5g (%5g:%5g:%5g;%4d)\n", 
                        DEBUG_PREFIX, 
                        (char*)  pTemplate->str_name,
                        (char*)  pTemplate->str_filename,
                        (char*)  pTemplate->str_ruleset,
                        (double) pTemplate->object_fwhm,
                        (double) pTemplate->redshift,
                        (double) pTemplate->mag0,
                        (double) pTemplate->mag_min,
                        (double) pTemplate->mag_max,
                        (double) pTemplate->mag_step,
                        (int)    pTemplate->num_mag);
              }
              i ++;  
            }
            else
            {
              fprintf(stderr, "%s Problem reading line %d of template file (n_tokens1=%d,n_tokens2=%d): \"%s\"\n",
                      WARNING_PREFIX, line, n_tokens1, n_tokens2, buffer); 
              
            }
          }
          else
          {
            fprintf(stderr, "%s Problem reading line %d of template file (n_tokens1=%d,n_tokens2=%d): \"%s\"\n",
                    WARNING_PREFIX, line, n_tokens1, n_tokens2, buffer); 
          }
        }
      }
    }
  }


  //close the file
  if ( pFile ) fclose ( pFile) ;
  pFile = NULL;

  
  fprintf(stdout, "%s Template list file contained %ld valid line%s:\n", COMMENT_PREFIX, i, N2S(i));fflush(stdout);
          //  fprintf(stdout, "%s %s contained %ld (valid) lines\n", DEBUG_PREFIX, pTL->str_filename, i); fflush(stdout);
  if ( i <= 0 )
  {
    fprintf(stderr, "%s I read zero valid template lines from the file: %s\n", ERROR_PREFIX, pTL->str_filename);
    fflush(stderr);
    return 1;
  }

 
  //tidy up 
  if ( i < pTL->num_templates )
  {
    pTL->num_templates = i;
    //allocate space for data
    if ((pTL->pTemplate = (template_struct*) realloc((template_struct*) pTL->pTemplate,
                                                     sizeof(template_struct) * pTL->num_templates)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

//  //find the longest template name
//  for ( i = 0; i< pTL->num_templates; i++)
//  {
//    template_struct *pTemplate = (template_struct*) &(pTL->pTemplate[i]);
//    if ( strlen(pTemplate->str_name) > pTL->longest_template_name )
//    {
//      pTL->longest_template_name = (int) strlen(pTemplate->str_name) ;
//    }
//  }

  //now read the template curves for each template
  //TODO - move this to just before folding each template through the response - for bulk processing
  for ( i = 0; i< pTL->num_templates; i++)
  {
    template_struct *pTemplate = (template_struct*) &(pTL->pTemplate[i]);
    strncpy(pTemplate->crvModel.str_filename, pTemplate->str_filename, STR_MAX);
    if ( curve_struct_read_from_file ( (curve_struct*) &(pTemplate->crvModel), (BOOL) verbose))
    {
      return 1;
    }

    //assume that ascii curves have units Flambda(erg/cm2/s/A)
    if ( pTemplate->crvModel.unit_type_y == CURVE_UNIT_TYPE_DIMENSIONLESS )
    {
      pTemplate->crvModel.unit_type_y = CURVE_UNIT_TYPE_FE_LAMBDA;
      pTemplate->crvModel.unit_y      = 1.0;
    }

  }
 
  return 0;
}
  
//---------------------------------------------------------------------------------------------------


//interpolate the filter to the template resolution, then 
//calculate the flux of an AB=0 source versus the flux of the template
// Magnitude is calculated from the ratio of detected photons (i.e. counts) between the 
// template spectrum fiolded through the filter bandpass and the reference spectrum (AB=0) 
// folded through the same filter bandpass.
// Algorithm:
// 1) interpolate filter curve to wavelength pixel scale in template
// 2) convert template spectrum into ph/cm2/s/AA
// 3) convert an AB = 0 spectrum into ph
// 4) convert both to ct/cm2/s/AA by mutiplying by interpolated filter
// 5) take ratio of the summed fluxes over bandpass
// 6) convert to a magnitude
// now calculate the number of photons expected through the bandpass
//
int template_struct_calc_mag_through_filter(template_struct* pTemplate, curve_struct* pcrvFilter, double *presult)
{
  long i;
  double sum_ph_AB0      = 0.0; 
  double sum_ph_template = 0.0;
  curve_struct *pcrvModel = NULL;
  double filter_start_AA = NAN;  //wavelength of first non-zero entry
  double filter_stop_AA  = NAN;  //wavelength of last non-zero entry
  if ( pTemplate  == NULL ||
       pcrvFilter == NULL ) return 1;
  pcrvModel = (curve_struct*) &(pTemplate->crvModel);


  if ( pcrvModel->min_x*pcrvModel->unit_x > pcrvFilter->max_x*pcrvFilter->unit_x ||
       pcrvModel->max_x*pcrvModel->unit_x < pcrvFilter->min_x*pcrvFilter->unit_x )
  {
    //no overlap between filter and model wavelength ranges
    fprintf(stderr, "%s Zero       wavelength overlap between template: \"%s\" and filter: \"%s\" (t=[%g:%g]AA f=[%g:%g]AA)\n",
            ERROR_PREFIX,
            pTemplate->str_filename,
            pcrvFilter->str_name,
            pcrvModel->min_x*pcrvModel->unit_x,   pcrvModel->max_x*pcrvModel->unit_x,
            pcrvFilter->min_x*pcrvFilter->unit_x, pcrvFilter->max_x*pcrvFilter->unit_x);
    *presult = (double) NAN;
    return 0;
  }
  //TODO add more checks on inputs

//   
//   if ( pcrvModel->min_x*pcrvModel->unit_x > pcrvFilter->max_x*pcrvFilter->unit_x ||
//        pcrvModel->max_x*pcrvModel->unit_x < pcrvFilter->min_x*pcrvFilter->unit_x )
//   {
//     //no overlap between filter and model wavelength ranges
//     fprintf(stderr, "%s Zero wavelength overlap between template: \"%s\" and filter: \"%s\" (t=[%g:%g]AA f=[%g:%g]AA)\n",
//             ERROR_PREFIX,
//             pTemplate->str_filename,
//             pcrvFilter->str_name,
//             pcrvModel->min_x*pcrvModel->unit_x,   pcrvModel->max_x*pcrvModel->unit_x,
//             pcrvFilter->min_x*pcrvFilter->unit_x, pcrvFilter->max_x*pcrvFilter->unit_x);
//     *presult = (double) NAN;
//     return 0;
//   }

  // search for first and last non-zero entries in filter curve
  for (i=0;i<pcrvFilter->num_rows;i++) //calc wavelength of first non-zero entry
  {
    if (pcrvFilter->py[i] > 0.0 )
    {
      filter_start_AA = pcrvFilter->px[i] / pcrvFilter->unit_x;
      break;
    }
  }
  for (i=pcrvFilter->num_rows-1;i>0;i--) //calc wavelength of last non-zero entry
  {
    if (pcrvFilter->py[i] > 0.0 )
    {
      filter_stop_AA = pcrvFilter->px[i] / pcrvFilter->unit_x;
      break;
    }
  }


  // check for, and warn about, any cases wher there is incomplete overlap
  // between filter and model wavelength ranges
  if ( pcrvModel->min_x*pcrvModel->unit_x > filter_start_AA ||
       pcrvModel->max_x*pcrvModel->unit_x < filter_stop_AA )
  {
    fprintf(stderr, "%s Incomplete wavelength overlap between template: \"%s\" and filter: \"%s\" (t=[%g:%g]AA f=[%g:%g]AA)\n",
            WARNING_PREFIX,
            pTemplate->str_filename,
            pcrvFilter->str_name,
            pcrvModel->min_x*pcrvModel->unit_x,   pcrvModel->max_x*pcrvModel->unit_x,
            filter_start_AA, filter_stop_AA);
    //    pTemplate->mag_norm = NAN;
    *presult = (double) NAN;
    return 0;
  }

    
  for (i=0;i<pcrvModel->num_rows;i++)
  {
    // Notes:
    // E = h * nu
    // nu = c / lambda
    // E = h * c / lambda
    //
    // lambda*f_lambda = nu*f_nu
    // f_nu = f_lambda*(lambda/nu) = f_lambda*lambda^2/c.
    // f_lambda = f_nu * c / lambda^2
    double l_AA        = pcrvModel->px[i]*pcrvModel->unit_x;
    double fl_template = pcrvModel->py[i]*pcrvModel->unit_y;
    double fph_template;
    double l_lo;
    double l_hi;
    double dl_AA;
    double npix;
    //    double dnu_Hz;
    double phot_per_erg;
    double filter_qe;  //interp from qe curve
    double fl_AB0, fph_AB0;
    //step along until reach start of filter
    if ( l_AA < filter_start_AA ) continue; 
    //check that haven't gone past end of filter
    if ( l_AA > filter_stop_AA ) break; 

    if (i==0)
    {
      l_lo=pcrvModel->px[i]  *pcrvModel->unit_x;
      l_hi=pcrvModel->px[i+1]*pcrvModel->unit_x;
      npix=1.0;
    }   
    else if (i==pcrvModel->num_rows-1)
    {
      l_lo=pcrvModel->px[i-1]*pcrvModel->unit_x;
      l_hi=pcrvModel->px[i]  *pcrvModel->unit_x;
      npix=1.0;
    }     
    else
    {
      l_lo=pcrvModel->px[i-1]*pcrvModel->unit_x;
      l_hi=pcrvModel->px[i+1]*pcrvModel->unit_x;
      npix=2.0;
    }    

    //get the QE of the filter at this lambda:
    if ( util_interp_linear_1D_doubles((double*) pcrvFilter->px, (double*) pcrvFilter->py, (long) pcrvFilter->num_rows,
                                       (double) l_AA/pcrvFilter->unit_x, (double*) &(filter_qe)) == 1) return 1;
    if ( filter_qe < 0.0 ) filter_qe = 0.0;
    
    dl_AA        = (l_hi-l_lo)/npix;                  
    //    dnu_Hz       = (CONVERT_M_TO_AA*CONSTANT_SPEED_OF_LIGHT)*(1.0/l_lo-1.0/l_hi)/npix;   
    phot_per_erg = (l_AA*CONVERT_AA_TO_M)/(CONVERT_J_TO_ERG*CONSTANT_PLANCK*CONSTANT_SPEED_OF_LIGHT); 
    fph_template = phot_per_erg*filter_qe*fl_template*dl_AA; 
    fl_AB0       = CONVERT_JY_TO_CGS*CONVERT_AA_TO_M*CONSTANT_SPEED_OF_LIGHT/(SQR(l_AA*CONVERT_AA_TO_M)); 
    fph_AB0=phot_per_erg*filter_qe*fl_AB0*dl_AA; 

    sum_ph_template+=fph_template;
    sum_ph_AB0     +=fph_AB0;

  }
  
  if ( sum_ph_AB0  <= 0.0 )
  {
    fprintf(stderr, "%s Summed AB photon count < 0 (sum_ph_AB0=%g)\n", ERROR_PREFIX, sum_ph_AB0);
    return 1;
  }
  else if (sum_ph_template>0.0)
  {
    //    pTemplate->mag_norm = (double) -2.5*log10(sum_ph_template/sum_ph_AB0);
    *presult = (double) -2.5*log10(sum_ph_template/sum_ph_AB0);
  }
  else
  {
    //    pTemplate->mag_norm = (double) 99.99;
    *presult =  (double) 99.99;
  }


  return 0;
}


//---------------------------------------------------------------------------------------------------
//lambda_struct functions
int lambda_struct_init ( lambda_struct *pLambda )
{
  if ( pLambda == NULL ) return 1;
  
  strncpy(pLambda->str_type, "UNKNOWN", STR_MAX);
  pLambda->type = LAMBDA_TYPE_CODE_UNKNOWN;    
  pLambda->num_pix = -1;                
  pLambda->refpix = (double) NAN;      
  pLambda->refval = (double) NAN;
  pLambda->disp = (double) NAN ;    
  if ( curve_struct_init ( (curve_struct*) &(pLambda->crvFile))) return 1;
  
  pLambda->pl  = NULL;       
  pLambda->pdl  = NULL;       
  pLambda->pres_pix = NULL;    
  pLambda->min = (double) NAN;      
  pLambda->max = (double) NAN;      

  return 0;
}

int lambda_struct_free ( lambda_struct *pLambda )
{
  if ( pLambda == NULL ) return 1;

  if ( curve_struct_free ( (curve_struct*) &(pLambda->crvFile))) return 1;

  if ( pLambda->pl ) free(pLambda->pl);
  pLambda->pl = NULL;
  if ( pLambda->pdl ) free(pLambda->pdl);
  pLambda->pdl = NULL;
  if ( pLambda->pres_pix) free(pLambda->pres_pix);
  pLambda->pres_pix = NULL;
  return 0;
}

//---------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
//response_struct functions
int response_struct_init ( response_struct *pResponse )
{
  if ( pResponse == NULL ) return 1;
  
  pResponse->num_rows = (long) 0;
  pResponse->num_cols = (long) 0;
  pResponse->plmid    = NULL;
  pResponse->pdl      = NULL;
  pResponse->pfirst   = NULL;
  pResponse->plast    = NULL;
  pResponse->pres     = NULL;    
  pResponse->parf     = NULL;    
  pResponse->pprmf     = NULL;    
  //  pResponse->prmf     = NULL;    

  return 0;
}

int response_struct_free ( response_struct *pResponse )
{
  if ( pResponse == NULL ) return 1;

  if ( pResponse->plmid   ) free(pResponse->plmid   ); pResponse->plmid    = NULL;
  if ( pResponse->pdl     ) free(pResponse->pdl     ); pResponse->pdl      = NULL;
  if ( pResponse->pfirst  ) free(pResponse->pfirst  ); pResponse->pfirst   = NULL;
  if ( pResponse->plast   ) free(pResponse->plast   ); pResponse->plast    = NULL;
  if ( pResponse->pres    ) free(pResponse->pres    ); pResponse->pres     = NULL;
  if ( pResponse->parf    ) free(pResponse->parf    ); pResponse->parf     = NULL;
  //  if ( pResponse->prmf    ) free(pResponse->prmf    ); pResponse->prmf     = NULL;
  if ( pResponse->pprmf   )
  {
    int i;
    for(i=0;i<pResponse->num_rows;i++)
    {
      if ( pResponse->pprmf[i]) {free(pResponse->pprmf[i]); pResponse->pprmf[i] = NULL;}
    }
    free(pResponse->pprmf   );
    pResponse->pprmf    = NULL;
  }

  //  if ( pResponse->pres_pix) free(pResponse->pres_pix); pResponse->pres_pix = NULL;
  return 0;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
//fibrecoupling_struct functions
int fibrecoupling_struct_init ( fibrecoupling_struct *pFibrecoupling )
{
  if ( pFibrecoupling == NULL ) return 1;

  pFibrecoupling->type_code            = (int) FIBRECOUPLING_TYPE_CODE_UNKNOWN;
  pFibrecoupling->frac_science         = (double) NAN;
  pFibrecoupling->frac_sky             = (double) NAN;
  pFibrecoupling->seeing_profile_code  = (int) SEEING_PROFILE_CODE_UNKNOWN;
  strncpy(pFibrecoupling->str_filename, "", STR_MAX);
  //  pFibrecoupling->ntilt = 0;
  pFibrecoupling->ptilt = NULL;
  //  pFibrecoupling->nIQ = 0;
  pFibrecoupling->pIQ = NULL;
  //  pFibrecoupling->nmisalign = 0;
  pFibrecoupling->pmisalign = NULL;
  pFibrecoupling->pTrans = NULL;
  pFibrecoupling->num_curves = 0;

  pFibrecoupling->tilt_min = 0.0;
  pFibrecoupling->tilt_max = 0.0;
  pFibrecoupling->IQ_min   = 0.0;  
  pFibrecoupling->IQ_max   = 0.0;
  pFibrecoupling->misalign_min  = 0.0;
  pFibrecoupling->misalign_max  = 0.0;
  return 0;
}

int fibrecoupling_struct_free ( fibrecoupling_struct *pFibrecoupling )
{
  if ( pFibrecoupling == NULL ) return 1;
  
  FREETONULL(pFibrecoupling->ptilt);
  FREETONULL(pFibrecoupling->pIQ);
  FREETONULL(pFibrecoupling->pmisalign);
  FREETONULL(pFibrecoupling->pTrans);

  return 0;
}

//read in a fiber coupling struct from a fits file
//expected format is a binary table
int fibrecoupling_struct_read_from_file ( fibrecoupling_struct *pFibrecoupling, BOOL verbose )
{
  fitsfile *pFile = NULL;
  int status = 0;
  int num_cols;
  int colnum_tilt, colnum_iq, colnum_mis, colnum_trans;
  long i;
  if ( pFibrecoupling == NULL ) return 1;

  //open the file for reading
  if ( fits_open_table((fitsfile**) &pFile, pFibrecoupling->str_filename, READONLY, &status))
  {
    fits_report_error(stderr, status);  //DEBUG
    return 1;
  }

  if ( verbose) {
    fprintf(stdout, "%s %s Reading fiber coupling matrix from file\n",
            DEBUG_PREFIX, pFibrecoupling->str_filename); fflush(stdout);
  }
  if ( fits_get_num_rows ((fitsfile*) pFile, (long*) &(pFibrecoupling->num_curves), &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( verbose) {
    fprintf(stdout, "%s %s contains %ld rows\n", DEBUG_PREFIX,
            pFibrecoupling->str_filename, pFibrecoupling->num_curves); fflush(stdout);
  }
  if ( pFibrecoupling->num_curves <= 0 )
  {
    fprintf(stderr, "%s I found zero valid rows in the fits file: %s\n",
            ERROR_PREFIX, pFibrecoupling->str_filename);fflush(stderr);
    return 1;
  }


  
  //work out the index of the columns we need to read
  if ( fits_get_num_cols ((fitsfile*) pFile, &num_cols, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "TILT", &colnum_tilt, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "IMAGE_QUALITY", &colnum_iq, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "MISALIGNMENT", &colnum_mis, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) "TRANSMISSION", &colnum_trans, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }


  //allocate space for data
  if ((pFibrecoupling->ptilt = (double*) malloc(sizeof(double) * pFibrecoupling->num_curves)) == NULL ||
      (pFibrecoupling->pIQ   = (double*) malloc(sizeof(double) * pFibrecoupling->num_curves)) == NULL ||
      (pFibrecoupling->pmisalign  = (double*) malloc(sizeof(double) * pFibrecoupling->num_curves)) == NULL ||
      (pFibrecoupling->pTrans= (double*) malloc(sizeof(double) * pFibrecoupling->num_curves)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //now read the data - do each row in turn - for future where each trans is a curve in lambda 
  for ( i=0; i<pFibrecoupling->num_curves; i++ )
  {
    fits_read_col(pFile, TDOUBLE, colnum_tilt,  i+1, 1, 1, NULL, (double*) &(pFibrecoupling->ptilt[i]),  NULL, &status);
    fits_read_col(pFile, TDOUBLE, colnum_iq,    i+1, 1, 1, NULL, (double*) &(pFibrecoupling->pIQ[i]),    NULL, &status);
    fits_read_col(pFile, TDOUBLE, colnum_mis,   i+1, 1, 1, NULL, (double*) &(pFibrecoupling->pmisalign[i]),   NULL, &status);
    fits_read_col(pFile, TDOUBLE, colnum_trans, i+1, 1, 1, NULL, (double*) &(pFibrecoupling->pTrans[i]), NULL, &status);
   
    if ( status)
    {
      fits_report_error(stderr, status);
      return 1;
    }
  }

  //tidy up
  status = 0;
  fits_close_file(pFile, &status);

  //TODO - get a list of unique values for each param 

  //now calc some stats
  pFibrecoupling->tilt_min =  DBL_MAX;
  pFibrecoupling->tilt_max = -DBL_MAX;
  pFibrecoupling->IQ_min   =  DBL_MAX;  
  pFibrecoupling->IQ_max   = -DBL_MAX;
  pFibrecoupling->misalign_min  =  DBL_MAX;
  pFibrecoupling->misalign_max  = -DBL_MAX;
  for ( i=0; i<pFibrecoupling->num_curves; i++ )
  {
    if ( pFibrecoupling->ptilt[i] < pFibrecoupling->tilt_min ) pFibrecoupling->tilt_min = pFibrecoupling->ptilt[i];
    if ( pFibrecoupling->ptilt[i] > pFibrecoupling->tilt_max ) pFibrecoupling->tilt_max = pFibrecoupling->ptilt[i];
    if ( pFibrecoupling->pIQ[i]   < pFibrecoupling->IQ_min   ) pFibrecoupling->IQ_min   = pFibrecoupling->pIQ[i];
    if ( pFibrecoupling->pIQ[i]   > pFibrecoupling->IQ_max   ) pFibrecoupling->IQ_max   = pFibrecoupling->pIQ[i];
    if ( pFibrecoupling->pmisalign[i]  < pFibrecoupling->misalign_min  ) pFibrecoupling->misalign_min  = pFibrecoupling->pmisalign[i];
    if ( pFibrecoupling->pmisalign[i]  > pFibrecoupling->misalign_max  ) pFibrecoupling->misalign_max  = pFibrecoupling->pmisalign[i];
  }

  if ( verbose) {
    fprintf(stdout, "%s Fiber coupling matrix:     tilt[min:max]=[%8g:%8g] mm\n", DEBUG_PREFIX, pFibrecoupling->tilt_min,pFibrecoupling->tilt_max);
    fprintf(stdout, "%s Fiber coupling matrix:       IQ[min:max]=[%8g:%8g] arcsec\n", DEBUG_PREFIX, pFibrecoupling->IQ_min,  pFibrecoupling->IQ_max);
    fprintf(stdout, "%s Fiber coupling matrix: misalign[min:max]=[%8g:%8g] arcsec\n", DEBUG_PREFIX, pFibrecoupling->misalign_min, pFibrecoupling->misalign_max);
  }
  return 0;
}

// find the nearest fibrecoupling entry to the requested tilt,IQ,misalign 
// calculate the minimum distance in 3D param space,
// normalised to the full ranges of the tabulated tilt,IQ,misalign values
int fibrecoupling_struct_find_nearest ( fibrecoupling_struct *pFibrecoupling,
                                        cond_struct          *pCond         ,
                                        BOOL                  print_flag)
{
  int i;
  double best_sq_dist = DBL_MAX;
  int best_i = -1;
  double scale_tilt;
  double scale_IQ;
  double scale_misalign;
  if ( pFibrecoupling == NULL ||
       pCond          == NULL ) return 1;

  pCond->fibrecoupling_factor = NAN;
  pCond->index_fibrecoupling  = -1;

  if ( pFibrecoupling->type_code == FIBRECOUPLING_TYPE_CODE_FIXED ||
       pFibrecoupling->type_code == FIBRECOUPLING_TYPE_CODE_NONE )
  {
    pCond->fibrecoupling_factor = pFibrecoupling->frac_science;
    pCond->index_fibrecoupling = -1;
    if ( print_flag ) 
    {
      fprintf(stdout,
            "%s Using fixed fiber coupling factor => %6.4f\n",
            COMMENT_PREFIX,
            pFibrecoupling->frac_science);
    }
    return 0;
  }
  
  if ( pCond->tilt     < pFibrecoupling->tilt_min ||
       pCond->tilt     > pFibrecoupling->tilt_max ||
       pCond->IQ       < pFibrecoupling->IQ_min   ||
       pCond->IQ       > pFibrecoupling->IQ_max   ||
       pCond->misalign      < pFibrecoupling->misalign_min  ||
       pCond->misalign      > pFibrecoupling->misalign_max  )
  {
    fprintf(stderr,
            "%s Requested fibrecoupling params [tilt,IQ,misalign]=[%5g,%5g,%5g] => outside covered range: [tilt,IQ,misalign] in [%g:%g][%g:%g][%g:%g]\n",
            ERROR_PREFIX,
            pCond->tilt,
            pCond->IQ,
            pCond->misalign,
            pFibrecoupling->tilt_min, pFibrecoupling->tilt_max,
            pFibrecoupling->IQ_min,   pFibrecoupling->IQ_max,
            pFibrecoupling->misalign_min,  pFibrecoupling->misalign_max);
    return 1;
  }
  
  scale_tilt = pFibrecoupling->tilt_max - pFibrecoupling->tilt_min;
  scale_IQ   = pFibrecoupling->IQ_max   - pFibrecoupling->IQ_min;
  scale_misalign  = pFibrecoupling->misalign_max  - pFibrecoupling->misalign_min;
  if ( scale_tilt > 0.0 ) scale_tilt = 1.0 / scale_tilt;
  if ( scale_IQ   > 0.0 ) scale_IQ   = 1.0 / scale_IQ;
  if ( scale_misalign  > 0.0 ) scale_misalign  = 1.0 / scale_misalign;
  
  for ( i=0; i<pFibrecoupling->num_curves; i++ )
  {
    double sq_dist =
      SQR((pCond->tilt - pFibrecoupling->ptilt[i])*scale_tilt) +
      SQR((pCond->IQ   - pFibrecoupling->pIQ[i]  )*scale_IQ  ) +
      SQR((pCond->misalign  - pFibrecoupling->pmisalign[i] )*scale_misalign );
    if ( sq_dist < best_sq_dist )
    {
      best_sq_dist = sq_dist;
      best_i = i;
    }
  }
  if ( best_i < 0 || best_i >= pFibrecoupling->num_curves) 
  {
     fprintf(stderr,
             "%s Error selecting best fibrecoupling entry (index = %d)\n",
             ERROR_PREFIX, best_i);
    return 1;
  }
  pCond->fibrecoupling_factor = pFibrecoupling->pTrans[best_i];
  pCond->index_fibrecoupling  = best_i;

  
  if ( print_flag ) 
  {
    fprintf(stdout,
            "%s Requested fibrecoupling params [tilt,IQ,misalign]=[%5g,%5g,%5g] => found nearest [tilt,IQ,misalign]=[%5g,%5g,%5g] =>Trans=%6.4f\n",
            COMMENT_PREFIX,
            pCond->tilt, pCond->IQ, pCond->misalign,
            pFibrecoupling->ptilt[best_i],
            pFibrecoupling->pIQ[best_i],
            pFibrecoupling->pmisalign[best_i],
            pFibrecoupling->pTrans[best_i]);
  }


  
  return 0;
}

//---------------------------------------------------------------------------------------------------
//arm_struct functions
int arm_struct_init ( arm_struct *pArm )
{
  if ( pArm == NULL ) return 1;
  strncpy(pArm->str_name, "", STR_MAX);
  pArm->aper_size      = (double) NAN;
  pArm->aper_eef       = (double) NAN;
  pArm->peak_pix_frac       = (double) NAN;
  //  pArm->spatial_fwhm   = (double) NAN;
  pArm->pixel_size     = (double) NAN;
  pArm->read_noise     = (double) NAN;
  pArm->sq_read_noise  = (double) NAN;
  pArm->dark_current_h = (double) NAN;
  pArm->dark_current_s = (double) NAN;
  pArm->full_well      = (double) NAN;
  pArm->binning_disp   = (int) -1;
  pArm->binning_cross  = (int) -1;

  pArm->fiber_diam     = (double) NAN;
  pArm->fiber_area     = (double) NAN;
  pArm->effective_area = (double) NAN;
  pArm->skysub_residual =  (double) NAN;


  
  pArm->num_sky = 0;
  pArm->psky_emiss = (double*) NULL;

  if ( curve_struct_init ( (curve_struct*) &(pArm->crvTput))) return 1;
  if ( curve_struct_init ( (curve_struct*) &(pArm->crvRes))) return 1;

  if ( lambda_struct_init ( (lambda_struct*) &(pArm->Lambda))) return 1;
  if ( response_struct_init ( (response_struct*) &(pArm->Rsp))) return 1;

  return 0;
}

int arm_struct_free ( arm_struct *pArm )
{
  if ( pArm == NULL ) return 1;

  if ( curve_struct_free ( (curve_struct*) &(pArm->crvTput))) return 1;
  if ( curve_struct_free ( (curve_struct*) &(pArm->crvRes)))  return 1;

  if ( lambda_struct_free ( (lambda_struct*) &(pArm->Lambda))) return 1;
  if ( response_struct_free ( (response_struct*) &(pArm->Rsp))) return 1;
  FREETONULL(pArm->psky_emiss);
  return 0;
}

//prepare an arm struct for use - this is after the input parameters have all been read in
int arm_struct_setup ( arm_struct *pArm) //, condlist_struct *pCondList )
{
  long i;
  lambda_struct *pLambda = NULL;
  response_struct *pRsp = NULL;
  double *pbuffer = NULL;
  //  double *pres = NULL;
  if ( pArm == NULL ) return 1; //|| pCondList == NULL) return 1;
  fprintf(stdout, "%s Setting up spectrograph arm: %s\n", COMMENT_PREFIX, pArm->str_name); fflush(stdout);

  pRsp = (response_struct*) &(pArm->Rsp);
  if ( pRsp == NULL ) return 1;
  pLambda = (lambda_struct*) &(pArm->Lambda);
  if ( pLambda == NULL ) return 1;

  //build a response function from crvRes,crvArea, - i.e. a 2-d response matrix
  // 
  //for the time being we will sample the wavelength direction at the minimal spacing which contains all of the
  //input calibration data
  //and we will use the full number of spectral pixels in the dispersion direction


  //use a scheme where the resolution is measured several times per pixel step 
  //  fprintf(stderr, "num_pix = %ld\n", pLambda->num_pix);
  pRsp->num_rows = pLambda->num_pix * RESPONSE_MATRIX_OVERSAMPLING;
  //  fprintf(stderr, "num_pix = %ld num_rows=%ld\n", pLambda->num_pix, pRsp->num_rows);
  assert (pRsp->num_rows > 0);
  if ( (pRsp->plmid  = (double*) malloc(sizeof(double) * pRsp->num_rows)) == NULL ||
       (pRsp->pdl    = (double*) malloc(sizeof(double) * pRsp->num_rows)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;    
  }

  for(i=0;i<pRsp->num_rows;i++)
  {
    double frac_pix = (double) i / (double)  RESPONSE_MATRIX_OVERSAMPLING;
    long jmin = (long) floor(frac_pix);
    long jmax = (long) jmin + 1;
    double lmin, lmax;
    if ( jmax < pLambda->num_pix )
    {
      lmin = pLambda->pl[jmin];
      lmax = pLambda->pl[jmax];
    }
    else //deal with the last few steps - 
    {
      lmin = pLambda->pl[jmin];
      //      lmax = pLambda->pl[jmin] + 2.0*(pLambda->pl[jmin] - pLambda->pl[jmin-1]) ;  // TODO check what this extra step factor (2.0) is for, and if it is correct.
      lmax = pLambda->pl[jmin] + 1.0*(pLambda->pl[jmin] - pLambda->pl[jmin-1]) ;  // TODO check what this extra step factor (2.0) is for, and if it is correct.
    }
    pRsp->plmid[i] = util_interp_linear_1D_double ((double) (jmin), (double) lmin,
                                                   (double) (jmax), (double) lmax,  frac_pix);
    //    fprintf(stdout, "%s PLMID %s %6ld %8.3f %6ld %6ld %12.4f %12.4f -> %12.4f\n", DEBUG_PREFIX,
    //            pArm->str_name, i, frac_pix, jmin, jmax, lmin, lmax,pRsp->plmid[i]) ;    fflush(stdout);
    //    pRsp->plmid[i] = pLambda->pl[i];
  }

  //calculate the delta_l which each step covers
  for(i=0;i<pRsp->num_rows;i++)
  {
    if ( i == 0 )
      pRsp->pdl[i] = pRsp->plmid[i+1] - pRsp->plmid[i];
    else if ( i == (pRsp->num_rows - 1) )
      pRsp->pdl[i] = (pRsp->plmid[i] - pRsp->plmid[i-1]);
    else
      pRsp->pdl[i] = 0.5 * (pRsp->plmid[i+1] - pRsp->plmid[i-1]);
  }


  
  //make space for the resampled res and arf arrays
  if ( (pRsp->pres     = (double*) malloc(sizeof(double) * pRsp->num_rows)) == NULL ||
       (pRsp->parf     = (double*) calloc(sizeof(double), pRsp->num_rows)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;    
  }
  
  //now calculate the res and arf at each wavelength step (interpolate)
  //remember that the wavelength scales may be different (so divide test wavelength by pArm->crvTput.unit_x)
  for(i=0;i<pRsp->num_rows;i++)
  {
    double scaled_wavelength_res  = (double) pRsp->plmid[i]/pArm->crvRes.unit_x;
    double scaled_wavelength_tput = (double) pRsp->plmid[i]/pArm->crvTput.unit_x;
    if ( util_interp_linear_1D_doubles((double*) pArm->crvRes.px, (double*) pArm->crvRes.py, (long) pArm->crvRes.num_rows,
                                       (double) scaled_wavelength_res,  (double*) &(pRsp->pres[i])) == 1) return 1;
    if ( util_interp_linear_1D_doubles((double*) pArm->crvTput.px, (double*) pArm->crvTput.py, (long) pArm->crvTput.num_rows,
                                       (double) scaled_wavelength_tput, (double*) &(pRsp->parf[i])) == 1) return 1;

    //multiply the arf by the telescope effective collecting area 
    pRsp->parf[i] *= (pArm->effective_area * CONVERT_M2_TO_CM2);

    //and multiply by the fractional light enclosed in the extraction aperture    
    pRsp->parf[i] *= pArm->aper_eef;
    
    //    fprintf(stdout, "%s RSP %s %6ld %8.3f %12.5g %12.5g\n", DEBUG_PREFIX,
    //            pArm->str_name, i, pRsp->plmid[i], pRsp->pres[i], pRsp->parf[i]);
  }

  
  
  pRsp->num_cols =  (long) pLambda->num_pix;
  fprintf(stdout, "%s Spectrograph arm %s : response matrix has dimensions %ldx%ld\n",
          COMMENT_PREFIX, pArm->str_name, pRsp->num_rows, pRsp->num_cols);
  //          sizeof(double) * pRsp->num_rows * pRsp->num_cols / (double) (1024.0 * 1024.0));
  fflush(stdout);
  assert ( pRsp->num_rows > 0 && pRsp->num_cols > 0);

  //make space for the other arrays 
  if ( (pRsp->pfirst = (long*)   calloc(sizeof(long)  , pRsp->num_rows)) == NULL ||
       (pRsp->plast  = (long*)   calloc(sizeof(long)  , pRsp->num_rows)) == NULL ||
       //       (pRsp->prmf   = (double*) calloc(sizeof(double), pRsp->num_cols * pRsp->num_rows)) == NULL ||
       (pRsp->pprmf  = (double**) malloc(sizeof(double*) * pRsp->num_rows)) == NULL ||
       (pbuffer      = (double*)  malloc(sizeof(double) * pRsp->num_cols)) == NULL)
    //       (pRsp->prmf   = (double*) calloc(sizeof(double), pRsp->num_rows * pRsp->num_cols )) == NULL)
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;    
  }

  //  fprintf(stdout, "DEBUG_SIZE sizeof(pRsp) = %ld MB\n", sizeof(pRsp)/(1024*1024)); 
  
  //now go through row by row and build up the pprmf matrix
  for (i=0;i<pRsp->num_rows;i++)
  {
    const double gauss_fwhm_to_sigma = (double) 1.0 / ((double)2.0 * sqrt((double)2.0 * log((double)2.0))); // ~1/2.35
    long j;
    double pixmid;
    long   jmin,jmax,jmid;
    double gauss_fwhm;
    double gauss_sigma;
    long ncols;
    

    pRsp->pprmf[i] = NULL;
    
    //It's fastest to use a binary search algorithm
    if ( util_binary_search_double ((long) pLambda->num_pix, (double*) pLambda->pl, (double) pRsp->plmid[i],
                                    (long*) &jmin, (long*) &jmax) < 0 ) return 1;
    pixmid = util_interp_linear_1D_double ((double) pLambda->pl[jmin], (double) (jmin),
                                           (double) pLambda->pl[jmax], (double) (jmax), pRsp->plmid[i]);
     
    //res is lambda/dlambda with dlambda=FWHM so convert to sigma
    assert ( pRsp->pres[i] > (double)0.0 ); 
    gauss_fwhm = pRsp->plmid[i]/pRsp->pres[i];
    gauss_sigma = gauss_fwhm * gauss_fwhm_to_sigma;
    //    gauss_sigma = gauss_fwhm / ((double)2.0 * sqrt((double)2.0 * log((double)2.0)));
    //    gauss_norm = 1.0 / (gauss_sigma * sqrt(2.0 * M_PI));
    //    minus_half_over_sq_gauss_sigma = -0.5 / (gauss_sigma * gauss_sigma);
//
//    fprintf(stdout, "%s RSP %s %6ld %8.3f %8ld %8ld %12.5g %12.5g = %12.5g\n", DEBUG_PREFIX,
//            pArm->str_name, i, pRsp->plmid[i], j1-1, j1, pLambda->pl[j1-1], pLambda->pl[j1], pixmid);
    
//    for (j=0;j<pRsp->num_cols;j++)
//    {
//      //assume a Gaussian
//      pRsp->prsp[i*pRsp->num_cols + j] = gauss_norm * exp (-0.5*(pow((pRsp->plmid[i] - pLambda->pl[j])/gauss_sigma, 2.0)));   
//    }

    //assume a Gaussian profile
    //first count upwards
    jmid = MAX(0, (long) floor(pixmid));
    for (j=jmid;j<pRsp->num_cols;j++)
    {
      //      pRsp->prmf[i*pRsp->num_cols + j] =
      pbuffer[j] =
        util_integrate_gaussian_over_interval (pRsp->plmid[i], gauss_sigma,
                                               (pLambda->pl[j]-(double)0.5*pLambda->pdl[j]),
                                               (pLambda->pl[j]+(double)0.5*pLambda->pdl[j]));
      //      pRsp->prmf[i*pRsp->num_cols + j] = pbuffer[j];
      
      //      if ( pRsp->prmf[i*pRsp->num_cols + j] < RSP_MIN_RESPONSE_VALUE ) break;
      if ( pbuffer[j] < RSP_MIN_RESPONSE_VALUE ) break;
    }
    pRsp->plast[i] = MIN(j,pRsp->num_cols-1);

    //now count downwards
    for (j=jmid-1;j>=0;j--)
    {
      //      pRsp->prmf[i*pRsp->num_cols + j] =
      pbuffer[j] =
        util_integrate_gaussian_over_interval (pRsp->plmid[i], gauss_sigma,
                                               (pLambda->pl[j]-(double)0.5*pLambda->pdl[j]),
                                               (pLambda->pl[j]+(double)0.5*pLambda->pdl[j]));
      //      pRsp->prmf[i*pRsp->num_cols + j] = pbuffer[j];

      //      if ( pRsp->prmf[i*pRsp->num_cols + j] < RSP_MIN_RESPONSE_VALUE ) break;
      if ( pbuffer[j] < RSP_MIN_RESPONSE_VALUE ) break;
    }
    pRsp->pfirst[i] = MAX(j,0);
    //    fprintf(stdout, "%s RSP %s %6ld %8.3f %12.3f [%6ld,%6ld] %10.3f %8.3f %10.5g\n", DEBUG_PREFIX,
    //            pArm->str_name, i, pRsp->plmid[i], pixmid, pRsp->pfirst[i], pRsp->plast[i], pRsp->pres[i], gauss_sigma, ptput[i]); fflush(stdout);
    // DEBUG     pRsp->prmf[i*pRsp->num_cols + pRsp->pfirst[i]] = -1.0;
    // DEBUG     pRsp->prmf[i*pRsp->num_cols + pRsp->plast[i]] = -1.0;


    //allocate some space for the columns needed for this row
    ncols = (long) 1 + pRsp->plast[i] - pRsp->pfirst[i];
    if ((pRsp->pprmf[i] = (double*) malloc(sizeof(double) * ncols)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
    //and copy over the useful section of the response
    //    for(j=0;j<ncols;j++)
    //    {
    //      pRsp->pprmf[i][j] = pbuffer[pRsp->pfirst[i] + j];
    //    }
    for(j=pRsp->pfirst[i];j<=pRsp->plast[i];j++)
      pRsp->pprmf[i][j-pRsp->pfirst[i]] = pbuffer[j];
    

  }


  //record the width of a resolution element in pixels
  //  if ( TRUE == FALSE )
  {
    int j;
    assert(pLambda->num_pix > 0);
    //make space for the resolution array
    if ( (pLambda->pres_pix = (double*) malloc(sizeof(double) * pLambda->num_pix)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  
    for(j=0;j<pLambda->num_pix;j++)
    {
      double res;
      double scaled_wavelength = (double) pLambda->pl[j]/pArm->crvRes.unit_x;
      if ( util_interp_linear_1D_doubles((double*) pArm->crvRes.px, (double*) pArm->crvRes.py, (long) pArm->crvRes.num_rows,
                                         (double) scaled_wavelength, (double*) &(res)) == 1) return 1;
      assert(res >  (double) 0.0 );
      assert (pLambda->pdl[j] > (double) 0.0);
      pLambda->pres_pix[j] =  pLambda->pl[j] / ( res * pLambda->pdl[j]);
    }
  }

  FREETONULL(pbuffer);
  
  return 0;
}
//---------------------------------------------------------------------------------------------------

int arm_struct_write_rmf_to_file ( arm_struct *pArm , char* str_filename )
{
  fitsfile *pFile = NULL;
  int status = 0;
  int naxis = 2;
  long naxes[2];
  long fpixel[2];
  response_struct *pRsp = NULL;
  double *pbuffer = NULL;
  long i;
  if ( pArm == NULL || strlen(str_filename) == 0 ) return 1;
  pRsp = (response_struct*) &(pArm->Rsp);
  if ( pRsp == NULL ) return 1;

  fprintf(stdout, "%s Writing spectrograph response matrix (rmf) for arm %s: %s\n",
          COMMENT_PREFIX, pArm->str_name, str_filename); fflush(stdout);

  if ( fits_create_file ((fitsfile**) &pFile, (char*) str_filename, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  naxes[1] = pRsp->num_rows;  //used to be other way round before
  naxes[0] = pRsp->num_cols;
  if ( fits_create_img((fitsfile*) pFile, (int) FLOAT_IMG, (int) naxis, (long*) naxes, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  if ( (pbuffer  = (double*) calloc(sizeof(double), pRsp->num_rows * pRsp->num_cols)) == NULL)
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;    
  }
  
  //TODO write the rmf as a variable length array - OGIP standard
  for (i=0;i<pRsp->num_rows;i++)
  {
    long j;
    for(j=0;j<pRsp->num_cols;j++)
    {
      if ( j >= pRsp->pfirst[i] &&
           j <= pRsp->plast[i] )
        pbuffer[i*pRsp->num_cols + j] = pRsp->pprmf[i][j - pRsp->pfirst[i]];
    }
  }
  fpixel[0] = 1;
  fpixel[1] = 1;
  if ( fits_write_pix ((fitsfile*) pFile, (int) TDOUBLE, (long*) fpixel, (LONGLONG) pRsp->num_rows * pRsp->num_cols,
                       (double*) pbuffer, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  

  
  fits_close_file(pFile, &status);
  FREETONULL(pbuffer);
  return 0;
}

//write all the per-pixel details to file
int arm_struct_write_arf_to_file ( arm_struct *pArm , char* str_filename )
{
  fitsfile *pFile = NULL;
  int status = 0;
  const char *ttype[] = {"LAMBDA", "DELTAL", "SPECRESP", "RESOLUTION"};
  const char *tform[] = {"1D", "1E", "1E", "1E" };
  const char *tunit[] = {"Angstrom", "Angstrom/pix", "cm^2", "lambda/dlambda"};
  const int colnum[] = { 1 , 2, 3, 4};
  const int ncols = (int) (sizeof(colnum)/sizeof(int));
  response_struct *pRsp = NULL;
  long i,nrows_per_chunk;
  
  if ( pArm == NULL || strlen(str_filename) == 0 ) return 1;
  pRsp = (response_struct*) &(pArm->Rsp);
  if ( pRsp == NULL ) return 1;

  fprintf(stdout, "%s Writing spectrograph ancillary response file (arf) for arm %s: %s\n",
          COMMENT_PREFIX, pArm->str_name, str_filename); fflush(stdout);

  if ( fits_create_file ((fitsfile**) &pFile, (char*) str_filename, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  if (  fits_create_tbl ((fitsfile*) pFile, (int) BINARY_TBL,
                         (LONGLONG) pRsp->num_rows, (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) "SPECRESP", (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  if ( fits_get_rowsize((fitsfile*) pFile, &nrows_per_chunk, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  for(i=0;i<pRsp->num_rows; i+= nrows_per_chunk )
  {
    long nrows_this_chunk = MIN(nrows_per_chunk, pRsp->num_rows-i);
    
    fits_write_col ((fitsfile*) pFile, (int) TDOUBLE, (int) colnum[0], (LONGLONG) i+1,
                    (LONGLONG) 1, (LONGLONG) nrows_this_chunk, &(pRsp->plmid[i]), (int*) &status);
    fits_write_col ((fitsfile*) pFile, (int) TDOUBLE, (int) colnum[1], (LONGLONG) i+1,
                    (LONGLONG) 1, (LONGLONG) nrows_this_chunk, &(pRsp->pdl[i]), (int*) &status);
    fits_write_col ((fitsfile*) pFile, (int) TDOUBLE, (int) colnum[2], (LONGLONG) i+1,
                    (LONGLONG) 1, (LONGLONG) nrows_this_chunk, &(pRsp->parf[i]), (int*) &status);
    fits_write_col ((fitsfile*) pFile, (int) TDOUBLE, (int) colnum[3], (LONGLONG) i+1,
                    (LONGLONG) 1, (LONGLONG) nrows_this_chunk, &(pRsp->pres[i]), (int*) &status);
   
    
    if ( status ) 
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }

  
  fits_close_file(pFile, &status);
 
  return 0;
}


int spectrograph_struct_write_response ( spectrograph_struct *pSpec, sim_struct* pSim )
{
  char str_buffer[STR_MAX];
  int a;
  if ( pSpec == NULL ||
       pSim == NULL ) return 1;
  for(a=0;a<pSpec->num_arms;a++)
  {
    arm_struct *pArm = (arm_struct*) &(pSpec->pArm[a]);
    if ( pSim->output & SIM_OUTPUT_CODE_RMF )
    {
      //write out response matrix as an image
      sprintf(str_buffer, "%s%s/response_matrix_rmf_arm_%s.fits",
              CLOBBER2S(pSim->clobber), pSim->str_outdir, pArm->str_name);
      if ( arm_struct_write_rmf_to_file ( (arm_struct*) pArm,  (char*) str_buffer) )
      {
        fprintf(stderr, "%s Problem writing response matrix (rmf) to file: %s\n", ERROR_PREFIX, str_buffer);
        return 1;
      }
    }
    if (  pSim->output & SIM_OUTPUT_CODE_ARF )
    {
      //write out arf as a file
      sprintf(str_buffer, "%s%s/response_matrix_arf_arm_%s.fits",
              CLOBBER2S(pSim->clobber), pSim->str_outdir, pArm->str_name);
      if ( arm_struct_write_arf_to_file ( (arm_struct*) pArm,  (char*) str_buffer) )
      {
        fprintf(stderr, "%s Problem writing response matrix (arf) to file: %s\n", ERROR_PREFIX, str_buffer);
        return 1;
      }
    }
  }
  return 0;
}







//---------------------------------------------------------------------------------------------------
//spectrograph_struct functions
int spectrograph_struct_init ( spectrograph_struct **ppSpec )
{
  if ( ppSpec == NULL ) return 1;
  if ( *ppSpec == NULL ) 
  {
    //make space for the struct
    if ( (*ppSpec = (spectrograph_struct*) malloc(sizeof(spectrograph_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  strncpy((*ppSpec)->str_name, "", STR_MAX);
  (*ppSpec)->num_arms  = (int) -1;
  (*ppSpec)->pArm = NULL;
  (*ppSpec)->lambda_min = NAN; //
  (*ppSpec)->lambda_max = NAN;
  return 0;
}

int spectrograph_struct_free ( spectrograph_struct **ppSpec )
{
  if ( ppSpec == NULL ) return 1;
  if ( *ppSpec )
  {
    if ( (*ppSpec)->pArm && (*ppSpec)->num_arms > 0 )
    {
      int i;
      for(i=0;i<(*ppSpec)->num_arms;i++)
      {
        if ( arm_struct_free ( (arm_struct*) &((*ppSpec)->pArm[i]))) return 1;
      }
      free((*ppSpec)->pArm);
      (*ppSpec)->pArm = NULL;
    }
    free(*ppSpec);
    *ppSpec = NULL;
  }
  return 0;
}

int spectrograph_struct_setup ( spectrograph_struct *pSpec)//, condlist_struct *pCondList )
{
  int a;
  if ( pSpec == NULL ) return 1;//||
  //       pCondList == NULL) return 1;
  //  for(a=0;a<pSpec->num_arms;a++)
  //  {
  //    arm_struct *pArm = (arm_struct*) &(pSpec->pArm[a]);
  //    fprintf(stderr, "AAAAAAAAAA a=%d num_pix = %ld\n", a, (pArm->Lambda).num_pix);
  //  }
  for(a=0;a<pSpec->num_arms;a++)
  {
    arm_struct *pArm = (arm_struct*) &(pSpec->pArm[a]);
    //    fprintf(stderr, "a=%d num_pix = %ld\n", a, (pArm->Lambda).num_pix);
    if ( arm_struct_setup ( (arm_struct*) pArm)) //, (condlist_struct*) pCondList))
    {
      fprintf(stderr, "%s I had problems setting up the spectrograph arm: %s\n",
              ERROR_PREFIX, pArm->str_name);
      return 1;
    }
  }
  return 0;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
// use the response matrix pArm->pRsp to convolve the spectrum in pTemplate, output written to array presult 
// need to worry about units
// expect the template/model to be supplied with defined x-axis and y-axis units
// we resample into bins of 1 pixel
// so we need to know the size of the pixel in wavelength units
// TODO - deal with cases where the curve is sampled at lower frequency than say 1/2 the spectral resolution 
int convolve_curve_with_response ( arm_struct *pArm, curve_struct *pCurve, BOOL mult_by_arf, double* presult )
{
  lambda_struct *pLambda = NULL;
  response_struct *pRsp = NULL;
  long k,i;
  //  static BOOL debug1st = TRUE;
  if ( pArm    == NULL ||
       pCurve  == NULL ||
       presult == NULL ) return 1;

  pRsp = (response_struct*) &(pArm->Rsp);
  if ( pRsp == NULL ) return 1;
  pLambda = (lambda_struct*) &(pArm->Lambda);
  if ( pLambda == NULL ) return 1;

  //  fprintf(stdout, "%s Got to here: file %s line %d\n", DEBUG_PREFIX, __FILE__, __LINE__);fflush(stdout);

  //assume that presult contains at least pLambda->num_pix steps
  //initialise to zero
  for(k=0;k<pLambda->num_pix;k++) presult[k] = (double) 0.0;
  //  fprintf(stdout, "%s Got to here: file %s line %d\n", DEBUG_PREFIX, __FILE__, __LINE__);fflush(stdout);


  //step through the response matrix, and at each wavelength step work out the average value of the curve over the interval
//  if ( debug1st )
//  {
//    fprintf(stdout, "DEBUG_FOLD %6s %8s %8s %8s  %6s  %8s %8s %8s %10s %10s %10s %10s %10s\n",
//            "i", "lmin", "lmax", "pdl[i]", "j", "li", "lii", "overlap", "py[j]", "unit_y", "curvey", "sum", "summed_overlap");
//  }

  for(i=0;i<pRsp->num_rows;i++)
  {
    long j;
    double lmin = (pRsp->plmid[i] - 0.5*pRsp->pdl[i]);  
    double lmax = (pRsp->plmid[i] + 0.5*pRsp->pdl[i]);
    double li, lii;
    double sum    = 0.0;
    double summed_overlap = 0.0;
    if ( pRsp->pdl[i] <= 0.0 ) return 1;
    if ( lmax < pCurve->min_x*pCurve->unit_x ) continue;
    if ( lmin > pCurve->max_x*pCurve->unit_x ) break;

    //find the template entry just blueward of lmin
    if ( util_binary_search_double ((long) pCurve->num_rows, (double*) pCurve->px, (double) lmin/pCurve->unit_x,
                                    (long*) &j, (long*) NULL ) < 0 ) return 1;

    if ( j < 0 ||  j >= pCurve->num_rows ) break;
    
    do
    {
      double curve_y;
      double overlap;
      if ( j == 0 )
      {
        lii = 0.5*(pCurve->px[j+1]+pCurve->px[j])    *pCurve->unit_x;
        li  = (pCurve->px[j] + (pCurve->px[j] - lii))*pCurve->unit_x; 
      }
      else if ( j == pCurve->num_rows - 1 )
      {
        li  = 0.5*(pCurve->px[j]+pCurve->px[j-1])    *pCurve->unit_x;
        lii = (pCurve->px[j] + (li - pCurve->px[j])) *pCurve->unit_x; 
      }
      else
      {
        li  = 0.5*(pCurve->px[j-1]+pCurve->px[j])  *pCurve->unit_x;
        lii = 0.5*(pCurve->px[j+1]+pCurve->px[j])  *pCurve->unit_x;
      }
      if ( pCurve->unit_type_y == CURVE_UNIT_TYPE_FE_LAMBDA ||
           pCurve->unit_type_y == CURVE_UNIT_TYPE_FE_LAMBDA_SOLID )
      {
        curve_y = pCurve->py[j] * pCurve->unit_y * CONVERT_ERG_TO_J *
          0.5*(lii+li) * CONVERT_AA_TO_M / (CONSTANT_PLANCK * CONSTANT_SPEED_OF_LIGHT);
      }
      else curve_y = pCurve->py[j] * pCurve->unit_y;
        

      //interval overlap is (minimum of the upper limits) minus (maximum of the lower limits)
      overlap = (double) (MAX(0.0, MIN(lmax, lii) - MAX(li, lmin)));
      sum += curve_y *overlap; 
      summed_overlap += overlap;

      //      if ( debug1st )
      //      {
      //        fprintf(stdout, "DEBUG_FOLD %6ld %8.3f %8.3f %8.3f  %6ld  %8.3f %8.3f %8.3f %10.5g %10.5g %10.5g %10.5g %10.5g\n",
      //                i, lmin, lmax, pRsp->pdl[i], j, li, lii, overlap, pCurve->py[j], pCurve->unit_y, curve_y, sum, summed_overlap);
      //      }
      
      j++;
    }
    while ( li < lmax && j < pCurve->num_rows); 
    
    if ( summed_overlap > 0.0 ) sum = sum / summed_overlap;

    if (  mult_by_arf )
    {
      sum = sum * pRsp->parf[i];
    }

    for ( k=pRsp->pfirst[i];k<=pRsp->plast[i];k++)
    {
      assert ( k >= 0 && k < pLambda->num_pix );
      //      presult[k] += sum * pRsp->prmf[i*pRsp->num_cols + k] * pRsp->pdl[i];
      presult[k] += sum * pRsp->pprmf[i][k - pRsp->pfirst[i]] * pRsp->pdl[i];
    }
  }
    
  return 0;
}


int convolve_curve_with_rmf_arf ( arm_struct *pArm, curve_struct *pCurve, double* presult )
{
  //  response_struct *pRsp = NULL;
  //  lambda_struct *pLambda = NULL;
  //long j;
  if ( pArm    == NULL ||
       pCurve  == NULL ||
       presult == NULL ) return 1;
  
//  pRsp = (response_struct*) &(pArm->Rsp);
//  if ( pRsp == NULL ) return 1;
//  pLambda = (lambda_struct*) &(pArm->Lambda);
//  if ( pLambda == NULL ) return 1;

  if ( convolve_curve_with_response ( (arm_struct*) pArm, (curve_struct*) pCurve,
                                      (BOOL) TRUE, (double*) presult )) return 1;

  //  //now multiply by the arf
  //  for ( j=0; j<pLambda->num_pix;j++)
  //  {
  //    presult[j] = presult[j] * pRsp->parf[j]; 
  //  }
  return 0;
}
  
//
int convolve_template_with_rmf ( arm_struct *pArm, template_struct *pTemplate, double* presult )
{
  if ( pArm == NULL ||
       pTemplate == NULL ||
       presult == NULL ) return 1;
 
  return convolve_curve_with_response ( (arm_struct*) pArm, (curve_struct*) &(pTemplate->crvModel),
                                        (BOOL) FALSE, (double*) presult );
}
//
int convolve_template_with_rmf_arf ( arm_struct *pArm, template_struct *pTemplate, double* presult )
{
  if ( pArm == NULL ||
       pTemplate == NULL ||
       presult == NULL ) return 1;
 
  return convolve_curve_with_rmf_arf ( (arm_struct*) pArm, (curve_struct*) &(pTemplate->crvModel), (double*) presult );
}
//---------------------------------------------------------------------------------------------------

//

//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
//main work functions
//work through the template list and do the required operations
int work_on_templatelist ( templatelist_struct *pTL,
                           spectrograph_struct *pSpec,
                           condlist_struct     *pCondList,
                           sim_struct          *pSim)
{

  if ( pTL       == NULL ||
       pSpec     == NULL ||
       pCondList == NULL ||
       pSim      == NULL ) return 1;
  switch ( pSim->mode )
  {
  case SIM_MODE_CODE_CALC_SNR :
    if ( work_on_templatelist_calc_snr ((templatelist_struct*) pTL,
                                        (spectrograph_struct*) pSpec,
                                        (condlist_struct*)     pCondList,
                                        (sim_struct*)          pSim))
    {
      fprintf(stderr, "%s I had problems when working on templatelist_calc_snr\n", ERROR_PREFIX);
      return 1;
    }
    break;
  case SIM_MODE_CODE_CALC_TEXP :
  case SIM_MODE_CODE_CALC_MAG :
    if ( work_on_templatelist_iter ((templatelist_struct*) pTL,
                                    (spectrograph_struct*) pSpec,
                                    (condlist_struct*)     pCondList,
                                    (sim_struct*)          pSim))
    {
      fprintf(stderr, "%s I had problems when working on templatelist_iter\n", ERROR_PREFIX);
      return 1;
    }
    break;
  default :
    fprintf(stderr, "%s Unknown simulation mode: %d\n", ERROR_PREFIX, pSim->mode );

    return 1;
    break;
  }
  return 0;
}

//---------------------------------------------------------------------------------------------------
//main work functions
//work through the template list and do the required operations
int work_on_templatelist_calc_snr ( templatelist_struct *pTL,
                                    spectrograph_struct *pSpec,
                                    condlist_struct     *pCondList,
                                    sim_struct          *pSim)
{
  int t;
  if ( pTL       == NULL ||
       pSpec     == NULL ||
       pCondList == NULL ||
       pSim      == NULL ) return 1;

 
  //////////////////////////////////////////////////////////////////////
  for (t=0;t<pTL->num_templates;t++)
  {
    template_struct *pTemplate  = (template_struct*) &(pTL->pTemplate[t]);
    simspec_struct *pSimSpec = NULL;
    int spec;

    //////////////////////////////////////////////////////////////////////
    if ( simspec_struct_init ((simspec_struct**) &pSimSpec )) return 1;
    pSimSpec->pTemplate = (template_struct*) pTemplate;
    if ( simspec_struct_alloc((simspec_struct*) pSimSpec,
                              (spectrograph_struct*) pSpec,
                              (condlist_struct*) pCondList)) return 1;
    //////////////////////////////////////////////////////////////////////
    for(spec=0;spec<pSimSpec->num_spec; spec++ )
    {
      pSimSpec->pstatus[spec] = (int) CALC_SNR_STATUS_START;
    }
    
    {
      char str_format[STR_MAX];
      snprintf(str_format, STR_MAX,
               "%%s Working on template index=%%4d/%%d name=%%-%ds: mag0=%%-6.2f mag_norm[0]=%%-6.2f Nmag=%%-4d Ncond*Ntexp=%%d z=%%-5g ruleset=%%-20s inputfile=%%s\n",
               pSim->longest_template_name);
      
      fprintf(stdout, str_format,
              COMMENT_PREFIX, t+1, pTL->num_templates,
              pTemplate->str_name,   pTemplate->mag0, pTemplate->mag_norm[0],
              pTemplate->num_mag, pSimSpec->num_spec,
              pTemplate->redshift, pTemplate->pRuleSet->str_name,
              pTemplate->str_filename ); fflush(stdout);
    }
    
    //////////////////////////////////////////////////////////////////////
    if ( calc_flux_fluence_sky_noise_snr_spectra((simspec_struct*) pSimSpec,
                                                 (template_struct*) pTemplate,
                                                 (condlist_struct*) pCondList,
                                                 (spectrograph_struct*) pSpec ))
    {
      fprintf(stderr, "%s I had problems when calling calc_flux_fluence_sky_noise_snr_spectra()\n", ERROR_PREFIX);
      return 1;
    }
    //////////////////////////////////////////////////////////////////////
    
    if ( simspec_struct_test_success ( (simspec_struct*) pSimSpec))
    {
      fprintf(stderr, "%s I had problems when calling simspec_struct_test_success()\n", ERROR_PREFIX);
      return 1;
    }

//    //now calculate the resampled spectra if necessary
//    if ( pSim->specformat & (int) SIM_SPECFORMAT_CODE_RESAMPLED )
//    {
//      if ( simspec_struct_resample ( (simspec_struct*) pSimSpec))
//      {
//        fprintf(stderr, "%s I had problems when calling simspec_struct_resample()\n", ERROR_PREFIX);
//        return 1;
//      }
//    }
      

    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //the next steps depend on the desired outputs
    //the following writes out the spectra to fits extensions
    if (  pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA )
    {
      if ( write_spec_data_to_files ((spectrograph_struct*) pSpec,
                                     (condlist_struct*)     pCondList,
                                     (template_struct*)     pTemplate,
                                     (simspec_struct*)      pSimSpec,
                                     (sim_struct*)          pSim))
      {
        fprintf(stderr, "%s I had problems when writing spec data to file\n", ERROR_PREFIX);
        return 1;
      }
    }//if we need to write SPECTRA to file
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ( pSim->output & (int) SIM_OUTPUT_CODE_SUMMARY_ASCII )
    {
      
      if ( pSim->pSummaryFile )
      {
        if ( write_summary_to_file_ascii ((FILE*) pSim->pSummaryFile, (template_struct*) pTemplate,
                                          (condlist_struct*) pCondList, (simspec_struct*) pSimSpec))
        {
          fprintf(stderr, "%s I had problems when writing ascii summary file\n", ERROR_PREFIX);
          return 1;
        }
      }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ( pSim->output & (int) SIM_OUTPUT_CODE_SUMMARY_FITS )
    {
      if ( write_summary_to_file_fits  ((sim_struct*) pSim, (template_struct*) pTemplate,
                                        (condlist_struct*) pCondList, (simspec_struct*) pSimSpec))
      {
        fprintf(stderr, "%s I had problems when writing fits summary file\n", ERROR_PREFIX);
        return 1;
      }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    if ( simspec_struct_free ( (simspec_struct**) &pSimSpec )) return 1;
  }//for each template
  //////////////////////////////////////////////////////////////////////

  return 0;
}
//---------------------------------------------------------------------------------------------------





//---------------------------------------------------------------------------------------------------
int work_on_templatelist_iter ( templatelist_struct *pTL,
                                spectrograph_struct *pSpec,
                                condlist_struct     *pCondList,
                                sim_struct          *pSim )
{
  int t;
  if ( pTL       == NULL ||
       pSpec     == NULL ||
       pCondList == NULL ||
       pSim      == NULL ) return 1;

   //////////////////////////////////////////////////////////////////////
  for (t=0;t<pTL->num_templates;t++)
  {
    int niter = 0;
    template_struct *pTemplate  = (template_struct*) &(pTL->pTemplate[t]);
    simspec_struct *pSimSpec = NULL;
    int num_search_spec;
    int spec;
    //force the number of mags to be == 1
    if ( pSim->mode == SIM_MODE_CODE_CALC_MAG ) pTemplate->num_mag = 1;
    
    //////////////////////////////////////////////////////////////////////
    if ( simspec_struct_init ((simspec_struct**) &pSimSpec )) return 1;
    pSimSpec->pTemplate = (template_struct*) pTemplate;
    if ( simspec_struct_alloc((simspec_struct*) pSimSpec,
                              (spectrograph_struct*) pSpec,
                              (condlist_struct*) pCondList)) return 1;
    for(spec=0;spec<pSimSpec->num_spec; spec++ )
    {
      if      ( pSim->mode == SIM_MODE_CODE_CALC_MAG )
        pSimSpec->pstatus[spec] = (int) CALC_MAG_STATUS_START;
      else if ( pSim->mode == SIM_MODE_CODE_CALC_TEXP )
         pSimSpec->pstatus[spec] = (int) CALC_TEXP_STATUS_START;
    }
    //////////////////////////////////////////////////////////////////////
    {
      char str_format[STR_MAX];
      
      snprintf(str_format, STR_MAX,
               "%%s Working on template index=%%4d/%%d name=%%-%ds: mag0=%%6.2f mag_norm[0]=%%-6.2f Ncond*%%s=%%d z=%%-5g ruleset=%%-20s filename=%%s\n",
               pSim->longest_template_name);
      //    fprintf(stdout, "%s Working on template index=%4d/%d name=%-18s: mag0=%6.2f Ncond*%s=%d z=%-5g ruleset=%-20s filename=%s\n",
      fprintf(stdout, str_format,
              COMMENT_PREFIX, t+1, pTL->num_templates,
              pTemplate->str_name,  pTemplate->mag0, pTemplate->mag_norm[0],
              (pSim->mode == SIM_MODE_CODE_CALC_MAG ? "Ntexp" : "Nmag"),
              pSimSpec->num_spec,
              pTemplate->redshift, pTemplate->pRuleSet->str_name,
              pTemplate->str_filename ); fflush(stdout);
    }
   
      
    num_search_spec = pSimSpec->num_spec;
    while ( num_search_spec >= 0 )
    {
      int i;
      int num_start       = 0;
      int num_converged   = 0;
      int num_search_in   = 0;
      int num_search_up   = 0;
      int num_search_down = 0;
      int num_impossible  = 0;
      niter ++;

      for ( i=0;i<pSimSpec->num_spec;i++)
      {
        switch ( pSimSpec->pstatus[i] )
        {
        case CALC_TEXP_STATUS_START       :
        case CALC_MAG_STATUS_START        :
          num_start++;
          break;
        case CALC_TEXP_STATUS_IMPOSSIBLE  :
        case CALC_MAG_STATUS_IMPOSSIBLE  :
          num_impossible++;
          break;
        case CALC_TEXP_STATUS_CONVERGED   :
        case CALC_MAG_STATUS_CONVERGED   :
          num_converged++;
          break;
        case CALC_TEXP_STATUS_SEARCH_IN   :
        case CALC_MAG_STATUS_SEARCH_IN   :
          num_search_in++;
          break;
        case CALC_TEXP_STATUS_SEARCH_UP   :
        case CALC_MAG_STATUS_SEARCH_UP   :
          num_search_up++;
          break;
        case CALC_TEXP_STATUS_SEARCH_DOWN :
        case CALC_MAG_STATUS_SEARCH_DOWN :
          num_search_down++;
          break;
        default: break;
        }
      }

      
      if ( pSim->mode == SIM_MODE_CODE_CALC_MAG )
      {
        char str_format[STR_MAX];
//        if ( niter == 1 )
//          fprintf(stdout, "%s Iter MAG   %4s/%-4s %-18s: %4s %5s %7s :: %11s %8s %8s %8s\n",
//                  COMMENT_PREFIX, "T","nTmp", "TEMPLATE", "iter", "nSpec", "nSearch",
//                  "STATUS[0]", "pmag[0]", "plo[0]", "phi[0]"); 
//        fprintf(stdout, "%s Iter MAG   %4d/%-4d %-18s: %4d %5d %7d :: %11s %8.2f %8.2f %8.2f\n",
//                COMMENT_PREFIX, t+1, pTL->num_templates,
//                pTemplate->str_name, niter, pSimSpec->num_spec, num_search_spec,
//                CALC_STATUS_STRING(pSimSpec->pstatus[0]),
//                pSimSpec->pmag[0],
//                pSimSpec->plo[0],pSimSpec->phi[0]);
        if ( niter == 1 )
        {   
      
          snprintf(str_format,STR_MAX,"%%s Iter MAG   %%4s/%%-4s %%-%ds: %%4s %%5s %%7s :: %%8s %%8s %%10s %%11s\n",
                   pSim->longest_template_name);
          fprintf(stdout, str_format,
                  COMMENT_PREFIX, "T","nTmp", "TEMPLATE", "iter", "nSpec", "nSearch",
                  "Nbracket", "Nrefine", "Nconverged", "Nimpossible" );
        }
        snprintf(str_format,STR_MAX,"%%s Iter MAG   %%4d/%%-4d %%-%ds: %%4d %%5d %%7d :: %%8d %%8d %%10d %%11d\n",
                 pSim->longest_template_name);
        fprintf(stdout, str_format,
                COMMENT_PREFIX, t+1, pTL->num_templates,
                pTemplate->str_name, niter, pSimSpec->num_spec, num_search_spec,
                num_search_up+num_search_down,
                num_search_in,
                num_converged,
                num_impossible);
      }
      else if ( pSim->mode == SIM_MODE_CODE_CALC_TEXP )
      {
        char str_format[STR_MAX];
//        if ( niter == 1 )
//          fprintf(stdout, "%s Iter TEXP  %4s/%-4s %-18s: %4s %5s %7s :: %11s %8s %8s %8s\n",
//                  COMMENT_PREFIX, "T","nTmp", "TEMPLATE", "iter", "nSpec", "nSearch",
//                  "STATUS[0]", "ptext[0]", "plo[0]", "phi[0]"); 
//        fprintf(stdout, "%s Iter TEXP  %4d/%-4d %-18s: %4d %5d %7d :: %11s %8.1f %8.1f %8.1f\n",
//                COMMENT_PREFIX, t+1, pTL->num_templates,
//                pTemplate->str_name, niter, pSimSpec->num_spec, num_search_spec,
//                CALC_STATUS_STRING(pSimSpec->pstatus[0]),
//                pSimSpec->ptexp[0],
//                pSimSpec->plo[0],pSimSpec->phi[0]);
        if ( niter == 1 )
        {
          snprintf(str_format,STR_MAX,"%%s Iter TEXP  %%4s/%%-4s %%-%ds: %%4s %%5s %%7s :: %%8s %%8s %%10s %%11s\n",
                   pSim->longest_template_name);
          fprintf(stdout, str_format,
                  COMMENT_PREFIX, "T","nTmp", "TEMPLATE", "iter", "nSpec", "nSearch",
                  "Nbracket", "Nrefine", "Nconverged", "Nimpossible" );
        }
        snprintf(str_format,STR_MAX,"%%s Iter TEXP  %%4d/%%-4d %%-%ds: %%4d %%5d %%7d :: %%8d %%8d %%10d %%11d\n",
                 pSim->longest_template_name);
        fprintf(stdout, str_format,
                COMMENT_PREFIX, t+1, pTL->num_templates,
                pTemplate->str_name, niter, pSimSpec->num_spec, num_search_spec,
                num_search_up+num_search_down,
                num_search_in,
                num_converged,
                num_impossible);
      }

      if ( niter > CALC_MAX_ITERS )
      {
        fprintf(stderr, "%s I have exceeded the maximum number of iterations(%d)\n", ERROR_PREFIX, CALC_MAX_ITERS);
        return 1;
      }

      if ( num_search_spec <= 0 ) break;   //we are finished
      
      //////////////////////////////////////////////////////////////////////
      //calculate the spectra for each condition,mag,texp
      if ( calc_flux_fluence_sky_noise_snr_spectra((simspec_struct*) pSimSpec,
                                                   (template_struct*) pTemplate,
                                                   (condlist_struct*) pCondList,
                                                   (spectrograph_struct*) pSpec ))
      {
        fprintf(stderr, "%s I had problems when calling calc_flux_fluence_sky_noise_snr_spectra()\n", ERROR_PREFIX);
        return 1;
      }
      if ( simspec_struct_test_success ( (simspec_struct*) pSimSpec))
      {
        fprintf(stderr, "%s I had problems when calling simspec_struct_test_success()\n", ERROR_PREFIX);
        return 1;
      }
      //////////////////////////////////////////////////////////////////////
      
      if ( pSim->mode == SIM_MODE_CODE_CALC_MAG )
      {
        //now update the mag_lo,mag_hi bounds and status flags for each spectrum
        if ( simspec_struct_update_mag_status ( (simspec_struct*) pSimSpec, (int*) &num_search_spec))
        {
          fprintf(stderr, "%s I had problems when calling simspec_struct_update_mag_status()\n", ERROR_PREFIX);
          return 1;
        }
      }
      else
      {
        //now update the texp_lo,texp_hi bounds and status flags for each spectrum
        if ( simspec_struct_update_texp_status ( (simspec_struct*) pSimSpec, (int*) &num_search_spec))
        {
          fprintf(stderr, "%s I had problems when calling simspec_struct_update_texp_status()\n", ERROR_PREFIX);
          return 1;
        }
      }
        
    }

    //do one last finalisation of the spectra to ensure that we meet the minimum requirements
    //TODO    


    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    //the next steps depend on the desired outputs
    //the following writes out the spectra to fits extensions
    if (  pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA )
    {
      if (  write_spec_data_to_files ((spectrograph_struct*) pSpec,
                                      (condlist_struct*) pCondList,
                                      (template_struct*) pTemplate,
                                      (simspec_struct*)  pSimSpec ,
                                      (sim_struct*)      pSim))
      {
        fprintf(stderr, "%s I had problems when writing spec data to file\n", ERROR_PREFIX);
        return 1;
      }
    }//if we need to write SPECTRA to file
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ( pSim->output & (int) SIM_OUTPUT_CODE_SUMMARY_ASCII )
    {
   
      if ( pSim->pSummaryFile )
      {
        if ( write_summary_to_file_ascii ((FILE*) pSim->pSummaryFile, (template_struct*) pTemplate,
                                          (condlist_struct*) pCondList, (simspec_struct*) pSimSpec))
        {
          fprintf(stderr, "%s I had problems when writing summary file\n", ERROR_PREFIX);
          return 1;
        }
      }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    if ( pSim->output & (int) SIM_OUTPUT_CODE_SUMMARY_FITS )
    {
      if ( write_summary_to_file_fits  ((sim_struct*) pSim, (template_struct*) pTemplate,
                                        (condlist_struct*) pCondList, (simspec_struct*) pSimSpec))
      {
        fprintf(stderr, "%s I had problems when writing fits summary file\n", ERROR_PREFIX);
        return 1;
      }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    if ( simspec_struct_free ( (simspec_struct**) &pSimSpec )) return 1;
  }//for each template
  //////////////////////////////////////////////////////////////////////

  return 0;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
int write_summary_to_file_ascii (FILE* pFile, template_struct* pTemplate, condlist_struct* pCondList, simspec_struct* pSimSpec)
{
  int m,r;
  if ( pFile == NULL ||
       pTemplate == NULL ||
       pCondList == NULL ||
       pSimSpec  == NULL ) return 1;
  
  fprintf(pFile, "#%5s %-30s %-18s %8s %8s %8s %4s %8s %8s %8s %8s %8s %10s %9s ",
          "INDEX", "TEMPLATE_NAME", "RULESET", "REDSHIFT", "MAG", 
          "TEXP(s)", "NSUB", "AIRMASS", "SKYMAG", "IQ", "TILT", "MISALIGN", "RESULT", "SUCCESS?");
  for(r=0;r<pSimSpec->num_rules;r++)
  {
    fprintf(pFile,"  RULE%02d ",  r+1);
  }
  fprintf(pFile,"\n");

  for(m=0;m<pTemplate->num_mag; m++ )
  {
    int e;
    for(e=0;e<pCondList->num_exp; e++ )
    {
      int c;
      for (c=0;c<pCondList->num_cond;c++)
      {
        cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);
        //        int spec =  (m*pCondList->num_exp + e)*pCondList->num_cond + c;
        int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);
        double mag  = pSimSpec->pmag[spec];
        double texp = pSimSpec->ptexp[spec];
        int nsub    = pSimSpec->pnsub[spec];
        char *pstr_success = (pSimSpec->pResult[spec] >= pTemplate->pRuleSet->required_value ? "YES" : "NO");

        if ( isnan(pSimSpec->pResult[spec]))  pstr_success = (char*) "-";

        if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_CONVERGED )
        {
          texp = pSimSpec->phi[spec];
          nsub = (int) ceil(texp / pCondList->ptexp[0]);
          pstr_success = (char*) "YES";
        } 
        else if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_CONVERGED )
        {
          mag = pSimSpec->plo[spec];
          pstr_success =  (char*)"YES";
        } 
        fprintf(pFile, "%6d %-30s %-18s %8.5g %8.5g %8.0f %4d %8.3f %8.2f %8.2f %8g %8g %10g %9s ",
                pTemplate->index + 1,
                pTemplate->str_name,
                pTemplate->pRuleSet->str_name,
                pTemplate->redshift,
                (mag  >= CALC_MAG_MAX_MAG || mag <= CALC_MAG_MIN_MAG  ? NAN : mag),
                (texp >= CALC_TEXP_MAX_TEXP ? NAN : texp),
                nsub,
                pCond->airmass,
                pCond->skybright,
                pCond->IQ,
                pCond->tilt,
                pCond->misalign,
                pSimSpec->pResult[spec],
                pstr_success); 
        for(r=0;r<pSimSpec->num_rules;r++)
        {
          fprintf(pFile,"%8.4g ",  pSimSpec->ppSubResult[spec][r]);
        }
        fprintf(pFile,"\n");

      }
    }
  }
  fprintf(pFile,"\n");
  return 0;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
int write_summary_to_file_fits (sim_struct* pSim, template_struct* pTemplate, condlist_struct* pCondList, simspec_struct* pSimSpec)
{
  const int colfmt[] = {TINT , TSTRING, TSTRING, TSTRING, TFLOAT, TFLOAT, TFLOAT, TINT, TFLOAT, TFLOAT, TFLOAT, TFLOAT, TFLOAT, TFLOAT, TDOUBLE, TFLOAT, TINT};
  const int colnum[] = {    1,       2,       3,       4,      5,      6,      7,    8,      9,     10,     11,     12,     13,      14,     15,   16,     17};
  const int ncols = (int) (sizeof(colnum)/sizeof(int));

  int status = 0;
  int m;
  if ( pSim      == NULL ||
       pTemplate == NULL ||
       pCondList == NULL ||
       pSimSpec  == NULL ) return 1;

  if ( pSim->SummaryFile_init_flag == FALSE )
  {
    // FITS column number           1          2           3          4           5        6       7       8          9           10        11      12         13           14              15                16              17
    const char *ttype[] = {"TR_INDEX", "TR_NAME", "TEMPLATE", "RULESET", "REDSHIFT",   "MAG", "TEXP", "NSUB", "AIRMASS", "SKYBRIGHT",     "IQ", "TILT", "MISALIGN", "MAG_NORM", "RULE_RESULTS", "RULESET_RESULT", "SUCCESS_FLAG" };
    char       *tform[] = {      "1J",     "40A",      "40A",     "30A",       "1E",    "1E",   "1E",   "1I",      "1E",        "1E",     "1E",   "1E",       "1E",         "", "",             "1E",             "1B"           };
    char       *tunit[] = {        "",        "",         "",        "",         "","mag,AB",  "sec",     "",  "sec(Z)",          "", "arcsec",   "mm",   "arcsec",      "mag", "",             "",               "boolean",    }; 
    // vector index                 0          1           2          3           4        5       6       7          8            9        10      11         12           13              14                15             16

    char tform_rule_results[STR_MAX];
    char tunit_skybright[STR_MAX];
    char tform_tr_name[STR_MAX];
    char tform_template[STR_MAX];
    char tform_ruleset[STR_MAX];
    char tform_mag_norm[STR_MAX];
    snprintf(tform_tr_name,  STR_MAX, "%dA", pSim->longest_template_name);
    snprintf(tform_template, STR_MAX, "%dA", pSim->longest_template_filename);
    snprintf(tform_ruleset,  STR_MAX, "%dA", pSim->longest_ruleset_name);
    tform[1]  = (char*) tform_tr_name;
    tform[2]  = (char*) tform_template;
    tform[3]  = (char*) tform_ruleset;

        
    snprintf(tform_mag_norm, STR_MAX, "%dE", pSim->num_filters);
    tform[13] = (char*) tform_mag_norm;

    //    snprintf(tform_rule_results, STR_MAX, "%dD", pSimSpec->num_rules);
    snprintf(tform_rule_results, STR_MAX, "%dE", pSimSpec->num_rules);
    tform[14] = (char*) tform_rule_results;

    snprintf(tunit_skybright, STR_MAX, "mag/arcsec2 (@%s)", SKYBRIGHT_TYPE_STRING(pCondList->skybright_type));
    tunit[9] = (char*) tunit_skybright;

    
    snprintf(pSim->str_summary_filename_fits,STR_MAX, "%s%s/%s",
             (pSim->clobber?"!":""), pSim->str_outdir, FITS_SUMMARY_FILENAME);
    
    if ( fits_create_file ((fitsfile**) &(pSim->pSummaryFile_fits), (char*) pSim->str_summary_filename_fits, &status))
    {
      fits_report_error(stderr, status);  
      fprintf(stderr, "%s  I had problems opening the file: %s\n",
              ERROR_PREFIX, pSim->str_summary_filename_fits);fflush(stderr);
      return 1;
    }

    {
      int ncols_here = (pSimSpec->num_rules > 0 ? ncols : ncols-3); //don't write the rule/ruleset columns + success flags if not using rules  
      if (  fits_create_tbl ((fitsfile*) pSim->pSummaryFile_fits, (int) BINARY_TBL,
                             (LONGLONG) pSim->SummaryFile_num_rows, (int) ncols_here,
                             (char**) ttype, (char**) tform, (char**) tunit,
                             (char*) "SUMMARY",
                             (int*) &status) )
        
      {
        fits_report_error(stderr, status);  
        return 1;
      }
    }
    pSim->SummaryFile_current_row = 1;
    pSim->SummaryFile_init_flag = (BOOL) TRUE;

    //add some header keywords
    {
      int f;
      //      char str_creator[STR_MAX];
      char str_username[STR_MAX];
      //      snprintf(str_creator,STR_MAX, "%s %s %s",ETC_SHORTNAME, GIT_VERSION, GIT_DATE);
      if ( getlogin_r((char*) str_username, (size_t) STR_MAX))
      {
        snprintf(str_username, STR_MAX, "Unknown");
      }
      if ( fits_write_key((fitsfile*) pSim->pSummaryFile_fits,
                          (int)   TSTRING,
                          (char*) "CREATOR",
                          (char*) ETC_FITS_CREATOR_KEYWORD,
                          (char*) "Software version",
                          (int*)   &status) ||
           fits_write_key((fitsfile*) pSim->pSummaryFile_fits,
                          (int)   TSTRING,
                          (char*) "USERNAME",
                          (char*) str_username,
                          (char*) "username of operator",
                          (int*)  &status))
      {
        fits_report_error(stderr, status);  
        return 1;
      }

      for(f=0;f<pSim->num_filters;f++)
      {
        char str_key_name[STR_MAX];
        char str_key_filename[STR_MAX];
        snprintf(str_key_name, STR_MAX, "FLTNAM%02d", f+1);
        snprintf(str_key_filename, STR_MAX, "FLTFIL%02d", f+1);

        if ( fits_write_key((fitsfile*) pSim->pSummaryFile_fits,
                            (int)   TSTRING,
                            (char*) str_key_name,
                            (char*) pSim->crvNormFilterList[f].str_name,
                            (char*) "Normalising filter name",
                            (int*)  &status) ||
             fits_write_key((fitsfile*) pSim->pSummaryFile_fits,
                            (int)   TSTRING,
                            (char*) str_key_filename,
                            (char*) pSim->crvNormFilterList[f].str_filename,
                            (char*) "Normalising filter filename",
                            (int*)  &status))
        {
          fits_report_error(stderr, status);  
          return 1;
        }
      }

    }
  }


  //now write the data to the file
  for(m=0;m<pTemplate->num_mag; m++ )
  {
    int e;
    for(e=0;e<pCondList->num_exp; e++ )
    {
      int c;
      for (c=0;c<pCondList->num_cond;c++)
      {
        int col;
        int f;
        cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);
        //int spec =  (m*pCondList->num_exp + e)*pCondList->num_cond + c;
        int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);
        float redshift  = (float) pTemplate->redshift;
        float mag       = (float) pSimSpec->pmag[spec];
        float texp      = (float) pSimSpec->ptexp[spec];
        float airmass   = (float) pCond->airmass;
        float skybright = (float) pCond->skybright;
        float IQ        = (float) pCond->IQ;
        float tilt      = (float) pCond->tilt;
        float misalign  = (float) pCond->misalign;
        int nsub        = (int) pSimSpec->pnsub[spec];
        int success     = (int) (pSimSpec->pResult[spec] >= pTemplate->pRuleSet->required_value ? 1 : 0);
        const long felem = 1;
        const long nelem = 1;
        int template_index = pTemplate->index + 1;
        fitsfile* pF = (fitsfile*) pSim->pSummaryFile_fits;
        long j = (long) pSim->SummaryFile_current_row;
        char* pstr_tr_name[1] = {NULL};
        char* pstr_template[1] = {NULL};
        char* pstr_ruleset_name[1] = {NULL};
        float ruleset_result = pSimSpec->pResult[spec];
        float mag_norm[SIM_MAX_FILTERS];
        for(f=0;f<pSim->num_filters;f++)
        {
          mag_norm[f] = (float) pTemplate->mag_norm[f] + mag - pTemplate->mag0; //offset by current renormalisation  
        }
        
        if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_CONVERGED )
        {
          texp    = (float) pSimSpec->phi[spec];
          nsub    = (int) ceil(texp / pCondList->ptexp[0]);
          success = (int) 0;
        } 
        else if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_CONVERGED )
        {
          mag     = (float) pSimSpec->plo[spec];
          success = (int) 1;
        } 
        if ( mag  >= CALC_MAG_MAX_MAG || 
             mag <= CALC_MAG_MIN_MAG  )   mag = (float) NAN;
        if ( texp >= CALC_TEXP_MAX_TEXP ) texp = (float) NAN;

        pstr_tr_name[0] = (char*) pTemplate->str_name;
        pstr_template[0] = (char*) pTemplate->str_filename;
        pstr_ruleset_name[0]  = (char*) pTemplate->pRuleSet->str_name;
        col= 0;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(template_index),    (int*) &status);
        col= 1;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, pstr_tr_name,         (int*) &status);
        col= 2;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, pstr_template,        (int*) &status);
        col= 3;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, pstr_ruleset_name,    (int*) &status);
        col= 4;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(redshift),          (int*) &status);
        col= 5;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(mag),               (int*) &status);
        col= 6;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(texp),              (int*) &status);
        col= 7;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(nsub),              (int*) &status);
        col= 8;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(airmass),           (int*) &status);
        col= 9;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(skybright),         (int*) &status);
        col=10;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(IQ),                (int*) &status);
        col=11;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(tilt),              (int*) &status);
        col=12;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem, &(misalign),          (int*) &status);
        col=13;fits_write_col((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,pSim->num_filters, mag_norm, (int*) &status);
        if ( pSimSpec->num_rules > 0 )
        {
          col=14;fits_write_col ((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,pSimSpec->num_rules, &(pSimSpec->ppSubResult[spec][0]),(int*) &status);
          col=15;fits_write_col ((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem,               &(ruleset_result),                (int*) &status);
          col=16;fits_write_col ((fitsfile*) pF,colfmt[col],colnum[col],(LONGLONG)j,felem,nelem,               &(success),                       (int*) &status);
        }
       pSim->SummaryFile_current_row ++;
      }
    }
  }

  

  
  return 0;
}
//---------------------------------------------------------------------------------------------------





//Fold template through sky and spectrograph
//for each arm do once per set of sky conditions and for first texp only
//calculate the fluence, sky, noise and SNR for each mag, condition and exposure time
int calc_flux_fluence_sky_noise_snr_spectra(simspec_struct* pSimSpec,
                                            template_struct* pTemplate,
                                            condlist_struct* pCondList,
                                            spectrograph_struct* pSpec )
{
  int a;
  int *pflags = NULL;
  int flag = 0;
  if ( pSimSpec  == NULL ||
       pTemplate == NULL ||
       pCondList == NULL ||
       pSpec     == NULL ) return 1;
  if ( (pflags = (int*) calloc((size_t) pSpec->num_arms, (size_t) sizeof(int))) == NULL )
    //  if ( (pflags = (int*) malloc(sizeof(int) * pSpec->num_arms)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //allow parallel operation across arms - TODO investigate why this is not causing very much speedup - I/O issues?
#if defined(_OPENMP)
  #pragma omp parallel for shared(pSimSpec,pTemplate,pCondList,pSpec,pflags)
#endif
  for (a=0;a<pSpec->num_arms;a++)
  {
    arm_struct *pArm  = (arm_struct*) &(pSpec->pArm[a]);
    pflags[a] = 0;

#if defined(_OPENMP)
//    printf("%s hello from thread %d of %d (arm=%s)\n",
//           DEBUG_PREFIX,
//           omp_get_thread_num(),
//           omp_get_num_threads(),
//           pArm->str_name);
#endif

    if ( pSimSpec->pstatus[0] == CALC_SNR_STATUS_START  ||
         pSimSpec->pstatus[0] == CALC_TEXP_STATUS_START ||
         pSimSpec->pstatus[0] == CALC_MAG_STATUS_START  )
    {
      //Fold the templates to get the Flux0 spectrum for each condition
      pflags[a] = (int) fold_template_through_arm_condlist((template_struct*) pTemplate,
                                                           (arm_struct*)      pArm,
                                                           (condlist_struct*) pCondList,
                                                           (double*)          &(pSimSpec->ppFlux0[a][0]),
                                                           (double)           pTemplate->mag0);
    }

    if ( pflags[a] == 0 ) //if no errors
    {
      int m;
      //now take magnitude-scaled copies of the flux0 arrays for each cond, texp and mag
      for(m=0;m<pTemplate->num_mag; m++ )
      {
        int e;
        for(e=0;e<pCondList->num_exp; e++ )
        {
          int c;
          for(c=0;c<pCondList->num_cond; c++ )
          {
            int i;
            int spec  =  (m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c;
            double magmult = pow((double)10.0, (double)0.4*(pTemplate->mag0 - pSimSpec->pmag[spec]));
            cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);

            //use the pre-computed fibrecoupling factor for this set of conditions
            double fluxmult = magmult * pCond->fibrecoupling_factor;


            if ( pSimSpec->pstatus[spec] == CALC_SNR_STATUS_START      ||
                 pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_START     ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_START      ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_IN  ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_UP  ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_DOWN )
            {      
              for(i=0;i<pArm->Lambda.num_pix;i++)
              {
                long pix0 =   c*pArm->Lambda.num_pix + i; 
                long pix = spec*pArm->Lambda.num_pix + i;
                pSimSpec->ppFlux[a][pix] = fluxmult * pSimSpec->ppFlux0[a][pix0];
              }
            }
          }
        }
      }
   
      //calculate the fluence, sky, noise and SNR for each mag, condition and exposure time
      for(m=0;m<pTemplate->num_mag; m++ )
      {
        int e;
        for(e=0;e<pCondList->num_exp; e++ )
        {
          int c;
          for (c=0;c<pCondList->num_cond;c++)
          {
            //            int spec =  (m*pCondList->num_exp + e)*pCondList->num_cond + c;
            int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);
            if ( pSimSpec->pstatus[spec] == CALC_SNR_STATUS_START        ||
                 pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_START       ||
                 pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_IN   ||
                 pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_UP   ||
                 pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_DOWN ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_START        ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_IN    ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_UP    ||
                 pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_DOWN )
            {
              int i;
              cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);
              int sky_em_index = pCond->index_airmass*pCondList->num_skybright + pCond->index_skybright;
              for(i=0;i<pArm->Lambda.num_pix;i++)
              {
                long pix = spec*pArm->Lambda.num_pix + i;
                pSimSpec->ppFluence[a][pix] = pSimSpec->ppFlux[a][pix] * pSimSpec->ptexp[spec];
                pSimSpec->ppSky[a][pix]     = (pArm->psky_emiss[pArm->Lambda.num_pix*sky_em_index + i] +
                                               pArm->dark_current_s) * pSimSpec->ptexp[spec];
                pSimSpec->ppTotal[a][pix]   = pSimSpec->ppFluence[a][pix] + pSimSpec->ppSky[a][pix];
                pSimSpec->ppNoise[a][pix]   =
                  calc_noise_gehrels_style ((double)pSimSpec->ppFluence[a][pix],
                                            (double)pSimSpec->ppSky[a][pix],
                                            (double)pArm->sq_read_noise*(double) pSimSpec->pnsub[spec],
                                            (double)pArm->skysub_residual);
                if ( pSimSpec->ppNoise[a][pix] > (double) 0.0 )
                  pSimSpec->ppSNR[a][pix]   = pSimSpec->ppFluence[a][pix] / pSimSpec->ppNoise[a][pix];
                else if ( pSimSpec->ppFluence[a][pix] > (double) 0.0 ) 
                  pSimSpec->ppSNR[a][pix]   = (double) DBL_MAX;
                else
                  pSimSpec->ppSNR[a][pix]   = (double) 0.0;
                
              }
            }
          }
        }
      }
    }
  } /*-- End of omp parallel for --*/

  for (a=0;a<pSpec->num_arms;a++)
  {
    if ( pflags[a] )
    {
      flag = pflags[a];
      break;
    }
  }
  FREETONULL(pflags);
  
  return flag;
}




//---------------------------------------------------------------------------------------------------
//for a set of conditions, fold the given template spectrum through
//the conditions, and through the instrument response
//multiply by sky transmission first, then convolve with instrument response
int fold_template_through_arm_cond      ( template_struct     *pTemplate,
                                          arm_struct          *pArm,
                                          cond_struct         *pCond,
                                          double              *pResult,
                                          double               mag)
{
  curve_struct crvCopy;

  if ( pTemplate == NULL ||
       pArm      == NULL ||
       pCond     == NULL ||
       pResult   == NULL ) return 1;
  //  fprintf(stdout, "%s Folding template through arm+condlist: arm=%s num_cond=%d delta_mag=%g\n",
  //          DEBUG_PREFIX,pArm->str_name, pCondList->num_cond, mag-pTemplate->mag0 ); fflush(stdout);
 
  if ( curve_struct_init ( (curve_struct*) &crvCopy ))
  {
    return 1;
  }
  
  if ( curve_struct_copy ((curve_struct*) &crvCopy, (curve_struct*) &(pTemplate->crvModel)))
  {
    return 1;
  }

  //apply the magnitude offset
  {
    int i;
    double fluxmult = pow((double)10.0, (double)0.4*(pTemplate->mag0 - mag));
    for(i=0;i<crvCopy.num_rows;i++) crvCopy.py[i] = crvCopy.py[i] * fluxmult;
    crvCopy.min_y = crvCopy.min_y  * fluxmult;
    crvCopy.max_y = crvCopy.max_y  * fluxmult;
  }

  //    fprintf(stdout, "%s Folding template through arm+cond    : arm=%s cond=%d (airmass=%.3f IQ=%.2f skybright=%.2f)\n",
  //            DEBUG_PREFIX,pArm->str_name, c, pCond->airmass, pCond->IQ, pCond->skybright); fflush(stdout);
  
  if ( pass_curve_through_cond ((curve_struct*) &(pTemplate->crvModel), (cond_struct*) pCond, (double*) crvCopy.py ))
  {
    return 1;
  }
  
  if ( convolve_curve_with_rmf_arf ((arm_struct*) pArm, (curve_struct*) &crvCopy,
                                    (double*) pResult))
  {
    return 1;
  }

  if ( curve_struct_free ( (curve_struct*) &crvCopy ))
  {
    return 1;
  }
  return 0;
}



//---------------------------------------------------------------------------------------------------
//for each set of conditions, fold the given template spectrum through
//the list of conditions, and through the instrument response
//multiply by sky transmission first, then convolve with instrument response
//try to minimise the number of foldings by duplicating where possible
int fold_template_through_arm_condlist  ( template_struct     *pTemplate,
                                          arm_struct          *pArm,
                                          condlist_struct     *pCondList,
                                          double              *pResult,
                                          double               mag)
{
  int c,i;
  curve_struct crvCopy;

 
  if ( pTemplate == NULL ||
       pArm      == NULL ||
       pCondList == NULL ||
       pResult   == NULL ) return 1;
  //  fprintf(stdout, "%s Folding template through arm+condlist: arm=%s num_cond=%d delta_mag=%g\n",
  //          DEBUG_PREFIX,pArm->str_name, pCondList->num_cond, mag-pTemplate->mag0 ); fflush(stdout);

  if ( curve_struct_init ( (curve_struct*) &crvCopy ))
  {
    return 1;
  }
  
  if ( curve_struct_copy ((curve_struct*) &crvCopy, (curve_struct*) &(pTemplate->crvModel)))
  {
    return 1;
  }

  //apply the magnitude offset
  {
    double fluxmult = pow((double)10.0, (double)0.4*(pTemplate->mag0 - mag));
    for(i=0;i<crvCopy.num_rows;i++) crvCopy.py[i] *= fluxmult;
    crvCopy.min_y *= fluxmult;
    crvCopy.max_y *= fluxmult;
  }

  for(c=0;c<pCondList->num_cond;c++)
  {
    cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);

    //    fprintf(stdout, "%s Folding template through arm+cond    : arm=%s cond=%d (airmass=%.3f IQ=%.2f skybright=%.2f)\n",
    //            DEBUG_PREFIX,pArm->str_name, c, pCond->airmass, pCond->IQ, pCond->skybright); fflush(stdout);


    //    fprintf(stdout, "DEBUG &(pTemplate->crvModel) = 0x%08lx\n", (unsigned long) (void*) &(pTemplate->crvModel));
    //    fprintf(stdout, "DEBUG pCond                  = 0x%08lx\n", (unsigned long) (void*) pCond);
    //    fprintf(stdout, "DEBUG crvCopy.py             = 0x%08lx\n", (unsigned long) (void*) crvCopy.py);
    //    fflush(stdout);    


    //TODO - speed up
    //can skip next steps if this (airmass,IQ) is same as last (airmass,IQ)
    if ( pass_curve_through_cond ((curve_struct*) &(pTemplate->crvModel),
                                  (cond_struct*) pCond,
                                  (double*) (crvCopy.py) ))
    {
      return 1;
    }

    if ( convolve_curve_with_rmf_arf ((arm_struct*) pArm, (curve_struct*) &crvCopy,
                                      (double*) &(pResult[c*pArm->Lambda.num_pix])))
    {
      return 1;
    }
  }
  

  if ( curve_struct_free ( (curve_struct*) &crvCopy ))
  {
    return 1;
  }
  return 0;
}

//calculate the sky background photon flux vectors (on detector)
//one for each set of sky conditions (airmass,skybrightness) 
//store the results in the arm_struct 
int fold_sky_through_arm ( arm_struct* pArm, condlist_struct* pCondList )
{
  long i, j;
  lambda_struct *pLambda = NULL;
  if ( pArm  == NULL ||
       pCondList  == NULL ) return 1;

  pLambda = (lambda_struct*) &(pArm->Lambda);
  if ( pLambda == NULL ) return 1;

  if ( pCondList->num_sky_emiss <= 0 ||
       pLambda->num_pix <= 0 ) return 1;

  fprintf(stdout, "%s Folding %dx%ld sky emission vectors through spectrograph arm: %s\n",
          COMMENT_PREFIX, pCondList->num_sky_emiss, pLambda->num_pix, pArm->str_name); fflush(stdout);

  pArm->num_sky = pCondList->num_sky_emiss;
  if ( (pArm->psky_emiss = (double*)  malloc(sizeof(double) * pArm->num_sky * pLambda->num_pix)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }


  for (i=0;i<pArm->num_sky;i++)
  {
    fprintf(stdout, "%s Folding sky emission vector %3ld through spectrograph arm: %s\n",
            COMMENT_PREFIX, i+1, pArm->str_name); fflush(stdout);
    if ( convolve_curve_with_rmf_arf ((arm_struct*) pArm, (curve_struct*) &(pCondList->pcrvEmiss[i]),
                                      (double*) &(pArm->psky_emiss[i*pLambda->num_pix])))  return 1;
    
    //now multiply by the solid angle of the fiber and the fibre sky coupling factor
    // to turn ph/s/arcsec2/pix to ph/s/pix
    for ( j=0; j<pLambda->num_pix;j++)
    {
      pArm->psky_emiss[i*pLambda->num_pix + j] *= (pArm->fiber_area * pCondList->Fibrecoupling.frac_sky);
    }
  }

//  //////////DEBUG DEBUG ////////////////////////////////////////////
//  //debug output
//  if ( TRUE == FALSE )
//  {
//    
//    fprintf(stdout, "#DEBUG_SKY_EMISSION %8s %s %6s %9s %9s %12s %12s %10s\n",
//            "ARM", "i", "j", "pl(AA)", "pdl(AA)", "emiss(phot/s/pix)", "parf(cm2)", "Res");
//    i = 0;
//    for ( j=0; j<pLambda->num_pix;j++)
//    {
//      fprintf(stdout, "DEBUG_SKY_EMISSION  %8s %ld %6ld %9.3f %9.3f %12.7g %12.7g %10.2f\n",
//              pArm->str_name, i, j, pLambda->pl[j], pLambda->pdl[j],
//              pArm->psky_emiss[i*pLambda->num_pix + j],
//              pArm->Rsp.parf[j],
//              pArm->Rsp.pres[j]);
//    }
//  }
//  //////////DEBUG DEBUG ////////////////////////////////////////////
  return 0;
}

int fold_sky_through_spectrograph ( spectrograph_struct* pSpec, condlist_struct* pCondList )
{
  int i;
  if ( pSpec  == NULL ||
       pCondList  == NULL ) return 1;
  for(i=0;i<pSpec->num_arms;i++)
  {
    arm_struct *pArm = (arm_struct*) &(pSpec->pArm[i]);
    if ( fold_sky_through_arm ( (arm_struct*) pArm, (condlist_struct*) pCondList))
    {
      fprintf(stderr, "%s I had problems calculating the sky background for spectrograph arm: %s\n",
              ERROR_PREFIX, pArm->str_name);
      return 1;
    }
  }

  return 0;
}

//---------------------------------------------------------------------------------------------------
//sim_struct functions
int sim_struct_init ( sim_struct **ppSim )
{
  int f;
  if ( ppSim == NULL ) return 1;
  if ( (*ppSim) == NULL )
  {
    //make space for the struct
    if ( (*ppSim = (sim_struct*) malloc(sizeof(sim_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  strncpy((*ppSim)->str_mode,"",STR_MAX);
  (*ppSim)->mode    = SIM_MODE_CODE_NONE;

  strncpy((*ppSim)->str_output,"",STR_MAX);
  (*ppSim)->output = SIM_OUTPUT_CODE_NONE;

  strncpy((*ppSim)->str_specformat,"",STR_MAX);
  (*ppSim)->specformat = SIM_SPECFORMAT_CODE_NONE;

  strncpy((*ppSim)->str_outdir,"",STR_MAX);

  strncpy((*ppSim)->str_summary_filename,"",STR_MAX);
  (*ppSim)->pSummaryFile = NULL;

  strncpy((*ppSim)->str_summary_filename_fits,"",STR_MAX);
  (*ppSim)->pSummaryFile_fits = NULL;
  (*ppSim)->SummaryFile_init_flag = (BOOL) FALSE;
  (*ppSim)->SummaryFile_num_rows = 0;
  (*ppSim)->SummaryFile_current_row = 1;

  //  if ( resample_struct_init ( (resample_struct*) &((*ppSim)->Resample) )) return 1;

  (*ppSim)->num_filters=0;
  strncpy((*ppSim)->str_norm_filter_magsys,"",STR_MAX);
  (*ppSim)->norm_filter_magsys = (int) MAGSYS_UNKNOWN;
  for(f=0;f<SIM_MAX_FILTERS;f++)
  {
    if ( curve_struct_init((curve_struct*) &((*ppSim)->crvNormFilterList[f]))) return 1;
  }
  
  (*ppSim)->longest_template_name     = (int) 0;
  (*ppSim)->longest_template_filename = (int) 0;
  (*ppSim)->longest_ruleset_name      = (int) 0;

  (*ppSim)->clobber = FALSE;
  (*ppSim)->debug = FALSE;
  (*ppSim)->random_seed = 0;
  
  
  return 0;
}

int sim_struct_free ( sim_struct **ppSim )
{
  if ( ppSim == NULL ) return 1;
  if ( (*ppSim) )
  {
    int f;
    for( f=0;f<SIM_MAX_FILTERS;f++)
    {
      if ( curve_struct_free((curve_struct*) &((*ppSim)->crvNormFilterList[f]) )) return 1;
    }
    free(*ppSim);
    *ppSim = NULL;
  }
  return 0;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
//cond_struct functions
int cond_struct_init ( cond_struct *pCond )
{
  if ( pCond == NULL ) return 1;
  pCond->index_airmass   = -1;
  pCond->index_IQ        = -1;
  pCond->index_skybright = -1;
  //  pCond->index_tilt      = -1;
  //  pCond->index_misalign       = -1;
  pCond->index_fibrecoupling = -1;
  pCond->airmass   = NAN;
  pCond->IQ        = NAN;
  pCond->skybright = NAN;
  pCond->tilt      = NAN;
  pCond->misalign       = NAN;
  pCond->pcrvEmiss = NULL;
  pCond->pcrvTrans = NULL;
  
  return 0;
}



//---------------------------------------------------------------------------------------------------
//condlist_struct functions
int condlist_struct_init ( condlist_struct **ppCondList )
{
  if ( ppCondList == NULL ) return 1;
  if ( (*ppCondList) == NULL )
  {
    //make space for the struct
    if ( (*ppCondList = (condlist_struct*) malloc(sizeof(condlist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  (*ppCondList)->num_airmass = 0;
  (*ppCondList)->pairmass    = NULL;
  (*ppCondList)->airmass_min = (double) DBL_MAX;
  (*ppCondList)->airmass_max = (double) -DBL_MAX;

  (*ppCondList)->num_skybright = 0;
  (*ppCondList)->pskybright    = NULL;
  (*ppCondList)->skybright_min = (double) DBL_MAX;
  (*ppCondList)->skybright_max = (double) -DBL_MAX;

  (*ppCondList)->num_IQ = 0;
  (*ppCondList)->pIQ    = NULL;
  (*ppCondList)->IQ_min = (double) DBL_MAX;
  (*ppCondList)->IQ_max = (double) -DBL_MAX;

  (*ppCondList)->num_tilt = 0;
  (*ppCondList)->ptilt    = NULL;
  (*ppCondList)->tilt_min = (double) DBL_MAX;
  (*ppCondList)->tilt_max = (double) -DBL_MAX;

  (*ppCondList)->num_misalign = 0;
  (*ppCondList)->pmisalign    = NULL;
  (*ppCondList)->misalign_min = (double) DBL_MAX;
  (*ppCondList)->misalign_max = (double) -DBL_MAX;

  (*ppCondList)->num_exp = 0;
  (*ppCondList)->ptexp  = NULL;
  (*ppCondList)->pnsub  = NULL;
  (*ppCondList)->texp_min = (double) DBL_MAX;
  (*ppCondList)->texp_max = (double) -DBL_MAX;
  (*ppCondList)->nsub_min = (int) INT_MAX;
  (*ppCondList)->nsub_max = (int) -INT_MAX;


  strcpy((*ppCondList)->str_interp_method, "");
  (*ppCondList)->interp_method = INTERP_METHOD_UNKNOWN;

  strcpy((*ppCondList)->str_skybright_type, "");
  (*ppCondList)->skybright_type = SKYBRIGHT_TYPE_UNKNOWN;
  
  (*ppCondList)->num_sky_emiss = 0;
  (*ppCondList)->pcrvEmiss = NULL;
  
  (*ppCondList)->num_sky_trans = 0;
  (*ppCondList)->pcrvTrans = NULL;

  (*ppCondList)->num_cond = 0;
  (*ppCondList)->pCond = NULL;

  if ( fibrecoupling_struct_init ( (fibrecoupling_struct*) &((*ppCondList)->Fibrecoupling))) return 1;

  return 0;
}

int condlist_struct_free ( condlist_struct **ppCondList )
{
  int i;
  if ( ppCondList == NULL ) return 1;
  if ( *ppCondList )
  {

    FREETONULL((*ppCondList)->pairmass  );
    FREETONULL((*ppCondList)->pskybright);
    FREETONULL((*ppCondList)->pIQ       );
    FREETONULL((*ppCondList)->ptilt     );
    FREETONULL((*ppCondList)->pmisalign );
    FREETONULL((*ppCondList)->ptexp     );
    FREETONULL((*ppCondList)->pnsub     );
    
    if ( (*ppCondList)->pcrvEmiss )
    {
      for ( i=0;i< (*ppCondList)->num_sky_emiss; i++) 
        if ( curve_struct_free ( (curve_struct*) &((*ppCondList)->pcrvEmiss[i]))) return 1;
      free ((*ppCondList)->pcrvEmiss) ;
      (*ppCondList)->pcrvEmiss = NULL;
    }
    if ( (*ppCondList)->pcrvTrans )
    {
      for ( i=0;i< (*ppCondList)->num_sky_trans; i++) 
        if ( curve_struct_free ( (curve_struct*) &((*ppCondList)->pcrvTrans[i]))) return 1;
      free ((*ppCondList)->pcrvTrans) ;
      (*ppCondList)->pcrvTrans = NULL;
    }

    if ( (*ppCondList)->pCond ) 
    {
      free((*ppCondList)->pCond);
      (*ppCondList)->pCond = NULL;
    }

    if ( fibrecoupling_struct_free ( (fibrecoupling_struct*) &((*ppCondList)->Fibrecoupling))) return 1;

    free(*ppCondList);
    *ppCondList = NULL;
  }
  return 0;
}


//interpolate the sky vectors to the requested conditions
int condlist_struct_setup ( condlist_struct *pCondList, sky_struct *pSky, BOOL verbose )
{
  int g,h,i,j,k;
  if ( pCondList == NULL || pSky == NULL ) return 1;
  pCondList->num_sky_emiss = pCondList->num_airmass*pCondList->num_skybright;
  pCondList->num_sky_trans = pCondList->num_airmass;

  fprintf(stdout, "%s Setting up requested sky conditions arrays: num_sky_trans=%d num_sky_emiss=%d interp=%s skybright_type=%s\n",
          COMMENT_PREFIX, pCondList->num_sky_trans, pCondList->num_sky_emiss, 
          INTERP_METHOD_STRING(pCondList->interp_method),
          SKYBRIGHT_TYPE_STRING(pCondList->skybright_type));
    //          (pCondList->interp_method==INTERP_METHOD_NEAREST?"NEAREST":
    //           (pCondList->interp_method==INTERP_METHOD_LINEAR?"LINEAR":
    //            (pCondList->interp_method==INTERP_METHOD_SPLINE?"SPLINE":"UNKNOWN"))));
    fflush(stdout);

  //allocate space for emiss and trans curves
  if ((pCondList->pcrvEmiss = (curve_struct*) malloc(sizeof(curve_struct) * pCondList->num_sky_emiss)) == NULL ||
      (pCondList->pcrvTrans = (curve_struct*) malloc(sizeof(curve_struct) * pCondList->num_sky_trans)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  //  for(i=0;i<pSky->Trans.num_curves;i++)
  //    fprintf(stdout, "%s TRANS %2d : AM=%5.3f\n", DEBUG_PREFIX, i, pSky->Trans.pairmass[i]); fflush(stdout);
  

  

  
  //////////////////////////////////////////////////////////////////////////
  // first do the transmission vectors
  // assume that transmission is a function of airmass only
  //////////////////////////////////////////////////////////////////////////
  for(i=0;i<pCondList->num_sky_trans;i++)
  {
    int util_result;
    long j1,j2;

    if ( curve_struct_init ((curve_struct*) &(pCondList->pcrvTrans[i]))) return 1;
    switch ( pCondList->interp_method)
    {
    case INTERP_METHOD_NEAREST:
      if ( util_unsorted_get_index_of_nearest_double((long) pSky->Trans.num_curves,
                                                     (double*) pSky->Trans.pairmass,
                                                     (double) pCondList->pairmass[i],
                                                     (long*) &j1)) return 1;
      assert(j1 >= 0 && j1 < pSky->Trans.num_curves);
      if ( curve_struct_copy ((curve_struct*) &(pCondList->pcrvTrans[i]),
                              (curve_struct*) &(pSky->Trans.pCurve[j1]))) return 1;

      if ( verbose ) {
        fprintf(stdout, "%s Sky conditions transmission %2d : AM=%5.3f (interp NEAREST from AM=%5.3f)\n",
                DEBUG_PREFIX, i+1, pCondList->pairmass[i], pSky->Trans.pairmass[j1]); fflush(stdout);
      }
      break;

    case INTERP_METHOD_LINEAR:
      //TODO - fix this so that it works properly with the unevenly spaced grid in airmass,skybright
      //first work out which two input trans curves bracket the requested airmass
      if ( (util_result = util_unsorted_binary_search_double ((long) pSky->Trans.num_curves,
                                                              (double*) pSky->Trans.pairmass,
                                                              (double) pCondList->pairmass[i],
                                                              (long*) &j1, (long*) &j2)) == -1 ) return 1;
      if ( j1 >= 0 && j2 >= 0 )
      {
        long l;
        double weight1 = 1.0, weight2 = 0.0, dl;
        assert(j1 >= 0 && j1 < pSky->Trans.num_curves);
        assert(j2 >= 0 && j2 < pSky->Trans.num_curves);
        if ( curve_struct_copy ((curve_struct*) &(pCondList->pcrvTrans[i]),
                                (curve_struct*) &(pSky->Trans.pCurve[j1]))) return 1;
        if ( (dl = pSky->Trans.pairmass[j2] - pSky->Trans.pairmass[j1]) > (double) TINY_DELTA_AIRMASS ) 
        {
          weight1 = (pSky->Trans.pairmass[j2] - pCondList->pairmass[i])/dl;
          weight2 = (pCondList->pairmass[i]   - pSky->Trans.pairmass[j1])/dl;
        }
        for(l=0;l<pCondList->pcrvTrans[i].num_rows;l++)
        {
          pCondList->pcrvTrans[i].py[l] = weight1 * pSky->Trans.pCurve[j1].py[l] + weight2 * pSky->Trans.pCurve[j2].py[l];
        }
      if ( verbose ) {
        fprintf(stdout,"%s Sky conditions transmission %2d : AM=%5.3f (interp LINEAR from AM=[%5.3f,%5.3f])%s\n",
                DEBUG_PREFIX, i+1, pCondList->pairmass[i],
                pSky->Trans.pairmass[j1], pSky->Trans.pairmass[j2],
                (util_result>0?" Warning - extrapolating outside tabulated airmass range":"")); fflush(stdout);
      }
        
      }
      else
      {
        fprintf(stderr, "%s I had problems interpolating sky transmission vectors\n", ERROR_PREFIX);
        return 1;
      }
      
      break;
    case INTERP_METHOD_SPLINE: //TODO
      return 1;
      break;
    default :
      return 1;
    }

    //now choose the most appropriate fibrecoupling values ???? Orphan comment?
    
  }
  //////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////
  // now do the emission vectors
  // assume that emission is a function of airmass and skybrightness only
  // remember that the sky brightness may be specified at the zenith or at the given airmass
  //////////////////////////////////////////////////////////////////////////
  for(k=0;k<pCondList->num_airmass;k++)
  {
    for(j=0;j<pCondList->num_skybright;j++)
    {
      double *pskybright = NULL;
      double skybright_min = NAN; 
      double skybright_max = NAN;
      int sky_em = k * pCondList->num_skybright + j;
      if ( curve_struct_init ((curve_struct*) &(pCondList->pcrvEmiss[sky_em]))) return 1;

      switch ( pCondList->skybright_type)
      {
      case SKYBRIGHT_TYPE_ZENITH :
        pskybright    = (double*) pSky->Emiss.pzenbright;
        skybright_min = (double)  pSky->Emiss.zenbright_min;
        skybright_max = (double)  pSky->Emiss.zenbright_max;
        break;
      case SKYBRIGHT_TYPE_LOCAL  :
        pskybright    = (double*) pSky->Emiss.pskybright;
        skybright_min = (double)  pSky->Emiss.skybright_min;
        skybright_max = (double)  pSky->Emiss.skybright_max;
        break;
      default : return 1;
        break;
      }
      
      switch ( pCondList->interp_method)
      {
      case INTERP_METHOD_NEAREST:
        {
          double dairmass, dskybright;
          double sqdist, sqdistmin = DBL_MAX;
          double skyscale ;
          long l, l1 = 0;
          //choose the sky emission curve which is nearest in airmass,skybrightness space
          //normalise to full range of each variable
          dairmass   = fabs(pSky->Emiss.airmass_max   - pSky->Emiss.airmass_min);
          dskybright = fabs(skybright_max - skybright_min);
          
          if (  dairmass   <= 0.0 ) dairmass   = (double) 1.0;  //avoid divide by zero 
          if (  dskybright <= 0.0 ) dskybright = (double) 1.0; 
               
          for(l=0;l<pSky->Emiss.num_curves;l++)
          {
            sqdist = 
              SQR((pSky->Emiss.pairmass[l] - pCondList->pairmass[k])/dairmass) +
              SQR((pskybright[l]           - pCondList->pskybright[j])/dskybright);  
            if ( sqdist < sqdistmin )
            {
              sqdistmin = sqdist;
              l1 = l;
            }
          }
          if ( curve_struct_copy ((curve_struct*) &(pCondList->pcrvEmiss[sky_em]),
                                  (curve_struct*) &(pSky->Emiss.pCurve[l1]))) return 1;

          //now scale the sky emission to the requested sky brightness
          skyscale = (double) pow(10.0,0.4*((pskybright[l1] - pCondList->pskybright[j])));
          if ( curve_struct_scale_y ((curve_struct*) &(pCondList->pcrvEmiss[sky_em]),
                                     (double) skyscale)) return 1;
          
          if ( verbose ) {
            fprintf(stdout, "%s Sky conditions emission     %2d : AM=%5.3f SB(%s)=%5.2f (interp NEAREST from AM=%5.3f,SB(%s)=%5.2f)\n",
                    DEBUG_PREFIX, j+1, pCondList->pairmass[k],
                    SKYBRIGHT_TYPE_STRING(pCondList->skybright_type), pCondList->pskybright[j],
                    pSky->Emiss.pairmass[l1],
                    SKYBRIGHT_TYPE_STRING(pCondList->skybright_type), pskybright[l1]); 
          }
          
        }
        break;
      case INTERP_METHOD_LINEAR:  //need bilinear interpolation
        {
          long index[4];
          double hilo_air[4]   = {1.0,  1.0, -1.0, -1.0}; 
          double hilo_sky[4]   = {1.0, -1.0,  1.0, -1.0}; 
          int index_min_max[4];
          int corner;

          switch ( pCondList->skybright_type)
          {
          case SKYBRIGHT_TYPE_ZENITH :
            index_min_max[0] = pSky->Emiss.index_max_airmass_max_zenbright;
            index_min_max[1] = pSky->Emiss.index_max_airmass_min_zenbright;
            index_min_max[2] = pSky->Emiss.index_min_airmass_max_zenbright;
            index_min_max[3] = pSky->Emiss.index_min_airmass_min_zenbright;
            break;
          case SKYBRIGHT_TYPE_LOCAL  :
            index_min_max[0] = pSky->Emiss.index_max_airmass_max_skybright;
            index_min_max[1] = pSky->Emiss.index_max_airmass_min_skybright;
            index_min_max[2] = pSky->Emiss.index_min_airmass_max_skybright;
            index_min_max[3] = pSky->Emiss.index_min_airmass_min_skybright;
          
            break;
          default : return 1;
            break;
          }
      
          
          //search for the bounding entries in airmass,skybrightness space
          //can't do a binary search using existing functions because it's a bivariate list
//          fprintf(stdout, "index_max_airmass_max_skybright = %3d => airmass=%8g skybright=%8g (airmass=%8g skybright=%8g)\n",
//                  pSky->Emiss.index_max_airmass_max_skybright,
//                  pSky->Emiss.pairmass[pSky->Emiss.index_max_airmass_max_skybright],
//                  pSky->Emiss.pskybright[pSky->Emiss.index_max_airmass_max_skybright],
//                  pSky->Emiss.airmass_max, pSky->Emiss.skybright_max);
//          fprintf(stdout, "index_max_airmass_min_skybright = %3d => airmass=%8g skybright=%8g (airmass=%8g skybright=%8g)\n",
//                  pSky->Emiss.index_max_airmass_min_skybright,
//                  pSky->Emiss.pairmass[pSky->Emiss.index_max_airmass_min_skybright],
//                  pSky->Emiss.pskybright[pSky->Emiss.index_max_airmass_min_skybright],
//                  pSky->Emiss.airmass_max, pSky->Emiss.skybright_min);
//           fprintf(stdout, "index_min_airmass_max_skybright = %3d => airmass=%8g skybright=%8g (airmass=%8g skybright=%8g)\n",
//                  pSky->Emiss.index_min_airmass_max_skybright,
//                  pSky->Emiss.pairmass[pSky->Emiss.index_min_airmass_max_skybright],
//                  pSky->Emiss.pskybright[pSky->Emiss.index_min_airmass_max_skybright],
//                  pSky->Emiss.airmass_min, pSky->Emiss.skybright_max);
//          fprintf(stdout, "index_min_airmass_min_skybright = %3d => airmass=%8g skybright=%8g (airmass=%8g skybright=%8g)\n",
//                  pSky->Emiss.index_min_airmass_min_skybright,
//                  pSky->Emiss.pairmass[pSky->Emiss.index_min_airmass_min_skybright],
//                  pSky->Emiss.pskybright[pSky->Emiss.index_min_airmass_min_skybright],
//                  pSky->Emiss.airmass_min, pSky->Emiss.skybright_min);
         

//          fprintf(stdout, "DEBUG_EMISS %8s %8s %6s %3s %3s  %4s %8s %8s %8s %8s %8s %8s %8s %8s %6s\n",
//                  "airmass", "skybri", "corner", "air", "sky",
//                  "l", "AM", "SKY", "d_air", "d_sky", "bestdair", "bestdsky","best_air", "best_sky", "index[c]");
          for ( corner=0;corner<4;corner++)
          {
            double best_d_air = DBL_MAX;
            double best_d_sky = DBL_MAX;
            double d_air;
            double d_sky;
            int l;
            index[corner] = index_min_max[corner];
            best_d_air = fabs((pSky->Emiss.pairmass[index_min_max[corner]] - pCondList->pairmass[k])*hilo_air[corner]);
            best_d_sky = fabs((pskybright[index_min_max[corner]]           - pCondList->pskybright[j])*hilo_sky[corner]);

            for(l=0;l<pSky->Emiss.num_curves;l++)
            {
              //              char *str_better = "";
              d_air = (pSky->Emiss.pairmass[l] - pCondList->pairmass[k])*hilo_air[corner];
              d_sky = (pskybright[l]           - pCondList->pskybright[j])*hilo_sky[corner];
              if ( d_air >= 0.0 && fabs(d_air) < best_d_air &&
                   d_sky >= 0.0 && fabs(d_sky) < best_d_sky )
              {
                best_d_air = fabs(d_air);
                best_d_sky = fabs(d_sky);
                index[corner] = l;
                //                str_better = "*better*";
              }
              //              fprintf(stdout,"DEBUG_EMISS %8g %8g %6d %3.0f %3.0f  %4d %8g %8g %8g %8g %8g %8g %8g %8g %6ld %s\n",
              //                      pCondList->pairmass[k], pCondList->pskybright[j], corner, hilo_air[corner], hilo_sky[corner],
              //                      l, pSky->Emiss.pairmass[l], pSky->Emiss.pskybright[l],
              //                      fabs(d_air), fabs(d_sky), best_d_air, best_d_sky,
              //                      pSky->Emiss.pairmass[index[corner]], pSky->Emiss.pskybright[index[corner]], index[corner], str_better);
              //            fflush(stdout);
            }
          }
          //taking a copy of 1st emission curve is easiest way of setting up the new curve:
          if ( curve_struct_copy ((curve_struct*) &(pCondList->pcrvEmiss[sky_em]),
                                  (curve_struct*) &(pSky->Emiss.pCurve[0]))) return 1;

          //now calc the weights of each corner: http://en.wikipedia.org/wiki/Bilinear_interpolation
          {
            int l;
            double weight[4];
            double x1,x2,y1,y2,x,y;
            double denominator;
            x1 = pSky->Emiss.pairmass[index[0]]; 
            y1 = pskybright[index[0]]; 
            x2 = pSky->Emiss.pairmass[index[2]]; 
            y2 = pskybright[index[1]]; 
            x = pCondList->pairmass[k];
            y = pCondList->pskybright[j];
            denominator = (x2 - x1)*(y2 - y1);
            assert( DBL_ISNOTZERO(denominator) ); //TODO
            weight[0] = (x2 - x)*(y2 - y)/denominator;
            weight[1] = (x - x1)*(y2 - y)/denominator;
            weight[2] = (x2 - x)*(y - y1)/denominator;
            weight[3] = (x2 - x)*(y - y1)/denominator;

            for(l=0;l<pCondList->pcrvEmiss[sky_em].num_rows;l++)
            {
              pCondList->pcrvEmiss[sky_em].py[l] =
                weight[0] * pSky->Emiss.pCurve[index[0]].py[l] +
                weight[1] * pSky->Emiss.pCurve[index[1]].py[l] +
                weight[2] * pSky->Emiss.pCurve[index[2]].py[l] +
                weight[3] * pSky->Emiss.pCurve[index[3]].py[l];
            }
          }
          //          fprintf(stdout, "%s Sky conditions emission     %2d : AM=%5.3f SB=%5.2f (interp LINEAR from AM=[%5.3f,%5.3f],SB=[%5.2f,%5.2f])\n",
          //                  DEBUG_PREFIX, i, pCondList->pairmass[k], pCondList->pskybright[j],
          //                  pSky->Emiss.pairmass[index[0]],   pSky->Emiss.pairmass[index[2]],
          //                  pSky->Emiss.pskybright[index[0]], pSky->Emiss.pskybright[index[1]]); fflush(stdout);
        }
        break;
        
      case INTERP_METHOD_SPLINE: //TODO
        return 1;
        break;
      default :
        return 1;
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////
  //set up the Cond struct array
  //  pCondList->num_cond      = pCondList->num_airmass * pCondList->num_skybright * pCondList->num_IQ;
  pCondList->num_cond = pCondList->num_airmass * pCondList->num_skybright * pCondList->num_IQ * pCondList->num_tilt * pCondList->num_misalign;
  fprintf(stdout, "%s Number of combinations of observing conditions: %d (Nairmass,Nskybright,NIQ,Ntilt,Nmisalign)=(%d,%d,%d,%d,%d)\n",
          COMMENT_PREFIX,
          pCondList->num_cond,
          pCondList->num_airmass,
          pCondList->num_skybright,
          pCondList->num_IQ,
          pCondList->num_tilt,
          pCondList->num_misalign);
  fprintf(stdout, "%s Number of combinations of fibre-coupling parameters: %d (NIQ,Ntilt,Nmisalign)=(%d,%d,%d)\n",
          COMMENT_PREFIX,
          pCondList->num_IQ*pCondList->num_tilt*pCondList->num_misalign,
          pCondList->num_IQ,
          pCondList->num_tilt,
          pCondList->num_misalign);

  if ( (pCondList->pCond     = (cond_struct*)  malloc(sizeof(cond_struct)  * pCondList->num_cond)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for(k=0;k<pCondList->num_airmass;k++)
  {
    for(j=0;j<pCondList->num_skybright;j++)
    {
      for(i=0;i<pCondList->num_IQ;i++)
      {
        for(h=0;h<pCondList->num_tilt;h++)
        {
          for(g=0;g<pCondList->num_misalign;g++)
          {
            long c = (((k*pCondList->num_skybright + j)*pCondList->num_IQ + i)*pCondList->num_tilt + h)*pCondList->num_misalign + g;
            long sky_em_index = k*pCondList->num_skybright + j;
            long sky_trans_index = k;
            cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);
            if ( cond_struct_init ((cond_struct*) pCond )) return 1;
            pCond->airmass   = pCondList->pairmass[k];
            pCond->skybright = pCondList->pskybright[j];
            pCond->IQ        = pCondList->pIQ[i];
            pCond->tilt      = pCondList->ptilt[h];
            pCond->misalign       = pCondList->pmisalign[g];
            pCond->pcrvTrans = (curve_struct*)  &(pCondList->pcrvTrans[sky_trans_index]);
            pCond->pcrvEmiss = (curve_struct*)  &(pCondList->pcrvEmiss[sky_em_index]);
            pCond->index_airmass   = k;
            pCond->index_skybright = j;
            pCond->index_IQ        = i;
            pCond->index_tilt      = h;
            pCond->index_misalign  = g;
            if ( fibrecoupling_struct_find_nearest ( (fibrecoupling_struct*) &(pCondList->Fibrecoupling),
                                                     (cond_struct*) pCond,
                                                     (BOOL) j==0 ? TRUE : FALSE))
            {
              return 1;
            }
            
          }
        }
      }
    }
  }
  //////////////////////////////////////////////////////////////////////////

  return 0;
}

//---------------------------------------------------------------------------------------------------

//multiply the input curve by the transmission vector specified in pCond
//assume that pResults has aleady been allocated and contains enough space
//to receive a copy of pCurve->py[]
int pass_curve_through_cond (curve_struct* pCurve, cond_struct *pCond, double* pResult )
{
  long i,j;
  double dlambda_trans;
  double half_dlambda_trans;
  double lmin;
  double lmax = 0.0;   //this must be outside the loop
  //  static BOOL debug1st = TRUE;
  
  curve_struct *pcrvTrans = NULL;

  //  fprintf(stdout, "Cuckoo at %s:%d\n", __FILE__, __LINE__); fflush(stdout);

  if ( pCurve  == NULL ||
       pCond   == NULL ||
       pResult == NULL ) return 1;

  if ( pCurve->num_rows < 2 ) return 1;

  pcrvTrans = (curve_struct*) pCond->pcrvTrans;
  if ( pcrvTrans  == NULL  ) return 1;
  if ( pcrvTrans->num_rows < 2 ) return 1;
  if ( pcrvTrans->unit_x <= 0.0 ) return 1;

  //assume that the transmission curve has constant steps in wavelength
  dlambda_trans = (double) (pcrvTrans->px[1]-pcrvTrans->px[0]); 
  half_dlambda_trans = 0.5 * dlambda_trans;
//  // For each step in Curve, work out the average sky transmission over the
//  // each delta_lambda step
//  // assume that trans curve is piecewise constant (step function)
//  // assume that the x-range for each step of pCurve is
//  // from 0.5*(px[i]+px[i-1]) to 0.5*(px[i+1]+px[i])  
//  if ( debug1st )
//  {
//    fprintf (stdout, "DEBUG_PASS %6s %8s %8s %6s %10s %10s %10s : %10s * => %10s\n",
//             "i", "lmin", "lmax", "j", "sum", "sumweight", "pCurve->py[i]", "mean_trans", "pResult[i]"); 
//  }
//  fprintf(stdout, "Cuckoo at %s:%d\n", __FILE__, __LINE__); fflush(stdout);
  j = 0;
  for ( i=0;i<pCurve->num_rows;i++)
  {
    double summed_weight = 0.0;
    double overlap = 0.0;
    double sum    = 0.0;
    double mean_trans = 0.0;
    double li, lii;
    double llmin,llmax;
    //work out the upper and lower limits for the pCurve delta_lambda step
    if ( i == 0 )
    {
      lmax = 0.5*(pCurve->px[i+1]+pCurve->px[i]);
      lmin = 2.0 * pCurve->px[i] - lmax; 
    }
    else if ( i == pCurve->num_rows - 1 )
    {
      lmin = lmax;
      lmax = 2.0 * pCurve->px[i] - lmin; 
    }
    else
    {
      lmin = lmax;
      lmax = 0.5*(pCurve->px[i+1]+pCurve->px[i]);
    }
    llmin = lmin * pCurve->unit_x / pcrvTrans->unit_x; ;  //work in the native wavelength units of the transmission curve
    llmax = lmax * pCurve->unit_x / pcrvTrans->unit_x;

    //now get the average value of the transmission over this range
    //do a weighted sum over the transmission vector
    //weight by the fractional overlap of each bin
    if ( j < pcrvTrans->num_rows)
    {
      //search ahead from last position
      while ( (lii=(pcrvTrans->px[j]+half_dlambda_trans)) < llmin &&
              j < pcrvTrans->num_rows) j++;
      //step back if necessary
      if ( (pcrvTrans->px[j]-half_dlambda_trans) > llmin && j > 0 ) j --;

      //calculate the interval overlap
      summed_weight = 0.0;
      sum = 0.0;
      //      li =(pcrvTrans->px[j]-half_dlambda_trans);
      //      lii=(pcrvTrans->px[j]+half_dlambda_trans);
      //      overlap =  (double) MAX (0.0, MIN(llmax, lii) - MAX(li, llmin));
      //      sum           = pcrvTrans->py[j]*pcrvTrans->unit_y*overlap;
      //      summed_weight = overlap;
      li =(pcrvTrans->px[j]-half_dlambda_trans);
      do
      {
        lii=pcrvTrans->px[j]+half_dlambda_trans;
        overlap =  (double) MAX (0.0, MIN(llmax, lii) - MAX(li, llmin));
        sum += pcrvTrans->py[j]*pcrvTrans->unit_y*overlap;
        summed_weight += overlap;
        j++;
        if ( j >= pcrvTrans->num_rows ) break;
      }
      while ((li=pcrvTrans->px[j]-half_dlambda_trans) < llmax);
        
      if ( summed_weight > 0.0 )
      {
        mean_trans = sum / summed_weight;
        pResult[i] = pCurve->py[i] * mean_trans;
      }
      else
        pResult[i] = NAN;
    }
    else
    {
      pResult[i] = 0.0;
    }


//    if ( debug1st )
//    {
//      fprintf (stdout, "DEBUG_PASS %6ld %8.2f %8.2f %6ld %10.6f %10.6f : %10.5e * %10.6f => %10.6e\n",
//               i, lmin, lmax, j, sum, summed_weight, pCurve->py[i], mean_trans, pResult[i]); 
//    }
  }
  
  //  debug1st = FALSE;
  
  return 0;  
}


//---------------------------------------------------------------------------------------------------
//rebin_struct functions
int rebin_struct_init ( rebin_struct *pRebin )
{
  if ( pRebin == NULL ) return 1;
  pRebin->num_bins = 0;
  pRebin->pNumPix = NULL;   
  pRebin->ppPixIndex = NULL;
  pRebin->ppArmIndex = NULL;
  pRebin->ppOverlap  = NULL; 
  pRebin->pLmin  = NULL; 
  pRebin->pLmax  = NULL; 
  return 0;
}

int rebin_struct_free ( rebin_struct *pRebin)
{
  int i;
  if ( pRebin == NULL ) return 1;
   
  for ( i=0;i<pRebin->num_bins;i++)
  {
    if (pRebin->ppPixIndex ) {if(pRebin->ppPixIndex[i]) {free(pRebin->ppPixIndex[i]);pRebin->ppPixIndex[i]=NULL;}}
    if (pRebin->ppArmIndex ) {if(pRebin->ppArmIndex[i]) {free(pRebin->ppArmIndex[i]);pRebin->ppArmIndex[i]=NULL;}}
    if (pRebin->ppOverlap  ) {if(pRebin->ppOverlap[i] ) {free(pRebin->ppOverlap[i]) ;pRebin->ppOverlap[i] =NULL;}}
  }
  if (pRebin->pNumPix    ) {free(pRebin->pNumPix)   ;pRebin->pNumPix   =NULL;} 
  if (pRebin->ppPixIndex ) {free(pRebin->ppPixIndex);pRebin->ppPixIndex=NULL;}
  if (pRebin->ppArmIndex ) {free(pRebin->ppArmIndex);pRebin->ppArmIndex=NULL;}
  if (pRebin->ppOverlap  ) {free(pRebin->ppOverlap) ;pRebin->ppOverlap =NULL;}
  if (pRebin->pLmin      ) {free(pRebin->pLmin)     ;pRebin->pLmin     =NULL;}
  if (pRebin->pLmax      ) {free(pRebin->pLmax)     ;pRebin->pLmax     =NULL;}

  return 0;
}
//---------------------------------------------------------------------------------------------------

 

int associate_templatelist_with_rulesetlist( templatelist_struct *pTL, rulesetlist_struct *pRuleSetList)
{

  int i;
  if ( pTL == NULL ||
       pRuleSetList == NULL ) return 1;
  for(i=0;i<pTL->num_templates;i++)
  {
    template_struct *pTemplate = (template_struct*) &(pTL->pTemplate[i]);
    int r;
    pTemplate->pRuleSet = NULL;
    //special cases
    if (strlen(pTemplate->str_ruleset)         == 0 ||
        strcmp(pTemplate->str_ruleset, "NONE") == 0 ||
        strcmp(pTemplate->str_ruleset, "none") == 0 ||
        strcmp(pTemplate->str_ruleset, "-")    == 0 )
    {
      pTemplate->pRuleSet = (ruleset_struct*) &(dummy_ruleset);
    }
    else
    {
      //now search to get the correct pointer from the rulesetlist
      for(r=0;r<pRuleSetList->num_rulesets;r++)
      {
        ruleset_struct *pRuleSet = (ruleset_struct*) &(pRuleSetList->pRuleSet[r]);
        if ( strcmp(pTemplate->str_ruleset, pRuleSet->str_name) == 0 )
        {
          pTemplate->pRuleSet = (ruleset_struct*) pRuleSet ;
          break;
        }
      }
      if ( pTemplate->pRuleSet == NULL )
      {
        fprintf(stderr, "%s An unknown RULESET \"%s\" was specified for template: %s (filename %s) \n",
                ERROR_PREFIX, pTemplate->str_ruleset, pTemplate->str_name, pTemplate->str_filename);
        return 1;
      }
    }
  }
  
  return 0;
}

//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
//simspec_single_struct functions
int simspec_single_struct_init ( simspec_single_struct *pSSS )
{
  if ( pSSS == NULL ) return 1;
  pSSS->num_arms  = 0;
  pSSS->pnum_pix  = NULL;
  //  pSSS->ppFlux0   = NULL;
  pSSS->ppFlux    = NULL;
  pSSS->ppFluence = NULL;
  pSSS->ppSky     = NULL;
  pSSS->ppTotal   = NULL;
  pSSS->ppNoise   = NULL;
  pSSS->ppSNR     = NULL;
  pSSS->pSpec     = NULL;
  pSSS->pTemplate = NULL;
  pSSS->pCond     = NULL;
  pSSS->texp      = (double) 0.0;
  pSSS->nsub      = (int) 0;
  pSSS->mag      = (double) NAN;
  return 0;
}
int simspec_single_struct_free ( simspec_single_struct *pSSS )
{
  if ( pSSS == NULL ) return 1;
  if ( pSSS->pnum_pix ) free ( pSSS->pnum_pix ) ; pSSS->pnum_pix  = NULL;
  //  if ( pSSS->ppFlux0  ) free ( pSSS->ppFlux0)   ; pSSS->ppFlux0   = NULL;
  if ( pSSS->ppFlux   ) free ( pSSS->ppFlux )   ; pSSS->ppFlux    = NULL;
  if ( pSSS->ppFluence) free ( pSSS->ppFluence ); pSSS->ppFluence = NULL;
  if ( pSSS->ppSky    ) free ( pSSS->ppSky )    ; pSSS->ppSky     = NULL;
  if ( pSSS->ppTotal  ) free ( pSSS->ppTotal)   ; pSSS->ppTotal   = NULL;
  if ( pSSS->ppNoise  ) free ( pSSS->ppNoise )  ; pSSS->ppNoise   = NULL;
  if ( pSSS->ppSNR    ) free ( pSSS->ppSNR )    ; pSSS->ppSNR     = NULL;
  return 0;
}

int simspec_single_struct_alloc (  simspec_single_struct *pSSS, spectrograph_struct *pSpec )
{
  int a;
  if ( pSSS  == NULL ||
       pSpec == NULL ) return 1;
  if ( pSpec->num_arms < 1 ) return 2;
  pSSS->pSpec = (spectrograph_struct*) pSpec;
  pSSS->num_arms = (long) pSpec->num_arms;

  if (//(pSSS->ppFlux0   = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->ppFlux    = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->ppFluence = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->ppSky     = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->ppTotal   = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->ppNoise   = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->ppSNR     = (double**) malloc(sizeof(double*) * pSSS->num_arms)) == NULL ||
      (pSSS->pnum_pix  = (long*)    malloc(sizeof(long)    * pSSS->num_arms)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for (a=0;a<pSSS->num_arms;a++)
  {
    //    pSSS->ppFlux0[a]   = (double*) NULL;
    pSSS->ppFlux[a]    = (double*) NULL;
    pSSS->ppFluence[a] = (double*) NULL;
    pSSS->ppSky[a]     = (double*) NULL;
    pSSS->ppTotal[a]   = (double*) NULL;
    pSSS->ppNoise[a]   = (double*) NULL;
    pSSS->ppSNR[a]     = (double*) NULL;
    pSSS->pnum_pix[a]  = (long) 0;
  }  
  return 0;
}

int simspec_single_struct_set (  simspec_single_struct *pSSS, simspec_struct *pSimSpec, int m, int c, int e )
{
  int a, spec;
  if ( pSSS == NULL ||
       pSimSpec == NULL  ) return 1;
  
  if ( m < 0 || m >= pSimSpec->pTemplate->num_mag  ) return 2;
  if ( c < 0 || c >= pSimSpec->pCondList->num_cond ) return 2;
  if ( e < 0 || e >= pSimSpec->pCondList->num_exp  ) return 2;

  if ( pSSS->num_arms < pSimSpec->num_arms) return 3;
  //  spec = (m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c;
  spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);

  for (a=0;a<pSimSpec->num_arms;a++)
  {
    long pix0 = spec*pSimSpec->pSpec->pArm[a].Lambda.num_pix;
    //    long pix0 = ((m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c)*pSimSpec->pSpec->pArm[a].Lambda.num_pix;
    //    pSSS->ppFlux0[a]   = (double*) &(pSimSpec->ppFlux0[a][c*pSimSpec->pSpec->pArm[a].Lambda.num_pix]);
    pSSS->ppFlux[a]    = (double*) &(pSimSpec->ppFlux[a][pix0]);
    pSSS->ppFluence[a] = (double*) &(pSimSpec->ppFluence[a][pix0]);
    pSSS->ppSky[a]     = (double*) &(pSimSpec->ppSky[a][pix0]);
    pSSS->ppTotal[a]   = (double*) &(pSimSpec->ppTotal[a][pix0]);
    pSSS->ppNoise[a]   = (double*) &(pSimSpec->ppNoise[a][pix0]);
    pSSS->ppSNR[a]     = (double*) &(pSimSpec->ppSNR[a][pix0]);
    pSSS->pnum_pix[a]  = (long) pSimSpec->pSpec->pArm[a].Lambda.num_pix;
  }

  pSSS->pTemplate = (template_struct*) pSimSpec->pTemplate;
  pSSS->pCond     = (cond_struct*) &(pSimSpec->pCondList->pCond[c]);
  //  pSSS->texp = (double) pSimSpec->pCondList->ptexp[e];
  //  pSSS->nsub = (int)    pSimSpec->pCondList->pnsub[e];
  pSSS->texp = (double) pSimSpec->ptexp[spec];
  pSSS->nsub = (int)    pSimSpec->pnsub[spec];
  pSSS->mag  = (double) pSimSpec->pmag[spec];

  
  return 0;
}

//---------------------------------------------------------------------------------------------------
//simspec_struct functions
int simspec_struct_init ( simspec_struct **ppSimSpec )
{
  if ( ppSimSpec == NULL ) return 1;
  if ( *ppSimSpec == NULL )
  {
    //make space for the struct
    if ( (*ppSimSpec = (simspec_struct*) malloc(sizeof(simspec_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  (*ppSimSpec)->num_arms   = 0;
  (*ppSimSpec)->num_spec   = 0;
  (*ppSimSpec)->pnum_pix   = NULL;
  (*ppSimSpec)->ppFlux0    = NULL;
  (*ppSimSpec)->ppFlux     = NULL;
  (*ppSimSpec)->ppFluence  = NULL;
  (*ppSimSpec)->ppSky      = NULL;
  (*ppSimSpec)->ppTotal    = NULL;
  (*ppSimSpec)->ppNoise    = NULL;
  (*ppSimSpec)->ppSNR      = NULL;
  (*ppSimSpec)->pCondList  = NULL;
  (*ppSimSpec)->pTemplate  = NULL;
  (*ppSimSpec)->pSpec      = NULL;

  (*ppSimSpec)->num_rules   = 0;
  (*ppSimSpec)->ppSubResult = NULL;
  (*ppSimSpec)->pResult     = NULL;
  (*ppSimSpec)->pmag        = NULL;
  (*ppSimSpec)->ptexp       = NULL;
  (*ppSimSpec)->pnsub       = NULL;
  (*ppSimSpec)->plo         = NULL;
  (*ppSimSpec)->phi         = NULL;
  (*ppSimSpec)->pstatus     = NULL;

//  (*ppSimSpec)->pResample   = NULL;
//  (*ppSimSpec)->pResFlux    = NULL;
//  (*ppSimSpec)->pResFluence = NULL;
//  (*ppSimSpec)->pResSky     = NULL;
//  (*ppSimSpec)->pResTotal   = NULL;
//  (*ppSimSpec)->pResNoise   = NULL;
//  (*ppSimSpec)->pResSNR     = NULL;
//  (*ppSimSpec)->pResNorm    = NULL;

  return 0;
}

int simspec_struct_free ( simspec_struct **ppSimSpec )
{
  if ( ppSimSpec == NULL ) return 1;
  if ( *ppSimSpec )
  {
    int a,s;
    for (a=0;a<(*ppSimSpec)->num_arms;a++)
    {
      if ((*ppSimSpec)->ppFlux0  ) {FREETONULL((*ppSimSpec)->ppFlux0[a]  );}
      if ((*ppSimSpec)->ppFlux   ) {FREETONULL((*ppSimSpec)->ppFlux[a]   );}
      if ((*ppSimSpec)->ppFluence) {FREETONULL((*ppSimSpec)->ppFluence[a]);}
      if ((*ppSimSpec)->ppSky    ) {FREETONULL((*ppSimSpec)->ppSky[a]    );}
      if ((*ppSimSpec)->ppTotal  ) {FREETONULL((*ppSimSpec)->ppTotal[a]  );}
      if ((*ppSimSpec)->ppNoise  ) {FREETONULL((*ppSimSpec)->ppNoise[a]  );}
      if ((*ppSimSpec)->ppSNR    ) {FREETONULL((*ppSimSpec)->ppSNR[a]    );}
    }
    FREETONULL((*ppSimSpec)->ppFlux0  );
    FREETONULL((*ppSimSpec)->ppFlux   );
    FREETONULL((*ppSimSpec)->ppFluence);
    FREETONULL((*ppSimSpec)->ppSky    );
    FREETONULL((*ppSimSpec)->ppTotal  );
    FREETONULL((*ppSimSpec)->ppNoise  );
    FREETONULL((*ppSimSpec)->ppSNR    );
    FREETONULL((*ppSimSpec)->pnum_pix );

//    FREETONULL((*ppSimSpec)->pResFlux   );
//    FREETONULL((*ppSimSpec)->pResFluence);
//    FREETONULL((*ppSimSpec)->pResSky    );
//    FREETONULL((*ppSimSpec)->pResTotal  );
//    FREETONULL((*ppSimSpec)->pResNoise  );
//    FREETONULL((*ppSimSpec)->pResSNR    );
//    FREETONULL((*ppSimSpec)->pResNorm   );

    if ( (*ppSimSpec)->ppSubResult)
    {
      for (s=0;s<(*ppSimSpec)->num_spec;s++)
      {
        FREETONULL((*ppSimSpec)->ppSubResult[s]);
      }
      FREETONULL((*ppSimSpec)->ppSubResult);
    }
    FREETONULL((*ppSimSpec)->pResult);
    FREETONULL((*ppSimSpec)->pmag)   ;
    FREETONULL((*ppSimSpec)->ptexp)  ;
    FREETONULL((*ppSimSpec)->pnsub)  ;
    FREETONULL((*ppSimSpec)->plo)    ;
    FREETONULL((*ppSimSpec)->phi)    ;
    FREETONULL((*ppSimSpec)->pstatus);

    FREETONULL(*ppSimSpec);
  }
  
  return 0;
}

// position in the data arrays is calculated as:
// pix = (pCond->num_cond*e + c)*pArm->num_pix + i
int simspec_struct_alloc ( simspec_struct *pSimSpec, spectrograph_struct *pSpec, condlist_struct *pCondList)
{
  int a,s,m;
  if ( pSimSpec  == NULL ||
       pSpec     == NULL ||
       pCondList == NULL ) return 1;
  if ( pSimSpec->pTemplate == NULL) return 1;
  pSimSpec->pSpec = (spectrograph_struct*) pSpec;
  pSimSpec->pCondList = (condlist_struct*) pCondList;
  
  pSimSpec->num_arms  = (int) pSpec->num_arms;
  //  pSimSpec->num_spec  = (int) pCondList->num_exp * pCondList->num_cond;
  pSimSpec->num_spec  = (int) pSimSpec->pTemplate->num_mag * pCondList->num_exp * pCondList->num_cond;
  pSimSpec->num_rules = (int) pSimSpec->pTemplate->pRuleSet->num_rules;
  
  if ((pSimSpec->ppFlux0    = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppFlux     = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppFluence  = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppSky      = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppTotal    = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppNoise    = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppSNR      = (double**) malloc(sizeof(double*) * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->pnum_pix   = (long*)    malloc(sizeof(long)    * pSimSpec->num_arms)) == NULL ||
      (pSimSpec->ppSubResult= (double**) malloc(sizeof(double*) * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->pResult    = (double*)  malloc(sizeof(double)  * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->pmag       = (double*)  malloc(sizeof(double)  * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->ptexp      = (double*)  malloc(sizeof(double)  * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->pnsub      = (int*)     malloc(sizeof(int)     * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->plo        = (double*)  malloc(sizeof(double)  * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->phi        = (double*)  malloc(sizeof(double)  * pSimSpec->num_spec)) == NULL ||
      (pSimSpec->pstatus    = (int*)     malloc(sizeof(int)     * pSimSpec->num_spec)) == NULL )
      //   || (pSimSpec->ppLambda  = (lambda_struct**) malloc(sizeof(lambda_struct*) * pSimSpec->num_arms)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  
  for(a=0;a<pSimSpec->num_arms;a++)
  {
    arm_struct *pArm  = (arm_struct*) &(pSpec->pArm[a]);
    //    long array_length = pCondList->num_cond * pCondList->num_exp * pArm->Lambda.num_pix;
    long array_length =  pSimSpec->pTemplate->num_mag * pCondList->num_exp * pCondList->num_cond * pArm->Lambda.num_pix;
    pSimSpec->ppFlux0[a]   = NULL;
    pSimSpec->ppFlux[a]    = NULL;
    pSimSpec->ppFluence[a] = NULL;
    pSimSpec->ppSky[a]     = NULL;
    pSimSpec->ppTotal[a]   = NULL;
    pSimSpec->ppNoise[a]   = NULL;
    pSimSpec->ppSNR[a]     = NULL;
    pSimSpec->pnum_pix[a]  = (long) pArm->Lambda.num_pix;
    //    pSimSpec->ppLambda[a]  = (lambda_struct*) &(pSpec->pArm[a].Lambda);

    if ((pSimSpec->ppFlux0[a]   = (double*) malloc(sizeof(double) * pCondList->num_cond * pArm->Lambda.num_pix)) == NULL ||
        (pSimSpec->ppFlux[a]    = (double*) malloc(sizeof(double) * array_length)) == NULL ||
        (pSimSpec->ppFluence[a] = (double*) malloc(sizeof(double) * array_length)) == NULL ||
        (pSimSpec->ppSky[a]     = (double*) malloc(sizeof(double) * array_length)) == NULL ||
        (pSimSpec->ppTotal[a]   = (double*) malloc(sizeof(double) * array_length)) == NULL ||
        (pSimSpec->ppNoise[a]   = (double*) malloc(sizeof(double) * array_length)) == NULL ||
        (pSimSpec->ppSNR[a]     = (double*) malloc(sizeof(double) * array_length)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  for(s=0;s<pSimSpec->num_spec;s++)
  {
    pSimSpec->ppSubResult[s]    = NULL;
    if ((pSimSpec->ppSubResult[s] = (double*) malloc(sizeof(double) * pSimSpec->num_rules)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  
  for(m=0;m<pSimSpec->pTemplate->num_mag; m++ )
  {
    int e;
    for(e=0;e<pSimSpec->pCondList->num_exp; e++ )
    {
      int c;
      for(c=0;c<pSimSpec->pCondList->num_cond; c++ )
      {
        //        int spec = (m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c;
        int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);
        pSimSpec->pResult[spec]  = (double) 0.0;
        pSimSpec->pmag[spec]     = (double) pSimSpec->pTemplate->pmag_list[m];
        pSimSpec->ptexp[spec]    = (double) pSimSpec->pCondList->ptexp[e];
        pSimSpec->pnsub[spec]    = (int)    pSimSpec->pCondList->pnsub[e];
        pSimSpec->pstatus[spec]  = (int) -1;
        pSimSpec->plo[spec]      = (double) NAN;
        pSimSpec->phi[spec]      = (double) NAN;
      }
    }
  }
  
  return 0;
}      



//---------------------------------------------------------------------------------------------------

//          if ( t==0)
//          {
//            c = 0;
//            fprintf(stdout, "TEST_FLUX %8s %4s %6s %8s %12s\n", "Arm", "cond", "pix", "lambda", "Flux");
//            for(i=0;i<pArm->Lambda.num_pix;i++)
//            {
//              fprintf(stdout, "TEST_FLUX %8s %4d %6ld %8.2f %12.7e\n",
//                      pArm->str_name, c, i, pArm->Lambda.pl[i], ppSpecFlux[a][i]);
//            }
//          }
//          if ( t==0)
//          {
//            c = 0;
//            fprintf(stdout, "TEST_SKY  %8s %4s %6s %8s %12s\n", "Arm", "cond", "pix", "lambda", "Sky");
//            for(i=0;i<pArm->Lambda.num_pix;i++)
//            {
//              fprintf(stdout, "TEST_SKY  %8s %4d %6ld %8.2f %12.7e\n",
//                      pArm->str_name, c, i, pArm->Lambda.pl[i], ppSpecSky[a][i]);
//            }
//          }




///////////////////////////////////////////////////////////////////////////////////////////////////
//assume that file is open already
int condlist_struct_write_to_chdu (condlist_struct *pCondList, template_struct *pTemplate, simspec_struct *pSimSpec, fitsfile* pFile )
//write the fits extension containing details of the conditions
{
  const char *ttype[] = {"INDEX" , "MAG"   , "AIRMASS", "SKYBRIGHT"  , "IQ"         , "TILT" , "MISALIGN", "TEXP" , "NSUBEXP", "STATUS"};
  const char *tform[] = {"1J"    , "1E"    , "1E"     , "1E"         , "1E"         , "1E"   , "1E"      , "1E"   , "1J"     , "1I"    };
  const char *tunit[] = {""      , "mag,AB", "sec(Z)" , "mag/arcsec2", "fwhm,arcsec", "mm"   , "arcsec"  , "s"    , ""       , "flag"  };
  const int  colnum[] = {1       , 2        , 3       , 4            , 5            , 6      , 7         , 8      , 9        , 10      };
  const int  colfmt[] = {TINT    , TDOUBLE , TDOUBLE  , TDOUBLE      , TDOUBLE      , TDOUBLE, TDOUBLE   , TDOUBLE, TINT     , TINT    };

  const int ncols = (int) (sizeof(colnum)/sizeof(int));
  int status = 0;
  int e,c,m;
  long j;
  long nrows;
  if ( pFile == NULL ||
       pCondList == NULL ||
       pSimSpec  == NULL ||
       pTemplate == NULL ) return 1;

  nrows = (long) (pTemplate->num_mag * pCondList->num_cond * pCondList->num_exp);
  
  if (  fits_create_tbl ((fitsfile*) pFile, (int) BINARY_TBL,
                         (LONGLONG) nrows, (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) "CONDITIONS",
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  //write the TDMINn,TDMAXn keywords for each column
  {
    double tdmin_d[] = {NAN,
                        (double) pTemplate->mag_min,
                        (double) pCondList->airmass_min,
                        (double) pCondList->skybright_min,
                        (double) pCondList->IQ_min,
                        (double) pCondList->tilt_min,
                        (double) pCondList->misalign_min,
                        (double) pCondList->texp_min,      
                        NAN};
    double tdmax_d[] = {NAN,
                        (double) pTemplate->mag_max,
                        (double) pCondList->airmass_max,
                        (double) pCondList->skybright_max,
                        (double) pCondList->IQ_max,
                        (double) pCondList->tilt_max,
                        (double) pCondList->misalign_max,
                        (double) pCondList->texp_max,
                        NAN};
    int    tdmin_i[] = {(int) 1,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        (int) pCondList->nsub_min};
    int    tdmax_i[] = {(int) nrows ,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        (int) pCondList->nsub_max};
    
    int    nunique[] = {(int) nrows,
                        (int) pTemplate->num_mag,
                        (int) pCondList->num_airmass,
                        (int) pCondList->num_skybright,
                        (int) pCondList->num_IQ,
                        (int) pCondList->num_tilt,
                        (int) pCondList->num_misalign,
                        (int) pCondList->num_exp,
                        (int) pCondList->num_exp};
    
    int nkeys = (int) (sizeof(tdmin_d)/sizeof(double));
    int k;
    for(k=0;k<nkeys;k++)
    {
      char str_tdmin[FLEN_KEYWORD];
      char str_tdmax[FLEN_KEYWORD];
      char str_nunique[FLEN_KEYWORD];
      snprintf(str_tdmin,   FLEN_KEYWORD, "TDMIN%d",   colnum[k]);
      snprintf(str_tdmax,   FLEN_KEYWORD, "TDMAX%d",   colnum[k]);
      snprintf(str_nunique, FLEN_KEYWORD, "NUNIQUE%d", colnum[k]);
      if (fits_write_key((fitsfile*) pFile, colfmt[k], (char*) str_tdmin,
                         (colfmt[k]==TDOUBLE?(void*) &(tdmin_d[k]):(void*) &(tdmin_i[k])),
                         (char*) "Minimum of values in column", &status) ||
          fits_write_key((fitsfile*) pFile, colfmt[k], (char*) str_tdmax,
                         (colfmt[k]==TDOUBLE?(void*) &(tdmax_d[k]):(void*) &(tdmax_i[k])),
                         (char*) "Maximum of values in column", &status) ||
          fits_write_key((fitsfile*) pFile, TINT,      (char*) str_nunique,
                         (void*) &(nunique[k]),
                         (char*) "Number of unique values in column", &status) )
      {
        fits_report_error(stderr, status);  
        return 1;
      }

    } 
  }
  
  j = 0;
  for(m=0;m<pTemplate->num_mag; m++ )
  {
    for(e=0;e<pCondList->num_exp; e++ )
    {
      for(c=0;c<pCondList->num_cond; c++ )
      {
        cond_struct *pCond = (cond_struct*) &(pCondList->pCond[c]);
        //        int spec = (m*pCondList->num_exp + e)*pCondList->num_cond + c;
        int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);
        const long felem = 1;
        const long nelem = 1;
        j++;
        fits_write_col ((fitsfile*) pFile, colfmt[0], colnum[0], j,felem, nelem, &(j),                       (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[1], colnum[1], j,felem, nelem, &(pSimSpec->pmag[spec]),    (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[2], colnum[2], j,felem, nelem, &(pCond->airmass),          (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[3], colnum[3], j,felem, nelem, &(pCond->skybright),        (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[4], colnum[4], j,felem, nelem, &(pCond->IQ),               (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[5], colnum[5], j,felem, nelem, &(pCond->tilt),             (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[6], colnum[6], j,felem, nelem, &(pCond->misalign),         (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[7], colnum[7], j,felem, nelem, &(pSimSpec->ptexp[spec]),   (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[8], colnum[8], j,felem, nelem, &(pSimSpec->pnsub[spec]),   (int*) &status);
        fits_write_col ((fitsfile*) pFile, colfmt[9], colnum[9], j,felem, nelem, &(pSimSpec->pstatus[spec]), (int*) &status);
        if ( status ) 
        {
          //          fprintf(stderr, "DEBUG m=%d e=%d c=%d spec=%d\n", m, e, c, spec);
          //          fprintf(stderr, "DEBUG mag=%g am=%g sky=%g texp=%g nsub=%d status=%d\n",
          //                  pTemplate->pmag_list[m],
          //                  pCond->airmass,
          //                  pCond->skybright,
          //                  texp_f,
          //                  pSimSpec->pnsub[spec],
          //                  pSimSpec->pstatus[spec]);
          fits_report_error(stderr, status);  
          return 1;
        }
      }
    }
  }
  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////////////////////////////////////////////
//assume that file is open already
int write_wavelength_solution_table_to_chdu (fitsfile* pFile, lambda_struct *pLambda)
//write the fits extension containing details of the per pixel wavelength solution
{
  const char *ttype[] = {"LAMBDA", "DLAMBDA", "RESELEM"};
  const char *tform[] = {"1D"    , "1E"     , "1E"     };
  const char *tunit[] = {"Angstrom", "Angstrom/pixel","pixel" };
  const int  colnum[] = {1      , 2         , 3       };
  const int  colfmt[] = {TDOUBLE, TDOUBLE, TDOUBLE  };
  const int ncols = (int) (sizeof(colnum)/sizeof(int));

  int status = 0;
  
  if ( pFile == NULL ||
       pLambda == NULL ) return 1;
  
  if (  fits_create_tbl ((fitsfile*) pFile, (int) BINARY_TBL,
                         pLambda->num_pix, (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) "WAVESOLN",
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  
  //TODO do this in chunks
  fits_write_col ((fitsfile*) pFile, (int) colfmt[0], (int) colnum[0], 1,
                  1, pLambda->num_pix, pLambda->pl,  (int*) &status);
  fits_write_col ((fitsfile*) pFile, (int) colfmt[1], (int) colnum[1], 1,
                  1, pLambda->num_pix, pLambda->pdl,  (int*) &status);
  fits_write_col ((fitsfile*) pFile, (int) colfmt[2], (int) colnum[2], 1,
                  1, pLambda->num_pix, pLambda->pres_pix,  (int*) &status);

  if ( status ) 
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
//assume that file is open already
int write_wavelength_solution_vector_to_chdu (fitsfile* pFile, lambda_struct *pLambda)
//write the fits extension containing details of the per pixel wavelength solution in a way that is machine readable
{
  char str_tform[STR_MAX];
  const char *ttype[] = {"LAMBDA"};
  const char *tform[] = {(char*) str_tform};
  const char *tunit[] = {"Angstrom"};
  const int  colnum[] = {1      };
  const int  colfmt[] = {TDOUBLE};
  const int ncols = (int) (sizeof(colnum)/sizeof(int));

  int status = 0;
  
  if ( pFile == NULL ||
       pLambda == NULL ) return 1;
  sprintf(str_tform, "%ldD", pLambda->num_pix);
  
  if (  fits_create_tbl ((fitsfile*) pFile, (int) BINARY_TBL,
                         1, (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) "WAVETAB",
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  
  fits_write_col ((fitsfile*) pFile, (int) colfmt[0], (int) colnum[0], 1,
                  1, pLambda->num_pix, pLambda->pl,  (int*) &status);

  if ( status ) 
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////////////












int write_spec_data_to_files (spectrograph_struct *pSpec,
                              condlist_struct *pCondList,
                              template_struct *pTemplate,
                              simspec_struct  *pSimSpec,
                              sim_struct      *pSim)
{
  
  int a;

  if (pSpec     == NULL ||
      pCondList == NULL ||
      pTemplate == NULL ||
      pSimSpec  == NULL ||
      pSim      == NULL ) return 1;
  
  for (a=0;a<pSpec->num_arms;a++)
  {
    const int mask_list[]= {SIM_OUTPUT_CODE_SPECTRA_FLUX,
                            SIM_OUTPUT_CODE_SPECTRA_FLUENCE,
                            SIM_OUTPUT_CODE_SPECTRA_SKY,
                            SIM_OUTPUT_CODE_SPECTRA_TOTAL,
                            SIM_OUTPUT_CODE_SPECTRA_NOISE,
                            SIM_OUTPUT_CODE_SPECTRA_SNR,
                            SIM_OUTPUT_CODE_SPECTRA_POISSON};
    double *ppData[]     = {(double*) pSimSpec->ppFlux[a],
                            (double*) pSimSpec->ppFluence[a],
                            (double*) pSimSpec->ppSky[a],
                            (double*) pSimSpec->ppTotal[a],
                            (double*) pSimSpec->ppNoise[a],
                            (double*) pSimSpec->ppSNR[a],
                            (double*) pSimSpec->ppTotal[a]};
    double *pData2       = (double*) pSimSpec->ppNoise[a];  //needed for poisson
    const int num_ext =  (int) (sizeof(mask_list)/sizeof(int));
    arm_struct *pArm  = (arm_struct*) &(pSpec->pArm[a]);
    char str_filename[STR_MAX];
    fitsfile *pFile = NULL;
    int ext;
    int status = 0;

    snprintf(str_filename, STR_MAX,
             "%s%s/specout_template_%s_%s.fits",
             CLOBBER2S(pSim->clobber), pSim->str_outdir, pTemplate->str_name, pArm->str_name);
  
    //    fprintf(stdout, "%s Writing data for %s to fits file: %s\n",
    //            DEBUG_PREFIX, pTemplate->str_filename, str_filename); fflush(stdout);
  
    if ( fits_create_file ((fitsfile**) &pFile, (char*) str_filename, &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  
    if ( condlist_struct_write_to_chdu ((condlist_struct*) pCondList, (template_struct*) pTemplate,
                                        (simspec_struct*) pSimSpec,(fitsfile*) pFile ))
    {
      fprintf(stderr, "%s Problem writing condlist for %s to fits file: %s\n",
              COMMENT_PREFIX, pTemplate->str_filename, str_filename);
      return 1;
    }
  
    if ( pSim->specformat & (int) SIM_SPECFORMAT_CODE_IMAGE ) //if image format oputput was requested
    {
      //write extensions with the source flux,fluence,sky,noise,snr etc
      for(ext=0;ext<num_ext;ext++)
      {
        //      fprintf(stdout, "%s %2d pSim->output = 0x%08x mask_list[ext] = 0x%08x => result=%d\n", DEBUG_PREFIX, ext, pSim->output, mask_list[ext], (int)(pSim->output & (int) mask_list[ext])); fflush(stdout);
        if ( pSim->output & (int) mask_list[ext] ) //if this extension has been requested
        {
          if ( write_spec_image_extension_to_chdu ((fitsfile*) pFile,
                                                   (condlist_struct*) pCondList,
                                                   (template_struct*) pTemplate,
                                                   (arm_struct*) pArm,
                                                   (double*) (ppData[ext]),
                                                   (double*) pData2,
                                                   (int) mask_list[ext]))
          {
            fprintf(stderr, "%s Problem writing spec image extension %d for %s to fits file: %s\n",
                    ERROR_PREFIX, ext+2, pTemplate->str_filename, str_filename);
            return 1;
          }
        }
      }
    
      //now write wavelength solution extensions (if needed)
      if ( pArm->Lambda.type != LAMBDA_TYPE_CODE_DISP_LINEAR )
      {
        if ( write_wavelength_solution_table_to_chdu ((fitsfile*) pFile, (lambda_struct*) &(pArm->Lambda)))
        {
          fprintf(stderr, "%s Problem writing wavelength solution extension for %s to fits file: %s\n",
                  ERROR_PREFIX, pTemplate->str_filename, str_filename);
          return 1;
        }
        if ( write_wavelength_solution_vector_to_chdu ((fitsfile*) pFile, (lambda_struct*) &(pArm->Lambda)))
        {
          fprintf(stderr, "%s Problem writing wavelength solution extension for %s to fits file: %s\n",
                  ERROR_PREFIX, pTemplate->str_filename, str_filename);
          return 1;
        }
      }
    }

    //if binary table format oputput was requested
    if ( pSim->specformat & (int) SIM_SPECFORMAT_CODE_TABLE ) 
    {
      if ( write_spec_table_extension_to_chdu ((fitsfile*) pFile,
                                               (sim_struct*) pSim,
                                               (condlist_struct*) pCondList,
                                               (template_struct*) pTemplate,
                                               (arm_struct*) pArm,
                                               (simspec_struct*) pSimSpec,
                                               (int) a))
      {
        fprintf(stderr, "%s Problem writing spec table extension for %s to fits file: %s\n",
                ERROR_PREFIX, pTemplate->str_filename, str_filename);
        return 1;
      }
    }
    
    fits_close_file(pFile, &status);
  }//for each arm
  return 0;
}




///////////////////////////////////////////////////////////////////////////////////////////////////
int write_spec_image_extension_to_chdu (fitsfile* pFile,
                                       condlist_struct* pCondList,
                                       template_struct* pTemplate,
                                       arm_struct* pArm,
                                       double *pData,
                                       double *pData2,   //normally null
                                       int extension_type )
//write image extensions with the source flux,fluence,sky,noise and snr
{
  const int naxis = 2;
  const long fpixel[2] = {1 , 1};
  long naxes[2];
  char *pstr_extname = NULL;
  char *pstr_units = NULL;
  int status = 0;
  int *pdata_int = NULL;
  int fits_image_type;
  if ( pFile == NULL ||
       pCondList == NULL ||
       pTemplate == NULL ||
       pArm ==  NULL ||
       pData == NULL  ) return 1;

  switch ( extension_type ) 
  {
  case SIM_OUTPUT_CODE_SPECTRA_FLUX :
    pstr_extname = (char*) "FLUX";
    pstr_units   = (char*) "ct s^-1 pix^-1";
    fits_image_type = FLOAT_IMG;
    break;
  case SIM_OUTPUT_CODE_SPECTRA_FLUENCE :
    pstr_extname = (char*) "FLUENCE";
    pstr_units   = (char*) "ct pix^-1";
    fits_image_type = FLOAT_IMG;
    break;
  case SIM_OUTPUT_CODE_SPECTRA_SKY :
    pstr_extname = (char*) "SKY";
    pstr_units   = (char*) "ct pix^-1";
    fits_image_type = FLOAT_IMG;
    break;
  case SIM_OUTPUT_CODE_SPECTRA_TOTAL :
    pstr_extname = (char*) "TOTAL";
    pstr_units   = (char*) "ct pix^-1";
    fits_image_type = FLOAT_IMG;
    break;
  case SIM_OUTPUT_CODE_SPECTRA_NOISE :
    pstr_extname = (char*) "NOISE";
    pstr_units   = (char*) "ct pix^-1";
    fits_image_type = FLOAT_IMG;
    break;
  case SIM_OUTPUT_CODE_SPECTRA_SNR :
    pstr_extname = (char*) "SNR";
    pstr_units   = (char*) "pix^-1";
    fits_image_type = FLOAT_IMG;
    break;
  case SIM_OUTPUT_CODE_SPECTRA_POISSON :
    pstr_extname = (char*) "REALISATION";
    pstr_units   = (char*) "ct pix^-1";
    fits_image_type = LONG_IMG;  //32bit integers
    if ( pData2 == NULL  ) return 1; //check that we have the noise vector too
    break;
  default :
    fprintf(stderr, "%s Unknown spectral data extension type: %d\n",
            ERROR_PREFIX, extension_type);
    return 1;
    break;

  }
  naxes[0] = pArm->Lambda.num_pix;
  naxes[1] = pTemplate->num_mag * pCondList->num_cond * pCondList->num_exp;

  
  //make some space for the integer data TODO, and poissonise the float values
  if ( extension_type  == SIM_OUTPUT_CODE_SPECTRA_POISSON )
  {
    int i;
    int npix = naxes[0]*naxes[1] ;
    if ((pdata_int = (int*) malloc(sizeof(int) * npix)) == NULL)
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
    for(i=0;i<npix;i++)
    {
      pdata_int[i] = (int) ROUND(util_rand_gaussian(pData[i],pData2[i]));
      // pdata_int[i] = (int) util_poisson_rand(pData[i]);
      // pdata_int[i] = (int) (i % naxes[0]); //test for debugging
    }
  }

  if ( fits_create_img((fitsfile*) pFile, (int) fits_image_type, (int) naxis, (long*) naxes, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  //write all relevant keywords
  if (fits_write_key_str((fitsfile*) pFile, (char*) "EXTNAME",
                         (char*) pstr_extname,           (char*) "", &status) ||
      fits_write_key_str((fitsfile*) pFile, (char*) "BUNIT",
                         (char*) pstr_units,             (char*) "", &status) ||
      fits_write_key_str((fitsfile*) pFile, (char*) "CREATOR",
                         (char*) ETC_FITS_CREATOR_KEYWORD,         (char*) "", &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  
  // The WCS keywords depend on the dispersion type of the detector
  if ( pArm->Lambda.type == LAMBDA_TYPE_CODE_DISP_LINEAR )
  {
    const double crpix2 = 1.0;
    const double crval2 = 1.0;
    const double cdelt2 = 1.0;
    if (fits_write_key((fitsfile*) pFile, TSTRING, (char*) "CTYPE1",
                       (void*) "WAVE",               (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CRPIX1",
                       (void*) &pArm->Lambda.refpix, (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CRVAL1",
                       (void*) &pArm->Lambda.refval, (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CDELT1",
                       (void*) &pArm->Lambda.disp,   (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TSTRING, (char*) "CUNIT1",
                       (char*) "Angstrom",           (char*) "WCS description", &status)     ||
        fits_write_key((fitsfile*) pFile, TSTRING, (char*) "CTYPE2",
                       (void*) "LINEAR",             (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CRPIX2",
                       (void*) &crpix2,              (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CRVAL2",
                       (void*) &crval2,              (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CDELT2",
                       (void*) &cdelt2,              (char*) "WCS description", &status) )
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }
  else
  {
    const double zero = 0.0, one = 1.0;
    //write the WCS as a TAB- type
    if (fits_write_key((fitsfile*) pFile, TSTRING, (char*) "CTYPE1",
                       (void*) "WAVE-TAB",         (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CRPIX1",
                       (void*) &zero,              (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CRVAL1",
                       (void*) &zero,              (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "CD1_1",
                       (void*) &one,               (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TSTRING, (char*) "PS1_0",
                       (void*) "WAVETAB",          (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "PV1_1",
                       (void*) &one,               (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "PV1_2",
                       (void*) &one,               (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TSTRING, (char*) "PS1_1",
                       (void*) "LAMBDA",           (char*) "WCS description", &status) ||
        fits_write_key((fitsfile*) pFile, TDOUBLE, (char*) "PV1_3",
                       (void*) &one,               (char*) "WCS description", &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
 
  }
  
  if ( extension_type  == SIM_OUTPUT_CODE_SPECTRA_POISSON )
  {
    if ( fits_write_pix ((fitsfile*) pFile, (int) TINT, (long*) fpixel,
                         (LONGLONG) naxes[0]*naxes[1],
                         (int*) pdata_int, &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }
  else
  {
    if ( fits_write_pix ((fitsfile*) pFile, (int) TDOUBLE, (long*) fpixel,
                         (LONGLONG) naxes[0]*naxes[1],
                         (double*) pData, &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }

  if ( pdata_int ) {free(pdata_int); pdata_int = NULL;}
  return 0;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
//write table extensions with the source flux,fluence,sky,noise and snr
//write the spectra as a FITS binary table with vector columns for the spectral outputs : LAMBDA,VALUE1[NCOND*NMAG*NEXP]...
int write_spec_table_extension_to_chdu (fitsfile *pFile,
                                        sim_struct *pSim,
                                        condlist_struct *pCondList,
                                        template_struct *pTemplate,
                                        arm_struct *pArm,
                                        simspec_struct *pSimSpec,
                                        int a)   // a=arm index
{
  int i;
  int status = 0;
  int ncols = 0;
  char *ttype[SIM_OUTPUT_MAX_CODES]; 
  char *tform[SIM_OUTPUT_MAX_CODES];
  char *tunit[SIM_OUTPUT_MAX_CODES];
  char str_tform[SIM_OUTPUT_MAX_CODES][STR_MAX];
  int  colnum[SIM_OUTPUT_MAX_CODES];
  int  colfmt[SIM_OUTPUT_MAX_CODES];
  int *pData_int = NULL;
  double *pData[SIM_OUTPUT_MAX_CODES]; //pointers to data arrays
  int vector_col_width;

  if ( pFile     == NULL ||
       pSim      == NULL ||
       pCondList == NULL ||
       pTemplate == NULL ||
       pArm      ==  NULL ||
       pSimSpec  == NULL  ) return 1;

  for(i=0;i<SIM_OUTPUT_MAX_CODES;i++)
  {
    tform[i] = (char*) (str_tform[i]);
    colnum[i] = (int) i+1;
    pData[i] = NULL;
    colfmt[i] = (int) TDOUBLE;
  }

  vector_col_width = pTemplate->num_mag * pCondList->num_cond * pCondList->num_exp;

  //set up the columns 
  ncols = 0;

  //always add wavelength and delta wavelength columns
  ttype[ncols] = "LAMBDA";
  tform[ncols] = "1D";
  tunit[ncols] = "Angstrom";
  pData[ncols] = (double*) pArm->Lambda.pl;
  ncols ++;

  ttype[ncols] = "DLAMBDA";
  tform[ncols] = "1E";
  tunit[ncols] = "Angstrom";
  pData[ncols] = (double*) pArm->Lambda.pdl;
  ncols ++;
 
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_FLUX )
  {
    ttype[ncols] = "FLUX";
    sprintf(str_tform[ncols], "%dE", vector_col_width);
    tunit[ncols] = "ct s^-1 pix^-1";
    pData[ncols] = (double*) pSimSpec->ppFlux[a];
    ncols ++;
  }

  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_FLUENCE )
  {
    ttype[ncols] = "FLUENCE";
    sprintf(str_tform[ncols], "%dE", vector_col_width);
    tunit[ncols] = "ct pix^-1";
    pData[ncols] = (double*) pSimSpec->ppFluence[a];
    ncols ++;
  }
  
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_SKY )
  {
    ttype[ncols] = "SKY";
    sprintf(str_tform[ncols], "%dE", vector_col_width);
    tunit[ncols] = "ct pix^-1";
    pData[ncols] = (double*) pSimSpec->ppSky[a];
    ncols ++;
  }
  
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_TOTAL )
  {
    ttype[ncols] = "TOTAL";
    sprintf(str_tform[ncols], "%dE", vector_col_width);
    tunit[ncols] = "ct pix^-1";
    pData[ncols] = (double*) pSimSpec->ppTotal[a];
    ncols ++;
  }
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_NOISE )
  {
    ttype[ncols] = "NOISE";
    sprintf(str_tform[ncols], "%dE", vector_col_width);
    tunit[ncols] = "ct pix^-1";
    pData[ncols] = (double*) pSimSpec->ppNoise[a];
    ncols ++;
  }
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_SNR )
  {
    ttype[ncols] = "SNR";
    sprintf(str_tform[ncols], "%dE", vector_col_width);
    tunit[ncols] = "pix^-1";
    pData[ncols] = (double*) pSimSpec->ppSNR[a];
    ncols ++;
  }
  if ( pSim->output & (int) SIM_OUTPUT_CODE_SPECTRA_POISSON )
  {
    ttype[ncols] = "REALISATION";
    sprintf(str_tform[ncols], "%dJ", vector_col_width);
    tunit[ncols] = "ct pix^-1";
    pData[ncols] = NULL;
    colfmt[ncols] = (int) TINT;
    ncols ++;

    //make some space for the integer data, and poissonise the float values
    {
      int i;
      int npix = pArm->Lambda.num_pix*vector_col_width ;
      if ((pData_int = (int*) malloc(sizeof(int) * npix)) == NULL)
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      for(i=0;i<npix;i++)
      {
        //// TODO IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /// replace this with a re-entrant random number generator, and take care to optimise
        double x = util_rand_gaussian(pSimSpec->ppTotal[a][i],pSimSpec->ppNoise[a][i]);
        pData_int[i] = (int) ROUND(x);
        //        pData_int[i] = (int) (i % pArm->Lambda.num_pix); //test for debugging
      }
    }
  }


  if (  fits_create_tbl ((fitsfile*) pFile, (int) BINARY_TBL,
                         (LONGLONG) pArm->Lambda.num_pix, (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) "RESULTS",
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  //write all relevant keywords
  if (fits_write_key_str((fitsfile*) pFile, (char*) "CREATOR",
                         (char*) ETC_FITS_CREATOR_KEYWORD,         (char*) "", &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  //write the wavelength and wavelength step columns first
  {
    int j;
    j = 0;
    fits_write_col ((fitsfile*) pFile, (int) colfmt[j], (int) colnum[j], (LONGLONG) 1,
                    (LONGLONG) 1, (LONGLONG) pArm->Lambda.num_pix, (double*) pData[j], (int*) &status);
    j = 1;    
    fits_write_col ((fitsfile*) pFile, (int) colfmt[j], (int) colnum[j], (LONGLONG) 1,
                    (LONGLONG) 1, (LONGLONG) pArm->Lambda.num_pix, (double*) pData[j], (int*) &status);
  }
    //now write the other columns:
  //have to do this in awkward way becase the internal data arrays are
  // transposed from the order required for vector columns
  // to speed things up transpose the columns first in memory, then write out all elements per col per row in one go 
  {
    //some temp space for the transposed data
    double *pTemp_d = NULL;
    int    *pTemp_i = NULL;
    if ((pTemp_d = (double*) malloc(sizeof(double) * vector_col_width)) == NULL ||
        (pTemp_i = (int*)    malloc(sizeof(int)    * vector_col_width)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }

    for(i=0;i<pArm->Lambda.num_pix; i++)
    {
      int j;
      for(j=2;j<ncols;j++)  //start at the first non-wavelength column
      {
        int k;
        for(k=0;k<vector_col_width;k++)
        {
          int pix = i + k*pArm->Lambda.num_pix;
          if      ( colfmt[j] == (int) TDOUBLE ) pTemp_d[k] = (double) (pData[j][pix]);
          else if ( colfmt[j] == (int) TINT    ) pTemp_i[k] = (int)    (pData_int[pix]);
          else return 1;
        }

        fits_write_col ((fitsfile*) pFile, (int) colfmt[j], (int) colnum[j], (LONGLONG) i+1,
                        (LONGLONG) 1, (LONGLONG) vector_col_width,
                        (colfmt[j] == (int) TDOUBLE ? (void*) pTemp_d : (void*) pTemp_i),
                        (int*) &status);
        
      }
    }
    FREETONULL(pTemp_d);
    FREETONULL(pTemp_i);
  }
  
//  for(i=0;i<pArm->Lambda.num_pix; i++)
//  {
//    int k;
//    for(k=0;k<vector_col_width;k++)
//    {
//      int pix = i + k*pArm->Lambda.num_pix;
//      int j;
//      for(j=2;j<ncols;j++)  //start at the first non-wavelength column
//      {
//        if ( colfmt[j] == (int) TDOUBLE )
//        {
//          fits_write_col ((fitsfile*) pFile, (int) colfmt[j], (int) colnum[j], (LONGLONG) i+1,
//                          (LONGLONG) k+1, (LONGLONG) 1, (double*) &(pData[j][pix]),  (int*) &status);
//        }
//        else if ( colfmt[j] == (int) TINT )
//        {
//          fits_write_col ((fitsfile*) pFile, (int) colfmt[j], (int) colnum[j], (LONGLONG) i+1,
//                          (LONGLONG) k+1, (LONGLONG) 1, (int*) &(pData_int[pix]),  (int*) &status);
//        }
//        else
//        {
//          //do nothing
//        }
//      }
//    }
//  }
        
  FREETONULL(pData_int);
  return 0;
}




///////////////////////////////////////////////////////////////////////////////////////////////////
//only works when we have a pSimSpec->pCondList->num_exp == 1
int simspec_struct_update_texp_status ( simspec_struct *pSimSpec, int *pnum_search_spec)
{
  const int e = 0;
  int m;
  int num_search_spec = 0;

  if ( pSimSpec  == NULL ||
       pnum_search_spec == NULL ) return 1;
  if ( pSimSpec->pTemplate == NULL ) return 1;
  if ( pSimSpec->pCondList->num_exp != 1 ) return 2;
  
  for(m=0;m<pSimSpec->pTemplate->num_mag; m++ )
  {
    int c;
    for(c=0;c<pSimSpec->pCondList->num_cond; c++ )
    {
      //      int spec = (m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c;
      int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);

      BOOL is_successful = (BOOL) ( pSimSpec->pResult[spec] >= pSimSpec->pTemplate->pRuleSet->required_value ? TRUE : FALSE);

      switch ( pSimSpec->pstatus[spec] )
      {
      case CALC_TEXP_STATUS_START : //first run
        pSimSpec->plo[spec] = pSimSpec->ptexp[spec];
        pSimSpec->phi[spec] = pSimSpec->ptexp[spec];
        if ( is_successful )
          pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_SEARCH_DOWN;
        else
          pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_SEARCH_UP;
        break;

      case CALC_TEXP_STATUS_SEARCH_IN : //texp_lo is unsuccessful, texp_hi is successful
       //check that new texp lies between pSimSpec->plo and pSimSpec->phi
        if ( pSimSpec->ptexp[spec] > pSimSpec->plo[spec] &&
             pSimSpec->ptexp[spec] < pSimSpec->phi[spec] )
        {
          if ( is_successful )
            pSimSpec->phi[spec] = pSimSpec->ptexp[spec];
          else
            pSimSpec->plo[spec] = pSimSpec->ptexp[spec]; 
        }
        break;
        
      case CALC_TEXP_STATUS_SEARCH_UP : //texp_hi is unsuccessful
        if ( pSimSpec->ptexp[spec] > pSimSpec->phi[spec] )
        {
          pSimSpec->plo[spec] = pSimSpec->phi[spec];
          pSimSpec->phi[spec] = pSimSpec->ptexp[spec];
          if ( is_successful ) pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_SEARCH_IN;
          if ( pSimSpec->ptexp[spec] > CALC_TEXP_MAX_TEXP )
            pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_IMPOSSIBLE;
        }
        break;
        
      case CALC_TEXP_STATUS_SEARCH_DOWN : //texp_lo is successful
        if ( pSimSpec->ptexp[spec] < pSimSpec->plo[spec] )
        {
          pSimSpec->phi[spec] = pSimSpec->plo[spec];
          pSimSpec->plo[spec] = pSimSpec->ptexp[spec];
          if (! is_successful ) pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_SEARCH_IN;
          if ( pSimSpec->ptexp[spec] < CALC_TEXP_MIN_TEXP )
            pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_IMPOSSIBLE;
        }
        break;
        
      default :  // do nothing
        break;
      }
      
      //now check if we reached a solution
      if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_IN )
      {
        if ( (pSimSpec->phi[spec]-pSimSpec->plo[spec]) < CALC_TEXP_CONVERGE_CRITERION )
        {
          pSimSpec->pstatus[spec] = CALC_TEXP_STATUS_CONVERGED;  //success!
        }
      }

      if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_IN ||
           pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_UP ||
           pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_DOWN )
      {
        num_search_spec ++;  //recored numberof spec that will need to be iterated at leastr one more time

        //update the texp for the next iteration
        if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_IN )
          pSimSpec->ptexp[spec] = ((double)0.5)*(pSimSpec->plo[spec]+pSimSpec->phi[spec]);
        else if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_UP )
          //          pSimSpec->ptexp[spec] = ((double)2.0)*(pSimSpec->phi[spec]);
          pSimSpec->ptexp[spec] = ((double)2.0)*(pSimSpec->ptexp[spec]);
        else if ( pSimSpec->pstatus[spec] == CALC_TEXP_STATUS_SEARCH_DOWN )
          //          pSimSpec->ptexp[spec] = ((double)0.5)*(pSimSpec->plo[spec]);
          pSimSpec->ptexp[spec] = ((double)0.5)*(pSimSpec->ptexp[spec]);

        //calculate the number of exposures that will be needed to achieve this new texp
        pSimSpec->pnsub[spec] = (int) ceil(pSimSpec->ptexp[spec] / pSimSpec->pCondList->ptexp[0]);
        
      }

      
    }
  }
  *pnum_search_spec = (int) num_search_spec;
  return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
int simspec_struct_update_mag_status ( simspec_struct *pSimSpec, int *pnum_search_spec)
{
  const int m = 0;
  int e;
  int num_search_spec = 0;

  if ( pSimSpec  == NULL ||
       pnum_search_spec == NULL ) return 1;
  if ( pSimSpec->pTemplate == NULL ) return 1;
  if ( pSimSpec->pTemplate->num_mag != 1 )
  {
    fprintf(stderr, "%s When SIM.MODE=CALC_MAG I expect exactly one input magnitude for each template\n",
            ERROR_PREFIX);fflush(stderr);
    return 2;
  }
  for(e=0;e<pSimSpec->pCondList->num_exp; e++ )
  {
    int c;
    for(c=0;c<pSimSpec->pCondList->num_cond; c++ )
    {
      int spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c);
    //      int spec = (m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c;
      BOOL is_successful = (BOOL) ( pSimSpec->pResult[spec] >= pSimSpec->pTemplate->pRuleSet->required_value ? TRUE : FALSE);

      switch ( pSimSpec->pstatus[spec] )
      {
      case CALC_MAG_STATUS_START : //first run
        pSimSpec->plo[spec] = pSimSpec->pmag[spec];
        pSimSpec->phi[spec] = pSimSpec->pmag[spec];
        if ( is_successful )
          pSimSpec->pstatus[spec] = CALC_MAG_STATUS_SEARCH_UP;  //go fainter
        else
          pSimSpec->pstatus[spec] = CALC_MAG_STATUS_SEARCH_DOWN; //go brighter
        break;

      case CALC_MAG_STATUS_SEARCH_IN : //mag=plo[spec] is successful, mag=phi[spec] is unsuccessful
        //check that new mag lies between pSimSpec->plo[spec] and pSimSpec->phi[spec]
        if ( pSimSpec->pmag[spec] > pSimSpec->plo[spec] &&
             pSimSpec->pmag[spec] < pSimSpec->phi[spec] )
        {
          if ( is_successful )
            pSimSpec->plo[spec] = pSimSpec->pmag[spec];
          else
            pSimSpec->phi[spec] = pSimSpec->pmag[spec]; 
        }
        break;
        
      case CALC_MAG_STATUS_SEARCH_UP : //previous mag=phi[spec] was successful, so go fainter
        if ( pSimSpec->pmag[spec] > pSimSpec->phi[spec] )
        {
          pSimSpec->plo[spec] = pSimSpec->phi[spec];
          pSimSpec->phi[spec] = pSimSpec->pmag[spec];
          if (! is_successful ) pSimSpec->pstatus[spec] = CALC_MAG_STATUS_SEARCH_IN;  //new mag=phi[spec] was unsuccessful
          if ( pSimSpec->pmag[spec] > CALC_MAG_MAX_MAG )
            pSimSpec->pstatus[spec] = CALC_MAG_STATUS_IMPOSSIBLE;
        }
        break;
        
      case CALC_MAG_STATUS_SEARCH_DOWN : //previous mag=plo[spec] is unsuccessful, so go brighter
        if ( pSimSpec->pmag[spec] < pSimSpec->plo[spec] )
        {
          pSimSpec->phi[spec] = pSimSpec->plo[spec];
          pSimSpec->plo[spec] = pSimSpec->pmag[spec];
          if ( is_successful ) pSimSpec->pstatus[spec] = CALC_MAG_STATUS_SEARCH_IN;
          if ( pSimSpec->pmag[spec] < CALC_MAG_MIN_MAG )
            pSimSpec->pstatus[spec] = CALC_MAG_STATUS_IMPOSSIBLE;
        }
        break;
        
      default :  // do nothing
        break;
      }
      
      //now check if we reached a solution
      if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_IN )
      {
        if ( (pSimSpec->phi[spec] - pSimSpec->plo[spec]) < CALC_MAG_CONVERGE_CRITERION )
        {
          pSimSpec->pstatus[spec] = CALC_MAG_STATUS_CONVERGED;  //success!
        }
      }

      if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_IN ||
           pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_UP ||
           pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_DOWN )
      {
        num_search_spec ++;  //record number of spectra that must be iterated at least one more time

        //        //update the mag in the template struct so that can scale flux array from it in next iteration
        //        pSimSpec->pTemplate->pmag_list[0] = pSimSpec->pmag[spec];

        //update the mag for the next iteration
        if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_IN )
          pSimSpec->pmag[spec] = ((double)0.5)*(pSimSpec->plo[spec]+pSimSpec->phi[spec]);
        else if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_UP )
          pSimSpec->pmag[spec] = pSimSpec->pmag[spec] + (double) 1.0;
        else if ( pSimSpec->pstatus[spec] == CALC_MAG_STATUS_SEARCH_DOWN )
          pSimSpec->pmag[spec] = pSimSpec->pmag[spec] - (double) 1.0;

      }
      
    }
  }
  *pnum_search_spec = (int) num_search_spec;
  return 0;
}















///////////////////////////////////////////////////////////////////////////////////////////////////
//test a ruleset against each spectrum in a simspec struct,
//the result of each test is returned in the presult array
//presult must be long enough to hold all results 
//remember that rulesets can span arms
int simspec_struct_test_success ( simspec_struct      *pSimSpec)
{
  int m;
  simspec_single_struct SSS;
  
  if ( pSimSpec  == NULL ) return 1;
  //       presult   == NULL  ) return 1;
  if ( pSimSpec->pTemplate == NULL ) return 1;
  
  
  if ( simspec_single_struct_init ((simspec_single_struct*) &SSS )) return 1;

  if ( simspec_single_struct_alloc((simspec_single_struct*) &SSS,  (spectrograph_struct*) pSimSpec->pSpec)) return 1;

  
  for(m=0;m<pSimSpec->pTemplate->num_mag; m++ )
  {
    int e;
    for(e=0;e<pSimSpec->pCondList->num_exp; e++ )
    {
      int c;
      for(c=0;c<pSimSpec->pCondList->num_cond; c++ )
      {
        //        int spec = (m*pSimSpec->pCondList->num_exp + e)*pSimSpec->pCondList->num_cond + c;
        int spec;
        if ((spec = (int) simspec_struct_spec_index((simspec_struct*) pSimSpec, (int) m, (int) e, (int) c)) < 0) return 1;
        if ( simspec_single_struct_set ((simspec_single_struct*) &SSS,
                                        (simspec_struct*) pSimSpec,
                                        (int) m, (int) c, (int) e ))
        {
          fprintf(stderr, "%s I had problems when calling simspec_single_struct_set()\n", ERROR_PREFIX);
          return 1;
        }
        pSimSpec->pResult[spec] = (double) 0.0;
        if ( test_success_of_simspec_single ((simspec_single_struct*) &SSS,
                                             (double*)  &(pSimSpec->pResult[spec]),
                                             (double*)  pSimSpec->ppSubResult[spec]))
        {
          fprintf(stderr, "%s I had problems when calling test_success_of_simspec_single()\n", ERROR_PREFIX);
          return 1;
        }
      }
    }
  }
  if ( simspec_single_struct_free ((simspec_single_struct*) &SSS )) return 1;

  return 0;
}

//test a ruleset on a spectrum simulated through a single set of (condition,texp) params
int test_success_of_simspec_single ( simspec_single_struct *pSSS,
                                     double                *pResult,
                                     double                *pSubResult)
{
  int status = 0;
  int r;
  double ruleset_result = (double) 0.0;
  ruleset_struct *pRuleSet;
  if ( pSSS       == NULL ||
       pResult    == NULL ||
       pSubResult == NULL  ) return 1;

  pRuleSet = (ruleset_struct*) pSSS->pTemplate->pRuleSet;
  if ( pRuleSet->num_rules > 0 )
  {
    for(r=0; r<pRuleSet->num_rules; r++)  // for each rule which applies to this ruleset
    {
      double rule_result = (double) 0.0;
      if ( test_rule_on_simspec_single ((int) r, 
                                        (simspec_single_struct*) pSSS,
                                        (double*)                &rule_result))
      {
        fprintf(stderr, "%s I had problems when calling test_rule_on_simspec_single()\n", ERROR_PREFIX);
        return 1;
      } 
      //    ruleset_result += ((double) rule_result *  (double) pRuleSet->pweight[r]);
      pSubResult[r] = (double) rule_result;
      
      //copy the rule results into the pRuleSet->pExprFile
      //column number is r+2 because 1st column contains ruleset result
      fits_write_col ((fitsfile*) pRuleSet->pExprFile, (int) TDOUBLE,
                      (int) pRuleSet->ppRule[r]->expr_file_col_num, (LONGLONG) 1,
                      (LONGLONG) 1, (LONGLONG) 1, &(rule_result), (int*) &status);
      if ( status ) 
      {
        fits_report_error(stderr, status);  
        return 1;
      }
      

    }

    //write the redshift into the redshift column
    fits_write_col ((fitsfile*) pRuleSet->pExprFile, (int) TDOUBLE,
                    (int) RULESET_COLNUM_REDSHIFT, (LONGLONG) 1,
                    (LONGLONG) 1, (LONGLONG) 1, (double*) &(pSSS->pTemplate->redshift), (int*) &status);
    if ( status ) 
    {
      fits_report_error(stderr, status);  
      return 1;
    }

    
    //now evaluate the expression
    if ( fits_calculator((fitsfile*) pRuleSet->pExprFile, (char*) pRuleSet->str_expression,
                         (fitsfile*) pRuleSet->pExprFile, (char*) RULESET_COLNAME_RESULT,
                         NULL, (int*) &status) > 0 )
    {
      fits_report_error(stderr, status);  
      return 1;
    }
    //now extract the result value from the column
    
    if ( fits_read_col(pRuleSet->pExprFile, TDOUBLE, RULESET_COLNUM_RESULT, 1, 1, 1, NULL, &(ruleset_result), NULL, &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }
  else
  {
    ruleset_result = NAN;  //no rules to evaluate
  }
  
  
  *pResult = (double) ruleset_result;

  //  if ( pRuleSet->required_weight > (double) 0.0 ) 
//    *pResult = (double) (ruleset_result / (double) pRuleSet->required_weight);
//  else *pResult = (double) NAN;
  //  *pResult = (double) (ruleset_result /
  //                       (DBL_ISNOTZERO( (double) pRuleSet->required_weight) ? (double) pRuleSet->required_weight : (double) 1.0));
  
  return 0;
}

////////////////////////////////////////////////////////////////////
int test_rule_on_simspec_single   (int r, //rule_struct           *pRule,
                                   simspec_single_struct *pSSS,
                                   double                *presult)
{
  curve_struct crvRebin;
  rule_struct *pRule = NULL;
  double result;
  long i;
  
  if ( //pRule   == NULL ||
       pSSS    == NULL ||
       presult == NULL  ) return 1;

  assert(r >= 0 && r  < pSSS->pTemplate->pRuleSet->num_rules);

  pRule = (rule_struct*) pSSS->pTemplate->pRuleSet->ppRule[r];
  
  if ( curve_struct_init((curve_struct*) &crvRebin)) return 1;
  
  if ( simspec_single_struct_rebin_for_rule ( (simspec_single_struct*) pSSS,
                                              (int) r, // (rule_struct*)  pSSS->pTemplate->pRuleSet->ppRule[r], //pRule,
                                              (curve_struct*)          &crvRebin) > 0 )
  {
    fprintf(stderr, "%s I had problems when calling simspec_single_struct_rebin_for_rule()\n", ERROR_PREFIX);
    return 1;
  }
  
  if ( crvRebin.num_rows <= 0 )
  {
    //there were no bins in the rebinned curve 
    result = (double) 0.0;
    //    fprintf(stdout, "%s Number of bins in rebinned spectrum is zero(rule=%s)\n", WARNING_PREFIX, pRule->str_name);
  }
  else 
  {
    double measure = (double)0.0;
    double comp_val;
    //now go through rebinned curve and evaluate the rule
    switch ( pRule->metric )
    {
    case RULE_METRIC_CODE_MEAN :
      for ( i=0; i< crvRebin.num_rows; i++ )
      {
        if (!isnan(crvRebin.py[i]))
        {
          measure += (double)crvRebin.py[i];
        }
      }
      measure = (double)measure / (double)(crvRebin.num_rows);
      break;

    case RULE_METRIC_CODE_MIN :
      measure = (double)DBL_MAX;
      for ( i=0; i< crvRebin.num_rows; i++ )
      {
        if ( crvRebin.py[i] < measure )
        {
          measure = (double)crvRebin.py[i];
        }
      }
      break;

    case RULE_METRIC_CODE_MAX : 
      measure = (double)-DBL_MAX;
      for ( i=0; i< crvRebin.num_rows; i++ )
      {
        if (!isnan(crvRebin.py[i]))
        {
          if ( crvRebin.py[i] > measure )
          {
            measure = (double)crvRebin.py[i];
          }
        }
      }
      break;

    case RULE_METRIC_CODE_SUM :
      for ( i=0; i< crvRebin.num_rows; i++ )
      {
        if (!isnan(crvRebin.py[i]))
        {
          measure += (double)crvRebin.py[i];
        }
      }
      break;

    case RULE_METRIC_CODE_MEDIAN :   ///VERIFIED with tests///
    {
      double *ptemp = NULL;
      long nrows_valid = 0;
      if ( (ptemp = (double*) calloc(crvRebin.num_rows, sizeof(double))) == NULL )
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      for ( i=0; i< crvRebin.num_rows; i++ )
      {
        if (!isnan(crvRebin.py[i]))
        {
          ptemp[nrows_valid] = crvRebin.py[i];
          nrows_valid ++;
        }
      }
      if ( nrows_valid > 0 )
      {
        util_dsort( nrows_valid, ptemp);
        if ( nrows_valid % 2 == 1 )
        {
          long i1 = (long) (nrows_valid / 2);
          measure = ptemp[i1];
          //          fprintf(stdout, "%s MEDIAN odd nrows_valid=%6ld i1=%6ld ptemp[i1]=%10g measure=%10g\n",
          //                  DEBUG_PREFIX, nrows_valid, i1, ptemp[i1], measure); fflush(stdout);
        }
        else
        {
          long i2 = (long) (nrows_valid / 2); //find the bounding pixels in the array 
          long i1 = (long) i2 - 1;
          measure = (double) 0.5 * (double) ( ptemp[i1] + ptemp[i2] );
          //          fprintf(stdout, "%s MEDIAN even nrows_valid=%6ld i1=%6ld i2=%6ld ptemp[i1]=%10g ptemp[i2]=%10g measure=%10g\n",
          //                  DEBUG_PREFIX, nrows_valid, i1, i2, ptemp[i1],ptemp[i2], measure); fflush(stdout);
         }
      }
      else measure = NAN;

//      for ( i=0; i< crvRebin.num_rows; i++ )
//      {
//        fprintf(stdout, "%s RESAMPLED curve %6ld %10g %10g\n", DEBUG_PREFIX, i, crvRebin.py[i],  ptemp[i]); 
//      }
//      fflush(stdout);
      FREETONULL(ptemp);
      break;
    }
    case RULE_METRIC_CODE_PC : ///VERIFIED that this routine returns identical result to MEDIAN when pRule->percentile=0.5 ///
    {
      double *ptemp = NULL;
      long i1, i2, nrows_valid = 0;
      double pc_row;
      if ( (ptemp = (double*) calloc(crvRebin.num_rows, sizeof(double))) == NULL )
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      for ( i=0; i< crvRebin.num_rows; i++ )
      {
        if (!isnan(crvRebin.py[i]))
        {
          ptemp[nrows_valid] = crvRebin.py[i];
          nrows_valid ++;
        }
        //        else ptemp[i] = DBL_MAX;
      }
      if ( nrows_valid > 0 )
      {
        util_dsort( nrows_valid, ptemp);
        pc_row = (double) pRule->percentile * (double) (nrows_valid - 1);
        i1 = (long) floor(pc_row); //find the bounding pixels in the array 
        if ( i1 > nrows_valid )
        {
          measure = (double) 0.0;
        }
        else
        {

          //    i2 = (long) ceil(frac_rows);
          i2 = MIN(i1 + 1,nrows_valid-1); 
      
          //        if (!(i1 >= 0 && i1 < crvRebin.num_rows &&
          //              i2 >= 0 && i2 < crvRebin.num_rows ))
          //        {
          //          fprintf(stdout, "DEBUG i1=%ld i2=%ld crvRebin.num_rows=%ld pRule->str_name=\"%s\" pRule->percentile=%g\n", i1, i2, crvRebin.num_rows, pRule->str_name, pRule->percentile);
          //          rule_struct_print ((rule_struct*) pRule, (FILE*) stdout);
          //          fflush(stdout);
          //        }
      
          //    fprintf(stdout, "TEST %6ld %6ld %6ld %g\n", i1, i2, crvRebin.num_rows, pRule->percentile); fflush(stdout);
          assert(i1 >= 0 && i1 < nrows_valid );
          assert(i2 >= 0 && i2 < nrows_valid );
          //interpolate between the bounding locations properly:
          //        measure = util_interp_linear_1D_double ((double) (i1), (double) crvRebin.py[i1],
          //                                                (double) (i2), (double) crvRebin.py[i2], pc_row);
          measure = util_interp_linear_1D_double ((double) (i1), (double) ptemp[i1],
                                                  (double) (i2), (double) ptemp[i2], pc_row);
          //          fprintf(stdout, "%s PERCENTILE   nrows_valid=%6ld pc_row=%10.4f i1=%6ld i2=%6ld ptemp[i1]=%10g ptemp[i2]=%10g measure=%10g\n",
          //                  DEBUG_PREFIX, nrows_valid, pc_row, i1, i2, ptemp[i1],ptemp[i2], measure); fflush(stdout);
        }
      }
      else measure = NAN;        
      FREETONULL(ptemp);
    }
    break;
    case RULE_METRIC_CODE_NSAMPLE :
      {
        long nrows_valid = 0;
        for ( i=0; i< crvRebin.num_rows; i++ )
        {
          if (!isnan(crvRebin.py[i])) nrows_valid ++;
        }
        //        measure = (double) crvRebin.num_rows;
        measure = (double) nrows_valid;
      }
      break;
    default:
      fprintf(stderr, "%s I do not understand this RULE:METRIC code: %d\n",
              ERROR_PREFIX, pRule->metric);
      return 1;          
      break;
    }//end switch
    

    comp_val = (double) pRule->comparison_value;
    if ( pRule->special )
    {
      //      if ( strcmp(pRule->str_name, "ClusEll_special_b") == 0 ||
      //           strcmp(pRule->str_name, "ClusEll_special_r") == 0 )
      if ( strcmp(pRule->str_name, "ClusEll_special") == 0 )
      {
        comp_val = MIN((double)4.0, (pSSS->pTemplate->redshift * (double) 15.0) - (double) 3.0);
      }
    }
      
    //now compare the measure with the comparison_value
    switch ( pRule->operator )
    {
    case RULE_OPERATOR_CODE_EQ   : result = (double)(DBL_CMP(measure,comp_val) ? (double)1.0 : (double)0.0 ); break;
    case RULE_OPERATOR_CODE_GT   : result = (double)(measure >  comp_val       ? (double)1.0 : (double)0.0 ); break;
    case RULE_OPERATOR_CODE_GE   : result = (double)(measure >= comp_val       ? (double)1.0 : (double)0.0 ); break;
    case RULE_OPERATOR_CODE_LT   : result = (double)(measure <  comp_val       ? (double)1.0 : (double)0.0 ); break;
    case RULE_OPERATOR_CODE_LE   : result = (double)(measure <= comp_val       ? (double)1.0 : (double)0.0 ); break;
    case RULE_OPERATOR_CODE_DIV  : result = (double)(DBL_ISZERO(comp_val) ? NAN : (double)measure / (double)comp_val); break;
    case RULE_OPERATOR_CODE_MULT : result = (double)((double)measure * (double)comp_val);                     break;
    default :
      fprintf(stderr, "%s I do not understand this RULE:OPERATOR code: %d\n",
              ERROR_PREFIX, pRule->operator);
      return 1;
      break;
    }
  }

  *presult = (double ) result;
  if ( curve_struct_free((curve_struct*) &crvRebin)) return 1;
  
  return 0;
}
  
///////////////////////////////////////////////////////////////////////////////////////////////////



int fold_templatelist_through_norm_filters ( sim_struct* pSim,
                                             templatelist_struct *pTL,
                                             int num_filters,
                                             curve_struct* pcrvNormFilterList)
{  
  int i, f;
  if ( pSim == NULL ||
       pTL == NULL ||
       pcrvNormFilterList == NULL  ) return 1;

  for ( f=0;f<num_filters;f++)
  {
    if ( pcrvNormFilterList[f].num_rows <= 0 )
    {
      fprintf(stderr, "%s A normalising filter bandpass is not supplied\n", WARNING_PREFIX);
      return 0;
    }
  }
  
  for ( i=0; i < pTL->num_templates; i++)
  {
    template_struct * pTemplate = (template_struct*) &(pTL->pTemplate[i]);
    for ( f=0;f<num_filters;f++)
    {
      if ( template_struct_calc_mag_through_filter((template_struct*) pTemplate,
                                                   (curve_struct*) &(pcrvNormFilterList[f]),
                                                   (double*) &(pTemplate->mag_norm[f])))
      {
        fprintf(stderr, "%s Problem folding template through filter bandpass %d\n", ERROR_PREFIX, f+1);
        return 1;
      }
      if ( pSim->debug ) 
      {
        char str_format[STR_MAX];
        snprintf(str_format, STR_MAX,
                 "%%s TEMPLATE_MAG_CHECK index=%%4d/%%d name=%%-%ds: mag0=%%-7.4f mag_norm[%%d]=%%-7.4f (%%s)\n",
                 pSim->longest_template_name);
        
        fprintf(stdout, str_format, DEBUG_PREFIX,
                i+1, pTL->num_templates, pTemplate->str_name, pTemplate->mag0,
                f, pTemplate->mag_norm[f], pcrvNormFilterList[f].str_name);
        fflush(stdout);
      }
    }
  }

  return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// prepare a set of rebin_struct objects which instruct the code how to rebin the
// data in order to measure the rule.
// for each wavelength interval required by the rule, we need to know the list of input pixels (from each arm)
// and their weights (=wavelength overlap with the inteval) 

int templatelist_struct_prepare_rebinning ( templatelist_struct *pTL,
                                            spectrograph_struct *pSpec)
                                       
{
  int i;
  if ( pTL == NULL ||
       pSpec == NULL  ) return 1;
  
  for ( i=0; i < pTL->num_templates; i++)
  {
    if ( template_struct_prepare_rebinning ( (template_struct*) &(pTL->pTemplate[i]),
                                             (spectrograph_struct*) pSpec)) return 1;
  }
  return 0;
}

int template_struct_prepare_rebinning ( template_struct     *pTemplate,
                                        spectrograph_struct *pSpec)
{
  int r;
  if ( pTemplate == NULL ||
       pSpec     == NULL  ) return 1;

  pTemplate->num_rebin = pTemplate->pRuleSet->num_rules;
  if ((pTemplate->pRebin = (rebin_struct*) malloc(sizeof(rebin_struct)*pTemplate->num_rebin)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
   
  for(r=0;r<pTemplate->num_rebin;r++)
  {
    //    fprintf(stdout, "%s DEBUG: Preparing binning for template=%s rule=%s\n",
    //            DEBUG_PREFIX, pTemplate->str_name, pTemplate->pRuleSet->ppRule[r]->str_name); fflush(stdout);

    //    rule_struct_print ((rule_struct*) pTemplate->pRuleSet->ppRule[r], stdout);

    if (rebin_struct_init ( (rebin_struct*) &(pTemplate->pRebin[r])))
    {
      return 1;
    }
    if ( rebin_struct_prepare_rebinning((rebin_struct*) &(pTemplate->pRebin[r]),
                                        (rule_struct*) (pTemplate->pRuleSet->ppRule[r]),
                                        (spectrograph_struct*) pSpec,
                                        (double) pTemplate->redshift))
    {
      return 1;
    }
  }
  
  return 0;
}
  


///////////////////////////////////////////////////////////////////////////////////////////////////
// We want to rebin a spectrum to the intervals requested by a rule
// Treat the input spectrum as a piecewise constant function (i.e. as a histogram)
// In general we must treat the wavelength solution of both input and resampled curves as variable.
// However we can assume that wavelength solutions monotonically increase, so can step through each in order.
// Resampling function should calculate number of output bins and allocate space.
// For SNR we treat noise and signal separately
// For NOISE we sum components in quadrature

// In general we will need to test a rule across all spectrograph arms.
// If wavelength binning is chosen, then we sum the measurement in the wavelength bins covered by two arms 
// If pixel or resolution element binning is chosen, then we treat the arms separately 
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////
int rebin_struct_prepare_rebinning   (rebin_struct        *pRebin,
                                      rule_struct         *pRule,
                                      spectrograph_struct *pSpec,
                                      double               redshift)
{
  double zplus1;
  double lmin_aa;
  double lmax_aa;
  double deltal;

  if ( pRebin == NULL ||
       pRule  == NULL ||
       pSpec  == NULL ) return 1;
       
  if (isnan(redshift)) zplus1 = (double) 1.0;
  else                 zplus1 = (double) redshift + (double) 1.0;

  //now examine the rule struct and work out the number of required output bins

  //determine the starting and ending wavelengths in unified units - observed frame AA
  switch ( pRule->lambda_unit )
  {
  case RULE_LAMBDA_UNIT_CODE_AA :
    lmin_aa = pRule->lmin;
    lmax_aa = pRule->lmax;
    break;
  case RULE_LAMBDA_UNIT_CODE_NM :
    lmin_aa = pRule->lmin*CONVERT_NM_TO_AA;
    lmax_aa = pRule->lmax*CONVERT_NM_TO_AA;
    break;
  case RULE_LAMBDA_UNIT_CODE_UM :
    lmin_aa = pRule->lmin*CONVERT_UM_TO_AA;
    lmax_aa = pRule->lmax*CONVERT_UM_TO_AA;
    break;
  case RULE_LAMBDA_UNIT_CODE_REST_AA :
    lmin_aa = pRule->lmin * zplus1;
    lmax_aa = pRule->lmax * zplus1;
    break;
  case RULE_LAMBDA_UNIT_CODE_REST_NM :
    lmin_aa = pRule->lmin*CONVERT_NM_TO_AA * zplus1;
    lmax_aa = pRule->lmax*CONVERT_NM_TO_AA * zplus1;
    break;
  case RULE_LAMBDA_UNIT_CODE_REST_UM :
    lmin_aa = pRule->lmin*CONVERT_UM_TO_AA * zplus1;
    lmax_aa = pRule->lmax*CONVERT_UM_TO_AA * zplus1;
    break;
  default:
    fprintf(stderr, "%s I do not understand this RULE:LAMBDA_UNIT entry: %d\n",
            ERROR_PREFIX, pRule->lambda_unit);
    return 1;
    break;
  }

  //now work out what the delta wavelength step is
  switch ( pRule->deltal_unit )
  {
  case RULE_DELTAL_UNIT_CODE_AA :
    deltal = pRule->deltal;
    break;
  case RULE_DELTAL_UNIT_CODE_NM :
    deltal = pRule->deltal*CONVERT_NM_TO_AA ;
    break;
  case RULE_DELTAL_UNIT_CODE_UM :
    deltal = pRule->deltal*CONVERT_UM_TO_AA ;
    break;
  case RULE_DELTAL_UNIT_CODE_REST_AA :
    deltal = pRule->deltal * zplus1;
    break;
  case RULE_DELTAL_UNIT_CODE_REST_NM :
    deltal = pRule->deltal*CONVERT_NM_TO_AA * zplus1 ;
    break;
  case RULE_DELTAL_UNIT_CODE_REST_UM :
    deltal = pRule->deltal*CONVERT_UM_TO_AA * zplus1 ;
    break;
  case RULE_DELTAL_UNIT_CODE_PIX :
    deltal = pRule->deltal; //number of pixels per rebin step
    break;
  case RULE_DELTAL_UNIT_CODE_RES :
    deltal = pRule->deltal;  //number of res elements per rebin step
    break;
  default:
    fprintf(stderr, "%s I do not understand this RULE.DELTAL_UNIT entry: %d\n",
            ERROR_PREFIX, pRule->deltal_unit);
    return 1;
    break;
  }

  if ( deltal > (double) 0.0 )
  {
    //now work out the number of bins
    if ( pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_PIX ||
         pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_RES )
    {
      //count the number of pixels, then divide by the number of pixels per bin
      //or by the mean number of pixels per res element
      int a;
      int num_pix = 0;
      double mean_res_pix = 0.0;
      for (a=0; a<pSpec->num_arms; a++)
      {
        int i;
        lambda_struct *pLambda = (lambda_struct*) &(pSpec->pArm[a].Lambda);
        if ( lmin_aa > pLambda->max ||         //we don't overlap with this spec arm at all
             lmax_aa < pLambda->min) continue; //
        for( i = 0; i < pLambda->num_pix; i++ )
        {
          if ( pLambda->pl[i] >= lmin_aa &&
               pLambda->pl[i] <  lmax_aa )
          {
            num_pix ++;
            if ( pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_RES ) mean_res_pix += pLambda->pres_pix[i];
          }
        }
      }
      //divide by the number of pixels per step, allow for part-bins at ends
      if (num_pix > 0)
      {
        if (pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_RES )
        {
          mean_res_pix = mean_res_pix / (double) num_pix;
          assert(mean_res_pix > 0.0 );
          pRebin->num_bins = (int) (ceil((double) num_pix)/(deltal*mean_res_pix));// + (int) pSpec->num_arms;
        }
        else
          pRebin->num_bins = (int) (ceil((double) num_pix)/deltal);// + (int) pSpec->num_arms;
      }
      else  pRebin->num_bins = 0;
    }
    else  //just divide the full range by the delta wavelength
    {
      pRebin->num_bins = (int) ceil((lmax_aa - lmin_aa) / deltal);
    }
  }
  else
  {
    fprintf(stderr, "%s Error calculating resampling bin size: z=%g lmin_aa=%g lmax_aa=%g deltal=%g\n",
            ERROR_PREFIX, redshift, lmin_aa, lmax_aa, deltal);      
    return 1;
  }
  

  //now allocate space for the rebinning details
  if ( pRebin->num_bins < 0 )
  {
    fprintf(stderr, "%s Calculating resampling bin size: z=%g lmin_aa=%g lmax_aa=%g deltal=%g num_bins=%d\n",
            ERROR_PREFIX, redshift, lmin_aa, lmax_aa, deltal, pRebin->num_bins);      
    return -2;
  }
  else if ( pRebin->num_bins == 0 )
  {
    fflush(stdout);
    fprintf(stdout, "%s Calculating resampling bin size: z=%g lmin_aa=%g lmax_aa=%g deltal=%g num_bins=%d\n",
            WARNING_PREFIX, redshift, lmin_aa, lmax_aa, deltal, pRebin->num_bins);      
    fflush(stdout);
    return 0; // no rebinning to be done
  }
  else
  {
    fflush(stdout);
    fprintf(stdout, "%s Calculating resampling bin size: z=%g lmin_aa=%g lmax_aa=%g deltal=%g num_bins=%d\n",
            COMMENT_PREFIX, redshift, lmin_aa, lmax_aa, deltal, pRebin->num_bins);      
    fflush(stdout);
  }

  if ((pRebin->pNumPix    = (int*)     calloc((size_t) pRebin->num_bins, (size_t) sizeof(int)    )) == NULL ||
      (pRebin->ppPixIndex = (int**)    calloc((size_t) pRebin->num_bins, (size_t) sizeof(int*)   )) == NULL ||
      (pRebin->ppArmIndex = (int**)    calloc((size_t) pRebin->num_bins, (size_t) sizeof(int*)   )) == NULL ||
      (pRebin->ppOverlap  = (double**) calloc((size_t) pRebin->num_bins, (size_t) sizeof(double*))) == NULL ||
      (pRebin->pLmin      = (double*)  calloc((size_t) pRebin->num_bins, (size_t) sizeof(double) )) == NULL ||
      (pRebin->pLmax      = (double*)  calloc((size_t) pRebin->num_bins, (size_t) sizeof(double) )) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  
  //now count the number of pixels contributing to each resampled bin.
  if (  pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_PIX ||
        pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_RES )
  {
    //will treat pixels in overlapping arms separately
    int a;
    int j = 0;
    for(a=0;a<pSpec->num_arms; a++ )
    {
      lambda_struct *pLambda = (lambda_struct*) &(pSpec->pArm[a].Lambda);
      long imin0 = 0,imax0 = 0, imin, imax, imid;
      double fimin0, fimax0;
      double fimin, fimax;
      double this_deltal;
      double lmin_aa_clip;
      double lmax_aa_clip;

      if ( lmin_aa > pLambda->max ||         //we don't overlap with this spec arm at all
           lmax_aa < pLambda->min) continue; //

      //clip the lambda range so that we don't search outside the wavelength range of the arm
      lmin_aa_clip = MAX(lmin_aa,pLambda->min);
      lmax_aa_clip = MAX(lmax_aa,pLambda->max);

      if ( lmax_aa_clip - lmin_aa_clip <= 0.0 ) continue;      //we don't overlap with this spec arm at all
      
      //need to find the decimal pixel indices corresponding to lmin_aa and lmax_aa
      //first find the pixel index which includes lmin_aa;
      if ( util_binary_search_double ((long) pLambda->num_pix, (double*) pLambda->pl,
                                      (double) lmin_aa_clip, (long*) &imin0, NULL ) < 0 ) return 1;
      //avoid indexing off ends of array:
      imin0 = MIN(pLambda->num_pix-2,imin0);
      imin0 = MAX(0,imin0);
      
      // let fimin0 be decimal version of imin
      fimin0 = (double) util_interp_linear_1D_double ((double) pLambda->pl[imin0],   ((double) imin0),
                                                      (double) pLambda->pl[imin0+1], ((double) imin0)+1.0,
                                                      (double) lmin_aa_clip);

      //now find the pixel index which includes lmax_aa;
      if ( util_binary_search_double ((long) pLambda->num_pix, (double*) pLambda->pl,
                                      (double) lmax_aa_clip, (long*) &imax0, NULL ) < 0 ) return 1;
      //avoid indexing off ends of array:
      imax0 = MIN(pLambda->num_pix-2,imax0);
      imax0 = MAX(0,imax0);
      
      // let fimax0 be decimal version of imin
      fimax0 = (double) util_interp_linear_1D_double ((double) pLambda->pl[imax0],   ((double) imax0),
                                                      (double) pLambda->pl[imax0+1], ((double) imax0)+1.0,
                                                      (double) lmax_aa_clip);


      fimin = fimin0;
      if (  pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_RES )
        this_deltal = deltal * pLambda->pres_pix[imin0];
      else this_deltal = deltal;
      fimax = (double) MIN(fimax0, fimin0 + this_deltal);
      while ( j     <  pRebin->num_bins &&
              fimin < fimax0 )
      {
        //convert the decimal pixel position to integers
        imin = (int) MIN(pLambda->num_pix - 1, (int) floor(fimin));
        imax = (int) MIN(pLambda->num_pix - 1, (int) floor(fimax));
        imid = (int) 0.5*(imax + imid);
        pRebin->pNumPix[j] = imax - imin + 1;
        pRebin->ppPixIndex[j] = NULL;
        pRebin->ppArmIndex[j] = NULL;
        pRebin->ppOverlap[j]  = NULL;
        pRebin->pLmin[j]  = (double)fimin;
        pRebin->pLmax[j]  = (double)fimax;
        
        if ( pRebin->pNumPix[j] > 0 )
        {
          int k = 0, i;
          if ((pRebin->ppPixIndex[j] = (int*)    malloc(sizeof(int)    * pRebin->pNumPix[j])) == NULL ||
              (pRebin->ppArmIndex[j] = (int*)    malloc(sizeof(int)    * pRebin->pNumPix[j])) == NULL ||
              (pRebin->ppOverlap[j]  = (double*) malloc(sizeof(double) * pRebin->pNumPix[j])) == NULL )
          {
            fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
          }
          for(i=imin;i<=imax;i++)
          {
            double li = (double) i;
            double lii = (double) (i + 1);
            pRebin->ppPixIndex[j][k] = i;
            pRebin->ppArmIndex[j][k] = a;
            pRebin->ppOverlap[j][k] = (double) MAX ((double)0.0, MIN(fimax, lii) - MAX(li, fimin));
            k ++;
          }
        }

        // DEBUG
        //        fprintf(stdout, "DEBUGGY j=%6d (%8g:%8g) a=%2d imin=%4ld imax=%4ld pNumPix=%4d\n",
        //                j, fimin, fimax, a, imin, imax, pRebin->pNumPix[j]); fflush(stdout);
        
        j ++;
        //increment the decimal pixel position
        fimin =  (double) fimax;
        if (  pRule->deltal_unit == RULE_DELTAL_UNIT_CODE_RES )
          this_deltal = deltal * pLambda->pres_pix[imid];
        else
          this_deltal = deltal;
        fimax =  (double) MIN(fimax0, fimax + this_deltal);
      }
    }
    pRebin->num_bins = j;
  }
  else  //otherwise we should deal with things in wavelength space
  {
    int j;
    long *pimin = NULL, *pimax = NULL, *pnpix = NULL;
    //and now allocate some local helper arrays 
    if ((pimin = (long*)  calloc((size_t) pSpec->num_arms, (size_t) sizeof(long))) == NULL ||
        (pimax = (long*)  calloc((size_t) pSpec->num_arms, (size_t) sizeof(long))) == NULL ||
        (pnpix = (long*)  calloc((size_t) pSpec->num_arms, (size_t) sizeof(long))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
    
    for(j=0; j < pRebin->num_bins; j++)
    {
      int a;
      double lmin = (double) lmin_aa + (((double) j)      )*deltal ;
      double lmax = (double) lmin_aa + (((double) j) + 1.0)*deltal ;
      lmax = MIN(lmax_aa,lmax);
      pRebin->pNumPix[j] = 0;
      pRebin->ppPixIndex[j] = NULL;
      pRebin->ppArmIndex[j] = NULL;
      pRebin->ppOverlap[j]  = NULL;
      pRebin->pLmin[j]  = (double)lmin;
      pRebin->pLmax[j]  = (double)lmax;
      for(a=0;a<pSpec->num_arms; a++ )
      {
        lambda_struct *pLambda = (lambda_struct*) &(pSpec->pArm[a].Lambda);
        pimin[a] = 0;
        pimax[a] = 0;
        pnpix[a] = 0;
        if ( lmin > pLambda->max || lmax < pLambda->min) //if this bin doesn't overlap with this spec arm at all
        {
        }
        else
        {
          //find the pixel in the lambda array which had midpoint just shortward of lmin
          //TODO: speed up after first iteration by only searching forwards.
          if ( util_binary_search_double ((long) pLambda->num_pix, (double*) pLambda->pl,
                                          (double) lmin, (long*) &(pimin[a]), NULL ) < 0 ) return 1;
          
          //          fprintf(stdout, "DEBUGGY1 j=%6d (%8g:%8g) a=%2d pimin=%4ld pimax=%4ld npix=%4ld\n",
          //                  j, lmin, lmax, a, pimin[a], pimax[a], pnpix[a]);
          pimin[a] = MAX(0,pimin[a]);
          pimin[a] = MIN(pLambda->num_pix-1,pimin[a]);
          //          fprintf(stdout, "DEBUGGY2 j=%6d (%8g:%8g) a=%2d pimin=%4ld pimax=%4ld npix=%4ld\n",
          //                  j, lmin, lmax, a, pimin[a], pimax[a], pnpix[a]);
          
          //step backwards until find first pixel overlaping with interval  
          while( pimin[a] > 0 &&
                 pLambda->pl[pimin[a]] - 0.5*pLambda->pdl[pimin[a]] > lmin ) pimin[a]--; 
          
          //step forwards until find last pixel overlaping with interval 
          pimax[a] = pimin[a];
          while( pimax[a] < (pLambda->num_pix - 1) &&
                 pLambda->pl[pimax[a]] + 0.5*pLambda->pdl[pimax[a]] < lmax) pimax[a]++; 
          
          pnpix[a] = pimax[a] - pimin[a] + 1;
          //          fprintf(stdout, "DEBUGGY j=%6d (%8g:%8g) a=%2d pimin=%4ld pimax=%4ld npix=%4ld\n",
          //                  j, lmin, lmax, a, pimin[a], pimax[a], pnpix[a]);
        }
        pRebin->pNumPix[j] += pnpix[a];
      }

      if ( pRebin->pNumPix[j] > 0 )
      {
        int k = 0;
        if ((pRebin->ppPixIndex[j] = (int*)    malloc(sizeof(int)    * pRebin->pNumPix[j])) == NULL ||
            (pRebin->ppArmIndex[j] = (int*)    malloc(sizeof(int)    * pRebin->pNumPix[j])) == NULL ||
            (pRebin->ppOverlap[j]  = (double*) malloc(sizeof(double) * pRebin->pNumPix[j])) == NULL )
        {
          fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
          return 1;
        }

        for(a=0; a<pSpec->num_arms; a++ )
        {
          int i;
          lambda_struct *pLambda = (lambda_struct*) &(pSpec->pArm[a].Lambda);
          if ( pnpix[a] > 0 )
          {
            for (i=pimin[a]; i<=pimax[a]; i++)
            {
              double li  = (pLambda->pl[i]-(double)0.5*pLambda->pdl[i]);
              double lii = (pLambda->pl[i]+(double)0.5*pLambda->pdl[i]);
              pRebin->ppPixIndex[j][k] = i;
              pRebin->ppArmIndex[j][k] = a;
              pRebin->ppOverlap[j][k] = (double) MAX ((double)0.0, MIN(lmax, lii) - MAX(li, lmin))/ pLambda->pdl[i];
              k ++;
            }
          }
        }
      }
    }
    if (pimin) {free(pimin); pimin = NULL;}
    if (pimax) {free(pimax); pimax = NULL;}
    if (pnpix) {free(pnpix); pnpix = NULL;}
  }

//  //DEBUG
//  {
//    int j,i;
//    //print the rebinning details
//    fprintf(stdout, "%s DEBUG PREP REBIN details: lmin_aa=%g lmax_aa=%g deltal=%g num_bins=%d lambda_unit=%s deltal_unit=%s\n",
//            COMMENT_PREFIX, lmin_aa, lmax_aa, deltal, pRebin->num_bins,
//            RULE_LAMBDA_UNIT_STRING(pRule->lambda_unit),
//            RULE_DELTAL_UNIT_STRING(pRule->deltal_unit));
//    fprintf(stdout, "%s DEBUG PREP REBIN details: %6s %6s (%8s:%8s): [%2s,%1s,%4s,%8s,%4s|%4s]\n",
//            COMMENT_PREFIX, "j", "npix", "lmin", "lmax", "i","a","pix","lmid","dl","over");
//    for(j=0;j< pRebin->num_bins;j++)
//    {
//      fprintf(stdout, "%s DEBUG PREP REBIN details: %6d %6d (%8g:%8g): ",
//              COMMENT_PREFIX, j, pRebin->pNumPix[j], pRebin->pLmin[j], pRebin->pLmax[j]);
//      if ( pRebin->pNumPix[j] > 0 )
//      {
//        for(i=0;i<pRebin->pNumPix[j];i++)
//        {
//          int a = pRebin->ppArmIndex[j][i];
//          fprintf(stdout, "[%2d,%d,%4d,%8.2f,%4.2f|%4.2f] ",
//                  i,
//                  a,
//                  pRebin->ppPixIndex[j][i],
//                  pSpec->pArm[a].Lambda.pl[pRebin->ppPixIndex[j][i]],
//                  pSpec->pArm[a].Lambda.pdl[pRebin->ppPixIndex[j][i]],
//                  pRebin->ppOverlap[j][i]);
//          fflush(stdout);
//        }
//      }
//      fprintf(stdout, "\n");
//      fflush(stdout);
//    }
//  }
  
  return 0;
}





int simspec_single_struct_rebin_for_rule ( simspec_single_struct *pSSS,
                                           int r, 
                                           curve_struct          *pCurve)
{
  int a,j;
  double **ppVar1 = NULL;             // array of pointers to variable arrays
  double **ppVar2 = NULL;             // array of pointers to variable arrays
  spectrograph_struct *pSpec = NULL;  // shortcut pointer
  template_struct *pTemplate = NULL;  // shortcut pointer
  rebin_struct *pRebin = NULL;        // shortcut pointer
  rule_struct *pRule = NULL;          // shortcut pointer
  if ( pSSS   == NULL ||
       pCurve == NULL ) return 1;

  pSpec     = (spectrograph_struct*) pSSS->pSpec; // assign shortcut pointer
  pTemplate = (template_struct*) pSSS->pTemplate; // assign shortcut pointer
  
  if ( curve_struct_init ((curve_struct*) pCurve)) return 1;

  if ((ppVar1 = (double**) calloc((size_t) pSpec->num_arms, (size_t) sizeof(double*))) == NULL ||
      (ppVar2 = (double**) calloc((size_t) pSpec->num_arms, (size_t) sizeof(double*))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //  //find out which rule/rebin we are interested in
  //  for(r=0;r<pTemplate->pRuleSet->num_rules;r++)
  //  {
  //    if((rule_struct*) (pTemplate->pRuleSet->ppRule[r]) == (rule_struct*) pRule) break; 
  //  }
  assert(r >= 0 && r  < pTemplate->pRuleSet->num_rules);
  pRule = (rule_struct*) pTemplate->pRuleSet->ppRule[r]; // assign shortcut pointer
  pRebin = (rebin_struct*) &(pTemplate->pRebin[r]);      // assign shortcut pointer

  for (a=0; a<pSpec->num_arms; a++)
  {
    arm_struct *pArm  = (arm_struct*) &(pSpec->pArm[a]);
    ppVar1[a] = NULL;
    ppVar2[a] = NULL;
    switch ( pRule->variable )
    {
    case RULE_VARIABLE_CODE_FLUX :
      ppVar1[a] = (double*) pSSS->ppFlux[a];
      break;
    case RULE_VARIABLE_CODE_FLUENCE :
      ppVar1[a] = (double*) pSSS->ppFluence[a];
      break;
    case RULE_VARIABLE_CODE_TOTAL_FLUENCE :
      ppVar1[a] = (double*) pSSS->ppTotal[a];
      break;
    case RULE_VARIABLE_CODE_NOISE :
      ppVar1[a] = (double*) pSSS->ppNoise[a];
      break;
    case RULE_VARIABLE_CODE_NOISE_CGS :
      return 1;   //not yet implemented   TODO
      break;
    case RULE_VARIABLE_CODE_SKY_FLUX :
      ppVar1[a] = (double*) pSSS->ppSky[a];
      break;
    case RULE_VARIABLE_CODE_SKY_FLUENCE :
      ppVar1[a] = (double*) pSSS->ppSky[a];
      break;
    case RULE_VARIABLE_CODE_SNR         :
      //      ppVar1[a] = (double*) pSSS->ppSNR[a]; 
      ppVar1[a] = (double*) pSSS->ppFluence[a];    //we need to know signal and noise separately in order to rebin
      ppVar2[a] = (double*) pSSS->ppNoise[a];
      break;
    case RULE_VARIABLE_CODE_RESOLUTION  :
      ppVar1[a] = (double*) pArm->Rsp.pres;
      break;
    case RULE_VARIABLE_CODE_TEXP :  //no action needed
    case RULE_VARIABLE_CODE_NSUB : 
      break;
    default:
      return 1;
      break;
    }
  }

  
  pCurve->num_rows = pRebin->num_bins;
  if ( pRebin->num_bins> 0 )
  {
    if ((pCurve->py = (double*) calloc((size_t) pCurve->num_rows, (size_t) sizeof(double))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  
  //follow the rebinning params already calculated for this template+rule combination
  for(j=0;j<pRebin->num_bins;j++)
  {
    double y1 = (double) 0.0;
    double y2 = (double) 0.0;
    //    double y3 = (double) 0.0;
    double sum_weight =(double) 0.0;
    int i;
    if ( pRebin->pNumPix[j] > 0 )
    {
      for(i=0;i<pRebin->pNumPix[j];i++)
      {
        int a = pRebin->ppArmIndex[j][i];
        double weight = pRebin->ppOverlap[j][i];
        int ii = pRebin->ppPixIndex[j][i];
        //        fprintf(stdout, "%s j=%d/%d i=%d/%d a=%d/%d ii=%d pSpec->pArm[a].Lambda.num_pix=%ld\n",
        //                DEBUG_PREFIX, j, pRebin->num_bins, i, pRebin->pNumPix[j], a,pSpec->num_arms, ii, pSpec->pArm[a].Lambda.num_pix);
        fflush(stdout);
        assert(a >= 0);
        assert(a < pSpec->num_arms); 
        assert(ii >= 0);
        assert(a < pSpec->pArm[a].Lambda.num_pix);
        switch ( pRule->variable )
        {
        case RULE_VARIABLE_CODE_FLUX        :  //for these we want the sum, taking account of fractional overlaps
        case RULE_VARIABLE_CODE_FLUENCE     :
        case RULE_VARIABLE_CODE_SKY_FLUENCE :
        case RULE_VARIABLE_CODE_TOTAL_FLUENCE :
          y1 += weight * ppVar1[a][ii];        //add up the values, multiplied by fraction of pixel that overlaps with interval
          break;
        case RULE_VARIABLE_CODE_SKY_FLUX    : //add up the values, multiplied by fraction of pixel that overlaps with interval
          y1 += weight * ppVar1[a][ii] / (MAX(DBL_MIN,pSSS->texp));  // and divide by exposure time

          break;
        case RULE_VARIABLE_CODE_RESOLUTION  :  //for these we want a weighted mean, weighted by overlap in AA
          y1 += weight * ppVar1[a][ii];
          break;
        case RULE_VARIABLE_CODE_NOISE     :
        case RULE_VARIABLE_CODE_NOISE_CGS :
          //          y1 += SQR(weight * ppVar1[a][ii]);   //add up the weighted squares of the noise values
          y1 += weight * SQR(ppVar1[a][ii]);   //add up the weighted squares of the noise values
          break;
        case RULE_VARIABLE_CODE_SNR       :    //for these we need to add up both the signal and the noise separately 
          if (ppVar2[a][ii] > 0.0 ) //only consider points with well defined errors
          {
            weight = pRebin->ppOverlap[j][i] / SQR(ppVar2[a][ii]);   // weight per pixel = overlap * sigma^-2
            y1 += weight * ppVar1[a][ii];
            y2 += weight;
          }
          else  weight = 0.0;
          break;
        case RULE_VARIABLE_CODE_TEXP :
          y1 += weight * pSSS->texp;          //add up the total exposure time, taking account of fractional overlaps
          break;
        case RULE_VARIABLE_CODE_NSUB :
          y1 += weight * (double) pSSS->nsub; //add up the number of subexposures, taking account of fractional overlaps
          break;

        default:
          return 1;
          break;
        }
        sum_weight += weight;
      }
    }

    switch ( pRule->variable )
    {
      ///these rule variables require the sum
    case RULE_VARIABLE_CODE_FLUX        :     
    case RULE_VARIABLE_CODE_FLUENCE     :
    case RULE_VARIABLE_CODE_SKY_FLUX    :
    case RULE_VARIABLE_CODE_SKY_FLUENCE :
    case RULE_VARIABLE_CODE_TOTAL_FLUENCE :
      pCurve->py[j] = y1;   //we actually wanted the sum, so just use y1 directly 
      break;
      ///these rule variables require the mean = sum/summed_weight
    case RULE_VARIABLE_CODE_RESOLUTION  :
    case RULE_VARIABLE_CODE_TEXP :
    case RULE_VARIABLE_CODE_NSUB :
      if ( sum_weight > 0.0 ) pCurve->py[j] = (double) y1 / sum_weight;   //take a weighted mean
      else                    pCurve->py[j] = (double) 0.0;
      break;
      ///these rule variables require the sqrt(sum)
    case RULE_VARIABLE_CODE_NOISE : 
    case RULE_VARIABLE_CODE_NOISE_CGS : 
      pCurve->py[j] = (double) sqrt(y1);  //we want the sqrt of the summed weighted squared noise 
      break;
    case RULE_VARIABLE_CODE_SNR   :  
      if ( sum_weight > 0.0 ) y1 = (double) y1 / sum_weight;   //take a weighted mean
      else                    y1 = (double) 0.0;
          //      y3 = (double) sqrt(1./y2);  //now first take the sqrt of the summed squared noise 
      if      ( y2 > (double) 0.0)  pCurve->py[j] = (double) y1 / sqrt(1.0/y2); //then take the ratio
      else if ( y1 > (double) 0.0)  pCurve->py[j] = (double) DBL_MAX;
      else                          pCurve->py[j] = (double) 0.0;
      break;
    default:
      return 1;
      break;
    }
  }

//  //DEBUG
//  {
//    int j;
//    //print the rebinning details
//    fprintf(stdout, "%s DEBUG REBIN details: %6s %6s (%8s:%8s): %8s\n",
//            COMMENT_PREFIX, "j", "NumPix", "Lmin[j]", "Lmax[j]", "y[j]");
//    for(j=0;j< pRebin->num_bins;j++)
//    {
//      fprintf(stdout, "%s DEBUG REBIN details: %6d %6d (%8g:%8g): %8g\n",
//              COMMENT_PREFIX, j, pRebin->pNumPix[j], pRebin->pLmin[j], pRebin->pLmax[j], pCurve->py[j]);
//      fflush(stdout);
//    }
//    
//  }

  if (ppVar1) {free(ppVar1); ppVar1 = NULL;}
  if (ppVar2) {free(ppVar2); ppVar2 = NULL;}
  
  
  return 0;
}


//https://heasarc.gsfc.nasa.gov/xanadu/xspec/manual/XSweight.html
//note that readnoise is actually Gaussian distributed but here is treated as a Poisson noise component
double calc_noise_gehrels_style (double signal, double bkgnd, double sq_read_noise, double skysub_residual )         
{
  return (double) (1.0 +
                   sqrt((double) 0.75 + signal + bkgnd + sq_read_noise) +
                   skysub_residual * bkgnd);
}

int simspec_struct_spec_index ( simspec_struct *pSimSpec, int mag_index, int exp_index, int cond_index)
{
  if ( pSimSpec  == NULL ) return -1;
  if ( mag_index  < 0 || mag_index  >= pSimSpec->pTemplate->num_mag)  return -1;
  if ( exp_index  < 0 || exp_index  >= pSimSpec->pCondList->num_exp)  return -1;
  if ( cond_index < 0 || cond_index >= pSimSpec->pCondList->num_cond) return -1;

  return (int) ((mag_index*pSimSpec->pCondList->num_exp + exp_index)*pSimSpec->pCondList->num_cond + cond_index);
}


/*
//---------------------------------------------------------------------------------------------------
//resample_struct functions
int resample_struct_init ( resample_struct *pResample )
{
  if ( pResample == NULL ) return 1;
  strncpy(pResample->str_mode, "", STR_MAX);
  pResample->mode  = (int) RESAMPLE_MODE_UNKNOWN;
  pResample->min   = (double) NAN;
  pResample->delta = (double) NAN;
  pResample->num_pix = (long) 0;
  return 0;
}


//resample all spectra in the simspec_struct according to defined format,
//results in one spectrum per template+condition
int simspec_struct_resample ( simspec_struct* pSimSpec)
{
  rebin_struct Rebin;
  rule_struct  Rule;
  //  curve_struct Curve;
  if ( pSimSpec == NULL ) return 1;

  if ( rebin_struct_init ( (rebin_struct*) &Rebin)) return 1;
  if ( rule_struct_init  ( (rule_struct*)  &Rule )) return 1;
  //  if ( curve_struct_init ( (curve_struct*) &Curve)) return 1;

  switch(pSimSpec->pResample->mode) 
  {
  case RESAMPLE_MODE_LINEAR_LAMBDA : 
    Rule.lambda_unit = RULE_LAMBDA_UNIT_CODE_AA;
    Rule.lambda_unit = RULE_LAMBDA_UNIT_CODE_AA;
    Rule.deltal_unit = RULE_DELTAL_UNIT_CODE_AA;
    Rule.lmin  = pSimSpec->pResample->min;
    Rule.lmax  = pSimSpec->pResample->min + pSimSpec->pResample->num_pix * pSimSpec->pResample->delta;
    Rule.delta = pSimSpec->pResample->delta;
    break;
    //TODO add other modes
  default :
    return 1;
    break;
  }
  
  if ( rebin_struct_prepare_rebinning((rebin_struct*) &(Rebin),
                                      (rule_struct*) &(Rule),
                                      (spectrograph_struct*) pSimSpec->pSpec,
                                      (double) NAN))
  {
    return 1;
  }

  //the following is based on simspec_single_struct_rebin_for_rule()
  {
    spectrograph_struct *pSpec =(spectrograph_struct*) pSimSpec->pSpec;  // shortcut pointer
    rebin_struct        *pRebin = (rebin_struct*) &Rebin;
    int a,j;

    
    if (pSimSpec->ppFlux   ) {if((pSimSpec->pResFlux  =(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL ) return 1;}
    if (pSimSpec->ppFluence) {if(pSimSpec->pResFluence=(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL  ) return 1;}
    if (pSimSpec->ppSky    ) {if(pSimSpec->pResSky    =(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL ) return 1;}
    if (pSimSpec->ppTotal  ) {if(pSimSpec->pResTotal  =(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL ) return 1;}
    if (pSimSpec->ppNoise  ) {if(pSimSpec->pNoise     =(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL ) return 1;}
    if (pSimSpec->ppSNR    ) {if(pSimSpec->pResSNR    =(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL ) return 1;}
   if (pSimSpec->ppResNorm ) {if(pSimSpec->pResNorm   =(double*)calloc((size_t)pSimSpec->num_spec*pRebin->num_bins,(size_t)sizeof(double))) == NULL ) return 1;}
   
    
    //now do the resampling for all spectra
    for(s=0;s<pSimSpec->num_spec;s++)
    {
      
      //follow the rebinning params already calculated for this template+rule combination
      for(j=0;j<pRebin->num_bins;j++)
      {
        double y1_noise = (double) 0.0;
        double y1_snr   = (double) 0.0;
        double y2_snr   = (double) 0.0;
        //        double y3 = (double) 0.0;
        double sum_weight =(double) 0.0;
        int i;
        int out_pix = s*pRebin->num_bins + j;
        if ( pRebin->pNumPix[j] > 0 )
        {
          for(i=0;i<pRebin->pNumPix[j];i++)
          {
            int a = pRebin->ppArmIndex[j][i];
            double weight = pRebin->ppOverlap[j][i];
            int ii = pRebin->ppPixIndex[j][i];
            assert(a >= 0 && a < pSpec->num_arms);
            assert(ii >= 0 && a < pSpec->pArm[a].Lambda.num_pix);

            if ( pSimSpec->pResFlux)    pSimSpec->pResFlux[out_pix]    +=  weight * pSimSpec->ppFlux[a][ii]; 
            if ( pSimSpec->pResFluence) pSimSpec->pResFluence[out_pix] +=  weight * ppVar1[a][ii]; 
            if ( pSimSpec->pResSky)     pSimSpec->pResSky[out_pix]     +=  weight * ppVar1[a][ii]; 
            if ( pSimSpec->pResTotal)   pSimSpec->pTotal[out_pix]      +=  weight * ppVar1[a][ii]; 
            if ( pSimSpec->pNoise)      pSimSpec->pNoise[out_pix]      +=  SQR(weight * ppVar1[a][ii]);
            sum_weight += weight;
          }
        }

        if ( pSimSpec->pNoise) pSimSpec->pNoise[out_pix] = sqrt(pSimSpec->pNoise[out_pix]);

      }
    }
        

  
  if ( rebin_struct_free ((rebin_struct*) &Rebin)) return 1;
  //  if ( curve_struct_free ((curve_struct*) &Curve)) return 1;

  return 0;
}
//---------------------------------------------------------------------------------------------------
*/


//double calc_snr (double signal, double bkgnd, double sq_read_noise )         
//{
//  return (double) signal / calc_noise((double)signal, (double) bkgnd, (double) sq_read_noise);         
//}

///////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// END OF FUNCTIONS ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
