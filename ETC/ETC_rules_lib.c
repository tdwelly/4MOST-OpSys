//----------------------------------------------------------------------------------
//--
//--    Filename: ETC_rules_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.2 $"
//#define CVS_DATE     "$Date: 2015/12/03 11:31:34 $"
//--    Note: Definitions are in the corresponding header file ETC_rules_lib.h
//--    Description:
//--      Used to handle sky emission and transmission structures
//--


#include <string.h>
#include <math.h>
#include <float.h>
#include <fitsio.h>
#include <ctype.h>

#include "utensils_lib.h"
#include "ETC_rules_lib.h"

//a few defintions that we do not want to escape beyond the scope of this file
#define COMMENT_PREFIX "#-ETC_rules_lib:Comment: "
#define WARNING_PREFIX "#-ETC_rules_lib:Warning: "
#define ERROR_PREFIX   "#-ETC_rules_lib:Error:   "
#define DEBUG_PREFIX   "#-ETC_rules_lib:Debug:   "
#define DATA_PREFIX    ""

////////////////////////////////////////////////////////////////////////
//functions
int ETC_rules_lib_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------
//rule_struct functions
int rule_struct_init ( rule_struct *pRule )
{
  if ( pRule == NULL ) return 1;
  strncpy(pRule->str_name, "", STR_MAX);
  pRule->variable = (int) RULE_VARIABLE_CODE_UNKNOWN;
  pRule->metric   = (int) RULE_METRIC_CODE_UNKNOWN;
  pRule->operator = (int) RULE_OPERATOR_CODE_UNKNOWN;
  pRule->comparison_value = (double) NAN;

  pRule->percentile = (double) NAN;
  pRule->lmin = (double) NAN;
  pRule->lmax = (double) NAN;
  pRule->lambda_unit = (int) RULE_LAMBDA_UNIT_CODE_UNKNOWN; 
  pRule->deltal = (double) NAN;
  pRule->deltal_unit = (int) RULE_DELTAL_UNIT_CODE_UNKNOWN; 
  pRule->special = (BOOL) FALSE;
  pRule->expr_file_col_num = 0;
  return 0;
}

int rulelist_struct_init ( rulelist_struct **ppRuleList )
{
  if ( ppRuleList == NULL ) return 1;
  if ( *ppRuleList == NULL )
  {
    //make space for the struct
    if ( (*ppRuleList = (rulelist_struct*) malloc(sizeof(rulelist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  strncpy((*ppRuleList)->str_filename, "", STR_MAX);
  (*ppRuleList)->num_rules = 0;
  (*ppRuleList)->pRule = NULL;
  return 0;
}

int rulelist_struct_free ( rulelist_struct **ppRuleList )
{
  if ( ppRuleList == NULL ) return 1;
  if ( *ppRuleList )
  {
    if ( (*ppRuleList)->pRule )
    {
      free((*ppRuleList)->pRule);
      (*ppRuleList)->pRule = NULL;
    }
    free(*ppRuleList);
    *ppRuleList = NULL;
  }
  return 0;
}


int ruleset_struct_init ( ruleset_struct *pRuleSet )
{
  if ( pRuleSet == NULL ) return 1;
  strncpy(pRuleSet->str_name, "", STR_MAX);
  pRuleSet->num_rules = (int) 0;
  pRuleSet->ppRule = NULL;
  //  pRuleSet->pweight = NULL;
  //  pRuleSet->required_weight = (double) NAN;
  pRuleSet->required_value = (double) NAN;
  pRuleSet->str_expression = NULL;
  pRuleSet->pExprFile = NULL;
  return 0;
}

int ruleset_struct_free ( ruleset_struct *pRuleSet )
{
  if ( pRuleSet)
  {
    FREETONULL(pRuleSet->ppRule);
    FREETONULL(pRuleSet->str_expression);
  }
  return 0;
}

int rulesetlist_struct_init ( rulesetlist_struct **ppRuleSetList )
{
  if ( ppRuleSetList == NULL ) return 1;
  if ( *ppRuleSetList == NULL )
  {
    //make space for the struct
    if ( (*ppRuleSetList = (rulesetlist_struct*) malloc(sizeof(rulesetlist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  strncpy((*ppRuleSetList)->str_filename, "", STR_MAX);
  (*ppRuleSetList)->num_rulesets = 0;
  (*ppRuleSetList)->pRuleSet = NULL;
  (*ppRuleSetList)->pExprFile = NULL;
  return 0;
}

int rulesetlist_struct_free ( rulesetlist_struct **ppRuleSetList )
{
  if ( ppRuleSetList == NULL ) return 1;
  if ( *ppRuleSetList )
  {
    if ( (*ppRuleSetList)->pRuleSet )
    {
      int r;
      for(r=0;r<(*ppRuleSetList)->num_rulesets;r++)
      {
        if ( ruleset_struct_free((ruleset_struct*) &((*ppRuleSetList)->pRuleSet[r]))) return 1;
      }
      FREETONULL((*ppRuleSetList)->pRuleSet);
      //      free((*ppRuleSetList)->pRuleSet);
      //      (*ppRuleSetList)->pRuleSet = NULL;
      if ( (*ppRuleSetList)->pExprFile)
      {
        int status = 0;
        fits_close_file((*ppRuleSetList)->pExprFile, &status);
      }
    }
    FREETONULL(*ppRuleSetList);
    //    free(*ppRuleSetList);
    //    *ppRuleSetList = NULL;
  }
  return 0;
}


// go through the input 
//expect the following format
// RULE_NAME  VARIABLE METRIC OPERATOR COMP_VALUE  LAMBDA_MIN  LAMBDA_MAX LAMBDA_UNIT DELTAL DELTAL_UNIT
// e.g.
// QSOBlueCont     SNR MEDIAN       GE        5.0      2000.0      3000.0          AA    1.0          AA
// QSOHbeta        SNR MEDIAN       GE       10.0      4851.3      4871.3          AA    3.0          AA
// all fields are required
int rulelist_struct_read_from_file ( rulelist_struct *pRuleList )
{
  FILE *pFile = NULL;
  long i;
  char buffer[STR_MAX];
  if ( pRuleList == NULL ) return 1;
  
  if ( strlen(pRuleList->str_filename) == 0 ||
       strcmp(pRuleList->str_filename, "NONE") == 0 )
  {
    fprintf(stdout, "%s No rulelist specified\n", WARNING_PREFIX); fflush(stdout);
    return 0;
  }
  
  //open the file for reading
  if ((pFile = fopen(pRuleList->str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the file: %s\n", ERROR_PREFIX, pRuleList->str_filename);
    fflush(stderr);
    return 1;
  }

  fprintf(stdout, "%s %s Reading rulelist list from file\n", DEBUG_PREFIX, pRuleList->str_filename); fflush(stdout);

    
  //scan through the file to determine the number of lines
  i = 0;
  while (fgets((char*) buffer, (int) STR_MAX, (FILE*) pFile))
  {
    int j = 0;
    buffer[STR_MAX - 1] = '\0'; //safety stop
    while( isspace(buffer[j])) j ++; //scan past leading white space
    if ( buffer[j] != '#' &&  buffer[j] != '\0' )  //ignore comment lines
    {      
      i ++;
    }
  }
  //go back to the start
  rewind((FILE*)pFile);
  pRuleList->num_rules = i;
 
  fprintf(stdout, "%s %s contains %d interesting lines\n",
          DEBUG_PREFIX, pRuleList->str_filename, pRuleList->num_rules); fflush(stdout);
  if ( pRuleList->num_rules <= 0 )
  {
    fprintf(stderr, "%s I found zero valid lines in the ascii file: %s\n", ERROR_PREFIX, pRuleList->str_filename);
    fflush(stderr);
    return 1;
  }

  //allocate and init space for rules
  if ((pRuleList->pRule = (rule_struct*) malloc(sizeof(rule_struct) * pRuleList->num_rules)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  for ( i = 0; i< pRuleList->num_rules; i++)
  {
    if ( rule_struct_init ( (rule_struct*) &(pRuleList->pRule[i])))
    {
      return 1;
    }
  }

  
  
  //now read in the rules
  //expect the following format
  // RULE_NAME  VARIABLE METRIC OPERATOR COMP_VALUE  LAMBDA_MIN  LAMBDA_MAX LAMBDA_UNIT DELTAL DELTAL_UNIT
  {
    char str_format[STR_MAX] = "%s %s %s %s %lf %lf %lf %s %lf %s"; 
    int line = 0;
    i = 0;
    while (fgets((char*) buffer, (int) STR_MAX, (FILE*) pFile))
    {
      int j = 0;
      line ++;
      buffer[STR_MAX - 1] = '\0'; //safety stop
      while( isspace(buffer[j])) j ++; //scan past leading white space
      if ( j < STR_MAX )
      {
        if ( buffer[j] != '#' && buffer[j] != '\0' )  //ignore comment and empty lines
        {
          int n_tokens;
          rule_struct *pRule = (rule_struct*) &(pRuleList->pRule[i]) ;
          char str_variable[STR_MAX];
          char str_metric[STR_MAX];
          char str_operator[STR_MAX];
          char str_lambda_unit[STR_MAX];
          char str_deltal_unit[STR_MAX];
          n_tokens = sscanf(buffer, (char*) str_format,
                            (char*) pRule->str_name,
                            (char*) str_variable,
                            (char*) str_metric,
                            (char*) str_operator,
                            (double*) &(pRule->comparison_value),
                            (double*) &(pRule->lmin),
                            (double*) &(pRule->lmax),
                            (char*) str_lambda_unit,
                            (double*) &(pRule->deltal),
                            (char*) str_deltal_unit);

          if ( n_tokens != 10 ) 
          {
            fprintf(stderr, "%s I found an invalid entry (line=%d) in the rulelist file: %s\n",
                    ERROR_PREFIX, line, pRuleList->str_filename);
            fprintf(stderr, "%s line has %d tokens: %s\n", ERROR_PREFIX, n_tokens, buffer);
            return 1;
            //emit error?
          }

          //now convert the strings to integer macro values

          ///////////////////////////////////////////////////
          //variable
          {
            const char *plist_names[]  = {"FLUX", "PHOTON_FLUX",
                                          "FLUENCE", "SIGNAL", 
                                          "NOISE", "SIGMA", "ERR",
                                          "NOISE_CGS", "SIGMA_CGS", "ERR_CGS",
                                          "SKY_FLUX",
                                          "SKY_FLUENCE", "SKY", 
                                          "TOTAL_FLUENCE", "TOTAL", 
                                          "SNR",
                                          "RESOLUTION",
                                          "TEXP","EXPOSURE",
                                          "NSUB", "NEXP"};
            const int   plist_values[] = {RULE_VARIABLE_CODE_FLUX, RULE_VARIABLE_CODE_FLUX,
                                          RULE_VARIABLE_CODE_FLUENCE, RULE_VARIABLE_CODE_FLUENCE,
                                          RULE_VARIABLE_CODE_TOTAL_FLUENCE, RULE_VARIABLE_CODE_TOTAL_FLUENCE,
                                          RULE_VARIABLE_CODE_NOISE, RULE_VARIABLE_CODE_NOISE, RULE_VARIABLE_CODE_NOISE,
                                          RULE_VARIABLE_CODE_NOISE_CGS,RULE_VARIABLE_CODE_NOISE_CGS,RULE_VARIABLE_CODE_NOISE_CGS,
                                          RULE_VARIABLE_CODE_SKY_FLUX,
                                          RULE_VARIABLE_CODE_SKY_FLUENCE, RULE_VARIABLE_CODE_SKY_FLUENCE,
                                          RULE_VARIABLE_CODE_SNR,
                                          RULE_VARIABLE_CODE_RESOLUTION,
                                          RULE_VARIABLE_CODE_TEXP, RULE_VARIABLE_CODE_TEXP,
                                          RULE_VARIABLE_CODE_NSUB, RULE_VARIABLE_CODE_NSUB};
            int len = (int) (sizeof(plist_values)/sizeof(int));
            if (util_select_option_from_string(len, plist_names, plist_values,
                                               str_variable,  &(pRule->variable)))
            {
              fprintf(stderr, "%s I do not understand this RULE:VARIABLE entry: \"%s\"\n",
                      ERROR_PREFIX, str_variable);fflush(stderr);
              return 1;
            }
          }
          ///////////////////////////////////////////////////

          ///////////////////////////////////////////////////
          //metric
          {
            char str_metric_orig[STR_MAX];
            strncpy(str_metric_orig, str_metric, STR_MAX);
            //first check for percentiles
            pRule->metric = RULE_METRIC_CODE_UNKNOWN;
            pRule->percentile = (double) NAN;
            if ( strlen(str_metric) >= 3 )
            {
              if ( (char) toupper(str_metric[strlen(str_metric)-2]) == (char) 'P'  &&
                   (char) toupper(str_metric[strlen(str_metric)-1]) == (char) 'C'  )
              {
                pRule->metric = RULE_METRIC_CODE_PC;
                str_metric[strlen(str_metric)-2] = '\0';
              }
              //              fprintf(stdout, "METRIC PC orig=\"%s\" now=\"%s\" \n", str_metric_orig, str_metric); fflush(stdout);
            }
            if ( pRule->metric != RULE_METRIC_CODE_PC &&  strlen(str_metric) >= 2 )
            {
              if ( str_metric[strlen(str_metric)-1] == '%' )
              {
                pRule->metric = RULE_METRIC_CODE_PC;
                str_metric[strlen(str_metric)-1] = '\0';
              }
              //              fprintf(stdout, "METRIC %% orig=\"%s\" now=\"%s\" \n", str_metric_orig, str_metric); fflush(stdout);
            }
            
            if ( pRule->metric == RULE_METRIC_CODE_PC ) 
            {
              if ( sscanf(str_metric, "%lf", &(pRule->percentile)) != 1 )
              {
                fprintf(stderr, "%s I do not understand this RULE:METRIC percentile entry: \"%s\"\n",
                      ERROR_PREFIX, str_metric_orig);
                return 1;
              }
              pRule->percentile = pRule->percentile / 100.0;
              if ( pRule->percentile > 1.0 || pRule->percentile < 0.0 || isnan(pRule->percentile))
              {
                fprintf(stderr, "%s I do not understand this RULE:METRIC percentile entry: \"%s\"\n",
                      ERROR_PREFIX, str_metric_orig);
                return 1;
              }
            }
            else
            {
              const char *plist_names[]  = {"MEAN",
                                            "MEDIAN",
                                            "MIN" ,
                                            "MAX" ,
                                            "SUM" ,
                                            "NSAMPLE"};
              const int   plist_values[] = {RULE_METRIC_CODE_MEAN  ,
                                            RULE_METRIC_CODE_MEDIAN,
                                            RULE_METRIC_CODE_MIN   ,
                                            RULE_METRIC_CODE_MAX   ,
                                            RULE_METRIC_CODE_SUM   ,
                                            RULE_METRIC_CODE_NSAMPLE};
              int len = (int) (sizeof(plist_values)/sizeof(int));
              if (util_select_option_from_string(len, plist_names, plist_values,
                                                 str_metric,  &(pRule->metric)))
              {
                fprintf(stderr, "%s I do not understand this RULE:METRIC entry: \"%s\"\n",
                        ERROR_PREFIX, str_metric_orig);
                return 1;
              }
              if ( pRule->metric == RULE_METRIC_CODE_MEDIAN )
              {
                pRule->percentile = (double) 0.50;
              //                pRule->metric = RULE_METRIC_CODE_PC;
              }
              
            }
          }
          ///////////////////////////////////////////////////

          ///////////////////////////////////////////////////
          //operator
          {
            const char *plist_names[]  = {"EQ",   "==",
                                          "GT",   ">" ,
                                          "GE",   ">=",
                                          "LT",   "<" ,
                                          "LE",   "<=",
                                          "DIV",  "/" ,
                                          "MULT", "*"};
            const int   plist_values[] = {RULE_OPERATOR_CODE_EQ  , RULE_OPERATOR_CODE_EQ  ,
                                          RULE_OPERATOR_CODE_GT  , RULE_OPERATOR_CODE_GT  ,
                                          RULE_OPERATOR_CODE_GE  , RULE_OPERATOR_CODE_GE  ,
                                          RULE_OPERATOR_CODE_LT  , RULE_OPERATOR_CODE_LT  ,
                                          RULE_OPERATOR_CODE_LE  , RULE_OPERATOR_CODE_LE  ,
                                          RULE_OPERATOR_CODE_DIV , RULE_OPERATOR_CODE_DIV ,
                                          RULE_OPERATOR_CODE_MULT, RULE_OPERATOR_CODE_MULT};
            
            int len = (int) (sizeof(plist_values)/sizeof(int));
            if (util_select_option_from_string(len, plist_names, plist_values,
                                               str_operator,  &(pRule->operator)))
            {
              fprintf(stderr, "%s I do not understand this RULE:OPERATOR entry: \"%s\"\n",
                      ERROR_PREFIX, str_operator);fflush(stderr);
              return 1;
            }
          }
          ///////////////////////////////////////////////////

          ///////////////////////////////////////////////////
          //lambda_unit
          {
            const char *plist_names[]  = {"AA", "OBS_AA", "AA_OBS",
                                          "NM", "OBS_NM", "NM_OBS",
                                          "UM", "OBS_UM", "UM_OBS",
                                          "REST_AA","AA_REST",
                                          "REST_NM","NM_REST",
                                          "REST_UM","UM_REST"};
            const int   plist_values[] = {RULE_LAMBDA_UNIT_CODE_AA, RULE_LAMBDA_UNIT_CODE_AA, RULE_LAMBDA_UNIT_CODE_AA,
                                          RULE_LAMBDA_UNIT_CODE_NM, RULE_LAMBDA_UNIT_CODE_NM, RULE_LAMBDA_UNIT_CODE_NM,
                                          RULE_LAMBDA_UNIT_CODE_UM, RULE_LAMBDA_UNIT_CODE_UM, RULE_LAMBDA_UNIT_CODE_UM,
                                          RULE_LAMBDA_UNIT_CODE_REST_AA, RULE_LAMBDA_UNIT_CODE_REST_AA,
                                          RULE_LAMBDA_UNIT_CODE_REST_NM, RULE_LAMBDA_UNIT_CODE_REST_NM,
                                          RULE_LAMBDA_UNIT_CODE_REST_UM, RULE_LAMBDA_UNIT_CODE_REST_UM};
            int len = (int) (sizeof(plist_values)/sizeof(int));
            if (util_select_option_from_string(len, plist_names, plist_values,
                                               str_lambda_unit,  &(pRule->lambda_unit)))
            {
              fprintf(stderr, "%s I do not understand this RULE:LAMBDA_UNIT entry: \"%s\"\n",
                      ERROR_PREFIX, str_lambda_unit);fflush(stderr);
              return 1;
            }
          }
          ///////////////////////////////////////////////////

          ///////////////////////////////////////////////////
          //deltal_unit
          {
            const char *plist_names[]  = {"PIX", "PIXEL",
                                          "RES", "RESOLUTION",
                                          "AA", "OBS_AA", "AA_OBS",
                                          "NM", "OBS_NM", "NM_OBS",
                                          "UM", "OBS_UM", "UM_OBS",
                                          "REST_AA","AA_REST",
                                          "REST_NM","NM_REST",
                                          "REST_UM","UM_REST"};
            const int   plist_values[] = {RULE_DELTAL_UNIT_CODE_PIX, RULE_DELTAL_UNIT_CODE_PIX,
                                          RULE_DELTAL_UNIT_CODE_RES, RULE_DELTAL_UNIT_CODE_RES,
                                          RULE_DELTAL_UNIT_CODE_AA, RULE_DELTAL_UNIT_CODE_AA, RULE_DELTAL_UNIT_CODE_AA,
                                          RULE_DELTAL_UNIT_CODE_NM, RULE_DELTAL_UNIT_CODE_NM, RULE_DELTAL_UNIT_CODE_NM,
                                          RULE_DELTAL_UNIT_CODE_UM, RULE_DELTAL_UNIT_CODE_UM, RULE_DELTAL_UNIT_CODE_UM,
                                          RULE_DELTAL_UNIT_CODE_REST_AA, RULE_DELTAL_UNIT_CODE_REST_AA,
                                          RULE_DELTAL_UNIT_CODE_REST_NM, RULE_DELTAL_UNIT_CODE_REST_NM,
                                          RULE_DELTAL_UNIT_CODE_REST_UM, RULE_DELTAL_UNIT_CODE_REST_UM};
            int len = (int) (sizeof(plist_values)/sizeof(int));
            if (util_select_option_from_string(len, plist_names, plist_values,
                                               str_deltal_unit,  &(pRule->deltal_unit)))
            {
              fprintf(stderr, "%s I do not understand this RULE:DELTAL_UNIT entry: \"%s\"\n",
                      ERROR_PREFIX, str_deltal_unit);fflush(stderr);
              return 1;
            }
          }
          ///////////////////////////////////////////////////


//          if ( ParLib_str_to_BOOL (str_restframe_flag, (BOOL*) &(pRule->restframe_flag)))
//          {
//            fprintf(stderr, "%s I do not understand this RULE:RESTFRAME_FLAG entry: \"%s\"\n",
//                    ERROR_PREFIX, str_restframe_flag);fflush(stderr);
//            return 1;
//          }
          i ++;
        }
      }
    }
  }


  //close the file
  if ( pFile ) fclose ( pFile) ;
  pFile = NULL;

  
  fprintf(stdout, "%s %s contained %ld (valid) lines\n", DEBUG_PREFIX, pRuleList->str_filename, i); fflush(stdout);
  if ( i <= 0 )
  {
    fprintf(stderr, "%s I read zero valid rule lines from the file: %s\n", ERROR_PREFIX, pRuleList->str_filename);
    fflush(stderr);
    return 1;
  }

 
  //tidy up 
  if ( i < pRuleList->num_rules )
  {
    pRuleList->num_rules = i;
    //reallocate space 
    if ((pRuleList->pRule = (rule_struct*) realloc((rule_struct*) pRuleList->pRule,
                                                   sizeof(rule_struct) * pRuleList->num_rules)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  //now add the special rules

  //add space for the special rules
  if ( rulelist_struct_add_specials ((rulelist_struct*) pRuleList))
  {
    return 1;
  }
    
  
  if ( rulelist_struct_print ((rulelist_struct*) pRuleList, stdout))
  {
    return 1;
  }

  
  return 0;
}



int rulelist_struct_add_specials (rulelist_struct* pRuleList)
{
  const int num_specials = 1; 
  rule_struct *pRule = NULL;
  int i;
  if ( pRuleList == NULL ) return 1;

  i = pRuleList->num_rules;
  pRuleList->num_rules += num_specials;

  if ((pRuleList->pRule = (rule_struct*) realloc((rule_struct*) pRuleList->pRule,
                                                 sizeof(rule_struct) * pRuleList->num_rules)) == NULL )
  {
    fprintf(stderr, "%s Problem re-assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

//  //#RULE_NAME        VARIABLE  METRIC OPERATOR COMP_VALUE  LAMBDA_MIN  LAMBDA_MAX LAMBDA_UNIT DELTAL DELTAL_TYPE
//  //ClusEll_zlt0.467b      SNR    MEDIAN     GE        4.0      4125.0      4375.0     REST_AA     1.0     OBS_AA        
//  //ClusEll_zlt0.467r      SNR    MEDIAN     GE        4.0      5200.0      5450.0     REST_AA     1.0     OBS_AA        
//  ///////////////////////////////////////////////////////////////////////
//  pRule = (rule_struct*) &(pRuleList->pRule[i]) ;
//  if ( rule_struct_init ( (rule_struct*) pRule )) return 1;
//  strncpy(pRule->str_name, "ClusEll_special_b", STR_MAX);
//  pRule->variable = (int) RULE_VARIABLE_CODE_SNR;
//  pRule->metric   = (int) RULE_METRIC_CODE_MEDIAN;
//  pRule->percentile = (double) 0.5;
//  pRule->operator = (int) RULE_OPERATOR_CODE_GE;
//  pRule->comparison_value = (double) NAN;  ////this is a function of z, so set it per template////
//  pRule->lmin = (double) 4125.0;
//  pRule->lmax = (double) 4375.0;
//  pRule->lambda_unit = (int) RULE_LAMBDA_UNIT_CODE_REST_AA; 
//  pRule->deltal = (double) 1.0;
//  pRule->deltal_unit = (int) RULE_DELTAL_UNIT_CODE_AA; 
//  pRule->special = (BOOL) TRUE;
//  ///////////////////////////////////////////////////////////////////////
//
//  ///////////////////////////////////////////////////////////////////////
//  i++;
//  pRule = (rule_struct*) &(pRuleList->pRule[i]) ;
//  if ( rule_struct_init ( (rule_struct*) pRule )) return 1;
//  strncpy(pRule->str_name, "ClusEll_special_r", STR_MAX);
//  pRule->variable = (int) RULE_VARIABLE_CODE_SNR;
//  pRule->metric   = (int) RULE_METRIC_CODE_MEDIAN;
//  pRule->percentile = (double) 0.5;
//  pRule->operator = (int) RULE_OPERATOR_CODE_GE;
//  pRule->comparison_value = (double) NAN;  ////this is a function of z, so set it per template ////
//  pRule->lmin = (double) 5200.0;
//  pRule->lmax = (double) 5450.0;
//  pRule->lambda_unit = (int) RULE_LAMBDA_UNIT_CODE_REST_AA; 
//  pRule->deltal = (double) 1.0;
//  pRule->deltal_unit = (int) RULE_DELTAL_UNIT_CODE_AA; 
//  pRule->special = (BOOL) TRUE;
//  ///////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////
  pRule = (rule_struct*) &(pRuleList->pRule[i]) ;
  if ( rule_struct_init ( (rule_struct*) pRule )) return 1;
  strncpy(pRule->str_name, "ClusEll_special", STR_MAX);
  pRule->variable = (int) RULE_VARIABLE_CODE_SNR;
  pRule->metric   = (int) RULE_METRIC_CODE_MEDIAN;
  pRule->percentile = (double) 0.5;
  pRule->operator = (int) RULE_OPERATOR_CODE_GE;
  pRule->comparison_value = (double) NAN;  ////this is a function of z, so set it per template////
  pRule->lmin = (double) 4000.0;
  pRule->lmax = (double) 4500.0;
  pRule->lambda_unit = (int) RULE_LAMBDA_UNIT_CODE_REST_AA; 
  pRule->deltal = (double) 1.0;
  pRule->deltal_unit = (int) RULE_DELTAL_UNIT_CODE_AA; 
  pRule->special = (BOOL) TRUE;
  ///////////////////////////////////////////////////////////////////////

  
  return 0;
}


//---------------------------------------------------------------------------------------------------
int rule_struct_print (rule_struct *pRule, FILE* pFile)
{
  char str_metric[STR_MAX];
  if ( pFile == NULL ||
       pRule == NULL ) return 1;
  if ( pRule->metric == RULE_METRIC_CODE_PC ) snprintf(str_metric, STR_MAX,"%gPC",  pRule->percentile*(double)100.);
  else                                        snprintf(str_metric, STR_MAX,"%s", RULE_METRIC_STRING(pRule->metric));
  fprintf(pFile, "%s %8s %8s %8s %8g %10.7g %10.7g %11s %10.7g %11s\n",
          (char*) pRule->str_name,
          (char*) RULE_VARIABLE_STRING(pRule->variable),
          (char*) str_metric,
          (char*) RULE_OPERATOR_STRING(pRule->operator),
          (double) pRule->comparison_value,
          (double) pRule->lmin,
          (double) pRule->lmax,
          (char*) RULE_LAMBDA_UNIT_STRING(pRule->lambda_unit),
          (double) pRule->deltal,
          (char*) RULE_DELTAL_UNIT_STRING(pRule->deltal_unit));
            
  return 0;
}
//---------------------------------------------------------------------------------------------------
int rulelist_struct_print (rulelist_struct *pRuleList, FILE* pFile)
{
  int r;
  int max_name_length = 8;
  char str_format[STR_MAX];
  if ( pFile == NULL ||
       pRuleList == NULL ) return 1;
  for (r=0; r<pRuleList->num_rules;r++)
  {
    rule_struct *pRule = (rule_struct*) &(pRuleList->pRule[r]) ;
    max_name_length = MAX ( max_name_length, strlen(pRule->str_name)); 
  }

  sprintf(str_format, "#%%-%ds %%8s %%8s %%8s %%8s %%10s %%10s %%11s %%10s %%11s\n", max_name_length - 1);
  fprintf(pFile, str_format,
          "RULE_NAME", "VARIABLE", "METRIC", "OPERATOR", "COMP_VAL",
          "LAMBDA_MIN", "LAMBDA_MAX", "LAMBDA_UNIT", "DELTAL", "DELTAL_UNIT");
  sprintf(str_format, "%%-%ds %%8s %%8s %%8s %%8g %%10.7g %%10.7g %%11s %%10.7g %%11s\n", max_name_length);
  for (r=0; r<pRuleList->num_rules;r++)
  {
    char str_metric[STR_MAX];
    rule_struct *pRule = (rule_struct*) &(pRuleList->pRule[r]) ;
    if ( pRule->metric == RULE_METRIC_CODE_PC ) snprintf(str_metric, STR_MAX, "%gPC",  pRule->percentile*100.);
    else                                        snprintf(str_metric, STR_MAX,"%s", RULE_METRIC_STRING(pRule->metric));
    fprintf(pFile, str_format,
            (char*) pRule->str_name,
            (char*) RULE_VARIABLE_STRING(pRule->variable),
            (char*) str_metric,
            (char*) RULE_OPERATOR_STRING(pRule->operator),
            (double) pRule->comparison_value,
            (double) pRule->lmin,
            (double) pRule->lmax,
            (char*) RULE_LAMBDA_UNIT_STRING(pRule->lambda_unit),
            (double) pRule->deltal,
            (char*) RULE_DELTAL_UNIT_STRING(pRule->deltal_unit));
            
  }
  return 0;
}
//---------------------------------------------------------------------------------------------------
int rulesetlist_struct_print (rulesetlist_struct *pRuleSetList, FILE* pFile)
{
  int i;
  int max_name_length = strlen("#RULESET_NAME");
  char str_format[STR_MAX];
  //  char str_format2[STR_MAX];
  if ( pFile == NULL ||
       pRuleSetList == NULL ) return 1;
  for (i=0; i<pRuleSetList->num_rulesets;i++)
  {
    ruleset_struct *pRuleSet = (ruleset_struct*) &(pRuleSetList->pRuleSet[i]) ;
    max_name_length = MAX ( max_name_length, strlen(pRuleSet->str_name)); 
  }

//  sprintf(str_format, "#%%-%ds %%12s %%s %%s %%s ... \n", max_name_length - 1);
//  fprintf(pFile, str_format,
//          "RULESET_NAME", "REQ_WEIGHT", 
//          "RULE_NAME1[*RULE_WEIGHT1]",  "[RULE_NAME2[*RULE_WEIGHT2]]",  "[RULE_NAME3[*RULE_WEIGHT3]]" );
//  sprintf(str_format, "%%-%ds %%12g ", max_name_length);
//  sprintf(str_format2, "%%-%ds %%12g \"%%s\"\n", max_name_length);
//  for (i=0; i<pRuleSetList->num_rulesets;i++)
//  {
//    int r;
//    ruleset_struct *pRuleSet = (ruleset_struct*) &(pRuleSetList->pRuleSet[i]) ;
//    fprintf(pFile, str_format,
//            (char*) pRuleSet->str_name,
//            (double) pRuleSet->required_weight);
//    for(r=0;r<pRuleSet->num_rules;r++)
//    {
//      fprintf(pFile,"%12s*%-8g ", pRuleSet->ppRule[r]->str_name, pRuleSet->pweight[r]);   
//    }
//    fprintf(pFile,"\n");  
//    //new version that prints the full expression
//    fprintf(pFile, str_format2,
//            (char*) pRuleSet->str_name,
//            (double) pRuleSet->required_weight,
//            (char*) pRuleSet->str_expression);
//    //    fflush( pFile);
//  }

  sprintf(str_format, "#%%-%ds %%14s %%s\n", max_name_length - 1);
  fprintf(pFile, str_format, "RULESET_NAME", "REQUIRED_VALUE", "EXPRESSION" );

  sprintf(str_format, "%%-%ds %%14g \"%%s\" (%%d known rule%%s)\n", max_name_length);
  for (i=0; i<pRuleSetList->num_rulesets;i++)
  {
    ruleset_struct *pRuleSet = (ruleset_struct*) &(pRuleSetList->pRuleSet[i]) ;
    fprintf(pFile, str_format,
            (char*)  pRuleSet->str_name,
            (double) pRuleSet->required_value,
            (char*)  pRuleSet->str_expression,
            (int)    pRuleSet->num_rules,
            (char*)  N2S(pRuleSet->num_rules));
  }


  return 0;
}

//---------------------------------------------------------------------------------------------------
//expect the following format
// Rulesetlist file format:
//old// // at least one RULENAME1 is required
//old// // RULESET_NAME REQUIRED_WEIGHT RULENAME1[*WEIGHT1] [RULENAME2[*WEIGHT2]] [RULENAME3[*WEIGHT3]] ....
//old// //e.g.
//old// // Type1QSO     2.0             QSOBlueCont         QSOHbeta
// RULESET_NAME REQUIRED_VALUE EXPRESSION
//e.g.
// Type1QSO     2.0            QSOBlueCont+QSOHbeta

int rulesetlist_struct_read_from_file ( rulesetlist_struct *pRuleSetList, rulelist_struct *pRuleList)
{
  FILE *pFile = NULL;
  long i;
  char buffer[LONG_STR_MAX];
  if ( pRuleSetList == NULL ||
       pRuleList    == NULL ) return 1;


  if ( strlen(pRuleSetList->str_filename) == 0 ||
       strcmp(pRuleSetList->str_filename, "NONE") == 0  )
  {
    fprintf(stdout, "%s No rulesetlist specified\n", WARNING_PREFIX); fflush(stdout);
    return 0;
  }
  
  //open the file for reading
  if ((pFile = fopen(pRuleSetList->str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the file: %s\n", ERROR_PREFIX, pRuleSetList->str_filename);
    fflush(stderr);
    return 1;
  }

  fprintf(stdout, "%s %s Reading rulesetlist list from file\n", DEBUG_PREFIX, pRuleSetList->str_filename); fflush(stdout);

    
  //scan through the file to determine the number of lines
  i = 0;
  while (fgets((char*) buffer, (int) LONG_STR_MAX, (FILE*) pFile))
  {
    int j = 0;
    buffer[LONG_STR_MAX - 1] = '\0'; //safety stop
    while( isspace(buffer[j])) j ++; //scan past leading white space
    if ( buffer[j] != '#' &&  buffer[j] != '\0' ) i++; //ignore comment lines
  }
  //go back to the start
  rewind((FILE*)pFile);
  pRuleSetList->num_rulesets = i;
 
  fprintf(stdout, "%s %s contains %d interesting lines\n",
          DEBUG_PREFIX, pRuleSetList->str_filename, pRuleSetList->num_rulesets); fflush(stdout);
  if ( pRuleSetList->num_rulesets <= 0 )
  {
    fprintf(stderr, "%s I found zero valid lines in the ascii file: %s\n", ERROR_PREFIX, pRuleSetList->str_filename);
    fflush(stderr);
    return 1;
  }
  
  //allocate and init space for rulesets
  if ((pRuleSetList->pRuleSet = (ruleset_struct*) malloc(sizeof(ruleset_struct) * pRuleSetList->num_rulesets)) == NULL)
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  for ( i = 0; i< pRuleSetList->num_rulesets; i++)
  {
    if ( ruleset_struct_init ( (ruleset_struct*) &(pRuleSetList->pRuleSet[i])))
    {
      return 1;
    }
  }

  
  //now read in the rulesets
  //expect the following format
  // RULESET_NAME REQUIRED_VALUE EXPRESSION
  {
    int line = 0;
    i = 0;
    while (fgets((char*) buffer, (int) LONG_STR_MAX, (FILE*) pFile))
    {
      int k, rr;
      char *pbuf = NULL;
      char* ptoken = NULL;
      ruleset_struct* pRuleSet = (ruleset_struct*) &(pRuleSetList->pRuleSet[i]);
      line ++;
      buffer[LONG_STR_MAX - 1] = '\0'; //safety stop
      pbuf = buffer;
      //      fprintf(stdout, "DEBUG ---------------------------------------------\n"); fflush(stdout);
      //      fprintf(stdout, "DEBUG LINE %5d pbuf=\"%s\"\n",  __LINE__, pbuf); fflush(stdout);
      while( isspace(*pbuf)) pbuf ++; //scan past leading white space
      //      fprintf(stdout, "DEBUG LINE %5d pbuf=\"%s\"\n",  __LINE__, pbuf); fflush(stdout);

      //trim off any trailing white space and new-line characters
      k = strlen(pbuf) - 1;
      while ( isspace(pbuf[k]) && k > 0 )
      {
        pbuf[k] = '\0';
        k--;
      }
      //      fprintf(stdout, "DEBUG LINE %5d pbuf=\"%s\"\n",  __LINE__, pbuf); fflush(stdout);

      if ( *pbuf == '#' ||
           *pbuf == '\0'||
           isspace(*pbuf) ) continue; //ignore comment and empty lines
      
      //now extract the RULESET_NAME and REQUIRED_VALUE
      //then look for matches of any of the RULENAMEs within the EXPRESSION

      //get the RULESET_NAME
      ptoken = strsep(&pbuf, " \t");
      strncpy((char*) pRuleSet->str_name, ptoken, STR_MAX); 
      //      fprintf(stdout, "DEBUG ptoken=\"%s\" =>\"%s\"\n", ptoken, pRuleSet->str_name); fflush(stdout);
      //      fprintf(stdout, "DEBUG LINE %5d pbuf=\"%s\"\n",  __LINE__, pbuf); fflush(stdout);

      //get the REQUIRED_VALUE
      while(strlen(ptoken = strsep(&pbuf, " \t")) == 0);
      sscanf(ptoken, "%lf", (double*) &(pRuleSet->required_value)); 
      //      fprintf(stdout, "DEBUG ptoken=\"%s\" =>\"%g\"\n", ptoken, pRuleSet->required_value); fflush(stdout);
      //      fprintf(stdout, "DEBUG LINE %5d pbuf=\"%s\"\n",  __LINE__, pbuf); fflush(stdout);
              
      //assume that remainder is the EXPRESSION
      //make some space for the expression string
      //a buffer as long as the trimmed input string is sufficient

      while( isspace(*pbuf)) pbuf ++; //scan past leading white space
      if ((pRuleSet->str_expression = (char*) calloc(strlen(pbuf)+1, sizeof(char))) == NULL )
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      strcpy(pRuleSet->str_expression, pbuf); 


      //allocate and init space for ruleset contents - pessimistically assume that all rules are referenced
      if ((pRuleSet->ppRule  = (rule_struct**) malloc(sizeof(ruleset_struct*) * pRuleList->num_rules)) == NULL )
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }

      pRuleSet->num_rules = 0;
      //now look for matches of any of the RULENAMEs within the EXPRESSION
      for(rr=0;rr<pRuleList->num_rules;rr++)
      {
        rule_struct *pRule = (rule_struct*) &(pRuleList->pRule[rr]);
        //TODO add checks that test if any other rule matches at this position
        //if so, then only count the rule having the longest name
        if ( strstr(pRuleSet->str_expression, pRule->str_name) != NULL )
        {
          pRuleSet->ppRule[pRuleSet->num_rules++] = (rule_struct*) pRule ;
        }
      }
      //tidy up if allocated more space than needed
      if ( pRuleSet->num_rules < pRuleList->num_rules)
      {
        if ( pRuleSet->num_rules > 0 )
        {
          //reallocate space
          if ((pRuleSet->ppRule = (rule_struct**) realloc((rule_struct*) pRuleSet->ppRule,
                                                          sizeof(rule_struct*) * pRuleSet->num_rules)) == NULL )
          {
            fprintf(stderr, "%s Problem re-assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
          }
        }
        else
        {
          FREETONULL(pRuleSet->ppRule);
        }
      }

      
      i++;  //increment the counter (so that we will move on to the next ruleset)
    }
  }    
 
  //close the file
  if ( pFile ) fclose ( pFile) ;
  pFile = NULL;

  
  fprintf(stdout, "%s %s contained %ld (valid) line%s\n", DEBUG_PREFIX, pRuleSetList->str_filename, i, N2S(i));
  fflush(stdout);
  if ( i <= 0 )
  {
    fprintf(stderr, "%s I read zero valid ruleset lines from the file: %s\n", ERROR_PREFIX, pRuleSetList->str_filename);
    fflush(stderr);
    return 1;
  }

 
  //tidy up 
  if ( i < pRuleSetList->num_rulesets )
  {
    pRuleSetList->num_rulesets = i;
    //reallocate space for data
    if ((pRuleSetList->pRuleSet = (ruleset_struct*) realloc((ruleset_struct*) pRuleSetList->pRuleSet,
                                                            sizeof(ruleset_struct) * pRuleSetList->num_rulesets)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }


  if ( rulesetlist_struct_print ((rulesetlist_struct*) pRuleSetList, stdout))
  {
    return 1;
  }
 
  return 0;
}



//set up the machinery to evaluate user expressions with fits_calculate_expression()
//assume that all rules and rulesets have already been read in
int rulesetlist_struct_setup_expressions (rulesetlist_struct *pRuleSetList, rulelist_struct* pRuleList)
{
  int i;
  int status = 0;
  const long nrows = 1;
  char **ttype = NULL;
  char **tform = NULL;

  if ( pRuleSetList == NULL || pRuleList == NULL ) return 1;

  //set up a fits binary table in memory
  //each rule will be a separate column with column name=rule name and there will be
  //one row that will be reused for every evaluation of a ruleset

  if ( pRuleList->num_rules <= 0 )
  {
    return 0;
  }
  
  //allocate space for column descriptions
  //remeber to allow space for two extra columns
  if ((ttype = (char**) calloc(pRuleList->num_rules+2, sizeof(char*))) == NULL ||
      (tform = (char**) calloc(pRuleList->num_rules+2, sizeof(char*))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  ttype[RULESET_COLNUM_RESULT-1] = (char*) RULESET_COLNAME_RESULT; //will receive the result of the evaluation of the ruleset
  tform[RULESET_COLNUM_RESULT-1] = (char*)"1D";

  ttype[RULESET_COLNUM_REDSHIFT-1] = (char*) RULESET_COLNAME_REDSHIFT; //
  tform[RULESET_COLNUM_REDSHIFT-1] = (char*)"1D";

  for(i=0;i<pRuleList->num_rules;i++)
  {
    pRuleList->pRule[i].expr_file_col_num = i+RULESET_COLNUM_FIRST_RULE;
    ttype[i+RULESET_COLNUM_FIRST_RULE-1] = (char*) pRuleList->pRule[i].str_name;
    tform[i+RULESET_COLNUM_FIRST_RULE-1] = (char*) "1D";
  }
  
  //create the file
  if ( fits_create_file ((fitsfile**) &(pRuleSetList->pExprFile),
                         (char*) RULESET_EXPR_FILENAME, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  //create the binary table extension  
  if (  fits_create_tbl ((fitsfile*) pRuleSetList->pExprFile, (int) BINARY_TBL,
                         (LONGLONG) nrows, (int) pRuleList->num_rules+1,
                         (char**) ttype, (char**) tform, (char**) NULL,
                         NULL,
                         (int*) &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  //make a copy of the pExprFile for each Ruleset
  for(i=0;i<pRuleSetList->num_rulesets;i++)
    pRuleSetList->pRuleSet[i].pExprFile = (fitsfile*) pRuleSetList->pExprFile;

  FREETONULL(ttype);
  FREETONULL(tform);
  

  return 0;
}

