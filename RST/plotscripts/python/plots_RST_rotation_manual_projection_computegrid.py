#!/usr/bin/python
# plot the output of the 4FS_RST module 
# the pointing where not enough guiding stars can be found .
# author agueguen 


# Set up matplotlib and use a nicer set of plot parameters
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import astropy
import astropy.coordinates as coord
import astropy.units as u
from astropy.coordinates import  Latitude, Longitude 
from astropy.io import ascii
from astropy.io import fits
#import aplpy
# results/etoiles_filtrees_28Janv_ForRot_16.0_17.0_un.fits results/failingpointing_28Janv_ForRot_16.0_17.0_un.fits
#import pywcsgrid2

#/home/agueguen/data1/bin/runtests/OptTile/per_field_logfile.txt
#import this code from gnuplot :
#This is for a scaled Hammer-Aitoff projection (equal area)
#a(ra,dec) = sqrt(1. + cos(dec)*cos((ra-180.)*0.5))
#x(ra,dec) = 360.0 -  (180.0+ 180.0*cos(dec)*sin((ra-180.)*0.5)/a(ra,dec))
#y(ra,dec) = 90.0*sin(dec)/a(ra,dec)
#uses degrees
def a(ra,dec):
  return np.sqrt(1.+np.cos(dec*np.pi /180.)*np.cos((ra-180.)*np.pi/180.*0.5))

def ix(ra,dec):#degrees
  return 360.0 -  (180.0+ 180.0*np.cos(dec*np.pi /180.)*np.sin((ra-180.)*np.pi /180.*0.5)/a(ra,dec))
def ygrec(ra,dec):
  return  90.0*np.sin(dec*np.pi /180.)/a(ra,dec)
# open 2 fits file containing results
Failure_fitsfilename = '../../results/failingpointing_28Janv_ForRot_16.0_17.0_un.fits'
list_processed       = '../../results/etoiles_filtrees_28Janv_ForRot_16.0_17.0_un.fits'
Failure_fitsfilename = '../../results/failingpointing_27Janv_ForRot_16.0_17.0_firstcat.fits'
list_processed       = '../../results/failingpointing_28Janv_ForRot_16.0_17.0_firstcat.fits'

Failure_fitsfilename = '../../results/failingpointing_28Janv_ForRot_16.0_17.0_firstcat.fits'
list_processed       = '../../results/etoiles_filtrees_28Janv_ForRot_16.0_17.0_firstcat.fits'
# ../../results/failingpointing_28Janv_ForRot_16.0_17.0_un.fits
# ../../results/etoiles_filtrees_28Janv_ForRot_16.0_17.0_un.fits
list_processed       = '../../results/etoiles_filtrees_3Feb_withRot_16.0_17.0_bis.fits'
Failure_fitsfilename = '../../results/failingpointing_3Feb_withRot_16.0_17.0_bis.fits'


#hdufailling = fits.open('../../results/failingpointing_2Feb_withRot_16.0_17.0.fits')
#hdufullist  = fits.open('../../results/etoiles_filtrees_2Feb_withRot_16.0_17.0.fits')
hdufailling = fits.open(Failure_fitsfilename)
hdufullist  = fits.open(list_processed) 
#../../results/failingpointing_27Janv_ForRot_16.0_17.0_firstcat.fits
#../../results/failingpointing_28Janv_ForRot_16.0_17.0_firstcat.fits
#../../results/failingpointing_28Janv_ForRot_16.0_17.0_un.fits
#../../results/failingpointing_29Janv_withRot_16.0_17.0.fits

tbdatafail = hdufailling[1].data
tbdatalistfull = hdufullist[1].data 

matplotlib.interactive(False)

colnames_gal = ('RA', 'DEC', 'LINE', 'NODE',)

galacgrid = ascii.read("../../results/plots/galactic_coord_grid_in_J2000.txt", Reader=ascii.NoHeader, names=colnames_gal, fill_values=("", '-999'), )


#for every 13  then 168  lines we must cut the file in a new array     
#colnames_tilling=('NAME' , 'INDEX'     , 'RAt'        , 'DECt', 'PA'   ,		  'Gal_l(deg)', 'Gal_b(deg)', 'SurveyCode', 'Dth', 'Nei',		  'nNeig','AMmin','NobjLo','NobjHi','TotalPrio',		  'RemainPrio','PrVsBB','PrVsDG','wPrVBB','wPrVDG',		  'Vis_BB','Vis_DG','Req_BB','Req_DG','PlanBB',		  'PlanDG','DoneBB',' DoneDG','   FhrsLo','   FhrsHi',' pVBBa1',' pVDGa1','  VBBa1','  VDGa1',' pVBBa2 ','pVDGa2','  VBBa2 ',' VDGa2',' pVBBa3',' pVDGa3','  VBBa3','  VDGa3',' pVBBa4 ','pVDGa4  ','VBBa4 ',' VDGa4',' pVBBa5 ','pVDGa5 ',' VBBa5 ',' VDGa5 ','pVBBa6',' pVDGa6 ',' VBBa6  ','VDGa6')

tillingpattern = ascii.read("/home/agueguen/data1/bin/runtests/OptTile/per_field_logfile.txt")#,  names=colnames_tilling) #Reader=ascii.NoHeader,
print "@@@@@star conputation and preparation coordinates"

ratil = coord.Angle(((tillingpattern['RA']))*u.degree)
#ratil = ratil.wrap_at(180*u.degree)
dectil = coord.Angle(tillingpattern['Dec']*u.degree)
dectil = dectil.wrap_at(180*u.degree)
 

ragrid = coord.Angle(((galacgrid['RA']+180))*u.degree)
ragrid = ragrid.wrap_at(180*u.degree)
decgrid = coord.Angle(galacgrid['DEC']*u.degree)
decgrid = decgrid.wrap_at(180*u.degree)


cols = hdufailling[1].columns
print cols.names
print len(tbdatafail)
#for i in range(len(tbdatafail)):
#  print tbdatafail['RA_CENTRE'][i],tbdatafail['DEC_CENTRE'][i], tbdatafail['WFS_STATUS'][i] ,  tbdatafail['ANG_STATUS'][i]
  
#(1, 'abc', 3.7000002861022949, 0)
#WFS_STATUS ANG_STATUS

mask0 = tbdatafail['WFS_STATUS'] == False
WFSdata = tbdatafail[mask0]

mask1 = tbdatafail['ANG_STATUS'] == False
ANGdata = tbdatafail[mask1]


mask2 = tbdatalistfull['ROTATED'] == True
#mask = (tbdatalistfull['ROTATED','FOV_INDEX'] == [True,0])
ROTATEDalldata = tbdatalistfull[mask2]
mask3 = ROTATEDalldata['FOV_INDEX'] == 0 #,tbdatalistfull['FOV_INDEX'] == 0 )
ROTATEDdata = ROTATEDalldata[mask3]
print " number rotated data =",len(ROTATEDdata)
print " number ang data     =",len(ANGdata)
print " number wfs data     =",len(WFSdata)

#mask = tbdatafail['ANG_STATUS'] == False
#ANGdata = tbdatafail[mask]
print "FAILURE ON WFS"
for i in range(0,len(WFSdata)):
  print "WFS FAILURE   field name = ", WFSdata['FIELD_NAME'][i], " index=",WFSdata['FIELD_INDEX'][i]
print "FAILURE ON AnG"  
for i in range(0,len(ANGdata)):  
  print "ANG FAILURE   field name = ", ANGdata['FIELD_NAME'][i], " index=",ANGdata['FIELD_INDEX'][i]

for i in range(0,len(ROTATEDdata)):  
  print "FIELD ROTATED field name = ", ROTATEDdata['FIELD_NAME'][i], " index=",ROTATEDdata['FIELD_INDEX'][i]
  
#ra = coord.Angle(tbdatafail['RA_CENTRE']*u.degree)
#ra = ra.wrap_at(180*u.degree)
#dec = coord.Angle(tbdatafail['DEC_CENTRE']*u.degree)
#dec = dec.wrap_at(180*u.degree)

#raWFS = coord.Angle((WFSdata['RA_CENTRE']+180)*u.degree)
raWFS = coord.Angle((WFSdata['RA_CENTRE'])*u.degree)
#raWFS = raWFS.wrap_at(180*u.degree)
decWFS = coord.Angle((WFSdata['DEC_CENTRE'])*u.degree)
#decWFS = decWFS.wrap_at(180*u.degree)


raANG = coord.Angle(ANGdata['RA_CENTRE']*u.degree)
#raANG = raANG.wrap_at(180*u.degree)
decANG = coord.Angle(ANGdata['DEC_CENTRE']*u.degree)
#decANG = decANG.wrap_at(180*u.degree)


rarot = coord.Angle((ROTATEDdata['RA_CENTRE'])*u.degree)
#rarot = rarot.wrap_at(180*u.degree)
decrot = coord.Angle(ROTATEDdata['DEC_CENTRE']*u.degree)
#decrot = decrot.wrap_at(180*u.degree)

rarotdirect = coord.Angle((ROTATEDdata['RA_CENTRE'])*u.degree)
decrotdirect = coord.Angle(ROTATEDdata['DEC_CENTRE']*u.degree)

#rarotdirect = rarotdirect.wrap_at(180*u.degree)
#decrotdirect = decrotdirect.wrap_at(180*u.degree)

#fig = plt.figure(figsize=(10,7))
#fig = plt.figure(figsize=(20,14), dpi=1200)  #, grid_helper = GridHelperCurveLinear(tr, inv_tr))
#axgrid = fig.add_subplot(111, projection="mollweide")

j=0
runningi=0

rameridan=np.zeros(13)
decmeridian=np.zeros(13)
raparallel=np.zeros(168)
decparallel=np.zeros(168)
f=open('../../results/plots/galactic_coord_grid_in_J2000.txt', 'r')
#for line in f:
#        print "|",line,"_" 
#        print "|",line.strip(),"|"
#        print "|",line.strip().strip(),"|"
#        if (line.strip() !=""):
#	  spltstr= line.split()
#	  ragrid=spltstr[0]
"""	  
while (j<len(galacgrid)):
  if (runningi<13):
    rameridan[runningi]=  galacgrid['RA'][j]
    decmeridian[runningi]= galacgrid['DEC'][j]
  if (runningi>=13 and runningi<181):
    raparallel[runningi-13]=  galacgrid['RA'][j]
    decparallel[runningi-13]= galacgrid['DEC'][j]
  if (runningi==180 ):
    runningi=0
    ragrid = coord.Angle(((rameridan+180)*-1)*u.degree)
    ragrid = ragrid.wrap_at(180*u.degree)
    decgrid = coord.Angle(decmeridian*u.degree)
    decgrid = decgrid.wrap_at(180*u.degree)
    #print "#################################################### "
    #print "------ ---- RA=",rameridan,"  DEC ",decmeridian," -------"
    #print "#################################################### "
    axgrid.plot(ragrid.radian,decgrid.radian,linewidth=.2,color='0.5')
    ragrid = coord.Angle(((raparallel+180)*-1)*u.degree)
    ragrid = ragrid.wrap_at(180*u.degree)
    decgrid = coord.Angle(decparallel*u.degree)
    decgrid = decgrid.wrap_at(180*u.degree)
   #axgrid.plot(ragrid.radian,decgrid.radian,linewidth=.2,color='0.5')
  else:
    runningi=runningi+1
  j=j+1  
"""
#axgrid.plot(ragrid.radian,decgrid.radian,linewidth=.2,color='0.25')
#for i in range(0,len(rarot)):
#  print "ra degree ",rarotdirect.degree[i]," dec degree=",decrotdirect.degree[i],"    converted X=",ix(rarotdirect.degree[i],decrotdirect.degree[i]), "  Y=",ygrec(rarotdirect.degree[i],decrotdirect.degree[i])

#axgrid.scatter(ratil.radian, dectil.radian, color='0.8',marker="+",label='tilling pattern')
#axgrid.scatter(rarot.radian, decrot.radian, color='b',marker='p',label='rotated')
#axgrid.scatter(raWFS.radian, decWFS.radian, color='r',marker='8',label='failing on wfs')
#axgrid.scatter(raANG.radian, decANG.radian, color='m',marker='h',label='failing on ang')


#axgrid.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h'])
#inversion of the axis 
#axgrid.set_xticklabels(['22h','20h','18h','16h','14h','12h','10h','8h','6h','4h','2h'])

#axgrid.grid(True)
#plt.legend(loc='upper right');
#plt.draw()
#plt.show()
#fig.savefig("map_rotationNfailures_std_tilling_2feb_newdata.pdf")
#fig.savefig("map_rotationNfailures_std_tilling_2feb_newdata.png")
fig2 = plt.figure(figsize=(18,7)) 
mysubplot = fig2.add_subplot(111)

#equatorial grid 
# 1st axe " every 1 degree 0,360  -90 +90 ev ery 15
i=0
j=-90
RAgrideq=np.arange(361)
DECgrideq=np.arange(361)
while j<90:
  i=0
  while i<361:
    RAgrideq[i]=i
    DECgrideq[i]=j
    i=i+1
  ragrideqangl = coord.Angle((RAgrideq)*u.degree)
  decgrideqangl = coord.Angle((DECgrideq)*u.degree)
  mysubplot.plot(ix(ragrideqangl.degree,decgrideqangl.degree), ygrec(ragrideqangl.degree,decgrideqangl.degree),linewidth=1,color='.5')
  j=j+15
##########################################################
i=0
j=-90
RAgrideq=np.arange(181)
DECgrideq=np.arange(181)
while i<361:
  j=-90
  while j<91:
    RAgrideq[j+90]=i
    DECgrideq[j+90]=j
    j=j+1
  ragrideqangl = coord.Angle((RAgrideq)*u.degree)
  decgrideqangl = coord.Angle((DECgrideq)*u.degree)
  mysubplot.plot(ix(ragrideqangl.degree,decgrideqangl.degree), ygrec(ragrideqangl.degree,decgrideqangl.degree),linewidth=1,color='.5')
  i=i+30
  
#############
runningi=0
j=0
while (j<len(galacgrid)):
  if (runningi<13):
    rameridan[runningi]=  galacgrid['RA'][j]
    decmeridian[runningi]= galacgrid['DEC'][j]
  if (runningi>=13 and runningi<181):
    raparallel[runningi-13]=  galacgrid['RA'][j]
    decparallel[runningi-13]= galacgrid['DEC'][j]
  if (runningi==180 ):
    runningi=0
    ragrid = coord.Angle(((rameridan))*u.degree)
    decgrid = coord.Angle(decmeridian*u.degree)
    mysubplot.plot(ix(ragrid.degree,decgrid.degree), ygrec(ragrid.degree,decgrid.degree),linewidth=1,color='.8')
    ragrid = coord.Angle(((raparallel))*u.degree)
    #ragrid = ragrid.wrap_at(180*u.degree)
    decgrid = coord.Angle(decparallel*u.degree)
    #decgrid = decgrid.wrap_at(180*u.degree)
    mysubplot.plot(ix(ragrid.degree,decgrid.degree), ygrec(ragrid.degree,decgrid.degree),linewidth=1,color='.8')
  else:
    runningi=runningi+1
  j=j+1  

#axgrid.scatter(ratil.radian, dectil.radian, color='0.8',marker="+",label='tilling pattern')
#axgrid.scatter(rarot.radian, decrot.radian, color='b',marker='p',label='rotated')
#axgrid.scatter(raWFS.radian, decWFS.radian, color='r',marker='8',label='failing on wfs')
#axgrid.scatter(raANG.radian, decANG.radian, color='m',marker='h',label='failing on ang')
mysubplot.scatter(ix(ratil.degree,dectil.degree), ygrec(ratil.degree,dectil.degree),linewidth=.5,color='0.8',marker=".",label='tilling pattern')
#mysubplot.scatter(ix(rarot.degree,decrot.degree), ygrec(rarot.degree,decrot.degree),linewidth=.2, color='b',marker='p',label='rotated')
mysubplot.scatter(ix(rarotdirect.degree,decrotdirect.degree), ygrec(rarotdirect.degree,decrotdirect.degree), color='b',marker="p",label='rotated')
mysubplot.scatter(ix(raWFS.degree,decWFS.degree), ygrec(raWFS.degree,decWFS.degree),linewidth=.2, color='r',marker='8',label='failing on wfs')
mysubplot.scatter(ix(raANG.degree,decANG.degree), ygrec(raANG.degree,decANG.degree),linewidth=.2, color='m',marker='h',label='failing on ang')
  

mysubplot.set_title(Failure_fitsfilename)
mysubplot.set_ylim([-85,+40])
#mysubplot.grid(True)
plt.legend(loc='upper right');
plt.draw()
plt.show()
