# plots the pointing failing on the RST test programm
#cd /data27s/4most/agueguen/Dev4most/4most_proto/RST/plotsscripts

reset
set angles degrees
set size 0.98,0.9
set origin 0.025,0.05
set key bottom left Left reverse samplen 2

infile_grid_eq  = "../results/plots/J2000_coord_grid_in_J2000.txt"
infile_grid_gal = "../results/plots/galactic_coord_grid_in_J2000.txt"
infile_grid_gal_l0_180 = "< gawk 'NF == 0 {print $0;print $0} $3 == 0 || $3 == 180 {n++;if(n>360){exit};print $0}' ../results/plots/galactic_coord_grid_in_J2000.txt"
notable_locations = "../results/plots/notable_locations.txt"
notable_locations_gals = "< grep NearbyGalaxy ../results/plots/notable_locations.txt"
notable_locations_pnts = "< grep PointOfInterest ../results/plots/notable_locations.txt"
set angles degrees
dec_min = -75
dec_max = 25

#This is for a scaled Hammer-Aitoff projection (equal area)
a(ra,dec) = sqrt(1. + cos(dec)*cos((ra-180.)*0.5))
x(ra,dec) = 360.0 -  (180.0+ 180.0*cos(dec)*sin((ra-180.)*0.5)/a(ra,dec))
y(ra,dec) = 90.0*sin(dec)/a(ra,dec)
unset xtics
unset ytics

coord_label_font = ",14"
coord_label_colour = "#909090"
unset ytics
unset xtics
set label 10001 "+30^{/Symbol \260}" at x(180,+30),y(180.,+30),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour
set label 10002 " +0^{/Symbol \260}" at x(180, +0),y(180., +0),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour
set label 10003 "-30^{/Symbol \260}" at x(180,-30),y(180.,-30),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour
set label 10004 "-60^{/Symbol \260}" at x(180,-60),y(180.,-60),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour
set label 10005 "-90^{/Symbol \260}" at x(180,-90),y(180.,-90),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour
set label 10006  "0^H" at x( 0.*15.,0.),y( 0.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour
set label 10007  "6^H" at x( 6.*15.,0.),y( 6.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour
set label 10008 "12^H" at x(12.*15.,0.),y(12.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour
set label 10009 "18^H" at x(18.*15.,0.),y(18.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour
set label 10010 "24^H" at x(24.*15.,0.),y(24.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour

#This is the small compass
set arrow 20001 from graph 0.98,0.10 to graph 0.98,0.18 lt 1 lw 3 lc rgb coord_label_colour head
 set arrow 20002 from graph 0.98,0.10 to graph 0.95,0.10 lt 1 lw 3 lc rgb coord_label_colour head
set label 20001 "N" at graph 0.98,0.18 left front offset 0.3,0.0 font coord_label_font tc rgb coord_label_colour
 set label 20002 "E" at graph 0.95,0.10 left front offset 0.0,0.6 font coord_label_font tc rgb coord_label_colour
set label 11 "J2000 Coordinates, Hammer-Aitoff Projection" at graph 0.995,0.985,1.0 font "Times-Roman,10" tc lt -1 right
set yrange [y(0.,dec_min):y(0.,dec_max)]
set xrange [0:360]

infile_boundaries = "< gawk 'BEGIN{for(i=0.0;i<=360.0;i+=5.0){print i}; exit}'"

set label 9999 "4MOST Facility Simulator - RST" at graph  0.98,0.02 font ",14" tc lt 3 right

set terminal pngcairo enhanced colour font "Times,16" size 1200,600
str_title = "4MOST Failing pointing from RST"
set title str_title font ",16" offset 0,-0.5
set cblabel "rien"
set cbrange [0:1]

set cbtics offset -0.5,0
set label 2 "" at graph 0.01,0.05 font ",20"
set label 3 "" at graph 0.01,1.10 font ",16"

tiling_dec_max = 18.5
tiling_dec_min = -68.5
hard_dec_max = 27.3673
hard_dec_min = -76.6179
num_years = 5
set label 3 sprintf("4FS RST only camera , red failing WFS, blue failing AnG")
#str_out = sprintf("fiber_efficiency_maps_res_%s_moon_%s_year_%d.png", res, moon, year)

str_out = sprintf("../results/plots/%s.png",plotname)
thumb_out = sprintf("../results/plots/%s_thumb.png",plotname)
set out str_out

#const char *ttype[] = {"FIELD_INDEX" , "RA_CENTRE", "DEC_CENTRE", "FIBRE_STATUS", "FIBRE_OPPOSITE", "CAMERA_STATUS","WFS_STATUS","ANG_STATUS"};//, "FIELD_STATUS" };
#				1		2	3		4		5		6		7		8		
	
#infile = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,FIBRE_OPPOSITE,CAMERA_STATUS,WFS_STATUS,ANG_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++; if ($4$5$6$7$8~/FFFFF/) codecoul[f]=10; else if ($4$5$6$7$8~/TFFFF/) codecoul[f]=11; else if ($4$5$6$7$8~/FTFFF/) codecoul[f]=12;else if ($4$5$6$7$8~/TTFFF/) codecoul[f]=13; else if ($4$5$6$7$8~/FFTTT/) codecoul[f]=14; else if ($4$5$6$7$8~/TTFTF/) codecoul[f]=15; else if ($4$5$6$7$8~/TTFTF/) codecoul[f]=16;else if ($4$5$6$7$8~/FFFTF/) codecoul[f]=17; else if ($4$5$6$7$8~/FFFTF/) codecoul[f]=18; else codecoul[f]=0; ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)

#infilefib = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,FIBRE_OPPOSITE,CAMERA_STATUS,WFS_STATUS,ANG_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++; if ($4$5~/FF/) codecoul[f]=2; else if ($4$5~/TF/) codecoul[f]=6; else if ($4$5~/FT/) codecoul[f]=2;else if ($4$5~/TT/) codecoul[f]=6; ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)

infilefib = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,FIBRE_OPPOSITE,CAMERA_STATUS,WFS_STATUS,ANG_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++; if ($4~/F/) codecoul[f]=1; else if ($4~/T/) codecoul[f]=6;  ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)

infilecamang = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,FIBRE_OPPOSITE,CAMERA_STATUS,WFS_STATUS,ANG_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++; if ($8~/F/) codecoul[f]=1; else if ($8~/T/) codecoul[f]=6; ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)

infilecamwfs = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,FIBRE_OPPOSITE,CAMERA_STATUS,WFS_STATUS,ANG_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++; if ($7~/F/) codecoul[f]=3; else if ($7~/T/) codecoul[f]=6; ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)

#infile = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,FIBRE_OPPOSITE,CAMERA_STATUS,WFS_STATUS,ANG_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++;if ($4$5~/FT/) codecoul[f]=10; else if ($4$5~/TF/) codecoul[f]=12; else if ($4$5~/FF/) codecoul[f]=13; else codecoul[f]=0; ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)

#infile = sprintf("<ftlist '%s[1][col RA_CENTRE,DEC_CENTRE,FIBRE_STATUS,CAMERA_STATUS]' T  colheader=no |gawk '//{f=$1;ra[f]=$2;dec[f]=$3; n[f]++;if ($4$5~/FT/) codecoul[f]=10; else if ($4$5~/TF/) codecoul[f]=12; else if ($4$5~/FF/) codecoul[f]=13; else codecoul[f]=0; ; } END {for(f in n){print ra[f],dec[f],codecoul[f]}}' " ,fitsname)
 #   infilefib               using (x($1,$2)) : (y($1,$2)):($3) lt 1 lc variable not,\
 #    infilefib               using (x($1,$2)) : (y($1,$2)):($3) lt 1 lc variable not,\

plot [][] \
    infilecamwfs            using (x($1,$2)) : (y($1,$2)):($3) lt 3 lc variable not,\
    infilecamang            using (x($1,$2)) : (y($1,$2)):($3) lt 2 lc variable not,\
    infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lc -1 lw 0.5 not,\
    infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5  not,\
    notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\
    notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font ",9" not,\
    notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\
    notable_locations_gals  using (x($2,$3))    : (y($2,$3))    : 1 with labels font ",9" not

    #          infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.5 dt 2 not,\
#         
#          notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font ",9" not,\
#          
#          infile_boundaries       using (x($1,tiling_dec_max)) : (y($1,tiling_dec_max)) with lines lc 2 lt 3 lw 3 not,\
#          infile_boundaries       using (x($1,tiling_dec_min)) : (y($1,tiling_dec_min)) with lines lc 2 lt 3 lw 3 not,\
#          infile_boundaries       using (x($1,hard_dec_max))   : (y($1,hard_dec_max))   with lines lc 2 lt 1 lw 1 not,\
#          infile_boundaries       using (x($1,hard_dec_min))   : (y($1,hard_dec_min))   with lines lc 2 lt 1 lw 1 not
       

# make thumbnail images
set output
#str_system = sprintf("convert -trim -bordercolor White -border 1x1 -resize x50 %s %s",str_out,thumb_out)
#system(str_system)
#red 10 green  11 blue 12
  #12 camera missing
  #13 both are missing
  #10 fibre missing

#    infilecamwfs                  using (x($1,$2)) : (y($1,$2)):($3) lt 3 lc variable not,\
#    infilecamang            using (x($1,$2)) : (y($1,$2)):($3) lt 2 lc variable not,\
