# This is the makefile for RST_input_params_lib.c
include ../arch.make

COMMON_DIR = ../common

IDIR = -I./ -I$(COMMON_DIR)/ -I$(CFITSIO_PATH)/include

OUTSTEM = RST_input_params_lib

OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))

DEP_LIBS = $(COMMON_DIR)/params_lib 4FS_RST

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) $(COMMON_DIR)/define.h
#$(COMMON_DIR)/geometry_lib

#No main() so only make object library

$(OFILE)	:	$(DEP_HFILES) $(CFILE)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE)

clean   : 
	rm -f $(OFILE)


#end

