
//----------------------------------------------------------------------------------
//--
//--    Filename: RST_input_params_lib.h
//--    Use: This is a header for the RST_input_params_lib.c C code file
//--
//--    Notes:
//--

#ifndef RST_INPUT_PARAMS_H
#define RST_INPUT_PARAMS_H

#define CVS_REVISION_RST_INPUT_PARAMS_LIB_H "$Revision: $"
#define CVS_DATE_RST_INPUT_PARAMS_LIB_H     "$Date: $"

#include <stdio.h>

#include "define.h"
#include "4FS_RST.h"
#include "params_lib.h"


#define STR_MAX DEF_STRLEN

//----------------------------------
//----------------------------------


//the input parameters list handling  has got too long, so separate it to this
//new separate module

typedef struct {
  param_list_struct  *pParamList; //pointer to the params_list struct
  
  ////////////////////////////////////////////////////////////////////////////////////
  ///////////////Start by listing all the possible input parameters //////////////////
  // that are listed in the spec doc ///////////////////
  char PARAM_FILENAME[STR_MAX];  //the name of the parameter file
  //the file listing the object templates
  char FIELDS_FILENAME[STR_MAX];       //

  //the file listing the rules
  char FOCALPLANE_FILENAME[STR_MAX];       //
  char HEALPIX_INDEX[STR_MAX];
  char STARCAT_FILENAME[STR_MAX];
 // result definition
  char   RES_OUTDIR[STR_MAX];
  char   RES_OUTFITSFILENAME[STR_MAX];
  char   RES_FAILUREFITSFILENAME[STR_MAX];
  char   MAGI_COLNAME[STR_MAX];
  float GLOBALMAGCUT;
  
  //float MAGLIMAnG;
  //float MAGLIMFIBRE;
  //float MAGLIMWFS;
  //new section for the range of magnitude
  float MAGLIMMaxAnG;
  float MAGLIMMaxFIBRE;
  float MAGLIMMaxWFS;

  float MAGLIMMinAnG;
  float MAGLIMMinFIBRE;
  float MAGLIMMinWFS;
  
  //float DistMin_Neighbour;		// minimum distance for the nearest neighbour
  //float DeltaMag_Neighbour;		// minimum diff in magnitude for the nearest neighbour
  float DistMin_CamNeighbour;		// minimum distance for the nearest neighbour (for camera )
  float DeltaMag_CamNeighbour;		// minimum diff in magnitude for the nearest neighbour (for camera)
  
  float DistMin_FibNeighbour;		// minimum distance for the nearest neighbour (for fibre)
  float DeltaMag_FibNeighbour;		// minimum diff in magnitude for the nearest neighbour (for fibre)
  float MagMax_FibNeighbour;		// magnitude max (fainter star ) to keep before sky limit for the neighbour stars (for fibre)
  // exclusion zone around very bright stars   
  float Exclusion_Radius;
  float Exclusion_MagMax;
  //the random number generator seed value, as supplied on the command line
  long SIM_RANDOM_SEED;
  BOOL SIM_CLOBBER;

  BOOL debug;
  ///////////////End of the list of all the possible input parameters //////////////
}  RST_input_params_struct;

// structure for storing the input catalague parameters


//----------------------------------------------------------------------------------------------
//-- public function declarations


int    RST_input_params_print_version              (FILE* pFile);
int    RST_input_params_print_usage_information    (FILE *pFile);

int    RST_input_params_struct_init                (RST_input_params_struct **ppIpar);
int    RST_input_params_struct_free                (RST_input_params_struct **ppIpar);
int    RST_input_params_handler                    (RST_input_params_struct **ppIpar, param_list_struct **ppParamList, int argc, char** argv);
int    RST_input_params_setup_ParamList            (RST_input_params_struct *pIpar, param_list_struct* pParamList,  int argc, char** argv);

int    RST_input_params_interpret_everything (RST_input_params_struct *pIpar,
                                             fieldlist_struct    *pTL,
                                             focalplane_struct		*pFocPl,
                                             starcat_struct *   pStarCat,
                                             resultsfiles_struct* PResFiles
                                             );


//int    RST_input_params_interpret_everything (RST_input_params_struct *pIpar,
//                                             fieldlist_struct         *pTL,
//                                             focalplane_struct		  *pFocPl,
//                                             starcat_struct           *pStarCat,
//                                             resultsfiles_struct      *PResFiles
//                                             )

int    RST_input_params_interpret_star_catalog(RST_input_params_struct* pIpar,starcat_struct *    pStarCat);

int    RST_input_params_interpret_fieldlist (RST_input_params_struct *pIpar,
                                            fieldlist_struct    *pTL );
/*int   RST_input_params_interpret_sky          (RST_input_params_struct *pIpar,
                                               sky_struct             *pSky);*/
/*int    RST_input_params_interpret_conditions   (RST_input_params_struct *pIpar,
                                               condlist_struct        *pCondList);*/
/*
int    RST_input_params_interpret_sim          (RST_input_params_struct *pIpar,
                                               sim_struct             *pSim);
int    RST_input_params_interpret_rules        (RST_input_params_struct *pIpar,
                                               rulelist_struct        *pRuleList,
                                               rulesetlist_struct     *pRuleSetList);
*/
/*int    RST_input_params_interpret_spectrograph (RST_input_params_struct *pIpar,
                                               spectrograph_struct    *pSpec);*/
/*int    RST_input_params_interpret_templatelist (RST_input_params_struct *pIpar,
                                               templatelist_struct    *pTL);
*/

#endif
