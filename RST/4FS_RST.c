//standard header files
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <float.h> 
#include <time.h> 
#include <sys/syscall.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <sys/utsname.h> 
#include <unistd.h> 
//needed for fork(), getcwd etc
#include <assert.h> //needed for debugging
#include <time.h> 
#include <fitsio.h> //needed for fits catalogues etc
#include <chealpix.h> //needed for nice map making
#include "hpx_extras_lib.h"
//my header files come next
#include "4FS_RST.h"
#include "RST_input_params_lib.h"

#define CVS_REVISION "$Revision: $"
#define CVS_DATE "$Date:  $"

//apart from a few defintions that we do not want to escape beyond the scope of this file
#define COMMENT_PREFIX "#-4FS_RST.c:Comment: "
#define WARNING_PREFIX "#-4FS_RST.c:Warning: "
#define ERROR_PREFIX "#-4FS_RST.c:Error:   "
#define DEBUG_PREFIX "#-4FS_RST.c:Debug:   "
#define DATA_PREFIX ""
#define NB_WFS 4
#define NB_AnG 2
#define NB_FIBGUIDE 12

//the following controls whether to emit very verbose debug information, use with care
//#define ALLOCATOR_DEBUG_OUTPUT

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////MAIN STARTS HERE////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
// now we start the main program
int main(int argc, char * argv[]) {
        /////////////////////////////////////
        //internal record of start of execution time
        clock_t clocks_main_start = clock();
        //////////////////////////////////

        //this structure deals with the reading in of input parameters
        param_list_struct * pParamList = NULL;

        //this structure holds the input parameters themselves
        RST_input_params_struct * pIpar = NULL;

        //these are the main data structures
        fieldlist_struct * pFldLst = NULL;
        focalplane_struct * pFocPl = NULL;

        starcat_struct * pStarCat = NULL;
        resultsfiles_struct * PResFiles = NULL;

        fitsfile * fptr_index = NULL; // FITS file pointer for the indexfile
        fitsfile * fptr_cat = NULL; // FITS file pointer for the star catalog file ;
        couple_fovID_FovTYpe * cplFibTyp = NULL;

        //  /////////////////////////
        // Prevent zombie child processes by setting a behaviour for signal handling
        //  struct sigaction sa;
        //  sa.sa_handler = SIG_IGN;
        //  sa.sa_flags   = SA_NOCLDWAIT;
        //  if ( sigaction(SIGCHLD, &sa, NULL) == -1 )
        //  {
        //    perror("sigaction");
        //    return 1;
        //  }
        //  /////////////////////////

        /////////////////////////////////////////////////////////
        //emit the command line and program details
        if (print_version_and_inputs(stdout, (int) argc, (char * * ) argv)) return 1;
        //fprintf(stdout, "%s Number of OMP threads= %d\n", DEBUG_PREFIX, omp_get_num_threads());
        /////////////////////////////////////////////////////////

        //make space for and init the data structures
        if (fieldlist_struct_init((fieldlist_struct * * ) & pFldLst)) return 1;
        if (focalplane_struct_init((focalplane_struct * * ) & pFocPl)) return 1;
        if (starcat_struct_init((starcat_struct * * ) & pStarCat)) return 1;
        if (resultsfiles_struct_init((resultsfiles_struct * * ) & PResFiles)) return 1;
        if (cplfovIdType_struct_init((couple_fovID_FovTYpe * * ) & cplFibTyp)) return 1;

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //set up and interpret the input params
        {
            int status;
            if ((status = RST_input_params_handler((RST_input_params_struct * * ) & pIpar,
                    (param_list_struct * * ) & pParamList,
                    (int) argc, (char * * ) argv)) != 0) {
                //return 1; //DEBUG
                if (status < 0) return 1; //this is due to a real error
                else exit(0); //not a real error, but we should exit
            }
        }
        //now interpret all the input parameters
        if (RST_input_params_interpret_everything((RST_input_params_struct * ) pIpar,
                (fieldlist_struct * ) pFldLst,
                (focalplane_struct * ) pFocPl,
                (starcat_struct * ) pStarCat,
                (resultsfiles_struct * ) PResFiles
            )) return 1;
        fprintf(stdout, "%s  Minimum (Brighter) mag limit used WFS %f AnG %f FIBRE %f \n", COMMENT_PREFIX,
            pIpar->MAGLIMMinWFS,
            pIpar->MAGLIMMinAnG,
            pIpar->MAGLIMMinFIBRE);
        fprintf(stdout, "%s  Maximum (Fainter) mag limit used WFS %f AnG %f FIBRE %f \n", COMMENT_PREFIX,
            pIpar->MAGLIMMaxWFS,
            pIpar->MAGLIMMaxAnG,
            pIpar->MAGLIMMaxFIBRE);

        //write some standard output files
        {
            char str_buffer[STR_MAX];
            snprintf(str_buffer, STR_MAX, "%s/%s", pIpar->RES_OUTDIR, INTERPRETED_PARAMETER_FILENAME);
            if (print_full_version_information_to_file((const char * ) str_buffer)) return 1;

            snprintf(str_buffer, STR_MAX, "%s/%s", pFldLst->str_outdir, INTERPRETED_PARAMETER_FILENAME);
            if (ParLib_print_param_list_to_file((const char * ) str_buffer,
                    (param_list_struct * ) pParamList)) return 1;
        }

        //////////////// read the file listing the fields position
        if (fieldlist_struct_read_from_file((fieldlist_struct * ) pFldLst)) {
            fprintf(stderr, "%s I had problems reading field list: %s\n",
                ERROR_PREFIX, pFldLst->str_filename);
            return 1;
        }

        //////////////// read the focal plane description
        if (focalplane_struct_read_from_file((focalplane_struct * ) pFocPl, (couple_fovID_FovTYpe * ) cplFibTyp)) // , (couple_fovID_FovTYpe *) cplFibTyp))
        {
            fprintf(stderr, "%s I had problems reading focal plane file  list: %s\n",
                ERROR_PREFIX, pFocPl->str_filename);
            return 1;
        }

        fprintf(stdout, "%s PROCESSING_TIME End of setup phase. It took %.3f seconds\n", COMMENT_PREFIX,
            (double)(clock() - clocks_main_start) / ((double) CLOCKS_PER_SEC));

        ////////////////////////////////////////////////////
        ///we have read the parameters, we start to play with it

        if (work_on_fieldlist((fieldlist_struct * ) pFldLst,
                (focalplane_struct * ) pFocPl,
                (starcat_struct * ) pStarCat,
                (couple_fovID_FovTYpe * ) cplFibTyp,
                fptr_index,
                fptr_cat,
                (resultsfiles_struct * ) PResFiles
            )) {
            fprintf(stderr, "%s I had problems when working on templatelist\n", ERROR_PREFIX);
            return 1;
        }
        /// end of working section , now free the memory

        // section to free the memory
        if (ParLib_free_param_list((param_list_struct * * ) & pParamList))
            fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        if (RST_input_params_struct_free((RST_input_params_struct * * ) & pIpar)) {
            fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        }
        if (fieldlist_struct_free((fieldlist_struct * * ) & pFldLst)) {
            fprintf(stderr, "%s Error freeing memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        }
        // freeing new structures
        //      starcat_struct		*pStarCat	=NULL;     resultsfiles_struct   *PResFiles=NULL;
        if (focalplane_struct_free((focalplane_struct * * ) & pFocPl)) {
            fprintf(stderr, "%s Error freeing memory focal plane structure (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        }
        if (resultsfiles_struct_free((resultsfiles_struct * * ) & PResFiles)) {
            fprintf(stderr, "%s Error freeing memory fresult file structure  (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        }
        if (starcat_struct_free((starcat_struct * * ) & pStarCat)) {
            fprintf(stderr, "%s Error freeing starcat memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        }

        fprintf(stdout, "%s PROCESSING_TIME End of processing. It took %.3f seconds\n", COMMENT_PREFIX,
            (double)(clock() - clocks_main_start) / ((double) CLOCKS_PER_SEC));
        fflush(stdout);
        return 0;
    } //

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// END OF MAIN /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// FUNCTIONS //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

int work_on_fieldlist(fieldlist_struct * pFldLst,
    focalplane_struct * pFocpl,
    starcat_struct * pStarCat,
    couple_fovID_FovTYpe * cplFibTyp,
    fitsfile * fptr_index,
    fitsfile * fptr_cat,
    resultsfiles_struct * PResFiles) {
    if (pFldLst == NULL || pFocpl == NULL) {
        fprintf(stderr, "%s field list or focal plane were not initialised  \n", ERROR_PREFIX);
        return 1;
    };
    int statusRes = 0;
    /// run thought the field list and from the RA DEC of each center , compute , assuming PA =0 the RA dec of the 1stg guide Spine
    statusRes = readPointingCoordinates((fieldlist_struct * ) pFldLst, (focalplane_struct * ) pFocpl, (starcat_struct * ) pStarCat,
        (couple_fovID_FovTYpe * ) cplFibTyp,
        fptr_index, fptr_cat,
        (resultsfiles_struct * ) PResFiles);
    fprintf(stdout, "%s List of pointing on the sky fully processed with returned code %i  \n", COMMENT_PREFIX, statusRes);
    return 0;
}

//--------   		main work functions   ----------
////function which access all the pointing coordinates and uses healpix on them.
int readPointingCoordinates(fieldlist_struct * pFldLst, focalplane_struct * pFocpl, starcat_struct * pStarCat,
    couple_fovID_FovTYpe * cplFibTyp,
    fitsfile * fptr_index, fitsfile * fptr_starcat,
    resultsfiles_struct * PResFiles) { //starcat_struct
    long nbhealpixindex;
    long * array_healpixnumber = NULL;
    long * array_ofnbrows = NULL;
    int statusindexfile = 0;
    int result;
    /// here open the indexfits file only once
    /// first test it: is it a fits file and if yes a fits table
    result = fhelp_test_if_file_is_fits((const char * ) pStarCat->str_healpix_index_filename);
    if (result == (int) FHELP_FILE_FORMAT_CODE_FITS) {
        pStarCat->format_index_code = CATALOGUE_FORMAT_FITS;
    } else if (result == (int) FHELP_FILE_FORMAT_CODE_NOT_FITS) {
        fprintf(stderr, "%s The healpix index file %s is not a valid fits file, but an ASCII file: (result=%d)\n ASCII is not supported at this moment. check the parameter file \n",
            ERROR_PREFIX, pStarCat->str_healpix_index_filename, result);
        return 1;
    } else {
        fprintf(stderr, "%s There were problems opening the healpix index  file: %s (result=%d)\n",
            ERROR_PREFIX, pStarCat->str_healpix_index_filename, result);
        return 1;
    }
    fprintf(stdout, "%s healpix index file %s is in format: %s \n",
        COMMENT_PREFIX,
        pStarCat->str_healpix_index_filename, (pStarCat->format_index_code == CATALOGUE_FORMAT_FITS ? "FITS" : "ASCII"));

    fprintf(stdout, "%s ://                  OPEN THE HEALPIX INDEX FILE            //\n", COMMENT_PREFIX);
    fprintf(stdout, "%s \n", COMMENT_PREFIX);

    long nrow = 0;
    int status = 0;

    // section to define the size of the arrays and initialise them.
    fitsfile * fptr_tmpindex;
    fits_open_table((fitsfile * * ) & fptr_tmpindex, pStarCat->str_healpix_index_filename, READONLY, & status);
    fits_get_num_rows(fptr_tmpindex, & nrow, & status);
    fits_close_file((fitsfile * ) fptr_tmpindex, & status);

    if (nrow <= 0) return 1;

    array_healpixnumber = (long * ) calloc(nrow, sizeof(long));
    array_ofnbrows = (long * ) calloc(nrow, sizeof(long));

    // read the list of the healpix index pixel to optimize the read of the star catalogue itself.
    if ((statusindexfile = read_healpix_index_fits(pStarCat->str_healpix_index_filename, fptr_index, & nbhealpixindex, (long * * ) array_healpixnumber, (long * * ) array_ofnbrows)) != 0) return 1;

    // main function of the program , the one which read the pointing list and find the references stars.
    if (ReadnCheckSurvey(pFldLst, fptr_starcat, pFocpl, pStarCat, (resultsfiles_struct * ) PResFiles,
            (couple_fovID_FovTYpe * ) cplFibTyp,
            (long * * ) array_healpixnumber, (long * * ) array_ofnbrows) != 0) return 1;

    free(array_ofnbrows);
    free(array_healpixnumber);
    return 0;
}

//////////////fits utility functions /////////////////////
BOOL test_if_file_is_fits(const char * str_filename) {
    fitsfile * fptr; // FITS file pointer
    int status = 0; // CFITSIO status value
    BOOL result;

    if (fits_open_file((fitsfile * * ) & fptr, str_filename, READONLY, & status) == 0)
        result = (BOOL) TRUE;
    else
        result = (BOOL) FALSE;
    fits_close_file(fptr, & status);
    return result;
}

BOOL test_if_file_is_fits_table(const char * str_filename) {
    fitsfile * fptr; // FITS file pointer
    int status = 0; // CFITSIO status value
    BOOL result;

    if (fits_open_table((fitsfile * * ) & fptr, str_filename, READONLY, & status) == 0)
        result = (BOOL) TRUE;
    else
        result = (BOOL) FALSE;
    fits_close_file(fptr, & status);
    return result;
}

////////////////////////init structure functions ///////////////////////////////////
int focalplane_struct_init(focalplane_struct * * pFPl) {
    if (pFPl == NULL) return 1;
    if ( * pFPl == NULL) {
        //make space for the struct
        if (( * pFPl = (focalplane_struct * ) malloc(sizeof(focalplane_struct))) == NULL) {
            fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
        }
    }
    strncpy(( * pFPl)->str_name, "", STR_MAX);
    strncpy(( * pFPl)->str_filename, "", STR_MAX);
    ( * pFPl)->num_tot_Fovs = (int) 0;
    // PDR => new definition of the focal plane 4WFS cameras  (should come from a cinfig file)
    ( * pFPl)->num_cameraWFS = (int) 4;
    ( * pFPl)->num_cameraAnG = (int) 2;
    ( * pFPl)->num_fibreguide = (int) 12;
    ( * pFPl)->pCaM = NULL;
    ( * pFPl)->pFibre = NULL;
    //if ( camera_WfsAg_struct_init ( (camera_struct) (pFPl->pCaM))) return 1;
    //if ( fibre_struct_init ( (fibre_struct*) &(pFPl->pFibre))) return 1;
    return 0;
}

/**
 * initialize the structure containing the result for saving in the fits files
 */
int resultsfiles_struct_init(resultsfiles_struct * * pRfil) {
        if (pRfil == NULL) return 1;
        if ( * pRfil == NULL) {
            //make space for the struct
            if (( * pRfil = (resultsfiles_struct * ) malloc(sizeof(resultsfiles_struct))) == NULL) {
                fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
                return 1;
            }
        }
        strncpy(( * pRfil)->str_fitsfilename, "", STR_MAX);
        strncpy(( * pRfil)->str_failurefitsfilename, "", STR_MAX);
        strncpy(( * pRfil)->str_outdir, "", STR_MAX);
        return 0;
    }
    /* structure to store the star catalogue data  */
int starcat_struct_init(starcat_struct * * pStarCat) {
    if (pStarCat == NULL) return 1;
    if ( * pStarCat == NULL) {
        //make space for the struct
        if (( * pStarCat = (starcat_struct * ) malloc(sizeof(starcat_struct))) == NULL) {
            fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
        }
    }
    strncpy(( * pStarCat)->str_healpix_index_filename, "", STR_MAX);
    strncpy(( * pStarCat)->str_starcat_filename, "", STR_MAX);
    return 0;
}

/////////////////////
int camera_WfsAg_struct_init(camera_struct * pCamera) {
    if (pCamera == NULL) return 1;
    strncpy(pCamera->str_name, "", STR_MAX);
    strncpy(pCamera->str_type, "", STR_MAX);
    pCamera->Xcen = (int) NAN;
    pCamera->Ycen = (int) NAN;
    pCamera->Radius_deg = (int) NAN;
    pCamera->Radius_pix = (int) NAN;
    return 0;
}

int fibre_struct_init(fibre_struct * pFibre) {
    if (pFibre == NULL) return 1;
    strncpy(pFibre->str_name, "", STR_MAX);
    pFibre->index = (int) NAN;
    pFibre->Xcen = (int) NAN;
    pFibre->Ycen = (int) NAN;
    pFibre->Radius_deg = (int) NAN;
    pFibre->Radius_mm = (int) NAN;
    return 0;
}

int fieldlist_struct_init(fieldlist_struct * * ppTL) {
    if (ppTL == NULL) return 1;
    if ( * ppTL == NULL) {
        //make space for the struct
        if (( * ppTL = (fieldlist_struct * ) malloc(sizeof(fieldlist_struct))) == NULL) {
            fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
        }
    }
    strncpy(( * ppTL)->str_name, "", STR_MAX);
    strncpy(( * ppTL)->str_filename, "", STR_MAX);
    strncpy(( * ppTL)->str_outdir, "", STR_MAX);
    ( * ppTL)->num_fields = (int) 0;
    ( * ppTL)->array_length = (int) 0;
    ( * ppTL)->pField = NULL;
    return 0;
}

int cplfovIdType_struct_init(couple_fovID_FovTYpe * * pcplIT) {
    if (pcplIT == NULL) return 1;
    if ( * pcplIT == NULL) {
        //make space for the struct
        if (( * pcplIT = (couple_fovID_FovTYpe * ) malloc(sizeof(couple_fovID_FovTYpe))) == NULL) {
            fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
        }
    }
    int localindex = 0;
    for (localindex = 0; localindex < (NB_WFS + NB_AnG + NB_FIBGUIDE) /*18*/ ; localindex++) {
        //pcplIT->arr_fovtype
        strncpy(( * pcplIT)->arr_fovtype[localindex], "", STR_MAX);
    }
    return 0;
}

int field_struct_init(field_struct * pField) {
    if (pField == NULL) return 1;

    strncpy(pField->str_fieldname, "", STR_MAX);
    strncpy(pField->str_DEC, "", STR_MAX);
    strncpy(pField->str_RA, "", STR_MAX);
    pField->DEC = (double) NAN;
    pField->DEC_tolerance = (double) NAN;
    pField->RA = (double) NAN;
    pField->RA_tolerance = (double) NAN;
    pField->PA = (double) NAN;
    pField->PA_tolerance = (double) NAN;
    pField->index = 0;
    return 0;
}

//////////  freeing memory functions
int starcat_struct_free(starcat_struct * * pStCt) {
    if (pStCt == NULL) return 1;
    if ( * pStCt)
        free( * pStCt);
    return 0;
}

int focalplane_struct_free(focalplane_struct * * pStCt) {
    if (pStCt == NULL) return 1;
    free((camera_struct * )( * pStCt)->pCaM);
    if ( * pStCt)
        free( * pStCt);
    return 0;
}

int fieldlist_struct_free(fieldlist_struct * * ppTL) {
    if (ppTL == NULL) return 1;
    if ( * ppTL) {
        if (( * ppTL)->pField) {
            int i;
            for (i = 0; i < ( * ppTL)->num_fields; i++) {
                if (field_struct_free((field_struct * ) & (( * ppTL)->pField[i]))) return 1;
            }
            free(( * ppTL)->pField);
            ( * ppTL)->pField = NULL;
        }
        free( * ppTL); * ppTL = NULL;
    }
    return 0;
}

int resultsfiles_struct_free(resultsfiles_struct * * presFile) {
    if (presFile == NULL) return 1;
    free( * presFile);
    return 0;
}

/* TODO this function is empty and must be finalized */
int field_struct_free(field_struct * pFld) {
    if (pFld == NULL) return 1;
    //if ( pFld )
    //{
    //free(pFld->str_fieldname);
    //free(pFld->str_DEC);
    //free(pFld->str_RA);
    /* free(*pFld->DEC);
    //free(pField->DEC_tolerance=(double) NAN;
    free(*pFld->RA);
    //free(pField->RA_tolerance=(double) NAN;
    free(*pFld->PA);
    //free(pField->PA_tolerance=(double) NAN;
    free(*pFld->index);
	 */
    /*if ( pResponse->plmid   ) free(pResponse->plmid   ); pResponse->plmid    = NULL;*/
    //free(pFld);
    //*pFld = NULL;
    //}
    return 0;
}

/// method which go through the field list file and store it in a local structure
/// author agueguen agueguen@mpe.mpg.de
/// input parameter pointer on the field list structure
/// Format of input file is:
/// OBJNAME  FILENAME  RA DEC PA * * * * * *
// 5 first fields are required, some extra column can exist but won't be used in the firsts releases
int fieldlist_struct_read_from_file(fieldlist_struct * pFlst) {
    FILE * pFile = NULL;
    long i;
    char buffer[STR_MAX];
    if (pFlst == NULL) return 1;
    //fprintf(stdout, "%s start reading field list  ", DEBUG_PREFIX);
    //open the file for reading
    if ((pFile = fopen((pFlst->str_filename), "r")) == NULL) {
        fprintf(stderr, "%s I had problems opening the field list file : %s\n", ERROR_PREFIX, pFlst->str_filename);
        fflush(stderr);
        return 1;
    }
    //scan through the file to determine the number of lines
    i = 0;
    while (fgets((char * ) buffer, (int) STR_MAX, (FILE * ) pFile)) {
        int j = 0;
        buffer[STR_MAX - 1] = '\0'; //safety stop
        while (buffer[j] == ' ') //scan past leading blanks
        {
            j++;
        }
        if (buffer[j] != '#' && buffer[j] != '\0') //ignore comment lines
        {
            i++;
        }
    }
    //go back to the start of the file
    rewind((FILE * ) pFile);
    pFlst->num_fields = i;
    //  fprintf(stdout, "%s %s contains %d interesting line%s:\n",
    //          DEBUG_PREFIX, pFlst->str_filename, pFlst->num_fields, N2S(pFlst->num_fields)); fflush(stdout);
    if (pFlst->num_fields <= 0) {
        fprintf(stderr, "%s I found zero valid lines in the ascii file: %s\n", ERROR_PREFIX, pFlst->str_filename);
        fflush(stderr);
        return 1;
    }

    //allocate and init space for FieldOfView templates
    if ((pFlst->pField = (field_struct * ) malloc(sizeof(field_struct) * pFlst->num_fields)) == NULL) {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
    }
    for (i = 0; i < pFlst->num_fields; i++) {
        if (field_struct_init((field_struct * ) & (pFlst->pField[i]))) {
            fprintf(stderr, "%s Problem initialising the field Nb %li over %i)\n", ERROR_PREFIX, i, pFlst->num_fields);
            return 1;
        }
    }
    /// end section initialisation
    //now read in the data
    {
        i = 0;
//        fprintf(stdout, "%s #FIELD %-24s %-3s %-5s %5s %5s \n",
//             DEBUG_PREFIX,
//             (char * )
//             "FILENAME", (char * )
//             "INDEX",
//             (char * )
//             "RA", (char * )
//             "DEC", (char * )
//             "PA");
        int line = 0;
        while (fgets((char * ) buffer, (int) STR_MAX, (FILE * ) pFile)) {
            int j = 0;
            field_struct * pField = (field_struct * ) & (pFlst->pField[i]);
            line++;
            buffer[STR_MAX - 1] = '\0'; //safety stop

            while (isspace(buffer[j])) j++; //scan past leading white space
            if (j < STR_MAX) {
                if (buffer[j] != '#') //ignore comment lines
                {
                    const char * str_format = "%s %li %lf %lf %lf %s ";
                    int n_tokens1 = 0;
                    char tmpstring[STR_MAX];
                    n_tokens1 = sscanf(buffer, (const char * ) str_format,
                        (char * ) pField->str_fieldname,
                        (long * ) & (pField->index),
                        (double * ) & (pField->RA),
                        (double * ) & (pField->DEC),
                        (double * ) & (pField->PA),
                        (char * ) tmpstring
                    );

                    //if ( n_tokens1 == 4 || n_tokens1 == 5 )// 5 or six field so probably an interesting line
                    if (n_tokens1 >= 5) // 5 or six field so probably an interesting line
                    {
                        pField->index = i;
                        // if ( n_tokens1 == 5 ) //need to interpret the supplied extra information
                        //	{ /* 6th parameter is basically comment since tolerance are in the main parameter file */ }
                        i++;
                    }
                }
            }
        }
    }
    //close the file
    if (pFile) fclose(pFile);
    pFile = NULL;

    fprintf(stdout,"%s %s contained %ld (valid) lines\n", COMMENT_PREFIX, pFlst->str_filename, i);
    fflush(stdout);
    if (i <= 0) {
        fprintf(stderr, "%s I read zero valid template lines from the field list file: %s\n", ERROR_PREFIX, pFlst->str_filename);
        fflush(stderr);
        return 1;
    }
    return 0;
}

//////////////////////////////////////////////////////////////////////////////
/// function which go through the focal plane description file and store it in a local structure
/// author agueguen agueguen@mpe.mpg.de
/// input parameter pointer on the focalplane structure
/// Format of input file is:
/// OBJNAME INDEX TYPE Xcen Ycen RADIUS X1 Y1 X2 Y2 X3 Y3 X4 Y4
/// for non  applicable column (corner for the guide spine for example ) the value is NaN

int focalplane_struct_read_from_file(focalplane_struct * pFocPl, couple_fovID_FovTYpe * local_cplFibTyp) // , couple_fovID_FovTYpe * local_cplFibTyp) //, char * str_filename )
    //TODO : fill a structure of couple FOVID FOV NAME TYPE .....
    { //str_filename = the filename containing the Focal plane description
        FILE * pFile = NULL;
        char buffer[STR_MAX];
        int num_Camwfs = 0, num_cam_AG = 0, num_fibre = 0;
        char * Cam1 = "CAMERA_WFS";
        char * Cam2 = "CAMERA_AnG";
        char * Fibre = "GUIDE_SPINE";

        if (pFocPl == NULL) return 1;
        //open the file for reading
        if ((pFile = fopen(pFocPl->str_filename, "r")) == NULL) {
            fprintf(stderr, "%s I had problems opening the focal plane description file : %s\n", ERROR_PREFIX, pFocPl->str_filename);
            fflush(stderr);
            return 1;
        }
        //scan through the file to determine the number of lines
        while (fgets((char * ) buffer, (int) STR_MAX, (FILE * ) pFile)) {
            int j = 0;
            buffer[STR_MAX - 1] = '\0'; //safety stop
            while (buffer[j] == ' ') //scan past leading blanks
            {
                j++;
            }
            if (buffer[j] != '#' && buffer[j] != '\0') //ignore comment lines
            {
                // look for a subtstring in the string to determine the type of object then increment the good one
                if (strstr(buffer, Cam1) != NULL)
                    num_Camwfs++;
                if (strstr(buffer, Cam2) != NULL)
                    num_cam_AG++;
                if (strstr(buffer, Fibre) != NULL)
                    num_fibre++;
            }
        }
        //go back to the start
        rewind((FILE * ) pFile);
        // is the sum of the nb of fibrespine, the num of cam AG  and the nb of camera WFS;
        fprintf(stdout, "%s %s contains %i interesting line:\n",
            COMMENT_PREFIX, pFocPl->str_filename, (num_Camwfs + num_cam_AG + num_fibre));
        fflush(stdout);
        if ((num_Camwfs + num_cam_AG + num_fibre) <= 0) {
            fprintf(stderr, "%s I found zero valid lines in the ascii file: %s\n", ERROR_PREFIX, pFocPl->str_filename);
            fflush(stderr);
            return 1;
        } else { // the 3 CST are defined in the c file : should add a section to read it from the property file
            if (num_Camwfs != NB_WFS) {
                fprintf(stderr, "%s the number of WFS camera is wrong in the file : %s, we expect %i\n", ERROR_PREFIX, pFocPl->str_filename, NB_WFS);
                fflush(stderr);
                return 1;
            }
            if (num_cam_AG != NB_AnG) {
                fprintf(stderr, "%s the number of A&Gcamera is wrong in the file : %s, we expect %i\n", ERROR_PREFIX, pFocPl->str_filename, NB_AnG);
                fflush(stderr);
                return 1;
            }
            if (num_fibre != NB_FIBGUIDE) {
                fprintf(stderr, "%s the number of guide spine  is wrong in the file : %s, we expect %i\n", ERROR_PREFIX, pFocPl->str_filename, NB_FIBGUIDE);
                fflush(stderr);
                return 1;
            }
            //we should update the number of camera & fibre
            pFocPl->num_tot_Fovs = num_Camwfs + num_cam_AG + num_fibre;
            pFocPl->num_cameraWFS = num_Camwfs;
            pFocPl->num_cameraAnG = num_cam_AG;
            pFocPl->num_fibreguide = num_fibre;

        }
        //allocate and init space for FieldOfView templates
        if ((pFocPl->pCaM = (camera_struct * ) malloc(sizeof(camera_struct) * (pFocPl->num_cameraAnG + pFocPl->num_cameraWFS))) == NULL) {
            fprintf(stderr, "%s Problem assigning memory for WFS and A&G Camera (in file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
        }
        if ((pFocPl->pFibre = (fibre_struct * ) malloc(sizeof(fibre_struct) * pFocPl->num_fibreguide)) == NULL) {
            fprintf(stderr, "%s Problem assigning memory for Fibre Guide (in file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
        }

        //  init the cameras
        int camind;
        for (camind = 0; camind < (pFocPl->num_cameraAnG + pFocPl->num_cameraWFS); camind++) {
            if (camera_WfsAg_struct_init((camera_struct * ) & (pFocPl->pCaM[camind]))) {
                fprintf(stderr, "%s Problem initialising the field Nb %i over %i)\n", ERROR_PREFIX, camind, (pFocPl->num_cameraAnG + pFocPl->num_cameraWFS));
                return 1;
            }
        }
        // init the fibres
        int fibri;
        for (fibri = 0; fibri < (pFocPl->num_fibreguide); fibri++) {
            if (fibre_struct_init((fibre_struct * ) & (pFocPl->pFibre[fibri]))) {
                fprintf(stderr, "%s Problem initialising the field Nb %i over %i)\n", ERROR_PREFIX, fibri, pFocPl->num_fibreguide);
                return 1;
            }
        }
        /// end section initialisation
        //now read in the data
        long i; { //#NAME	INDEX	TYPE		Xcen	Ycen	RADIUS	X1	Y1	X2	Y2	X3	Y3	X4	Y4
            i = 0;
//             fprintf(stdout, "%s #FIELD %-24s %-3s %-5s %5s %5s %5s %5s %5s %5s %5s %5s %5s %5s %5s\n",
//                 DEBUG_PREFIX,
//                 (char * )
//                 "NAME", (char * )
//                 "INDEX", (char * )
//                 "TYPE",
//                 (char * )
//                 "Xcen", (char * )
//                 "Ycen", (char * )
//                 "RADIUS",
//                 (char * )
//                 "X1", (char * )
//                 "Y1",
//                 (char * )
//                 "X2", (char * )
//                 "Y2",
//                 (char * )
//                 "X3", (char * )
//                 "Y3",
//                 (char * )
//                 "X4", (char * )
//                 "Y4"
//             );
            int line = 0;
            char str_tmpName[STR_MAX], str_tmpType[STR_MAX];
            int fovindex = 0;
            double Xcentmp = 0;
            double Ycentmp = 0;
            double Radiustmp = 0;
            double X1tmp = 0;
            double Y1tmp = 0;
            double X2tmp = 0;
            double Y2tmp = 0;
            double X3tmp = 0;
            double Y3tmp = 0;
            double X4tmp = 0;
            double Y4tmp = 0;
            //3 string to define the type of FoV
            char * typewfs = "CAMERA_WFS";
            char * typeag = "CAMERA_AnG";
            char * typefibre = "GUIDE_SPINE";
            int numfibr = 0;
            int numcam = 0;
            int numline = 0;
            int indexCpl = 0;
            int n_tokens1 = 0;
            while (fgets((char * ) buffer, (int) STR_MAX, (FILE * ) pFile)) //  loop over the file
            {
                int j = 0;
                // Initialisation of a camera OR a guide_spine  => must wait to now the type of object to fill
                line++;
                //buffer[STR_MAX - 1] = '\0'; //safety stop
                buffer[STR_MAX - 1] = 0; //safety stop
                while (isspace(buffer[j])) j++; //scan past leading white space
                if (j < STR_MAX) {
                    if (buffer[j] != '#') //ignore comment lines
                    {
                        //const char* str_format = "%s %i %lf %lf %lf %s ";
                        //(char*)  "NAME", (char*)  "INDEX", (char*)  "TYPE",
                        //(char*)  "Xcen", (char*)  "Ycen", (char*)  "RADIUS", (char*)  "X1", (char*)  "Y1",
                        //(char*)  "X2", (char*)  "Y2",  (char*)  "X3", (char*)  "Y3", (char*)  "X4", (char*)  "Y4"

                        const char * str_format = "%s %i %s %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf ";

                        char tmpstring[STR_MAX];
                        n_tokens1 = sscanf(buffer, (const char * ) str_format,
                            (char * ) str_tmpName,
                            (int * ) & fovindex,
                            (char * ) str_tmpType,
                            (double * ) & Xcentmp, (double * ) & Ycentmp, (double * ) & Radiustmp,
                            (double * ) & X1tmp, (double * ) & Y1tmp,
                            (double * ) & X2tmp, (double * ) & Y2tmp,
                            (double * ) & X3tmp, (double * ) & Y3tmp,
                            (double * ) & X4tmp, (double * ) & Y4tmp
                        );

                        if (n_tokens1 == 14) {

                            if (strstr(str_tmpType, typewfs) != NULL) {
                                // init a wfs camera
                                camera_struct * pcamera = (camera_struct * ) & (pFocPl->pCaM[numcam]);
                                strcpy(pFocPl->str_name, str_tmpName);
                                strcpy(pcamera->str_name, str_tmpName);
                                strcpy(pcamera->str_type, str_tmpType);

                                pcamera->index = fovindex;
                                pcamera->Xcen = Xcentmp;
                                pcamera->Ycen = Ycentmp;
                                pcamera->Radius_pix = Radiustmp;
                                pcamera->Xcorner[0] = X1tmp;
                                pcamera->Ycorner[0] = Y1tmp;
                                pcamera->Xcorner[1] = X2tmp;
                                pcamera->Ycorner[1] = Y2tmp;
                                pcamera->Xcorner[2] = X3tmp;
                                pcamera->Ycorner[2] = Y3tmp;
                                pcamera->Xcorner[3] = X4tmp;
                                pcamera->Ycorner[3] = Y4tmp;
                                numcam++;
                                local_cplFibTyp->arr_fovid[indexCpl] = fovindex;
                                strcpy(local_cplFibTyp->arr_fovtype[indexCpl], str_tmpType);
                                indexCpl++;
                            } else if (strstr(str_tmpType, typeag) != NULL) {
                                // init a AG camera
                                //fprintf(stderr, "			%s A&G init camera \n", DEBUG_PREFIX);
                                camera_struct * pcamera = (camera_struct * ) & (pFocPl->pCaM[numcam]);
                                strcpy(pFocPl->str_name, str_tmpName);
                                strcpy(pcamera->str_name, str_tmpName);
                                strcpy(pcamera->str_type, str_tmpType);

                                pcamera->index = fovindex;
                                pcamera->Xcen = Xcentmp;
                                pcamera->Ycen = Ycentmp;
                                pcamera->Radius_pix = Radiustmp;
                                pcamera->Xcorner[0] = X1tmp;
                                pcamera->Ycorner[0] = Y1tmp;
                                pcamera->Xcorner[1] = X2tmp;
                                pcamera->Ycorner[1] = Y2tmp;
                                pcamera->Xcorner[2] = X3tmp;
                                pcamera->Ycorner[2] = Y3tmp;
                                pcamera->Xcorner[3] = X4tmp;
                                pcamera->Ycorner[3] = Y4tmp;
                                numcam++;
                                local_cplFibTyp->arr_fovid[indexCpl] = fovindex;
                                strcpy(local_cplFibTyp->arr_fovtype[indexCpl], str_tmpType);
                                indexCpl++;
                            } else if (strstr(str_tmpType, typefibre) != NULL) {
                                // init a guide spine (fibre)
                                //fprintf(stderr, "			%s init GUIDE SPINE fibre %i\n", DEBUG_PREFIX,numfibr);
                                fibre_struct * localfibre = (fibre_struct * ) & (pFocPl->pFibre)[numfibr];
                                strcpy(localfibre->str_name, str_tmpName);
                                localfibre->index = fovindex;
                                localfibre->Xcen = Xcentmp;
                                localfibre->Ycen = Ycentmp;
                                //fprintf(stdout, "			%s init GUIDE SPINE fibre %i with the value %f for its field Radius_pix \n", DEBUG_PREFIX,numfibr,Radiustmp);
                                localfibre->Radius_mm = Radiustmp;
                                local_cplFibTyp->arr_fovid[indexCpl] = fovindex;
                                strcpy(local_cplFibTyp->arr_fovtype[indexCpl], str_tmpType);
                                indexCpl++;
                                numfibr++;
                            }
                            numline++;
                        }
                        if (n_tokens1 > 14) //need to interpret the supplied extra information
                        {
                            fprintf(stdout, "%s , we have some extra information in the line %li [%s] can't manage the line\n",
                                WARNING_PREFIX, i, tmpstring);
                            /* 6th parameter is basically comment since tolerance are in the main parameter file */
                        }
                        i++;
                    }
                }
            }
        }

        //close the file
        if (pFile) fclose(pFile);
        pFile = NULL;
        if (i <= 0) {
            fprintf(stderr, "%s I read zero valid template lines from the focal plane description file: %s\n", ERROR_PREFIX, pFocPl->str_filename);
            fflush(stderr);
            return 1;
        }
        return 0;
    }
    /** important function which check for each Fov of every pointing the number of stars in it , and apply some rules to the answer  to validate or not the field */
int ReadnCheckSurvey(fieldlist_struct * pFldLst, fitsfile * fptr_starcat, focalplane_struct * pFocpl,
    starcat_struct * pStarCat, resultsfiles_struct * PResFiles,
    couple_fovID_FovTYpe * cplFibTyp,
    long * * array_firstrowhealpixnumber, long * * array_ofnbrows) {
    int i, status = 0;
    long nlist, hpx_pix_temp;
    long nside = 512;
    long HPX_MEMBERS_PER_CHUNK = 512;
    long plistpix[HPX_MEMBERS_PER_CHUNK + 1];

    double Ra, Dec, RotAngle, radiusDeg;
    double Xfibre = 0, Yfibre = 0, Xcam = 0, Ycam = 0;
    double pntra = 0, pntdec = 0;

    /* platescale is the conversion factor for skycoordinates to mm on the foacal plane
     arcsec= dim in MM /platescale  =>  unit of platescale = [mm / arcsec]
     platescale is in units of mm/arcsec   this is the scale of the sky on the focal plane
     */
    double platescale = 0.0595; //0.0594;
    double fibrespineradiusmm = 0.; //11.5;
    double fibrespineradiusArcsec = 0.; //fibrespineradiusmm / platescale;
    double fibrespineradiusDeg = 0.; //fibrespineradiusArcsec / 3600.;
    double cameraradiusmm = 39.094;
    double cameraradiusArcsec = cameraradiusmm / platescale;
    double cameraradiusDeg = cameraradiusArcsec / 3600.;
    double inv_platescale = 1 / platescale;
    char str_cataloguefitsfile[STR_MAX];
    char str_indexfitsfile[STR_MAX];
    int statusStarCatFile = check_fits_cat(pStarCat);

    BOOL nest_flag = (BOOL) TRUE;
    BOOL inclusive_flag = (BOOL) TRUE;
    lstobjectsInFoV_struct * starsInFov = NULL;
    pointingStarsPerFovs_struct * currentPointing;
    pointingStarsPerFovs_struct * arr_StarsInPointings;

    if (statusStarCatFile != 0)
        return 1;
    if (fits_open_table((fitsfile * * ) & fptr_starcat, pStarCat->str_starcat_filename, READONLY, & status)) {
        fprintf(stderr, "%s could not open the star catalogue fits file %s \n", ERROR_PREFIX, pStarCat->str_starcat_filename);
        return 1;
    }
    strncpy(str_cataloguefitsfile, pStarCat->str_starcat_filename, STR_MAX);
    strncpy(str_indexfitsfile, pStarCat->str_healpix_index_filename, STR_MAX);
    arr_StarsInPointings = (pointingStarsPerFovs_struct * ) malloc(sizeof(pointingStarsPerFovs_struct) * (pFldLst->num_fields));
    int nbfibre;
    for (i = 0; i < pFldLst->num_fields; i++) { // loop fields for catalogue extraction
        currentPointing = & arr_StarsInPointings[i]; // local reference to the external structure
        currentPointing->PointingID = i;
        field_struct * pField = (field_struct * ) & (pFldLst->pField[i]);
        // store the coordinate of all the pointing even if they are not ok () keep trace of tyhe errrors.
        currentPointing->DECPointing = pField->DEC;
        currentPointing->RAPointing = pField->RA;
        currentPointing->PhaseAngle = pField->PA;
        strncpy(currentPointing->pointingname, pField->str_fieldname, STR_MAX);

        if (!isnan(pField->RA) && !isnan(pField->DEC)) { // if data for the field
            Ra = pField->RA;
            Dec = pField->DEC;
            RotAngle = pField->PA;
            //read the first spine guide from the focal guide
            //fprintf(stdout,"%s ////////////////////////////////////////////////////////////////////////////////////////////////////////////// \n",COMMENT_PREFIX);
            //fprintf(stdout,"%s ////                                   start field   %i                                                       // \n",COMMENT_PREFIX,i);
            //fprintf(stdout,"%s ////////////////////////////////////////////////////////////////////////////////////////////////////////////// \n",COMMENT_PREFIX);
            //fprintf(stdout,"%s //      RA=%f DEC=%f rotation angle =%f\n",DEBUG_PREFIX,Ra,Dec,RotAngle);
            int nbfov;
            for (nbfov = 0; nbfov < (NB_WFS + NB_AnG + NB_FIBGUIDE) /*18*/ ; nbfov++) { // loop on the Fovs  -> depend of the structure of the focal plane could need an update?
                starsInFov = & (currentPointing->FOVs[nbfov]);
                starsInFov->fieldID = i;
                nbfibre = nbfov; // this var should be renamed into IDFov since its used for both fibre and camera
                if (nbfov >= 0 && nbfov < 12) {
                    strncpy(starsInFov->str_FovTYPE, pFocpl[nbfov].str_name /*"fibre"*/ , STR_MAX);
                    //nbfibre=nbfov;
                    fibre_struct * pFibreloc = (fibre_struct * ) & (pFocpl->pFibre[nbfibre]);
                    if (pFibreloc == NULL) {
                        fprintf(stderr, "%s problem reading the fibre spine  \n", ERROR_PREFIX);
                        return 1;
                    }
                    //strncpy(starsInFov->str_FovTYPE,&(pFibreloc->str_name)/*"fibre"*/,STR_MAX);
                    strncpy(starsInFov->str_FovTYPE, "GUIDE_SPINE", STR_MAX);
                    Xfibre = pFibreloc->Xcen;
                    Yfibre = pFibreloc->Ycen;
		    fibrespineradiusmm = pFibreloc->Radius_mm;
                    fibrespineradiusArcsec = fibrespineradiusmm / platescale;
                    fibrespineradiusDeg = fibrespineradiusArcsec / 3600.;

                    radiusDeg = fibrespineradiusDeg;
                    // read from the lstobjectsInFoV_struct  the following fields
                    //double XPOScenterFov; YPOScenterFov;Xcorner[4];Ycorner[4];XYradius[4]
                    starsInFov->XPOScenterFov = Xfibre;
                    starsInFov->YPOScenterFov = Yfibre;
		    // store the sky coordinates of the center of the fiber guide 
		    //for (int cornerindex=0;cornerindex<4;cornerindex++){ // only one point to store : the center and one value the radius in arcsec on the sky
		    starsInFov->X_corner[0]=     pFibreloc->Xcen;
		    starsInFov->Y_corner[0]=     pFibreloc->Ycen;
		    // store x y ra dec coordinates of the fibre field of view
		    if (geom_project_inverse_gnomonic_from_mm(Ra, Dec, RotAngle, starsInFov->X_corner[0], starsInFov->Y_corner[0], inv_platescale, (double * ) & (pntra), (double * ) & (pntdec)) != 1){
			 starsInFov->RA_corner[0]=  pntra ;//pCameraloc->
			 starsInFov->DEC_corner[0]= pntdec;//  pCameraloc->
			 //fprintf(stdout, "%s storing FIBRE CENTRE pos FIELD ID=%i NBFOV %i X=%f Y=%f \t  RA=%f DEC =%f\n", DEBUG_PREFIX,i,nbfov, starsInFov->X_corner[0], starsInFov->Y_corner[0],starsInFov->RA_corner[0],starsInFov->DEC_corner[0]);
		    }
		    starsInFov->RA_corner[1]= radiusDeg ;//pCameraloc->
		    starsInFov->DEC_corner[1]= radiusDeg;//  pCameraloc->    
		    
		    for (int cornerindex=2;cornerindex<4;cornerindex++){// last entries are useless for fibre 
		      starsInFov->RA_corner[cornerindex]=   0 ;
		      starsInFov->DEC_corner[cornerindex]= 0;  
		    }
		    
                    //fprintf(stdout,"%s when filling data for pointing %i fibre nb %i, Pos center is X=%f Y=%f\n",ERROR_PREFIX,i , nbfov,starsInFov->XPOScenterFov  , starsInFov->YPOScenterFov  );
                    starsInFov->XYradius = fibrespineradiusmm;
                    // put them in the starsInFov struct to copy them in the result struc at the end
                    if (geom_project_inverse_gnomonic_from_mm(Ra, Dec, RotAngle, Xfibre, Yfibre, inv_platescale, (double * ) & (pntra), (double * ) & (pntdec)) == 1) {
                        fprintf(stderr, "%s problem finding the sky coordinates of the Fov NB %i\n", ERROR_PREFIX, nbfov);
                        return 1;
                    }
                } else { // section camera
                    // Warning : in case of problem correct the -12 or -11 for the indice
                    camera_struct * pCameraloc = (camera_struct * ) & (pFocpl->pCaM[nbfov - 12]);
                    if (pCameraloc == NULL) {
                        fprintf(stderr, "%s problem reading the Camera NB %i \n", ERROR_PREFIX, nbfov);
                        return 1;
                    }
                    strncpy(starsInFov->str_FovTYPE, pCameraloc->str_type, STR_MAX);
                    Xcam = pCameraloc->Xcen;
                    Ycam = pCameraloc->Ycen;
                    // store in the lstobjectsInFoV_struct  the following fields
                    //double XPOScenterFov; YPOScenterFov;Xcorner[4];Ycorner[4];XYradius[4] with the corresponfding values
                    starsInFov->XPOScenterFov = Xcam;
                    starsInFov->YPOScenterFov = Ycam;
		    //store the geometry of the FoV
		    for (int cornerindex=0;cornerindex<4;cornerindex++){
		       starsInFov->X_corner[cornerindex]=     pCameraloc->Xcorner[cornerindex];
		       starsInFov->Y_corner[cornerindex]=     pCameraloc->Ycorner[cornerindex];
		       if (geom_project_inverse_gnomonic_from_mm(Ra, Dec, RotAngle, starsInFov->X_corner[cornerindex], starsInFov->Y_corner[cornerindex], inv_platescale, (double * ) & (pntra), (double * ) & (pntdec)) != 1){                                
			 starsInFov->RA_corner[cornerindex]=  pntra ;//pCameraloc->
			 starsInFov->DEC_corner[cornerindex]= pntdec;//  pCameraloc->
			 //fprintf(stdout, "%s storing coordinates CAMERA pointing ID=%i NBFOV %i cornerID=%i X=%f Y=%f \t  RA=%f DEC =%f\n", DEBUG_PREFIX,i,nbfov, cornerindex, starsInFov->X_corner[cornerindex], starsInFov->Y_corner[cornerindex],starsInFov->RA_corner[cornerindex],starsInFov->DEC_corner[cornerindex]);
		       }
		    }
		    
                    starsInFov->XYradius = -1;
                    radiusDeg = cameraradiusDeg;
                    if (geom_project_inverse_gnomonic_from_mm(Ra, Dec, RotAngle, Xcam, Ycam, inv_platescale, (double * ) & (pntra), (double * ) & (pntdec)) == 1) {
                        fprintf(stderr, "%s problem finding the sky coordinates of the center of the Fov (camera) NB %i\n", ERROR_PREFIX, nbfov);
                        return 1;
                    }
                }
                // Here pntra and pntdec store the ra/dec position of the fibre center
                // put zero in all the array,   optional if we assure to trace the number of hpx pixels in
                for (int init = 0; init < HPX_MEMBERS_PER_CHUNK + 1; init++) {
                    plistpix[init] = 0;
                }
                // code copied from Tom's toolbox
                // TODO check that this is appropriate
                (void) ang2pix_nest(nside, M_PI_2 - pntdec * DEG_TO_RAD, pntra * DEG_TO_RAD, (long * ) & (hpx_pix_temp));
                nlist = 0;
                // find the number of healpix inside the region of interest radius depend of the fov type : fibre or camera (see above)
                hpx_extras_query_disc(nside, (double) pntra, (double) pntdec, (double) radiusDeg, (long) HPX_MEMBERS_PER_CHUNK, (long * ) plistpix, (long * ) & nlist, (BOOL) nest_flag, (BOOL) inclusive_flag);

                //check of the nbr of healpix pixels and their index
                if (nlist < 1) {
                    fprintf(stderr, "		%s  ERROR when reading the Field %6d %10.5f %10.5f found %li hpx pixels\n", WARNING_PREFIX,
                        i, (double) pntra, (double) pntdec, nlist);
                    return 1;
                }
                // reserve space for the return value : a new struct (4/12/15) must store the stars properties to filter them latter.
                // take the 2 arrays and their dimension and read the star catalogue , retrieve the stars one by one and keep trace of it
                //define the fov parameters in the structure
                starsInFov->FovID = nbfov;
                starsInFov->RAcenterFov = pntra;
                starsInFov->DECcenterFov = pntdec;
                starsInFov->ValidityFlag = TRUE; // by default before the retrieve and filtering
                // read a full list of stars present in or around the Fov
                char * str_magIcolumnName = pStarCat->str_magI_colname;
                int retour = access_fits_catalogue(str_cataloguefitsfile, (fitsfile * ) fptr_starcat, (long * * ) plistpix, array_firstrowhealpixnumber,
                    array_ofnbrows, nlist, starsInFov, nbfibre, str_magIcolumnName);
                if (retour != 0) {
                    return 1;
                };
                currentPointing->pointingStatus = starsInFov->ValidityFlag;
            }
            //fprintf(stdout,"%s  //******                     end field %i                     *****************//\n",DEBUG_PREFIX,i);
        } else {
            currentPointing->pointingStatus = FALSE;
        }
    } // end first loop on fields (pointing), for initalisation
    // this fits close was moved at the end of the process to allow the rotation on jan 26
    //fits_close_file((fitsfile*)  fptr_starcat, &status);
    fitsfile * fptr_outputresult = NULL;
    fitsfile * fptr_outputfailure = NULL;
    double modifiedphaseangle = -400;
    int NbRejectedPointing = 0;
    int Nbrotation;
    int Nbrotatedfield = 0;
    char localoutdir[STR_MAX];
    char str_OutputFilename[STR_MAX];
    char str_OutputFailureFilename[STR_MAX];
    char lastslash = '/';
    // last correction on march 10 ( pb  comparison char and const
    if (strcmp( & lastslash, & (PResFiles->str_outdir)[(strlen(PResFiles->str_outdir) - 1)]) != 0) {
        //if (!(PResFiles->str_outdir)[(strlen(PResFiles->str_outdir)-1)]=='/'){// security: we check that the folder in the property file is correctly formated , if not we add the slash
        strncpy(localoutdir, PResFiles->str_outdir, STR_MAX);
        strncat(localoutdir, "/", STR_MAX);
    } else {
        strncpy(localoutdir, PResFiles->str_outdir, STR_MAX);
    }
    if (strstr(PResFiles->str_fitsfilename, ".fits") != NULL ||
        strstr(PResFiles->str_fitsfilename, ".fit") != NULL) {
        strncpy((char * ) str_OutputFilename, localoutdir, STR_MAX);
        strncat((char * ) str_OutputFilename, PResFiles->str_fitsfilename, STR_MAX);
    } else {
        fprintf(stdout, "%s 		 the filename in the property file does not have the fit extension, we add it :%s \n", WARNING_PREFIX, PResFiles->str_fitsfilename);
        strncpy(str_OutputFilename, localoutdir, STR_MAX);
        strncat(str_OutputFilename, PResFiles->str_fitsfilename, STR_MAX);
        strncat(str_OutputFilename, ".fits", STR_MAX);
    }
    if (strstr(PResFiles->str_failurefitsfilename, ".fits") != NULL ||
        strstr(PResFiles->str_failurefitsfilename, ".fit") != NULL) {
        strncpy((char * ) str_OutputFailureFilename, localoutdir, STR_MAX);
        strncat((char * ) str_OutputFailureFilename, PResFiles->str_failurefitsfilename, STR_MAX);
    } else {
        fprintf(stdout, "%s 		 the filename for failing pointings in the property file does not have the fit extension, we add it :%s \n", WARNING_PREFIX, PResFiles->str_fitsfilename);
        strncpy(str_OutputFailureFilename, localoutdir, STR_MAX);
        strncat(str_OutputFailureFilename, PResFiles->str_failurefitsfilename, STR_MAX);
        strncat(str_OutputFailureFilename, ".fits", STR_MAX);
    }

    fprintf(stdout, "%s 	the results of the check of the references stars will be written in the output fits file :  %s \n", COMMENT_PREFIX, str_OutputFilename);

    if (write_detailed_summary_to_file_fits_header((fitsfile * * ) & fptr_outputresult, str_OutputFilename) != 0)
        return 1;
    if (write_summarized_failure_results_to_file_fits_header((fitsfile * * ) & fptr_outputfailure, str_OutputFailureFilename) != 0)
        return 1;

    write_comments_inHeaderFitsFiles(fptr_outputresult, fptr_outputfailure, pFldLst, pStarCat, pFocpl);

    fibre_struct * pFibreloc = (fibre_struct * ) & (pFocpl->pFibre[0]);
    if (fibrespineradiusmm == 0.0) {
        fibrespineradiusmm = pFibreloc->Radius_mm;
        fibrespineradiusArcsec = fibrespineradiusmm / platescale;
        fibrespineradiusDeg = fibrespineradiusArcsec / 3600.;
    }
    /* here we can acces coordinates of the corner */
    for (i = 0; i < pFldLst->num_fields; i++) { // LOOP ON FIELD FOR STAR ANALYSIS
        // should be an option : to rotate or not go into the loop if not just do it one time
        pointingStarsPerFovs_struct tmppointing = arr_StarsInPointings[i];
        double RApointing = ( & tmppointing)->RAPointing;
        double DECpointing = ( & tmppointing)->DECPointing;
        double PhaseAngle = ( & tmppointing)->PhaseAngle;
        double PhaseAngleinit = PhaseAngle;
        guidestars_struct * arr_guidestarsCorrect;
        BOOL statusFov[18]; //(NB_WFS + NB_AnG + NB_FIBGUIDE)];
        BOOL fieldStatus = TRUE;
        BOOL camvalid = FALSE;
        BOOL camwfsvalid = FALSE;
        BOOL camAnGvalid = FALSE;
        BOOL fibrevalid = FALSE;
        BOOL oppositestatus = FALSE;
        int ID_star_stored = 0;
        int nbintswfs = 0;
        int nbValidfibre = 0;
        char namepointing[STR_MAX];
        strncpy(namepointing, ( & tmppointing)->pointingname, STR_MAX);
        Nbrotation = 0;
        int rotatestep;
        char * arrayFovType[NB_WFS + NB_AnG + NB_FIBGUIDE] = {
            '\0'
        };
        //char arr_fovtype[18][STR_MAX] ;
        for (rotatestep = 0; rotatestep < 3; rotatestep++) {
            if (rotatestep > 0) {
                fprintf(stdout, "%s FIELD =%i, NAME  %s, RA=%f - DEC=%f,  ROTATED uses %f \n", COMMENT_PREFIX, i, namepointing, RApointing, DECpointing, PhaseAngle);
            }
            //first step determine the maximal number of stars to store for the whole pointing
            int nstarmax = 0;
            int jfov;
            /*(NB_WFS + NB_AnG + NB_FIBGUIDE)*/
            for (jfov = 0; jfov < (NB_WFS + NB_AnG + NB_FIBGUIDE); jfov++) {
                nstarmax += ( & ( & tmppointing)->FOVs[jfov])->NbtotStore;
            }
            arr_guidestarsCorrect = (guidestars_struct * ) malloc((nstarmax) * sizeof(guidestars_struct));
            ID_star_stored = 0;
            fieldStatus = TRUE;
            double distancedeg;
            int kstar;

            for (jfov = 0; jfov < (NB_WFS + NB_AnG + NB_FIBGUIDE) /*18*/ ; jfov++) {
                lstobjectsInFoV_struct locFOVs = ( & tmppointing)->FOVs[jfov];
                int nstars = ( & locFOVs)->NbtotStore;
                guidestars_struct * arr_guidestarsprop;
                arr_guidestarsprop = (guidestars_struct * ) malloc((nstars) * sizeof(guidestars_struct));
                if (jfov >= 0 && jfov < 12) { // fibre guide
                    int okstar = 0;
                    // TODO :   - fill the structure, order it
                    //  - check for the neighbor of the 5 brightest ->   keep the 5 brightest stars
                    for (kstar = 0; kstar < nstars; kstar++) { //store the good stars
                        if ((( & locFOVs)->MAGIobj[kstar] < pFocpl->MagLimMax_Fibre) && (( & locFOVs)->MAGIobj[kstar] > pFocpl->MagLimMin_Fibre)) { //mag is ok
			  

                            // check that the star is actually in the Fov : distanc too the center less than patrol radius .
                            distancedeg = geom_arc_dist((double)( & locFOVs)->RAcenterFov, (double)( & locFOVs)->DECcenterFov, (double)( & locFOVs)->RAobj[kstar], (double)( & locFOVs)->DECobj[kstar]);
                            if (distancedeg < fibrespineradiusDeg) {
                                //check if we have data
                                //if ((&locFOVs)->RAobj[kstar]!=0. && (&locFOVs)->DECobj[kstar] &&)
                                // keep the star , store it somewhere, check if no other bright star is there,
                                // keep trace to check the opposite,do some stats for the pointings
                                ( & arr_guidestarsprop[okstar])->fovID = jfov;
                                strncpy(( & arr_guidestarsprop[okstar])->fovtype, ( & locFOVs)->str_FovTYPE, STR_MAX);
                                ( & arr_guidestarsprop[okstar])->RAfov = (double)( & locFOVs)->RAcenterFov;
                                ( & arr_guidestarsprop[okstar])->DECfov = (double)( & locFOVs)->DECcenterFov;
                                ( & arr_guidestarsprop[okstar])->RAstar = (double)( & locFOVs)->RAobj[kstar];
                                ( & arr_guidestarsprop[okstar])->DECstar = (double)( & locFOVs)->DECobj[kstar];
                                ( & arr_guidestarsprop[okstar])->MAGBobj = (float)( & locFOVs)->MAGBobj[kstar];
                                ( & arr_guidestarsprop[okstar])->MAGRobj = (float)( & locFOVs)->MAGRobj[kstar];
                                ( & arr_guidestarsprop[okstar])->MAGIobj = (float)( & locFOVs)->MAGIobj[kstar];
                                //Must work on X Y coordinates, not in RA dec-> convert ra dec of stars to x y on focal plane and compare with x y corner
                                ( & arr_guidestarsprop[okstar])->Xcoord_FOV[0] = (double)( & locFOVs)->XPOScenterFov;
                                ( & arr_guidestarsprop[okstar])->Ycoord_FOV[0] = (double)( & locFOVs)->YPOScenterFov;
                                ( & arr_guidestarsprop[okstar])->Xcoord_FOV[1] = fibrespineradiusmm;
                                ( & arr_guidestarsprop[okstar])->Ycoord_FOV[1] = fibrespineradiusmm;
                                okstar++;
                            }
                        }
                    }
                    // sorting section
                    //					qsort((void*)arr_guidestarsprop,okstar,sizeof(guidestars_struct),(__compar_fn_t)compare);
                    qsort((void * ) arr_guidestarsprop, okstar, sizeof(guidestars_struct), compare);

                    // REMARK:  searching for bright neighbour
                    // read the first stars, and for each identify some near and bright stars
                    int identifiedstars = 0;
                    float magRef, deltamag;
                    BOOL flagvoisin = FALSE;
                    //for deltamag
                    // fprintf(stderr ,"%s 		for delta mag we could use: %f\n",DEBUG_PREFIX, pFocpl->DeltaMag_Neighbour);
                    float tmp_distmin = pFocpl->DistMin_FibNeighbour / 3600.; // convert from
                    // fprintf(stdout ,"%s DISTANCE FOR REJECTION : %f (in the param file )  which is considered as  %f in degree\n",DEBUG_PREFIX, pFocpl->DistMin_Neighbour,tmp_distmin);
                    int loci;
                    int nextstar;
                    for (loci = 0; loci < okstar; loci++) {
                        // proceed only if star is ok  ::: at least one or 2 parameters must be != 0
                        magRef = ( & arr_guidestarsprop[loci])->MAGIobj;
                        flagvoisin = FALSE;
 			printf("%s FOR field =%i fov=%i CHECK STAR N %i  -----  RA =%f DEC =%f MAG=%f \n",DEBUG_PREFIX,i,jfov,loci,(&locFOVs)->RAobj[loci],(&locFOVs)->DECobj[loci],(&locFOVs)->MAGIobj[loci]);
			//printf("%s    => guidestarsCorrec for field =%i fov=%i (FIBRE) center of FOV    Xcoord_FOV[0]  Ycoord_FOV[0] are %f %f \n",COMMENT_PREFIX,i,jfov ,(&arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[0],(&arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[0]);
                        for (nextstar = loci + 1; nextstar < okstar - 1; nextstar++) { // read the fainter stars
                            //APRIL 17 must introduce the new criteria : forget stars fainter than 20.5
                            if (( & arr_guidestarsprop[nextstar])->MAGIobj < pFocpl->MagMax_FibNeighbour) {
                                deltamag = ( & arr_guidestarsprop[nextstar])->MAGIobj - magRef;
				printf("%s\t compare to -----  RA =%f ,DEC =%f ,delta MAG=%f, ",DEBUG_PREFIX,( & arr_guidestarsprop[nextstar])->RAstar,( & arr_guidestarsprop[nextstar])->DECstar,( & arr_guidestarsprop[nextstar])->MAGIobj);
 
                                if (fabsf(deltamag) < pFocpl->DeltaMag_FibNeighbour) {
                                    //distance computed from the tom's toolbox, in common/geometry'
                                    distancedeg = geom_arc_dist(( & locFOVs)->RAobj[loci], ( & locFOVs)->DECobj[loci], ( & locFOVs)->RAobj[nextstar], ( & locFOVs)->DECobj[nextstar]);
				    printf("\t distance =%f , compared to =%f", distancedeg,tmp_distmin);
                                    if (distancedeg <= tmp_distmin /*0.00083*/ ) { // 3 arc sec in degree
                                        flagvoisin = TRUE;
                                    }
                                }
                                printf("\n");
                                if (flagvoisin) { // exit the current loop in error
				   printf("\t\t\t jump to star %i \n ", nextstar);
				    loci= nextstar;
                                    nextstar = okstar - 1;
                                }
                            }
                        }
                        if (flagvoisin) { //reject the current star
                             //printf("%s field =%i fov=%i TOO NEAR REJECT STAR  ------ REF:    RA =%f DEC =%f MAG= \n",COMMENT_PREFIX,i,jfov,(&locFOVs)->RAobj[loci],(&locFOVs)->DECobj[loci]);
			  printf("%s -\t\t\t REJECT THE STAR \n",DEBUG_PREFIX);
                        } else { //store the star
			  printf("%s -\t\t\t KEEP THE STAR \n",DEBUG_PREFIX);
                            ( & arr_guidestarsCorrect[ID_star_stored])->fovID = jfov;
                            strncpy(( & arr_guidestarsCorrect[ID_star_stored])->fovtype, ( & locFOVs)->str_FovTYPE, STR_MAX);
                            ( & arr_guidestarsCorrect[ID_star_stored])->RAfov = (double)( & locFOVs)->RAcenterFov;
                            ( & arr_guidestarsCorrect[ID_star_stored])->DECfov = (double)( & locFOVs)->DECcenterFov;
                            ( & arr_guidestarsCorrect[ID_star_stored])->RAstar = (double)( & arr_guidestarsprop[loci])->RAstar;
                            ( & arr_guidestarsCorrect[ID_star_stored])->DECstar = (double)( & arr_guidestarsprop[loci])->DECstar;
                            ( & arr_guidestarsCorrect[ID_star_stored])->MAGBobj = (float)( & arr_guidestarsprop[loci])->MAGBobj;
                            ( & arr_guidestarsCorrect[ID_star_stored])->MAGRobj = (float)( & arr_guidestarsprop[loci])->MAGRobj;
                            ( & arr_guidestarsCorrect[ID_star_stored])->MAGIobj = (float)( & arr_guidestarsprop[loci])->MAGIobj;
                            // store the X Y mm coordinates of the fov and the fibre patrol radius (mm)
                            ( & arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[0] = ( & arr_guidestarsprop[loci])->Xcoord_FOV[0]; //(double) (&locFOVs)->XPOScenterFov;
                            ( & arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[0] = ( & arr_guidestarsprop[loci])->Ycoord_FOV[0]; //(double)  (&locFOVs)->YPOScenterFov;
                            ( & arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[1] = ( & arr_guidestarsprop[loci])->Xcoord_FOV[1]; //(double)  (&locFOVs)->XYradius;
                            ( & arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[1] = ( & arr_guidestarsprop[loci])->Ycoord_FOV[1]; //(double)  (&locFOVs)->XYradius;

                            // put the RA DEC values for center of the spine and try to put the radius in sky dimensions..
                            // 1st convert 
                            geom_project_inverse_gnomonic_from_mm((double)( & locFOVs)->RAcenterFov, (double)( & locFOVs)->DECcenterFov, PhaseAngle, ( & arr_guidestarsprop[loci])->Xcoord_FOV[0], ( & arr_guidestarsprop[loci])->Ycoord_FOV[0], inv_platescale, (double * ) & (pntra), (double * ) & (pntdec));

                            //2 put in the array
                            ( & arr_guidestarsCorrect[ID_star_stored])->RAcoord_FOV[0] = pntra; //(double) (&locFOVs)->XPOScenterFov;
                            ( & arr_guidestarsCorrect[ID_star_stored])->DECcoord_FOV[0] = pntdec; //(double)  (&locFOVs)->YPOScenterFov;
          
                            //printf("%s    => guidestarsCorrec for field =%i fov=%i (FIBRE) center of FOV    Xcoord_FOV[0]  Ycoord_FOV[0] are %f %f \n",COMMENT_PREFIX,i,jfov ,(&arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[0],(&arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[0]);
                            //printf("%s    => uidestarsprop for field =%i fov=%i (FIBRE) center of FOV    Xcoord_FOV[0]  Ycoord_FOV[0] are %f %f \n",COMMENT_PREFIX,i,jfov ,(&arr_guidestarsprop[loci])->Xcoord_FOV[0],(&arr_guidestarsprop[loci])->Ycoord_FOV[0]);
                            //printf(" RADIUS of patrol Xcoord_FOV[1]  Ycoord_FOV[1] are %f %f \n",(&arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[1],(&arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[1]);
                            ID_star_stored++;
                            identifiedstars++;
                            if (identifiedstars >= 5) { // we need to keep only the 5 brightest stars
                                loci = nstars;
                            }
                        }
                    }
                    if (identifiedstars >= 1)
                        statusFov[jfov] = TRUE;
                    else
                        statusFov[jfov] = FALSE;
                    //fprintf(stdout,"%s      	  for this %s A.K.A 'GUIDE_SPINE' we keep %i stars \n",COMMENT_PREFIX,((&locFOVs)->str_FovTYPE), identifiedstars);
                    // end section fibre
                } else { // camera section
                    int nstars = ( & locFOVs)->NbtotStore;
                    int okstar = 0;
                    double pntracorner = 0, pntdeccorner = 0;
                    double pntXstar = 0, pntYstar = 0;
                    double Xcornercamera[4], Ycornercamera[4];
                    int insidecamera, nbcamerastars;
                    // Refresh pointing coordinates , just in case
                    RApointing = ( & tmppointing)->RAPointing;
                    DECpointing = ( & tmppointing)->DECPointing;
                    //PhaseAngle  = (&tmppointing)->PhaseAngle;
                    //section cameras : keep only stars mag <16 inside an area ....
                    // TODO :
                    //  - fill the structure, order it
                    //  - check for the neighbor of the 5 brightest -> keep only the on
                    //  - keep the 5 brightest stars
                    // check if the star is inside a square
                    camera_struct current_camera = pFocpl->pCaM[jfov - 12];
                    //define the corner of the camera
                    int corner;

                    for (corner = 0; corner < 4; corner++) {
                        //must check the Phase angle   double PhaseAngle  = (&tmppointing)->PhaseAngle;
                        if (geom_project_inverse_gnomonic_from_mm(RApointing, DECpointing, PhaseAngle, ( & current_camera)->Xcorner[corner], ( & current_camera)->Ycorner[corner], inv_platescale, (double * ) & (pntracorner), (double * ) & (pntdeccorner)) == 1) {
                            fprintf(stderr, "%s problem finding the sky coordinates of the Fov \n", ERROR_PREFIX);
                            return 1;
                        }
                        Xcornercamera[corner] = ( & current_camera)->Xcorner[corner];
                        Ycornercamera[corner] = ( & current_camera)->Ycorner[corner];
                    }
                    //find  for all the stars if they are inside the corner
                    //  arr_guidestarsCorrect =(guidestars_struct *) malloc((nstars)*sizeof(guidestars_struct));
                    //float MagLimCam=23;
                    float MagLimMinCam = -99;
                    float MagLimMaxCam = 23;

                    // adapt the limit magnitud to the type of camera
                    if (strstr(( & current_camera)->str_type, "WFS") != NULL) {
                        MagLimMinCam = pFocpl->MagLimMin_CamWFS;
                        MagLimMaxCam = pFocpl->MagLimMax_CamWFS;
                    } else if (strstr(( & current_camera)->str_type, "AnG") != NULL) {
                        MagLimMinCam = pFocpl->MagLimMin_CamAnG;
                        MagLimMaxCam = pFocpl->MagLimMax_CamAnG;
                    }
                    if (MagLimMaxCam == 23) {
                        fprintf(stderr, "%s problem finding the limit magnitude max for the current camera %s \n", ERROR_PREFIX, ( & current_camera)->str_type);
                        return 1;
                    }
                    if (MagLimMinCam == -99) {
                        fprintf(stderr, "%s problem finding the limit magnitude min for the current camera %s \n", ERROR_PREFIX, ( & current_camera)->str_type);
                        return 1;
                    }

                    int kstar;
                    //fprintf(stdout,"%s we analyse the camera %s %s and the nb of star to store is %i\n",COMMENT_PREFIX,(&current_camera)->str_type,(&current_camera)->str_name,nstars);
                    for (kstar = 0; kstar < nstars; kstar++) {
                        // check if the star is in the CCD frame:
                        // convert RA/DEC for stars and fov to X Y in focal plane
                        // then use the function geom_test_if_point_inside_polygon ( 4, double *px, double *py, double x, double y );
                        if ((( & locFOVs)->MAGIobj[kstar] < MagLimMaxCam) && (( & locFOVs)->MAGIobj[kstar] > MagLimMinCam)) { //mag is ok
                            // keep the star , store it somewhere, check if no other bright star is there ,
                            // keep trace to check the e,do some stats for the pointings
                            ( & arr_guidestarsprop[okstar])->fovID = jfov;
                            ( & arr_guidestarsprop[okstar])->RAfov = (double)( & locFOVs)->RAcenterFov;
                            ( & arr_guidestarsprop[okstar])->DECfov = (double)( & locFOVs)->DECcenterFov;
                            ( & arr_guidestarsprop[okstar])->RAstar = (double)( & locFOVs)->RAobj[kstar];
                            ( & arr_guidestarsprop[okstar])->DECstar = (double)( & locFOVs)->DECobj[kstar];
                            ( & arr_guidestarsprop[okstar])->MAGBobj = (float)( & locFOVs)->MAGBobj[kstar];
                            ( & arr_guidestarsprop[okstar])->MAGRobj = (float)( & locFOVs)->MAGRobj[kstar];
                            ( & arr_guidestarsprop[okstar])->MAGIobj = (float)( & locFOVs)->MAGIobj[kstar];
                            //Must work on X Y coordinates, not in RA dec-> convert ra dec of stars to x y on focal plane and compare with x y corner
                            ( & arr_guidestarsprop[okstar])->Xcoord_FOV[0] = (double)( & locFOVs)->XPOScenterFov;
                            ( & arr_guidestarsprop[okstar])->Ycoord_FOV[0] = (double)( & locFOVs)->YPOScenterFov;
                            double RAcorner_FOV[4], DECcorner_FOV[4];
                            for (corner = 0; corner < 4; corner++) {
                                ( & arr_guidestarsprop[okstar])->Xcoord_FOV[corner + 1] = Xcornercamera[corner];
                                ( & arr_guidestarsprop[okstar])->Ycoord_FOV[corner + 1] = Ycornercamera[corner];
                                geom_project_inverse_gnomonic_from_mm((double)( & locFOVs)->RAcenterFov, (double)( & locFOVs)->DECcenterFov, PhaseAngle, Xcornercamera[corner], Ycornercamera[corner], inv_platescale, (double * ) & (pntra), (double * ) & (pntdec));
                                RAcorner_FOV[corner] = pntra;
                                DECcorner_FOV[corner] = pntdec;
                                ( & arr_guidestarsprop[okstar])->RAcoord_FOV[corner] = RAcorner_FOV[corner];
                                ( & arr_guidestarsprop[okstar])->DECcoord_FOV[corner] = DECcorner_FOV[corner];
                                //							
                            }
                            okstar++;
                        }
                    }
                    //qsort((void *)arr_guidestarsprop,okstar,sizeof(guidestars_struct),(__compar_fn_t)compare);
                    qsort((void * ) arr_guidestarsprop, okstar, sizeof(guidestars_struct), compare);

                    nbcamerastars = 0;
                    float tmp_distmin = pFocpl->DistMin_CamNeighbour / 3600.;
                    float magRef, deltamag;
                    BOOL flagvoisin = FALSE;
                    double distancedeg;
                    // check how many stars fit the neighbour rule
                    for (kstar = 0; kstar < okstar; kstar++) {
                        //position in mm of the star compare to the center of pointing in mm
                        geom_project_gnomonic_to_mm(RApointing, DECpointing, PhaseAngle,
                            ( & arr_guidestarsprop[kstar])->RAstar, ( & arr_guidestarsprop[kstar])->DECstar,
                            platescale, (double * ) & pntXstar, (double * ) & pntYstar);
                        // check the size of the cameras. and if the star is in it
                        insidecamera = geom_test_if_point_inside_polygon(4, Xcornercamera, Ycornercamera, pntXstar, pntYstar);
                        if (insidecamera == 1) {
                            //look if a neighbour is bright   added on february 18
                            magRef = ( & arr_guidestarsprop[kstar])->MAGIobj;
                            flagvoisin = FALSE;
                            int nextstar;
                            for (nextstar = kstar + 1; nextstar < okstar - 1; nextstar++) { // read the fainter stars
                                deltamag = ( & arr_guidestarsprop[nextstar])->MAGIobj - magRef;
                                if (fabsf(deltamag) < pFocpl->DeltaMag_CamNeighbour) {
                                    //distance computed from the tom's toolbox, in common/geometry'
                                    distancedeg = geom_arc_dist(( & locFOVs)->RAobj[kstar], ( & locFOVs)->DECobj[kstar], ( & locFOVs)->RAobj[nextstar], ( & locFOVs)->DECobj[nextstar]);
                                    if (distancedeg <= tmp_distmin) { // initialy 3 arc sec converted in degree
                                        flagvoisin = TRUE;
                                    }
                                }
                                if (flagvoisin == TRUE) { // exit the current loop in error
                                    nextstar = okstar - 1;
                                }
                            }
                            //  end addition
                            if (flagvoisin == TRUE) {
                                //		 fprintf(stdout,"%s       reject a star for the camera %s \n",ERROR_PREFIX,(&current_camera)->str_type);
                            } else {
                                ( & arr_guidestarsCorrect[ID_star_stored])->fovID = ( & arr_guidestarsprop[kstar])->fovID;
                                strncpy(( & arr_guidestarsCorrect[ID_star_stored])->fovtype, ( & current_camera)->str_type, STR_MAX); /* (&current_camera)->str_type*/

                                ( & arr_guidestarsCorrect[ID_star_stored])->RAfov = (double)( & arr_guidestarsprop[kstar])->RAfov;
                                ( & arr_guidestarsCorrect[ID_star_stored])->DECfov = (double)( & arr_guidestarsprop[kstar])->DECfov;
                                ( & arr_guidestarsCorrect[ID_star_stored])->RAstar = (double)( & arr_guidestarsprop[kstar])->RAstar;
                                ( & arr_guidestarsCorrect[ID_star_stored])->DECstar = (double)( & arr_guidestarsprop[kstar])->DECstar;
                                ( & arr_guidestarsCorrect[ID_star_stored])->MAGBobj = (float)( & arr_guidestarsprop[kstar])->MAGBobj;
                                ( & arr_guidestarsCorrect[ID_star_stored])->MAGRobj = (float)( & arr_guidestarsprop[kstar])->MAGRobj;
                                ( & arr_guidestarsCorrect[ID_star_stored])->MAGIobj = (float)( & arr_guidestarsprop[kstar])->MAGIobj;

                                ( & arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[0] = ( & arr_guidestarsprop[kstar])->Xcoord_FOV[0]; //(double) (&locFOVs)->XPOScenterFov;
                                ( & arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[0] = ( & arr_guidestarsprop[kstar])->Ycoord_FOV[0]; //(double)  (&locFOVs)->YPOScenterFov;
                                for (corner = 0; corner < 4; corner++) {
                                    ( & arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[corner + 1] = ( & arr_guidestarsprop[kstar])->Xcoord_FOV[corner + 1]; //Xcornercamera[corner];
                                    ( & arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[corner + 1] = ( & arr_guidestarsprop[kstar])->Ycoord_FOV[corner + 1]; //ycornercamera[corner];
                                    // skycoordinates for the corner 
                                    ( & arr_guidestarsCorrect[ID_star_stored])->RAcoord_FOV[corner] = ( & arr_guidestarsprop[kstar])->RAcoord_FOV[corner]; //Xcornercamera[corner];
                                    ( & arr_guidestarsCorrect[ID_star_stored])->DECcoord_FOV[corner] = ( & arr_guidestarsprop[kstar])->DECcoord_FOV[corner]; //ycornercamera[corner];
                                }
                                ID_star_stored++;
                                nbcamerastars++;
                            }
                        }
                        // the array on struct arr_guidestarsCorrect is bigger than the number of element to store in it:
                        //the number of stored elements is nbcamerastars
                        if (nbcamerastars >= 5)
                            kstar = okstar;
                    }

                    if (nbcamerastars >= 1) {
                        statusFov[jfov] = TRUE;
                    } else
                        statusFov[jfov] = FALSE;
                }
            } // end loop over fovs

            //check opposites fibre
            fibrevalid = FALSE;
            oppositestatus = FALSE;
            nbValidfibre = 0;
            // we count the total of good fibre guide
            int fovstat;
            //char arr_fovtype[NB_WFS + NB_AnG + NB_FIBGUIDE][STR_MAX];
            //char strs[ ][STRING_LENGTH+1];
            for (fovstat = 0; fovstat < 12; fovstat++) { /* 12 ==== NB_FIBGUIDE*/
                //strcpy(arrayFovType[fovstat], "GUIDE_SPINE_NIOU");
                arrayFovType[fovstat] = "GUIDE_SPINE";
                if (statusFov[fovstat] == TRUE)
                    nbValidfibre++;
            }

            if (nbValidfibre >= 4) {
                fibrevalid = TRUE;
                //WARNING : this is dependent of the structure of the focal plane
                // check that if one couple of fibre as one star the opposite one as one also : we needs at least one pair of couple with one star each
                if ((statusFov[0] == TRUE || statusFov[11] == TRUE) && (statusFov[5] == TRUE || statusFov[6] == TRUE)) {
                    //fprintf(stdout,"%s    opposite found for the first couple of fibre \n",ERROR_PREFIX);
                    oppositestatus = TRUE;
                }
                if ((statusFov[1] == TRUE || statusFov[2] == TRUE) && (statusFov[7] == TRUE || statusFov[8] == TRUE)) {
                    //fprintf(stdout,"%s    opposite found for the second couple of fibre \n",ERROR_PREFIX);
                    oppositestatus = TRUE;
                }
                if ((statusFov[3] == TRUE || statusFov[4] == TRUE) && (statusFov[9] == TRUE || statusFov[10] == TRUE)) {
                    //fprintf(stdout,"%s    opposite found for the third couple of fibre \n",ERROR_PREFIX);
                    oppositestatus = TRUE;
                }

                if (oppositestatus == FALSE)
                    fprintf(stdout, "%s    REJECTION ABOUT OPPOSITE FOR FIBRE\n", ERROR_PREFIX);

            }

            //oppositestatus is the global status of the presence of stars in opposite field
            // check cameras : they must all be true if not the whole field is false
            camvalid = FALSE;
            camwfsvalid = FALSE;
            camAnGvalid = FALSE;
            nbintswfs = 0;
            // 12 shoulb be replaced by the Nbof fibre
            for (fovstat = NB_FIBGUIDE /*12*/ ; fovstat < (NB_WFS + NB_AnG + NB_FIBGUIDE) /*20*/ ; fovstat++) {
                // we check the camera rules :
                //at at least one Ang as a guide star in its field
                // each WFS sees at least one star
                lstobjectsInFoV_struct locFOVs = ( & tmppointing)->FOVs[fovstat];
                if (strstr(( & locFOVs)->str_FovTYPE, "AnG"))
                //strcpy(arrayFovType[fovstat], "CAMERA_AnG_NIOU");//,STR_MAX);
                    arrayFovType[fovstat] = "CAMERA_AnG";
                if (strstr(( & locFOVs)->str_FovTYPE, "WFS"))
                //strcpy(arrayFovType[fovstat], "CAMERA_WFS_NIOU");//,STR_MAX);
                    arrayFovType[fovstat] = "CAMERA_WFS";
                if (strstr(( & locFOVs)->str_FovTYPE, "AnG") && statusFov[fovstat] == TRUE) {
                    camAnGvalid = TRUE;
                }
                // count the number of wfs valid camera: need the 4 to be ok to valid the pointing
                if (strstr(( & locFOVs)->str_FovTYPE, "WFS") && statusFov[fovstat] == TRUE) {
                    nbintswfs++;
                }
            }
            // combine the 4

            if (nbintswfs >= NB_WFS) {
                camwfsvalid = TRUE;
                if (camAnGvalid == TRUE)
                    camvalid = TRUE;
                else
                    camvalid = FALSE;
            } else
                camvalid = FALSE;

            // define the final field status
            if (fibrevalid == FALSE || oppositestatus == FALSE || camvalid == FALSE) {
                fieldStatus = FALSE;
            }

            if (fieldStatus == TRUE) {
                modifiedphaseangle = -400;
                if (rotatestep > 0) {
                    Nbrotation = rotatestep;
                    modifiedphaseangle = PhaseAngle;
                }
                Nbrotation = rotatestep;
                rotatestep = 4;

            } else if (fieldStatus == FALSE && rotatestep < 2) {
                //rerun one time with a new phase angle
                fprintf(stdout, "%s FIELD =%i, NAME  %s, RA=%f - DEC=%f, failed we ROTATE it by 60 degree from Phase angle=%f,", COMMENT_PREFIX, i, namepointing, RApointing, DECpointing, PhaseAngle);
                //PhaseAngle  = (&tmppointing)->PhaseAngle;
                PhaseAngle += 60;
                fprintf(stdout, " to %f and in the structure it is: %f (current rotatestep=%i)\n", PhaseAngle, ( & tmppointing)->PhaseAngle, rotatestep);
                //fprintf(stdout ,"   to %f  (current rotatestep=%i)\n",PhaseAngle, rotatestep);
                read_starcat_for_pointing(fptr_starcat, pStarCat, & tmppointing, (focalplane_struct * ) pFocpl,
                    array_firstrowhealpixnumber, array_ofnbrows,
                    PhaseAngle, i); // i is the current field index
            } else {
                Nbrotation = rotatestep;
                fprintf(stdout, "%s FIELD =%i, NAME  %s, RA=%f - DEC=%f,  still fail after 2 rotations we will store it as failed \n", COMMENT_PREFIX, i, namepointing, RApointing, DECpointing);
            }

        } // end for 3 rotate
        // should trace the input the ouptut for the rotated fields
        char tmpFname[25];
        strncpy(tmpFname, ( & tmppointing)->pointingname, 25);
        BOOL rotsatus = FALSE;
        if (Nbrotation > 0) {
            Nbrotatedfield++;
            rotsatus = TRUE;
        }
	
// 	  pointingStarsPerFovs_struct * currentPointingtest= & arr_StarsInPointings[i];
// 	  for (int ii=0;ii<4;ii++){
// 	    lstobjectsInFoV_struct * starsInFov2 = & (currentPointingtest->FOVs[12]);
// 	    double *toto = (double *)    (& starsInFov2->RA_corner);
// 	    fprintf(stdout, "%s PROUTCH before writing in FITS Ra pointing %i corner %i  =%f    \n", COMMENT_PREFIX,i,ii,  toto[ii]);
// 	  }

	pointingStarsPerFovs_struct * currentPointingForMetadata= & arr_StarsInPointings[i];
        write_detailed_summary_to_file_fits_data((fitsfile * ) fptr_outputresult, (guidestars_struct * ) arr_guidestarsCorrect,
            i, /* pointing number */
	    tmpFname,   /*pointing*/
            RApointing, DECpointing,   
            PhaseAngleinit,
            modifiedphaseangle,
	    (pointingStarsPerFovs_struct *) currentPointingForMetadata,
            (BOOL * ) statusFov, // array of the status of each fov
            (char * * ) arrayFovType, // array of the Fov type 
	    
            fieldStatus,
            rotsatus,
            ID_star_stored /* total number of stored stars */ );

        if (fieldStatus == FALSE) {
            write_failure_summary_to_file_fits_one_data((fitsfile * ) fptr_outputfailure, i, tmpFname, RApointing, DECpointing,
                fibrevalid,
                oppositestatus,
                camvalid,
                camwfsvalid,
                camAnGvalid);
            NbRejectedPointing++;
        }

        // update the lines for the field
        // if all opposite are ok (for camera AND fibre) then field can be ok
        // here we must check for the whole field if we meet the global needs :
        //need to update values of the lines with the field status

    } // end loop over field
    fprintf(stdout, "%s\n", COMMENT_PREFIX);
    fprintf(stdout, "%s\t\t\t-- Summary of the results --\n", COMMENT_PREFIX);
    
    if (NbRejectedPointing == 0)
        fprintf(stdout, "%s %i pointing processed, all the %i succeeded. %i were rotated \n", COMMENT_PREFIX, pFldLst->num_fields, pFldLst->num_fields - NbRejectedPointing, Nbrotatedfield);
    else if (NbRejectedPointing == 1)
        fprintf(stdout, "%s %i pointing processed, %i was rejected , %i succeeded. %i were rotated \n", COMMENT_PREFIX, pFldLst->num_fields, NbRejectedPointing, pFldLst->num_fields - NbRejectedPointing, Nbrotatedfield);
    else
        fprintf(stdout, "%s %i pointing processed, %i were rejected , %i succeeded. %i were rotated \n", COMMENT_PREFIX, pFldLst->num_fields, NbRejectedPointing, pFldLst->num_fields - NbRejectedPointing, Nbrotatedfield);

    fprintf(stdout, "\n%s \t\tId, position and cause of failure for the failing pointing(s) can be found in the file \n \t %s \n\n", DATA_PREFIX, str_OutputFailureFilename);
    fprintf(stdout, "%s \t\tId, full result of the tilling with references stars and corrected position , in case of rotation, can be found in the file \n \t %s \n\n", DATA_PREFIX, str_OutputFilename);

    char buf[80];
    snprintf(buf, sizeof(buf), "Nb of pointing in input %i", pFldLst->num_fields);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "Nb of pointing rejected %i", NbRejectedPointing);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "Nb of good pointing %i", pFldLst->num_fields - NbRejectedPointing);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    geom_write_coordinate_grid_files(PResFiles->str_outdir);

    if (fptr_outputresult != NULL) {
        fits_close_file((fitsfile * ) fptr_outputresult, & status);
    }
    if (fptr_outputfailure != NULL) {
        fits_close_file((fitsfile * ) fptr_outputfailure, & status);
    }
    //final closure of the starcatalogue fits file
    fits_close_file((fitsfile * ) fptr_starcat, & status);
    return 1;
}

/////////////////////////
/// read a fits catalogue and its associated index fits file
///
/// first : read the index file for the healpix ;
int read_healpix_index_fits(char * str_indexfitsfilename, fitsfile * fptr_index, long * pnbrows, long * * array_healpixnumber, long * * array_ofnbrows) {
    long nrows = 0;
    int ncols = 0;
    int status = 0; // CFITSIO status value
    /*long nulllngval= (long)nan;
    int anynul;*/
    long jj; //,returnedjj;
    int numcolfirst = 1000;
    // open the index file
    fits_open_table((fitsfile * * ) & fptr_index, str_indexfitsfilename, READONLY, & status);
    fits_get_colnum(fptr_index, CASEINSEN, "FIRSTROW", & numcolfirst, & status);
    printf("%s num col firstrow=%i \n", COMMENT_PREFIX, numcolfirst);

    // get the number of rows
    fits_get_num_rows(fptr_index, & nrows, & status);
    // get the number of columns
    fits_get_num_cols(fptr_index, & ncols, & status);

    for (jj = 1; jj < (long) nrows; jj++) {
        fits_read_col((fitsfile * ) fptr_index, TLONG, 1, jj, 1, 1, NULL, & array_healpixnumber[jj], NULL, & status);
        fits_read_col((fitsfile * ) fptr_index, TLONG, 2, jj, 1, 1, NULL, & array_ofnbrows[jj], NULL, & status);
    } * pnbrows = nrows;
    fits_close_file((fitsfile * ) fptr_index, & status);
    return 0;
}

// function  check_fits_cat check if the file "str_catalogue_filename" for the starcat is a fits table
// close the fits file at the end
int check_fits_cat(starcat_struct * pStarCat /*,fitsfile * paramfptr_cat*/ ) {
    int result = fhelp_test_if_file_is_fits((const char * ) pStarCat->str_starcat_filename);
    if (result == (int) FHELP_FILE_FORMAT_CODE_FITS) {
        pStarCat->format_StarCat_code = CATALOGUE_FORMAT_FITS;
        //fhelp_test_if_file_is_fits_table  ???
    } else if (result == (int) FHELP_FILE_FORMAT_CODE_NOT_FITS) {
        fprintf(stderr, "%s The catalogue file %s is not a valid fits file, but an ASCII file: (result=%d)\n ASCII is not supported at this moment. check the parameter file \n",
            ERROR_PREFIX, pStarCat->str_starcat_filename, result);
        return 1;
    } else {
        fprintf(stderr, "%s There were problems opening the star catalogue file: %s (result=%d)\n",
            ERROR_PREFIX, pStarCat->str_starcat_filename, result);
        return 1;
    }
    fprintf(stdout, "%s star catalogue file %s is in format: %s \n",
        COMMENT_PREFIX,
        pStarCat->str_starcat_filename, (pStarCat->format_StarCat_code == CATALOGUE_FORMAT_FITS ? "FITS" : "ASCII"));
    return 0;
}

/// //////////////////////////////////////////////////
/// read the fits catalogue on one FoV (fibre OR Camera)

// we use only on column for the    magnitude so we read only on  the I ( which will be passed in parameter )
/////-------------------------------------
int access_fits_catalogue(char * str_catalogue, fitsfile * paramfptrStarCat, long * * listhpxpix, long * * firstrowtoread, long * * nbrowstoread,
    long nbhpxpixels, lstobjectsInFoV_struct * Fovstars, int paramFovID, char * str_colunmname) {
    int * status;
    long i, j;
    int numcolRA, numcolDEC, numcolmagI, numcolpixel;
    //numcolmagB2,numcolmagR2,numcolclassB, numcolclassR,numcolclassI,
    // first line (Nb0) of firstrowtoread and nbrowstoread should not be read : it contain 0 0
    // start reading at the line corresponding to the FIRSTROW colunm  ; need this value as parameter
    long lowerlimit = 0, upperlimit = 0, nbtord = 0;

    double testvalueRA = 0, testvalueDEC = 0;
    long testvaluePIX = 0;
    float testvalueMAGI = 0; //testvalueMAGB =0,testvalueMAGR=0,
    //int testvalueClassI=0;// testvalueClassB =0,testvalueClassR=0,
    int ncols = 0;
    status = (int * ) calloc(1, sizeof(int));

    //////////////// structure of the data in the fits file
    //  ra 		col 1
    //  dec 		col 2
    //  flags		col 3
    //  mag b2	col 4
    //  mag r2	col 5
    //  mag i		col 6
    //  class b2	col 7
    //  class r2	col 8
    //  class i 	col 9
    //  pixel 	col 10

    // controle the number of rows
    fits_get_num_cols(paramfptrStarCat, & ncols, status);
    // find the rows
    fits_get_colnum((fitsfile * ) paramfptrStarCat, CASEINSEN, "ra", (int * ) & numcolRA, status);
    fits_get_colnum((fitsfile * ) paramfptrStarCat, CASEINSEN, "dec", (int * ) & numcolDEC, status);

    //	 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "mag_b2", (int*)&numcolmagB2, status);
    //	 if ( numcolmagB2  <= 0 || numcolmagB2 > ncols ) {
    //		 //fprintf(stderr,"%s Problem reading %s column from file try it an other way \n", ERROR_PREFIX, "MAG_B2"         );
    //		 *status=0;
    //		 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "bmag2", (int*)&numcolmagB2, status);
    //		 if ( numcolmagB2  <= 0 || numcolmagB2 > ncols ){
    //			 fprintf(stderr,"%s   Problem reading %s column , column %s not found neither in file %s\n", ERROR_PREFIX,"MAG_B2"   , "BMAG2"    ,str_catalogue     );
    //			 return 1;
    //		 }
    //	 }

    //	 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "mag_r2",(int*) &numcolmagR2, status);
    //	 if ( numcolmagR2  <= 0 || numcolmagR2 > ncols ) {
    //		 //fprintf(stderr,"%s Problem reading %s column from file try it an other way \n", ERROR_PREFIX, "MAG_R2"    );
    //		 //return 1;
    //		 *status=0;
    //		 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "rmag2", (int*)&numcolmagR2, status);
    //		 if ( numcolmagR2  <= 0 || numcolmagR2 > ncols ){
    //			 fprintf(stderr,"%s    Problem reading %s column ,   column %s not found neither in file %s\n", ERROR_PREFIX, "MAG_R2" ,"RMAG2"    ,str_catalogue     );
    //			 return 1;
    //		 }
    //	 };

    // new version with the column name in parameter

    fits_get_colnum((fitsfile * ) paramfptrStarCat, CASEINSEN, str_colunmname, (int * ) & numcolmagI, status);
    if (numcolmagI <= 0 || numcolmagI > ncols) {
        fprintf(stderr, "%s Problem reading %s column from file \n", ERROR_PREFIX, str_colunmname /*"MAG_I" */ );
        return 1;

        //		 *status=0;
        //		 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "imag", (int*)&numcolmagI, status);
        //		 if ( numcolmagI  <= 0 || numcolmagI > ncols ){
        //			 fprintf(stderr,"%s     Problem reading %s column ,  column %s not found neither in file %s\n", ERROR_PREFIX,"MAG_I" , "IMAG"    ,str_catalogue     );
        //			 return 1;
        //		 }

    };

    //	 *status=0;
    //	 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "class_b2", (int*)&numcolclassB, status);
    //	 *status=0;
    //	 fits_get_colnum ( (fitsfile*)  paramfptrStarCat, CASEINSEN,  "class_r2", (int*)&numcolclassR, status);
    //	 *status=0;
    //	 fits_get_colnum ((fitsfile*)   paramfptrStarCat, CASEINSEN,  "class_i",(int*) &numcolclassI, status);
    * status = 0;

    fits_get_colnum((fitsfile * ) paramfptrStarCat, CASEINSEN, "PIXEL", (int * ) & numcolpixel, status);
    if (numcolRA <= 0 || numcolRA > ncols) {
        fprintf(stderr, "%s Problem reading %s column from file\n", ERROR_PREFIX, "RA");
        return 1;
    };
    if (numcolDEC <= 0 || numcolDEC > ncols) {
        fprintf(stderr, "%s Problem reading %s column from file\n", ERROR_PREFIX, "DEC");
        return 1;
    };

    //class is not use in the first version
    //	 if ( numcolclassB <= 0 || numcolclassB> ncols ) {
    //		 //fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "CLASS_B"      );
    //		 //return 1;
    //	 };
    //	 if ( numcolclassR <= 0 || numcolclassR> ncols ) {
    //		 //fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "CLASS_R"      );
    //		 //return 1;
    //		 };
    //	 if ( numcolclassI <= 0 || numcolclassI> ncols ) {
    //		 //fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "CLASS_I"  );
    //		 //return 1;
    //	 };

    if (numcolpixel <= 0 || numcolpixel > ncols) {
        fprintf(stderr, "%s Problem reading %s column from file\n", ERROR_PREFIX, "PIXEL");
        return 1;
    };

    //if one of the fits function failed then the status will show it
    if ( * status) {
        fits_report_error(stderr, (int) * status); // print any error messages
        return 1;
    }
    //printf("%s num col ra=%i dec=%i magb=%i magr=%i magi=%i classb=%i classr=%i classi=%i pixel=%i \n",COMMENT_PREFIX, numcolRA,numcolDEC, numcolmagB2,
    //			 numcolmagR2,numcolmagI,numcolclassB, numcolclassR,numcolclassI,numcolpixel);
    //printf("%s nbhpxpixels to read = %li \n",COMMENT_PREFIX, nbhpxpixels);
    //define the theorical max total number of stars to store for the current FoV
    long sizeofreturnarrays = 0;
    for (i = 1; i <= nbhpxpixels; i++) {
        sizeofreturnarrays += (long) nbrowstoread[(long) listhpxpix[i] + 1];
    }
    // reserve space for the result in the structure
    Fovstars->RAobj = (double * ) malloc(sizeof(double) * sizeofreturnarrays);
    Fovstars->DECobj = (double * ) malloc(sizeof(double) * sizeofreturnarrays);
    Fovstars->MAGBobj = (float * ) malloc(sizeof(float) * sizeofreturnarrays);
    Fovstars->MAGRobj = (float * ) malloc(sizeof(float) * sizeofreturnarrays);
    Fovstars->MAGIobj = (float * ) malloc(sizeof(float) * sizeofreturnarrays);
    Fovstars->hpxPIXobj = (long * ) malloc(sizeof(long) * sizeofreturnarrays);
    Fovstars->FovID = 0;
    Fovstars->NbtotStore = 0;

    // NOTE: There is a shift in the arrays of first line to read and nb line to read: this shift must be corrected here.
    // TODO see with Tom if this is an error or not
    int nbGoodstar = 0;
    int nbTooFaintStar = 0;
    int nbStarNoMag = 0;

    for (i = 1; i <= nbhpxpixels; i++) {
        lowerlimit = (long) firstrowtoread[(long) listhpxpix[i] + 1];
        nbtord = (long) nbrowstoread[(long) listhpxpix[i] + 1];
        upperlimit = lowerlimit + nbtord;

        for (j = lowerlimit; j < upperlimit; j++) { //&& !status
            * status = 0;
            fits_read_col((fitsfile * ) paramfptrStarCat, TDOUBLE, numcolRA, j, 1, 1, NULL, & testvalueRA, NULL, status); * status = 0;
            fits_read_col((fitsfile * ) paramfptrStarCat, TDOUBLE, numcolDEC, j, 1, 1, NULL, & testvalueDEC, NULL, status); * status = 0;
            // fits_read_col((fitsfile*)paramfptrStarCat, TFLOAT, numcolmagB2, j, 1, 1, NULL,&testvalueMAGB, NULL, status);
            //	*status=0;
            //	fits_read_col((fitsfile*)paramfptrStarCat, TFLOAT, numcolmagR2, j, 1, 1, NULL,&testvalueMAGR, NULL, status);
            //	*status=0;
            fits_read_col((fitsfile * ) paramfptrStarCat, TFLOAT, numcolmagI, j, 1, 1, NULL, & testvalueMAGI, NULL, status); * status = 0;
            //	fits_read_col((fitsfile*)paramfptrStarCat, TINT, numcolclassB, j, 1, 1, NULL,&testvalueClassB, NULL, status);
            //	*status=0;
            //	fits_read_col((fitsfile*)paramfptrStarCat, TINT, numcolclassR, j, 1, 1, NULL,&testvalueClassR, NULL, status);
            //	*status=0;
            //	fits_read_col((fitsfile*)paramfptrStarCat, TINT, numcolclassI, j, 1, 1, NULL,&testvalueClassI, NULL, status);
            //	*status=0;
            fits_read_col((fitsfile * ) paramfptrStarCat, TLONG, numcolpixel, j, 1, 1, NULL, & testvaluePIX, NULL, status);

            // basic FILTER SECTION BEFORE TO STORE THE RESULTS
            // we filter only on mag i , the on used for the test ,
            // we should us a parameter
            // TODO :should create a limitcatmag here for the pre filtering
            //	if(!( testvalueMAGB>19 || isnan(testvalueMAGB) ||
            //		testvalueMAGR >19 || isnan(testvalueMAGR) ||
            //		testvalueMAGI >19 || isnan(testvalueMAGI))){
            if (!(testvalueMAGI > 19 || isnan(testvalueMAGI))) {
                Fovstars->RAobj[nbGoodstar] = testvalueRA;
                Fovstars->DECobj[nbGoodstar] = testvalueDEC;
                //	Fovstars->MAGBobj[nbGoodstar]=testvalueMAGB;
                //	Fovstars->MAGRobj[nbGoodstar]=testvalueMAGR;
                Fovstars->MAGIobj[nbGoodstar] = testvalueMAGI;
                Fovstars->hpxPIXobj[nbGoodstar] = testvaluePIX;
                Fovstars->FovID = paramFovID;
                //////////////////////////
                nbGoodstar++;
            } else if ( /*testvalueMAGB>19 || testvalueMAGR >19 ||*/ testvalueMAGI > 19) {
                nbTooFaintStar++;
            } else if ( /*isnan(testvalueMAGB) || isnan(testvalueMAGR) || */ isnan(testvalueMAGI)) {
                nbStarNoMag++;
            }
            //return the results in a list of stars (structure ) -> filter and store from the outside of this function
        }
    }

    // first update of the validity flag
    Fovstars->NbtotStore += nbGoodstar;
    if (Fovstars->NbtotStore >= 1) {
        Fovstars->ValidityFlag = TRUE;
    } else {
        Fovstars->ValidityFlag = FALSE;
    }
    free(status);
    return 0;
}

///---------------------------------------
int getStrFovType(int fovId, char * result) {
    if (fovId >= 0 && fovId < 12) {
        strncpy(result, "FIB", 3);
        return 0;
    } else if (fovId >= 12 && fovId < 14) {
        strncpy(result, "WFS", 3);
        return 0;
    } else if (fovId >= 14 && fovId < 16) {
        strncpy(result, "AnG", 3);
        return 0;
    } else {
        strncpy(result, "ERR", 3);
        return 1;
    }
    return 1;
}

//---------------------------------------------------------------------------------------------------
int write_summary_to_file_ascii(FILE * pFile, fieldlist_struct * pfieldLst /*, condlist_struct* pCondList, simspec_struct* pSimSpec*/ )
    //int write_summary_to_file_ascii (FILE* pFile, template_struct* pTemplate, condlist_struct* pCondList, simspec_struct* pSimSpec)
    {
        // int r;//m
        if (pFile == NULL ||
            pfieldLst == NULL
            /* pCondList == NULL ||
             pSimSpec  == NULL */
        ) return 1;
        fprintf(pFile, "#%5s %-20s %-18s %8s %8s %8s %4s %8s %8s %8s %8s %8s %10s %9s ",
            "INDEX", "TEMPLATE_NAME", "RULESET", "REDSHIFT", "MAG",
            "TEXP(s)", "NSUB", "AIRMASS", "SKYMAG", "IQ", "TILT", "MISALIGN", "RESULT", "SUCCESS?");
        fprintf(pFile, "\n");
        fprintf(pFile, "\n");
        return 0;
    }
    //////////////////////////////////////////////////////////////////////////////
    /// function to loop over the various fields read the Fovs and look for the number of stars .
    ///
int write_comments_inHeaderFitsFiles(fitsfile * fptr_outputresult,
    fitsfile * fptr_outputfailure,
    fieldlist_struct * pFldLst,
    starcat_struct * pStarCat,
    focalplane_struct * pFocpl) {
    char buf[125];
    time_t timestamp;
    struct tm * t;
    time((time_t * ) & timestamp);
    t = localtime( & timestamp);
    //camera_struct *pCameraloc= (camera_struct*)  &(pFocpl->pCaM[0]);
    snprintf(buf, sizeof(buf), "file created on %s", asctime(t));
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "tilling list %s", pFldLst->str_filename);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "star catalogue %s", pStarCat->str_starcat_filename);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "focal plane descr %s", pFocpl->str_filename);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);

    snprintf(buf, sizeof(buf), "Mag limit Min for Fibre %f", pFocpl->MagLimMin_Fibre);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "Mag limit Max for Fibre %f", pFocpl->MagLimMax_Fibre);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);

    snprintf(buf, sizeof(buf), "Mag limit Min for Cam AnG %f", pFocpl->MagLimMin_CamAnG);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "Mag limit Max for Cam AnG %f", pFocpl->MagLimMax_CamAnG);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);

    snprintf(buf, sizeof(buf), "Mag limit Min for Cam WFS  %f", pFocpl->MagLimMin_CamWFS);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);
    snprintf(buf, sizeof(buf), "Mag limit Max for Cam WFS %f", pFocpl->MagLimMax_CamWFS);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputfailure, buf);

    snprintf(buf, sizeof(buf), "Xcorner_FOV_LIST store in a 5 elements array the coordinates of the FOV in MM in the focal plane,with the coordinates of the corner for the cameras, or the patrol radius for the fibre guide (3 last values are zero )");
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    snprintf(buf, sizeof(buf), "Ycorner_FOV_LIST store in a 5 elements array the coordinates of the FOV in MM in the focal plane,with the coordinates of the corner for the cameras, or the patrol radius for the fibre guide (3 last values are zero )");
    write_comment_in_fits_header((fitsfile * * ) & fptr_outputresult, buf);
    snprintf(buf, sizeof(buf), "RA_LIST DEC_LIST store in a 5 elements array the coordinates of the 5 brightest stars in each of the FOV (in sky coordinates)");

    return 0;
}

//---------------------------------------------------------------------------------------------------
//// this function open an prepare a fits file to store the results: the list of guide stars and status of success of the field.
int write_detailed_summary_to_file_fits_header(fitsfile * * ppOutputFile, char * str_OutputFilename) {
        int status = 0;
        if (fits_create_file((fitsfile * * ) ppOutputFile, (char * ) str_OutputFilename, & status)) {
            fits_report_error(stderr, status);
            fprintf(stderr, "%s  I had problems opening the file summary: %i \n",
                ERROR_PREFIX, status);
            fflush(stderr);
            return 1;
        }
        //    ttype[j]="FIELD_NAME"      ;tform[j]="12A"       ;tunit[j]=""               ;colfmt[j]=TSTRING;colwidth[j]=1;

        const char * ttype[] = {
            "FIELD_INDEX",
            "FIELD_NAME",
            "RA_CENTRE",
            "DEC_CENTRE",
            "PHASE_ANGLE_INIT",
            "PHASE_ANGLE_UPDATED",
            "FOV_INDEX",
            "FOV_TYPE",
            "RA_FOV",
            "DEC_FOV",
            "Xcorner_FOV_LIST",
            "Ycorner_FOV_LIST",
            "RAcorner_FOV_LIST",
            "DECcorner_FOV_LIST",
            "RA_LIST",
            "DEC_LIST",
            "MAG_B_LIST",
            "MAG_R_LIST",
            "MAG_I_LIST",
            "NSTARS",
            "FOV_STATUS",
            "FIELD_STATUS",
            "ROTATED"
        };
        char * tform[] = {
            "1I",
            "25A",
            "1D",
            "1D",
            "1D",
            "1D",
            "1I",
            "25A",
            "1D",
            "1D",
            "4D",
            "4D",
            "4D",
            "4D",
            "5D",
            "5D",
            "5D",
            "5D",
            "5D",
            "1I",
            "1L",
            "1L",
            "1L"
        };
        const char * tunit[] = {
            "",
            "",
            "Deg",
            "Deg",
            "Deg",
            "Deg",
            "",
            "",
            "Deg",
            "Deg",
            "MM",
            "MM",
            "Deg",
            "Deg",
            "Deg",
            "Deg",
            "mag",
            "mag",
            "mag",
            "nb of stars in the FoV",
            "",
            "",
            "status"
        };
        LONGLONG SummaryFile_num_rows = 1; // initial value will be updated latter during the insertion process
        int ncols = 23;
        if (fits_create_tbl( * ppOutputFile, (int) BINARY_TBL,
                (LONGLONG) SummaryFile_num_rows, (int) ncols,
                (char * * ) ttype, (char * * ) tform, (char * * ) tunit,
                (char * )
                "SUMMARY",
                (int * ) & status)) {
            fits_report_error(stderr, status);
            fprintf(stderr, "%s  error on create tbl: %s\n",
                ERROR_PREFIX, str_OutputFilename);
            fflush(stderr);
            return 1;
        }

        return 0;

    }
    // add a comment in the primary header
int write_comment_in_fits_header(fitsfile * * ppOutputFile, char * str_comment) {
    int status = 0;
    fits_write_comment( * ppOutputFile, str_comment, & status);
    return status;
}

//// this function open an prepare a fits file to store the summarised results: the number of stars and the result status of the pointing.
int write_summarized_failure_results_to_file_fits_header(fitsfile * * ppOutputFile, char * str_OutputFilename) {
    int status = 0;
    if (fits_create_file((fitsfile * * ) ppOutputFile, (char * ) str_OutputFilename, & status)) {
        fits_report_error(stderr, status);
        fprintf(stderr, "%s  I had problems opening the summarised result file: %i \n",
            ERROR_PREFIX, status);
        fflush(stderr);
        return 1;
    }

    const char * ttype[] = {
        "FIELD_INDEX",
        "FIELD_NAME",
        "RA_CENTRE",
        "DEC_CENTRE",
        "PHASE_ANGLE_INIT",
        "PHASE_ANGLE_UPDATED",
        "FIBRE_STATUS",
        "FIBRE_OPPOSITE",
        "CAMERA_STATUS",
        "WFS_STATUS",
        "ANG_STATUS",
        "ROTATED"
    }; //, "FIELD_STATUS" };
    char * tform[] = {
        "1I",
        "25A",
        "1D",
        "1D",
        "1D",
        "1D",
        "1L",
        "1L",
        "1L",
        "1L",
        "1L",
        "1L"
    }; //, "1L" };
    const char * tunit[] = {
        "",
        "",
        "Deg",
        "Deg",
        "Deg",
        "Deg",
        "status",
        "status",
        "status",
        "status",
        "status",
        "status"
    }; //, "status"};
    LONGLONG SummaryFile_num_rows = 0; // initial value will be updated latter during the insertion process
    int ncols = 12;

    if (fits_create_tbl( * ppOutputFile, (int) BINARY_TBL,
            (LONGLONG) SummaryFile_num_rows, (int) ncols,
            (char * * ) ttype, (char * * ) tform, (char * * ) tunit,
            (char * )
            "FAILURE",
            (int * ) & status)) {
        fits_report_error(stderr, status);
        fprintf(stderr, "%s  error on create tbl: %s\n",
            ERROR_PREFIX, str_OutputFilename);
        fflush(stderr);
        return 1;
    }
    return 0;
}

// this functrion add line in the tbl created by the function above
int write_detailed_summary_to_file_fits_data(fitsfile * pOutputFile, guidestars_struct * arr_guidestars,
        int pointingid, char * fieldname,
        float RApointing, float DECpointing,
        float PhaseAngleInit,
        float PhaseAngleUpdated,
	pointingStarsPerFovs_struct * currentPointingMetadata,
        BOOL * statusFov,
        char * * arrayoffovtype, // dimension [nbtotal fov ] [STR_MAX= 512]
        BOOL FieldStatus,
        BOOL RotationStatus,
        int nstars) {
        // definition of the columns.
        //	const char *ttype[] = {"FIELD_INDEX" ,"FIELD_NAME" 	,"RA_CENTRE", "DEC_CENTRE"	,  "PHASE_ANGLE_INIT" , "PHASE_ANGLE_UPDATED"  , "FOV_INDEX", "FOV_TYPE",  "RA_FOV" , "DEC_FOV" , "Xcorner_FOV_LIST", "Ycorner_FOV_LIST", "RA_LIST" , "DEC_LIST" , "MAG_B_LIST" , "MAG_R_LIST"  , "MAG_I_LIST" 	, "NSTARS" , "FOV_STATUS", "FIELD_STATUS"  , "ROTATED"};
        //	char       *tform[] = {"1I"          ,"25A"			,"1D"     	, "1D"   	    ,  "1D"   	          , "1D"   	        	   , "1I"       ,"25A"    	,  "1D"     , "1D"      , "5D"              , "5D"              , "5D"     	, "5D"       , "5D"      	, "5D"      	, "5D"      	, "1I"     , "1L"        , "1L"            , "1L"};
        //	const char *tunit[] = {""            , ""			,"Deg"    	,"Deg"      	,  "Deg"			  , "Deg"   	           , ""         , ""        ,  "Deg"    , "Deg"     , "MM"				, "MM"			    , "Deg"   	, "Deg"      , "mag"     	, "mag"     	, "mag"     	, ""       , ""          , ""              , "status"};
        //	int ncols = 21;
        int status = 0;
        int istar, currentFov;
        long currentLine = 0;
        long lineNB;
        float flzero[5];
        for (istar = 0; istar < 5; istar++) {
            flzero[istar] = 0;
        }
        float RA[5], DEC[5], MAG_I[5];
        float Xpos[4], Ypos[4];
	float  RAcorner[4];
	float DECcorner[4];
	float RaFov, DecFov;
	int nmaxstars = 0;
	for (currentFov = 0; currentFov < (NB_WFS + NB_AnG + NB_FIBGUIDE) /*18*/ ; currentFov++) { //loop on fov , read the array , file the needed sub arrays then at the end write the lines for the fov
            //MAG_B[5] , MAG_R[5],
            //int istar;
            for (istar = 0; istar < 5; istar++) {
                flzero[istar] = 0;
                RA[istar] = 0;
                DEC[istar] = 0;
                MAG_I[istar] = 0;
            }
            lstobjectsInFoV_struct * CurrentMetdata = & (currentPointingMetadata->FOVs[currentFov]);
            for (istar = 0; istar < 4; istar++) {
                Xpos[istar] = 0;
                Ypos[istar] = 0;
		RAcorner[istar]=  (float)    ( CurrentMetdata->RA_corner[istar]);
		DECcorner[istar]=  (float)    ( CurrentMetdata->DEC_corner[istar]);
            }
           
	    
	    
// 	    for (int ii=0;ii<4;ii++){
// 	      RAcorner[ii]=  (float)    ( CurrentMetdata->RA_corner[ii]);
// 	      DECcorner[ii]=  (float)    ( CurrentMetdata->DEC_corner[ii]);
// 	      fprintf(stdout, "%s PROUTCH in the writing FUNCTION for pointing %i Fov %i \tRA  corner %i  =%f    \n", COMMENT_PREFIX,pointingid,currentFov,ii, RAcorner[ii] );
// 	      fprintf(stdout, "%s PROUTCH                                                \tDEC corner %i  =%f    \n", COMMENT_PREFIX,ii, DECcorner[ii] );
// 	    }
	    
          
            nmaxstars = 0;
	    int i;
            //char * strfovtype = NULL;;
            status = 0;
            //Xcorner_FOV_LIST
            // for each fov we search if it have some stars stored , what if it has no stars ?
            //printf("%s 	DEBUG LOOP \n",DEBUG_PREFIX);
            for (lineNB = 0; lineNB < nstars; lineNB++) {
                if (( & arr_guidestars[lineNB])->fovID == currentFov) {
                    RA[nmaxstars] = ( & arr_guidestars[lineNB])->RAstar;
                    DEC[nmaxstars] = ( & arr_guidestars[lineNB])->DECstar;
                    //MAG_B[nmaxstars] = (&arr_guidestars[lineNB])->MAGBobj;
                    //strfovtype = ( & arr_guidestars[lineNB])->fovtype;
                    //MAG_R[nmaxstars]= (&arr_guidestars[lineNB])->MAGRobj;
                    MAG_I[nmaxstars] = ( & arr_guidestars[lineNB])->MAGIobj;
                    RaFov = ( & arr_guidestars[lineNB])->RAfov;
                    DecFov = ( & arr_guidestars[lineNB])->DECfov;
                    for (i = 0; i < 4; i++) {
                        Xpos[i] = (float)( & arr_guidestars[lineNB])->Xcoord_FOV[i];
                        Ypos[i] = (float)( & arr_guidestars[lineNB])->Ycoord_FOV[i];
                        //printf("%s 	LOOP corner %i Xpos should be %f is %f\n",COMMENT_PREFIX,i, (float)(&arr_guidestars[lineNB])->Xcoord_FOV[i], Xpos[i]);
                    }
                    nmaxstars++;
                }
            }

            BOOL curr_statusFov = statusFov[currentFov];
            if (pointingid > 0) {
                fits_get_num_rows((fitsfile * ) pOutputFile, & currentLine, & status);
            }
            currentLine++;

            fits_write_col((fitsfile * ) pOutputFile, TINT, 1, currentLine, 1, 1, & pointingid, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TSTRING, 2, currentLine, 1, 1, & fieldname, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 3, currentLine, 1, 1, & RApointing, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 4, currentLine, 1, 1, & DECpointing, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 5, currentLine, 1, 1, & PhaseAngleInit, (int * ) & status);
            // always write the rotation status , but store the rotation angle only if rotated
            fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 21, currentLine, 1, 1, & RotationStatus, (int * ) & status);
            if (RotationStatus == TRUE) {
                fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 6, currentLine, 1, 1, & PhaseAngleUpdated, (int * ) & status);
            }

            fits_write_col((fitsfile * ) pOutputFile, TINT, 7, currentLine, 1, 1, & currentFov, (int * ) & status);
            
            //if (strfovtype!=NULL) // when there is no star for a Fov then there is no line with the fovid and so strfovtype is not initialized (correction must be done in the code)
            //fits_write_col ((fitsfile*) pOutputFile, TSTRING,  8,  currentLine,1, 1, &strfovtype, (int*)&status);
            fits_write_col((fitsfile * ) pOutputFile, TSTRING, 8, currentLine, 1, 1, ( & arrayoffovtype[currentFov]), (int * ) & status);

            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 9, currentLine, 1, 1, & RaFov, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 10, currentLine, 1, 1, & DecFov, (int * ) & status);
            // JUNE 16 add an array of pos in mm in focal planes
            //if (RotationStatus==TRUE){
            // store xpos and ypos of the corners 
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 11, currentLine, 1, 4, & Xpos, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 12, currentLine, 1, 4, & Ypos, (int * ) & status);
            //}
            //if (RotationStatus==TRUE){// temporary addition (april 6 17) will be a RA DEC array of corners 
            //printf("%s    => guidestarsCorrec for field =%i fov=%i (FIBRE) center of FOV    Xcoord_FOV[0]  Ycoord_FOV[0] are %f %f \n",COMMENT_PREFIX,i,jfov ,(&arr_guidestarsCorrect[ID_star_stored])->Xcoord_FOV[0],(&arr_guidestarsCorrect[ID_star_stored])->Ycoord_FOV[0]);

	    fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 13, currentLine, 1, 4, & RAcorner, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 14, currentLine, 1, 4, & DECcorner, (int * ) & status);

            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 15, currentLine, 1, 5, & RA, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 16, currentLine, 1, 5, & DEC, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 17, currentLine, 1, 5, & flzero, (int * ) & status); //magb column
            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 18, currentLine, 1, 5, & flzero, (int * ) & status); //magr column

            fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 19, currentLine, 1, 5, & MAG_I, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TINT, 20, currentLine, 1, 1, & nmaxstars, (int * ) & status);
            fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 21, currentLine, 1, 1, & curr_statusFov, (int * ) & status); //fov status
            fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 22, currentLine, 1, 1, & FieldStatus, (int * ) & status); //fov status
        }
        return 0;
    }
    // this functrion add one line in the tbl created by the function above
int write_detailed_summary_to_file_fits_one_data(fitsfile * pOutputFile, guidestars_struct * arr_guidestars,
        int pointingid, int fovid, float RApointing, float DECpointing,
        float RaFov, float DecFov, BOOL * statusFov, int nstars) {

        // definition of the columns.
        //const char *ttype[] = {"FIELD_INDEX" , "RA_CENTRE", "DEC_CENTRE", "FOV_INDEX",  "RA_FOV" , "DEC_FOV" , "RA_LIST"  , "DEC_LIST" , "MAG_B_LIST" , "MAG_R_LIST" , "MAG_I_LIST" , "NSTARS" , "FOV_STATUS", "FIELD_STATUS"  };
        //char       *tform[] = {"1I"          , "30A"      , "30A"   	  , "1I"       ,  "1D"     , "1D"      , "5D"    , "5D"    , "5D"      , "5D"      , "5D"      , "1I"     , "1L"        , "1L"            };
        //const char *tunit[] = {""            , "Deg"      , "Deg"       , ""         ,  "Deg"    , "Deg"     , "Deg"   , "Deg"   , "mag"     , "mag"     , "mag"     , ""       , ""          , ""              };

        int status = 0;
        long ligneNB = 0;
        float RA[5], DEC[5], MAG_B[5], MAG_R[5], MAG_I[5];
        int nmaxstars = MIN(nstars, 5);
        for (ligneNB = 0; ligneNB < nmaxstars; ligneNB++) {
            RA[ligneNB] = ( & arr_guidestars[ligneNB])->RAstar;
            DEC[ligneNB] = ( & arr_guidestars[ligneNB])->DECstar;
            MAG_B[ligneNB] = ( & arr_guidestars[ligneNB])->MAGBobj;
            MAG_R[ligneNB] = ( & arr_guidestars[ligneNB])->MAGRobj;
            MAG_I[ligneNB] = ( & arr_guidestars[ligneNB])->MAGIobj;
        }
        if (pointingid > 0) {
            fits_get_num_rows((fitsfile * ) pOutputFile, & ligneNB, & status);
        }
        ligneNB++;
        fits_write_col((fitsfile * ) pOutputFile, TINT, 1, ligneNB, 1, 1, & pointingid, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 2, ligneNB, 1, 1, & RApointing, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 3, ligneNB, 1, 1, & DECpointing, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TINT, 4, ligneNB, 1, 1, & fovid, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 5, ligneNB, 1, 1, & RaFov, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 6, ligneNB, 1, 1, & DecFov, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 7, ligneNB, 1, 5, & RA, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 8, ligneNB, 1, 5, & DEC, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 9, ligneNB, 1, 5, & MAG_B, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 10, ligneNB, 1, 5, & MAG_R, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 11, ligneNB, 1, 5, & MAG_I, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TINT, 12, ligneNB, 1, 1, & nmaxstars, (int * ) & status);
        fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 12, ligneNB, 1, 1, statusFov, (int * ) & status);
        return 0;
    }
    // this function add one line for each failing pointing in the tbl created by the function above
int write_failure_summary_to_file_fits_one_data(fitsfile * pOutputFile,
    int pointingid, char * fieldname, float RApointing, float DECpointing,
    BOOL fibrestatus,
    BOOL oppositefibrestatus,
    BOOL camerastatus,
    BOOL wfsstatus,
    BOOL angstatus
) {
    //definition of the columns
    //	const char *ttype[] = {"FIELD_INDEX" ,"FIELD_NAME" 	, "RA_CENTRE", "DEC_CENTRE", "PHASE_ANGLE_INIT" , "PHASE_ANGLE_UPDATED"	, "FIBRE_STATUS", "FIBRE_OPPOSITE", "CAMERA_STATUS","WFS_STATUS","ANG_STATUS", "ROTATED"};
    //	char       *tform[] = {"1I"          ,"25A"			, "1D"       , "1D"   	   , "1D"   	        ,"1D"   	        	, "1L"          , "1L"            , "1L"           , "1L"       , "1L"       , "1L"};
    //	const char *tunit[] = {""            , ""			, "Deg"      , "Deg"       , "Deg"				,"Deg"   	        	, "status"      , "status"       , "status"        , "status"   , "status"   , "status"};

    int status = 0;
    long ligneNB = 0;
    fits_get_num_rows((fitsfile * ) pOutputFile, & ligneNB, & status);
    ligneNB++;
    fits_write_col((fitsfile * ) pOutputFile, TINT, 1, ligneNB, 1, 1, & pointingid, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TSTRING, 2, ligneNB, 1, 1, & fieldname, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 3, ligneNB, 1, 1, & RApointing, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TFLOAT, 4, ligneNB, 1, 1, & DECpointing, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 7, ligneNB, 1, 1, & fibrestatus, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 8, ligneNB, 1, 1, & oppositefibrestatus, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 9, ligneNB, 1, 1, & camerastatus, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 10, ligneNB, 1, 1, & wfsstatus, (int * ) & status);
    fits_write_col((fitsfile * ) pOutputFile, TLOGICAL, 11, ligneNB, 1, 1, & angstatus, (int * ) & status);
    return 0;
}

//---------------------------------------------------------------------------------------------------
//read the TUNIT keyword for the given column and interpret the content
int fits_read_unit_for_numbered_column(fitsfile * pFile, int colnum, char * str_unit, int * punit_type, double * punit) {
    char str_keyname[STR_MAX];
    char str_key[STR_MAX];
    int status = 0;
    snprintf(str_keyname, STR_MAX, "TUNIT%d", colnum);
    if (fits_read_key((fitsfile * ) pFile, (int) TSTRING, str_keyname, str_key, NULL, & status)) {
        fits_report_error(stderr, status);
        return 1;
    }
    //now preprocess the key string
    //  fprintf(stdout, "%s Processing units key %s: ->%s<-\n", DEBUG_PREFIX, str_keyname, str_key); fflush(stdout);
    if (interpret_unit_string((char * ) str_key, (int * ) punit_type, (double * ) punit)) return 1;
    return 0;
}

// function for the sorting of the stars
//int compare(guidestars_struct *elem1, guidestars_struct *elem2)
int compare(const void * elem1,  const void * elem2) {
    if (((guidestars_struct * ) elem1)->MAGIobj < ((guidestars_struct * ) elem2)->MAGIobj)
        return -1;
    
    else if (((guidestars_struct * ) elem1)->MAGIobj > ((guidestars_struct * ) elem2)->MAGIobj)
        return 1;
    else
        return 0;
    return 0;
}

//https://heasarc.gsfc.nasa.gov/xanadu/xspec/manual/XSweight.html
//note that readnoise is actually Gaussian distributed but here is treated as a Poisson noise component
double calc_noise_gehrels_style(double signal, double bkgnd, double sq_read_noise, double skysub_residual) {
    return (double)(1.0 +
        sqrt((double) 0.75 + signal + bkgnd + sq_read_noise) +
        skysub_residual * bkgnd);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
int _4FS_RST_print_version(FILE * pFile) {
    if (pFile == NULL) return 1;
    GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__, __VERSION__);
    return 0;
}

int print_version_and_inputs(FILE * pstream, int argc, char * * argv) {
    int i;
    //print out the command line and program details
    //some of these values are set by CVS
    if (pstream == NULL) return 1;
    //  if ( _4FS_RST_print_version ((FILE*) pstream)) return 1;
    fprintf(stdout, "%s The command line (%d args) was: ", COMMENT_PREFIX, argc);
    for (i = 0; i < argc; i++) {
        fprintf(stdout, "%s ", argv[i]);
    }
    fprintf(stdout, "\n");
    fflush(stdout);
    return 0;
}

int print_full_version_information_to_file(const char * str_filename) {
    FILE * pFile = NULL;
    fprintf(stdout,"%s print_full_version_information_to_file ## %s\n",COMMENT_PREFIX, str_filename);
    if ((pFile = (FILE * ) fopen(str_filename, "w")) == NULL) {
        fprintf(stderr, "%s  I had problems opening the file 4: %s\n",
            ERROR_PREFIX, str_filename);
        fflush(stderr);
        return 1;
    }

    if (print_full_version_information((FILE * ) pFile)) {
        fprintf(stderr, "%s  I had problems writing to the file: %s\n",
            ERROR_PREFIX, str_filename);
        fflush(stderr);
        return 1;
    }

    fflush(pFile);
    if (pFile) fclose(pFile);
    pFile = NULL;
    return 0;
}

int print_full_version_information(FILE * pFile) {
    if (pFile == NULL) return 1;
    fprintf(pFile, "%s Full software version information:\n", COMMENT_PREFIX);
    if (_4FS_RST_print_version((FILE * ) pFile)) return 1;
    if (RST_input_params_print_version((FILE * ) pFile)) return 1;
    if (ParLib_print_version((FILE * ) pFile)) return 1;
    if (util_print_version((FILE * ) pFile)) return 1;

    return 0;
}

int print_full_help_information(FILE * pFile) {
    if (pFile == NULL) return 1;
    fprintf(pFile, "%s Printing full help information as requested:\n", COMMENT_PREFIX);
    fflush(pFile);
    if (RST_input_params_print_usage_information(pFile)) return 1;
    return 0;
}

// function to read the catfor one given pointing, used for test with upodated phase angle
int read_starcat_for_pointing(fitsfile * fptr_starcat, starcat_struct * pStarCat,
    pointingStarsPerFovs_struct * currentPointing, focalplane_struct * pFocpl,
    long * * array_firstrowhealpixnumber, long * * array_ofnbrows,
    double phaseangle, int FieldIndex) {
    double Ra, Dec, radiusDeg;
    double Xfibre = 0, Yfibre = 0, Xcam = 0, Ycam = 0;
    double pntra = 0, pntdec = 0;
    double platescale = 0.0595;
    double fibrespineradiusmm = 0; //11.5;
    double fibrespineradiusArcsec = 0; //fibrespineradiusmm / platescale;
    double fibrespineradiusDeg = 0; //fibrespineradiusArcsec / 3600.;
    double cameraradiusmm = 39.094;
    double cameraradiusArcsec = cameraradiusmm / platescale;
    double cameraradiusDeg = cameraradiusArcsec / 3600.;
    double inv_platescale = 1 / platescale;
    long HPX_MEMBERS_PER_CHUNK = 512;
    long plistpix[HPX_MEMBERS_PER_CHUNK + 1];
    long nlist, hpx_pix_temp;
    long nside = 512;

    BOOL nest_flag = (BOOL) TRUE;
    BOOL inclusive_flag = (BOOL) TRUE;

    if (!isnan(currentPointing->RAPointing) && !isnan(currentPointing->DECPointing)) { // if data for the field
        Ra = currentPointing->RAPointing;
        Dec = currentPointing->DECPointing;
        //RotAngle=pField->PA;// must be replaced by a parameter input
        lstobjectsInFoV_struct * starsInFov = NULL;
        //		//read the first spine guide from the focal guide
        //		//fprintf(stdout,"%s ////////////////////////////////////////////////////////////////////////////////////////////////////////////// \n",COMMENT_PREFIX);
        //		//fprintf(stdout,"%s ////                                   start field   %i                                                       // \n",COMMENT_PREFIX,i);
        //		//fprintf(stdout,"%s // \n",COMMENT_PREFIX);
        //		//fprintf(stdout,"%s //      RA=%f DEC=%f rotation angle =%f\n",DEBUG_PREFIX,Ra,Dec,RotAngle);
        int nbfibre;
        char str_cataloguefitsfile[STR_MAX];
        strncpy(str_cataloguefitsfile, pStarCat->str_starcat_filename, STR_MAX);
        int nbfov;
        for (nbfov = 0; nbfov < (NB_WFS + NB_AnG + NB_FIBGUIDE) /*18*/ ; nbfov++) { // loop on the Fovs  -> depend of the structure of the focal plane could need an update?
            starsInFov = & (currentPointing->FOVs[nbfov]);
            starsInFov->fieldID = FieldIndex;
            if (nbfov >= 0 && nbfov < 12) { //fibre
                nbfibre = nbfov;
                fibre_struct * pFibreloc = (fibre_struct * ) & (pFocpl->pFibre[nbfibre]);
                if (pFibreloc == NULL) {
                    fprintf(stderr, "%s problem reading the fibre spine  \n", ERROR_PREFIX);
                    return 1;
                }
                Xfibre = pFibreloc->Xcen;
                Yfibre = pFibreloc->Ycen;
                /* added on june 2 */
                fibrespineradiusmm = pFibreloc->Radius_mm;
                fibrespineradiusArcsec = fibrespineradiusmm / platescale;
                fibrespineradiusDeg = fibrespineradiusArcsec / 3600.;
                //fprintf(stderr,"%s Xfibre=%lf   Yfibre=%lf\n",ERROR_PREFIX, Xfibre, Yfibre);
                radiusDeg = fibrespineradiusDeg;
                if (geom_project_inverse_gnomonic_from_mm(Ra, Dec, phaseangle, Xfibre, Yfibre, inv_platescale, (double * ) & (pntra), (double * ) & (pntdec)) == 1) {
                    fprintf(stderr, "%s problem finding the sky coordinates of the Fov %i\n", ERROR_PREFIX, nbfov);
                    return 1;
                }
                //fprintf(stderr,"%s     fov %i   FOR CURRENT  FIBRE WITH ROTATION rot%f coord center Xcam %f Ycam %f	RA =%f DEC =%f\n",DEBUG_PREFIX,nbfov,RotAngle,Xfibre,Yfibre,pntra,pntdec);
            } else {
                // Warning : in case of problem correct the -12 or -11 for the indice
                camera_struct * pCameraloc = (camera_struct * ) & (pFocpl->pCaM[nbfov - 12]);
                if (pCameraloc == NULL) {
                    fprintf(stderr, "%s problem reading the Camera  \n", ERROR_PREFIX);
                    return 1;
                }
                strncpy(starsInFov->str_FovTYPE, pCameraloc->str_type, STR_MAX);
                Xcam = pCameraloc->Xcen;
                Ycam = pCameraloc->Ycen;
                radiusDeg = cameraradiusDeg;
                if (geom_project_inverse_gnomonic_from_mm(Ra, Dec, phaseangle, Xcam, Ycam, inv_platescale, (double * ) & (pntra), (double * ) & (pntdec)) == 1) {
                    fprintf(stderr, "%s problem finding the sky coordinates of the Fov \n", ERROR_PREFIX);
                    return 1;
                }
            }
            nbfibre = nbfov;
            // Here pntra and pntdec store the ra/dec position of the fibre center
            // put zero in all the array,   optional if we assure to trace the number of hpx pixels in
            int init;
            for (init = 0; init < HPX_MEMBERS_PER_CHUNK + 1; init++) {
                plistpix[init] = 0;
            }
            // check that this is appropriate
            (void) ang2pix_nest(nside, M_PI_2 - pntdec * DEG_TO_RAD, pntra * DEG_TO_RAD, (long * ) & (hpx_pix_temp));
            nlist = 0;
            // find the number of healpix inside the region of interest radius depend of the fov type : fibre or camera (see above)
            hpx_extras_query_disc(nside, (double) pntra, (double) pntdec, (double) radiusDeg, (long) HPX_MEMBERS_PER_CHUNK, (long * ) plistpix, (long * ) & nlist, (BOOL) nest_flag, (BOOL) inclusive_flag);
            //check of the nbr of healpix pixels and their index
            if (nlist < 1) {
                fprintf(stderr, "		%s  ERROR when reading the Field %6d %10.5f %10.5f found %li hpx pixels\n", WARNING_PREFIX,
                    FieldIndex, (double) pntra, (double) pntdec, nlist);
                return 1;
            }
            // reserve space for the return value : a new struct (4/12/15) must store the stars properties to filter them latter.
            // take the 2 arrays and their dimension and read the star catalogue , retrieve the stars one by one and keep trace of it
            //define the fov parameters in the structure
            starsInFov->FovID = nbfov;
            starsInFov->RAcenterFov = pntra;
            starsInFov->DECcenterFov = pntdec;
            starsInFov->ValidityFlag = TRUE; // by default before the retrieve and filtering

            // read a full list of stars present in or around the Fov
            char * str_magIcolumnName = pStarCat->str_magI_colname;
            int retour = access_fits_catalogue(str_cataloguefitsfile, (fitsfile * ) fptr_starcat, (long * * ) plistpix, array_firstrowhealpixnumber,
                array_ofnbrows, nlist, starsInFov, nbfibre, str_magIcolumnName);
            if (retour != 0) {
                return 1;
            };
            currentPointing->pointingStatus = starsInFov->ValidityFlag;
        }
    } else {
        currentPointing->pointingStatus = FALSE;
    }
    return 0;
}

//write a handy html summary page
int write_summary_page_html(param_list_struct * pParamList,
    const char * str_filename) {
    FILE * pFile = NULL;
    char str_cwd[STR_MAX];
    if (pParamList == NULL) return 1;

    if ((pFile = (FILE * ) fopen(str_filename, "w")) == NULL) {
        fprintf(stderr, "%s  I had problems opening the file 5: %s\n",
            ERROR_PREFIX, str_filename);
        fflush(stderr);
        return 1;
    }

    //get the working directory path
    if (getcwd((char * ) str_cwd, (size_t) STR_MAX) == NULL) {
        fprintf(stderr, "%s Could not get CWD at (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        (void) fflush(stderr);
        return 1;
    }

    fprintf(stdout, "%s Writing HTML summary file to: file://%s/%s\n",
        COMMENT_PREFIX, str_cwd, str_filename);
    fflush(stdout);
    //write HTML header inc style sheet
    fprintf(pFile, "<!DOCTYPE html>\n\
<html>\n\
<head>\n\
  <title>4FS TS results summary page</title>\n\
  <style type='text/css'>\n\
    body {font-size:80%%;}\n\
    h1 {font-size:2.5em;}\n\
    h2 {font-size:1.875em;}\n\
    p  {font-size:0.875em;}\n\
    tr {background-color:#F0F0F0;}\n\
    td {font-size:0.800em;}\n\
    #header-row  {background-color:#A0A0A0; font-size:1.20em;}\n\
    #special-row {background-color:#E0E0E0;}\n\
  </style>\n\
  <meta name='description' content='A simple summary page detailing one run of the 4MOST Facility Simulator Throughput Simulator (4FS TS)' />\n\
  <link rel='icon' type='img/gif' href='http://www.mpe.mpg.de/~tdwelly/4MOSTlogo_icon.gif' />\n\
</head>\n");

    fprintf(pFile, "<body>\n\
<img src='http://www.aip.de/en/research/research-area-ea/research-groups-and-projects/milky-way/overview-1/4MOSTLogo.png' height='120' alt='4MOST logo' style='float:left;' />\n\
<img src='http://www2011.mpe.mpg.de/PIFICONS/logo-mpe-transparent.gif' height='100' alt='4MOST logo' style='float:right;' />\n\
<h1>4FS TS results summary page</h1>\n");

    {
        time_t rawtime;
        struct tm * timeinfo;
        struct utsname unameinfo;
        char str_hostname[STR_MAX] = {
            '\0'
        };
        //    char str_domainname[STR_MAX] = {'\0'};

        if (time( & rawtime) < 0) return 1;
        timeinfo = localtime( & rawtime);

        if (gethostname((char * ) str_hostname, (size_t) STR_MAX)) return 1;
        //    if ( getdomainname((char*) str_domainname, (size_t) STR_MAX)) return 1;
        if (uname((struct utsname * ) & unameinfo)) return 1;

        fprintf(pFile, "<h3>Simulation finish date/time: %s</h3>\n\
<h3>Simulation run on host: %s (%s %s %s %s) </h3>\n",
            asctime(timeinfo),
            str_hostname, //str_domainname,
            unameinfo.sysname, unameinfo.release, unameinfo.version, unameinfo.machine);
    }

    {
        char buffer[STR_MAX] = {
            '\0'
        };
        if (ParLib_get_param_str_value((param_list_struct * ) pParamList,
                (const char * )
                "PARAM_FILENAME", (char * ) buffer) > 0) return 1;

        fprintf(pFile, "<h2> Input files/parameters</h2>\n\
<a href='%s' target='_blank' type='text/plain' title='The input parameter file that was supplied to the TS'>Full Input Parameter file</a><br>\n\
<a href='%s' target='_blank' type='text/plain' title='The array of input parameters after interpretation by the TS'>Input parameter array after interpretation</a>\n\
<br><br>\n",
            buffer, "RST_interpreted_parameters.txt");
    }

    fprintf(pFile, "<table border=0>\n\
  <tr id='header-row'>\n\
    <td>Selected Parameter Name</td>\n\
    <td>Value</td>\n\
    <td>Units</td>\n\
  </tr>\n");

    //  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "TELESCOPE.FOV",             "",  "deg<sup>2</sup>")) return 1;

    fprintf(pFile, "</table>\n");

    {
        time_t rawtime;
        struct tm * timeinfo;
        if (time((time_t * ) & rawtime) < 0) return 1;
        timeinfo = localtime( & rawtime);
        fprintf(pFile, " <hr>\n\
\n\
A standard product of the <span style='color:blue;'>%s</span>. Written at %s. \n\
Contact <a href='mailto:%s?Subject=4FS%%20TS'>%s</a> for further information.</p>\n\
</body>\n\
</html>\n\n\n",
            RST_BRANDING_STRING,
            asctime(timeinfo), CONTACT_EMAIL_ADDRESS, CONTACT_EMAIL_ADDRESS);
    }
    fflush(pFile);
    if (pFile) fclose(pFile);
    pFile = NULL;
    return 0;
}

/// utilities functions //////
int interpret_unit_string(char * str_unit, int * punit_type, double * punit) {
    int i, j;
    char buffer[STR_MAX];
    char last_char;
    char * pbuf;

    if (strlen(str_unit) >= STR_MAX) return 1;
    /*  else if ( strlen(str_unit) == 0 )
      {
        *punit_type = (int) CURVE_UNIT_TYPE_UNKNOWN;
        *punit      = (double) NAN;
        return 0;
      }*/
    //now preprocess the unit string
    //convert to lower case, and remove "[","]", carets
    j = 0;
    last_char = '\0';
    for (i = 0; i < strlen(str_unit); i++) {
        if (str_unit[i] != '[' && str_unit[i] != ']' && str_unit[i] != '^') {
            buffer[j] = tolower(str_unit[i]);
            //      buffer[j] = str_unit[i];
            j++;
        }
        last_char = str_unit[i];
    }
    buffer[j] = '\0';
    //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

    //remove any surplus white spaces
    j = 0;
    last_char = '\0';
    for (i = 0; i < strlen(buffer); i++) {
        if (!(buffer[i] == ' ' && (last_char == ' ' || last_char == '/' || last_char == '\0')) &&
            !(buffer[i] == '/' && last_char == '/')) {
            buffer[j] = buffer[i];
            j++;
        }
        last_char = buffer[i];
    }
    buffer[j] = '\0';
    //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

    //make some replacements to simplify the following steps
    //replace variations on "photon" with "ph"
    while ((pbuf = (char * ) strstr(buffer, "photons"))) {
        pbuf[0] = ' ';
        pbuf[1] = ' ';
        pbuf[2] = ' ';
        pbuf[3] = ' ';
        pbuf[4] = ' ';
        pbuf[5] = 'p';
        pbuf[6] = 'h';
    }
    while ((pbuf = (char * ) strstr(buffer, "photon"))) {
        pbuf[0] = ' ';
        pbuf[1] = ' ';
        pbuf[2] = ' ';
        pbuf[3] = ' ';
        pbuf[4] = 'p';
        pbuf[5] = 'h';
    }
    while ((pbuf = (char * ) strstr(buffer, "phot"))) {
        pbuf[0] = ' ';
        pbuf[1] = ' ';
        pbuf[2] = 'p';
        pbuf[3] = 'h';
    }
    //replace "ergs" with "erg"
    while ((pbuf = (char * ) strstr(buffer, "ergs"))) {
        pbuf[0] = ' ';
        pbuf[1] = 'e';
        pbuf[2] = 'r';
        pbuf[3] = 'g';
    }
    //replace "jansky" with "jy"
    while ((pbuf = (char * ) strstr(buffer, "jansky"))) {
        pbuf[0] = ' ';
        pbuf[1] = ' ';
        pbuf[2] = ' ';
        pbuf[3] = ' ';
        pbuf[4] = 'j';
        pbuf[5] = 'y';
    }
    //replace variations on "aa" with "a"
    while ((pbuf = (char * ) strstr(buffer, "\\aa"))) {
        pbuf[0] = ' ';
        pbuf[1] = ' ';
        pbuf[2] = 'a';
    }
    while ((pbuf = (char * ) strstr(buffer, "aa"))) {
        pbuf[0] = ' ';
        pbuf[1] = 'a';
    }
    //replace variations on "/s/unit" with "/unit/s"
    while ((pbuf = (char * ) strstr(buffer, "/a/cm2"))) {
        pbuf[0] = '/';
        pbuf[1] = 'c';
        pbuf[2] = 'm';
        pbuf[3] = '2';
        pbuf[4] = '/';
        pbuf[5] = 'a';
    }
    while ((pbuf = (char * ) strstr(buffer, "/a/m2"))) {
        pbuf[0] = '/';
        pbuf[1] = 'm';
        pbuf[2] = '2';
        pbuf[3] = '/';
        pbuf[4] = 'a';
    }
    while ((pbuf = (char * ) strstr(buffer, "/nm/cm2"))) {
        pbuf[0] = '/';
        pbuf[1] = 'c';
        pbuf[2] = 'm';
        pbuf[3] = '2';
        pbuf[4] = '/';
        pbuf[5] = 'n';
        pbuf[6] = 'm';
    }
    while ((pbuf = (char * ) strstr(buffer, "/nm/m2"))) {
        pbuf[0] = '/';
        pbuf[1] = 'm';
        pbuf[2] = '2';
        pbuf[3] = '/';
        pbuf[4] = 'a';
        pbuf[5] = 'm';
    }
    while ((pbuf = (char * ) strstr(buffer, "a-1 cm-2"))) {
        pbuf[0] = 'c';
        pbuf[1] = 'm';
        pbuf[2] = '-';
        pbuf[3] = '2';
        pbuf[4] = ' ';
        pbuf[5] = 'a';
        pbuf[6] = '-';
        pbuf[7] = '1';
    }
    while ((pbuf = (char * ) strstr(buffer, "a-1 m-2"))) {
        pbuf[0] = 'm';
        pbuf[1] = '-';
        pbuf[2] = '2';
        pbuf[3] = ' ';
        pbuf[4] = 'a';
        pbuf[5] = '-';
        pbuf[6] = '1';
    }
    while ((pbuf = (char * ) strstr(buffer, "nm-1 cm-2"))) {
        pbuf[0] = 'c';
        pbuf[1] = 'm';
        pbuf[2] = '-';
        pbuf[3] = '2';
        pbuf[4] = ' ';
        pbuf[5] = 'n';
        pbuf[6] = 'm';
        pbuf[7] = '-';
        pbuf[8] = '1';
    }
    while ((pbuf = (char * ) strstr(buffer, "nm-1 m-2"))) {
        pbuf[0] = 'm';
        pbuf[1] = '-';
        pbuf[2] = '2';
        pbuf[3] = ' ';
        pbuf[4] = 'n';
        pbuf[5] = 'm';
        pbuf[6] = '-';
        pbuf[7] = '1';
    }
    while ((pbuf = (char * ) strstr(buffer, "/s/cm2"))) {
        pbuf[0] = '/';
        pbuf[1] = 'c';
        pbuf[2] = 'm';
        pbuf[3] = '2';
        pbuf[4] = '/';
        pbuf[5] = 's';
    }
    while ((pbuf = (char * ) strstr(buffer, "/s/m2"))) {
        pbuf[0] = '/';
        pbuf[1] = 'm';
        pbuf[2] = '2';
        pbuf[3] = '/';
        pbuf[4] = 's';
    }
    while ((pbuf = (char * ) strstr(buffer, "s-1 cm-2"))) {
        pbuf[0] = 'c';
        pbuf[1] = 'm';
        pbuf[2] = '-';
        pbuf[3] = '2';
        pbuf[4] = ' ';
        pbuf[5] = 's';
        pbuf[6] = '-';
        pbuf[7] = '1';
    }
    while ((pbuf = (char * ) strstr(buffer, "s-1 m-2"))) {
        pbuf[0] = 'm';
        pbuf[1] = '-';
        pbuf[2] = '2';
        pbuf[3] = ' ';
        pbuf[4] = 's';
        pbuf[5] = '-';
        pbuf[6] = '1';
    }
    //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

    //remove any surplus white spaces
    j = 0;
    last_char = '\0';
    for (i = 0; i < strlen(buffer); i++) {
        if (!(buffer[i] == ' ' && (last_char == ' ' || last_char == '/' || last_char == '\0')) &&
            !(buffer[i] == '/' && last_char == '/')) {
            buffer[j] = buffer[i];
            j++;
        }
        last_char = buffer[i];
    }
    buffer[j] = '\0';
    fprintf(stderr, "%s Interpreting unit string: \"%s\" ==> \"%s\" ==> unknown type\n",
        WARNING_PREFIX, str_unit, buffer);
    return 0;
}