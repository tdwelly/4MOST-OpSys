//----------------------------------------------------------------------------------
//--
//--    Filename: RST_input_params_lib.c
//--    Author: Alain Gueguen	agueguen@mpe.mpg.de
#define CVS_REVISION "$Revision:  $"
#define CVS_DATE     "$Date:  $"
//--    Note: Definitions are in the corresponding header file RST_input_params_lib.h
//--    Description:
//--      This module is used to control all input parameter operations
//--

#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <ctype.h>  //for toupper

#include "RST_input_params_lib.h"
//#include "4FS_RSTmodule.h"

#define ERROR_PREFIX   "#-RST_input_params_lib.c : Error   :"
#define WARNING_PREFIX "#-RST_input_params_lib.c : Warning :"
#define COMMENT_PREFIX "#-RST_input_params_lib.c : Comment :"
#define DEBUG_PREFIX   "#-RST_input_params_lib.c : DEBUG   :"


////////////////////////////////////////////////////////////////////////
//functions
int RST_input_params_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}

int RST_input_params_struct_init (RST_input_params_struct **ppIpar)
{
  if ( ppIpar == NULL ) return 1;
  if ( (*ppIpar) == NULL )
  {
    //make space for the struct
    if ( ((*ppIpar) = (RST_input_params_struct*) malloc(sizeof(RST_input_params_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  (*ppIpar)->SIM_RANDOM_SEED = 0;
  (*ppIpar)->debug = (BOOL) FALSE;
  return 0;
}

int RST_input_params_struct_free (RST_input_params_struct **ppIpar)
{

  if ( ppIpar == NULL ) return 1;
  if ( *ppIpar )
  {
	  *((*ppIpar)->FIELDS_FILENAME)        = (char) '\0';
	  *((*ppIpar)->FOCALPLANE_FILENAME)    = (char) '\0';
	  *((*ppIpar)->HEALPIX_INDEX)          = (char) '\0';
	  *((*ppIpar)->PARAM_FILENAME)         = (char) '\0';
    *((*ppIpar)->RES_OUTDIR)             = (char) '\0';
    *((*ppIpar)->STARCAT_FILENAME)       = (char) '\0';
    *((*ppIpar)->RES_OUTFITSFILENAME)    = (char) '\0';
    *((*ppIpar)->RES_FAILUREFITSFILENAME)= (char) '\0';
    *((*ppIpar)->MAGI_COLNAME)           = (char) '\0';
	  //free(*ppIpar);
	  *ppIpar = NULL;
  }
  return 0;
}

int RST_input_params_print_usage_information(FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile, "\nUsage: \n\
  4FS_RST [SWITCHES] [PARAM_FILENAME=filename] [PARAM1=val1 PARAM2=val2 ...]\n\
\n\
[SWITCHES] are taken from the following list:\n\n\
-d,--debug    Turn on debugging mode (expert use only)\n\n\
-h,--help     Print full help listing (and then exit)\n\
-u,--usage    Print this short usage listing\n\
-v,--version  Print full version information (and then exit)\n\
-x,--example  Print an example input parameter file \n\
              (listing all possible input parameters)\n\
\n\
Parameter values are typically supplied in the file indicated by PARAM_FILENAME\n\
Additional input parameters can be supplied on the command line and will override \n\
any values set in the parameter file\n\
For a full description of input parameters consult the interface documentation\n\
The format for input parameters is 'identifier=value', strings and arrays must\n\
be supplied surrounded by quotes, and the token separator for arrays must be one\n\
of the following characters: ',' '|' ';' ' '\n\
\n");

  return 0;
}


//ways to expand macros to strings
#define xstringify_macro(s) stringify_macro(s)
#define stringify_macro(s) #s


//add all the RST input params to the ParamList structure
int RST_input_params_setup_ParamList ( RST_input_params_struct *pIpar,
                                      param_list_struct* pParamList,
                                      int argc, char** argv) 
{
  param_list_struct *pPL = (param_list_struct*) pParamList;  //just an alias
  if ( pIpar == NULL || pParamList == NULL) return 1;

  //make a copy of the param_list_struct pointer
  pIpar->pParamList =  (param_list_struct*) pParamList;
  
  if(ParLib_setup_param(pPL,"PARAM_FILENAME", PARLIB_ARGTYPE_STRING, 1,pIpar->PARAM_FILENAME, "",FALSE, "parfile" ,"Input parameters filename")) return 1;
  //  ParLib_print_param_list (stdout, (param_list_struct*) pPL);

  //immediately try to read the value of this param from the command line:
  {
    param_struct *pParam = NULL;
    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL, (const char*) "PARAM_FILENAME")) == NULL) return 1;
    //    fprintf(stdout, "DEBUG: line=%d name= >%s<\n", __LINE__, pParam->name); fflush(stdout);

    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;
    //    fprintf(stdout, "DEBUG: line=%d name= >%s< value= >%s<\n", __LINE__, pParam->name, pParam->str_value); fflush(stdout);
  }

  //now set up all the other possible input parameters

  //first set up the switches
  if(ParLib_setup_param(pPL,"--help"    ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-h","Print help information and exit")) return 1;
  if(ParLib_setup_param(pPL,"--usage"   ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-u","Print short usage instructions and exit")) return 1;
  if(ParLib_setup_param(pPL,"--version" ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-v","Print version information and exit")) return 1;
  if(ParLib_setup_param(pPL,"--example" ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-x","Print example input params file and exit")) return 1;
  if(ParLib_setup_param(pPL,"--debug"   ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a",
                        FALSE, "-d","Run in debug mode (verbose output etc)")) return 1;

  if(ParLib_setup_param(pPL,"OPTIMISATION.TILE.FILENAME",  PARLIB_ARGTYPE_STRING,1,pIpar->FIELDS_FILENAME  ,"field_list.txt"  ,
                        TRUE,  "POINTIGS",   "Name of file containing the list of pointing (fields on the sky) to be checked")) return 1;

  if(ParLib_setup_param(pPL,"OPTIMISATION.FOCPLANE.FILENAME",  PARLIB_ARGTYPE_STRING,1,pIpar->FOCALPLANE_FILENAME,"focalplane.txt"  ,
                          TRUE,  "FOCAL_PLANE",   "Name of file containing the description of the focal plane ")) return 1;

  if(ParLib_setup_param(pPL,"RES.OUTDIR",    PARLIB_ARGTYPE_STRING, 1,  pIpar->RES_OUTDIR   ,"./"                   ,
                          TRUE,"OUTDIR", "Where should we put output files?")) return 1;
  if(ParLib_setup_param(pPL,"RES.FITS.FILENAME",    PARLIB_ARGTYPE_STRING, 1,  pIpar->RES_OUTFITSFILENAME   ,"4fsFilteredStar.fits"                   ,
                            TRUE,"OUTFILE", "what is the name of the output result fits file ?")) return 1;
  if(ParLib_setup_param(pPL,"RES.FITS.FAILUREFILENAME",    PARLIB_ARGTYPE_STRING, 1,  pIpar->RES_FAILUREFITSFILENAME,"4fsFailingPointings.fits"                   ,
                            TRUE,"FAILUREOUTFILE", "what is the name of the output fits file for failing pointing?")) return 1;

  if(ParLib_setup_param(pPL,"OPTIMISATION.HEALPIXINDEX.FILENAME",    PARLIB_ARGTYPE_STRING, 1,  pIpar->HEALPIX_INDEX   ,"./"                   ,
                              FALSE,"HEALPIXINDEX", "how do we read the healpix indexes in the star cat?")) return 1;

  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.FILENAME",    PARLIB_ARGTYPE_STRING, 1,  pIpar->STARCAT_FILENAME   ,"./"                   ,
                          FALSE,"STARCATALOGUE", "where do we look for the guide stars?")) return 1;
  //if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMIT.FIBRE",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMFIBRE   ,"17.0"                   ,
  //                          FALSE,"MAGLIMFIBRE", "what is the mag limit of the guide star for fibre ?")) return 1;
  //if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMIT.WFS",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMWFS   ,"17.0"                  ,
  //                            FALSE,"MAGLIMWFS", "what is the mag limit of the guide star for fibre ?")) return 1;
  //if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMIT.ANG",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMAnG   ,"17.0"                   ,
  //                            FALSE,"MAGLIMAnG", "what is the mag limit of the guide star for fibre ?")) return 1;
  // 15 march 2017   -- we use now a magnitude range 
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMITMAX.FIBRE",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMMaxFIBRE   ,"17.0"                   ,
                            TRUE,"MAGLIMFIBRE", "what is the mag limit max of the guide star for fibre ?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMITMAX.WFS",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMMaxWFS   ,"17.0"                  ,
                              TRUE,"MAGLIMWFS", "what is the mag limit max of the guide star for Cam WFS ?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMITMAX.ANG",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMMaxAnG   ,"17.0"                   ,
                              TRUE,"MAGLIMAnG", "what is the mag limit max of the guide star for cam AnG ?")) return 1;
  
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMITMIN.FIBRE",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMMinFIBRE   ,"-99.0"                   ,
                            TRUE,"MAGLIMFIBRE", "what is the mag limit min of the guide star for fibre ?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMITMIN.WFS",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMMinWFS   ,"-99.0"                  ,
                              TRUE,"MAGLIMWFS", "what is the mag limit min of the guide star for cam WFS ?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.MAGLIMITMIN.ANG",    PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MAGLIMMinAnG   ,"-99.0"                   ,
                              TRUE,"MAGLIMAnG", "what is the mag limit min of the guide star for cam AnG ?")) return 1;
  
  // for the name of the magnitude columns in the start catalogue , we expect 3 names , if they are not here we should quit
  // if the letters I B R are not here we must quit also
  //char str_colnames[STR_MAX];
//  if(ParLib_setup_param(pPL,"RES.OUTDIR",    PARLIB_ARGTYPE_STRING, 1,  pIpar->RES_OUTDIR   ,"./"                   ,
//                          TRUE,"OUTDIR", "Where should we put output files?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.BAND.MAG.COLNAME",   PARLIB_ARGTYPE_STRING, 1,  pIpar->MAGI_COLNAME , "mag_I"                   ,
                              TRUE,"MAGCOLNAMES", "what is the name of the mag (band I) column in the catalogue ?")) return 1;
  // limit magnitude for neighbour (cameras)
  //if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.NEIGHBOUR.DELTAMAG",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->DeltaMag_Neighbour , "1"                   ,
  //                              TRUE,"DeltaMagNeighbour", "what is the smaller delta  magnitude for neighbour for the camera?")) return 1;
  //if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.NEIGHBOUR.DISTMIN",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->DistMin_Neighbour , "1." ,
  //                              TRUE,"DistMinNeighbour", "what is the minimum distance to check magnitude of neighbour (arcsec) for the cameras")) return 1;
  
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.CAMERA.NEIGHBOUR.DELTAMAG",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->DeltaMag_CamNeighbour , "1"                   ,
                                TRUE,"DeltaMagNeighbour", "what is the smaller delta  magnitude for neighbour for the camera?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.CAMERA.NEIGHBOUR.DISTMIN",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->DistMin_CamNeighbour , "1." ,
                                TRUE,"DistMinNeighbour", "what is the minimum distance to check magnitude of neighbour (arcsec) for the cameras")) return 1;
  
  
  // man 230 April 2017: change the neighbour rejection rules camera and fibre don't use the sames.
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.FIBRE.NEIGHBOUR.DELTAMAG",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->DeltaMag_FibNeighbour , "1"                   ,
                                TRUE,"DeltaMagNeighbour", "what is the smaller delta  magnitude for neighbour for the fibre ?")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.FIBRE.NEIGHBOUR.DISTMIN",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->DistMin_FibNeighbour , "1." ,
                                TRUE,"DistMinNeighbour", "what is the minimum distance to check magnitude of neighbour (arcsec) for the fibre ")) return 1;
  if(ParLib_setup_param(pPL,"OPTIMISATION.STARCAT.FIBRE.NEIGHBOUR.MAGMAX",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->MagMax_FibNeighbour , "20.5" ,
                                TRUE,"DistMinNeighbour", "what is the maximum magnitude  for a star to be eligible for fibre")) return 1;
// section exclusion zone around brightstars 
//OPTIMISATION.BRIGHTSTAR.EXCLUSION.MAG=9.
  if(ParLib_setup_param(pPL,"OPTIMISATION.BRIGHTSTAR.EXCLUSION.MAG",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->Exclusion_MagMax , "6." ,
                                TRUE,"exclusionBrightStarsLim", "exclusion mag limit for very bright stars")) return 1;
//OPTIMISATION.BRIGHTSTAR.EXCLUSION.RADIUS.MIN
  if(ParLib_setup_param(pPL,"OPTIMISATION.BRIGHTSTAR.EXCLUSION.RADIUS.MIN",   PARLIB_ARGTYPE_FLOAT, 1,  &pIpar->Exclusion_Radius , "15." ,
                                TRUE,"exclusionBrightStarsRadius", "exclusion radius in Arc Minutes for very bright stars")) return 1;

// OPTIMISATION.STARCAT.FIBRE.NEIGHBOUR.DELTAMAG= 5.0
// OPTIMISATION.STARCAT.FIBRE.NEIGHBOUR.MAGMAX= 20.5
// OPTIMISATION.STARCAT.FIBRE.NEIGHBOUR.DISTMIN=5.  
  

  return 0;
}

//this does everything related to reading command line and input files
//return < 0 if error
//return = 0 if we should proceed as normal
//return = 1 if need to exit (without error)
int RST_input_params_handler ( RST_input_params_struct **ppIpar,
                              param_list_struct **ppParamList,
                              int argc, char** argv)
{
  //  int status;
  param_struct *pP = NULL; 
  if ( ppIpar == NULL || ppParamList == NULL ) return -1;
  
  //make space for and init the TS input params struct 
  if ( RST_input_params_struct_init ((RST_input_params_struct**) ppIpar)) return -1;
  /////////////////////////////////////////////////////////

  //make space for and init the param_list structure
  if ( ParLib_init_param_list((param_list_struct**) ppParamList))  return -1;

  //now add all the input parameters
  if ( RST_input_params_setup_ParamList ( (RST_input_params_struct*) *ppIpar,
                                         (param_list_struct*) *ppParamList,
                                         (int) argc, (char**) argv))
  {
    fprintf(stderr, "%s Problem setting up the input parameter list...\n", ERROR_PREFIX);
    fflush(stderr);
    return -1;
  }

  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "PARAM_FILENAME" )) == NULL)
  {
    fprintf(stderr, "%s You do not have PARAM_FILENAME added as a parameter!\n", ERROR_PREFIX);
    fflush(stderr);
    return -1;
  }
  if ( ParLib_read_param_from_command_line ((param_struct*) pP, (int) argc, (char**) argv))
  {
    //    fprintf(stderr, "%s Problem reading the input parameter file\n", ERROR_PREFIX);
  }
  if ( pP->supplied && strlen(pP->pCval))
  {
    //    status = 0;
    if (ParLib_read_params_from_file ((param_list_struct*) *ppParamList, (*ppIpar)->PARAM_FILENAME))
    {
      RST_input_params_print_usage_information(stderr);
      return -1;
    }
  }
  //  else status = 1;
  
  if ( ParLib_read_params_from_command_line ((param_list_struct*) *ppParamList, (int) argc, (char**) argv))
  {
    return -1;
  }

  //interpret any supplied command line switches here
  ////////////////////////////////////////////////////////
  //first see if we should print out the version information
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--VERSION" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( print_full_version_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the help file
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--help" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( print_full_help_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the useage info
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--usage" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( RST_input_params_print_usage_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the example input params file
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--example" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( ParLib_print_example_params_file((FILE*) stdout, (param_list_struct*) *ppParamList)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if the debug flag is set
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList,
                                                      (const char*) "--debug" )) == NULL)  return -1;
  if ( pP->supplied ) (*ppIpar)->debug = TRUE;
  else                (*ppIpar)->debug = FALSE;
  ////////////////////////////////////////////////////////


  //special check - if nothing is supplied on the command line
  if ( argc == 1 )
  {
    fprintf(stdout, "%s Printing help information because you didn't supply any input parameters:\n", COMMENT_PREFIX);
    if ( RST_input_params_print_usage_information((FILE*) stdout)) return -1;
    return -1;
  }
 
  if ( ParLib_check_param_list ((param_list_struct*) *ppParamList))
  {
    ParLib_print_param_list (stdout, (param_list_struct*) *ppParamList);
    return -1;
  }

  //  if ( (*ppIpar)->debug )
  ParLib_print_param_list (stdout, (param_list_struct*) *ppParamList);

  //seed the random number generator(s)
  if ( util_srand((*ppIpar)->SIM_RANDOM_SEED) == 0)
  {
    fprintf(stderr, "%s Problem seeding the random number generator\n", ERROR_PREFIX);
    return 1;
  }

  return 0;
}




///////////////////////////////////////////////////////////////////////////
////these funcs interpret the input values in a sensible way
/*
int RST_input_params_interpret_spectrograph (RST_input_params_struct *pIpar,
                                            spectrograph_struct    *pSpec )
{
  int i;
  if ( pIpar        == NULL ||
       pSpec        == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting spectrograph parameters\n", COMMENT_PREFIX); fflush(stdout);
  //  fprintf(stdout, "%s (at file %s line %d)\n", DEBUG_PREFIX, __FILE__, __LINE__); fflush(stdout); //DEBUG//

  //first we need to know how many arms our spectrograph has
  pSpec->num_arms = pIpar->SPECTRO_NUM_ARMS;
  if ( pSpec->num_arms > MAX_SPECTRO_ARMS ||
       pSpec->num_arms <= 0 )
  {
    fprintf(stderr, "%s Number of spectrograph arms (%d) exceeds the maximum (%d) or is < 1\n",
            ERROR_PREFIX, pSpec->num_arms , MAX_SPECTRO_ARMS);
    return 1;
  }
    

  
  //make some space for the arms
  if ((pSpec->pArm = (arm_struct*) malloc(pSpec->num_arms * sizeof(arm_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for (i=0; i<pSpec->num_arms; i++ )
  {
    arm_struct    *pArm    = (arm_struct*)    &(pSpec->pArm[i]);
    lambda_struct *pLambda = (lambda_struct*) &(pArm->Lambda);
    if ( arm_struct_init ( (arm_struct*) pArm ))  return 1;

    strncpy(pArm->str_name, pIpar->SPECTRO_ARM_CODENAME[i], STR_MAX);
    fprintf(stdout, "%s Interpreting spectrograph arm parameters: %s\n", COMMENT_PREFIX, pArm->str_name); fflush(stdout);
    pArm->aper_size      = (int)    pIpar->SPECTRO_ARM_APER_SIZE[i]; 
    pArm->spatial_fwhm   = (double) pIpar->SPECTRO_ARM_SPATIAL_FWHM[i]; 
    pArm->pixel_size     = (double) pIpar->SPECTRO_ARM_PIXEL_SIZE[i];
    pArm->read_noise     = (double) pIpar->SPECTRO_ARM_READ_NOISE[i];
    pArm->dark_current_h = (double) pIpar->SPECTRO_ARM_DARK_CURRENT[i];
    pArm->dark_current_s = (double) pArm->dark_current_h / 3600.;
    pArm->full_well      = (double) pIpar->SPECTRO_ARM_FULL_WELL[i];
    pArm->binning_disp   = (int)    pIpar->SPECTRO_ARM_BINNING_DISP[i];
    pArm->binning_cross  = (int)    pIpar->SPECTRO_ARM_BINNING_CROSS[i];
    pArm->effective_area = (double) pIpar->SPECTRO_EFFECTIVE_AREA; 
    pArm->fiber_diam     = (double) pIpar->SPECTRO_FIBER_DIAM; 
    pArm->fiber_area     = (double) M_PI * 0.25 * SQR(pArm->fiber_diam);   // fiber area = pi r^2
    pArm->skysub_residual= (double) pIpar->SPECTRO_SKYSUB_RESIDUAL;

    pArm->sq_read_noise = (double) pArm->aper_size*SQR(pArm->read_noise);  //TOD check this is a valid assumption

    
    strncpy(pLambda->str_type, pIpar->SPECTRO_ARM_LAMBDA_TYPE[i], STR_MAX);
    //determine the dispersion description type
    {
      const char *plist_names[]  = {"LINEAR",
                                    "DISPFILE",
                                    "FULLFILE"};
      const int   plist_values[] = {LAMBDA_TYPE_CODE_DISP_LINEAR,
                                    LAMBDA_TYPE_CODE_DISP_CURVE,
                                    LAMBDA_TYPE_CODE_LAMBDA_CURVE};
      int len = (int) (sizeof(plist_values)/sizeof(int));
      if (util_select_option_from_string(len, plist_names, plist_values,
                                         pIpar->SPECTRO_ARM_LAMBDA_TYPE[i],  &(pLambda->type)))
      {
        fprintf(stderr, "%s I do not understand this SPECTRO.ARM%d.LAMBDA.TYPE: >%s<\n",
                ERROR_PREFIX, (i+1), pIpar->SPECTRO_ARM_LAMBDA_TYPE[i]);fflush(stderr);
        return 1;
      }
    }
    
    pLambda->num_pix = (long)   pIpar->SPECTRO_ARM_LAMBDA_NUMPIX[i]; 
    pLambda->refpix  = (double) pIpar->SPECTRO_ARM_LAMBDA_REFPIX[i]; 
    pLambda->refval  = (double) pIpar->SPECTRO_ARM_LAMBDA_REFVAL[i]; 
    pLambda->disp    = (double) pIpar->SPECTRO_ARM_LAMBDA_DISP[i]; 
    strncpy(pLambda->crvFile.str_filename, pIpar->SPECTRO_ARM_LAMBDA_FILENAME[i], STR_MAX);

    if ( pLambda->type == LAMBDA_TYPE_CODE_DISP_CURVE   ||
         pLambda->type == LAMBDA_TYPE_CODE_LAMBDA_CURVE )       
    {

      if ( pLambda->type == LAMBDA_TYPE_CODE_DISP_CURVE  )
      {
        strncpy(pLambda->crvFile.str_colname_x, "LAMBDA", STR_MAX);
        strncpy(pLambda->crvFile.str_colname_y, "DISPERSION", STR_MAX);
      }
      else if ( pLambda->type == LAMBDA_TYPE_CODE_LAMBDA_CURVE )
      {
        strncpy(pLambda->crvFile.str_colname_x, "PIXEL", STR_MAX);
        strncpy(pLambda->crvFile.str_colname_y, "LAMBDA", STR_MAX);
      }
      //read the file from disk
      if ( curve_struct_read_from_file ( (curve_struct*) &(pLambda->crvFile)))
      {
        fprintf(stderr, "%s I had problems reading curve: %s\n",
                ERROR_PREFIX, pLambda->crvFile.str_filename);
        return 1;
      }
      if ( pLambda->type == LAMBDA_TYPE_CODE_LAMBDA_CURVE )       
      {
        pLambda->num_pix = (long) pLambda->crvFile.num_rows;
      }
    }

    //make some space for the lambda array
    if ((pLambda->pl  = (double*) malloc(pLambda->num_pix * sizeof(double))) == NULL ||
        (pLambda->pdl = (double*) malloc(pLambda->num_pix * sizeof(double))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }

    switch ( pLambda->type )
    {
      long j;
    case LAMBDA_TYPE_CODE_DISP_LINEAR :
      for(j=0;j<pLambda->num_pix;j++)
      {
        pLambda->pl[j]  = (double) pLambda->refval + ((double) j - pLambda->refpix) * pLambda->disp;
        pLambda->pdl[j] = (double) pLambda->disp;
      }
      break;

    case LAMBDA_TYPE_CODE_DISP_CURVE :
      {
        long refpix = (long) (0.5 + pLambda->refpix);  //TODO proper rounding?? Check this rounding is ok
        if ( refpix < 0 || refpix >= pLambda->num_pix )
        {
          fprintf(stderr, "%s Error - refpix value (=%g) is outside valid pixel range [%ld:%ld]\n",
                  ERROR_PREFIX, pLambda->refpix, (long) 0, (long) pLambda->num_pix);
          return 1;
        }
        //start at the reference pixel
        pLambda->pl[refpix]  = (double) pLambda->refval;
        //now work up to end of CCD
        j=refpix+1;
        while ( j<pLambda->num_pix )
        {
          long jmin,jmax;
          double disp, dl;
          //do a binary search to get the location in the dispersion file
          if ( util_binary_search_double ((long) pLambda->crvFile.num_rows,
                                          (double*) pLambda->crvFile.px,
                                          (double) pLambda->pl[j-1]/pLambda->crvFile.unit_x,
                                          (long*) &jmin, (long*) &jmax) < 0 ) return 1;
          disp = 0.5 * ( pLambda->crvFile.py[jmin] + pLambda->crvFile.py[jmax]); //TODO : proper interpolation
          //scale the dispersion units to internal standard of AA/um, then mult by pixsize (in um) to get AA/pix
          dl = disp * pLambda->crvFile.unit_y * (pArm->pixel_size);
          pLambda->pl[j] = (double) pLambda->pl[j-1] + dl;

          //          fprintf(stdout, "%s DEBUG1 ARM %s %6ld  [%4ld,%4ld,%12.5f,%12.5f,%12.5f,%12.5f,%12.5f] %8.5f %8.5f %12.5f\n",
          //                  COMMENT_PREFIX, pArm->str_name, j,
          //                  jmin,jmax,pLambda->crvFile.px[jmin],pLambda->crvFile.px[jmax],
          //                  pLambda->crvFile.py[jmin],pLambda->crvFile.py[jmax], pLambda->pl[j-1]/pLambda->crvFile.unit_x,
          //                  disp, dl, pLambda->pl[j]);

          j ++;
        }
        //now work down to beginning of CCD
        j=refpix-1;
        while ( j>=0 )
        {
          long jmin,jmax;
          double disp, dl;
          //do a binary search to get the location in the dispersion file
          if ( util_binary_search_double ((long) pLambda->crvFile.num_rows,
                                          (double*) pLambda->crvFile.px,
                                          (double) pLambda->pl[j+1]/pLambda->crvFile.unit_x,
                                          (long*) &jmin, (long*) &jmax) < 0 ) return 1;
          disp = 0.5 * ( pLambda->crvFile.py[jmin] + pLambda->crvFile.py[jmax]); //TODO : proper interpolation
          dl = disp * pLambda->crvFile.unit_y * (pArm->pixel_size);
          pLambda->pl[j] = (double) pLambda->pl[j+1] - dl;

//          fprintf(stdout, "%s DEBUG1 ARM %s %6ld  [%4ld,%4ld,%12.5f,%12.5f,%12.5f,%12.5f,%12.5f] %8.5f %8.5f %12.5f\n",
//                  COMMENT_PREFIX, pArm->str_name, j,
//                  jmin,jmax,pLambda->crvFile.px[jmin],pLambda->crvFile.px[jmax],
//                  pLambda->crvFile.py[jmin],pLambda->crvFile.py[jmax], pLambda->pl[j-1]/pLambda->crvFile.unit_x,
//                  disp, dl, pLambda->pl[j]);
          j --;
        }
        // now calc the dl for each pixel 
        pLambda->pdl[0] = pLambda->pl[1] - pLambda->pl[0];
        for(j=1;j<pLambda->num_pix;j++) pLambda->pdl[j] = pLambda->pl[j] - pLambda->pl[j-1];
        
        //        fprintf(stdout, "\n\n%s DEBUG0 ARM %s\n", COMMENT_PREFIX, pArm->str_name) ;
        //        for(j=0;j<pLambda->num_pix;j++)
        //        {
        //          fprintf(stdout, "%s DEBUG0 ARM %s %6ld %12.5f %12.5f\n", COMMENT_PREFIX, pArm->str_name, j, pLambda->pl[j],pLambda->pdl[j]);
        //        }

      }
      //      fprintf(stderr, "%s Error - I have not yet implemented this option (at file %s line %d)\n",
      //              ERROR_PREFIX, __FILE__, __LINE__);
      //      return 1;
      break;
      
    case LAMBDA_TYPE_CODE_LAMBDA_CURVE :
      for(j=0;j<pLambda->num_pix;j++)
      {
        pLambda->pl[j] = (double) pLambda->crvFile.py[j];
        if ( j >= 1 ) {pLambda->pdl[j] = pLambda->pl[j] - pLambda->pl[j-1];};
      }
      pLambda->pdl[0] = pLambda->pl[1] - pLambda->pl[0];
      break;
      
    default:
      return 1;
    }

    pLambda->min = (double) pLambda->pl[0];
    pLambda->max = (double) pLambda->pl[pLambda->num_pix-1];

    fprintf(stdout, "%s Spectrograph arm %s: wavelength range: [%8g:%8g]AA npixels=%ld\n",
            COMMENT_PREFIX,
            pArm->str_name,
            pLambda->min, pLambda->max,
            pLambda->num_pix); fflush(stdout);
    
    //read the throughput curve from file
    strncpy(pArm->crvTput.str_filename, pIpar->SPECTRO_ARM_TPUT_FILENAME[i], STR_MAX);
    strncpy(pArm->crvTput.str_colname_x, "LAMBDA", STR_MAX);
    strncpy(pArm->crvTput.str_colname_y, "TRANSMISSION", STR_MAX);
    fprintf(stdout, "%s Reading spectrograph arm throughput file: %s\n",
            COMMENT_PREFIX, pArm->crvTput.str_filename); fflush(stdout);
    //read the file from disk
    if ( curve_struct_read_from_file ( (curve_struct*) &(pArm->crvTput)))
    {
      fprintf(stderr, "%s I had problems reading throughput curve: %s\n",
              ERROR_PREFIX, pArm->crvTput.str_filename);
      return 1;
    }

    
    //read the resolution curve from file
    strncpy(pArm->crvRes.str_filename, pIpar->SPECTRO_ARM_RES_FILENAME[i], STR_MAX);
    strncpy(pArm->crvRes.str_colname_x, "LAMBDA", STR_MAX);
    strncpy(pArm->crvRes.str_colname_y, "RESOLUTION", STR_MAX);
    fprintf(stdout, "%s Reading spectrograph arm resolution file: %s\n",
            COMMENT_PREFIX, pArm->crvRes.str_filename); fflush(stdout);
    //read the file from disk
    if ( curve_struct_read_from_file ( (curve_struct*) &(pArm->crvRes)))
    {
      fprintf(stderr, "%s I had problems reading resolution curve: %s\n",
              ERROR_PREFIX, pArm->crvRes.str_filename);
      return 1;
    }

  }

  
  return 0;
}*/



/*int RST_input_params_interpret_templatelist (RST_input_params_struct *pIpar,
                                            templatelist_struct    *pTL )
{
  if ( pIpar   == NULL ||
       pTL     == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting template list parameters\n", COMMENT_PREFIX); fflush(stdout);
  
  strncpy(pTL->str_filename, pIpar->TEMPLATES_FILENAME, STR_MAX);

  return 0;
}*/

int RST_input_params_interpret_fieldlist (RST_input_params_struct *pIpar,
                                            fieldlist_struct    *pTL )
{
  if ( pIpar   == NULL ||
       pTL     == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting template list parameters\n", COMMENT_PREFIX); fflush(stdout);

  strncpy(pTL->str_filename, pIpar->FIELDS_FILENAME, STR_MAX);
  strncpy(pTL->str_outdir, pIpar->RES_OUTDIR, STR_MAX);

  return 0;
}
int  RST_input_params_interpret_focal_plane(RST_input_params_struct* pIpar,
                                            focalplane_struct*    pFocPlan)
{
  if ( pIpar   == NULL || pFocPlan     == NULL ) return 1;

  fprintf(stdout, "%s Interpreting focal plane parameter file\n", COMMENT_PREFIX); fflush(stdout);
  strncpy(pFocPlan->str_filename, pIpar->FOCALPLANE_FILENAME, STR_MAX);
  
  // must add the use of the magnitude range (min max ) per camera or fibre 
  fprintf(stdout, "%s Interpreting min maglimfibre\n", COMMENT_PREFIX); fflush(stdout);
  pFocPlan->MagLimMin_Fibre= pIpar->MAGLIMMinFIBRE;
  fprintf(stdout, "%s Interpreting min maglim wfs\n", COMMENT_PREFIX); fflush(stdout);
  pFocPlan->MagLimMin_CamWFS= pIpar->MAGLIMMinWFS;
  fprintf(stdout, "%s Interpreting min maglim ang\n", COMMENT_PREFIX); fflush(stdout);
  pFocPlan->MagLimMin_CamAnG= pIpar->MAGLIMMinAnG;
  
  fprintf(stdout, "%s Interpreting max maglimfibre\n", COMMENT_PREFIX); fflush(stdout);
  pFocPlan->MagLimMax_Fibre= pIpar->MAGLIMMaxFIBRE;
  fprintf(stdout, "%s Interpreting max maglim wfs\n", COMMENT_PREFIX); fflush(stdout);
  pFocPlan->MagLimMax_CamWFS= pIpar->MAGLIMMaxWFS;
  fprintf(stdout, "%s Interpreting max maglim ang\n", COMMENT_PREFIX); fflush(stdout);
  pFocPlan->MagLimMax_CamAnG= pIpar->MAGLIMMaxAnG;
  
  // must be updated with selective regection criteria (april 17)
  // camera 
  pFocPlan->DeltaMag_CamNeighbour=pIpar->DeltaMag_CamNeighbour;
  pFocPlan->DistMin_CamNeighbour= pIpar->DistMin_CamNeighbour;
  // fibre 
  pFocPlan->DeltaMag_FibNeighbour=pIpar->DeltaMag_FibNeighbour;
  pFocPlan->DistMin_FibNeighbour= pIpar->DistMin_FibNeighbour;
  pFocPlan->MagMax_FibNeighbour= pIpar->MagMax_FibNeighbour;
  // need new fields in the pfocplan structure to store exclusion radius and magnitude 
  pFocPlan->VeryBrightStar_exclusion_Radius= pIpar->Exclusion_Radius;	// radius of exclusion around very bragtstars , in minutes
  pFocPlan->VeryBrightStar_exclusion_Mag =pIpar->Exclusion_MagMax;		// magnitud for the very bright star exclusion
  return 0;
}



int RST_input_params_interpret_outputfiles(RST_input_params_struct* pIpar,
		resultsfiles_struct*    pResFile)
{
if ( pIpar   == NULL ||
		pResFile     == NULL ) return 1;
//
fprintf(stdout, "%s Interpreting output files parameters \n", COMMENT_PREFIX); fflush(stdout);

strncpy(pResFile->str_fitsfilename, pIpar->RES_OUTFITSFILENAME, STR_MAX);
strncpy(pResFile->str_failurefitsfilename, pIpar->RES_FAILUREFITSFILENAME, STR_MAX);
strncpy(pResFile->str_outdir, pIpar->RES_OUTDIR, STR_MAX);

return 0;
}

int  RST_input_params_interpret_star_catalog(RST_input_params_struct* pIpar,
		starcat_struct *    pStarCat)
{
  if ( pIpar   == NULL ||
		  pStarCat     == NULL ) return 1;
  //
  fprintf(stdout, "%s Interpreting star catalogue parameter file\n", COMMENT_PREFIX); fflush(stdout);

  strncpy(pStarCat->str_starcat_filename, pIpar->STARCAT_FILENAME, STR_MAX);
  strncpy(pStarCat->str_healpix_index_filename, pIpar->HEALPIX_INDEX, STR_MAX);
  strncpy(pStarCat->str_magI_colname, pIpar->MAGI_COLNAME, STR_MAX);
  return 0;
}


int    RST_input_params_interpret_everything (RST_input_params_struct *pIpar,
                                             fieldlist_struct    *pTL,
                                             focalplane_struct		*pFocPl,
                                             starcat_struct *   pStarCat,
                                             resultsfiles_struct* PResFiles
                                             )
{
  if ( pIpar        == NULL ||
       pTL          == NULL ||
       pFocPl  == NULL ||
       PResFiles== NULL
       ) return 1;

  if ( RST_input_params_interpret_focal_plane ((RST_input_params_struct*) pIpar,
                                                (focalplane_struct*)    pFocPl)) return 1;
  if ( RST_input_params_interpret_fieldlist ((RST_input_params_struct*) pIpar,
                                                (fieldlist_struct*)    pTL)) return 1;
  if ( RST_input_params_interpret_star_catalog((RST_input_params_struct*) pIpar,
												(starcat_struct *)   pStarCat)) return 1;

  if ( RST_input_params_interpret_outputfiles ((RST_input_params_struct*) pIpar,
                                                 (resultsfiles_struct*)    PResFiles)) return 1;

  return 0;
}
/*
int RST_input_params_interpret_conditions (RST_input_params_struct *pIpar,
                                          condlist_struct      *pCondList )
{
  int i,nsub;
  if ( pIpar == NULL ||
       pCondList == NULL ) return 1;

  if ( pIpar->pParamList == NULL ) return 1;
  
  //
  fprintf(stdout, "%s Interpreting observing conditions parameters\n", COMMENT_PREFIX); fflush(stdout);

  strncpy(pCondList->str_interp_method, pIpar->OBS_PARAMS_INTERP_METHOD, STR_MAX);
  //determine the interpolation method
  {
    const char *plist_names[]  = {"NEAREST",
                                  "LINEAR",
                                  "SPLINE"};
    const int   plist_values[] = {INTERP_METHOD_NEAREST,
                                  INTERP_METHOD_LINEAR,
                                  INTERP_METHOD_SPLINE};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pCondList->str_interp_method,  &(pCondList->interp_method)))
    {
      fprintf(stderr, "%s I do not understand this OBS_PARAMS.INTERP_METHOD: >%s<\n",
                ERROR_PREFIX, pCondList->str_interp_method);fflush(stderr);
      return 1;
    }
  }



  //determine the fibercoupling type

  {
    const char *plist_names[]  = {"NONE",
                                  "FIXED",
                                  "SEEING",
                                  "MATRIX", "FILE"};
    const int   plist_values[] = {FIBERCOUPLING_TYPE_CODE_NONE,
                                  FIBERCOUPLING_TYPE_CODE_FIXED,
                                  FIBERCOUPLING_TYPE_CODE_SEEING,
                                  FIBERCOUPLING_TYPE_CODE_MATRIX, FIBERCOUPLING_TYPE_CODE_MATRIX};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pIpar->FIBERCOUPLING_TYPE, &(pCondList->Fibercoupling.type_code)))
    {
      fprintf(stderr, "%s I do not understand this FIBERCOUPLING.TYPE: >%s<\n",
              ERROR_PREFIX, pIpar->FIBERCOUPLING_TYPE);fflush(stderr);
      return 1;
    }
  }
  
  pCondList->Fibercoupling.frac_sky    = (double) pIpar->FIBERCOUPLING_FRAC_SKY; 
  if ( pCondList->Fibercoupling.type_code == FIBERCOUPLING_TYPE_CODE_FIXED )
    pCondList->Fibercoupling.frac_science = (double) pIpar->FIBERCOUPLING_FRAC_SCIENCE; 
  else
    pCondList->Fibercoupling.frac_science = (double) 1.0;
  
  //determine the seeing profile type
  {
    const char *plist_names[]  = {"GAUSSIAN", "GAUSS",
                                  "MOFFAT"};
    const int   plist_values[] = {SEEING_PROFILE_CODE_GAUSSIAN,SEEING_PROFILE_CODE_GAUSSIAN,
                                  SEEING_PROFILE_CODE_MOFFAT};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pIpar->FIBERCOUPLING_SEEING_PROFILE, &(pCondList->Fibercoupling.seeing_profile_code)))
    {
      fprintf(stderr, "%s I do not understand this FIBERCOUPLING.SEEING_PROFILE: >%s<\n",
              ERROR_PREFIX, pIpar->FIBERCOUPLING_SEEING_PROFILE);fflush(stderr);
      return 1;
    }
  }

  if ( pCondList->Fibercoupling.type_code == FIBERCOUPLING_TYPE_CODE_MATRIX )
  {
    //take a copy of the filename which contains the fibercoupling matrix
    //then attempt to read in the fibercoupling matrix
    strncpy(pCondList->Fibercoupling.str_filename, pIpar->FIBERCOUPLING_FILENAME, STR_MAX);
    if ( fibercoupling_struct_read_from_file ( (fibercoupling_struct*) &(pCondList->Fibercoupling)))
    {
      fprintf(stderr, "%s I had problems reading the fiber coupling matrix file: %s\n",
              ERROR_PREFIX, pIpar->FIBERCOUPLING_FILENAME);
      return 1;
    }
  }


  
  pCondList->num_airmass   = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.AIRMASS");
  pCondList->num_IQ        = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.IQ");
  pCondList->num_skybright = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.SKYBRIGHT");
  pCondList->num_tilt      = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.TILT");
  pCondList->num_misalign  = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.MISALIGN");


  pCondList->num_exp       = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.TEXP");
  //check that number of nsub is same length as texp
  nsub = ParLib_get_dim_supplied ((param_list_struct*) pIpar->pParamList, (const char*) "OBS_PARAMS.NSUB");
  if ( nsub !=  pCondList->num_exp )
  {
    fprintf(stderr, "%s Mismatched dimensions for OBS_PARAMS.TEXP(%d) and OBS_PARAMS.NSUB(%d) parameters\n",
            ERROR_PREFIX, pCondList->num_exp, nsub);
    return 1;
  }

  if ( pCondList->num_airmass <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of airmass values to sample: %d\n", ERROR_PREFIX, pCondList->num_airmass);
    return 1;
  }
  if ( pCondList->num_IQ <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of IQ values to sample: %d\n", ERROR_PREFIX, pCondList->num_IQ);
    return 1;
  }
  if ( pCondList->num_skybright <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of sky brightness values to sample: %d\n", ERROR_PREFIX, pCondList->num_skybright);
    return 1;
  }
  if ( pCondList->num_tilt <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of spine tilt values to sample: %d\n", ERROR_PREFIX, pCondList->num_tilt);
    return 1;
  }
  if ( pCondList->num_misalign <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of spine misalignment values to sample: %d\n", ERROR_PREFIX, pCondList->num_misalign);
    return 1;
  }
  if ( pCondList->num_exp <= 0 ) 
  {
    fprintf(stderr, "%s Invalid number of exposure times/sub exposures to sample: %d\n", ERROR_PREFIX, pCondList->num_exp);
    return 1;
  }

  
 //make some space 
  if ((pCondList->pairmass   = (double*) malloc(pCondList->num_airmass   * sizeof(double))) == NULL ||
      (pCondList->pIQ        = (double*) malloc(pCondList->num_IQ        * sizeof(double))) == NULL ||
      (pCondList->pskybright = (double*) malloc(pCondList->num_skybright * sizeof(double))) == NULL ||
      (pCondList->ptilt      = (double*) malloc(pCondList->num_tilt      * sizeof(double))) == NULL ||
      (pCondList->pmisalign  = (double*) malloc(pCondList->num_misalign  * sizeof(double))) == NULL ||
      (pCondList->ptexp      = (double*) malloc(pCondList->num_exp       * sizeof(double))) == NULL ||
      (pCondList->pnsub      = (int*)    malloc(pCondList->num_exp       * sizeof(int)))    == NULL )
  {
    fprintf(stderr, "%s Error assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //copy the arrays
  //  for(i=0;i<pCondList->num_airmass;  i++) pCondList->pairmass[i]   = (double) pIpar->pOBS_PARAMS_AIRMASS[i];
  //  for(i=0;i<pCondList->num_IQ;       i++) pCondList->pIQ[i]        = (double) pIpar->pOBS_PARAMS_IQ[i];
  //  for(i=0;i<pCondList->num_skybright;i++) pCondList->pskybright[i] = (double) pIpar->pOBS_PARAMS_SKYBRIGHT[i];
  //  for(i=0;i<pCondList->num_tilt;     i++) pCondList->ptilt[i]      = (double) pIpar->pOBS_PARAMS_TILT[i];
  //  for(i=0;i<pCondList->num_misalign; i++) pCondList->pmisalign[i]  = (double) pIpar->pOBS_PARAMS_MISALIGN[i];
  //  for(i=0;i<pCondList->num_exp;      i++)
  //  {
  //    pCondList->ptexp[i] = (double) pIpar->pOBS_PARAMS_TEXP[i];
  //    pCondList->pnsub[i] = (int)    pIpar->pOBS_PARAMS_NSUB[i];
  //  }

////  //and make sure that the values are sorted
//  util_dsort  ((unsigned long) pCondList->num_airmass,   (double*) pCondList->pairmass);
//  util_dsort  ((unsigned long) pCondList->num_IQ,        (double*) pCondList->pIQ);
//  util_dsort  ((unsigned long) pCondList->num_skybright, (double*) pCondList->pskybright);
//  util_dsort  ((unsigned long) pCondList->num_tilt,      (double*) pCondList->ptilt);
//  util_dsort  ((unsigned long) pCondList->num_misalign,  (double*) pCondList->pmisalign);
//  util_disort ((unsigned long) pCondList->num_texp,      (double*) pCondList->ptexp, (int*) pCondList->pnsub);

  // No, better to avoid sorting (allow the user to specifiy their own order)
  // What we actually want is the min and max of each parameter
  for(i=0;i<pCondList->num_airmass;  i++)
  {
    pCondList->pairmass[i]   = (double) pIpar->pOBS_PARAMS_AIRMASS[i];
    if ( pCondList->pairmass[i] > pCondList->airmass_max )  pCondList->airmass_max = pCondList->pairmass[i];
    if ( pCondList->pairmass[i] < pCondList->airmass_min )  pCondList->airmass_min = pCondList->pairmass[i];
  }
  for(i=0;i<pCondList->num_IQ;  i++)
  {
    pCondList->pIQ[i]   = (double) pIpar->pOBS_PARAMS_IQ[i];
    if ( pCondList->pIQ[i] > pCondList->IQ_max )  pCondList->IQ_max = pCondList->pIQ[i];
    if ( pCondList->pIQ[i] < pCondList->IQ_min )  pCondList->IQ_min = pCondList->pIQ[i];
  }
  for(i=0;i<pCondList->num_skybright;  i++)
  {
    pCondList->pskybright[i]   = (double) pIpar->pOBS_PARAMS_SKYBRIGHT[i];
    if ( pCondList->pskybright[i] > pCondList->skybright_max )  pCondList->skybright_max = pCondList->pskybright[i];
    if ( pCondList->pskybright[i] < pCondList->skybright_min )  pCondList->skybright_min = pCondList->pskybright[i];
  }
  for(i=0;i<pCondList->num_tilt;  i++)
  {
    pCondList->ptilt[i]   = (double) pIpar->pOBS_PARAMS_TILT[i];
    if ( pCondList->ptilt[i] > pCondList->tilt_max )  pCondList->tilt_max = pCondList->ptilt[i];
    if ( pCondList->ptilt[i] < pCondList->tilt_min )  pCondList->tilt_min = pCondList->ptilt[i];
  }
  for(i=0;i<pCondList->num_misalign;  i++)
  {
    pCondList->pmisalign[i]   = (double) pIpar->pOBS_PARAMS_MISALIGN[i];
    if ( pCondList->pmisalign[i] > pCondList->misalign_max )  pCondList->misalign_max = pCondList->pmisalign[i];
    if ( pCondList->pmisalign[i] < pCondList->misalign_min )  pCondList->misalign_min = pCondList->pmisalign[i];
  }
  for(i=0;i<pCondList->num_exp;  i++)
  {
    pCondList->ptexp[i]   = (double) pIpar->pOBS_PARAMS_TEXP[i];
    pCondList->pnsub[i]   = (int)    pIpar->pOBS_PARAMS_NSUB[i];
    if ( pCondList->ptexp[i] > pCondList->texp_max )  pCondList->texp_max = pCondList->ptexp[i];
    if ( pCondList->ptexp[i] < pCondList->texp_min )  pCondList->texp_min = pCondList->ptexp[i];
    if ( pCondList->pnsub[i] > pCondList->nsub_max )  pCondList->nsub_max = pCondList->pnsub[i];
    if ( pCondList->pnsub[i] < pCondList->nsub_min )  pCondList->nsub_min = pCondList->pnsub[i];
  }


  
  return 0;
}
*/

/*
int RST_input_params_interpret_rules (RST_input_params_struct *pIpar,
                                     rulelist_struct        *pRuleList,
                                     rulesetlist_struct     *pRuleSetList)

{
  if ( pIpar        == NULL ||
       pRuleList    == NULL ||
       pRuleSetList == NULL) return 1;
  //
  fprintf(stdout, "%s Interpreting spectral success rules parameters\n", COMMENT_PREFIX); fflush(stdout);

  strncpy(pRuleList->str_filename, pIpar->RULELIST_FILENAME, STR_MAX);
  strncpy(pRuleSetList->str_filename, pIpar->RULESETLIST_FILENAME, STR_MAX);

  return 0;
}
*/


//int RST_input_params_interpret_sky (RST_input_params_struct *pIpar,
//                                   sky_struct   *pSky )
//{
//  if ( pIpar   == NULL ||
//       pSky    == NULL ) return 1;
//  //
//  fprintf(stdout, "%s Interpreting sky parameters\n", COMMENT_PREFIX); fflush(stdout);
//
//  strncpy(pSky->Trans.str_filename, pIpar->SKY_TRANSMISSION_FILENAME, STR_MAX);
//  strncpy(pSky->Emiss.str_filename, pIpar->SKY_EMISSION_FILENAME,     STR_MAX);
//
//  return 0;
//}
//


/// was part of the function RST_input_params_setup_ParamList
  /*if(ParLib_setup_param(pPL,"OPTIMISATION.TILE.FILENAME",   PARLIB_ARGTYPE_STRING,1,pIpar->RULELIST_FILENAME   ,"rulelist.txt"   ,
                        FALSE, "RULELIST",    "Name of file containing the list of spectral success rules")) return 1;*/
  /*if(ParLib_setup_param(pPL,"OPTIMISATION.TILE.TILING_METHOD",PARLIB_ARGTYPE_STRING,1,pIpar->RULESETLIST_FILENAME,"rulesetlist.txt",
                        FALSE, "RULESETLIST", "Name of file containing the list of spectral success rulesets")) return 1;*/

  //int ParLib_setup_param (param_list_struct *pParamList, const char *name, int argtype, int max_dim,
      //                    void *pValue, const char *default_value, BOOL compulsory,  const char *name_alt, const char *comment)

  /*if(ParLib_setup_param(pPL,"SIM.CODE_NAME", PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_CODE_NAME,"testrun_v0.1"         ,
                        FALSE,"CODE_NAME","Human readable codename for this run of the 4FS_TS")) return 1;
  if(ParLib_setup_param(pPL,"SIM.MODE",      PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_MODE     ,"CALC_SNR"             ,
                        FALSE,"MODE",   "Should we calculate SNR from given TEXP, or TEXP/MAG from given SNR? (CALC_SNR,CALC_TEXP,CALC_MAG)")) return 1;

  if(ParLib_setup_param(pPL,"SIM.OUTPUT",    PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_OUTPUT   ,"SUMMARY,SPECTRA_SNR"  ,
                        FALSE,"OUTPUT","Which output types to produce? (ADD LIST OF OPTIONS HERE)")) return 1;
  if(ParLib_setup_param(pPL,"SIM.SPECFORMAT",PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_SPECFORMAT,"TABLE"              ,
                        FALSE,"SPECFORMAT","Which output spectral formats should be produced? (IMAGE,TABLE,RESAMPLED)")) return 1;
                                           */
  /*
  if(ParLib_setup_param(pPL,"SIM.CLOBBER",   PARLIB_ARGTYPE_BOOL,   1,  &pIpar->SIM_CLOBBER  ,"no"                  ,
                        FALSE,"clobber", "Run in clobber mode? (existing output files will be overwritten)")) return 1;

*/
  /*if(ParLib_setup_param(pPL,"TEMPLATES.FILENAME",  PARLIB_ARGTYPE_STRING,1,pIpar->TEMPLATES_FILENAME  ,"templates.txt"  ,
                        TRUE,  "TEMPLATES",   "Name of file containing the list of spectral templates")) return 1;
  if(ParLib_setup_param(pPL,"RULELIST.FILENAME",   PARLIB_ARGTYPE_STRING,1,pIpar->RULELIST_FILENAME   ,"rulelist.txt"   ,
                        FALSE, "RULELIST",    "Name of file containing the list of spectral success rules")) return 1;
  if(ParLib_setup_param(pPL,"RULESETLIST.FILENAME",PARLIB_ARGTYPE_STRING,1,pIpar->RULESETLIST_FILENAME,"rulesetlist.txt",
                        FALSE, "RULESETLIST", "Name of file containing the list of spectral success rulesets")) return 1;*/

  // this controls how we calculate the science fiber input losses
  // NONE   - no losses (assume that already in Thoughput curves)
  // FIXED  - fixed multiplier giving fraction of object light that is transmitted
  // SEEING - Fraction of light tranmitted is calculated from the delivered seeing, the object profile and the fiber diameter
  // MATRIX - fraction of light lost is calculated from a lookup table giving the dependence on airmass,zenith seeing,tilt angle and wavelength.
/*  if(ParLib_setup_param(pPL,"FIBERCOUPLING.TYPE",PARLIB_ARGTYPE_STRING, 1,  pIpar->FIBERCOUPLING_TYPE,"NONE",
                        FALSE, "FIBERCOUPLING.MODE", "Method by which fiber losses are calculated (NONE,FIXED,SEEING,MATRIX)")) return 1;
  if(ParLib_setup_param(pPL,"FIBERCOUPLING.FILENAME", PARLIB_ARGTYPE_STRING,1,pIpar->FIBERCOUPLING_FILENAME  ,"fibercoupling.fits"  ,
                        FALSE,  "FIBERCOUPLING_FILENAME",   "Name of file containing the matrix of fibercoupling values")) return 1;
  if(ParLib_setup_param(pPL,"FIBERCOUPLING.FRAC_SCIENCE",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->FIBERCOUPLING_FRAC_SCIENCE,"1.0" ,
                        FALSE, "" ,"Fraction of science light transmitted down fiber (FIBERCOUPLING.TYPE=FIXED)")) return 1;
  if(ParLib_setup_param(pPL,"FIBERCOUPLING.FRAC_SKY",    PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->FIBERCOUPLING_FRAC_SKY    ,"1.0" ,
                        FALSE, "" ,"Fraction of sky transmitted down fiber (FIBERCOUPLING.TYPE=any)")) return 1;
  if(ParLib_setup_param(pPL,"FIBERCOUPLING.SEEING_PROFILE",PARLIB_ARGTYPE_STRING, 1,  pIpar->FIBERCOUPLING_SEEING_PROFILE,"GAUSSIAN",
                        FALSE, "", "Functional form of seeing profile (GAUSSIAN, MOFFAT)")) return 1;*/
  /// .... TODO add more seeing params
  /*
  //fiber size - in arcsec
  if(ParLib_setup_param(pPL,"SPECTRO.FIBER_DIAM",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_FIBER_DIAM  ,"1.5" ,
                        FALSE, "" ,"Fiber diameter (arcsec)")) return 1;
  //
  if(ParLib_setup_param(pPL,"SPECTRO.EFFECTIVE_AREA",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_EFFECTIVE_AREA,"1.0" ,
                        FALSE, "" ,"Effective collecting area of telescope (m^2)")) return 1;
  if(ParLib_setup_param(pPL,"SPECTRO.SKYSUB_RESIDUAL",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_SKYSUB_RESIDUAL,"0.0" ,
                        FALSE, "" ,"Fractional uncertaintity on sky subtraction")) return 1;

  //-----------------resampling params---------------------//
  //TODO add units options for x-axis, y-axis??
  if(ParLib_setup_param(pPL,"RESAMPLE.MODE", PARLIB_ARGTYPE_STRING, 1,  pIpar->SIM_RESAMPLE_MODE,  "LINEAR_LAMBDA",
                        FALSE, "RESAMPLE_MODE", "Method by which resampled spectra are binned (LINEAR_LAMBDA,LOG_LAMBDA,FREQ...)")) return 1;
  if(ParLib_setup_param(pPL,"RESAMPLE.MIN",  PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SIM_RESAMPLE_MIN,   "3000.0" ,
                        FALSE, "RESAMPLE_MIN" ,"Minimum wavelength/frequency coordinate at centre of 1st pixel of resampled output")) return 1;
  if(ParLib_setup_param(pPL,"RESAMPLE.DELTA",PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SIM_RESAMPLE_DELTA, "10.0" ,
                        FALSE, "RESAMPLE_DELTA" ,"Pixel width in wavelength/frequency/log(wavelength)/log(freq) in resampled output")) return 1;
  if(ParLib_setup_param(pPL,"RESAMPLE.NUM_PIX",PARLIB_ARGTYPE_LONG,  1, &pIpar->SIM_RESAMPLE_NUM_PIX,"700" ,
                        FALSE, "RESAMPLE_NUM_PIX" ,"Number of pixels in resampled output")) return 1;
  //-----------------resampling params---------------------//

*/
  ///////////////////////////////////////////////////////
  //special case - accept a variable number of spectrograph arms
/*
  {
    int a;
    param_struct *pParam = NULL;

    if(ParLib_setup_param(pPL,"SPECTRO.NUM_ARMS" ,PARLIB_ARGTYPE_INT,  1, &pIpar->SPECTRO_NUM_ARMS ,"1" ,
                          TRUE, "NUM_ARMS" ,"Number of spectrograph arms")) return 1;
    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL,
                                                           (const char*) "SPECTRO.NUM_ARMS")) == NULL) return 1;

    //check to see if the "SPECTRO.NUM_ARMS" is supplied in the "PARAM_FILENAME=xx" file
    if ( strlen(pIpar->PARAM_FILENAME) > 0 )
    {
      if(ParLib_read_param_from_file ((param_struct*)pParam, (char*) pIpar->PARAM_FILENAME ) > 0)
      {
        fprintf(stderr, "%s Problem reading from input params file: >%s<\n", ERROR_PREFIX, pIpar->PARAM_FILENAME);
        return 1;
      }
    }
    //check to see if the "SPECTRO.NUM_ARMS" is supplied on the command line
    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;

    if ( pIpar->SPECTRO_NUM_ARMS > MAX_SPECTRO_ARMS )
    {
      fprintf(stderr, "%s Too many spectrograph arms: %d (>%d)\n", ERROR_PREFIX, pIpar->SPECTRO_NUM_ARMS, MAX_SPECTRO_ARMS);
      return 1;
    }

    for (a = 0; a<pIpar->SPECTRO_NUM_ARMS; a++)
    {
      char str_par_name[STR_MAX];
      char str_arm_code[STR_MAX];
      //add the standard params for each spectrograph
      snprintf(str_arm_code, STR_MAX,"SPECTRO.ARM%d",a+1);

      //arm name
      snprintf(str_par_name, STR_MAX, "%s.CODENAME",str_arm_code );
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SPECTRO_ARM_CODENAME[a] ,"-"            ,
                            FALSE, "" ,"Codename for spectrograph arm")) return 1;

      //resolution filename
      snprintf(str_par_name, STR_MAX, "%s.RES_FILENAME", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SPECTRO_ARM_RES_FILENAME[a]  ,"-"       ,
                            TRUE, "" ,"Filename describing spectral resolution")) return 1;

      //throughput filename
      snprintf(str_par_name, STR_MAX, "%s.TPUT_FILENAME", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING, 1, pIpar->SPECTRO_ARM_TPUT_FILENAME[a] ,"-"       ,
                            TRUE, "" ,"Filename describing spectral throughput")) return 1;

      //number of pixels to add up per object in x-dispersion direction
      snprintf(str_par_name, STR_MAX, "%s.APER_SIZE", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT, 1, &pIpar->SPECTRO_ARM_APER_SIZE[a]        ,"3"       ,
                            FALSE, "" ,"Size of software aperture in the cross-dispersion direction (pix)")) return 1;

      //FWHM of spectrum in spatial direction, in um
      snprintf(str_par_name, STR_MAX, "%s.SPATIAL_FWHM", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_SPATIAL_FWHM[a]  ,"30.0"   ,
                            FALSE, "" ,"FWHM of spectra in the cross-dispersion direction (um)")) return 1;

      //pixel size in um
      snprintf(str_par_name, STR_MAX, "%s.PIXEL_SIZE", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_PIXEL_SIZE[a]    ,"15"     ,
                            FALSE, "" ,"Pixel size (um, in the dispersion direction)")) return 1;

      //readnoise (e-/pix)
      snprintf(str_par_name, STR_MAX, "%s.READ_NOISE", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_READ_NOISE[a]    ,"2.0"    ,
                            FALSE, "" ,"CCD read noise (e-/pix)")) return 1;

      //dark current (e-/hr/pix)
      snprintf(str_par_name, STR_MAX, "%s.DARK_CURRENT", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_DARK_CURRENT[a]  ,"3.0"   ,
                            FALSE, "" ,"CCD dark current (e-/hr/pix)")) return 1;

//      //ADC gain (e-/ADU)
//      snprintf(str_par_name, STR_MAX, "%s.GAIN", str_arm_code);
//      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_GAIN[a]          ,"1.0"   ,
//      FALSE, "" ,"CCD readout gain (e-/ADU)")) return 1;

      //Full well
      snprintf(str_par_name, STR_MAX, "%s.FULL_WELL", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE, 1, &pIpar->SPECTRO_ARM_FULL_WELL[a]     ,"1.5e5" ,
                            FALSE, "" ,"Full well capacity of the CCD (e-/pix)")) return 1;

      //on chip binning
      snprintf(str_par_name, STR_MAX, "%s.BINNING.DISP", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT, 1, &pIpar->SPECTRO_ARM_BINNING_DISP[a]     ,"1"   ,
                            FALSE, "" ,"On-chip binning in dispersion direction")) return 1;
      snprintf(str_par_name, STR_MAX, "%s.BINNING.CROSS", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT, 1, &pIpar->SPECTRO_ARM_BINNING_CROSS[a]     ,"1"   ,
                            FALSE, "" ,"On-chip binning in cross-dispersion direction")) return 1;


      //dispersion type
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.TYPE",str_arm_code );
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING,1, pIpar->SPECTRO_ARM_LAMBDA_TYPE[a]    ,"LINEAR",
                            FALSE,"","Type of dispersion description, LINEAR, from DISPFILE, or FULLFILE")) return 1;
      //dispersion filename
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.FILENAME", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_STRING,1, pIpar->SPECTRO_ARM_LAMBDA_FILENAME[a],"-"     ,
                            TRUE ,"","Filename describing wavlength solution (dispersion scale or full lambda solution)")) return 1;
      //number of pixels on ccd in dispersion direction
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.NUMPIX", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_INT   ,1,&pIpar->SPECTRO_ARM_LAMBDA_NUMPIX[a]  ,"4096"  ,
                            FALSE,"","Number of CCD pixels in the dispersion direction")) return 1;
      //reference pixel coord
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.REFPIX", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE,1,&pIpar->SPECTRO_ARM_LAMBDA_REFPIX[a]  ,"2048.0",
                            FALSE,"","Coordinate of the reference pixel")) return 1;
      //wavelength of ref pixel, in AA
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.REFVAL", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE,1,&pIpar->SPECTRO_ARM_LAMBDA_REFVAL[a]  ,"400.0" ,
                            FALSE,"","Wavelength at the reference pixel (AA)")) return 1;
      //dispersion pixel scale, in AA/pix
      snprintf(str_par_name, STR_MAX, "%s.LAMBDA.DISP", str_arm_code);
      if(ParLib_setup_param(pPL,str_par_name,PARLIB_ARGTYPE_DOUBLE,1,&pIpar->SPECTRO_ARM_LAMBDA_DISP[a]    ,"1.0"   ,
                            FALSE,"","Linear dispersion scale (AA/pix)")) return 1;


    }
  }
*/


/*
  if(ParLib_setup_param(pPL,"SKY.TRANSMISSION.FILENAME",PARLIB_ARGTYPE_STRING, 1, pIpar->SKY_TRANSMISSION_FILENAME ,"sky_trans.fits" ,
                        TRUE, "SKY_TRANS", "Name of file containing the sky transmission info")) return 1;
  if(ParLib_setup_param(pPL,"SKY.EMISSION.FILENAME"    ,PARLIB_ARGTYPE_STRING, 1, pIpar->SKY_EMISSION_FILENAME     ,"sky_emiss.fits" ,
                        TRUE, "SKY_EMISS", "Name of file containing the sky emission info")) return 1;


  //observational parameters to simulate
  if(ParLib_setup_param(pPL,"OBS_PARAMS.INTERP_METHOD",PARLIB_ARGTYPE_STRING, 1, pIpar->OBS_PARAMS_INTERP_METHOD         ,"LINEAR",
                        FALSE, "OBS_PARAMS_INTERP", "Type of interpolation to use, NEAREST,LINEAR,SPLINE")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.AIRMASS",      PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_AIRMASS  ,   "1.0"  ,
                        FALSE, "" ,"List of airmasses to simulate")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.IQ",           PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_IQ       ,   "1.0"  ,
                        FALSE, "" ,"List of delivered image quality values to simulate (V-band,FWHM,arcsec)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.SKYBRIGHT",    PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_SKYBRIGHT   ,"22.0" ,
                        FALSE, "" ,"List of zenithal sky brightness to simulate (V-band,ABmag/arcsec2)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.TILT",         PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_TILT        ,"0.0"  ,
                        FALSE, "" ,"List of tilts to simulate (mm)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.MISALIGNMENT", PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_MISALIGN    ,"0.0"  ,
                        FALSE, "" ,"List of fiber->target misalignments to simulate (arcsec)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.TEXP",         PARLIB_ARGTYPE_DOUBLE, MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_TEXP        ,"1200.",
                        FALSE, "" ,"List of total exposure times to simulate (sec)")) return 1;
  if(ParLib_setup_param(pPL,"OBS_PARAMS.NSUB",         PARLIB_ARGTYPE_INT,    MAX_OBS_PARAMS, &pIpar->pOBS_PARAMS_NSUB        ,"1"    ,
                        FALSE, "" ,"List of numbers of sub-exposures to simulate")) return 1;


*/

