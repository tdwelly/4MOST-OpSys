#------------------------------------
# This is the makefile for 4FS_RST
# CVS_REVISION "$Revision: 1.2 $"
# CVS_DATE     "$Date: 2015/10/06 10:25:18 $"
#------------------------------------


include ../arch.make

#the following allows you to have an include file 
#that lives outside the CVS tree:
#include ~/code/arch.make

# If for some reason you need to override the settings 
# in these arch.make files then do so below: 
#OPTIMISE = -O2
OPTIMISE = -O2
#DEBUG =
#BINARY_DIR = 
#ARCH =
#CFLAGS = $(ARCH) -Wall -Wno-parentheses -Wl,--no-as-needed $(OPTIMISE) $(DEBUG)
#CXX = gcc

COMMON_DIR = ../common


LDIR = -L./ -L$(HEALPIX_PATH)/lib -L$(CFITSIO_PATH)/lib -L$(COMMON_DIR)/

IDIR = -I./ -I$(HEALPIX_PATH)/include -I$(CFITSIO_PATH)/include -I$(COMMON_DIR)/


#for paralel processing  if activated the OFILE section must be modified also : add this flag to the cxx line
#OMP_FLAGS = -fopenmp

LIBS =  -lm -lchealpix -lcfitsio $(OMP_FLAGS) $(IDIR) $(LDIR)
#-lchealpix 
OUTSTEM = 4FS_RST
OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))
OUTFILE = $(OUTSTEM)

DEP_LIBS = $(COMMON_DIR)/utensils_lib $(COMMON_DIR)/params_lib $(COMMON_DIR)/quadtree_lib $(COMMON_DIR)/geometry_lib $(COMMON_DIR)/fits_helper_lib $(COMMON_DIR)/hpx_extras_lib RST_input_params_lib

DEP_OFILES  = $(OFILE) $(addsuffix .o,$(DEP_LIBS)) 

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) $(COMMON_DIR)/define.h

DEP_CFILES  = $(CFILE) $(addsuffix .c,$(DEP_LIBS)) 


$(OUTFILE)	:	$(DEP_OFILES)
	$(CXX) $(CFLAGS) $(LIBS) $(IDIR) $(LDIR) -o $(OUTFILE) $(DEP_OFILES)

$(OFILE)	:	$(DEP_HFILES) $(DEP_CFILES)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE) 

#define the rules to make the library files
%_lib.o	:	%_lib.[ch]
	make -f $*_lib.make

install   : 
	cp -pv $(OUTFILE) $(BINARY_DIR)

clean   : 
	rm -f $(OFILE) $(OUTFILE)

clean_all   : 
	rm -f $(OFILES) $(OUTFILE) $(DEP_OFILES)

#end

