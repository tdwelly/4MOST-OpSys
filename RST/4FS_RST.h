//----------------------------------------------------------------------------------
//--
//--    Filename: 4FS_RST.h
//--    Use: This is a header for the 4FS_RST.c C code file
//--
//--    Notes:
//--   Author A Gueguen  2016

#ifndef FOURFS_RST_H
#define FOURFS_RST_H

#define CVS_REVISION_4FS_RST_H "$Revision: $"
#define CVS_DATE_4FS_RST_H     "$Date:  $"

//standard header files - need this for FILE* pointers
#include <stdio.h>

//my header files which include structure definitions come next
#include "define.h"
#include "utensils_lib.h"
#include "params_lib.h"
#include "fits_helper_lib.h"
#include "geometry_lib.h"

////////////////////////////////////
#define STR_MAX        DEF_STRLEN
#define LONG_STR_MAX   DEF_LONGSTRLEN
#define LSTR_MAX       DEF_LONGSTRLEN

#define RST_BRANDING_STRING      "4MOST Tiling Optimisation tool- RST"
#define RST_BRANDING_PLOTSTRING_GRAPH  \
  "set label 9999 \"4MOST Tiling Optimisation tool - RST\" at graph  0.98,0.02 font \"Times-Roman,14\" tc lt 3 right"
#define RST_BRANDING_PLOTSTRING_SCREEN \
  "set label 9999 \"4MOST Tiling Optimisation tool- RST\" at screen 0.98,0.02 font \"Times-Roman,14\" tc lt 3 right"
#define RST_BRANDING_PLOTSTRING_UNSET  \
  "unset label 9999"
#define RST_FITS_CREATOR_KEYWORD "4FS_RST version xxx"
#define CONTACT_EMAIL_ADDRESS "agueguen@mpe.mpg.de"

#define MAX_THREADS_LIMIT              8   // We don't want to go crazy and squash the system
                                           // Multi-thread handling is not really implemented yet

#define VERSION_INFO_FILENAME               "4FS_RST_version_info.txt"
#define INTERPRETED_PARAMETER_FILENAME      "4FS_RST_interpreted_parameters.txt"

#define CATALOGUE_FORMAT_ASCII     1 //tbd
#define CATALOGUE_FORMAT_FITS      2
#define N2S(x) ((x)==1?"":"s")

//#define FILE_FORMAT_UNKNOWN 0
//#define FILE_FORMAT_ASCII   1
//#define FILE_FORMAT_FITS    2

// alain : struct for RST
typedef struct{
	 char str_fieldname[STR_MAX];     //templatelist identifier
	 long index;
	 char str_RA[STR_MAX];
	 char str_DEC[STR_MAX];
	 double RA;
	 double RA_tolerance;
	 double DEC;
	 double DEC_tolerance;
	 double PA;   // phase angle
	 double PA_tolerance;

}field_struct;

typedef struct {
  char str_name[STR_MAX];     //templatelist identifier
  char str_filename[STR_MAX];  // file from which this list of FoV was read
  char str_outdir[STR_MAX];
  int num_fields;
  long array_length;  //size of assigned array
  field_struct *pField;
  
} fieldlist_struct;

//this describes the focal plane
typedef struct {
  char str_name[STR_MAX]; //identifier of the camera
  char str_type[STR_MAX]; //type of the camera
  int index;           //index of the camera
  double Xcen, Ycen;
  double Xcorner[4], Ycorner[4];
  double Radius_pix;
  double Radius_deg;
} camera_struct;

typedef struct {
	  char str_name[STR_MAX]; //identifier of the fibre spine
	  //char str_type[STR_MAX]; //type of the camera
	  int index;           //index of the fibre spine
	  double Xcen, Ycen;
	  double Radius_mm;
	  //double Radius_pix;
	  double Radius_deg;
} fibre_struct;

typedef struct {
  char str_name[STR_MAX]; 		//Fov identifier in the focal plan
  char str_filename[STR_MAX];	// file from which this focal plane description was read
  int num_tot_Fovs;			    //Total number of FoV
  int num_cameraWFS;			//number of camera Wave Front Sensor
  int num_cameraAnG;			// number of camera AnG
  int num_fibreguide;			//number of fibre guide (spine)
  camera_struct *pCaM;			//pointer to array of camera structs  (
  fibre_struct *pFibre;			//pointer to array of fibre structs
  
  //derived params
  //float MagLim_Fibre;			// the mag limit to use for the fibres
  //float MagLim_CamWFS;			// the mag limit to use for the WFS cameras
  //float MagLim_CamAnG;			// the mag limit to use for the A n G  cameras
  
  //------ new magnitude range for the star selection 
  float MagLimMax_Fibre;			// the mag limit to use for the fibres
  float MagLimMax_CamWFS;			// the mag limit to use for the WFS cameras
  float MagLimMax_CamAnG;			// the mag limit to use for the A n G  cameras
  
  float MagLimMin_Fibre;			// the mag limit to use for the fibres
  float MagLimMin_CamWFS;			// the mag limit to use for the WFS cameras
  float MagLimMin_CamAnG;			// the mag limit to use for the A n G  cameras
  
  //------ 
  //float DistMin_Neighbour;		// minimum distance for the nearest neighbour
  //float DeltaMag_Neighbour;		// minimum diff in magnitude for the nearest neighbour
  // man 230 april 17 4
  float DistMin_CamNeighbour;		// minimum distance for the nearest neighbour camera
  float DeltaMag_CamNeighbour;		// minimum diff in magnitude for the nearest neighbour camera
  float DistMin_FibNeighbour;		// minimum distance for the nearest neighbour (fibre guide )
  float DeltaMag_FibNeighbour;		// minimum diff in magnitude for the nearest neighbour (fibre guide )
  float MagMax_FibNeighbour;		// minimum diff in magnitude for the nearest neighbour (fibre guide )
  float VeryBrightStar_exclusion_Radius;	// radius of exclusion around very bragtstars , in minutes
  float VeryBrightStar_exclusion_Mag;		// magnitud for the very bright star exclusion
  
} focalplane_struct;

// this struct store the properties of a given FoV on the sky (Fov name and stars present in the region of the star)
typedef struct {
	int fieldID;
	int FovID; // the index the Fov
	// focal plane pos of the FoV and the corner ...
	double XPOScenterFov;
	double YPOScenterFov;
	// sky coordinate pos of the FoV and the corner ...
	double RAcenterFov;
	double DECcenterFov;
	// coordinates x y and RA DEC of the corners of the cameras 
	double X_corner[4];
	double Y_corner[4];
	double RA_corner[4];
	double DEC_corner[4];
	double XYradius;  //patrol radius of the fiber
	
	char str_FovTYPE[STR_MAX];
	double *RAobj;
	double *DECobj;
	float *MAGBobj;
	float *MAGRobj;
	float *MAGIobj;
	int *ClassBobj;
	int *ClassRobj;
	int *ClassIobj;
	long *hpxPIXobj;
	int NbtotStore;//dim of the arrays
	int NbValidStore;
	BOOL ValidityFlag;
	BOOL VeryBrightStarValidityFlag;
}  lstobjectsInFoV_struct;

typedef struct {
	int PointingID;     // the ids of the pointing;
	double RAPointing;  // their RA  coordinates
	double DECPointing; // their DEC coordinates
	double PhaseAngle; // their phase angle
	char pointingname[STR_MAX];
	lstobjectsInFoV_struct FOVs[18];
	//we should add the  x y coordinates of the FoV and the RA DEC here => 4 array
	
	BOOL pointingStatus;
}  pointingStarsPerFovs_struct;

typedef struct {
  char str_starcat_filename[STR_MAX];
  char str_healpix_index_filename[STR_MAX];
  char str_magI_colname[STR_MAX];
  int format_index_code;
  int format_StarCat_code;
}  starcat_struct;

typedef struct {
	int arr_fovid[18];
	char arr_fovtype[18][STR_MAX] ;
}  couple_fovID_FovTYpe;

/// structure used for sorting by increasing magnitude the stars in a fov
typedef struct {
	int fovID;
	char fovtype[STR_MAX] ;
	double RAfov;
	double DECfov;
	// section to store the position of the FoVs on the focal plane, its radius (for fibre) and its corner (for camera)
        double Xcoord_FOV[5];
        double Ycoord_FOV[5];
	double RAcoord_FOV[5];
        double DECcoord_FOV[5];
	double RAstar;
	double DECstar;
	//we should store the position of the stars in the focalplane in X Y coordinates
	float MAGBobj;
	float MAGRobj;
	float MAGIobj;
}  guidestars_struct;

typedef struct {
//"FIELD_INDEX",
	long fieldIndex;
//"RA_CENTRE",
	double RAfield;
//"DEC_CENTRE",
	double DECfield;
//"FOV_INDEX",
	int FovId;
//"RA_FOV" ,
	double RAFov;
//"DEC_FOV" ,
	double DECFov;
//"RA[]"  ,
	double * RaStars;
//"DEC[]" ,
	double * DecStars;
//"MAG_B[]" ,
	double * MagB;
//"MAG_R[]" ,
	double * MagR;
//"MAG_I[]" ,
	double * MagI;
//"NSTARS" ,
	long NstarsFov;
//"FOV_STATUS",
	long StausFov;
//"FIELD_STATUS"
	long StatusField;

}  result_Field;

typedef struct {
  char str_fitsfilename[STR_MAX];  // file from which this list of FoV was read
  char str_failurefitsfilename[STR_MAX];  // file from which this list of FoV was read
  char str_outdir[STR_MAX];
  //field_struct *pField;

} resultsfiles_struct;

///////////////////////////////////////////////////////////////////////////////////
///Here comes a list of function prototypes
///////////////////////////////////////////////////////////////////////////////////

int print_version_and_inputs                 (FILE* pstream, int argc, char **argv);
int _4FS_RST_print_version                    (FILE *pFile);
int print_full_version_information_to_file   (const char* str_filename);
int print_full_version_information           (FILE *pFile);
int print_full_help_information              (FILE *pFile);
int write_summary_page_html                  (param_list_struct *pParamList, const char* str_filename);

//////////////////////////// function in RST  by alain

/// 4FS_RST structures function

//structure initilisation and space freeing functions
int cplfovIdType_struct_init ( couple_fovID_FovTYpe **pcplIT );
int fieldlist_struct_init		(fieldlist_struct	**ppFieldList);
int fieldlist_struct_free		(fieldlist_struct	**ppFieldList);
int fieldlist_struct_read_from_file	(fieldlist_struct 	*pTL );
int fieldlist_struct_read_from_file	(fieldlist_struct 	*pTL );
// focal plane description
//int focalplane_struct_read_from_file	(focalplane_struct *pFocPl);//, couple_fovID_FovTYpe * local_cplFibTyp) ;
int focalplane_struct_read_from_file( focalplane_struct *pFocPl , couple_fovID_FovTYpe * local_cplFibTyp);
int focalplane_struct_init		(focalplane_struct    **ppFocal);
int focalplane_struct_free		(focalplane_struct    **ppFocal);
int starcat_struct_init 		(starcat_struct 	 **pStarCat  );
int starcat_struct_free 		(starcat_struct **pStCt );
int field_struct_free 			(field_struct *pFld );

int read_healpix_index_fits(char * str_indexfitsfilename,fitsfile *fptr_index, long * nbligne, long ** array_healpixnumber,long ** array_nbrows);
int check_fits_cat(starcat_struct * pStarCatfptr_cat);

int fibre_struct_init ( fibre_struct *pFibre );
int camera_WfsAg_struct_init ( camera_struct *pCamera );
int resultsfiles_struct_init( resultsfiles_struct **pRfil );
int resultsfiles_struct_free ( resultsfiles_struct **presFile );

int ReadnCheckSurvey (fieldlist_struct *pFldLst, fitsfile * fptr_starcat,focalplane_struct *pFocpl, starcat_struct *   pStarCat,resultsfiles_struct* PResFiles, couple_fovID_FovTYpe *cplFibTyp,long **array_firstrowhealpixnumber, long **array_ofnbrows	/*,couple_fovID_FovTYpe * pCplFibTyp*/);

int access_fits_catalogue(char * str_catalogue,fitsfile * paramfptrStarCat,  long **listhpxpix, long **firstrowtoread, long ** nbrowstoread,
		long  nbhpxpixels ,lstobjectsInFoV_struct *Fovstars,int paramFovID,char *str_colunmname);

int access_fits_Very_brightstars_catalogue(char * str_catalogue, fitsfile * paramfptrStarCat, long * * listhpxpix, long * * firstrowtoread, long * * nbrowstoread,
		long nbhpxpixels, lstobjectsInFoV_struct * Fovstars, int paramFovID, char * str_colunmname, float radius, float VeryBrightstarmagnitude);
/* //lstobjectsInFoV_struct * Fovstars, // list of stars in the fov .. useless in this version  */ 
					 
int read_starcat_for_pointing(//field_struct *pField,
		fitsfile*  fptr_starcat,
		starcat_struct *   pStarCat,
		pointingStarsPerFovs_struct *currentPointing,
		focalplane_struct *pFocpl,
		long **array_firstrowhealpixnumber,
		long **array_ofnbrows ,
		double phaseangle,
		int FieldIndex);

int getStrFovType(int fovId,char * result);
// used for the qsort function
//int compare(guidestars_struct *elem1, guidestars_struct *elem2);
int compare( const void *elem1, const void *elem2);

int work_on_fieldlist ( fieldlist_struct *pFldLst,
		focalplane_struct *pFocpl,
		starcat_struct *   pStarCat,
		 couple_fovID_FovTYpe *cplFibTyp,
		fitsfile * fptr_index ,
		fitsfile * fptr_cat ,
		 resultsfiles_struct* PResFiles);

int readPointingCoordinates( fieldlist_struct *pFldLst, focalplane_struct *pFocpl, starcat_struct *   pStarCat,
		 couple_fovID_FovTYpe *cplFibTyp,
		fitsfile * fptr_index ,fitsfile * fptr_cat,
		resultsfiles_struct* PResFiles);
int interpret_unit_string(char* str_unit, int* punit_type, double* punit);
//*********************************

int write_summary_to_file_ascii (FILE* pFile, fieldlist_struct* pfieldLst/*, condlist_struct* pCondList, simspec_struct* pSimSpec*/);
int write_summary_to_file_fits (fieldlist_struct* pSim);
int write_detailed_summary_to_file_fits_header (fitsfile ** ppOutputFile,char * str_OutputFilename);

int  write_comments_inHeaderFitsFiles(fitsfile *fptr_outputresult,
		fitsfile *fptr_outputfailure,fieldlist_struct *pFldLst,
		starcat_struct *pStarCat,focalplane_struct *pFocpl);

int write_detailed_summary_to_file_fits_data (fitsfile * pOutputFile,guidestars_struct * arr_guidestars,
		/* couple_fovID_FovTYpe * localcplFibType,*/
		 int pointingid, char * fieldname,
		 float RApointing ,float DECpointing ,
		 float PhaseAngleInit,
		 float PhaseAngleUpdated,
		 pointingStarsPerFovs_struct * currentPointingMetadata,
		 BOOL *statusFov,
		 char **arrayoffovtype,
		 BOOL FieldStatus,
		 BOOL RotationStatus,
		 int nstars);

// second file : only failure

int write_failure_summary_to_file_fits_one_data (fitsfile * pOutputFile,
		// int pointingid,float RApointing ,float DECpointing ,
		 int pointingid,char *fieldname,float RApointing ,float DECpointing ,

		 BOOL fibrestatus,
		 BOOL oppositefibrestatus,
		 BOOL camerastatus,
		 BOOL wfsstatus,
		 BOOL angstatus
		 );

int write_summarized_failure_results_to_file_fits_header (fitsfile ** ppOutputFile ,char * str_OutputFilename);
int write_comment_in_fits_header (fitsfile ** ppOutputFile ,char * str_comment);

int cplfovIdType_struct_init ( couple_fovID_FovTYpe **pcplIT );


#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
