#----------------------------------------
# This is a helper makefile for 4FS software
#CVS_REVISION "$Revision: 1.11 $"
#CVS_DATE     "$Date: 2015/10/26 12:38:50 $"
#----------------------------------------

#Determine the architecture of the host system
kernel := $(shell uname)

# only support two options so far (for OSX and GNU/Linux)
ifeq ($(kernel),Darwin)
  # specify the path to cfitsio and healpix libraries and headers (later we will append include/ and lib/ to these paths)
  CFITSIO_PATH = $(HOME)/software/osx
  HEALPIX_PATH = $(HOME)/software/osx
  hostarch := $(shell arch)
  ARCH = -arch $(hostarch)
  NO_AS_NEEDED = 
  #This is the compiler to use - make sure this is _real_ gcc (not the Apple LLVM that is aliased as gcc on OSX) 
#  CXX = gcc-5
  CXX = gcc
  # openmp is not part of clang/LLVM by default, so avoid using it on OSX
  OMP_FLAGS = 
else ifeq ($(kernel),Linux)
  # specify the path to cfitsio and healpix libraries and headers (later we will append include/ and lib/ to these paths)
  CFITSIO_PATH = $(HOME)/software/linux
  HEALPIX_PATH = $(HOME)/software/linux
  ARCH = 
  NO_AS_NEEDED = -Wl,--no-as-needed
  #This is the compiler to use
  CXX = gcc
#  OMP_FLAGS =
  OMP_FLAGS = -fopenmp
else
  .PHONY	:	print_vars
print_vars	:	
	@echo "Error *** I do not understand this kernel type:" $(kernel)
endif



# Note, the BINARY_DIR parameter sets the path to the 
# directory where the executable will be installed
# You might want to check that the BINARY_DIR is 
# in your shell's PATH
BINARY_DIR   = $(HOME)/4most/code/bin



# The following are parameters controlling build options 
# You almost certainly want to leave these as they are
#DEBUG = -g -pg
DEBUG = -g3
#OPTIMISE = -O2 
OPTIMISE = -O2 
#OPTIMISE = -O3
#OPTIMISE =  


# switch to choose whether to use the adress sanitizer option
# turning this on has a big performance impact for some operations
SANITIZE = 
#SANITIZE = true
ifeq "$(SANITIZE)" "true" 
  # test if the gcc version is > 4.8
  GCCVERSION_GE_4p8 := $(shell expr `$(CXX) -dumpversion | awk '{if($$1>=4.8){print 1}else{print 0}}'`)
  ifeq "$(GCCVERSION_GE_4p8)" "1"
	DEBUG +=  -fno-omit-frame-pointer -fsanitize=address
  endif
endif



# see http://stackoverflow.com/questions/1704907/how-can-i-get-my-c-code-to-automatically-print-out-its-git-version-hash

# Get the git version string (this will be accessible in code as the macro GIT_VERSION
GIT_VERSION := $(shell git describe --abbrev=8 --dirty --always --tags)
#GIT_DATE := $(firstword $(shell git --no-pager show --date=short --format="%ad" --name-only))
GIT_DATE := $(shell git --no-pager show --date=iso8601 --name-only --format="%ad" | head -n 1)

# the following seems to mess up the make process so commenting out
# # recompile version.h dependants when GIT_VERSION changes, uses temporary file version~
# .PHONY: force
# git_version_date~: force
# 	@echo '$(GIT_VERSION) $(GIT_DATE)' | cmp -s - $@ || echo '$(GIT_VERSION) $(GIT_DATE)' > $@
# git_version_date.h: git_version_date~
# 	@touch $@
# 	@echo Git version $(GIT_VERSION) $(GIT_DATE)


###########################################################
# the flags to pass to the compiler
CFLAGS = $(ARCH) -std=gnu99 -pedantic -Wall -Wno-parentheses $(NO_AS_NEEDED) $(OPTIMISE) $(DEBUG) -DGIT_VERSION=\"$(GIT_VERSION)\" -DGIT_DATE="\"$(GIT_DATE)\""



#end

#test
