#!/bin/tcsh -f

# check if stilts exists, if not then try to install it in the given path

set FORCE = "FALSE"
if ( $#argv == 1 ) then
  set BINARY_DIR = "$argv[1]"
else if ( $#argv == 2 ) then
  set BINARY_DIR = "$argv[1]"
  set FORCE      = "$argv[2]"
else
  echo "Error"
  exit 1
endif

if ( -X stilts && "$FORCE" != "TRUE" ) then
  echo "You already have stilts in your path, so skipping install"
  echo "Here it is: `which stilts`"
  exit 0
endif


echo "Installing stilts here: $BINARY_DIR"
mkdir -p $BINARY_DIR
if ( ! -d $BINARY_DIR ) then
  echo "Error, can't create $BINARY_DIR"
  exit 1
endif

cd $BINARY_DIR

wget -q http://www.star.bris.ac.uk/~mbt/stilts/stilts.jar 
wget -q http://www.star.bris.ac.uk/~mbt/stilts/stilts

if ( ! -e stilts || ! -e stilts.jar ) then
  echo "Error, can't create $BINARY_DIR"
  exit 1
endif

# make sure that the script is executable
chmod a+x stilts

#echo "Here it is: `which stilts`"

echo "Done"


exit
