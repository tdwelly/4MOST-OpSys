//----------------------------------------------------------------------------------
//--
//--    Filename: geometry_lib.h
//--    Description:
//--    Use: This module contains a library of functions used to carry out simple geometric tests
//--
//--
//--
#ifndef GEOMETRY_LIB_H
#define GEOMETRY_LIB_H

//#define CVS_REVISION_GEOMETRY_LIB_H "$Revision: 1.4 $"
//#define CVS_DATE_GEOMETRY_LIB_H     "$Date: 2015/08/18 11:42:08 $"

#include <stdio.h>
//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.4 $"
#define CVS_DATE_H     "$Date: 2015/08/18 11:42:08 $"
inline static int geom_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//


#define GEOM_COORDINATE_GRID_FILENAME_EQUATORIAL "J2000_coord_grid_in_J2000.txt"
#define GEOM_COORDINATE_GRID_FILENAME_GALACTIC   "galactic_coord_grid_in_J2000.txt"
#define GEOM_NOTABLE_LOCATION_FILENAME           "notable_locations.txt"

#define GEOM_MAP_PROJECTION_RECTANGULAR       0    //
#define GEOM_MAP_PROJECTION_SINUSOIDAL        1    //properly known as SANSON-FLAMSTEED
#define GEOM_MAP_PROJECTION_AITOFF            2
#define GEOM_MAP_PROJECTION_HAMMER_AITOFF     3
#define GEOM_MAP_PROJECTION_PARABOLIC         4


#define GEOM_COORD_SMC_RA0    13.1866         //J2000 coords of the centres of the Magellanic Clouds
#define GEOM_COORD_SMC_DEC0  -72.8286         //
#define GEOM_COORD_LMC_RA0    80.8939         //
#define GEOM_COORD_LMC_DEC0  -69.7561         //

//#define GEOM_COORD_GC_RA0   266.4168262       // Galactic centre - specifically SgrA*
//#define GEOM_COORD_GC_DEC0  -29.0077969
#define GEOM_COORD_GC_RA0   266.4050            // Galactic centre - specifically 0.0,0.0 in Gal coords
#define GEOM_COORD_GC_DEC0  -28.9362
#define GEOM_COORD_GAC_RA0    86.4050            // Galactic anti-centre - specifically 180.0,0.0 in Gal coords
#define GEOM_COORD_GAC_DEC0  +28.9362

#define GEOM_COORD_SEP_RA0   90.0             // South Ecliptic Pole 
#define GEOM_COORD_SEP_DEC0  -66.56071

#define GEOM_SHAPE_NONE      0
#define GEOM_SHAPE_CIRCLE    1
#define GEOM_SHAPE_TRIANGLE  3
#define GEOM_SHAPE_SQUARE    4
#define GEOM_SHAPE_PENTAGON  5
#define GEOM_SHAPE_HEXAGON   6
#define GEOM_SHAPE_OCTAGON   8
//etc etc



//typedef struct {
//  double x1;
//  double x2;
//  double y1;
//  double y2;
//} lineseg_struct;


//function prototypes
int    geom_print_version                          (FILE* pFile);
double geom_dot_product2D                          (double dx1,double dy1,  double dx2,double dy2) ;
double geom_cross_product2D                        (double dx1,double dy1,  double dx2,double dy2) ;

double geom_sqdist_to_line                         (double x0, double y0, double x1, double y1, double x2, double y2);
double geom_dist_to_line                           (double x0, double y0, double x1, double y1, double x2, double y2);
double geom_sqdist_to_line_seg                     (double x0, double y0, double x1, double y1, double x2, double y2);
double geom_dist_to_line_seg                       (double x0, double y0, double x1, double y1, double x2, double y2);
double geom_sqdist_line_seg_line_seg               (double xa1, double ya1, double xb1, double yb1, double xa2, double ya2, double xb2, double yb2);

double geom_dist_line_seg_line_seg                 (double xa1, double ya1, double xb1, double yb1, double xa2, double ya2, double xb2, double yb2);

int    geom_test_if_near_line_seg                  (double x0, double y0, double x1, double y1, double x2, double y2, double d);
int    geom_test_if_near_line                      (double x0, double y0, double x1, double y1, double x2, double y2, double d);
int    geom_test_if_intersect_line_seg_line_seg    (double xa1, double ya1, double xb1, double yb1, double xa2, double ya2, double xb2, double yb2);

int    geom_rotate_about_axis                      (double x0, double y0, double pa, double x, double y, double *pxx, double *pyy );
int    geom_fast_rotate_about_axis                 (double x0, double y0, double sin_pa, double cos_pa, double x, double y, double *pxx, double *pyy );

int    geomf_rotate_about_axis                     (float x0, float y0, float pa, float x, float y, float *pxx, float *pyy );
int    geomf_fast_rotate_about_axis                (float x0, float y0, float sin_pa, float cos_pa, float x, float y, float *pxx, float *pyy );

int    geom_test_if_in_hexagon                     (double outer_radius, double x, double y);
int    geom_test_if_near_hexagon_perimeter         (double outer_radius, double x, double y, double d );
int    geom_test_if_in_long_range                  (double a0, double a_min, double a_max);
int    geom_test_if_in_long_lat_range              (double a0, double d0, double a_min, double a_max, double d_min, double d_max);


double geom_angsep                                 (double ra1, double dec1, double ra2, double dec2);
double geom_arc_dist                               (double ra1, double dec1, double ra2, double dec2);
double geom_arc_dist_no_check                      (double ra1, double dec1, double ra2, double dec2);
double geom_fast_arc_dist                          (double ra1, double ra2,  double sin_dec1, double cos_dec1, double sin_dec2, double cos_dec2);
double geom_very_fast_cos_arc_dist                 (double ra1, double ra2,  double sin_dec1, double cos_dec1, double sin_dec2, double cos_dec2);
double geom_calc_bearing                           (double ra1, double dec1, double ra2, double dec2);
double geom_fast_calc_bearing                      (double ra1, double dec1, double ra2, double dec2); 
int    geom_ra_dec_to_gal_l_b                      (double ra, double dec, double *pl, double *pb) ;
int    geom_gal_l_b_to_ra_dec                      (double l, double b,  double *pra, double *pdec);

void   geom_radec2unitsquare                       (double ra_min,  double ra_max,  double dec_min,  double dec_max, double ra, double dec, double *pux, double *puy );
void   geom_unitsquare2radec                       (double ra_min,  double ra_max,  double dec_min,  double dec_max, double ux, double uy, double *pra, double *pdec );

int    geom_project_inverse_gnomonic_from_arcsec   (double ra0, double dec0, double pa0, double x, double y, double *pra, double *pdec );
int    geom_project_inverse_gnomonic_from_mm       (double ra0, double dec0, double pa0, double x, double y, double inv_platescale, double *pra, double *pdec );
int    geom_project_gnomonic_to_arcsec             (double ra0, double dec0, double pa0, double ra, double dec, double *px, double *py );
int    geom_project_gnomonic_to_mm                 (double ra0, double dec0, double pa0, double ra, double dec, double platescale, double *px, double *py );
int    geom_find_intersection_of_circles_on_plane  (double x1, double y1, double r1, double x2, double y2, double r2, double *px1, double *py1, double *px2, double *py2);




int geom_collision_line_seg_line_seg(double xa1, double ya1, double xb1, double yb1, double xa2, double ya2, double xb2, double yb2, double d);
int geom_collision_line_seg_circle(double x1, double y1, double x2, double y2, double x0, double y0, double r0);


int geom_calc_midpoint (double ra1, double dec1, double ra2, double dec2, double *pramid, double *pdecmid);

int geom_interpolate_long_lat (double t1, double long1,   double lat1, double t2, double long2,   double lat2, double t0, double *plong0, double *plat0);


int geom_ra_dec_to_unit_vectors ( double ra, double dec, double *px, double *py,  double *pz);
int geom_unit_vectors_to_ra_dec ( double x,  double y,   double z,   double *pra, double *pdec);
int geom3d_rotate_about_axes_fast (double x, double y, double z, double sin_a, double cos_a, double sin_b, double cos_b, double sin_c, double cos_c, double *px, double *py, double *pz);
int geom3d_rotate_about_axes (double x, double y, double z, double a, double b, double c, double *px, double *py, double *pz );
int geom3d_rotate_ra_dec_about_axes (double ra, double dec, double a, double b, double c, double *pra, double *pdec);

int geom_calc_area_of_polygon (int npts, double *px, double *py, double *parea); 

int  geom_write_coordinate_grid_file_galactic    (const char *str_filename);
int  geom_write_coordinate_grid_file_equatorial  (const char *str_filename);
int  geom_write_coordinate_grid_files            (const char *str_dirname );
int  geom_write_notable_location_file            (const char *str_filename);
int  geom_write_long_lat_tics                    (FILE *pPlotfile,  const char *str_coord_label_colour);
int  geom_write_mini_compass                     (FILE *pPlotfile, BOOL left_is_east);

int  geom_write_gnuplot_map_projection_functions (FILE *pPlotfile, const char *str_griddirname, int map_projection,
                                                  BOOL left_is_east, double dec_min, double dec_max, int label_num,
                                                  const char *str_coord_label_colour);

int  geom_test_if_point_inside_polygon (int nvertex, double *px, double *py, double x, double y );
int  geom_test_if_point_near_edges_of_polygon (int nvertex, double *px, double *py, double dist, double x, double y );

#endif
