//----------------------------------------------------------------------------------
//--
//--    Filename: curve_lib.c
//--    Description:
//--    Use: This module contains a library of functions used handle generic "curve_struct" objects
//--         i.e. simple one dimensional vectors 
//--
//--

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <ctype.h>

#include "define.h"
#include "fits_helper_lib.h"
#include "curve_lib.h"

#define CVS_REVISION "$Revision: 1.1 $"
#define CVS_DATE     "$Date: 2015/11/16 20:02:59 $"

#define ERROR_PREFIX   "#-curve_lib.c : Error   :"
#define WARNING_PREFIX "#-curve_lib.c : Warning :"
#define COMMENT_PREFIX "#-curve_lib.c : Comment :"
#define DEBUG_PREFIX   "#-curve_lib.c : DEBUG   :"


//functions
int curve_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}

//---------------------------------------------------------------------------------------------------
//curve_struct functions

int curve_struct_init ( curve_struct *pCurve )
{
  if ( pCurve == NULL ) return 1;
  strncpy(pCurve->str_name,     "", STR_MAX);
  strncpy(pCurve->str_filename, "", STR_MAX);

  strncpy(pCurve->str_unit_x, "", STR_MAX);
  strncpy(pCurve->str_unit_y, "", STR_MAX);
  pCurve->unit_x = (double) NAN;
  pCurve->unit_y = (double) NAN;
  pCurve->unit_type_x = (int) CURVE_UNIT_TYPE_UNKNOWN;
  pCurve->unit_type_y = (int) CURVE_UNIT_TYPE_UNKNOWN;
  
  pCurve->file_format = (int) CURVE_FILE_FORMAT_UNKNOWN;
  strncpy(pCurve->str_colname_x, "", STR_MAX);
  strncpy(pCurve->str_colname_y, "", STR_MAX);
  pCurve->colnum_x = (int) -1;
  pCurve->colnum_y = (int) -1;

  pCurve->num_rows = (long) -1;  
  pCurve->px  = (double*) NULL;   
  pCurve->py  = (double*) NULL;   
  
  pCurve->min_x = (double) NAN;
  pCurve->max_x = (double) NAN;
  pCurve->min_y = (double) NAN;
  pCurve->max_y = (double) NAN;
  return 0;
}

int curve_struct_free ( curve_struct *pCurve )
{
  if ( pCurve == NULL ) return 1;
  if ( pCurve->px ) {free ( pCurve->px ); pCurve->px = NULL;}
  if ( pCurve->py ) {free ( pCurve->py ); pCurve->py = NULL;}
  return 0;
}


int curve_struct_scale_y ( curve_struct *pCurve, double scale )
{
  long i;
  if ( pCurve == NULL) return 1;
  for( i=0;i<pCurve->num_rows; i++)
  {
    pCurve->py[i]  *= scale;   
  }
  pCurve->min_y *= scale;
  pCurve->max_y *= scale;

  return 0;
}

//allocate memory for, and copy contents of a curve struct
//should probably do this with a memcpy 
int curve_struct_copy ( curve_struct *pCurveDest, curve_struct *pCurveOrig )
{
  if ( pCurveDest == NULL ||
       pCurveOrig == NULL ||
       pCurveDest == pCurveOrig ) return 1;

  if ( curve_struct_init(pCurveDest)) return 1;
  
  strncpy(pCurveDest->str_name,     pCurveOrig->str_name, STR_MAX);
  strncpy(pCurveDest->str_filename, pCurveOrig->str_filename, STR_MAX);
  pCurveDest->file_format = (int) pCurveOrig->file_format;

  strncpy(pCurveDest->str_unit_x, pCurveOrig->str_unit_x, STR_MAX);
  strncpy(pCurveDest->str_unit_y, pCurveOrig->str_unit_y, STR_MAX);

  pCurveDest->unit_x = (double) pCurveOrig->unit_x;
  pCurveDest->unit_y = (double) pCurveOrig->unit_y;
  pCurveDest->unit_type_x = (int) pCurveOrig->unit_type_x;
  pCurveDest->unit_type_y = (int) pCurveOrig->unit_type_y;
  
  strncpy(pCurveDest->str_colname_x, pCurveOrig->str_colname_x, STR_MAX);
  strncpy(pCurveDest->str_colname_y, pCurveOrig->str_colname_y, STR_MAX);
  pCurveDest->colnum_x = (int) pCurveOrig->colnum_x;
  pCurveDest->colnum_y = (int) pCurveOrig->colnum_y;

  pCurveDest->num_rows = (long) pCurveOrig->num_rows;
  pCurveDest->px = NULL;
  pCurveDest->py = NULL;
  if ( pCurveDest->num_rows <= 0 )
  {
    fprintf(stderr, "%s Problem before assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  {
    long i;
    long nalloc = pCurveDest->num_rows + 1; //debug buffer
    if ( pCurveOrig->px)
    {
      if ((pCurveDest->px = (double*) malloc(sizeof(double) * nalloc)) == NULL)
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      for( i=0;i<pCurveDest->num_rows; i++) pCurveDest->px[i] = (double) pCurveOrig->px[i];   
    }
    
    if ( pCurveOrig->py)
    {
      if ((pCurveDest->py = (double*) malloc(sizeof(double) * nalloc)) == NULL)
      {
        fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      for( i=0;i<pCurveDest->num_rows; i++) pCurveDest->py[i] = (double) pCurveOrig->py[i];   
    }
  }

  pCurveDest->min_x = (double) pCurveOrig->min_x;
  pCurveDest->max_x = (double) pCurveOrig->max_x;
  pCurveDest->min_y = (double) pCurveOrig->min_y;
  pCurveDest->max_y = (double) pCurveOrig->max_y;

  return 0;
}





//---------------------------------------------------------------------------------------------------
//functions to read in generic curves
//need a fits version and an ascii version
//expect that pCurve->str_filename is already set by calling function
int curve_struct_read_from_file ( curve_struct *pCurve, BOOL verbose )
{
  int fhelp_file_type;
  long i;
  if ( pCurve == NULL ) return 1;

  fhelp_file_type = (int) fhelp_test_if_file_is_fits((const char*) pCurve->str_filename);
  if ( fhelp_file_type == FHELP_FILE_FORMAT_CODE_FITS )
  {
    pCurve->file_format = CURVE_FILE_FORMAT_FITS;
  }
  else
  {
    if ( fhelp_file_type == FHELP_FILE_FORMAT_CODE_NOT_FITS ) 
    {
      pCurve->file_format = CURVE_FILE_FORMAT_ASCII;
    }
    else
    {
      fprintf(stderr, "%s I had problems opening the file: %s (fhelp_file_type=%d)\n",
              ERROR_PREFIX, pCurve->str_filename, fhelp_file_type);
      return 1;

    }
  }

  if (verbose) {
    fprintf(stdout, "%s %s is file type: %s\n", DEBUG_PREFIX, pCurve->str_filename,
            (pCurve->file_format==CURVE_FILE_FORMAT_FITS ? "FITS":
             (pCurve->file_format==CURVE_FILE_FORMAT_ASCII ? "ASCII": "UNKNOWN")));
  }
 
  switch ( pCurve->file_format )
  {
  case CURVE_FILE_FORMAT_ASCII:
    if ( curve_struct_read_from_asciifile ( (curve_struct*) pCurve, (BOOL) verbose) )
    {
      return 1;
    }
    break;
    
  case CURVE_FILE_FORMAT_FITS:
    if ( curve_struct_read_from_fitsfile ( (curve_struct*) pCurve, (BOOL) verbose) )
    {
      return 1;
    }
    break;
    
  case CURVE_FILE_FORMAT_UNKNOWN:
    break;


  default:
    break;
  }
  
  
  //and record some metadata
  pCurve->min_x = (double) DBL_MAX;
  pCurve->max_x = (double) -DBL_MAX;
  pCurve->min_y  = (double) DBL_MAX;
  pCurve->max_y  = (double) -DBL_MAX;
  for(i=0;i<pCurve->num_rows;i++)
  {
    if ( pCurve->px[i] < pCurve->min_x ) pCurve->min_x = pCurve->px[i];
    if ( pCurve->px[i] > pCurve->max_x ) pCurve->max_x = pCurve->px[i];
    if ( pCurve->py[i] < pCurve->min_y ) pCurve->min_y = pCurve->py[i];
    if ( pCurve->py[i] > pCurve->max_y ) pCurve->max_y = pCurve->py[i];
  }
  
  if (verbose) {
    fprintf(stdout, "%s %s data ranges: x=[%g:%g]%s y=[%g:%g]%s nrows=%ld\n", DEBUG_PREFIX,
            pCurve->str_filename,
            pCurve->min_x*pCurve->unit_x,
            pCurve->max_x*pCurve->unit_x,
            CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_x),
            pCurve->min_y*pCurve->unit_y,
            pCurve->max_y*pCurve->unit_y,
            CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_y),
            pCurve->num_rows); fflush(stdout);
  }
  return 0;
}

//assume that have already checked that file exists, is fits, is open, and chdu is table format
int curve_struct_read_from_fits_table ( curve_struct *pCurve, fitsfile* pFile, BOOL verbose )
{
  int status = 0;
  int num_cols;
  long num_rows_per_chunk, i;
  if ( pCurve == NULL || pFile == NULL ) return 1;

  if (verbose) {
    fprintf(stdout, "%s %s Reading FITS curve from CHDU (table)\n", DEBUG_PREFIX, pCurve->str_filename); fflush(stdout);
  }
  if ( fits_get_num_rows ((fitsfile*) pFile, &(pCurve->num_rows), &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }

  if (verbose) {
    fprintf(stdout, "%s %s contains %ld rows\n", DEBUG_PREFIX, pCurve->str_filename, pCurve->num_rows); fflush(stdout);
  }
  if ( pCurve->num_rows <= 0 )
  {
    fprintf(stderr, "%s I found zero valid rows in the fits file: %s\n", ERROR_PREFIX, pCurve->str_filename);
    fflush(stderr);
    return 1;
  }

  

  //work out the index of the columns we need to read
  if ( fits_get_num_cols ((fitsfile*) pFile, &num_cols, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  if ( strlen(pCurve->str_colname_x) )
  {
    if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) pCurve->str_colname_x, &(pCurve->colnum_x), &status))
    {
      fprintf(stderr, "%s I couldn't find column '%s' in the fits file: %s\n",
              ERROR_PREFIX, pCurve->str_colname_x, pCurve->str_filename);
      fits_report_error(stderr, status);
      return 1;
    }

  }
  else
  {
    pCurve->colnum_x = 1;
  }

  //and get some column meta data
  if ( curve_read_unit_for_numbered_column((fitsfile*) pFile, (int) pCurve->colnum_x,
                                          (char*) pCurve->str_unit_x,
                                          (int*) &(pCurve->unit_type_x),
                                          (double*) &(pCurve->unit_x))) return 1;
    

  
  if ( strlen(pCurve->str_colname_y) )
  {
    if ( fits_get_colnum ((fitsfile*) pFile, CASEINSEN, (char*) pCurve->str_colname_y, &(pCurve->colnum_y), &status))
    {
      fprintf(stderr, "%s I couldn't find column '%s' in the fits file: %s\n",
              ERROR_PREFIX, pCurve->str_colname_y, pCurve->str_filename);
      fits_report_error(stderr, status);
      return 1;
    }
  }
  else
  {
    pCurve->colnum_y = 2;
  }

  //and get some column meta data
  if ( curve_read_unit_for_numbered_column((fitsfile*) pFile, (int) pCurve->colnum_y,
                                          (char*) pCurve->str_unit_y,
                                          (int*) &(pCurve->unit_type_y),
                                          (double*) &(pCurve->unit_y))) return 1;

  //check that colnums are sensible
  //set to < 0 to ignore
  if ( pCurve->colnum_x > num_cols || pCurve->colnum_y > num_cols )
  {
    fprintf(stderr, "%s Strange column numbers returned when reading from: %s\n", ERROR_PREFIX, pCurve->str_filename);
    fflush(stderr);
    return 1;
  }

  if ( pCurve->colnum_x < 0 && pCurve->colnum_y < 0 )
  {
    fprintf(stderr, "%s Skipping because cannot find any sensible columns in file: %s \n",
            ERROR_PREFIX, pCurve->str_filename);
    return 1;  
  }

  //work out the most efficient size of chunks
  if ( fits_get_rowsize(pFile, &num_rows_per_chunk, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }

  //allocate space for data
  if ( pCurve->colnum_x > 0 )
  {
    if ((pCurve->px = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  if ( pCurve->colnum_y > 0 )
  {
    if ((pCurve->py = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  
  for ( i=0; i<pCurve->num_rows; i+=num_rows_per_chunk )
  {
    long num_rows_this_chunk = MIN(num_rows_per_chunk, pCurve->num_rows-i);
    
    //read in the data, using sensible chunk sizes
    if ( pCurve->colnum_x > 0 )
      fits_read_col(pFile, TDOUBLE, pCurve->colnum_x, i+1, 1, num_rows_this_chunk, NULL,&(pCurve->px[i]), NULL, &status);
    if ( pCurve->colnum_y > 0 )
      fits_read_col(pFile, TDOUBLE, pCurve->colnum_y, i+1, 1, num_rows_this_chunk, NULL,&(pCurve->py[i]), NULL, &status);
    
    if ( status)
    {
      fits_report_error(stderr, status);
      return 1;
    }
  }

  return 0;
}
////////////////////////////////////////////////////////////////////////////////////////



//assume that have already checked that file exists, is fits, is open, and chdu is image format
//only 1-d image arrays are valid
int curve_struct_read_from_fits_image ( curve_struct *pCurve, fitsfile* pFile, BOOL verbose )
{
  const long fpixel[2] = {1, 1};
  int status = 0;
  int naxis;
  long pnaxes[2] = {1, 1};
  int i;
  if ( pCurve == NULL || pFile == NULL ) return 1;

  if (verbose) {
    fprintf(stdout, "%s %s Reading FITS curve from CHDU (image)\n", DEBUG_PREFIX, pCurve->str_filename); fflush(stdout);
  }
  
  if ( fits_get_img_dim ((fitsfile*) pFile, (int*) &naxis, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }

  if (verbose) {
    fprintf(stdout, "%s %s contains an image with %d dimension%s\n", DEBUG_PREFIX, pCurve->str_filename, naxis, (naxis>1?"s":"")); fflush(stdout);
  }
  if ( naxis > 2 ) return 1;
  
  if ( fits_get_img_size ((fitsfile*) pFile, naxis, (long*) pnaxes, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }

  if (verbose) {
    for ( i=0;i<naxis;i++)
      fprintf(stdout, "%s %s contains an image with naxis%d = %ld\n", DEBUG_PREFIX, pCurve->str_filename, i+1, pnaxes[i]); fflush(stdout);
  }
  
  pCurve->num_rows = pnaxes[0];
  
  //allocate space for data
  if ((pCurve->px = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL ||
      (pCurve->py = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //read the y values from the array
  if ( fits_read_pix ((fitsfile*) pFile, (int) TDOUBLE,
                      (long*) fpixel, (LONGLONG) pCurve->num_rows,
                      NULL, pCurve->py, NULL, &status))
  {
    
    fits_report_error(stderr, status);
    return 1;
  }

  //now get the wcs...
  {
    double crval1, crpix1, cdelt1;
    //    char str_ctype1[STR_MAX];
    char str_bunit[STR_MAX];
    char str_cunit[STR_MAX];
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, "CRVAL1", &crval1,  NULL, &status);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, "CRPIX1", &crpix1,  NULL, &status);
    fits_read_key ((fitsfile*) pFile, (int) TDOUBLE, "CDELT1", &cdelt1,  NULL, &status);

    if ( status )
    {
      fits_report_error(stderr, status);
      return 1;
    }
    status = 0;
    strncpy (str_bunit, "", STR_MAX);
    strncpy (str_cunit, "", STR_MAX);

    //assume that the x-axis is in angstroms unless otherwise stated
    // should check agaianst http://fits.gsfc.nasa.gov/fits_standard.html
    // e.g. tables 25,26
    // the following is all rubbish - should stick to the FITS standards
//    if ( fits_read_key ((fitsfile*) pFile, (int) TSTRING, "CTYPE1", str_ctype1,  NULL, &status) == 0)
//    {
//      for(i=0;i<strlen(str_ctype1);i++)
//        str_ctype1[i] = toupper(str_ctype1[i]);
//
//      if ( strcmp(str_ctype1, "ANGSTROM") == 0 )
//      {
//        units = (double) 0.1;
//      }
//      else if ( strcmp(str_ctype1, "WAVE") == 0 )
//      {
//        units = (double) 1.0;
//      }
//      else if ( strcmp(str_ctype1, "UM") == 0 ||
//                strcmp(str_ctype1, "MICRON") == 0 )
//      {
//        units = (double) 1.0;
//      }
//      else
//      {
//        fprintf(stderr, "%s %s Unknown CTYPE1 keyword: %s\n", ERROR_PREFIX,  pCurve->str_filename, str_ctype1);
//        return 1;
//      }
//    }
//    else
//    {
//      units = 0.1; //assume that is given in Angstroms
//    }

    //get the x-axis units
    status = 0;
    if ( fits_read_key ((fitsfile*) pFile, (int) TSTRING, "CUNIT1", str_cunit,  NULL, &status))
    {
      if ( status != KEY_NO_EXIST )
      {
        fits_report_error(stderr, status);
      //      strncpy(pCurve->str_unit_y, "units?", STR_MAX);
        return 1;
      }
    }
    if ( strlen(str_cunit) == 0 )  //assume that x-axis is in Angstroms
    {
      pCurve->unit_type_x = CURVE_UNIT_TYPE_WAVELENGTH;
      pCurve->unit_x = (double) 1.0;
      strncpy(pCurve->str_unit_x, CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_x), STR_MAX);  
      fprintf(stderr, "%s %s : Could not read CUNIT1 keyword, assuming that x-axis units are: %s\n",
              WARNING_PREFIX,  pCurve->str_filename, pCurve->str_unit_x);
    }
    else
    {
      strncpy(pCurve->str_unit_x, str_cunit, STR_MAX);  
      if ( curve_interpret_unit_string((char*) pCurve->str_unit_x, (int*) &(pCurve->unit_type_x), (double*)&(pCurve->unit_x)))
        return 1;
    }

    
    //    //Fudged to make input spectra work.
    //    units = 1.0; //assume that wavelength is given in Angstroms - but we want AA
   
    for(i=0;i<pCurve->num_rows;i++)
    {
      pCurve->px[i] = (double) (pCurve->unit_x * (crval1 + cdelt1 * ((double) (i + 1) - crpix1))); 
    }
    //    strncpy(pCurve->str_unit_x, "AA", STR_MAX);  
    //    if ( curve_interpret_unit_string((char*) pCurve->str_unit_x, (int*) &(pCurve->unit_type_x), (double*)&(pCurve->unit_x)))
    //      return 1;

    //get the y-axis units   
    status = 0;
    if ( fits_read_key ((fitsfile*) pFile, (int) TSTRING, "BUNIT", str_bunit,  NULL, &status))
    {
      if ( status != KEY_NO_EXIST )
      {
        fits_report_error(stderr, status);
      //      strncpy(pCurve->str_unit_y, "units?", STR_MAX);
        return 1;
      }
    }
    if ( strlen(str_bunit) == 0 )
    {
      pCurve->unit_type_y = CURVE_UNIT_TYPE_FE_LAMBDA;
      pCurve->unit_y = 1.0;
      strncpy(pCurve->str_unit_y, CURVE_UNIT_TYPE_DEFAULT_UNIT(pCurve->unit_type_y), STR_MAX);  
      fprintf(stderr, "%s %s : Could not read BUNIT keyword, assuming that y-axis units are: %s\n",
              WARNING_PREFIX,  pCurve->str_filename, pCurve->str_unit_y);
    }
    else
    {
      strncpy(pCurve->str_unit_y, str_bunit, STR_MAX);  
      if ( curve_interpret_unit_string((char*) pCurve->str_unit_y, (int*) &(pCurve->unit_type_y), (double*)&(pCurve->unit_y)))
        return 1;
    }

    
  }
  
  return 0;
}



///////////////////////////////////////////////////////////////////////////////////////////  
//assume that have already checked that file exists and is in fits format
//TODO make sure that the logic of the various steps actualy works.
int curve_struct_read_from_fitsfile ( curve_struct *pCurve, BOOL verbose )
{
  int status = 0;
  fitsfile *pFile = NULL;     // FITS file pointer
  if ( pCurve == NULL ) return 1;

  if (verbose) {
    fprintf(stdout, "%s %s Reading FITS curve from file\n", DEBUG_PREFIX, pCurve->str_filename); fflush(stdout);
  }
  
  // try to open as an image
  fits_open_image((fitsfile**) &pFile, pCurve->str_filename, READONLY, &status);
  if (status == 0)
  {
    int naxis;
    if ( fits_get_img_dim ((fitsfile*) pFile, (int*) &naxis, &status))
    {
      fits_report_error(stderr, status);
      return 1;
    }
    
    if ( naxis >= 1 ) 
    {
      //      const int maxdim = 1;
      enum { maxdim = 1};   //avoids compiler warnings in clang
      long naxes[maxdim];
      if ( fits_get_img_size ((fitsfile*) pFile, (int) maxdim, (long*) naxes, (int*) &status))
      {
        return 1;
      }
      if ( naxis == 1 && naxes[0] == 0 )
      {
        fits_close_file(pFile, &status);  //this is probably a null extension
        //so get ready to be opened again by fits_open_table
      }
      else
      {  
        if ( curve_struct_read_from_fits_image ( (curve_struct*) pCurve, (fitsfile*) pFile, (BOOL) verbose ) == 0)
        {
          fits_close_file(pFile, &status);
          return 0;  //SUCCESS
        }
        else
        {
          fprintf(stderr, "%s Problem reading curve from fits image: %s\n", ERROR_PREFIX, pCurve->str_filename);
          return 1;
        }
      }
    }
    else
    {
      //get here if we found a hdu with naxis=0
      fits_close_file(pFile, &status);  //ready to be opened again by fits_open_table
      //fprintf(stderr, "%s Problem reading curve from fits image (naxis=0): %s\n", ERROR_PREFIX, pCurve->str_filename);
      //return 1;
    }
  }
  else
  {
    if ( status == NOT_IMAGE ) // fine, this just means that we should try to open the file as a table
    {
      status = 0;
    }
    else
    {
      fits_report_error(stderr, status);  //DEBUG
      return 1;
    }
  }

  
  //now try to open as a table
  status = 0;
  if (verbose) {
    fprintf(stdout, "%s %s Reading FITS curve from file (as BINARY_TBL)\n", DEBUG_PREFIX, pCurve->str_filename); fflush(stdout);
  }
  fits_open_table((fitsfile**) &pFile, pCurve->str_filename, READONLY, &status);
  if (status == 0)
  {
    if ( curve_struct_read_from_fits_table ( (curve_struct*) pCurve, (fitsfile*) pFile, (BOOL) verbose ) == 0 )
    {
      fits_close_file(pFile, &status);
      return 0; //success
    }
    else
    {
      fprintf(stderr, "%s Problem reading curve from fits table: %s\n", ERROR_PREFIX, pCurve->str_filename);
      return 1;
    }
  }
  else
  {
    fits_report_error(stderr, status);  //DEBUG
    return 1;
  }
    
  return 0;
}




//assume that have already checked that file exists and is in ascii format
int curve_struct_read_from_asciifile ( curve_struct *pCurve, BOOL verbose )
{
  FILE *pFile = NULL;
  long i;
  char buffer[STR_MAX];
  if ( pCurve == NULL ) return 1;

  //do some sanity checks
  
  //open the file for reading
  if ((pFile = fopen(pCurve->str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the file: %s\n", ERROR_PREFIX, pCurve->str_filename);
    fflush(stderr);
    return 1;
  }

  if (verbose) {
    fprintf(stdout, "%s %s Reading ASCII curve from file\n", DEBUG_PREFIX, pCurve->str_filename); fflush(stdout);
  }
  //scan through the file to determine the number of lines
  i = 0;
  while (fgets((char*) buffer, (int) STR_MAX, (FILE*) pFile))
  {
    if ( buffer[0] != '#')  //ignore comment lines
    {
      i ++;
    }
  }
  //go back to the start
  rewind((FILE*)pFile);
  pCurve->num_rows = i;

  if (verbose) {
    fprintf(stdout, "%s %s contains %ld (max) lines\n", DEBUG_PREFIX, pCurve->str_filename, pCurve->num_rows); fflush(stdout);
  }
  if ( pCurve->num_rows <= 0 )
  {
    fprintf(stderr, "%s I found zero valid rows in the ascii file: %s\n", ERROR_PREFIX, pCurve->str_filename);
    fflush(stderr);
    return 1;
  }

  //allocate space for data
  if ((pCurve->px = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL ||
      (pCurve->py = (double*) malloc(sizeof(double) * pCurve->num_rows)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  //now read in the data
  {
    char str_format[STR_MAX] = "%lf %lf"; //TODO - pay attention to column numbers 
    i = 0;
    while (fgets((char*) buffer, (int) STR_MAX, (FILE*) pFile))
    {
      if ( buffer[0] != '#')  //ignore comment lines
      {
        int n_tokens;
        double temp1, temp2;
        n_tokens = sscanf(buffer, (char*) str_format,
                          (double*) &(temp1),
                          (double*) &(temp2));
        
        if ( n_tokens == 2 ) 
        {
          pCurve->px[i] = (double) temp1;
          pCurve->py[i]  = (double) temp2;
          i ++;  //valid row
        }
        else
        {
          //emit error?
        }
      }
    }
  }
  
  //close the file
  if ( pFile ) fclose ( pFile) ;
  pFile = NULL;

  //make a guess of the units - TODO interpret comments lines in header
  strncpy(pCurve->str_unit_x, "AA", STR_MAX );
  strncpy(pCurve->str_unit_y, "fraction", STR_MAX );
  if ( curve_interpret_unit_string((char*) pCurve->str_unit_x, (int*) &(pCurve->unit_type_x), (double*)&(pCurve->unit_x)))   return 1;
  if ( curve_interpret_unit_string((char*) pCurve->str_unit_y, (int*) &(pCurve->unit_type_y), (double*)&(pCurve->unit_y)))   return 1;
  
  
  if (verbose) {
    fprintf(stdout, "%s %s contained %ld (valid) lines\n", DEBUG_PREFIX, pCurve->str_filename, i); fflush(stdout);
  }
  if ( i <= 0 )
  {
    fprintf(stderr, "%s I read zero valid rows from the ascii file: %s\n", ERROR_PREFIX, pCurve->str_filename);
    fflush(stderr);
    return 1;
  }

  //tidy up 
  if ( i < pCurve->num_rows )
  {
    pCurve->num_rows = i;
    //allocate space for data
    if ((pCurve->px = (double*) realloc((double*)pCurve->px, sizeof(double) * pCurve->num_rows)) == NULL ||
        (pCurve->py = (double*) realloc((double*)pCurve->py, sizeof(double) * pCurve->num_rows)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }


  return 0;
}


int curve_struct_write_to_fits_table ( curve_struct *pCurve, BOOL verbose )
{
  fitsfile *pFile = NULL;
  int status = 0;
  const int ncols = 2;
  char *ttype[2];
  const char *tform[] = {"1D", "1D"};
  char *tunit[2];
  const int colnum[] = { 1 , 2};

  long i,nrows_per_chunk;
  
  if ( pCurve == NULL) return 1;

  if (verbose) {
    fprintf(stdout, "%s Writing curve %s to fits file (table): %s\n",
            COMMENT_PREFIX, pCurve->str_name, pCurve->str_filename); fflush(stdout);
  }
  if ( fits_create_file ((fitsfile**) &pFile, (char*) pCurve->str_filename, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  ttype[0] = (strlen(pCurve->str_colname_x) > 0 ? pCurve->str_colname_x : "X");
  ttype[1] = (strlen(pCurve->str_colname_y) > 0 ? pCurve->str_colname_y : "Y");
  tunit[0] = pCurve->str_unit_x;
  tunit[1] = pCurve->str_unit_y;
  
  if (  fits_create_tbl ((fitsfile*) pFile, (int) BINARY_TBL,
                         (LONGLONG) pCurve->num_rows, (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) (strlen(pCurve->str_name) > 0 ? pCurve->str_name : "CURVE"),
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  if ( fits_get_rowsize((fitsfile*) pFile, &nrows_per_chunk, &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  for(i=0;i<pCurve->num_rows; i+= nrows_per_chunk )
  {
    long nrows_this_chunk = MIN(nrows_per_chunk, pCurve->num_rows-i);
    
    fits_write_col ((fitsfile*) pFile, (int) TDOUBLE, (int) colnum[0], (LONGLONG) i+1,
                    (LONGLONG) 1, (LONGLONG) nrows_this_chunk, &(pCurve->px[i]), (int*) &status);
    fits_write_col ((fitsfile*) pFile, (int) TDOUBLE, (int) colnum[1], (LONGLONG) i+1,
                    (LONGLONG) 1, (LONGLONG) nrows_this_chunk, &(pCurve->py[i]), (int*) &status);
    if ( status ) 
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }

  
  fits_close_file(pFile, &status);
 
  return 0;
}

//---------------------------------------------------------------------------------------------------

//---
//---------------------------------------------------------------------------------------------------
//read the TUNIT keyword for the given column and interpret the content
int curve_read_unit_for_numbered_column( fitsfile* pFile, int colnum, char* str_unit, int* punit_type, double* punit)
{
  char str_keyname[STR_MAX];
  char str_key[STR_MAX];
  int status = 0;
  snprintf(str_keyname, STR_MAX, "TUNIT%d", colnum);
  if ( fits_read_key ((fitsfile*) pFile, (int) TSTRING, str_keyname , str_key, NULL, &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }

  //now preprocess the key string
  //  fprintf(stdout, "%s Processing units key %s: ->%s<-\n", DEBUG_PREFIX, str_keyname, str_key); fflush(stdout);
  if ( curve_interpret_unit_string((char*) str_key, (int*) punit_type, (double*) punit)) return 1;
  
  return 0;
}


int curve_interpret_unit_string(char* str_unit, int* punit_type, double* punit)
{
  int i,j;
  char buffer[STR_MAX];
  char last_char;
  char *pbuf;

  //  fprintf(stdout, "%s Interpreting unit string: ->%s<-\n", DEBUG_PREFIX, str_unit); fflush(stdout);

  if ( strlen(str_unit) >= STR_MAX ) return 1;
  else if ( strlen(str_unit) == 0 )
  {
    *punit_type = (int) CURVE_UNIT_TYPE_UNKNOWN;
    *punit      = (double) NAN;
    return 0;
  }
  //now preprocess the unit string
  //convert to lower case, and remove "[","]", carets
  j = 0;
  last_char = '\0';
  for(i=0;i<strlen(str_unit); i++)
  {
    if ( str_unit[i] != '[' && str_unit[i] != ']' && str_unit[i] != '^' )
    {
      buffer[j] = tolower(str_unit[i]);
      //      buffer[j] = str_unit[i];
      j++;
    }
    last_char = str_unit[i];
  }
  buffer[j] = '\0';
  //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

  //remove any surplus white spaces
  j = 0;
  last_char = '\0';
  for(i=0;i<strlen(buffer); i++)
  {
    if ( !( buffer[i] == ' ' && (last_char == ' ' || last_char == '/' || last_char == '\0')) &&
         !( buffer[i] == '/' && last_char == '/'))
    {
      buffer[j] = buffer[i];
      j++;
    }
    last_char = buffer[i];
  }
  buffer[j] = '\0';
  //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

  //make some replacements to simplify the following steps
  //replace variations on "photon" with "ph"
  while ( (pbuf = (char*) strstr(buffer, "photons"))) { pbuf[0]=' ';pbuf[1]=' ';pbuf[2]=' ';pbuf[3]=' ';pbuf[4]=' ';pbuf[5]='p';pbuf[6]='h';}
  while ( (pbuf = (char*) strstr(buffer, "photon")) ) { pbuf[0]=' ';pbuf[1]=' ';pbuf[2]=' ';pbuf[3]=' ';pbuf[4]='p';pbuf[5]='h';}
  while ( (pbuf = (char*) strstr(buffer, "phot"))   ) { pbuf[0]=' ';pbuf[1]=' ';pbuf[2]='p';pbuf[3]='h';}
  //replace "ergs" with "erg"
  while ( (pbuf = (char*) strstr(buffer, "ergs"))   ) { pbuf[0]=' ';pbuf[1]='e';pbuf[2]='r';pbuf[3]='g';}
  //replace "jansky" with "jy"
  while ( (pbuf = (char*) strstr(buffer, "jansky")) ) { pbuf[0]=' ';pbuf[1]=' ';pbuf[2]=' ';pbuf[3]=' ';pbuf[4]='j';pbuf[5]='y';}
  //replace variations on Angstrom with "a"
  while ( (pbuf = (char*) strstr(buffer, "angstrom"))) {pbuf[0]=' ';pbuf[1]='a';pbuf[2]=' ';pbuf[3]=' ';pbuf[4]=' ';pbuf[5]=' ';pbuf[6]=' ';pbuf[7]=' '; }
  while ( (pbuf = (char*) strstr(buffer, "ang"))    ) { pbuf[0]=' ';pbuf[1]='a';pbuf[2]=' ';}
  while ( (pbuf = (char*) strstr(buffer, "\\aa"))   ) { pbuf[0]=' ';pbuf[1]=' ';pbuf[2]='a';}
  while ( (pbuf = (char*) strstr(buffer, "aa"))     ) { pbuf[0]=' ';pbuf[1]='a';}
  //replace variations on "/s/unit" with "/unit/s"
  while ( (pbuf = (char*) strstr(buffer, "/a/cm2")) ) { pbuf[0]='/';pbuf[1]='c';pbuf[2]='m';pbuf[3]='2';pbuf[4]='/';pbuf[5]='a';}
  while ( (pbuf = (char*) strstr(buffer, "/a/m2"))  ) { pbuf[0]='/';pbuf[1]='m';pbuf[2]='2';pbuf[3]='/';pbuf[4]='a';}
  while ( (pbuf = (char*) strstr(buffer, "/nm/cm2"))) { pbuf[0]='/';pbuf[1]='c';pbuf[2]='m';pbuf[3]='2';pbuf[4]='/';pbuf[5]='n';pbuf[6]='m';}
  while ( (pbuf = (char*) strstr(buffer, "/nm/m2")) ) { pbuf[0]='/';pbuf[1]='m';pbuf[2]='2';pbuf[3]='/';pbuf[4]='a';pbuf[5]='m';}
  while ( (pbuf = (char*) strstr(buffer, "a-1 cm-2"))){ pbuf[0]='c';pbuf[1]='m';pbuf[2]='-';pbuf[3]='2';pbuf[4]=' ';pbuf[5]='a';pbuf[6]='-';pbuf[7]='1';}
  while ( (pbuf = (char*) strstr(buffer, "a-1 m-2"))) { pbuf[0]='m';pbuf[1]='-';pbuf[2]='2';pbuf[3]=' ';pbuf[4]='a';pbuf[5]='-';pbuf[6]='1';}
  while ( (pbuf = (char*) strstr(buffer, "nm-1 cm-2"))){pbuf[0]='c';pbuf[1]='m';pbuf[2]='-';pbuf[3]='2';pbuf[4]=' ';pbuf[5]='n';pbuf[6]='m';pbuf[7]='-';pbuf[8]='1';}
  while ( (pbuf = (char*) strstr(buffer, "nm-1 m-2"))) {pbuf[0]='m';pbuf[1]='-';pbuf[2]='2';pbuf[3]=' ';pbuf[4]='n';pbuf[5]='m';pbuf[6]='-';pbuf[7]='1';}
  
  while ( (pbuf = (char*) strstr(buffer, "/s/cm2")) ) { pbuf[0]='/';pbuf[1]='c';pbuf[2]='m';pbuf[3]='2';pbuf[4]='/';pbuf[5]='s';}
  while ( (pbuf = (char*) strstr(buffer, "/s/m2"))  ) { pbuf[0]='/';pbuf[1]='m';pbuf[2]='2';pbuf[3]='/';pbuf[4]='s';}
  while ( (pbuf = (char*) strstr(buffer, "s-1 cm-2"))){ pbuf[0]='c';pbuf[1]='m';pbuf[2]='-';pbuf[3]='2';pbuf[4]=' ';pbuf[5]='s';pbuf[6]='-';pbuf[7]='1';}
  while ( (pbuf = (char*) strstr(buffer, "s-1 m-2"))) { pbuf[0]='m';pbuf[1]='-';pbuf[2]='2';pbuf[3]=' ';pbuf[4]='s';pbuf[5]='-';pbuf[6]='1';}

  //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

  //remove any surplus white spaces
  j = 0;
  last_char = '\0';
  for(i=0;i<strlen(buffer); i++)
  {
    if ( !( buffer[i] == ' ' && (last_char == ' ' || last_char == '/' || last_char == '\0')) &&
         !( buffer[i] == '/' && last_char == '/'))
    {
      buffer[j] = buffer[i];
      j++;
    }
    last_char = buffer[i];
  }
  buffer[j] = '\0';

  //trim trailing white space
  while (strlen(buffer) > 0 && buffer[strlen(buffer)-1] == ' ') buffer[strlen(buffer)-1] = '\0';
  //  fprintf(stdout, "%s Interpreting unit string: \"%s\"\n", DEBUG_PREFIX, buffer); fflush(stdout);

  
  // CURVE_UNIT_TYPE_DIMENSIONLESS
  if      (strcasecmp(buffer, "ratio")         == 0 ||
           strcasecmp(buffer, "fraction")      == 0 ||
           strcasecmp(buffer, "fractional")    == 0 ||
           strcasecmp(buffer, "throughput")    == 0 ||
           strcasecmp(buffer, "resolution")    == 0 ||
           strcasecmp(buffer, "res")           == 0 ||
           strcasecmp(buffer, "pix")           == 0 ||
           strcasecmp(buffer, "pixel")         == 0 ||
           strcasecmp(buffer, "transmission")  == 0 ||
           strcasecmp(buffer, "dimensionless") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DIMENSIONLESS;  *punit = (double) 1.e0; }
  
  // CURVE_UNIT_TYPE_WAVELENGTH
  else if (strcasecmp(buffer, "a")             == 0 ||
           strcasecmp(buffer, "angstrom")      == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_WAVELENGTH;     *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "nm")            == 0 ||
           strcasecmp(buffer, "nanometre")     == 0 ||
           strcasecmp(buffer, "nanometres")    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_WAVELENGTH;     *punit = (double) 1.e1; }
  else if (strcasecmp(buffer, "um")            == 0 ||
           strcasecmp(buffer, "mum")           == 0 ||
           strcasecmp(buffer, "micron")        == 0 ||
           strcasecmp(buffer, "microns")       == 0 ||
           strcasecmp(buffer, "micrometre")    == 0 ||
           strcasecmp(buffer, "micrometres")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_WAVELENGTH;     *punit = (double) 1.e4; }
  else if (strcasecmp(buffer, "mm")            == 0 ||
           strcasecmp(buffer, "millimetre")    == 0 ||
           strcasecmp(buffer, "millimetres")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_WAVELENGTH;     *punit = (double) 1.e7; }
  else if (strcasecmp(buffer, "cm")            == 0 ||
           strcasecmp(buffer, "centimetre")    == 0 ||
           strcasecmp(buffer, "centimetres")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_WAVELENGTH;     *punit = (double) 1.e7; }
  else if (strcasecmp(buffer, "m")             == 0 ||
           strcasecmp(buffer, "metre")         == 0 ||
           strcasecmp(buffer, "metres")        == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_WAVELENGTH;     *punit = (double) 1.e10;}
  // CURVE_UNIT_TYPE_GEOMETRIC_AREA
  else if (strcasecmp(buffer, "m2")            == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_GEOMETRIC_AREA; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "cm2")           == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_GEOMETRIC_AREA; *punit = (double) 1.e-4;}
  else if (strcasecmp(buffer, "mm2")           == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_GEOMETRIC_AREA; *punit = (double) 1.e-6;}
  else if (strcasecmp(buffer, "um2")           == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_GEOMETRIC_AREA; *punit = (double) 1.e-12;}
  // CURVE_UNIT_TYPE_SOLID_ANGLE
  else if (strcasecmp(buffer, "deg2")          == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_SOLID_ANGLE;    *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "arcmin2")       == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_SOLID_ANGLE;    *punit = (double) SQR(1./60.);}
  else if (strcasecmp(buffer, "arcsec2")       == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_SOLID_ANGLE;    *punit = (double) SQR(1./3600.);}
  else if (strcasecmp(buffer, "ster")          == 0 ||
           strcasecmp(buffer, "steradian")     == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_SOLID_ANGLE;    *punit = (double) SQR(180./M_PI);}
  //  CURVE_UNIT_TYPE_FPHOT
  else if (strcasecmp(buffer, "ph/cm2/s")         == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1")      == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT;     *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "ph/m2/s")          == 0 ||
           strcasecmp(buffer, "ph m-2 s-1")       == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT;     *punit = (double) 1.e-4; }
  //  CURVE_UNIT_TYPE_FPHOT_LAMBDA
  else if (strcasecmp(buffer, "ph/cm2/s/a")       == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 a-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "ph/cm2/s/nm")      == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 nm-1") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA; *punit = (double) 1.e-1; }
  else if (strcasecmp(buffer, "ph/cm2/s/um")      == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 um-1") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "ph/m2/s/a")        == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 a-1")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "ph/m2/s/nm")       == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 nm-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA; *punit = (double) 1.e-5; }
  else if (strcasecmp(buffer, "ph/m2/s/um")       == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 um-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA; *punit = (double) 1.e-8; }

  //  CURVE_UNIT_TYPE_FPHOT_NU
  else if (strcasecmp(buffer, "ph/cm2/s/hz")       == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 hz-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_NU; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "ph/m2/s/hz")        == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 hz-1")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_NU; *punit = (double) 1.e-4; }

  //  CURVE_UNIT_TYPE_FE
  else if (strcasecmp(buffer, "erg/cm2/s")         == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1")      == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE;     *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "erg/m2/s")          == 0 ||
           strcasecmp(buffer, "erg m-2 s-1")       == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE;     *punit = (double) 1.e-4; }

  //  CURVE_UNIT_TYPE_FE_LAMBDA
  else if (strcasecmp(buffer, "erg/cm2/s/a")       == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 a-1")  == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 a")    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "erg/cm2/s/nm")      == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 nm-1") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e-1; }
  else if (strcasecmp(buffer, "erg/cm2/s/um")      == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 um-1") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "erg/m2/s/a")        == 0 ||
           strcasecmp(buffer, "erg m-2 s-1 a-1")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "erg/m2/s/nm")       == 0 ||
           strcasecmp(buffer, "erg m-2 s-1 nm-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e-5; }
  else if (strcasecmp(buffer, "erg/m2/s/um")       == 0 ||
           strcasecmp(buffer, "erg m-2 s-1 um-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e-8; }
  else if (strcasecmp(buffer, "w/m2/um")           == 0 ||
           strcasecmp(buffer, "w m-2 um-1")        == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_LAMBDA; *punit = (double) 1.e-1; }

  //  CURVE_UNIT_TYPE_FE_NU
  else if (strcasecmp(buffer, "erg/cm2/s/hz")       == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 hz-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_NU; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "erg/m2/s/hz")        == 0 ||
           strcasecmp(buffer, "erg m-2 s-1 hz-1")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_NU; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "jy")                 == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_NU; *punit = (double) 1.e-23; }
  else if (strcasecmp(buffer, "mjy")                == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_NU; *punit = (double) 1.e-26; }
  else if (strcasecmp(buffer, "ujy")                == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_NU; *punit = (double) 1.e-29; }
  else if (strcasecmp(buffer, "njy")                == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_NU; *punit = (double) 1.e-32; }
  //could have problems with megajanskys
  
  //  CURVE_UNIT_TYPE_FE_E
  else if (strcasecmp(buffer, "erg/cm2/s/keV")       == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 keV-1")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_E; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "erg/m2/s/keV")        == 0 ||
           strcasecmp(buffer, "erg m-2 s-1 keV-1")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_E; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "erg/cm2/s/eV")        == 0 ||
           strcasecmp(buffer, "erg cm-2 s-1 eV-1")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_E; *punit = (double) 1.e3; }
  else if (strcasecmp(buffer, "erg/m2/s/eV")         == 0 ||
           strcasecmp(buffer, "erg m-2 s-1 eV-1")    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FE_E; *punit = (double) 1.e-1; }

  //  CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID
  else if (strcasecmp(buffer, "ph/cm2/s/a/arcsec2")       == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 a-1 arcsec-2") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "ph/cm2/s/nm/arcsec2")      == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 nm-1 arcsec-2")== 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-1; }
  else if (strcasecmp(buffer, "ph/cm2/s/um/arcsec2")      == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 um-1 arcsec-2")== 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "ph/m2/s/a/arcsec2")        == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 a-1 arcsec-2")  == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-4; }
  else if (strcasecmp(buffer, "ph/m2/s/nm/arcsec2")       == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 nm-1 arcsec-2") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-5; }
  else if (strcasecmp(buffer, "ph/m2/s/um/arcsec2")       == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 um-1 arcsec-2") == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-8; }
  else if (strcasecmp(buffer, "ph/cm2/s/a/arcsec2")       == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 a-1 deg-2")    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e0/SQR(3600.0); }
  else if (strcasecmp(buffer, "ph/cm2/s/nm/deg2")         == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 nm-1 deg-2")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-1/SQR(3600.0); }
  else if (strcasecmp(buffer, "ph/cm2/s/um/deg2")         == 0 ||
           strcasecmp(buffer, "ph cm-2 s-1 um-1 deg-2")   == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-4/SQR(3600.0); }
  else if (strcasecmp(buffer, "ph/m2/s/a/deg2")           == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 a-1 deg-2")     == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-4/SQR(3600.0); }
  else if (strcasecmp(buffer, "ph/m2/s/nm/deg2")          == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 nm-1 deg-2")    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-5/SQR(3600.0); }
  else if (strcasecmp(buffer, "ph/m2/s/um/deg2")          == 0 ||
           strcasecmp(buffer, "ph m-2 s-1 um-1 deg-2")    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID; *punit = (double) 1.e-8/SQR(3600.0); }

  ///todo - add all "per solid angle" units 
  //CURVE_UNIT_TYPE_DISPERSION
  else if (strcasecmp(buffer, "a/um")                     == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DISPERSION;         *punit = (double) 1.e0; }
  else if (strcasecmp(buffer, "a/mm")                     == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DISPERSION;         *punit = (double) 1.e-3; }
  else if (strcasecmp(buffer, "a/m")                      == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DISPERSION;         *punit = (double) 1.e-6; }
  else if (strcasecmp(buffer, "nm/um")                    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DISPERSION;         *punit = (double) 1.e1; }
  else if (strcasecmp(buffer, "nm/mm")                    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DISPERSION;         *punit = (double) 1.e-2; }
  else if (strcasecmp(buffer, "nm/mm")                    == 0 ) { *punit_type = (int) CURVE_UNIT_TYPE_DISPERSION;         *punit = (double) 1.e-5; }



  //error condition 
  else
  {
    fprintf(stderr, "%s Interpreting unit string: \"%s\" ==> \"%s\" ==> unknown type\n",
            WARNING_PREFIX, str_unit, buffer);
  }

  //  fprintf(stdout, "%s Interpreting unit string: \"%s\" ==> \"%s\" ==> type=%s unit=%g %s\n",
  //          DEBUG_PREFIX,
  //          str_unit, buffer, CURVE_UNIT_TYPE_STRING(*punit_type),
  //          *punit, (CURVE_UNIT_TYPE_DEFAULT_UNIT(*punit_type))); fflush(stdout);

  return 0;
}

//---------------------------------------------------------------------------------------------------
