//----------------------------------------------------------------------------------
//--
//--    Filename: utensils_lib.h
//--    Use: This is a header for the utensils_lib.c C program
//--
//--    Notes:
//--

#ifndef UTENSILS_H
#define UTENSILS_H

//#define CVS_REVISION_UTENSILS_LIB_H "$Revision: 1.9 $"
//#define CVS_DATE_UTENSILS_LIB_H     "$Date: 2015/08/18 11:42:08 $"

#include <stdio.h>

#include "define.h"

//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.9 $"
#define CVS_DATE_H     "$Date: 2015/08/18 11:42:08 $"
inline static int util_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

#define UTIL_DATATYPE_CHAR   1
#define UTIL_DATATYPE_SHORT  2
#define UTIL_DATATYPE_USHORT 3
#define UTIL_DATATYPE_INT    4
#define UTIL_DATATYPE_UINT   5
#define UTIL_DATATYPE_LONG   6
#define UTIL_DATATYPE_ULONG  7
#define UTIL_DATATYPE_FLOAT  8
#define UTIL_DATATYPE_DOUBLE 9
#define UTIL_DATATYPE_BOOL   10

//----------------------------------------------------------------------------------------------
//-- public structure definitions

//a simple two-dimensional histogram structure
typedef struct {
  double xmin;
  double xmax;
  double dx;
  double inv_dx;

  double ymin;
  double ymax;
  double dy;
  double inv_dy;

  int nx;
  int ny;
  int npix;
  int data_sum;
  int *pData;
} twoDhisto_struct;


///////////////////////////////////////////
//generic sorting structure
typedef struct {
  void *pDataStruct;         //pointer to a data structure
  double sorting_variable;   //variable which we will use to sort the structures
} sorting_struct;

typedef struct {
  int array_length;          //actual length of the array pointed to by pSorting
  int nItems;                //number of sorting_structs currently in the pSorting array
  sorting_struct *pSorting;  //pointer to array of sorting_structs
} sortinglist_struct;
///////////////////////////////////////////

//----------------------------------------------------------------------------------------------
//-- public function declarations

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// General purpose functions
int    util_print_version                          (FILE* pFile);
int    util_select_option_from_string              (const int noptions,  const char** plist_names, const int *plist_values,  const char *str_value, int *presult);
int    util_isort                                  (unsigned long n, int *pdat);
int    util_usort                                  (unsigned long n, unsigned int *pdat);
int    util_fsort                                  (unsigned long n, float *pdat);
int    util_dsort                                  (unsigned long n, double *pdat);
int    util_fisort                                 (unsigned long n, float *pdat, int *pcdat);
int    util_disort                                 (unsigned long n, double *pdat, int *pcdat);
int    util_dusort                                 (unsigned long n, double *pdat, unsigned int *pcdat);
int    util_dlsort                                 (unsigned long n, double *pdat, long *pcdat);
int    util_dUsort                                 (unsigned long n, double *pdat, unsigned long *pcdat);
int    util_dfsort                                 (unsigned long n, double *pdat, float *pcdat);
int    util_ddsort                                 (unsigned long n, double *pdat, double *pcdat);
int    util_dddsort                                (unsigned long n, double *pdat, double *pc1dat, double* pc2dat);
int    util_ddisort                                (unsigned long n, double *pdat, double *pcdat, int *pc1dat);
int    util_fiisort                                (unsigned long n, float *pdat, int *pcdat, int *pc1dat);

int    util_diisort                                (unsigned long n, double *pdat, int *pcdat, int *pc1dat);
int    util_dllsort                                (unsigned long n, double *pdat, long *pcdat, long *pc1dat);
int    util_dffsort                                (unsigned long n, double *pdat, float *pcdat, float *pc1dat);

int    util_int2bitstr_LE                          (const unsigned int val, const unsigned int len, char* presult);
int    util_int2bitstr_BE                          (const unsigned int val, const unsigned int len, char* presult);
int    util_int2bitstr_dots_LE                     (const unsigned int val, const unsigned int len, const unsigned int max_len, char *presult);
long   util_hash_file                              (char *str, const unsigned long max_bytes);
long   util_srand                                  (long seed_val);

int util_solve_Mx_V( double *inM, double *inV, double *outV, unsigned int N);
int util_solve_Mx_V_3x3( double *inM, double *inV, double *outV);

//these ones act only on individual values
float  util_interp_linear_1D_float  (float x1,  float y1,  float x2,  float y2,  float x);
double util_interp_linear_1D_double (double x1, double y1, double x2, double y2, double x);
int    util_interp_linear_1D_floats (float*  px,     float* py,     long n,        float x,    float* y );
int    util_interp_linear_1D_doubles(double* px,     double* py,    long n,        double x,   double* y );
double util_interp_linear_2D_doubles(double  param1, double param2, double *list1, double *list2, double *array, long len1, long len2);
//these ones act on arrays - from NR interpolation routines
//int    util_cubic_spline_1D_doubles (double* x,      double* y,     long n,        double* y2 );
//int    util_cubic_splint_1D_doubles (double* xa,     double* ya,    double* y2a,   long n,     double x, double* y );
//int    util_cubic_spline_1D_floats  (float*  x,      float* y,      long n,        float* y2 );
//int    util_cubic_splint_1D_floats  (float*  xa,     float* ya,     float* y2a,    long n,     float x, float* y );

int util_cubic_spline_1D_prep_doubles(long n, double *px, double *py, double *py2 );
int util_cubic_spline_1D_calc_double (long n, double *px, double *py, double *py2,             double x_out,   double *py_out);
int util_cubic_spline_1D_calc_doubles(long n, double *px, double *py, double *py2, long n_out, double *px_out, double *py_out);

int util_cubic_spline_1D_prep_floats (long n, float *px, float *py, float *py2 );
int util_cubic_spline_1D_calc_float  (long n, float *px, float *py, float *py2,             float x_out,   float *py_out);
int util_cubic_spline_1D_calc_floats (long n, float *px, float *py, float *py2, long n_out, float *px_out, float *py_out);



float  util_nanoLambert_to_magsqarcsec             (float flux);
float  util_magsqarcsec_to_nanoLambert             (float mag);
double util_airmass_lookup(double Z);

double util_airmass_secz                           (double Z);
float  util_airmass_seczf                          (float Z);
double util_airmass_rozenberg66                    (double Z);
double util_airmass_kriscicunas                    (double Z);
int    util_print_version_and_inputs               (FILE* pstream, char **argv, int argc);
int    util_make_directory                         (const char *str_dirname);
//int    util_shuffle_array                          (unsigned long n, int datatype, void *pdata, double *ptemp_array);
//int    util_shuffle_2arrays                        (unsigned long n, int datatype, void *pdata1, void *pdata2, double *ptemp_array);
//int    util_shuffle_matches_array                  (unsigned long n, int datatype, float *pdata, void *parray);
//int    util_shuffle_matches_2arrays                (unsigned long n, int datatype, float *pdata, void *parray1, void *parray2);

int    util_Fisher_Yates_shuffle_array             (unsigned long length, size_t datasize, void *pArray);
int    util_Fisher_Yates_shuffle_2arrays           (unsigned long length, size_t datasize1, void *pArray1, size_t datasize2, void *pArray2);
int    util_Fisher_Yates_shuffle_3arrays           (unsigned long length, size_t datasize1, void *pArray1, size_t datasize2, void *pArray2, size_t datasize3, void *pArray3);
int    util_shuffle_matches_array                  (unsigned long length, size_t datasize0, void *pArray0, size_t datasize1, void *pArray1);
int    util_shuffle_matches_2arrays                (unsigned long length, size_t datasize0, void *pArray0, size_t datasize1, void *pArray1, size_t datasize2, void *pArray2);

int    util_normalise_and_linear_clip_distribution (int num, float *pData, float limit_lo, float limit_hi);
double util_rand_gaussian                          (double mean, double sigma);
float  util_rand_gaussianf                         (float mean, float sigma);
int    util_write_cd_line_to_plotfile              (FILE* pPlotfile, const char* str_dir, const char* str_prefix);


double util_calc_travel_time                       (double dist,double max_speed, double acceleration);

double util_calc_diurnal_arc                       (double dec, double lat, double airmass);
int    util_run_gnuplot_on_plotfile                (const char* str_plotfile, BOOL use_thread);
int    util_gzip_file                              (const char* str_filename, BOOL use_thread);

int    twoDhisto_struct_init                       (twoDhisto_struct *pHisto);
int    twoDhisto_struct_add_count                  (twoDhisto_struct *pHisto, double dx, double dy );
int    twoDhisto_struct_write_to_file              (twoDhisto_struct *pHisto, const char *str_filename );
                                                  
int    sorting_struct_init                         (sorting_struct *pSorting);
int    sortinglist_struct_clear_items              (sortinglist_struct *pSortingList);
int    sortinglist_struct_init                     (sortinglist_struct *pSortingList, int num_items);
int    sortinglist_struct_add_item                 (sortinglist_struct *pSortingList, void *pDataStruct, double sorting_variable);
int    sortinglist_struct_free                     (sortinglist_struct *pSortingList);
int    sorting_struct_compare_standard             (const void *pSort1, const void *pSort2);
int    sorting_struct_compare_reverse              (const void *pSort1, const void *pSort2);
int    sortinglist_struct_sort                     (sortinglist_struct *pSortingList,  BOOL reverse);

long   util_est_num_lines_in_named_file            (const char* str_filename, const long num_sample_lines);

int    util_binary_search_double                   (long nrows, double *parray, double value, long *pimin, long *pimax); 
int    util_binary_search_float                    (long nrows, float  *parray, float  value, long *pimin, long *pimax); 
int    util_binary_search_interp_double            (long nrows, double *parray, double value, double *pimid);
int    util_binary_search_interp_float             (long nrows, float  *parray, float  value, float  *pimid);

//the following do not require arrays to be pre-sorted
int    util_unsorted_get_index_of_nearest_double   (long nrows, double *parray, double value, long *pindex);
int    util_unsorted_binary_search_double          (long nrows, double *parray, double value, long *pimin, long *pimax );

double util_integrate_gaussian_over_interval       (double x0, double sigma, double xmin, double xmax);

double util_lngamma_func                           (double xx);
long   util_poisson_rand                           (double lambda);
int    util_uniqueify_i                            (unsigned long n, int  *pdata, unsigned long *pnunique);
int    util_uniqueify_u                            (unsigned long n, uint *pdata, unsigned long *pnunique);

#endif

