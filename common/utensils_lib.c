// -*-mode:C; compile-command:"make -f utensils_lib.make"; -*- 
//----------------------------------------------------------------------------------
//--
//--    Filename: utensils_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
#define CVS_REVISION "$Revision: 1.12 $"
#define CVS_DATE     "$Date: 2015/06/25 14:23:14 $"
//--    Description:
//--    Use: This module is for general purpose utensil functions 
//--         
//--
//--

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <sys/stat.h>   //needed for mkdir() 
#include <sys/types.h>  //needed for mkdir()
//#include <sys/syscall.h>
#include <unistd.h>
#include <time.h>

#include "utensils_lib.h"

#define ERROR_PREFIX   "#-utensils_lib.c : Error   :"
#define WARNING_PREFIX "#-utensils_lib.c : Warning :"
#define COMMENT_PREFIX "#-utensils_lib.c : Comment :"
#define DEBUG_PREFIX   "#-utensils_lib.c : DEBUG   :"

//recommendation is to use rand() rather than drand48()
//#define UTIL_DRAND ((double)(random()*(1.0 / ((double)RAND_MAX + 1))))
#define UTIL_DRAND (drand48())

////////////////////////////////////////////////////////////////////////
//functions

int util_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}

//select an integer value from a list of possible options
//case insensitive
int util_select_option_from_string (const int noptions,
                               const char** plist_names,
                               const int *plist_values,
                               const char *str_value,
                               int *presult)
{
  int i;
  int len;
  if ( plist_names  == NULL ||
       plist_values == NULL ) return -1;

  len = (int) strlen(str_value);
  for(i=0; i< noptions; i++)
  {
    if (strncasecmp((const char*) str_value, (const char*) plist_names[i],  (int) len) == 0 )
    {
      *presult = (int) plist_values[i];
      return 0;
    }
  }
  return 1;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
///sort on ints
int util_isort(unsigned long n, int *pdat)
{
  unsigned long i, ir, j, l;
  int data;
  int *pdata = pdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
    }
    else
    {
      data = pdata[ir];
      pdata[ir] = pdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
///sort on unsigned ints
int util_usort(unsigned long n, unsigned int *pdat)
{
  unsigned long i, ir, j, l;
  unsigned int data;
  unsigned int *pdata = pdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
    }
    else
    {
      data = pdata[ir];
      pdata[ir] = pdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
///sort on floats
int util_fsort(unsigned long n, float *pdat)
{
  unsigned long i, ir, j, l;
  float data;
  float *pdata = pdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
    }
    else
    {
      data = pdata[ir];
      pdata[ir] = pdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
///sort on doubles
int util_dsort(unsigned long n, double *pdat)
{
  unsigned long i, ir, j, l;
  double data;
  double *pdata = pdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
    }
    else
    {
      data = pdata[ir];
      pdata[ir] = pdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
///sort on floats , but preserve ints ...useful if want to keep the old index of the data column
int util_fisort(unsigned long n, float *pdat, int *pcdat)
{
  unsigned long i, ir, j, l;
  float data;
  int cdata;
  float *pdata = pdat - 1;
  int  *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
int util_disort(unsigned long n, double *pdat, int *pcdat)
{
  unsigned long i, ir, j, l;
  double data;
  int cdata;
  double *pdata = pdat - 1;
  int  *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
int util_dusort(unsigned long n, double *pdat, unsigned int *pcdat)
{
  unsigned long i, ir, j, l;
  double data;
  unsigned int cdata;
  double *pdata = pdat - 1;
  unsigned int  *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
int util_dlsort(unsigned long n, double *pdat, long *pcdat)
{
  unsigned long i, ir, j, l;
  double data;
  long cdata;
  double *pdata = pdat - 1;
  long  *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
int util_dUsort(unsigned long n, double *pdat, unsigned long *pcdat)
{
  unsigned long i, ir, j, l;
  double data;
  unsigned long cdata;
  double *pdata = pdat - 1;
  unsigned long  *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
int util_dfsort(unsigned long n, double *pdat, float *pcdat)
{
  unsigned long i, ir, j, l;
  double data;
  float cdata;
  double *pdata = pdat - 1;
  float  *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
int util_ddsort(unsigned long n, double *pdat, double *pcdat)
{
  unsigned long i, ir, j, l;
  double data;
  double cdata;
  double *pdata = pdat - 1;
  double *pcdata = pcdat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////
///also sort two other arrays of data: double version
int util_dddsort(unsigned long n, double *pdat,  double *pcdat,  double *pc1dat)
{
  unsigned long i, ir, j, l;
  double data;
  double cdata;
  double c1data;
  double *pdata = pdat - 1;
  double *pcdata = pcdat - 1;
  double *pc1data = pc1dat - 1;

  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
      c1data=pc1data[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      c1data = pc1data[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      pc1data[ir] = pc1data[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        pc1data[1] = c1data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        pc1data[i]=pc1data[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
    pc1data[i]=c1data;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
///also sort two other arrays of data: float version
int util_dffsort(unsigned long n, double *pdat,  float *pcdat,  float *pc1dat)
{
  unsigned long i, ir, j, l;
  double data;
  float cdata;
  float c1data;
  double *pdata = pdat - 1;
  float *pcdata = pcdat - 1;
  float *pc1data = pc1dat - 1;

  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
      c1data=pc1data[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      c1data = pc1data[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      pc1data[ir] = pc1data[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        pc1data[1] = c1data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        pc1data[i]=pc1data[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
    pc1data[i]=c1data;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///also sort two other arrays of data: int version
int util_diisort(unsigned long n, double *pdat, int *pcdat, int *pc1dat)
{
  unsigned long i, ir, j, l;
  double data;
  int cdata;
  int c1data;
  double *pdata = pdat - 1;
  int *pcdata = pcdat - 1;
  int *pc1data = pc1dat - 1;

  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
      c1data=pc1data[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      c1data = pc1data[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      pc1data[ir] = pc1data[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        pc1data[1] = c1data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        pc1data[i]=pc1data[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
    pc1data[i]=c1data;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///also sort two other arrays of data: int version
int util_dllsort(unsigned long n, double *pdat, long *pcdat, long *pc1dat)
{
  unsigned long i, ir, j, l;
  double data;
  long cdata;
  long c1data;
  double *pdata = pdat - 1;
  long *pcdata = pcdat - 1;
  long *pc1data = pc1dat - 1;

  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
      c1data=pc1data[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      c1data = pc1data[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      pc1data[ir] = pc1data[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        pc1data[1] = c1data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        pc1data[i]=pc1data[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
    pc1data[i]=c1data;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
///also sort another array of data: twos sets of doubles with ints ...useful if want to keep the old index of the data column
int util_ddisort(unsigned long n, double *pdat, double *pcdat, int *pc1dat)
{
  unsigned long i, ir, j, l;
  double data;
  double cdata;
  int c1data;
  double *pdata = pdat - 1;
  double *pcdata = pcdat - 1;
  int    *pc1data = pc1dat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
      c1data=pc1data[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      c1data = pc1data[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      pc1data[ir] = pc1data[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        pc1data[1] = c1data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        pc1data[i]=pc1data[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
    pc1data[i]=c1data;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
///sort on floats , but preserve two arrays of ints ...useful if want to keep the old index of the data column
int util_fiisort(unsigned long n, float *pdat, int *pcdat, int *pc1dat)
{
  unsigned long i, ir, j, l;
  float data;
  int cdata;
  int c1data;
  float *pdata = pdat - 1;
  int  *pcdata = pcdat - 1;
  int  *pc1data = pc1dat - 1;
 
  if ( n < 2 ) return 0;
  l = (n >> 1)+1; 
  ir=n;
  for(;;)
  {
    if (l > 1 )
    {
      data=pdata[--l];
      cdata=pcdata[l];
      c1data=pc1data[l];
    }
    else
    {
      data = pdata[ir];
      cdata = pcdata[ir];
      c1data = pc1data[ir];
      pdata[ir] = pdata[1];
      pcdata[ir] = pcdata[1];
      pc1data[ir] = pc1data[1];
      if(--ir == 1 )
      {
        pdata[1] = data;
        pcdata[1] = cdata;
        pc1data[1] = c1data;
        break; //out of for loop
      }
    }//else
    i = l;
    j = l + l;
    while (j <= ir)
    {
      if (j < ir && pdata[j] < pdata[j+1]) j++;
      if ( data < pdata[j])
      {
        pdata[i]=pdata[j];
        pcdata[i]=pcdata[j];
        pc1data[i]=pc1data[j];
        i=j;
        j <<= 1;
      }
      else break;
    }
    pdata[i]=data;
    pcdata[i]=cdata;
    pc1data[i]=c1data;
  }
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//calling function must ensure that presult buffer is at least len+1 long
int util_int2bitstr_LE(const unsigned int val, const unsigned int len, char *presult)
{
  unsigned int i;
  if ( presult == NULL ) return 1;
  for (i=0;i<len;i++) presult[i] = (char) (((val >> i) & ((unsigned int ) 1)) != 0 ? '1' : '0');
  presult[len] = '\0';
  return 0;
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
int util_int2bitstr_BE(const unsigned int val, const unsigned int len, char *presult )
{
  unsigned int i;
  if ( presult == NULL ) return 1;
  for (i=0;i<len;i++) presult[len-i-1] = (char) (((val >> i) & ((unsigned int ) 1)) != 0 ? '1' : '0');
  presult[len] = '\0';
  return 0;
}
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//calling function must ensure that presult buffer is at least max_len+1 long
int util_int2bitstr_dots_LE(const unsigned int val, const unsigned int len, const unsigned int max_len, char *presult)
{
  unsigned int i;
  if ( presult == NULL ) return 1;
  for (i=0;i<len;i++) presult[i] = (char) (((val >> i) & ((unsigned int ) 1)) != 0 ? '1' : '0');
  for (i=len;i<max_len;i++) presult[i] = (char) '.';
  presult[i] = '\0';
  return 0;
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
long util_hash_file(char *str, const unsigned long max_bytes)
{
  FILE *pfile = NULL;
  unsigned long hash = 0;
  unsigned long temp;
  unsigned int i;
  char buffer[5];
  unsigned long total = 0;
  if ((pfile = fopen(str, "r")) == NULL)
  {
    fprintf(stderr, "Eh? serious problem cos couldn't open %s\n", str);
    return 0;
  }
  while ( fgets(buffer, 5, pfile) )
  {
    //    printf ("0x%08X -%4s- : ", seed_val,  buffer);
    for (i = 0; i < (unsigned int) strlen(buffer) ; i++)
    {
      temp = ((unsigned long) buffer[i]) << (i * 8);
      //      temp = (long) temp << (i * 8);
      hash += (unsigned long) temp;
      //      printf ("0x%08X ", temp);
    }
    total += 5;
    if ( total >= max_bytes && max_bytes > 0 ) break;
    //    printf ("0x%08X \n", seed_val);
  }
  (void) fclose(pfile);
  return (long) hash;
}
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////
///new version which uses the contents of /proc/stat and /proc/keys to generate a randseed
//make a hash of the contents to generate a ulong
// if supplied seed_val is 0 then generate a seed from /dev/urandom
long util_srand (long seed_val)
{
  int counter = 0;
  long local_seed_val = seed_val;
  //  char *str_fname = "/proc/stat";

  //  FILE *pfile = NULL;
  while ( local_seed_val == 0 )
  {
    local_seed_val = (long) util_hash_file("/dev/urandom", (unsigned long) 256);

    //make sure that we don't keep on going around this loop forever.
    counter ++;
    if ( counter > 100 )
    {
      return 0;
    }
  }
  srand48(local_seed_val); //seed with the value
  srandom((uint) local_seed_val); //seed with a uint version of the value
  fprintf(stdout, "%s Seeding the drand48() random number generator with this seed (long): %ld\n",
          COMMENT_PREFIX, (long) local_seed_val);
  fprintf(stdout, "%s Seeding the random()  random number generator with this seed (uint): %u\n",
          COMMENT_PREFIX, (uint) local_seed_val);
  return (long) local_seed_val;
}
////////////////////////////////////////

float  util_interp_linear_1D_float (float x1, float y1, float x2, float y2, float x)
{
  float x2mx1 = x2 - x1;
  if ( FLT_ISNOTZERO(x2mx1) )
    return (float) ((y1 + (x - x1)*(y2 - y1)/(x2mx1)));
  else
    return (float) ((y1 + y2) * (float) 0.5);
}

double util_interp_linear_1D_double (double x1, double y1, double x2, double y2, double x)
{
  double x2mx1 = x2 - x1;
  if ( DBL_ISNOTZERO(x2mx1) )
    return (double) ((y1 + (x - x1)*(y2 - y1)/(x2mx1)));
  else
    return (double) ((y1 + y2) * (double) 0.5);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//linear interpolation from given table of x-y values - assume that already sorted with x-ascending
int util_interp_linear_1D_floats(float* px, float* py, long n, float x, float* y )
{
  int ret_val = 0;
  float result;
  
  if ( px == NULL || py == NULL || y == NULL ) return 1;
  if ( n <= 0 )
  {
    result = 0.0;
    ret_val = 1;
  }
  else if ( n == 1 )
  {
    result = py[0];
    ret_val = 2;
  }
  else if ( x == px[0])  { result = py[0];   ret_val = 0; }
  else if ( x == px[n-1]){ result = py[n-1]; ret_val = 0; }
  else if ( x < px[0]) //extrapolation below range
  {
    result = util_interp_linear_1D_float(px[0], py[0], px[1], py[1], x);
    ret_val = 2;
  }
  else if ( x > px[n - 1]) //extrapolation above range
  {
    result = util_interp_linear_1D_float(px[n-2], py[n-2], px[n-1], py[n-1], x);
    ret_val = 2;
  }
  else
  {
    //find bounding pair of values - improve this with a geometric routine
    long i = 0;
    while (px[i] < x ) i++;
    result = util_interp_linear_1D_float(px[i-1], py[i-1], px[i], py[i], x);
    ret_val = 0;
  }
  
  *y = result;
  return ret_val;
}



//linear interpolation from given table of x-y values - assume that already sorted with x-ascending
int util_interp_linear_1D_doubles(double* px, double* py, long n, double x, double* y )
{
  int ret_val = 0;
  double result;
  //  fprintf(stdout, "Got to line %d in file %s\n", __LINE__, __FILE__); fflush(stdout);
  //  fprintf(stdout, "px=0x%x py=0x%x n=%d x=%f y=0x%x\n", px, py, n, x, y ); fflush(stdout);
  
  if ( px == NULL || py == NULL || y == NULL ) return 1;
  if ( n <= 0 )
  {
    result = 0.0;
    ret_val = 1;
  }
  else if ( n == 1 )
  {
    result = py[0];
    ret_val = 2;
  }
  else if ( x == px[0])  { result = py[0];   ret_val = 0; }
  else if ( x == px[n-1]){ result = py[n-1]; ret_val = 0; }
  else if ( x < px[0]) //extrapolation below range, use section between [0] and [1] as guide
  {
    result = util_interp_linear_1D_double(px[0], py[0], px[1], py[1], x);
    ret_val = 2;
  }
  else if ( x > px[n - 1]) //extrapolation above range
  {
    result = util_interp_linear_1D_double(px[n-2], py[n-2], px[n-1], py[n-1], x);
    ret_val = 2;
  }
  else
  {
    //find bounding pair of values
    long i = 0;
    while (px[i] < x ) i++;
    result = util_interp_linear_1D_double(px[i-1], py[i-1], px[i], py[i], x);
    ret_val = 0;
  }
  
  *y = result;
  return ret_val;
}




//--------------------------------------------------------------------------------------
// util_solve_Mx_V
// find the solution to M.x = V by inverting M
int util_solve_Mx_V( double *inM, double *inV, double *outV, unsigned int N )
{
  int i,j, k;
  double *matrix;
  double Mji_over_Mii;
  if (matrix = (double*) malloc(sizeof(double) * N * (N+1)))
  {
    for (i = 0; i < N; i++)   //initialise from arguments
    {
      for (k = 0; k < N; k++)
        matrix [i*(N + 1) + k] = inM[i*N + k];
      matrix [i*(N + 1) + N] = inV[i];
    }
    #ifdef UTENSILS_DEBUG
    fprintf(stdout, "Input matrix and vector\n");
    print_matrix(matrix, N*(N+1), N+1);
    #endif

    for (i = 0; i < N; i++)              //subtracting row number
      for (j = 0; j < N; j++)            //subtractee row number
      {
        if ( i != j )
        {
          if (matrix[i*(N + 1) + i])               //avoid dividing by 0
          {
            Mji_over_Mii = matrix[j*(N + 1) + i] / matrix[i*(N + 1) + i];
            for (k = 0; k < (N + 1); k++)   //column number
              matrix [j*(N + 1) + k] -= matrix[i*(N + 1) + k] * Mji_over_Mii;
          }
          #ifdef UTENSILS_DEBUG
          fprintf(stdout, "Row%d - %8.4g * Row%d\n", j+1, Mji_over_Mii,i+1);
          print_matrix(matrix, N*(N+1), N+1);
          #endif
        }//if
      }//for j
    for(i = 0; i < N; i++)
    {
      if ( matrix[i*(N + 1) + i] == 0. )
      {
        free(matrix);
        return 1;               //avoid dividing by 0
      }
      else outV[i] = matrix[i*(N + 1) + N] / matrix [i*(N + 1) + i];
    }
      #ifdef UTENSILS_DEBUG
      fprintf(stdout, "Output vector\n");
      print_matrix(outV, N, 1);
      #endif
    free(matrix);
    return 0;
  }//if
  else return 1;
}//util_solve_Mx_V

//--------------------------------------------------------------------------------------
// util_solve_Mx_V_3x3
// find the solution to M.x = V by inverting M
int util_solve_Mx_V_3x3( double *inM, double *inV, double *outV)
{
  int i,j, k;
  double matrix[12];
  double Mji_over_Mii;
  for (i = 0; i < 3; i++)   //initialise from arguments
  {
    for (k = 0; k < 3; k++)
      matrix [i*4 + k] = inM[i*3 + k];
    matrix [i*4 + 3] = inV[i];
  }
  for (i = 0; i < 3; i++)              //subtractor row number
  {
    for (j = 0; j < 3; j++)            //subtractee row number
    {
      if ( i != j )
      {
        if (matrix[i*4 + i])               //avoid dividing by 0
        {
          Mji_over_Mii = matrix[j*4 + i] / matrix[i*4 + i];
          for (k = 0; k < 4; k++)   //column number
            matrix [j*4 + k] -= matrix[i*4 + k] * Mji_over_Mii;
        }
      }//if
    }//for j
  }
  for(i = 0; i < 3; i++)
  {
    if ( matrix[i*4 + i] == 0. )
      return 1;               //avoid dividing by 0
    else
      outV[i] = matrix[i*4 + 3] / matrix [i*4 + i];
  }
  return 0;  //return succesfully
}//util_solve_Mx_V_3x3



//----------------------------------------------------------------------------------------------------
//--util_2D_interpolate
//-- this function interpolates linearly in 2D parameter space
//-- assumes that have a  grid of values in (x,y) space
//-- At the nth vertex (i, j), we have  x = list1[i], y = list2[j] and Value = array[i * len2 + j]
//-- assumes that array is of form array[len1][len2] or array [len1 * len2]
double util_interp_linear_2D_doubles(double param1, double param2, double *list1, double *list2, double *array, long len1, long len2)
{
  long low1 = 0, low2 = 0, high1 = 0, high2 = 0;        //indicies of the bounding values
  long i;//, j;
  double matrix [9] = {1.,1.,1., 1.,1.,1., 1.,1.,1.};   //3x3 matrix
  double vect1[3];
  double vect2[3];
  double return_val;
  //find the four list values which bound (param1, param2) (don't assume that lists are in order)
  for (i = 0; i < len1; i ++)
  {
    if (list1[i] < param1)
      if ( fabs(param1 - list1[i]) < fabs(param1 - list1[low1])) low1=i;
    if (list1[i] > param1)
      if ( fabs(param1 - list1[i]) < fabs(param1 - list1[high1])) high1=i;   
  }
  for (i = 0; i < len2; i ++)
  {
    if (list2[i] < param2)
      if ( fabs(param2 - list2[i]) < fabs(param2 - list2[low2])) low2=i;
    if (list2[i] > param2)
      if ( fabs(param2 - list2[i]) < fabs(param2 - list2[high2])) high2=i;

  }
  if (param1 <= list1[0])
  {
    low1 = 0;
    high1 = 0;
  }
  if (param2 <= list2[0])
  {
    low2 = 0;
    high2 = 0;
  }
  if (param1 >= list1[len1 - 1])
  {
    low1 = len1 - 1;
    high1 = len1 - 1;
  }
  if (param2 >= list2[len2 - 1])
  {
    low2 = len2 - 1;
    high2 = len2 - 1;
  }

  vect1[0]  = array[low1 * len2 + low2];  //assuming (param1, param2) is in lower triangle
  vect1[1]  = array[low1 * len2 + high2];
  vect1[2]  = array[high1 * len2 + low2];
  matrix[0] = list1[low1];                //assuming (param1, param2) is in lower triangle
  matrix[1] = list2[low2];                //
  matrix[3] = list1[low1];  
  matrix[4] = list2[high2];
  matrix[6] = list1[high1];  
  matrix[7] = list2[low2];
  //now see if (param1, param2) is in upper triangle 
  if ( ((list2[high2] - list2[low2]) * param1 + list1[low1]*list2[low2] - list1[high1]*list2[high2])/(list1[low1] - list1[high1]) < param2 )
  {
    vect1[0]   = array[high1 * len2 + high2];
    matrix[0] = list1[high1];  
    matrix[1] = list2[high2];
  }

  if (util_solve_Mx_V_3x3(matrix, vect1, vect2))                 //interpolate the three values
    return_val = (vect1[0] + vect1[1] + vect1[2]) / 3.0;          //couldn't interpolate, so just take an average
  else
    return_val = param1*vect2[0] + param2*vect2[1] + vect2[2];   //final output value


  //debug output
  //fprintf(stdout, "param1=%12g param2=%12g bounds= %5g %5g %5g %5g boundvals= %12g %12g %12g return=%12g\n", param1, param2, list1[low1], list1[high1], list2[low2], list2[high2], vect1[0], vect1[1], vect1[2], return_val);

  
  return return_val;
}//util_interp_linear_2D_doubles


/////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
//from numerical recipies
//call this one once, then call util_cubic_splint for each interpolated value.
//converted to work with zero based arrays
//assumed that input arrays are presorted in x coordinate
//n is the length of the arrays
//px is an array with n entries giving the x coordinate of the sample points to be interpolated from
//py is an array with n entries giving the y coordinate of the sample points to be interpolated from
//py2 either points to an array, with enough space to hold n output values 
//or if py2 == NULL then we will allocate space, and it will be up to the calling routine to free the space later

int util_cubic_spline_1D_prep_doubles(long n, double *px, double *py, double *py2 )
{
  double yp1, ypn;
  long i,k;
  double p, qn, sig, un;
  double *pu = NULL;  //temporary array
  if ( px == NULL ||
       py == NULL || 
       n<2 ) return 2;
  
  if ( py2 == NULL )
  {
    if ( (py2 = (double*) malloc(sizeof(double) *  n)) == NULL)
    {
      return 1;
    }
  }

  if ( (pu = (double*) malloc(sizeof(double) *  (n-1))) == NULL)
  {
    return 1;
  }


  //my extra bit
  //work out end gradients from last two points only
  if (DBL_CMP(px[1],px[0])) yp1 = DBL_MAX;
  else                      yp1 = (py[1] - py[0])/(px[1]-px[0]);

  if (DBL_CMP(px[n-1],px[n-2])) ypn = DBL_MAX;
  else                          ypn = (py[n-1] - py[n-2])/(px[n-1]-px[n-2]);

  if ( yp1 >= DBL_MAX )
  {
    py2[0] = 0.0;
    pu[0]  = 0.0;
  }
  else
  {
    py2[0] = -0.5;
    pu[0] = (3.0/(px[1]-px[0]))*((py[1]-py[0])/(px[1]-px[0])-yp1);
  }

  for(i=1;i<n-2;i++)
  {
    sig    = (px[i]-px[i-1])/(px[i+1]-px[i-1]);
    p      = sig*py2[i-1]+2.0;
    py2[i] = (sig-1.0)/p;
    pu[i]  = (py[i+1]-py[i])/(px[i+1]-px[i]) - (py[i]-py[i-1])/(px[i]-px[i-1]);
    pu[i]  = (6.0*pu[i]/(px[i+1]-px[i-1])-sig*pu[i-1])/p;
  }

  if ( ypn >= DBL_MAX )
  {
    qn = 0.0;
    un = 0.0;
  }
  else
  { 
    qn = 0.5;
    un = (3.0/(px[n-1]-px[n-2]))*(ypn-(py[n-1]-py[n-2])/(px[n-1]-px[n-2]));
  }
  py2[n-1]=(un-qn*pu[n-2])/(qn*py2[n-2]+1.0);
  for(k=n-2;k>=0;k--)
  {
    py2[k] = py2[k]*py2[k+1]+pu[k];
  }

  if ( pu) free(pu); pu = NULL;
  return 0;
}

//see above, converted to zero based arrays
//n is the length of the input arrays
//px is an array with n entries giving the x coordinate of the sample points to be interpolated from
//py is an array with n entries giving the y coordinate of the sample points to be interpolated from
//py2 points to an array that has already been filled with intermediate gradients by util_cubic_spline_1D_doubles
//x_out is the single x coordinate of the requested interpolated value 
//py_out will hold the single y coordinate of the interpolated value 
int util_cubic_spline_1D_calc_double(long n, double *px, double *py, double *py2, double x_out, double *py_out )
{
  long klo, khi, k;
  double h,b,a;
  if ( px == NULL ||
       py == NULL || 
       py2 == NULL || 
       py_out == NULL || 
       n<2 ) return 2;

  klo=1;
  khi=n;
  while(khi-klo > 1)
  {
    k = (khi+klo) >> 1;
    if (px[k-1] > x_out ) khi=k;
    else klo=k;
  }
  h=px[khi-1] - px[klo-1];
  if ( DBL_ISZERO(h) )
  {
    *py_out = NAN;
    return 1;
  }
  a = (px[khi-1] - x_out)/h;
  b = (x_out - px[klo-1])/h;
  *py_out = a*py[klo-1]
    +  b*py[khi-1]
    +  ((a*(a*a - 1.0)) * py2[klo-1]+(b*(b*b - 1.0))*py2[khi-1])*h*h/6.0;

  return ( x_out < px[0] || x_out > px[n-1] ? 2 : 0);
}

//n is the length of the input arrays
//px is an array with n entries giving the x coordinate of the sample points to be interpolated from
//py is an array with n entries giving the y coordinate of the sample points to be interpolated from
//py2 points to an array that has already been filled with intermediate gradients by util_cubic_spline_1D_doubles
//n_out is the number of interpolated points to calculate
//px_out is an array of x coordinates of the requested interpolated values 
//py_out will hold the array of y coordinates of the interpolated values 
int util_cubic_spline_1D_calc_doubles( long n, double *px, double *py, double *py2, long n_out, double *px_out, double *py_out )
{
  long i;
  for (i = 0; i< n_out; i++)
  {
    int status;
    if ((status = util_cubic_spline_1D_calc_double((long) n, (double*) px, (double*) py, (double*) py2,
                                                   (double) px_out[i], (double*) &(py_out[i]))))
      return status;
  }
  return 0;
}


//////////FLOATS version ////////////////////////////////
//from numerical recipies
//call this one once, then call util_cubic_splint for each interpolated value.
//converted to work with zero based arrays
//assumed that input arrays are presorted in x coordinate
//n is the length of the arrays
//px is an array with n entries giving the x coordinate of the sample points to be interpolated from
//py is an array with n entries giving the y coordinate of the sample points to be interpolated from
//py2 either points to an array, with enough space to hold n output values 
//or if py2 == NULL then we will allocate space, and it will be up to the calling routine to free the space later

int util_cubic_spline_1D_prep_floats(long n, float *px, float *py, float *py2 )
{
  float yp1, ypn;
  long i,k;
  float p, qn, sig, un;
  float *pu = NULL;  //temporary array
  if ( px == NULL ||
       py == NULL || 
       n<2 ) return 2;
  
  if ( py2 == NULL )
  {
    if ( (py2 = (float*) malloc(sizeof(float) *  n)) == NULL)
    {
      return 1;
    }
  }

  if ( (pu = (float*) malloc(sizeof(float) *  (n-1))) == NULL)
  {
    return 1;
  }


  //my extra bit
  //work out end gradients from last two points only
  if (DBL_CMP(px[1],px[0])) yp1 =  DBL_MAX;
  else                      yp1 = (py[1] - py[0])/(px[1]-px[0]);

  if (DBL_CMP(px[n-1],px[n-2])) ypn = DBL_MAX;
  else                          ypn = (py[n-1] - py[n-2])/(px[n-1]-px[n-2]);

  if ( yp1 >= DBL_MAX )
  {
    py2[0] = 0.0;
    pu[0]  = 0.0;
  }
  else
  {
    py2[0] = -0.5;
    pu[0] = (3.0/(px[1]-px[0]))*((py[1]-py[0])/(px[1]-px[0])-yp1);
  }

  for(i=1;i<n-2;i++)
  {
    sig    = (px[i]-px[i-1])/(px[i+1]-px[i-1]);
    p      = sig*py2[i-1]+2.0;
    py2[i] = (sig-1.0)/p;
    pu[i]  = (py[i+1]-py[i])/(px[i+1]-px[i]) - (py[i]-py[i-1])/(px[i]-px[i-1]);
    pu[i]  = (6.0*pu[i]/(px[i+1]-px[i-1])-sig*pu[i-1])/p;
  }

  if ( ypn >= DBL_MAX )
  {
    qn = 0.0;
    un = 0.0;
  }
  else
  { 
    qn = 0.5;
    un = (3.0/(px[n-1]-px[n-2]))*(ypn-(py[n-1]-py[n-2])/(px[n-1]-px[n-2]));
  }
  py2[n-1]=(un-qn*pu[n-2])/(qn*py2[n-2]+1.0);
  for(k=n-2;k>=0;k--)
  {
    py2[k] = py2[k]*py2[k+1]+pu[k];
  }

  if ( pu) free(pu); pu = NULL;
  return 0;
}

//see above, converted to zero based arrays
//n is the length of the input arrays
//px is an array with n entries giving the x coordinate of the sample points to be interpolated from
//py is an array with n entries giving the y coordinate of the sample points to be interpolated from
//py2 points to an array that has already been filled with intermediate gradients by util_cubic_spline_1D_floats
//x_out is the single x coordinate of the requested interpolated value 
//py_out will hold the single y coordinate of the interpolated value 
int util_cubic_spline_1D_calc_float(long n, float *px, float *py, float *py2, float x_out, float *py_out )
{
  long klo, khi, k;
  float h,b,a;
  if ( px == NULL ||
       py == NULL || 
       py2 == NULL || 
       py_out == NULL || 
       n<2 ) return 2;

  klo=1;
  khi=n;
  while(khi-klo > 1)
  {
    k = (khi+klo) >> 1;
    if (px[k-1] > x_out ) khi=k;
    else klo=k;
  }
  h=px[khi-1] - px[klo-1];
  if ( DBL_ISZERO(h) )
  {
    *py_out = NAN;
    return 1;
  }
  a = (px[khi-1] - x_out)/h;
  b = (x_out - px[klo-1])/h;
  *py_out = a*py[klo-1]
    +  b*py[khi-1]
    +  ((a*(a*a - 1.0)) * py2[klo-1]+(b*(b*b - 1.0))*py2[khi-1])*h*h/6.0;

  return ( x_out < px[0] || x_out > px[n-1] ? 2 : 0);
}

//n is the length of the input arrays
//px is an array with n entries giving the x coordinate of the sample points to be interpolated from
//py is an array with n entries giving the y coordinate of the sample points to be interpolated from
//py2 points to an array that has already been filled with intermediate gradients by util_cubic_spline_1D_floats
//n_out is the number of interpolated points to calculate
//px_out is an array of x coordinates of the requested interpolated values 
//py_out will hold the array of y coordinates of the interpolated values 
int util_cubic_spline_1D_calc_floats( long n, float *px, float *py, float *py2, long n_out, float *px_out, float *py_out )
{
  long i;
  for (i = 0; i< n_out; i++)
  {
    int status;
    if ((status = util_cubic_spline_1D_calc_float((long) n, (float*) px, (float*) py, (float*) py2,
                                                  (float) px_out[i], (float*) &(py_out[i]))))
      return status;
  }
  return 0;
}






//
////////////////////////////////////////////
////from numerical recipies - simalar to above but done with floats to save space
////call this one once, then call util_cubic_splint_float for each interpolated value.
////converted to work with zero based arrays
//int util_cubic_spline_1D_floats(float* x, float* y, long n, float* y2 )
//{
//  float yp1, ypn;
//  long i,k;
//  float p, qn, sig, un;
//  float *pu = NULL;
//  if ( (pu = (float*) malloc(sizeof(float) *  (n-1))) == NULL)
//  {
//    return 1;
//  }
//
//  if ( n<2 ) return 2; 
//
//  //tom's extra bit
//  //work out end gradients from last two points only
//  yp1 = (y[1] - y[0])/(x[1]-x[0]);
//  ypn = (y[n-1] - y[n-2])/(x[n-1]-x[n-2]);
//  //
//  
//  if ( yp1 >= FLT_MAX )
//  {
//    y2[0] = 0.0;
//    pu[0]  = 0.0;
//  }
//  else
//  {
//    y2[0] = -0.5;
//    pu[0] = (3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-yp1);
//  }
//
//  for(i=1;i<n-2;i++)
//  {
//    sig   = (x[i]-x[i-1])/(x[i+1]-x[i-1]);
//    p     = sig*y2[i-1]+2.0;
//    y2[i] = (sig-1.0)/p;
//    pu[i] = (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
//    pu[i] = (6.0*pu[i]/(x[i+1]-x[i-1])-sig*pu[i-1])/p;
//  }
//
//  if ( ypn >= FLT_MAX )
//  {
//    qn = 0.0;
//    un = 0.0;
//  }
//  else
//  { 
//    qn = 0.5;
//    un = (3.0/(x[n-1]-x[n-2]))*(ypn-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
//  }
//  y2[n-1]=(un-qn*pu[n-2])/(qn*y2[n-2]+1.0);
//  for(k=n-2;k>=0;k--)
//  {
//    y2[k] = y2[k]*y2[k+1]+pu[k];
//  }
//  if ( pu) free(pu); pu = NULL;
//  return 0;
//}
//
////see above, converted to zero based arrays
//int util_cubic_splint_1D_float(float* xa, float* ya, float* y2a, long n, float x, float* y )
//{
//  long klo, khi, k;
//  float h,b,a;
//  klo=1;
//  khi=n;
//  while(khi-klo > 1)
//  {
//    k = (khi+klo) >> 1;
//    if (xa[k-1] > x ) khi=k;
//    else klo=k;
//  }
//  h=xa[khi-1] - xa[klo-1];
//  if (  FLT_ISZERO(h) )
//  {
//    return 1;
//  }
//  a = (xa[khi-1] - x)/h;
//  b = (x - xa[klo-1])/h;
//  *y = a*ya[klo-1]
//    +  b*ya[khi-1]
//    +  ((a*(a*a - 1.)) * y2a[klo-1]+(b*(b*b - 1.))*y2a[khi-1])*h*h/6.0;
//
//  if ( x < xa[0] || x > xa[n-1] ) return 2;
//  return 0;
//}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define UTIL_NUM_AIRMASS_LOOKUP_ENTRIES 9000
double util_airmass_lookup(double Z)
{
  int index;
  static BOOL is_initialised = FALSE;
  static double AM_Z[UTIL_NUM_AIRMASS_LOOKUP_ENTRIES];
  const  double inv_delta_Z = (double) UTIL_NUM_AIRMASS_LOOKUP_ENTRIES  /(double) 90.0;
  if ( Z <= (double)  0.0 ) return (double) 1.0;
  if ( Z >= (double) 90.0 ) return DBL_MAX;

  if ( is_initialised == FALSE )
  {
    int i;
    double z = 0.0;
    double delta_Z = (double) 90.0 / (double) UTIL_NUM_AIRMASS_LOOKUP_ENTRIES;
    for (i=0;i<UTIL_NUM_AIRMASS_LOOKUP_ENTRIES;i++)
    {
      AM_Z[i] = util_airmass_kriscicunas(z);
      z+= delta_Z;
    }
    is_initialised = TRUE;
  }
  
  index = (int) (Z * inv_delta_Z);
  return AM_Z[index];
}

//the airmass as a function of the zenith distance
//Z given in degrees, result in airmasses
//from Rozenberg G.Z., 1966, Twilight 
//see Kriscunias & Schaefer 1991, PASP, 103, 1033
double util_airmass_secz(double Z)
{
  double cos_Z;
  if ( Z <  (double) 0.0 ) return (double) 1.0;
  if ( Z >= (double) 90.0 ) return DBL_MAX;
  cos_Z = cosd(Z);
  return (double) (cos_Z > 0. ? 1.0/cos_Z : DBL_MAX);
}
float util_airmass_seczf(float Z)
{
  float cos_Z;
  if ( Z <  (float) 0.0 ) return (float) 1.0;
  if ( Z >= (float) 90.0 ) return FLT_MAX;
  cos_Z = cosf(Z * DEG_TO_RAD);
  return (float) (cos_Z > 0. ? 1.0/cos_Z : FLT_MAX);
}

//the airmass as a function of the zenith distance
//Z given in degrees, result in airmasses
//from Rozenberg G.Z., 1966, Twilight 
//see Kriscunias & Schaefer 1991, PASP, 103, 1033
double util_airmass_rozenberg66(double Z)
{
  double cos_Z;
  if ( Z <  (double) 0.0 ) return (double) 1.0;
  if ( Z >= (double) 90.0 ) return DBL_MAX;
  cos_Z = cosd(Z);
  return (double) 1.0 / (cos_Z + 0.025*exp(-11.0*cos_Z));
}

//the airmass as a function of the zenith distance used for sky brightness calculations
//Z given in degrees, result in airmasses
//see Kriscunias & Schaefer 1991, PASP, 103, 1033
double util_airmass_kriscicunas(double Z)
{
  double sin_Z;
  if ( Z <  (double) 0.0 ) return (double) 1.0;
  if ( Z >= (double) 90.0 ) return DBL_MAX;
  sin_Z = sind(Z);
  return (double) pow(1.0 - 0.96*SQR(sin_Z),-0.5);
}

//from Krisciunas&Schaefer 1991, also Garstang 1989
//yes, that is supposed to be a natural logarithm
float util_nanoLambert_to_magsqarcsec(float flux)
{
  return (float) ((20.7233 - log(flux / 34.08) ) / 0.92104);
}

//from Krisciunas&Schaefer 1991, also Garstang 1989
float util_magsqarcsec_to_nanoLambert(float mag)
{
  return (float) (34.08 * exp(20.7233 - 0.92104 * mag)); 
}


int util_print_version_and_inputs( FILE* pstream, char **argv, int argc)
{
  int i;
  //print out the command line and program details
  //some of these values are set by CVS
  if ( pstream == NULL ) return 1;
  
  fprintf(stdout, "\n%s This is %s compiled from %s (CVS version %s, %s) at %s on %s\n",
          COMMENT_PREFIX,
          argv[0],
          __FILE__,
          CVS_REVISION,
          CVS_DATE,
          __TIME__,
          __DATE__ );

  fprintf(stdout, "%s The command line was: ", COMMENT_PREFIX );
  for ( i=0;i<argc;i++) 
  {
    fprintf(stdout, "%s ", argv[i]);
  }
  fprintf(stdout, "\n");
  (void) fflush(stdout);
  return 0;
}


//make a directory, safe and quiet version
//will not complain if try to open an existing directory
int util_make_directory (const char *str_dirname)
{
  
  if ( strlen(str_dirname) == 0)
  {
    fprintf(stderr, "%s I had problems making the directory: You gave a nonsensical directory name: ->%s<-\n",
            ERROR_PREFIX, str_dirname);
    return 1;
  }
  
  //make the output directory with a mkdir system call - will not clobber existing directory
  if (mkdir(str_dirname,(mode_t)(S_IRWXU|S_IXGRP|S_IXOTH|S_IRGRP|S_IROTH)) == -1)
  {
    switch (errno)
    {
    case EEXIST :  //directory might already exist
      {
        struct stat st;
        //get the status of the directory
        if(stat((const char*) str_dirname,(struct stat*) &st) != 0)
        {
          //error 'stat'ing file
          fprintf(stderr, "%s The requested directory ->%s<- seems to exist but is not stat'able!\n", ERROR_PREFIX, str_dirname);
          return errno;
        }
        else
        {
          if ( S_ISDIR((mode_t)st.st_mode))
          {
            //this is expected - the requested directory exists, and is a directory!
          }
          else
          {
            //this is unexpected - the requested directory exists, but is not a directory!
            fprintf(stderr, "%s The requested directory ->%s<- exists but is not a directory!\n", ERROR_PREFIX, str_dirname);
            (void) fflush(stderr);
            return 1;
          }
        }
        break;
      }
    default :
      fprintf(stderr, "%s I had problems making this directory:->%s<-, the errno was %d\n",
              ERROR_PREFIX, str_dirname, errno);
      (void) fflush(stderr);
      return 1;
      //      break;
    }
  }
  return 0;
}
  
// this should be much more useful, and efficient
// see http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
// length is number of elements in array
// datasize is result of sizeof(datatype)
// pArray contains at least length*datasize bytes
// assumes that srand48() has already been called to seed rand number generator
int util_Fisher_Yates_shuffle_array(unsigned long length, size_t datasize, void *pArray)
{
  unsigned long i;
  unsigned long j;
  BYTE *pSpare = NULL;

  if ( pArray == NULL ) return 1;
  if ( length < 1 ) return 0;

  if ((pSpare = (BYTE*) malloc(sizeof(BYTE)*datasize)) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  for (i=length-1;i>0;i--)
  {
    j = (unsigned long) (UTIL_DRAND*(i+1));
    if ( j != i )
    {
      memcpy((void*) pSpare, (const void*) &(((BYTE*) pArray)[j*datasize]), (size_t) datasize);
      memcpy((void*) &(((BYTE*) pArray)[j*datasize]), (const void*) &(((BYTE*) pArray)[i*datasize]), (size_t) datasize);
      memcpy((void*) &(((BYTE*) pArray)[i*datasize]), (const void*) pSpare, (size_t) datasize);
    }
  }
  if (pSpare) free (pSpare);
  return 0;
}

//now shuffle members of two arrays, retaining original pairing
int util_Fisher_Yates_shuffle_2arrays(unsigned long length, size_t datasize1, void *pArray1, size_t datasize2, void *pArray2)
{
  unsigned long i;
  unsigned long j;
  BYTE* pSpare1 = NULL;
  BYTE* pSpare2 = NULL;

  if ( pArray1 == NULL ||
       pArray2 == NULL ) return 1;

  if ((pSpare1 = (BYTE*) malloc(sizeof(BYTE)*datasize1)) == NULL ||
      (pSpare2 = (BYTE*) malloc(sizeof(BYTE)*datasize2)) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  for (i=length-1;i>0;i--)
  {
    j = (unsigned long) (UTIL_DRAND*(i+1));
    if ( j != i )
    {
      memcpy((void*) pSpare1, (const void*) &(((BYTE*) pArray1)[j*datasize1]), (size_t) datasize1);
      memcpy((void*) &(((BYTE*) pArray1)[j*datasize1]), (const void*) &(((BYTE*) pArray1)[i*datasize1]), (size_t) datasize1);
      memcpy((void*) &(((BYTE*) pArray1)[i*datasize1]), (const void*) pSpare1, (size_t) datasize1);
      
      memcpy((void*) pSpare2, (const void*) &(((BYTE*) pArray2)[j*datasize2]), (size_t) datasize2);
      memcpy((void*) &(((BYTE*) pArray2)[j*datasize2]), (const void*) &(((BYTE*) pArray2)[i*datasize2]), (size_t) datasize2);
      memcpy((void*) &(((BYTE*) pArray2)[i*datasize2]), (const void*) pSpare2, (size_t) datasize2);
    }
  }
  if (pSpare1) free (pSpare1);
  if (pSpare2) free (pSpare2);
  return 0;
}
//now shuffle members of three arrays, retaining original pairing
int util_Fisher_Yates_shuffle_3arrays(unsigned long length, size_t datasize1, void *pArray1, size_t datasize2, void *pArray2, size_t datasize3, void *pArray3)
{
  unsigned long i;
  unsigned long j;
  BYTE* pSpare1 = NULL;
  BYTE* pSpare2 = NULL;
  BYTE* pSpare3 = NULL;

  if ( pArray1 == NULL ||
       pArray2 == NULL ||
       pArray3 == NULL ) return 1;

  if ((pSpare1 = (BYTE*) malloc(sizeof(BYTE)*datasize1)) == NULL ||
      (pSpare2 = (BYTE*) malloc(sizeof(BYTE)*datasize2)) == NULL ||
      (pSpare3 = (BYTE*) malloc(sizeof(BYTE)*datasize3)) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  for (i=length-1;i>0;i--)
  {
    j = (unsigned long) (UTIL_DRAND*(i+1));
    if ( j != i )
    {
      memcpy((void*) pSpare1, (const void*) &(((BYTE*) pArray1)[j*datasize1]), (size_t) datasize1);
      memcpy((void*) &(((BYTE*) pArray1)[j*datasize1]), (const void*) &(((BYTE*) pArray1)[i*datasize1]), (size_t) datasize1);
      memcpy((void*) &(((BYTE*) pArray1)[i*datasize1]), (const void*) pSpare1, (size_t) datasize1);
      
      memcpy((void*) pSpare2, (const void*) &(((BYTE*) pArray2)[j*datasize2]), (size_t) datasize2);
      memcpy((void*) &(((BYTE*) pArray2)[j*datasize2]), (const void*) &(((BYTE*) pArray2)[i*datasize2]), (size_t) datasize2);
      memcpy((void*) &(((BYTE*) pArray2)[i*datasize2]), (const void*) pSpare2, (size_t) datasize2);
      
      memcpy((void*) pSpare3, (const void*) &(((BYTE*) pArray3)[j*datasize3]), (size_t) datasize3);
      memcpy((void*) &(((BYTE*) pArray3)[j*datasize3]), (const void*) &(((BYTE*) pArray3)[i*datasize3]), (size_t) datasize3);
      memcpy((void*) &(((BYTE*) pArray3)[i*datasize3]), (const void*) pSpare3, (size_t) datasize3);
    }
  }
  if (pSpare1) free (pSpare1);
  if (pSpare2) free (pSpare2);
  if (pSpare3) free (pSpare3);
  return 0;
}




// go through the first array of values, for each run of identical values then shuffle the
// correspoinding values in another array (e.g. an array of indices) 
// this relies on a bitwise comparison of values being able to detect identities
// this only really only makes sense to run if the array data has previously been sorted
int util_shuffle_matches_array (unsigned long length, size_t datasize0, void *pArray0, size_t datasize1, void *pArray1)
{
  unsigned long i;
  unsigned long j;
  if ( pArray0 == NULL ||
       pArray1 == NULL ) return 1;
  i = 0;
  while ( i < length ) 
  {
    j = i;
    while ( ++j < length )
    {
      if ( memcmp((void*) &(((BYTE*) pArray0)[i*datasize0]),
                  (void*) &(((BYTE*) pArray0)[j*datasize0]),
                  (size_t) datasize0) != 0 ) break;
    }
    if ( j > i )
    {
      if ( util_Fisher_Yates_shuffle_array((unsigned long) ( j - i),
                                           (size_t) datasize1, (void*) &(((BYTE*) pArray1)[i*datasize1])) != 0 )
      {
        return 1;
      }
    }
    i = j+1;
  }
  return 0;
}
  
int util_shuffle_matches_2arrays (unsigned long length, size_t datasize0, void *pArray0, size_t datasize1, void *pArray1, size_t datasize2, void *pArray2)
{
  unsigned long i;
  unsigned long j;
  if ( pArray0 == NULL ||
       pArray1 == NULL ||
       pArray2 == NULL ) return 1;
  i = 0;
  while ( i < length ) 
  {
    j = i;
    while ( ++j < length )
    {
      if ( memcmp((void*) &(((BYTE*) pArray0)[i*datasize0]),
                  (void*) &(((BYTE*) pArray0)[j*datasize0]),
                  (size_t) datasize0) != 0 ) break;
    }
    if ( j > i )
    {
      if ( util_Fisher_Yates_shuffle_2arrays((unsigned long) ( j - i),
                                             (size_t) datasize1, (void*) &(((BYTE*) pArray1)[i*datasize1]),
                                             (size_t) datasize2, (void*) &(((BYTE*) pArray2)[i*datasize2])) != 0)
      {
        return 1;
      }
    }
    i = j+1;
  }
  return 0;
}


int util_normalise_and_linear_clip_distribution (int num, float *pData, float limit_lo, float limit_hi)
{
  int i;
  float mean = 0.;
  float inv_mean;
  if ( pData == NULL ||
       num <= 0       ) return -1;

  for(i=0;i<num;i++) mean += pData[i];
  //  if ( (float) mean == (float) 0.0 ) inv_mean = (float) 1.0;
  if ( FLT_ISZERO(mean) ) inv_mean = (float) 1.0;
  else                    inv_mean = ((float)  num) / mean;

  for(i=0;i<num;i++)
  {
    pData[i] = pData[i] * inv_mean;
    if      ( pData[i] > limit_hi ) pData[i] = limit_hi; 
    else if ( pData[i] < limit_lo ) pData[i] = limit_lo; 
  }    
  return 0;
}



//--util_rand_gaussian
//--randomly assign a value from a gaussian distribution about mean
//--use the Box mueller method from NR
// P = (1/sigma*sqrt(2*Pi))exp( (Xm - X)^2 / 2 sigma^2)
//y1 = (mu - x1) / sigma
//y2 = (mu - x2) / sigma
//z1 = sqrt(-2*log(y1))cos(2*Pi*y2)
//z2 = sqrt(-2*log(y2))sin(2*Pi*y1)
//
double util_rand_gaussian (double mean, double sigma)
{
  double random1; //assume that rand48 is already seeded
  double random2;
  double square;
  if ( sigma <= (double) 0.0 ) return mean;
  do 
  {
    random1 = 2.0 * UTIL_DRAND - 1.0; 
    random2 = 2.0 * UTIL_DRAND - 1.0;
    square  = SQR(random1) + SQR(random2);
  }
  while ( square > (double) 1.0 || DBL_ISZERO(square));  //only accept results within unit circle

  //transform from y1 to x1 to z1
  return (double) sqrt( -2.0 * log(square) / square) * random1 * sigma + mean;

}//util_rand_gaussian
float util_rand_gaussianf (float mean, float sigma)
{
  float random1; //assume that rand48 is already seeded
  float random2;
  float square;
  if ( sigma <= (float) 0.0 ) return (float) mean;
  do 
  {
    random1 = 2.0 * UTIL_DRAND - 1.0; 
    random2 = 2.0 * UTIL_DRAND - 1.0;
    square  = SQR(random1) + SQR(random2);
  }
  while ( square > (float) 1.0 || FLT_ISZERO(square));  //only accept results in unit circle

  //transform from y1 to x1 to z1
  return (float) sqrtf( -2.0 * logf(square) / square) * random1 * sigma + mean;

}//util_rand_gaussian



//write a 'cd to working directory' line to a gnuplot plotfile
//require that the file has already been opened
int util_write_cd_line_to_plotfile (FILE* pPlotfile, const char* str_dir, const char* str_prefix)
{
  char str_cwd[STR_MAX];
  char str_path[STR_MAX] = {'\0'};
  if ( pPlotfile == NULL )
  {
    fprintf(stderr, "%s Could write to null file handle at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    (void) fflush(stderr); 
    return 1;
  }

  //get the working directory path
  if ( getcwd((char *) str_cwd, (size_t) STR_MAX) == NULL )
  {
    fprintf(stderr, "%s Could not get CWD at at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    (void) fflush(stderr); 
    return 1;
  }

  
  //get the part of the str_prefix that is a directory
  if ( strlen(str_prefix) > 0 )
  {
    int i;
    strncpy(str_path, str_prefix, STR_MAX);
    i = MIN(STR_MAX-1,strlen(str_path));
    while ( i >= 0 )
    {
      if ( str_path[i] == '/' ||
           i == 0)
      {
        str_path[i] = '\0';
        break;
      }
      i --;
    }
  }
  if ( strlen(str_dir) > 0 ) fprintf(pPlotfile, "cd \"%s/%s/%s\"\n", str_cwd, str_dir, str_path);
  else                       fprintf(pPlotfile, "cd \"%s/%s\"\n",    str_cwd, str_path);
    
  return 0;
}








//assume that start at rest, accellerate up to max speed then decelerate to rest at end
//retrun the time that this will take
//assume that inputs are all in the same units system
double util_calc_travel_time (double dist, double max_speed, double acceleration)
{
  double result = 0.0;
  if ( dist <= 0. ) result = 0.0;
  else if ( max_speed    <= 0.0 || isnan(max_speed) ) result = NAN;
  else if ( acceleration <= 0.0 || isnan(acceleration) )
  {
    //special case. Assume that we travel at a constant speed
    result = dist / max_speed;
  }
  else
  {
    double dist_to_reach_max_speed = 0.5 * SQR(max_speed) / acceleration;
    if ( dist_to_reach_max_speed > dist * 0.5 )
    {
      //time taken is given by the linear acceleration law
      result = 2.0 * sqrt(dist/acceleration);
    }
    else
    {
      //time taken is 2*time to accelerate to max_speed, then time to travel remaining distance at max_speed
      result = 2.0* (max_speed / acceleration) + (dist - 2.0 * dist_to_reach_max_speed)/max_speed;
    }
  }  
  
  return result;
}


//calculate the length of the arc between a coordinate rising above
//a given airmass and setting below that airmass
//all angles in degrees, lat is latitude of observer
//derived from formula for semi-dirurnal arc
//convert to time by dividing by 15 deg/hour (check)
double util_calc_diurnal_arc(double dec, double lat, double airmass)
{
  double alt;
  double arc;
  if ( airmass < 1.0 ) return NAN;
  if ( airmass > 30.0 ) alt = 0.0;
  else                  alt = (90.0 - acosd(1.0/airmass));
  arc = acosd((sind(alt) - (sind(dec)*sind(lat)))/(cosd(dec)*cosd(lat)));
  return (double) arc;  
}




int util_run_gnuplot_on_plotfile ( const char* str_plotfile, BOOL use_thread)
{
  char strSystem[STR_MAX];
  if ( strlen(str_plotfile) <= 0 ) return 1;

  snprintf(strSystem, STR_MAX, "gnuplot %s &> /dev/null", str_plotfile);
  
  ////////////////////////////////////////////////
  //when profiling make sure to comment out the following:
  ////////////////////////////////////////////////
  if ( use_thread )
  {
    pid_t pid_child;
    if ( (pid_child = (pid_t) fork()) == 0 )  //this is the child process
    {
      pid_t pid_grandchild;
      if ( (pid_grandchild = (pid_t) fork()) == 0 ) //second fork - this is the grandchild
      {
        //try it the execlp() way
        execlp((const char*) "gnuplot_quiet", "gnuplot_quiet", str_plotfile, NULL);
        _exit(1); //error if got to here
        //        if ( system(strSystem))
        //        {
        //          //don't worry if it fails to run correctly, we'll just have to cope
        //        }
        //        exit(0);
      }
      else if (pid_grandchild < 0) //failed to fork for some reason! This is bad. 
      {
        exit(1);
      }
      else
      {
        //this is the child process, so exit immediately to ensure that
        //grandchild process is orphaned and reaped by init process
        exit(0);
      }
    }
    else if (pid_child < 0) //failed to fork for some reason! This is bad. 
    {
      exit(1);
    }
    else  //this is the parent - so do nothing
    {
    }
  }
  else  //just make system call from within the current process
  {
    if ( system(strSystem))
    {
      //don't worry if it fails to run correctly, we'll just have to cope
    }
  }

  ////////////////////////////////////////////////
  //end of part to remove for profiling
  ////////////////////////////////////////////////
  return 0;
}



int util_gzip_file ( const char* str_filename, BOOL use_thread)
{
  char strSystem[STR_MAX];
  if ( strlen(str_filename) <= 0 ) return 1;

  snprintf(strSystem, STR_MAX, "gzip -q -f %s &> /dev/null", str_filename);
  
  ////////////////////////////////////////////////
  //when profiling make sure to comment out the following:
  ////////////////////////////////////////////////
  if ( use_thread )
  {
    pid_t pid_child;
    if ( (pid_child = (pid_t) fork()) == 0 )  //this is the child process
    {
      pid_t pid_grandchild;
      if ( (pid_grandchild = (pid_t) fork()) == 0 ) //second fork - this is the grandchild
      {
        //try it the execlp() way
        execlp((const char*) "gzip", "gzip", "-q", "-f", str_filename, NULL);
        _exit(1); //error if got to here
        //        if ( system(strSystem))
        //        {
        //          //don't worry if it fails to run correctly, we'll just have to cope
        //        }
        //        exit(0);
      }
      else if (pid_grandchild < 0) //failed to fork for some reason! This is bad. 
      {
        exit(1);
      }
      else
      {
        //this is the child process, so exit immediately to ensure that
        //grandchild process is orphaned and reaped by init process
        exit(0);
      }
    }
    else if (pid_child < 0) //failed to fork for some reason! This is bad. 
    {
      exit(1);
    }
    else  //this is the parent - so do nothing
    {
    }
  }
  else  //just make system call from within the current process
  {
    if ( system(strSystem))
    {
      //don't worry if it fails to run correctly, we'll just have to cope
    }
  }

  ////////////////////////////////////////////////
  //end of part to remove for profiling
  ////////////////////////////////////////////////

  return 0;
}





int twoDhisto_struct_init (twoDhisto_struct *pHisto)
{
  if ( pHisto == NULL ) return 1;
  pHisto->xmin = NAN;
  pHisto->xmax = NAN;
  pHisto->dx = NAN;

  pHisto->ymin = NAN;
  pHisto->ymax = NAN;
  pHisto->dy = NAN;

  pHisto->nx = 0;
  pHisto->ny = 0;
  pHisto->npix = 0;
  pHisto->data_sum = 0;
  pHisto->pData = NULL;
  return 0;
}


int twoDhisto_struct_add_count (twoDhisto_struct *pHisto, double dx, double dy )
{
  if ( pHisto == NULL ) return 1; 
  
  if ( pHisto->pData != NULL ) // not an error condition, silently ignore
  {
    int pix_x = (int) floor((dx - pHisto->xmin)*pHisto->inv_dx);
    int pix_y = (int) floor((dy - pHisto->ymin)*pHisto->inv_dy);
    if ( pix_x >= 0 &&
         pix_y >= 0 &&
         pix_x < pHisto->nx &&
         pix_y < pHisto->ny )
    {
      pHisto->pData[pix_x + pix_y * pHisto->nx] ++;
      pHisto->data_sum ++;
    }
  }

  return 0;
}




//write out the two-d histo in a standard way
int twoDhisto_struct_write_to_file (twoDhisto_struct *pHisto, const char *str_filename )
{
  FILE *pFile = NULL;
  int i,j;
  if ( pHisto == NULL ) return 1;

  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  //print a header
  fprintf(pFile, "#2DHISTO_SUMMARY xaxis => pix_max= %6d range= %8.3f %8.3f  step= %8.5g inv_step= %8.5g\n",
          pHisto->nx -1, pHisto->xmin, pHisto->xmax, pHisto->dx, pHisto->inv_dx); 
  fprintf(pFile, "#2DHISTO_SUMMARY yaxis => pix_max= %6d range= %8.3f %8.3f  step= %8.5g inv_step= %8.5g\n",
          pHisto->ny -1, pHisto->ymin, pHisto->ymax, pHisto->dy, pHisto->inv_dy);
  fprintf(pFile, "#2DHISTO_SUMMARY npix= %8d data_sum= %8d pix_area= %8.5g\n",
          pHisto->npix, pHisto->data_sum, pHisto->dx*pHisto->dy );
  fprintf(pFile, "#%-5s %6s %8s %8s %10s %10s %10s %10s\n",
          "ind_x", "ind_y", "pos_x", "pos_y", "number", "fraction", "radius(mm)", "angle(rad)" );
  
  for(j=0;j<pHisto->ny;j++)
  {
    for(i=0;i<pHisto->nx;i++)
    {
      double radius = sqrt(SQR(pHisto->xmin + (double) (i+0.5) * pHisto->dx) +
                           SQR(pHisto->ymin + (double) (j+0.5) * pHisto->dy));
      double angle  = atan2(pHisto->xmin + (double) (i+0.5) * pHisto->dx, pHisto->ymin + (double) (j+0.5) * pHisto->dy);
      if ( angle < 0.) angle += 2.0 *M_PI;
      fprintf(pFile, "%6d %6d %8.3f %8.3f %10d %10.7f %10.5f %10.5f\n",
              i, j,
              pHisto->xmin + (float) i * pHisto->dx,
              pHisto->ymin + (float) j * pHisto->dy,
              pHisto->pData[i + j * pHisto->nx],
              (float) pHisto->pData[i + j * pHisto->nx]/(float)MAX(1,pHisto->data_sum),
              radius, angle );

    }
    fprintf(pFile,"\n");
  }
 
  
  if ( pFile) fclose(pFile); pFile = NULL;
  return 0;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// sorting_struct/sortinglist_struuct functions ////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
int sorting_struct_init (sorting_struct *pSorting)
{
  if ( pSorting == NULL ) return 1;
  pSorting->pDataStruct = NULL;
  pSorting->sorting_variable = NAN;
  return 0;
}

int sortinglist_struct_clear_items (sortinglist_struct *pSortingList)
{
  if ( pSortingList == NULL ) return 1;
  if( pSortingList->pSorting )
  {
    int i;
    for(i=0;i<pSortingList->array_length;i++)
      if ( sorting_struct_init ((sorting_struct*) &(pSortingList->pSorting[i]))) return 1;
  }
  pSortingList->nItems = 0;
  return 0;
}

int sortinglist_struct_init (sortinglist_struct *pSortingList, int num_items)
{
  if ( pSortingList == NULL ) return 1;
  if ( num_items < 0 ) return 1;
  pSortingList->pSorting = NULL;

  pSortingList->array_length = num_items;
  pSortingList->nItems = 0;
  if ( num_items > 0 )
  {
    if ( (pSortingList->pSorting = (sorting_struct*) malloc(sizeof(sorting_struct) * pSortingList->array_length)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
    //initialise the new Sorting structs
    if ( sortinglist_struct_clear_items ((sortinglist_struct*) pSortingList)) return 1;
  }
  return 0;
}

int sortinglist_struct_add_item (sortinglist_struct *pSortingList, void *pDataStruct, double sorting_variable )
{
  if ( pSortingList == NULL ) return 1;
  if ( pSortingList->nItems >= pSortingList->array_length )
  {
    fprintf(stderr, "%s Problem adding item to sorting list - the list is full (%d items)\n",
            ERROR_PREFIX, pSortingList->array_length);
    return 2; //too many items
  }

  //add the new item
  pSortingList->pSorting[pSortingList->nItems].pDataStruct      = (void*)  pDataStruct;
  pSortingList->pSorting[pSortingList->nItems].sorting_variable = (double) sorting_variable;

  //  if ( memcpy((void*) &(pSortingList->pSorting[pSortingList->nItems]),
  //              (void*) pSorting , (size_t) sizeof(sorting_struct)) == NULL) return 1;

  pSortingList->nItems++;
  return 0;
}

int sortinglist_struct_free (sortinglist_struct *pSortingList)
{
  if ( pSortingList == NULL ) return 1;

  if ( pSortingList->pSorting ) free ( pSortingList->pSorting);
  pSortingList->pSorting = NULL;
  pSortingList->array_length = 0;
  pSortingList->nItems = 0;
  return 0;
}

//wrapper for qsort comparison function - standard order sort
//NANs are always put to the end i.e as if they had a high value
int sorting_struct_compare_standard (const void *pSort1, const void *pSort2)
{
  BOOL isnan1;
  BOOL isnan2;
  if ( pSort1 == NULL || pSort2 == NULL ) return 0;
  isnan1 = (BOOL) isnan(((sorting_struct*) pSort1)->sorting_variable);
  isnan2 = (BOOL) isnan(((sorting_struct*) pSort2)->sorting_variable);
  if      ( isnan1 && isnan2 ) return 0;
  else if ( isnan1 )           return 1; 
  else if ( isnan2 )           return -1; 
  else if (((sorting_struct*) pSort1)->sorting_variable > ((sorting_struct*) pSort2)->sorting_variable) return  1;
  else if (((sorting_struct*) pSort1)->sorting_variable < ((sorting_struct*) pSort2)->sorting_variable) return -1;
  else return 0;
}

//wrapper for qsort comparison function - reversed sort
//NANs are always put to the end i.e as if they had a low value
int sorting_struct_compare_reverse  (const void *pSort1, const void *pSort2)
{
  BOOL isnan1;
  BOOL isnan2;
  if ( pSort1 == NULL || pSort2 == NULL ) return 0;
  isnan1 = (BOOL) isnan(((sorting_struct*) pSort1)->sorting_variable);
  isnan2 = (BOOL) isnan(((sorting_struct*) pSort2)->sorting_variable);
  if      ( isnan1 && isnan2 ) return 0;
  else if ( isnan1 )           return -1; 
  else if ( isnan2 )           return 1; 
  else if (((sorting_struct*) pSort1)->sorting_variable > ((sorting_struct*) pSort2)->sorting_variable) return -1;
  else if (((sorting_struct*) pSort1)->sorting_variable < ((sorting_struct*) pSort2)->sorting_variable) return  1;
  else return 0;
}


int sortinglist_struct_sort (sortinglist_struct *pSortingList,  BOOL reverse )
{
  int(*psort_func)(const void *, const void *);
  if ( pSortingList == NULL ) return 1;

  if ( reverse == TRUE ) psort_func = sorting_struct_compare_reverse;
  else                   psort_func = sorting_struct_compare_standard;
  
  //do the sorting
  qsort((void*) pSortingList->pSorting,
        (size_t) pSortingList->nItems,
        (size_t) sizeof(sorting_struct),
        psort_func );
  return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// sorting_struct/sortinglist_struuct functions ////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////
//estimate number of lines in a file
//read first few lines, and the total file size, then deduce the total number of lines
long util_est_num_lines_in_named_file (const char* str_filename, const long num_sample_lines)
{
  //  clock_t clocks_start = clock();
  long num_lines = -1;
  long current_line;
  long current_byte;
  long num_bytes = 0;
  FILE *pFile = NULL;
  struct stat st;
  int in_char;

  //get the file status
  if ( stat(str_filename, &st) )
  {
    fprintf(stderr, "%s I had problems stat'ing the file: %s (errno= %d)\n", ERROR_PREFIX, str_filename, errno);
    fflush(stderr);
    return -1;
  }
  num_bytes = (long) st.st_size;

  //now need to sample the file
  
  //open the file read only
  if ((pFile = fopen(str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the file: %s\n", ERROR_PREFIX, str_filename);
    fflush(stderr);
    return -1;
  }

  //scan forward the given number of lines
  current_line = 0 ; 
  current_byte = 0 ; 
  while ((in_char = (int) fgetc(pFile)) != EOF )
  {
    current_byte ++; 
    if ( (char) in_char == '\n' ) current_line ++;
    if ( current_line >= num_sample_lines ) break; 
  }

  if ( pFile )
  {
    fclose (pFile);
    pFile = NULL;
  }
  
  num_lines = (long) num_bytes * current_line / MAX(1,current_byte);
  
  //  fprintf(stdout, "%s File %s has est. %ld lines (total size %ld bytes, sampled %ld lines, %ld bytes) it took %.7gs\n",
  //          COMMENT_PREFIX, str_filename,
  //          num_lines, num_bytes, current_line, current_byte,
  //          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);

  return num_lines;
}

//search for the bounding values then interpolate
int util_binary_search_interp_double (long nrows, double *parray, double value, double *pimid)
{
  long imin,imax;
  int ret_val = 0;
  if ( parray == NULL || pimid == NULL ) return -1;
  if ( nrows < 2 ) return -1;

  if ( (ret_val = util_binary_search_double ((long)nrows,
                                             (double*) parray,
                                             (double) value,
                                             (long*) &imin, (long*) &imax)) < 0 )
  {
    return (int) ret_val;
  }

  *pimid = (double) util_interp_linear_1D_double ((double) parray[imin], (double) (imin), (double) parray[imax], (double) (imax), value);
  return (int) ret_val;
}
//search for the bounding values then interpolate
int util_binary_search_interp_float (long nrows, float *parray, float value, float *pimid)
{
  long imin,imax;
  int ret_val = 0;
  if ( parray == NULL || pimid == NULL ) return -1;
  if ( nrows < 2 ) return -1;

  if ( (ret_val = util_binary_search_float ((long)nrows,
                                             (float*) parray,
                                             (float) value,
                                             (long*) &imin, (long*) &imax)) < 0 )
  {
    return (int) ret_val;
  }

  *pimid = (float) util_interp_linear_1D_float ((float) parray[imin], (float) (imin), (float) parray[imax], (float) (imax), value);
  return (int) ret_val;
}

//assumes that parray is pre-sorted - must be ascending
//return -1 for a real error
//return 0 for success
//return 1,2 if value is outside parray
//http://en.wikipedia.org/wiki/Binary_search
int util_binary_search_double (long nrows, double *parray, double value, long *pimin, long *pimax)
{
  long imin,imax;
  if ( parray == NULL ) return -1;
  if ( nrows < 2 ) return -1;
  if ( pimin == NULL && pimax == NULL ) return 0;  //nothing to do
  
  if ( value < parray[0] )
  {
    if ( pimin) *pimin = (long) 0;
    if ( pimax) *pimax = (long) 1;  //return the first two array indices
    return 1;
  }
  else if  ( value > parray[nrows - 1])
  {
    if ( pimin) *pimin = (long) (nrows - 2);
    if ( pimax) *pimax = (long) (nrows - 1); //return the last two array indices
    return 2;
  }
  
  imin = (long) 0;
  imax = (long) (nrows - 1);
  // continue searching while [imin,imax] is not empty
  while (imax > imin+1)
  {
    // calculate the midpoint for roughly equal partition
    long imid = imin + ((imax - imin) / 2);
    if (parray[imid] < value)
      imin = imid;       // change min index to search upper subarray
    else         
      imax = imid;       // change max index to search lower subarray
  }
  // success
  if ( pimin) *pimin = (long) imin;
  if ( pimax) *pimax = (long) imax;
  return 0;
}

int util_binary_search_float (long nrows, float *parray, float value, long *pimin, long *pimax)
{
  long imin,imax;
  if ( parray == NULL ) return -1;
  if ( nrows < 2 ) return -1;
  if ( pimin == NULL && pimax == NULL ) return 0;  //nothing to do

  if ( value < parray[0] )
  {
    if ( pimin) *pimin = (long) 0;
    if ( pimax) *pimax = (long) 1;  //return the first two array indices
    return 1;
  }
  else if  ( value > parray[nrows - 1])
  {
    if ( pimin) *pimin = (long) (nrows - 2);
    if ( pimax) *pimax = (long) (nrows - 1); //return the last two array indices
    return 2;
  }
  
  imin = (long) 0;
  imax = (long) (nrows - 1);
  // continue searching while [imin,imax] is not empty
  while (imax > imin+1)
  {
    // calculate the midpoint for roughly equal partition
    long imid = imin + ((imax - imin) / 2);
    if (parray[imid] < value)
      imin = imid;       // change min index to search upper subarray
    else         
      imax = imid;       // change max index to search lower subarray
  }
  // success
  if ( pimin) *pimin = (long) imin;
  if ( pimax) *pimax = (long) imax;
  return 0;
}



//return 1 for a real error
//return 0 for normal success
int util_unsorted_get_index_of_nearest_double (long nrows, double *parray, double value, long *pindex)
{
  long i, ibest = 0;
  double bestdist = DBL_MAX;
  double dist;
  if ( parray == NULL ||
       nrows < 1 )
    return 1;

  for ( i=0; i<nrows; i++)
  {
    dist = fabs(parray[i]-value);
    if (dist < bestdist)
    {
      ibest = i;
      bestdist = dist;
    }
  }
  *pindex = (long) ibest;
  return 0;
}

//return -1 for a real error
//return 0 for normal success
//return 1,2 if value is outside parray
int util_unsorted_binary_search_double (long nrows, double *parray, double value, long *pimin, long *pimax )
{
  long   *pindex = NULL;
  double *pdata  = NULL;
  long i, jmin, jmax;
  int result;
  if ( parray == NULL ) return -1;
  if ( nrows < 2 ) return -1;

   //allocate space for data
  if ((pindex = (long*)   malloc(sizeof(long) * nrows)) == NULL ||
      (pdata  = (double*) malloc(sizeof(double) * nrows)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory (at file %s line %d)\n", ERROR_PREFIX, __FILE__, __LINE__);
    return -1;
  }

  for(i=0;i<nrows;i++)
  {
    pindex[i] = i;
    pdata[i] = parray[i];
  }

  if ( util_dlsort((unsigned long) nrows, (double*) pdata, (long*) pindex)) return -1;

  result = util_binary_search_double ((long) nrows, (double*) pdata, (double) value, (long*) &jmin, (long*) &jmax);

  if ( result >= 0 )
  {
    *pimin = (long) pindex[jmin];
    *pimax = (long) pindex[jmax];
  }
  if ( pindex ) free (pindex);
  if ( pdata )  free (pdata);
  return result;
}



////return -1 for a real error
////return 0 for normal success
////return 1,2 if value is outside parray
////return 3 if exact match 
//int util_unsorted_get_index_of_bounds_double (long nrows, double *parray, double value, long *pimin, long *pimax )
//{
//  long nok = 0;
//  long i, ibestmin = -1,ibestmax = -1;
//  long i2ndbestmin = -1,i2ndbestmax = -1;
//  double distbestmin = DBL_MAX;
//  double distbestmax = DBL_MAX;
//  double dist2ndbestmin = DBL_MAX;
//  double dist2ndbestmax = DBL_MAX;
//
//  if ( parray == NULL ) return -1;
//  if ( nrows < 2 ) return -1;
//
//  for ( i=0; i<nrows; i++)
//  {
//    if ( !isnan(parray[i]) )
//    {
//      nok ++;
//      if ( DBL_ISZERO(parray[i] - value)) //matched with exact equality - we can stop immediately
//      {
//        *pimin = (long) i; 
//        *pimax = (long) i; 
//        return 3;
//      }
//      else if ( parray[i] > value )
//      {
//        if ( (parray[i] - value) < distbestmax )
//        {
//          ibestmax = i;
//          distbestmax = parray[i] - value;
//        }
//      }
//      else // parray[i] < value
//      {
//        if ( (value - parray[i]) < distbestmin )
//        {
//          ibestmin = i;
//          distbestmin = value - parray[i];
//        }
//      }
//    }
//  }
//  if ( nok < 2 ) return -1;
//  
//  if ( ibestmax < 0  && ibestmin < 0 )
//  {
//    return -1;  // we coulnd't find any valid values!
//  }
//  
//  if ( ibestmax < 0 )  // all parray values were smaller than given value - ok
//  {
//    *pimin = (long) ibestmin; 
//    for ( i=0; i<nrows; i++)
//    {
//      if ( parray[i] > parray[ibestmin] &&
//           abs(parray[i] - parray[ibestmin]) < dist2ndbestmin )
//      {
//        dist2ndbestmin = abs(parray[i] - parray[ibestmin]);
//        i2ndbestmin = i;
//      }
//    }
//    *pimax = (long) i2ndbestmin;
//    return 1;  //signals that result was outside range of data (below)
//  }
//  else if ( ibestmin < 0 )  // there were no parray values smaller than given value - ok
//  {
//    *pimax = (long) ibestmax; 
//    for ( i=0; i<nrows; i++)
//    {
//      if ( parray[i] < parray[ibestmax] &&
//           abs(parray[ibestmax] - parray[i]) < dist2ndbestmax )
//      {
//        dist2ndbestmax = abs(parray[ibestmax] - parray[i]);
//        i2ndbestmax = i;
//      }
//    }
//    *pimin = (long) i2ndbestmax;
//    return 2;//signals that result was outside range of data (above)
//  }
//  
//  *pimin = (long) ibestmin; 
//  *pimax = (long) ibestmax; 
//  
//  return 0;
//}



//from wikipedia!
//integral of Gaussian from -inf to x = 0.5*[1 + erf((x-x0)/(sigma*sqrt(2)))] 
//So integral of Gaussian over interval x1 to x2
//                =   0.5*[1 + erf((x2-x0)/(sigma*sqrt(2)))] -  0.5*[1 + erf((x1-x0)/(sigma*sqrt(2)))]
//                =   0.5*[erf((x2-x0)/(sigma*sqrt(2))) - erf((x1-x0)/(sigma*sqrt(2)))]
double util_integrate_gaussian_over_interval (double x0, double sigma, double xmin, double xmax)
{
  const double  one_over_sqrt_2 = (double) 1. /sqrt((double) 2.);
  double result = 0.0;
  double one_over_sigma_sqrt2;
  if ( DBL_ISZERO(sigma) ) return NAN;
  one_over_sigma_sqrt2 = (double) one_over_sqrt_2 / sigma;

  result = (double) 0.5 * (erf((xmax-x0)*one_over_sigma_sqrt2) - erf((xmin-x0)*one_over_sigma_sqrt2));
  return result;
   
}

//-------------------------------------------------------------------------------------
//--util_lngamma_func
// Returns the natural log of the gamma function, used by util_poisson_rand_NR(), NR method
//see http://library.lanl.gov/numerical/bookcpdf/c6-1.pdf
double util_lngamma_func (double xx)
{
  double x,y,tmp,ser;
  const double cof[6] = {76.18009172947146,
                         -86.50532032941677,
                         24.01409824083091,
                         -1.231739572450155,
                         0.1208650973866179e-2,
                         -0.5395239384953e-5};
  int j;
  y=x=xx;
  tmp = x + 5.5;
  tmp -= (x +0.5)*log(tmp);
  ser = 1.000000000190015;
  for (j=0;j<=5;j++) ser += cof[j]/++y;
  
  return (log(2.5066282746310005*ser/x) - tmp);
}


//-------------------------------------------------------------------------------------
//--util_poisson_rand_NR
// Generates a poisson distributed random integer using the NR method
// see http://library.lanl.gov/numerical/bookcpdf/c7-3.pdf
// lambda is the mean value
// P(n) * exp(lambda) = lambda^-n / n!
// NOT THREADSAFE!
long util_poisson_rand(double lambda)
{
  static double sq,ln_lambda,g,oldm=(-1.0);
  long result;
  double em,t,y;
  if ( lambda < 12.0 )
  {
    if (lambda != oldm)
    {
      oldm = lambda;
      g = exp(-1.0 * lambda);
    }
    em = -1.0;
    t=1.0;
    do
    {
      ++em;
      //      t *= gsl_drand(NULL);
      t *= (double) random()/(double) RAND_MAX;
    }
    while (t > g ) ;
  }
  else
  {
    if (lambda != oldm)
    {
      oldm = lambda;
      sq=sqrt(2.0*lambda);
      ln_lambda=log(lambda);
      g=lambda*ln_lambda - util_lngamma_func(lambda + 1.0);
    }
    do
    {
      do
      {
        //        y=tan(M_PI * gsl_drand(NULL));
        y=tan(M_PI * (double) random()/(double) RAND_MAX);
        em = sq * y + lambda;
      }
      while (em < 0.);
      em=floor(em);
      t = 0.9 * (1.0 + y*y)*exp(em*ln_lambda - util_lngamma_func(em + 1.0)-g);
    }
    //    while (gsl_drand(NULL) > t);
    while (((double) random()/(double) RAND_MAX) > t);
  }
  result = (long) em;
  return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// filter the provided list so that it contains only a sorted list of *pnunique entries
//integer array version
int util_uniqueify_i(unsigned long n, int *pdata, unsigned long *pnunique)
{
  unsigned long n_in = n;
  if ( n_in < 1 || pdata == NULL ) return 1;
  else if ( n_in == 1 )
  {
    *pnunique = (unsigned long) n_in;
    return 0;
  }
  else
  {
    unsigned long i,j=0;
    if ( util_isort ( n_in, pdata )) return 1;
    for(i=1;i<n_in;i++)
    {
      if ( pdata[i] > pdata[j] ) pdata[++j] = pdata[i];
    }
    *pnunique = (unsigned long) (j+1);
  }
  return 0;
}

//unsigned integer array version
int util_uniqueify_u(unsigned long n, uint *pdata, unsigned long *pnunique)
{
  unsigned long n_in = n;
  if ( n_in < 1 || pdata == NULL ) return 1;
  else if ( n_in == 1 )
  {
    *pnunique = (unsigned long) n_in;
    return 0;
  }
  else
  {
    unsigned long i,j=0;
    if ( util_usort ( n_in, pdata )) return 1;
    for(i=1;i<n_in;i++)
    {
      if ( pdata[i] > pdata[j] ) pdata[++j] = pdata[i];
    }
    *pnunique = (unsigned long) (j+1);
  }
  return 0;
}
