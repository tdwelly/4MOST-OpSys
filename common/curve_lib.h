//----------------------------------------------------------------------------------
//--
//--    Filename: curve_lib.h
//--    Description:
//--    Use: This module contains a library of functions used to carry out simple geometric tests
//--
//--
//--
#ifndef CURVE_LIB_H
#define CURVE_LIB_H

#include <stdio.h>
#include <fitsio.h>

//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.1 $"
#define CVS_DATE_H     "$Date: 2015/11/16 20:02:59 $"
inline static int curve_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

// #defines
#define STR_MAX        DEF_STRLEN

//add more formats?


#define CURVE_FILE_FORMAT_UNKNOWN 0
#define CURVE_FILE_FORMAT_ASCII   1
#define CURVE_FILE_FORMAT_FITS    2


//                                             STANDARD UNITS
enum curve_unit_type {
  CURVE_UNIT_TYPE_UNKNOWN          =0,         // 
  CURVE_UNIT_TYPE_DIMENSIONLESS      ,         // none
  CURVE_UNIT_TYPE_WAVELENGTH         ,         // AA
  CURVE_UNIT_TYPE_GEOMETRIC_AREA     ,         // m2
  CURVE_UNIT_TYPE_SOLID_ANGLE        ,         // deg2
  CURVE_UNIT_TYPE_FPHOT              ,         // ph/cm2/s    
  CURVE_UNIT_TYPE_FPHOT_LAMBDA       ,         // ph/cm2/s/AA 
  CURVE_UNIT_TYPE_FPHOT_NU           ,         // ph/cm2/s/Hz 
  CURVE_UNIT_TYPE_FE                 ,         // erg/cm2/s     
  CURVE_UNIT_TYPE_FE_LAMBDA          ,         // erg/cm2/s/AA  
  CURVE_UNIT_TYPE_FE_NU              ,         // erg/cm2/s/Hz  
  CURVE_UNIT_TYPE_FE_E               ,         // erg/cm2/s/keV 
  CURVE_UNIT_TYPE_FPHOT_SOLID        ,         // ph/cm2/s/arcsec2
  CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID ,         // ph/cm2/s/AA/arcsec2
  CURVE_UNIT_TYPE_FPHOT_NU_SOLID     ,         // ph/cm2/s/Hz/arcsec2
  CURVE_UNIT_TYPE_FE_SOLID           ,         // erg/cm2/s/arcsec2
  CURVE_UNIT_TYPE_FE_LAMBDA_SOLID    ,         // erg/cm2/s/AA/arcsec2
  CURVE_UNIT_TYPE_FE_NU_SOLID        ,         // erg/cm2/s/Hz/arcsec2
  CURVE_UNIT_TYPE_FE_E_SOLID         ,         // erg/cm2/s/keV/arcsec2
  CURVE_UNIT_TYPE_DISPERSION                   // AA/um
};
  
#define CURVE_UNIT_TYPE_STRING(x) ((x)==CURVE_UNIT_TYPE_UNKNOWN ? "UNIT_TYPE_UNKNOWN":\
((x)==CURVE_UNIT_TYPE_DIMENSIONLESS     ? "UNIT_TYPE_DIMENSIONLESS":\
((x)==CURVE_UNIT_TYPE_WAVELENGTH        ? "UNIT_TYPE_WAVELENGTH":\
((x)==CURVE_UNIT_TYPE_GEOMETRIC_AREA    ? "UNIT_TYPE_GEOMETRIC_AREA":\
((x)==CURVE_UNIT_TYPE_SOLID_ANGLE       ? "UNIT_TYPE_SOLID_ANGLE":\
((x)==CURVE_UNIT_TYPE_FPHOT             ? "UNIT_TYPE_FPHOT":\
((x)==CURVE_UNIT_TYPE_FPHOT_LAMBDA      ? "UNIT_TYPE_FPHOT_LAMBDA":\
((x)==CURVE_UNIT_TYPE_FPHOT_NU          ? "UNIT_TYPE_FPHOT_NU":\
((x)==CURVE_UNIT_TYPE_FE                ? "UNIT_TYPE_FE":\
((x)==CURVE_UNIT_TYPE_FE_LAMBDA         ? "UNIT_TYPE_FE_LAMBDA":\
((x)==CURVE_UNIT_TYPE_FE_NU             ? "UNIT_TYPE_FE_NU":\
((x)==CURVE_UNIT_TYPE_FE_E              ? "UNIT_TYPE_FE_E":\
((x)==CURVE_UNIT_TYPE_FPHOT_SOLID       ? "UNIT_TYPE_FPHOT_SOLID":\
((x)==CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID? "UNIT_TYPE_FPHOT_LAMBDA_SOLID":\
((x)==CURVE_UNIT_TYPE_FPHOT_NU_SOLID    ? "UNIT_TYPE_FPHOT_NU_SOLID":\
((x)==CURVE_UNIT_TYPE_FE_SOLID          ? "UNIT_TYPE_FE_SOLID":\
((x)==CURVE_UNIT_TYPE_FE_LAMBDA_SOLID   ? "UNIT_TYPE_FE_LAMBDA_SOLID":\
((x)==CURVE_UNIT_TYPE_FE_NU_SOLID       ? "UNIT_TYPE_FE_NU_SOLID":\
((x)==CURVE_UNIT_TYPE_FE_E_SOLID        ? "UNIT_TYPE_FE_E_SOLID":\
((x)==CURVE_UNIT_TYPE_DISPERSION        ? "UNIT_TYPE_DISPERSION":\
 "Error"))))))))))))))))))))

#define CURVE_UNIT_TYPE_DEFAULT_UNIT(x) ((x)==CURVE_UNIT_TYPE_UNKNOWN ? "Unknown":\
((x)==CURVE_UNIT_TYPE_DIMENSIONLESS     ? "dimensionless":\
((x)==CURVE_UNIT_TYPE_WAVELENGTH        ? "AA":\
((x)==CURVE_UNIT_TYPE_GEOMETRIC_AREA    ? "m2":\
((x)==CURVE_UNIT_TYPE_SOLID_ANGLE       ? "deg2":\
((x)==CURVE_UNIT_TYPE_FPHOT             ? "ph/cm2/s":\
((x)==CURVE_UNIT_TYPE_FPHOT_LAMBDA      ? "ph/cm2/s/AA":\
((x)==CURVE_UNIT_TYPE_FPHOT_NU          ? "ph/cm2/s/Hz":\
((x)==CURVE_UNIT_TYPE_FE                ? "erg/cm2/s":\
((x)==CURVE_UNIT_TYPE_FE_LAMBDA         ? "erg/cm2/s/AA":\
((x)==CURVE_UNIT_TYPE_FE_NU             ? "erg/cm2/s/Hz":\
((x)==CURVE_UNIT_TYPE_FE_E              ? "erg/cm2/s/keV":\
((x)==CURVE_UNIT_TYPE_FPHOT_SOLID       ? "ph/cm2/s/arcsec2":\
((x)==CURVE_UNIT_TYPE_FPHOT_LAMBDA_SOLID? "ph/cm2/s/AA/arcsec2":\
((x)==CURVE_UNIT_TYPE_FPHOT_NU_SOLID    ? "ph/cm2/s/Hz/arcsec2":\
((x)==CURVE_UNIT_TYPE_FE_SOLID          ? "erg/cm2/s/arcsec2":\
((x)==CURVE_UNIT_TYPE_FE_LAMBDA_SOLID   ? "erg/cm2/s/AA/arcsec2":\
((x)==CURVE_UNIT_TYPE_FE_NU_SOLID       ? "erg/cm2/s/Hz/arcsec2":\
((x)==CURVE_UNIT_TYPE_FE_E_SOLID        ? "erg/cm2/s/keV/arcsec2":\
((x)==CURVE_UNIT_TYPE_DISPERSION        ? "AA/nm":\
 "Error"))))))))))))))))))))




// structures

//this describes a general curve e.g. throughput/emission/wavelength curve
typedef struct {
  char str_name[STR_MAX];     //identifier
  char str_filename[STR_MAX]; //file from which this curve was read
  int  file_format;      //ascii or fits

  char str_unit_x[STR_MAX];  //x-axis (independent) unit description
  char str_unit_y[STR_MAX];  //y-axis (dependent) unit description
  double unit_x; //x axis scaling from the standard value - derived from str_unit_x
  double unit_y; //y axis scaling from the standard value - derived from str_unit_y

  int unit_type_x; //e.g. CURVE_UNIT_TYPE_WAVELENGTH
  int unit_type_y; //e.g. CURVE_UNIT_TYPE_PHOTON_FLUX_DENSITY
  
  char str_colname_x[STR_MAX];     //identifier, used for fits
  char str_colname_y[STR_MAX];      //
  int colnum_x;        //can be used for fits or ascii
  int colnum_y;

  
  long num_rows;      //number of valid rows
  double *px;     //x values
  double *py;     //y values

  double min_x;
  double min_y;

  double max_x;
  double max_y;
} curve_struct;
////////////////////////////////////////////////////////////////

//function prototypes

/////////////////////////////////////////////////////////////////////////////
int curve_struct_init                        ( curve_struct *pCurve );
int curve_struct_free                        ( curve_struct *pCurve );
int curve_struct_scale_y                     ( curve_struct *pCurve, double scale );
int curve_struct_copy                        ( curve_struct *pCurveDest, curve_struct *pCurveOrig );
int curve_struct_read_from_file              ( curve_struct *pCurve, BOOL verbose );
int curve_struct_read_from_fitsfile          ( curve_struct *pCurve, BOOL verbose );
int curve_struct_read_from_fits_table        ( curve_struct *pCurve, fitsfile* pFile, BOOL verbose );
int curve_struct_read_from_fits_image        ( curve_struct *pCurve, fitsfile* pFile, BOOL verbose );
int curve_struct_read_from_asciifile         ( curve_struct *pCurve, BOOL verbose );
int curve_struct_write_to_fits_table         ( curve_struct *pCurve, BOOL verbose );
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
int curve_read_unit_for_numbered_column      (fitsfile* pFile, int colnum, char* str_unit, int* punit_type, double* punit);
int curve_interpret_unit_string              (char* str_unit, int* punit_type, double* punit);
/////////////////////////////////////////////////////////////////////////////



#endif
