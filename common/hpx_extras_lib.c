// -*-mode:C; compile-command:"make -f hpx_extras_lib.make"; -*- 
//----------------------------------------------------------------------------------
//--
//--    Filename: hpx_extras_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
#define CVS_REVISION "$Revision: 1.6 $"
#define CVS_DATE     "$Date: 2015/08/18 11:42:08 $"
//--    Description:
//--    Use: This module contains extensions to the basic chealpix routines
//--         
//--
//--
//#define _GNU_SOURCE   // to enable sincos
#include "hpx_extras_lib.h"

#include <stdio.h>
#include <math.h>
#include <float.h>
#include <chealpix.h>   
#include <fitsio.h>



#define ERROR_PREFIX   "#-hpx_extras_lib.c : Error   :"
#define WARNING_PREFIX "#-hpx_extras_lib.c : Warning :"
#define COMMENT_PREFIX "#-hpx_extras_lib.c : Comment :"
#define DEBUG_PREFIX   "#-hpx_extras_lib.c : DEBUG   :"

////////////////////////////////////////////////////////////////////////
//functions

int hpx_extras_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}



// Ported to C from original fortran90 
// ${HEALPIX}/src/f90/mod/pix_tools.F90
//  !=======================================================================
long hpx_extras_ring_num (long nside, double z, long shift)
{
//    !=======================================================================
//    ! ring = ring_num(nside, z [, shift=])
//    !     returns the ring number in {1, 4*nside-1}
//    !     from the z coordinate
//    ! usually returns the ring closest to the z provided
//    ! if shift < 0, returns the ring immediatly north (of smaller index) of z
//    ! if shift > 0, returns the ring immediatly south (of smaller index) of z
//    !
//    !=======================================================================
  long iring;
  double my_shift = (double) shift * 0.5;
  double nside_d = (double) nside;

  //    !     ----- equatorial regime ---------
  iring = (long) round(nside_d*(2.0-1.5*z) + my_shift); //replaced NINT() by round()

  //    !     ----- north cap ------
  if (z > TWOTHIRDS)
  {
    iring = (long) round(nside_d*sqrt(3.0*(1.0-z)) + my_shift);
    if (iring == 0) iring = 1;
  }

  //    !     ----- south cap -----
  if (z < -TWOTHIRDS )
  {
    //       ! beware that we do a -shift in the south cap
    iring = (long) round(nside_d*sqrt(3.0*(1.0+z)) - my_shift);
    if (iring == 0) iring = 1;
    iring = 4*nside - iring;
  }

  return (long) iring;
}

double hpx_extras_ring2z (long nside, long ir)
{
  //    !=======================================================================
  //    !     returns the z coordinate of ring ir for Nside
  //    ! 2009-03-25: accepts Nside > 8192
  //    !=======================================================================
  //    integer(kind=I4B), intent(in) :: ir
  //    integer(kind=I4B), intent(in) :: nside
  //    real(kind=DP)                 :: z
  //
  //    real(DP)     :: fn, tmp
  //    !=======================================================================
  double tmp, z;
  double fn = (double) nside;
  if (ir < nside) //  polar cap (north)
  {
    tmp = (double) ir;
    z = 1.0 - SQR(tmp) / (3.0 * SQR(fn));
  }
  else if (ir < 3*nside) // tropical band
  {
    z = (double) (2*nside-ir) * 2.0 / (3.0 * fn);
  }
  else                   // polar cap (south)
  {
    tmp = (double) (4*nside - ir);
    z = -1.0 + SQR(tmp) / (3.0 * SQR(fn));
  }
  return z;
}

double hpx_extras_fudge_query_radius(long nside, double radius_in, BOOL do_quadratic)
{
  //  !=======================================================================
  //    !  radius_out = fudge_query_radius( nside, radius_in, QUADRATIC=)
  //    ! 
  //    !  with 
  //    !    radius_out = radius_in + fudge            (default)
  //    !  or
  //    !    radius_out = sqrt(radius_in^2 + fudge^2)  (quadratic)
  //    ! 
  //    !  if absent, radius_in = 0
  //    ! where
  //    !    fudge = factor(nside) * Pi / (4 *Nside)
  //    !  with factor = sqrt( 5/9 + (8/Pi)^2 / 5 * (1-1/(2*Nside)) )
  //    ! 
  //    !  an upper bound of the actual largest radius 
  //    !  (determined by pixel located at z=2/3 and its North corner).
  //    !  
  //    ! 
  //    !  2011-10-14, EH, v1.0
  //    !=======================================================================
  const double fudge_const1 = (5.0/9.0);
  const double fudge_const2 = 1.29691115062192347448165712908451377797579231580476; // wcalc -P50 "(8/pi)**2 / 5"
                              
  double factor = sqrt(fudge_const1 + fudge_const2*(1.0-1.0/(2.0*(double)nside)));
  double fudge  = factor * M_PI / (4.0 * (double) nside); //! increase effective radius
  double result;
  if (do_quadratic) result = sqrt(SQR(radius_in) + SQR(fudge));
  else              result = radius_in + fudge;
  
  return MIN(result, M_PI);
}

//  !=======================================================================
//in c version still assume that pvector0[] is a one-indexed array
//but assume that it is already normalised to have length = unity
int hpx_extras_discphirange_at_z(double *pvector0, double radius_rad, double *pz, long nz, double *pdphi, double *pphi0)
{
  //    !=======================================================================
  //    !  for a disc centered on  vcenter and given radius,
  //    !  and for location z=cos(theta)
  //    !  returns dphi such that the disc has   abs(phi-phi0) <= dphi
  //    !
  //    ! solving disc equation on sphere:
  //    !  sin(theta) sin(theta0) cos(phi-phi0) + cos(theta) cos(theta0) >= cos(radius)
  //    !
  //    !=======================================================================
  const double tolerance = 1.0e-12;
  double a, b, c, cosdphi;
  long i;
  double cosang = cos(radius_rad);
  double norm =  sqrt(SQR(pvector0[1]) + SQR(pvector0[2]) + SQR(pvector0[3]));
  double x0 = (double) pvector0[1]/norm;
  double y0 = (double) pvector0[2]/norm;
  double z0 = (double) pvector0[3]/norm;

  if (DBL_ISZERO(x0) && DBL_ISZERO(y0))
    *pphi0 = (double) 0.0;
  else
    *pphi0 = (double) atan2(y0, x0) ; //  ! in ]-Pi, Pi]  cosphi0 = cos(phi0)

  a = SQR(x0) + SQR(y0); //  sin(theta0)^2  
  for(i=1; i<=nz; i++) //note one-based array indexing
  {
    pdphi[i] = (double) -1000.0; // default value for out of disc

    b = cosang - pz[i]*z0; // cos(rad) - cos(theta)cos(theta0)
    if (DBL_ISZERO(a)) // poles
    {
      if (b <= 0.0) pdphi[i] = M_PI;
    }
    else
    {
      c = MAX(1.0 - SQR(pz[i]), tolerance); // ! sin(theta)^2;
      cosdphi = b / sqrt(a*c);
      if (cosdphi       < -1.0) pdphi[i] = M_PI; // ! all the pixels at this elevation are in the disc
      if (fabs(cosdphi) <= 1.0) pdphi[i] = acos(cosdphi); // ! in [0,Pi]
    }
  }
  return 0;
}

//  !=======================================================================
int hpx_extras_pixels_per_ring(long nside, long ring, long *pnpr, long *pkshift, long *pnpnorth)
{
//  !=======================================================================
//    ! for a given Nside and ring index in [1,4*Nside-1], 
//    ! returns the number of pixels in ring, their shift (0 or 1) in azimuth
//    ! and the number of pixels in current ring and above (=North)
//    !
//    ! NB: 'rings' 0 and 4*Nside respectively are North and South Poles
//    !=======================================================================
//    integer(i4b), intent(in) :: nside, ring
//    integer(i4b), intent(out) :: npr, kshift
//    integer(i8b), intent(out), optional :: npnorth
//    integer(i8b) :: ncap, npix, ir
  long npr;
  long kshift;
  
//    ! number of pixels in current ring
  npr = MIN(MIN(nside, ring),4*nside-ring) * 4;
  //    ! shift
  kshift = (ring + 1) % 2; // ! 1 for even, 0 for odd
  if (nside == 1) kshift = 1 - kshift; // ! except for Nside=1
  if (npr < 4*nside) kshift = 1 ; // ! 1 on polar cap

  if (pnpnorth != NULL )
  {
    long npnorth;
    if (ring <= nside) //  in North cap
      npnorth = (long) (ring*(ring+1)*2);
    else if (ring <= 3*nside) // in Equatorial region
    {
      long ncap = nside*(nside+1)*2;
      long ir = ring - nside;
      npnorth = (long) (ncap + 4*nside*ir);
    }
    else //! in South cap
    {
      long npix = nside2npix((long) nside);
      long ir = 4*nside-ring - 1;//  count ring from south
      npnorth = (long) (npix - ir*(ir+1)*2);
    }
    *pnpnorth = (long) npnorth;
  }

  *pnpr = (long) npr;
  *pkshift = (long) kshift;

  return 0;
}
//  !=======================================================================

//  !=======================================================================
//keep original one-indexed arrays
//convert pringphi from a 2d array to a 3 1d arrays , clunky but works 
int hpx_extras_pixels_on_edge(long nside, long irmin, long irmax, double phi0, double *pdphi,
                              long *pringphi1, long *pringphi2, long *pringphi3, long *pngr)
{
  long thisring;
  long ngr = 0;
  *pngr = (long) 0;
  for(thisring = irmin; thisring<= irmax; thisring++)
  {
    long npr, kshift;
    long ir = thisring - irmin + 1;
    if ( hpx_extras_pixels_per_ring((long) nside, (long) thisring, (long*) &npr,  (long*) &kshift, NULL)) return 1; 
    
    if (pdphi[ir] >= M_PI) // full ring
    {
      ngr++;
      pringphi1[ngr] = (long) thisring;
      pringphi2[ngr] = (long) 0;
      pringphi3[ngr] = (long) npr-1;
    }
    else if ( pdphi[ir] >= 0.0) // partial ring
    {
      double shift  = kshift * 0.5;
      long iphi_low = (long) ceil (npr * (phi0 - pdphi[ir]) / TWOPI - shift);
      long iphi_hi  = (long) floor(npr * (phi0 + pdphi[ir]) / TWOPI - shift);
      if (iphi_hi >= iphi_low) // pixel center in range
      {
        ngr++;
        pringphi1[ngr] = (long) thisring;
        pringphi2[ngr] = (long) iphi_low % npr;             //note difference between fortran modulus and c mod
        while ( pringphi2[ngr] < 0 ) pringphi2[ngr] += npr; // make sure that the modulus is +ve
        pringphi3[ngr] = (long) iphi_hi % npr;              //note difference between fortran modulus and c mod
        while ( pringphi3[ngr] < 0 ) pringphi3[ngr] += npr; // make sure that the modulus is +ve
      }
    }    
  }
  *pngr = (long) ngr;
  return 0;
}

//  !=======================================================================
int hpx_extras_find_pixel_bounds (long nside, long nsboost, long iring, long iphi, double *pphiw, double *pphie)
{
  double *pf = NULL, *pf1 = NULL, *pphiw_t = NULL, *pphie_t = NULL;
  long npr, kshift, nq, i;
  BOOL transition;
  if ( nsboost <= 0 ) return 1;
  if ( hpx_extras_pixels_per_ring((long) nside, (long) iring, (long*) &npr, (long*) &kshift, NULL) ) return 1;

  if ((pf        = (double*) calloc((size_t) 2*nsboost+1+1, sizeof(double))) == NULL ||
      (pf1       = (double*) calloc((size_t) 2*nsboost+1+1, sizeof(double))) == NULL ||
      (pphiw_t   = (double*) calloc((size_t) 2*nsboost+1+1, sizeof(double))) == NULL ||
      (pphie_t   = (double*) calloc((size_t) 2*nsboost+1+1, sizeof(double))) == NULL ) return 1;
  
  for(i=0;i<=2*nsboost;i++)
    pf[i+1] = (double) ((i - nsboost)/(double) nsboost);

  nq = npr/4; // ! number of pixels on current ring in [0,Pi/2] (quadrant)
  transition = (BOOL) (iring == nside || iring == nside*3);

  if (nq == nside || transition) //! equatorial region (and transition rings)
  {
    double c0 = HALFPI * ((double) iphi + 0.5* (double) kshift) /  (double) nq; 
    for(i=0;i<=2*nsboost;i++)
    {
      pf1[i+1] = HALFPI * (1.0-fabs(pf[i+1]))*0.5 / (double) nq;
      pphiw[i+1] = (double) (c0 - pf1[i+1]);
      pphie[i+1] = (double) (c0 + pf1[i+1]);
      if (transition) // store for future use
      {
        pphiw_t[i+1] = pphiw[i+1];
        pphie_t[i+1] = pphie[i+1];
      }
    }
  }

  if (nq < nside || transition) // ! polar regions and transition rings
  {
    long quadrant = (long) (iphi / nq); //  ! quadrant in [0,3]
    long ip = iphi % nq; // ! in [0,nq-1]
    for(i=0;i<=2*nsboost;i++)
    {
      if (iring <= nside*2) pf1[i+1] = HALFPI / (double) ((double)nq + pf[i+1]) ;
      else                  pf1[i+1] = HALFPI / (double) ((double)nq - pf[i+1]) ; //! swap sign for South pole
    }
    for (i=1; i<=2*nsboost+1; i++)
    {
      double    cv = (double) pf1[i];
      double phiw1 = MIN(cv*(double) (   ip  ), (double)HALFPI);
      double phie1 = MIN(cv*(double) (   ip+1), (double)HALFPI);
      double phiw2 = MIN(cv*(double) (nq-ip-1), (double)HALFPI);
      double phie2 = MIN(cv*(double) (nq-ip  ), (double)HALFPI);
      pphiw[i] = (double) MAX(phiw1, (double)HALFPI - phie2) + ((double)quadrant * (double)HALFPI);
      pphie[i] = (double) MIN(phie1, (double)HALFPI - phiw2) + ((double)quadrant * (double)HALFPI);
    }
  }

  if (transition)
  {
    if (iring == nside) //then ! transition in N hemisphere
    {
      for (i=nsboost+2;i<=2*nsboost+1;i++)
      {
        pphiw[i] = pphiw_t[i];
        pphie[i] = pphie_t[i];
      }
    }
    else //! transition in S hemisphere
    {
      for (i=1;i<=nsboost+1;i++)
      {
        pphiw[i] = pphiw_t[i];
        pphie[i] = pphie_t[i];
      }
    }
  }

  if (pf     ) free (pf     ); pf      = NULL;
  if (pf1    ) free (pf1    ); pf1     = NULL;
  if (pphiw_t) free (pphiw_t); pphiw_t = NULL;
  if (pphie_t) free (pphie_t); pphie_t = NULL;
  
  return 0;
}
 //  !=======================================================================

//  !=======================================================================
int hpx_extras_check_edge_pixels(long nside, long nsboost,  long irmin, long irmax, double phi0,
                                 double *pdphi,
                                 long *pringphi1, long *pringphi2, long *pringphi3,
                                 long *pngr)
{
  long i, j, k, ngr_out, ngr_in,  nrh;
  double *pphiw = NULL, *pphie = NULL;
  long *pringphik = NULL;
  if (nsboost <= 1) return 0;
  if ( (pphiw    = (double*) calloc((size_t) 2*nsboost+1+1, sizeof(double))) == NULL ||
       (pphie    = (double*) calloc((size_t) 2*nsboost+1+1, sizeof(double))) == NULL) return 1;  //TODO free

  ngr_in = (long) *pngr;
  nrh = irmax-irmin+1;
  for(i=1;i<=ngr_in;i++) // ! loop on low-res rings
  {
    long i0 = pringphi1[i] * nsboost - nsboost - irmin;
    for(k=-1;k<=1;k+=2) //! West and East side of disc
    {
      BOOL flag = TRUE;
      if ( k< 0) pringphik = (long*) pringphi2;
      else       pringphik = (long*) pringphi3;
      //      long kk = (k+5)/2; // ! 2 or 3
      while (flag) // in place of dodgy GOTO statements
      {
        long iphi = pringphik[i];
        if (pringphi2[i] <= pringphi3[i] &&  iphi >= 0 )
        {
          if ( hpx_extras_find_pixel_bounds((long) nside, (long) nsboost, (long) pringphi1[i], (long) iphi,
                                            (double*) pphiw, (double*) pphie)) return 1; 
          for(j=1; j<=2*nsboost+1; j++)
          {
            if (i0+j >= 1 && i0+j <= nrh)
            {
              double phic = (double) (pphie[j]+pphiw[j])*0.5; //! pixel center
              double dph  = (double) (pphie[j]-pphiw[j])*0.5 + pdphi[i0+j]; // ! pixel size + circle radius
              double dd   = (double) fabs(phi0 - phic); //    ! distance from disc center to pixel border sample
              dd =  (double) MIN(dd,  (double) TWOPI - dd); // ! in [0,Pi]
              if (dd <= dph)
              {
                flag = FALSE;
                break; //break out of for loop
              }            
            }
          }
          if ( flag ) pringphik[i] = (long) iphi - k;
            //            if ( kk == 2 ) pringphi2[i] = iphi - k;  // pixel not in disc, move edge pixel inwards
            //            else           pringphi3[i] = iphi - k; // ditto
            //
        }
        else {  flag = FALSE; }
      } //end while loop
    }// end loop on side
  }// end loop on low-res rings

  // remove empty rings
  ngr_out = 0;
  for(i=1;i<=ngr_in;i++)
  {
    long diff = pringphi3[i] - pringphi2[i];
    if (pringphi2[i] >=0 &&
        pringphi3[i] >=0 &&
        diff != -2       &&
        diff != -1)
    {
      ngr_out++;
      pringphi1[ngr_out] = pringphi1[i];
      pringphi2[ngr_out] = pringphi2[i];
      pringphi3[ngr_out] = pringphi3[i];
    }
  }
  // ! set empty rings to -1
  for(i=ngr_out+1;i<=ngr_in;i++)
  {
    pringphi2[i] = -1;
    pringphi3[i] = -1;
  }
  *pngr = (long) ngr_out;
  if ( pphiw ) free (pphiw ) ; pphiw = NULL;
  if ( pphie ) free (pphie ) ; pphie = NULL;

  return 0;
}
//  !=======================================================================

//  !=======================================================================
int hpx_extras_discedge2fulldisc( long nside, long *pringphi1, long *pringphi2, long *pringphi3,
                                  long ngr,   long maxlist,    long *plist,     long *pnlist)
{
    
  long  j,nlist = 0;
  *pnlist = (long) 0;
  if (ngr <= 0) //then ! no valid rings
  {
    plist[1] = -1; 
    return 0;
  }

  for(j=0; j<=ngr-1; j++)
  {
    long ir = pringphi1[j+1];//             ! current ring, in [1, nl4-1]
    long nr, kshift, npc;
    long my_low = pringphi2[j+1];
    long i;
    if ( hpx_extras_pixels_per_ring((long) nside, (long) ir, (long*) &nr, (long*) &kshift, (long*) &npc)) return 1;
    if (my_low >= 0)
    {
      long my_hi  = pringphi3[j+1];
      long np = my_hi - my_low; //     ! in [-nr+1, nr-1]
      np = np % nr; //  ! dthis used to be a modulo, so make sure that result is +ve
      while(np < 0 ) np += nr;
      np ++;
      np = MIN(np, nr);
      if (nlist + np > maxlist)
      {
        fprintf(stderr, "Pixel query: too many pixels found for output list provided.\n");
        return 1;
      }
      for(i=0;i<=np-1;i++)
      {
        long ip = (my_low + i) % nr;
        plist[nlist+1+i] = npc - nr + ip ; // ! fill final list
      }
      nlist += np;
    }
  }
    
  if (nlist == 0) plist[1] = -1;
  *pnlist = (long) nlist;
  return 0;
}

// Ported to C from original fortran90 http://healpix.jpl.nasa.gov/html/subroutinesnode72.htm#sub:query_disc
// ${HEALPIX}/src/f90/mod/pixel_routines.F90
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
//
//=======================================================================
//
//      query_disc (Nside, Vector0, Radius, Listpix, Nlist[, Nest, Inclusive])
//      ----------
//      routine for pixel query in the RING or NESTED scheme
//      all pixels within an angular distance Radius of the center
//
//     Nside    = resolution parameter (a power of 2)
//     Vector0  = central point vector position (x,y,z in double precision)
//     Radius   = angular radius in RADIAN (in double precision)
//     Listpix  = list of pixel closer to the center (angular distance) than Radius
//     Nlist    = number of pixels in the list
//     nest  (OPT), :0 by default, the output list is in RING scheme
//                  if set to 1, the output list is in NESTED scheme
//     inclusive (OPT) , :0 by default, only the pixels whose center
//                       lie in the triangle are listed on output
//                  if set to 1, all pixels overlapping the triangle are output
//
//      * all pixel numbers are in {0, 12*Nside*Nside - 1}
//     NB : the dimension of the listpix array is fixed in the calling
//     routine and should be large enough for the specific configuration
//
//      lower level subroutines called by getdisc_ring :
//       (you don't need to know them)
//      x=fudge_query_radius()
//      x=ring_num (nside, ir)
//      x=ring2z()
//      discphirange_at_z()
//      pixels_on_edge()
//      check_edge_pixels()
//      discedge2fulldisc()
//      -------
//
// v1.0, EH, TAC, ??
// v1.1, EH, Caltech, Dec-2001
// v1.2, EH, IAP, 2008-03-30: fixed bug appearing when disc centered on either pole
// 2009-06-17: deals with Nside > 8192
// 2011-06-09: uses ring2z
// 2011-10-18: improve fudge radius determination.
// New algorithm for Inclusive case: test boundary of edge pixels on each ring
//    2013-04-02: bug correction in query_disc in inclusive mode
//    2014-07-07: bug correction in discedge2fulldisc
//    !=======================================================================
int hpx_extras_query_disc ( long nside, double long0_deg, double lat0_deg, double radius_deg,
                            long maxlist, long *plistpix, long *pnlist, BOOL is_nest, BOOL do_inclusive)
{

  //  long i;
  long irmin, irmax, iz, ip, nr, ngr;
  double radius_eff;
  double phi0;
  double rlat0, rlat1, rlat2, zmin, zmax;
  double *pztab = NULL, *pdphitab = NULL;
  long *pringphi1 = NULL;
  long *pringphi2 = NULL;
  long *pringphi3 = NULL;
  double radius = radius_deg * DEG_TO_RAD; 
  double pvector0[3+1];
  long nlist;

//  fprintf(stdout, "DEBUG_query_disc nside      = %ld\n", nside);    
//  fprintf(stdout, "DEBUG_query_disc long0_deg  = %g\n",  long0_deg);
//  fprintf(stdout, "DEBUG_query_disc lat0_deg   = %g\n",  lat0_deg); 
//  fprintf(stdout, "DEBUG_query_disc radius_deg = %g\n",  radius_deg);
//  fprintf(stdout, "DEBUG_query_disc maxlist    = %ld\n", maxlist);
//  fflush(stdout);

  if ( (pztab     = (double*) calloc((size_t) 4*nside-1+1, sizeof(double))) == NULL ||
       (pdphitab  = (double*) calloc((size_t) 4*nside-1+1, sizeof(double))) == NULL ||
       (pringphi1 = (long*)   calloc((size_t) 4*nside-1+1, sizeof(long)  )) == NULL ||
       (pringphi2 = (long*)   calloc((size_t) 4*nside-1+1, sizeof(long)  )) == NULL ||
       (pringphi3 = (long*)   calloc((size_t) 4*nside-1+1, sizeof(long)  )) == NULL ) return 1;
  
  //    !     ---------- check inputs ----------------
  //  npix = (long) nside2npix((long) nside);

  if (radius_deg < 0.0 || radius_deg > 180.0)
  {
    return 1;
    //    write(unit=*,fmt="(a)") code//"> the angular radius is in RADIAN "
    //      write(unit=*,fmt="(a)") code//"> and should lie in [0,Pi] "
    //       call fatal_error("> program abort ")
  }

  radius_eff = radius;
  if (do_inclusive) radius_eff = hpx_extras_fudge_query_radius((long) nside, (double) radius, (BOOL) FALSE); 

  //  fprintf(stdout, "DEBUG_query_disc radius_eff = %g rad (=%g deg)\n", radius_eff, radius_eff*RAD_TO_DEG); fflush(stdout);

  //http://healpix.jpl.nasa.gov/html/csubnode6.htm#csub:ang2vec
  // x = sin(theta)*cos(phi);
  // y = sin(theta)*sin(phi);
  // z = cos(theta);
  // theta = 90. - dec;
  // phi = ra;
  pvector0[0] = NAN;
  pvector0[1] = (double) sind(90.0 - lat0_deg) * cosd(long0_deg);
  pvector0[2] = (double) sind(90.0 - lat0_deg) * sind(long0_deg);
  pvector0[3] = (double) cosd(90.0 - lat0_deg);
  //  fprintf(stdout, "DEBUG_query_disc pvector[1,2,3] = [%g,%g,%g]\n", pvector0[1], pvector0[2], pvector0[3]); fflush(stdout);

  //assume that this already gives a unit vector
  //  // ---------- circle center -------------
  //  norm_vect0 =  SQRT(SQR(pvector0[1]) + SQR(pvector0[2]) + SQR(pvector0[3]));
  //  z0 = pvector0[3] / norm_vect0;

  // --- coordinate z of highest and lowest points in the disc ---
  //  rlat0  = asin(z0)    ; // ! latitude in RAD of the center
  rlat0  = lat0_deg*DEG_TO_RAD;
  rlat1  = rlat0 + radius_eff;
  rlat2  = rlat0 - radius_eff;
  //  fprintf(stdout, "DEBUG_query_disc [rlat0,rlat1,rlat2] = [%g,%g,%g]\n", rlat0,rlat1,rlat2); fflush(stdout);

  if (rlat1 >= HALFPI)  zmax =  1.0;
  else                  zmax = sin(rlat1);
  irmin = (long) hpx_extras_ring_num((long) nside, (double) zmax, (long) 0);  
  irmin = MAX(1, irmin - 1);      // start from a higher point, to be safe

  if (rlat2 <= -HALFPI) zmin = -1.0;
  else                  zmin = sin(rlat2);
  irmax = (long) hpx_extras_ring_num((long) nside, (double) zmin, (long) 0);
  irmax = MIN(4*nside-1, irmax + 1); // ! go down to a lower point
  nr = irmax-irmin+1; // ! in [1, 4*Nside-1]

  //  fprintf(stdout, "DEBUG_query_disc [zmin,zmax] = [%g,%g]\n", zmin, zmax); fflush(stdout);
  //  fprintf(stdout, "DEBUG_query_disc [irmin,irmax,nr] = [%ld,%ld,%ld]\n", irmin, irmax,nr); fflush(stdout);

  
  //  for(iz = MIN(irmin,irmax); iz <= MAX(irmin,irmax); iz++ )
  for(iz = irmin; iz <= irmax; iz++ )
  {
    pztab[iz-irmin+1] = (double) hpx_extras_ring2z((long) nside, (long) iz); //
    //    fprintf(stdout, "DEBUG_query_disc iz=%8ld pztab[iz-irmin+1] = %10g\n", iz,  pztab[iz-irmin+1] ); fflush(stdout);
  }
  
  if ( hpx_extras_discphirange_at_z((double*) pvector0, (double) radius_eff,
                                    (double*) pztab, (long) nr, (double*) pdphitab, (double*) &phi0)) return 1;

//  fprintf(stdout, "DEBUG_query_disc phi0 = %g\n", phi0); fflush(stdout);
//  for(i = 1; i <= nr; i++ )
//    fprintf(stdout, "DEBUG_query_disc i=%8ld pztab[i]=%10g pdphitab[i]=%10g \n",
//            i,  pztab[i], pdphitab[i]);
//  fflush(stdout);

  if ( hpx_extras_pixels_on_edge((long) nside, (long) irmin, (long) irmax, (double) phi0, (double*) pdphitab,
                                 (long*) pringphi1, (long*) pringphi2, (long*) pringphi3, (long*) &ngr)) return 1; 

//  fprintf(stdout, "DEBUG_query_disc ngr = %ld\n", ngr); fflush(stdout);
//  for(i = 1; i <= ngr; i++ )
//    fprintf(stdout, "DEBUG_query_disc i=%8ld pztab[i]=%10g pdphitab[i]=%10g pringphi1,2,3[i]=[%6ld,%6ld,%6ld]\n",
//            i,  pztab[i], pdphitab[i], pringphi1[i], pringphi2[i], pringphi3[i]);

  
  if (do_inclusive) // sample edge pixels at larger Nside
  {
    //    long i;
    const long ns_max8 = 268435456;  // 2^28 = largest nside available
    const long nsboost = 16;  //set to value in f90 library
    long nsideh = MIN(ns_max8, nside * nsboost);
    double radiush = (double) hpx_extras_fudge_query_radius((long) nsideh, (double) radius, (BOOL) TRUE); 
    long nrh;
    //    fprintf(stdout, "DEBUG_query_disc nsideh = %ld\n", nsideh); fflush(stdout);
    //    fprintf(stdout, "DEBUG_query_disc radiush = %g rad (=%g deg)\n", radiush, radiush*RAD_TO_DEG); fflush(stdout);

    irmin = (long) hpx_extras_ring_num((long) nsideh, (double) zmax, (long)+1);// ! shifted South
    irmax = (long)hpx_extras_ring_num((long) nsideh, (double) zmin, (long)-1);// ! shifted North 
    nrh = (long) (irmax - irmin + 1);
    if ( nrh > 0 )
    {
      double *pzlist = NULL, *pdphilist = NULL;
    
      if ( (pzlist    = (double*) calloc((size_t) nrh+1, sizeof(double))) == NULL ||
           (pdphilist = (double*) calloc((size_t) nrh+1, sizeof(double))) == NULL ) return 1;
      
      for(iz = irmin; iz <= irmax; iz++)
      {
        pzlist[iz-irmin+1] = (double) hpx_extras_ring2z((long) nsideh, (long) iz);
        //        fprintf(stdout, "DEBUG_query_disc iz=%8ld pzlist[%4ld] = %10g\n", iz,  iz-irmin+1, pzlist[iz-irmin+1] ); fflush(stdout);
      }
      
      if ( hpx_extras_discphirange_at_z((double*) pvector0, (double) radiush,
                                        (double*) pzlist, (long) nrh, (double*) pdphilist, (double*) &phi0)) return 1;
      
//      fprintf(stdout, "DEBUG_query_disc phi0 = %g\n", phi0); fflush(stdout);
//      for(i = 1; i <= nrh; i++ )
//        fprintf(stdout, "DEBUG_query_disc i=%8ld pzlist[i]=%10g pdphilist[i]=%10g \n",
//                i,  pzlist[i], pdphilist[i]);
//       fflush(stdout);
      
      
      if ( hpx_extras_check_edge_pixels((long) nside, (long) nsboost, (long) irmin, (long) irmax, (double) phi0,
                                        (double*) pdphilist,
                                        (long*) pringphi1, (long*) pringphi2, (long*) pringphi3, (long*) &ngr)) return 1; 
      //      fprintf(stdout, "DEBUG_query_disc ngr = %ld\n", ngr); fflush(stdout);
      
      
      if ( pzlist    ) free ( pzlist   ); pzlist    = NULL;
      if ( pdphilist ) free ( pdphilist); pdphilist = NULL;
    }
    //    else
      //      fprintf(stdout, "DEBUG_query_disc [irmin,irmax,nrh] = [%ld,%ld,%ld]\n", irmin, irmax,nrh); fflush(stdout);

  }
  
  if ( hpx_extras_discedge2fulldisc((long) nside,
                                    (long*) pringphi1, (long*) pringphi2, (long*) pringphi3,
                                    (long) ngr, (long) maxlist, (long*) plistpix, (long*) &nlist)) return 1;

  //  fprintf(stdout, "DEBUG_query_disc nlist = %ld\n", nlist); fflush(stdout);
  if ( is_nest == TRUE ) //convert pixel numbers from ring to nested format
  {
    for(ip=1;ip<=nlist-1+1;ip++)
    {
      long result;
      ring2nest((long) nside, (long) plistpix[ip], (long*) &(result)); 
      //fprintf(stdout, "%s %ld %ld -> %ld\n", DEBUG_PREFIX, nside, plistpix[ip], result);    fflush(stdout);
      
      plistpix[ip] = (long) result;
    }
  }
  if ( pztab    ) free ( pztab   );  pztab     = NULL;
  if ( pdphitab ) free ( pdphitab);  pdphitab  = NULL;
  if ( pringphi1) free ( pringphi1); pringphi1 = NULL;
  if ( pringphi2) free ( pringphi2); pringphi2 = NULL;
  if ( pringphi3) free ( pringphi3); pringphi3 = NULL;
  *pnlist = (long) nlist;

  if (nlist < 0) return 1;
  return 0;
}






////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
int hpx_pixlist_init (hpx_pixlist_struct *pPixlist)
{
  if ( pPixlist == NULL ) return 1;
  pPixlist->nside = 0;
  pPixlist->npix = 0;
  pPixlist->pix_area_deg2 = 0.0;
  pPixlist->pix_size_deg = 0.0;
  pPixlist->pPix = NULL;
  return 0;
}

int hpx_pixlist_setup (hpx_pixlist_struct *pPixlist, long nside)
{
  long i;
  if ( pPixlist == NULL ) return 1;
  if ( pPixlist->pPix ) return 1;   //error if space is already allocated
  pPixlist->nside = nside;
  pPixlist->npix  = nside2npix(pPixlist->nside); 
  // allocate some space
  if ((pPixlist->pPix = (hpx_pix_struct*) calloc((size_t) pPixlist->npix, sizeof(hpx_pix_struct))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  pPixlist->pix_area_deg2 = (double) FULL_SKY_AREA_SQDEG / (double) (pPixlist->npix);
  pPixlist->pix_size_deg  = sqrt(pPixlist->pix_area_deg2); //this is an approximation

  //calculate the details of each pixel
  //todo only deal with pixels at the desired resolution level
  for(i=0;i<pPixlist->npix;i++)
  {
    double theta, phi;
    hpx_pix_struct *pPix = (hpx_pix_struct*) &(pPixlist->pPix[i]);
    pPix->index = i;
    pix2ang_nest((long) pPixlist->nside, (long) pPix->index, (double*) &theta, (double*) &phi);
    pPix->ra0 = phi * RAD_TO_DEG;
    pPix->dec0  = (M_PI_2 - theta) * RAD_TO_DEG;
    //remove the call to sincos to improve portability
    //compiler optimisations should do it anyway 
    pPix->sin_dec0 = sind((double)pPix->dec0);
    pPix->cos_dec0 = cosd((double)pPix->dec0);
    //(void) sincos((double) DEG_TO_RAD*pPix->dec0, (double*) &(pPix->sin_dec0), (double*) &(pPix->cos_dec0));

    pPix->nMembers = 0;
    pPix->in_bounds_flag = TRUE; //TODO base this on whether pixel lies within the survey Dec bounds
    // allocate some space
    pPix->pMembers = NULL;
    pPix->array_length = (int) 0; // only assign memory when needed
    //    pPix->array_length = (int) HPX_MEMBERS_PER_CHUNK;
//    if ((pPix->pMembers = (void*) calloc((size_t) pPix->array_length, sizeof(void*))) == NULL )
//    {
//      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
//      return 1;
//    }
  }
  
  return 0;
}


int hpx_pixlist_free (hpx_pixlist_struct *pPixlist)
{
  long i;
  if ( pPixlist == NULL ) return 1;
  for(i=0;i<pPixlist->npix;i++)
  {
    hpx_pix_struct *pPix = (hpx_pix_struct*) &(pPixlist->pPix[i]);
    if ( pPix->pMembers ) free ( pPix->pMembers );
    pPix->pMembers = NULL;
  }
  if ( pPixlist->pPix ) free (pPixlist->pPix); pPixlist->pPix = NULL;
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
