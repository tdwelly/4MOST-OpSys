// -*-mode:C; compile-command:"make -f fits_helper_lib.make"; -*- 
//----------------------------------------------------------------------------------
//--
//--    Filename: fits_helper_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
#define CVS_REVISION "$Revision: 1.7 $"
#define CVS_DATE     "$Date: 2015/08/18 11:42:08 $"
//--    Description:
//--    Use: This module contains extensions which aid accessing FITS files
//--         
//--
//--

#include "fits_helper_lib.h"


#define ERROR_PREFIX   "#-fits_helper_lib.c : Error   :"
#define WARNING_PREFIX "#-fits_helper_lib.c : Warning :"
#define COMMENT_PREFIX "#-fits_helper_lib.c : Comment :"
#define DEBUG_PREFIX   "#-fits_helper_lib.c : DEBUG   :"

////////////////////////////////////////////////////////////////////////
//functions


int fhelp_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}


int fhelp_test_if_file_is_fits(const char* str_filename)
{
  fitsfile *fptr;     // FITS file pointer
  int status = 0;    // CFITSIO status value
  int result;
  
  if (fits_open_file((fitsfile**) &fptr, str_filename, READONLY, &status) == 0 )
  {
    result = (int) FHELP_FILE_FORMAT_CODE_FITS;
  }
  else
  {
    if (status == READ_ERROR || status == UNKNOWN_REC )
    {
      result = (int) FHELP_FILE_FORMAT_CODE_NOT_FITS;
    }
    else
    {
      result = (int) FHELP_FILE_FORMAT_CODE_ERROR;
      fits_report_error(stderr, status);
    }    
  }
  status = 0;
  fits_close_file(fptr, &status);
  return (int) result;
}

int fhelp_test_if_file_is_fits_table(const char* str_filename)
{
  fitsfile *fptr;     // FITS file pointer
  int status = 0;    // CFITSIO status value
  int result;
  
  if (fits_open_table((fitsfile**) &fptr, str_filename, READONLY, &status) == 0 )
  {
    //    int hdunum;
    //    int hdutype;
    //    fits_get_hdu_num  ((fitsfile*) fptr, &hdunum);
    //    fprintf(stdout, "currently FILE=%s HDUNUM=%d\n", str_filename, hdunum);
    //    fflush(stdout);
    //    fits_get_hdu_type ((fitsfile*) fptr, &hdutype, &status);
    //    fprintf(stdout, "currently FILE=%s HDUNUM=%d HDUTYPE=%d\n", str_filename, hdunum, hdutype);
    //    fflush(stdout);
    result = (int) FHELP_FILE_FORMAT_CODE_FITS;
  }
  else
  {
    if (status == READ_ERROR || status == UNKNOWN_REC )
    {
      result = (int) FHELP_FILE_FORMAT_CODE_NOT_FITS;
    }
    else
    {
      if (status == NOT_TABLE )
      {
        result = (int) FHELP_FILE_FORMAT_CODE_NOT_CORRECT_FITS_TYPE;
      }
      else 
      {
        result = (int) FHELP_FILE_FORMAT_CODE_ERROR;
        fits_report_error(stderr, status);
      }
    }
  }
  status = 0;
  fits_close_file(fptr, &status);
  return (int) result;
}



int fhelp_test_if_file_is_fits_image(const char* str_filename)
{
  fitsfile *fptr;     // FITS file pointer
  int status = 0;    // CFITSIO status value
  int result;
  
  if (fits_open_image((fitsfile**) &fptr, str_filename, READONLY, &status) == 0 )
  {
    result = (int) FHELP_FILE_FORMAT_CODE_FITS;
  }
  else
  {
    if (status == READ_ERROR || status == UNKNOWN_REC )
    {
      result = (int) FHELP_FILE_FORMAT_CODE_NOT_FITS;
    }
    else
    {
      if (status == NOT_IMAGE )
      {
        result = (int) FHELP_FILE_FORMAT_CODE_NOT_CORRECT_FITS_TYPE;
      }
      else 
      {
        result = (int) FHELP_FILE_FORMAT_CODE_ERROR;
      }
      fits_report_error(stderr, status);
    }
  }
  status = 0;
  fits_close_file(fptr, &status);
  return (int) result;
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// fits_table_helper_struct routines

int fths_init (fits_table_helper_struct *pTable)
{
  int j;
  if ( pTable == NULL ) return 1;
  pTable->pFile = NULL;
  strncpy(pTable->str_filename, "", LSTR_MAX);
  strncpy(pTable->str_extname, "", STR_MAX);
  for ( j=0;j<FITS_TABLE_HELPER_MAX_COLS;j++)
  {
    int i;
    pTable->pttype[j] = (char*) pTable->ttype[j];
    pTable->ptform[j] = (char*) pTable->tform[j];
    pTable->ptunit[j] = (char*) pTable->tunit[j];
    for(i=0;i<FLEN_CARD;i++)
    {
      pTable->ttype[j][i]  = (char) '\0'; 
      pTable->tform[j][i]  = (char) '\0'; 
      pTable->tunit[j][i]  = (char) '\0';
      pTable->tcomm[j][i]  = (char) '\0';
    }
    //    pTable->ttype[i][0]  = (char*) ""; 
    //    pTable->tform[i][0]  = (char*) ""; 
    //    pTable->tunit[i][0]  = (char*) "";
    //    pTable->tcomm[i][0]  = (char*) "";
    pTable->colfmt[j]    = (int) 0;
    pTable->colnum[j]    = (int) j+1;
    pTable->colwidth[j]  = (LONGLONG) 1;
    pTable->pvoid[j]     = (void*) NULL;
  }
  pTable->ncols = (int) 0;
  pTable->current_row = (LONGLONG) 1;
  return 0;
}

int fths_addcol (fits_table_helper_struct* pTable,
                                     const char* ttype,
                                     const char* tform,
                                     const int   colfmt,
                                     const LONGLONG colwidth,
                                     const char* tunit,
                                     const char* tcomm)
{
  if ( pTable == NULL ) return 1;
  if ( pTable->ncols >= FITS_TABLE_HELPER_MAX_COLS ) return 1; //too many cols

  //check for invalid column names and formats
  if ( strlen(ttype) <= 0 || strlen(ttype) >= FLEN_CARD ) return 1;
  if ( strlen(tform) <= 0 || strlen(tform) >= FLEN_CARD ) return 1;
  //silently truncate the tunit and tcomm strings

  strncpy((char*) pTable->ttype[pTable->ncols], (char*) ttype, FLEN_CARD); 
  strncpy((char*) pTable->tform[pTable->ncols], (char*) tform, FLEN_CARD); 
  strncpy((char*) pTable->tunit[pTable->ncols], (char*) tunit, FLEN_CARD); 
  strncpy((char*) pTable->tcomm[pTable->ncols], (char*) tcomm, FLEN_CARD);
  pTable->colfmt[pTable->ncols]   = (int) colfmt;
  pTable->colwidth[pTable->ncols] = (LONGLONG) colwidth;

  //// debug print out
  //  fprintf(stdout, "%s Adding fits column: %3d %-16s %-8s %-12s %s\n", COMMENT_PREFIX,
  //          pTable->ncols,
  //          pTable->ttype[pTable->ncols],
  //          pTable->tform[pTable->ncols],
  //          pTable->tunit[pTable->ncols],
  //          pTable->tcomm[pTable->ncols]); fflush(stdout);

  pTable->ncols++;

  return 0;
}

int fths_create (fits_table_helper_struct *pTable,
                                     char* str_filename,
                                     char* str_extname,
                                     LONGLONG num_rows,
                                     BOOL clobber)
{
  int status = 0;
  int j;
  if ( pTable == NULL ) return 1;

  //add a "!" prefix if we are going to clobber the output
  //  fprintf(stdout, "clobber=%d = \"%s\"\n", clobber, (clobber?"!":"")); fflush(stdout);
  snprintf(pTable->str_filename, LSTR_MAX+1, "%s%s", (clobber?"!":""), str_filename);
  
  if (fits_create_file ((fitsfile**) &(pTable->pFile), (char*) pTable->str_filename, (int*) &status))
  {
    fits_report_error(stderr, status); // print any error messages
    fprintf(stderr, "%s  There was a problem creating the FITS file: \"%s\"\n",
            ERROR_PREFIX, pTable->str_filename);
    return 1;
  }

  strncpy(pTable->str_extname, (char*) str_extname, STR_MAX);

//  for(j=0;j<pTable->ncols;j++)
//  {
//    fprintf(stdout, "%s Creating table %s with fits column: %3d %-16s %-8s %-12s %s\n",
//            COMMENT_PREFIX,
//            pTable->str_extname,
//            j,
//            pTable->ttype[j],
//            pTable->tform[j],
//            pTable->tunit[j],
//            pTable->tcomm[j]);
//    fflush(stdout);
//  }
  
  if ( fits_create_tbl ((fitsfile*) pTable->pFile,
                        (int)      BINARY_TBL,
                        (LONGLONG) num_rows,
                        (int)      pTable->ncols,
                        (char**)   pTable->pttype,
                        (char**)   pTable->ptform,
                        (char**)   pTable->ptunit,
                        (char*)    pTable->str_extname,
                        (int*)     &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  for(j=0;j<pTable->ncols;j++)
  {
    //now add some comments to the column name keywords
    char str_keyname[STR_MAX];
    if ( strlen(pTable->tcomm[j]) > 0 )
    {
      snprintf(str_keyname, STR_MAX, "TTYPE%d", pTable->colnum[j]);
      if (fits_modify_comment ((fitsfile*)pTable->pFile,
                               (char*) str_keyname,
                               (char*) pTable->tcomm[j],
                               (int*) &status))
      {
        fits_report_error(stderr, status);  
        return 1;
      }
    }
  }

  fits_flush_file ((fitsfile*)pTable->pFile, (int*) &status);


  
  return 0;
}



int fths_write_row (fits_table_helper_struct *pTable, BOOL increment)
{
  const LONGLONG nelements = 1;
  int j;
  int status = 0;
  if ( pTable == NULL ) return 1;
  
  for(j=0;j<pTable->ncols;j++)
  {
    if ( pTable->pvoid[j] != NULL &&
         pTable->colfmt[j] > 0     ) //only write to cols that we have fully set up
    {
      fits_write_col ((fitsfile*) pTable->pFile,
                      (int)       pTable->colfmt[j],
                      (int)       pTable->colnum[j],
                      (LONGLONG)  pTable->current_row,
                      (LONGLONG)  nelements,
                      (LONGLONG)  pTable->colwidth[j],
                      (void*)     pTable->pvoid[j],
                      (int*)      &status);
      if ( status ) 
      {
        fprintf(stderr, "%s Error writing file=%s[%s][row=%ld][col=%d=%s]\n",
                ERROR_PREFIX,
                pTable->str_filename,
                pTable->str_extname,
                (long) pTable->current_row,
                (int) j,
                (char*) pTable->ttype[j]);
        fits_report_error(stderr, status);  
        return 1;
      }
    }
  }
  fits_flush_buffer ((fitsfile*)pTable->pFile, 0, (int*) &status);

  if ( increment) pTable->current_row++;
  return 0;
}



int fths_close (fits_table_helper_struct *pTable)
{
  int status = 0;
  if ( pTable == NULL ) return 1;
  if ( pTable->pFile == NULL ) return 0;  //this is ok
  fprintf(stdout, "%s Closing fits file: %s\n", COMMENT_PREFIX, pTable->str_filename); fflush(stdout);

  if ( fits_close_file((fitsfile*) pTable->pFile, (int*) &status))
  {
    fits_report_error(stderr, status);  
    return 1;
  }
  pTable->pFile = NULL;
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




