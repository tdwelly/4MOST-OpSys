# This is the makefile for hpx_extras_lib.c
include ../arch.make

IDIR = -I./ -I$(HEALPIX_PATH)/include -I$(CFITSIO_PATH)/include

OUTSTEM = hpx_extras_lib

OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))

DEP_LIBS  = 

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) define.h

#No main() so only make object library

$(OFILE)	:	$(DEP_HFILES) $(CFILE)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE)

clean   : 
	rm -f $(OFILE)

#end

