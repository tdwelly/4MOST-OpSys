# This is the makefile for geometry_lib.c
include ../arch.make

IDIR = -I./

OUTSTEM = geometry_lib

OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))

DEP_LIBS  = utensils_lib

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) define.h

#No main() so only make object library

$(OFILE)	:	$(DEP_HFILES) $(CFILE)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE)

clean   : 
	rm -f $(OFILE)

#end

