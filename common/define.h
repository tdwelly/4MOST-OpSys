//this is a general place for all the standard typedefs and #defines that I normally use
#ifndef DEFINE_H
#define DEFINE_H

//#define CVS_REVISION_DEFINE_H "$Revision: 1.10 $"
//#define CVS_DATE_DEFINE_H     "$Date: 2015/11/24 08:47:31 $"

#include <stdio.h>
#include <sys/types.h>

#ifndef GENERIC_PRINT_VERSION
  #define GENERIC_PRINT_VERSION(pFile,str__FILE__,strCVS_REVISION,strCVS_DATE,str__TIMESTAMP__,str__TIME__,str__DATE__,str__VERSION__) fprintf((pFile), "# %-30s (CVS: %-19s, %s; File timestamp: %s) compiled at %s %s (compiler version %s)\n",(str__FILE__),(strCVS_REVISION),(strCVS_DATE),(str__TIMESTAMP__),(str__DATE__),(str__TIME__),(str__VERSION__))
#endif

#ifndef GENERIC_PRINT_GIT_VERSION
  #define GENERIC_PRINT_GIT_VERSION(pFile,str__FILE__,strGIT_VERSION,strGIT_DATE,str__TIMESTAMP__,str__TIME__,str__DATE__,str__VERSION__) fprintf((pFile), "# %-30s (git: %-19s, %s; File timestamp: %s) compiled at %s %s (compiler version %s)\n",(str__FILE__),(strGIT_VERSION),(strGIT_DATE),(str__TIMESTAMP__),(str__DATE__),(str__TIME__),(str__VERSION__))
#endif


//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.10 $"
//#define CVS_DATE_H     "$Date: 2015/11/24 08:47:31 $"
inline static int define_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//


#ifndef BOOL_FLAG
#define BOOL_FLAG
//typedef unsigned int BOOL;         //boolean integer type (0 = false, all other values = TRUE)
typedef unsigned char BOOL;          //boolean integer type (0 = false, all other values = TRUE)
#endif


//uint is not defined when -std=gnu99
#ifndef UINT_FLAG
#define UINT_FLAG
typedef unsigned int uint;           // a normal sized unsigned integer type
#endif

//ulong is not defined when -std=gnu99
#ifndef ulong
  #ifndef ULONG_FLAG
    #define ULONG_FLAG
    typedef unsigned long ulong;           // a long sized unsigned integer type
  #endif
#endif

#ifdef __APPLE__
  #ifndef ulong
    #ifndef ULONG_FLAG
      #define ULONG_FLAG
      typedef unsigned long ulong;           // a long sized unsigned integer type
    #endif
  #endif
#endif


#ifndef UTINY_FLAG
#define UTINY_FLAG
typedef unsigned char utiny;          // a tiny unsigned integer type
#endif

#ifndef TINY_FLAG
#define TINY_FLAG
typedef char tiny;                    // a tiny signed integer type 
#endif

#ifndef USHORT_FLAG
#define USHORT_FLAG
typedef unsigned short ushort;          // a short unsigned integer type
#endif

#ifndef BYTE_FLAG
#define BYTE_FLAG
typedef unsigned char BYTE;
#endif


#ifndef MULTI_TYPE_FLAG
#define MULTI_TYPE_FLAG
typedef union {
  BOOL    bval;
  BYTE    Bval;
  tiny    tval;
  utiny  utval;
  int     ival;
  uint   uival;
  long    lval;
  ulong  ulval;
  float   fval;
  double  dval;
  char    cval;
  void     *pv;
} multi_type;
#endif

  
#ifndef SQR
#define SQR(x)  ((x) * (x))                //simple 'square a variable' macro
#endif

#ifndef SQUARE
#define SQUARE(x)  ((x) * (x))                //simple 'square a variable' macro
#endif

#ifndef CUBE
#define CUBE(x)  ((x) * (x) * (x))         //simple 'cube a variable' macro
#endif

#ifndef CLIP
#define CLIP(x,min,max)  ((x)<(min)?(min):((x)>(max)?(max):(x)))  //simple macro to clip a value into a range
#endif

#ifndef ROUND_TO_NEAREST
#define ROUND_TO_NEAREST(x,y) ((x)(0.5 + (y)))         //simple round to integer routine
#endif



#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

//#ifndef NAN
//#define NAN ((double) sqrt((double) -1.0))
//#endif

#ifndef MIN
#define MIN(x,y) ((x) < (y) ? (x) : (y))
#endif

//#ifndef MIN3
//#define MIN3(x,y,z) ((x) < (y) && (x) < (z) ? (x) : ((y) < (x) && (y) < (z) ? (y) : (z)))
//#endif

#ifndef MIN3
#define MIN3(x,y,z) (MIN((x),MIN(y,z)))
#endif

#ifndef MIN4
#define MIN4(w,x,y,z) (MIN(MIN(w,x),MIN(y,z)))
#endif

#ifndef MIN5
#define MIN5(v,w,x,y,z) (MIN((v),MIN4(w,x,y,z)))
#endif


#ifndef MAX
#define MAX(x,y) ((x) > (y) ? (x) : (y))
#endif


#ifndef MAX3
#define MAX3(x,y,z) ((x) > (y) && (x) > (z) ? (x) : ((y) > (x) && (y) > (z) ? (y) : (z)))
#endif



//round a floating point macro
#ifndef ROUND
#define ROUND(x) ( ((x) - ((long) (x))) >= 0.5 ? ((long) (x)) + 1 : (long) (x) )   
#endif

#ifndef BOOL2STR
#define BOOL2STR(x) (((x)?"yes":"no"))
#endif

#ifndef BOOL2YN
#define BOOL2YN(x) (((x)?"YES":"NO"))
#endif

#ifndef BOOL2TF
#define BOOL2TF(x) (((x)?"TRUE":"FALSE"))
#endif

//these comparisons return TRUE if x==y
//otherwise they return FALSE
#ifndef DBL_CMP
#define DBL_CMP(x,y) (fabs((x)-(y)) < DBL_EPSILON)
#endif

#ifndef FLT_CMP
#define FLT_CMP(x,y) (fabsf((x)-(y)) < FLT_EPSILON)
#endif

#ifndef DBL_ISZERO
#define DBL_ISZERO(x) (fabs(x) < DBL_EPSILON)
#endif

#ifndef FLT_ISZERO
#define FLT_ISZERO(x) (fabsf(x) < FLT_EPSILON)
#endif

#ifndef DBL_ISNOTZERO
#define DBL_ISNOTZERO(x) (fabs(x) >= DBL_EPSILON)
#endif

#ifndef FLT_ISNOTZERO
#define FLT_ISNOTZERO(x) (fabsf(x) >= FLT_EPSILON)
#endif

////cross product macro
#ifndef CROSSP
#define CROSSP(x1,y1,x2,y2,x3,y3) (((x2) - (x1))*((y3) - (y1)) - ((x3) - (x1))*((y2) - (y1)))
#endif

#ifndef FREETONULL
#define FREETONULL(x) if((x)){free((x)); (x)=NULL;}
#endif

//The following are the numerical values of some commonly used constants.
//It's a bit manky to define them all explicitly here, but the idea is to
//make sure that no computation is required to use these values at run time.

//wcalc -P50 "pi*2.0"
#ifndef TWOPI
#define TWOPI 6.28318530717958647692528676655900576839433879875021
#endif

//wcalc -P50 "pi*0.5"
#ifndef HALFPI
#define HALFPI 1.57079632679489661923132169163975144209858469968755
#endif

#ifndef TWOTHIRDS
#define TWOTHIRDS 0.666666666666666666666666666666666666666666666667
#endif



//wcalc -P50 "pi/180.0"
#ifndef DEG_TO_RAD
#define DEG_TO_RAD 0.01745329251994329576923690768488612713442871888541
#endif

//wcalc -P50 "180.0/pi"
#ifndef RAD_TO_DEG
#define RAD_TO_DEG 57.29577951308232087679815481410517033240547246656432
#endif

//wcalc -P50 "3600.*180.0/pi"
#ifndef RAD_TO_ARCSEC
#define RAD_TO_ARCSEC 206264.80624709635515647335733077861319665970087963155758
#endif


#ifndef DEG_TO_RA_HOURS
#define DEG_TO_RA_HOURS 0.03888888888888888888888888888888888888888888888888888889 // 1/15
#endif

#ifndef RA_HOURS_TO_DEG
#define RA_HOURS_TO_DEG 15.0
#endif


#ifndef DEG_TO_ARCSEC
#define DEG_TO_ARCSEC 3600.0
#endif

#ifndef DEG_TO_ARCMIN
#define DEG_TO_ARCMIN 60.0
#endif

#ifndef ARCSEC_TO_DEG
#define ARCSEC_TO_DEG 0.0002777777777777777777777777777777777777777777777777777777777778
#endif

#ifndef ARCMIN_TO_DEG
#define ARCMIN_TO_DEG 0.0166666666666666666666666666666666666666666666666666666666666667
#endif

//wcalc -P50 "pi/(3600.*180.0)"
#ifndef ARCSEC_TO_RAD
#define ARCSEC_TO_RAD 0.00000484813681109535993589914102357947975956353302
#endif

//wcalc -P50 "(pi/180.0)**2"
#ifndef SQRDEG_TO_STERAD
#define SQRDEG_TO_STERAD 0.00030461741978670859934674354937889355355906479651
#endif

//wcalc -P50 "(180.0/pi)**2"
#ifndef STERAD_TO_SQRDEG
#define STERAD_TO_SQRDEG 3282.80635001174379478169460799517550050122429938078818
#endif

#ifndef SQRDEG_TO_SQRARCSEC
#define SQRDEG_TO_SQRARCSEC 12960000.0  //3600*3600
#endif

//wcalc -P50 "(1/3600)**2"
#ifndef SQRARCSEC_TO_SQRDEG
//#define SQRARCSEC_TO_SQRDEG 7.716049383e-8  // 1/(3600*3600)
#define SQRARCSEC_TO_SQRDEG 0.00000007716049382716049382716049382716049382716049
#endif

//wcalc -P50 "(4*pi*((180/pi)**2))"
#ifndef FULL_SKY_AREA_SQDEG
#define FULL_SKY_AREA_SQDEG 41252.96124941927103129467146615572263933194017592631152
#endif


//multiplicative conversions
#ifndef SECS_TO_MINS
#define SECS_TO_MINS 0.0166666666666666666666666666666666666666666666666667
#endif
#ifndef SECS_TO_HOURS
#define SECS_TO_HOURS 0.000277777777777777777777777777777777777777777777778
#endif
#ifndef SECS_TO_DAYS
#define SECS_TO_DAYS  0.000011574074074074074074074074074074074074074074074
#endif

#ifndef MINS_TO_SECS
#define MINS_TO_SECS 60.0
#endif
#ifndef MINS_TO_HOURS
#define MINS_TO_HOURS 0.016666666666666666666666666666666666666666666666667
#endif
#ifndef MINS_TO_DAYS
#define MINS_TO_DAYS  0.000694444444444444444444444444444444444444444444444
#endif

#ifndef YEARS_TO_DAYS
#define YEARS_TO_DAYS 365.25            //this is crude but good enough
#endif

#ifndef DAYS_TO_SECS
#define DAYS_TO_SECS 86400.0
#endif
#ifndef DAYS_TO_MINS
#define DAYS_TO_MINS 1440.0
#endif
#ifndef DAYS_TO_HOURS
#define DAYS_TO_HOURS 24.0
#endif
#ifndef DAYS_TO_WEEKS
#define DAYS_TO_WEEKS (1.0/7.0)
#endif
#ifndef DAYS_TO_YEARS
#define DAYS_TO_YEARS (1.0/365.25)
#endif

#ifndef HOURS_TO_SECS
#define HOURS_TO_SECS 3600.0
#endif
#ifndef HOURS_TO_MINS
#define HOURS_TO_MINS 60.0
#endif
#ifndef HOURS_TO_DAYS
#define HOURS_TO_DAYS 0.041666666666666666666666666667
#endif


//wcalc -P50 "sqrt(3.)"
#ifndef SQRT_3
#define SQRT_3 1.73205080756887729352744634150587236694280525381038
#endif

//wcalc -P50 "sqrt(3.)/2."
#ifndef SQRT_3_OVER_2
#define SQRT_3_OVER_2 0.86602540378443864676372317075293618347140262690519
#endif

//wcalc -P50 "sqrt(3.)/-2."
#ifndef MSQRT_3_OVER_2
#define MSQRT_3_OVER_2 -0.86602540378443864676372317075293618347140262690519
#endif

//wcalc -P50 "sqrt(2.)/2."
#ifndef SQRT_2_OVER_2
#define SQRT_2_OVER_2 0.70710678118654752440084436210484903928483593768847
#endif

//wcalc -P50 "sqrt(2.)"
#ifndef SQRT_2
#define SQRT_2 1.41421356237309504880168872420969807856967187537695
#endif

//wcalc -P50 "1./sqrt(3.)"
#ifndef ONE_OVER_SQRT_3
#define ONE_OVER_SQRT_3 0.57735026918962576450914878050195745564760175127012
#endif

//wcalc -P50 "1./sqrt(2.)"
#ifndef ONE_OVER_SQRT_2
#define ONE_OVER_SQRT_2 0.70710678118654752440084436210484903928483593768847
#endif

////this section provides translations for the trig functions

#ifndef WCSLIB_WCSTRIG
#define WCSLIB_WCSTRIG
//double versions
#define cosd(x) cos( DEG_TO_RAD * (x) )
#define sind(x) sin( DEG_TO_RAD * (x) )
#define tand(x) tan( DEG_TO_RAD * (x) )
#define acosd(x) (RAD_TO_DEG * acos(x))
#define asind(x) (RAD_TO_DEG * asin(x))
#define atand(x) (RAD_TO_DEG * atan(x))
#define atan2d(y,x) (RAD_TO_DEG * atan2((y),(x)))
//float versions
#define cosfd(x) cosf( DEG_TO_RAD * (x) )
#define sinfd(x) sinf( DEG_TO_RAD * (x) )
#define tanfd(x) tanf( DEG_TO_RAD * (x) )
#define acosfd(x) (RAD_TO_DEG * acosf(x))
#define asinfd(x) (RAD_TO_DEG * asinf(x))
#define atanfd(x) (RAD_TO_DEG * atanf(x))
#define atan2fd(y,x) (RAD_TO_DEG * atan2f((y),(x)))
#endif


#ifndef  DEF_STRLEN
#define DEF_STRLEN 512 
#endif

#ifndef  DEF_LONGSTRLEN
//#define DEF_LONGSTRLEN 2048 
#define DEF_LONGSTRLEN 4096 
#endif


#ifndef  DEF_VERYLONGSTRLEN
#define DEF_VERYLONGSTRLEN 32768 
#endif


#ifndef CLOBBER2S
#define CLOBBER2S(x) ((x)?"!":"")
#endif

// derived from 1 sidereal day = 23h56m4.090530833s (wikipedia!)
#ifndef SIDEREAL_DAYS_TO_HOURS
#define SIDEREAL_DAYS_TO_HOURS 23.934469591898
#endif

//and hence
#ifndef SIDEREAL_RATE_DEG_PER_HOUR
#define SIDEREAL_RATE_DEG_PER_HOUR 15.041068640261905
#endif

#ifndef  TERABYTE
#define TERABYTE 1099511627776.0 
#endif

#ifndef  GIGABYTE
#define GIGABYTE 1073741824.0
#endif

#ifndef  MEGABYTE
#define MEGABYTE 1048576.0 
#endif

#ifndef  KILOBYTE
#define KILOBYTE 1024.0 
#endif

/////////////////////////////////////////////////////
#ifndef CONVERT_UNITS
#define CONVERT_AA_TO_NM  ((double) 1.0e-1)
#define CONVERT_AA_TO_UM  ((double) 1.0e-4)
#define CONVERT_AA_TO_MM  ((double) 1.0e-7)
#define CONVERT_AA_TO_CM  ((double) 1.0e-8)
#define CONVERT_AA_TO_M   ((double) 1.0e-10)

#define CONVERT_NM_TO_AA  ((double) 1.0e1)
#define CONVERT_NM_TO_UM  ((double) 1.0e-3)
#define CONVERT_NM_TO_MM  ((double) 1.0e-6)
#define CONVERT_NM_TO_CM  ((double) 1.0e-7)
#define CONVERT_NM_TO_M   ((double) 1.0e-9)

#define CONVERT_UM_TO_AA  ((double) 1.0e4)
#define CONVERT_UM_TO_NM  ((double) 1.0e3)

#define CONVERT_CM2_TO_M2 ((double) 1.0e-4)
#define CONVERT_M2_TO_CM2 ((double) 1.0e4)

#define CONVERT_ERG_TO_J  ((double) 1.0e-7)
#define CONVERT_J_TO_ERG  ((double) 1.0e7)

#define CONVERT_JY_TO_CGS ((double) 3631.0e-23)    //convert 1 Jy to erg/cm2/s/Hz
#define CONVERT_CGS_TO_JY ((double) (1.0/3631.0e-23))
#endif


/////////////////////////////////////////////////////

#ifndef CONSTANT_PLANCK
// E = h*nu = h*c/lambda
// h = 6.6260755e-34 J·s
// c = 299792458 m/s
#define CONSTANT_PLANCK         ((double) 6.6260755e-34)  // J·s
#endif

#ifndef CONSTANT_SPEED_OF_LIGHT
#define CONSTANT_SPEED_OF_LIGHT ((double) 299792458.0)     // m/s
#endif

//Used when deciding whether to use a plural form depending on the value of an integer
#ifndef N2S
#define N2S(x) ((x)==1?"":"s")
#endif

//end
#endif
 
//
//
 
