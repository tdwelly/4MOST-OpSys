//----------------------------------------------------------------------------------
//--
//--    Filename: geometry_lib.c
//--    Description:
//--    Use: This module contains a library of functions used to carry out simple geometric tests
//--
//--
//--

#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <unistd.h> //needed for getcwd

#include "define.h"
#include "utensils_lib.h"
#include "geometry_lib.h"

#define CVS_REVISION "$Revision: 1.3 $"
#define CVS_DATE     "$Date: 2015/05/19 15:14:24 $"

#define ERROR_PREFIX   "#-geometry_lib.c : Error   :"
#define WARNING_PREFIX "#-geometry_lib.c : Warning :"
#define COMMENT_PREFIX "#-geometry_lib.c : Comment :"
#define DEBUG_PREFIX   "#-geometry_lib.c : DEBUG   :"


//functions
int geom_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}

double geom_dot_product2D ( double dx1,double dy1,  double dx2,double dy2) 
{
  return dx1*dx2 + dy1*dy2;
}

double geom_cross_product2D ( double dx1,double dy1,  double dx2,double dy2) 
{
  return dx1*dy2 - dy1*dx2;
}
  


//
//this function expects x1,y1 and x2,y2 to describe two points on an infinite line
// x0 and y0 are coords of the point
double geom_sqdist_to_line (double x0, double y0, double x1, double y1, double x2, double y2)
{
  double sq_dist,sq_length;
  sq_length = SQR(x1-x2) + SQR(y1-y2);
  if ( sq_length <= 0. )
  {
    //line is actually zero length, so:
    sq_dist = SQR(x1-x0) + SQR(y1-y0);
  }
  else
  {
    sq_dist = geom_cross_product2D(x2-x1,y2-y1, x0-x1,y0-y1);
    sq_dist =  SQR(sq_dist)/sq_length;
  }
  return sq_dist;
}


double geom_dist_to_line (double x0, double y0, double x1, double y1, double x2, double y2)
{
  return sqrt(geom_sqdist_to_line(x0,y0,x1,y1,x2,y2));
}

double geom_dist_to_line_seg (double x0, double y0, double x1, double y1, double x2, double y2)
{
  return sqrt(geom_sqdist_to_line_seg(x0,y0,x1,y1,x2,y2));
}


///now but for a line segment
///////////////////
//http://www.topcoder.com/tc?d1=tutorials&d2=geometry1&module=Static
//this function expects x1,y1 and x2,y2 to describe the start and end points of the line segment
// x0 and y0 are coords of the point
// let r be vector from x0,y0 to x1,y1 
// let s be vector from x0,y0 to x2,y2
// let t be vector from x1,y1 to x2,y2 

double geom_sqdist_to_line_seg (double x0, double y0, double x1, double y1, double x2, double y2)
{
  double sq_dist, sq_length;
  //check the length of the line segment
  sq_length = SQR(x1-x2) + SQR(y1-y2);
  if ( sq_length <= (double)0.0 )
  {
    //line is actually zero length, so:
    sq_dist = SQR(x1-x0) + SQR(y1-y0);
  }
  else
  {
    //the line has length > 0
   
    //calculate the dot product to see if nearest point is between x1,y1 and x2,y2
    if      ( geom_dot_product2D(x2-x1,y2-y1, x0-x2,y0-y2) > (double) 0.0)
    {
      //nearest point is beyond x2,y2, so use x2,y2
      sq_dist = SQR(x0-x2) + SQR(y0-y2);
    }
    else if ( geom_dot_product2D(x1-x2,y1-y2, x0-x1,y0-y1) > (double) 0.0)
    {
      //nearest point is beyond x1,y1, so use x1,y1
      sq_dist = SQR(x0-x1) + SQR(y0-y1);
    }
    else
    {
      // the closest point lies somewhere between x1,y1 and x2,y2
      sq_dist = geom_cross_product2D(x2-x1,y2-y1, x0-x1,y0-y1);
      sq_dist =  SQR(sq_dist)/sq_length;
    }
    //    fprintf(stdout,"TEST u=%12.8f sq_dist=%12.8f\n", u, sq_dist);
  }
  return sq_dist;
}

///
//  This function tests for a 2D collision between two line segments 
//  see example at http://paulbourke.net/geometry/lineline2d/
// his x1 <=> our xa1 
// his y1 <=> our ya1 
// his x2 <=> our xb1 
// his y2 <=> our yb1 
// his x3 <=> our xa2 
// his y3 <=> our ya2 
// his x4 <=> our xb2 
// his y4 <=> our yb2 
int geom_test_if_intersect_line_seg_line_seg(double xa1, double ya1, double xb1, double yb1,
                                             double xa2, double ya2, double xb2, double yb2)
{
  double denominator = (yb2-ya2)*(xb1-xa1) - (xb2-xa2)*(yb1-ya1);
  if (DBL_ISZERO(denominator))  //therefeore line segments are parallel
  {
    double ua_numerator = (xb2-xa2)*(ya1-ya2) - (yb2-ya2)*(xa1-xa2);
    double ub_numerator = (xb1-xa1)*(ya1-ya2) - (yb1-ya1)*(xa1-xa2);
    if (DBL_CMP(ua_numerator,ub_numerator) ) return 1; //lines are coincident
    else                                     return 0; //lines are parallel but separated
  }
  else   //therefeore line segments are not parallel, so test to see if the line segs cross
  {
    double ua= ((xb2-xa2)*(ya1-ya2) - (yb2-ya2)*(xa1-xa2))/denominator;
    if ( ua >= (double) 0.0 )
    {
      if ( ua <= (double) 1.0 )
      {
        double ub = ((xb1-xa1)*(ya1-ya2) - (yb1-ya1)*(xa1-xa2))/denominator;
        if ( ub >= (double) 0.0 )
        {
          if ( ub <= (double) 1.0 ) return 2;
        }
      }
    }
  }
  return 0;  //line segs do not cross
}

//////// - this is the most computationally intensive function
//when running with a potzpoz configuration
double geom_sqdist_line_seg_line_seg (double xa1, double ya1, double xb1, double yb1,
                                      double xa2, double ya2, double xb2, double yb2)
{
  double this_sq_dist, best_sq_dist;
  if ( geom_test_if_intersect_line_seg_line_seg(xa1,ya1,xb1,yb1,xa2,ya2,xb2,yb2) ) return (double) 0.0;
  // Therefore line segments do not intersect
  //   so we now cycle through each of the four line ends, to find the nearest point to the other line

  //   Distance between starting coords of line 2 vs line 1
  best_sq_dist = geom_sqdist_to_line_seg(xa2,ya2, xa1,ya1,xb1,yb1);

  //    Distance between ending coords of line 2 vs line 1
  this_sq_dist = geom_sqdist_to_line_seg(xb2,yb2, xa1,ya1,xb1,yb1);
  best_sq_dist = MIN(this_sq_dist,best_sq_dist);

  //    Distance between starting coords of line 1 vs line 2
  this_sq_dist = geom_sqdist_to_line_seg(xa1,ya1, xa2,ya2,xb2,yb2);
  best_sq_dist = MIN(this_sq_dist,best_sq_dist);

  //    Distance between ending coords of line 1 vs line 2
  this_sq_dist = geom_sqdist_to_line_seg(xb1,yb1, xa2,ya2,xb2,yb2);
  best_sq_dist = MIN(this_sq_dist,best_sq_dist);

  return best_sq_dist ;
}




double geom_dist_line_seg_line_seg (double xa1, double ya1, double xb1, double yb1,
                                    double xa2, double ya2, double xb2, double yb2)
{
  return sqrt(geom_sqdist_line_seg_line_seg (xa1,ya1,xb1,yb1,xa2,ya2,xb2,yb2));
}

//-------------------------------------------------------------------------------------
//       FUNCTION geom_collision_line_seg_line_seg
//       This function tests for a 2D collision between two line segments 
//       with an additional minimum separation enforced (d)
//       quicker version duplicating behaviour of geom_sqdist_line_seg_line_seg to reduce number of tests
int geom_collision_line_seg_line_seg(double xa1, double ya1, double xb1, double yb1,
                                     double xa2, double ya2, double xb2, double yb2, double d)
{
  //old way - but we can maybe reduce teh number of tests
  //  if ( geom_sqdist_line_seg_line_seg(xa1,ya1,xb1,yb1,xa2,ya2,xb2,yb2) <= SQR(d))  return 1;

  //some initial filtering:
  //test if the two mid points of the two line segments
  //are further apart from each other than the sum of  
  //the line half-segment lengths plus the min separation distance
  //this involves two calls to sqrt(), so could be made more efficient if lengths were already known
  double sqr_mid_point_sep = 0.25*(SQR((xa1+xb1)-(xa2+xb2)) +  SQR((ya1+yb1)-(ya2+yb2)));
  double max_dist_for_intersect = 0.5*(sqrt(SQR(xa1-xb1)+SQR(ya1-yb1))  +
                                       sqrt(SQR(xa2-xb2)+SQR(ya2-yb2))) + d;
  
  if ( sqr_mid_point_sep > SQR(max_dist_for_intersect)) return 0;
  
  // Test if line segments intersect
  if ( geom_test_if_intersect_line_seg_line_seg(xa1,ya1,xb1,yb1,xa2,ya2,xb2,yb2) ) return 1;

  //  if get to here then no intersection,
  if ( d > (double) 0.0 )
  {
    double sq_dist = SQR(d);
    //  so we now cycle through each of the four line ends, to find if ay of the other points on the line intersect
    //  exit as soon as a point below the thresh is found
    
    //  test distance between starting coords of line 2 vs line 1
    if ( geom_sqdist_to_line_seg(xa2,ya2, xa1,ya1,xb1,yb1) <= sq_dist ) return 2;
    
    //   test distance between ending coords of line 2 vs line 1
    if ( geom_sqdist_to_line_seg(xb2,yb2, xa1,ya1,xb1,yb1) <= sq_dist ) return 3;
    
    //   test distance between starting coords of line 1 vs line 2
    if ( geom_sqdist_to_line_seg(xa1,ya1, xa2,ya2,xb2,yb2) <= sq_dist ) return 4;
    
    //   test distance between ending coords of line 1 vs line 2
    if ( geom_sqdist_to_line_seg(xb1,yb1, xa2,ya2,xb2,yb2) <= sq_dist ) return 5;
  }
  
  //So, it seems that the line segments are far enough apart to avoid colliding
  return 0;
}

//-------------------------------------------------------------------------------------
//       FUNCTION geom_collision_line_seg_circle
//       This function tests for a 2D collision between a line segment and a circle 
int geom_collision_line_seg_circle(double x1, double y1, double x2, double y2, double x0, double y0, double r0)
{
  if ( geom_sqdist_to_line_seg(x0,y0,x1,y1,x2,y2) <= SQR(r0)) return 1;
  return 0;
}












//this function expects x1,y1 and x2,y2 to describe the start and end points of the line
// x0 and y0 are coords of the point
//d is the allowed distance
//returns >0 for true, 0 for false, and < 0 for error 
int geom_test_if_near_line_seg (double x0, double y0, double x1, double y1, double x2, double y2, double d)
{
  double sq_dist = (double) geom_sqdist_to_line_seg(x0,y0,x1,y1,x2,y2);
  if (  sq_dist < (double) 0.0    )
  {
    return -1;
  }
  else if ( sq_dist < SQR(d) )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

//this function expects x1,y1 and x2,y2 to describe two points on the line
// x0 and y0 are coords of the point
//d is the allowed distance
//returns >0 for true, 0 for false, and < 0 for error 
int geom_test_if_near_line (double x0, double y0, double x1, double y1, double x2, double y2, double d)
{
  double sq_dist = (double) geom_sqdist_to_line(x0,y0,x1,y1,x2,y2);
  if (  sq_dist < (double)  0.0    )
  {
    return -1;
  }
  else if ( sq_dist < SQR(d) )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

//rotate x,y by pa degrees about x0,y0
int geom_rotate_about_axis (double x0, double y0, double pa, double x, double y, double *pxx, double *pyy )
{
  double cos_pa = cosd(pa);
  double sin_pa = sind(pa);
  *pxx = (double) x0 + (cos_pa*(x-x0) - sin_pa*(y-y0)) ;  
  *pyy = (double) y0 + (sin_pa*(x-x0) + cos_pa*(y-y0)) ;  
  return 0;
}

//rotate x,y by pa degrees about x0,y0 - pre-supplied cos and sin
int geom_fast_rotate_about_axis (double x0, double y0, double sin_pa, double cos_pa, double x, double y, double *pxx, double *pyy )
{
  *pxx = (double) (x0 + (cos_pa*(x-x0) - sin_pa*(y-y0))) ;  
  *pyy = (double) (y0 + (sin_pa*(x-x0) + cos_pa*(y-y0))) ;  
  return 0;
}

//rotate x,y by pa degrees about x0,y0 floats version
int geomf_rotate_about_axis (float x0, float y0, float pa, float x, float y, float *pxx, float *pyy )
{
  float cos_pa = cosd(pa);
  float sin_pa = sind(pa);
  *pxx = (float) x0 + (cos_pa*(x-x0) - sin_pa*(y-y0)) ;  
  *pyy = (float) y0 + (sin_pa*(x-x0) + cos_pa*(y-y0)) ;  
  return 0;
}

//rotate x,y by pa degrees about x0,y0 - pre-supplied cos and sin
int geomf_fast_rotate_about_axis (float x0, float y0, float sin_pa, float cos_pa, float x, float y, float *pxx, float *pyy )
{
  *pxx = (float) (x0 + (cos_pa*(x-x0) - sin_pa*(y-y0))) ;  
  *pyy = (float) (y0 + (sin_pa*(x-x0) + cos_pa*(y-y0))) ;  
  return 0;
}






//////////////////////////////////////////////
// test if a point is inside a regular hexagon with outer radius = outer_radius, centred at (x,y) = (0,0)
// this assumes that the hexagon is not rotated wrt to the x,y axes, ie it has a vertex pointing upwards
int geom_test_if_in_hexagon ( double outer_radius, double x, double y )
{
  double x_OVER_sqrt_3 = x*ONE_OVER_SQRT_3;

  //test if completely outside the circle that bounds the hexagon
  if ( SQR(x) + SQR(y) > SQR(outer_radius) ) return 0;

  //we check whether objects are within the hexagon
  // - do in nested steps to reduce number of comparisons
  if (x >=  outer_radius * MSQRT_3_OVER_2 )
    if ( x <=  outer_radius *  SQRT_3_OVER_2  )
      if ( y <=      outer_radius + x_OVER_sqrt_3 ) 
        if ( y <=       outer_radius - x_OVER_sqrt_3 )
          if ( y >=  -1.0*outer_radius - x_OVER_sqrt_3 )
            if ( y >=  -1.0*outer_radius + x_OVER_sqrt_3 )
              return 1;
  
  return 0;
}

/*hexagon vertices are
            a
            /\
           /  \
          /    \
        f/      \b
        |        |
        |    .   |                        
        |        |
        |        |
        e\      /c
          \    /
           \  /
            \/
             d
*/

// test if a point is close to the perimeter of a regular hexagon with
// outer radius = outer_radius, centred at (x,y) = (0,0)
// this assumes that the hexagon is not rotated wrt to the x,y axes, ie it has a vertex pointing upwards
int geom_test_if_near_hexagon_perimeter ( double outer_radius, double x, double y, double d )
{
  double  xmin = MSQRT_3_OVER_2*outer_radius;
  double  xmax =  SQRT_3_OVER_2*outer_radius;
  double  hrad = outer_radius * (double) 0.5;
  double mhrad = outer_radius * (double) -0.5;
  double sq_rad = SQR(x) + SQR(y);

  //test if completely outside the (extended) hexagon
  if ( sq_rad > SQR(outer_radius + d) ) return 0;

  //test if completely inside the hexagon (measure to middle of a vertical face)
  if ( sq_rad < SQR(xmax - d) ) return 0;

  //left edge:   e->f
  if ( geom_test_if_near_line_seg (x,y, xmin, mhrad, xmin,    hrad,         d) > 0 )  return 1;
  //right edge: c->b
  if ( geom_test_if_near_line_seg (x,y, xmax, mhrad, xmax,    hrad,         d) > 0 )  return 1;
  //top left edge: f->a 
  if ( geom_test_if_near_line_seg (x,y, xmin,  hrad, 0.0,     outer_radius, d) > 0 )  return 1;
  //bottom left edge: e->d  
  if ( geom_test_if_near_line_seg (x,y, xmin, mhrad, 0.0, -1.*outer_radius, d) > 0 )  return 1;
  //top right edge: b->a    
  if ( geom_test_if_near_line_seg (x,y, xmax,  hrad, 0.0,     outer_radius, d) > 0 )  return 1;
  //bottom right edge : c->d
  if ( geom_test_if_near_line_seg (x,y, xmax, mhrad, 0.0, -1.*outer_radius, d) > 0 )  return 1;
  
  return 0;
}





//a check if point a0 is within the given range of longitude [a_min:a_max] 
//tests are non-inclusive of the limits ( ie '<' and '>' rather than '<=' and '>=' ) 
//assume inputs are in degrees
//a (alpha) is longitude
int geom_test_if_in_long_range ( double a0, double a_min, double a_max)
{
  //separate the tests so that do as few as possible.
  //this test is dependent on whether the longitude limits wrap around the longitude=0 meridian
  //a_min < a_max requires a vanilla test
  if (a_min < a_max )
  {
    if ( a0 > a_min )
    {
      if  ( a0 < a_max )  return 1;
    }
  }
  //here a_min > a_max so change the test slightly, assume that no objects lie outside 0<=a<=360deg 
  else
  {
    //the next is an OR test, so allow two routes out
    if ( a0 > a_min ) return 1;
    if ( a0 < a_max ) return 1;
  }
  return 0;
}




//a check if point (a0,d0) is within the given range of longitude [a_min:a_max] and latitude [d_min:d_max]
//tests are non-inclusive of teh limits ( ie '<' and '>' rather than '<=' and '>=' ) 
//assume inputs are in degrees
//a (alpha) is longitude, d (dec) is latitude
int geom_test_if_in_long_lat_range ( double a0, double d0, double a_min, double a_max, double d_min, double d_max)
{
  //separate the tests so that do as few as possible.
  //first test the latitude limits to whittle things down
  if ( d0 < d_max )
  {
    if ( d0 > d_min )
    {
      //next test is dependent on whether the longitude limits wrap around the longitude=0 meridian
      //a_min < a_max requires a vanilla test
      if (a_min < a_max )
      {
        if ( a0 > a_min )
        {
          if  ( a0 < a_max )  return 1;
        }
      }
      //here a_min > a_max so change the test slightly, assume that no objects lie outside 0<=a<=360deg 
      else
      {
        //the next is an OR test, so allow two routes out
        if ( a0 > a_min ) return 1;
        if ( a0 < a_max ) return 1;
      }
    }
  }
  return 0;
}


//-----------------------------------------------------------------------------------------------------------
// This uses the "Law of Haversines"
double geom_angsep (double ra1, double dec1, double ra2, double dec2)
{
  double a, sra, sdec;
  //check for nonsense inputs
  if (isnan(ra1)   ||
      isnan(dec1)  ||
      isnan(ra2)   ||
      isnan(dec2)  ||
      ra1 <  (double)    0.0 ||
      ra1 >  (double)  360.0 ||
      dec1 <  (double) -90.0 ||
      dec1 > (double)   90.0 ||
      ra2 <  (double)    0.0 ||
      ra2 >  (double)  360.0 ||
      dec2 <  (double) -90.0 ||
      dec2 >  (double)  90.0  ) return -1.0;
  if ( DBL_CMP(ra1,ra2) && DBL_CMP(dec1,dec2) ) return 0.0; 

  // This code is adapted from the cfitsio code
  // see heacore/cfitsio/eval_y.c:
  // egrep -A30 -n 'double angsep_calc' heacore/cfitsio/eval_y.c

  /* The algorithm is the law of Haversines.  This algorithm is
     stable even when the points are close together.  The normal
     Law of Cosines fails for angles around 0.1 arcsec. */

  sra  = sind( (ra2 - ra1) / 2.0 );
  sdec = sind( (dec2 - dec1) / 2.0);
  a = sdec*sdec + cosd(dec1)*cosd(dec2)*sra*sra;

  /* Sanity checking to avoid a range error in the sqrt()'s below */
  if (a < 0.0) { a = (double) 0.0; }
  if (a > 1.0) { a = (double) 1.0; }

  return (double) 2.0*atan2d(sqrt(a), sqrt(1.0 - a));
}



//-----------------------------------------------------------------------------------------------------------
// This is based on the "law of cosines"
// TODO add a reference here: 
double geom_arc_dist (double ra1, double dec1, double ra2, double dec2)
{
  double result;
  //check for nonsense inputs
  if (isnan(ra1)   ||
      isnan(dec1)  ||
      isnan(ra2)   ||
      isnan(dec2)  ||
      ra1 <  (double)    0.0 ||
      ra1 >  (double)  360.0 ||
      dec1 <  (double) -90.0 ||
      dec1 > (double)   90.0 ||
      ra2 <  (double)    0.0 ||
      ra2 >  (double)  360.0 ||
      dec2 <  (double) -90.0 ||
      dec2 >  (double)  90.0  ) return -1.0;
  if ( DBL_CMP(ra1,ra2) && DBL_CMP(dec1,dec2) ) return 0.0; 
  result = sind(dec1)*sind(dec2) + cosd(dec1)*cosd(dec2)*cosd(ra2 - ra1);
  //  if (result == 0.0 || (ra1 == ra2 && dec1 == dec2) || (!(result > 0.0) && !(result < 0.0)) ) return 0.0;
  if ( DBL_ISZERO(result)) return 0.0;
  return acosd(result);
}

double geom_arc_dist_no_check (double ra1, double dec1, double ra2, double dec2)
{
  //do not check for nonsense inputs
  double result = sind(dec1)*sind(dec2) + cosd(dec1)*cosd(dec2)*cosd(ra2 - ra1);
  if ( DBL_CMP(ra1,ra2) && DBL_CMP(dec1,dec2) ) return 0.0; 
  if ( DBL_ISZERO(result)) return 0.0;
  //  if (result == (double) 0.0) return (double) 0.0;
  return acosd(result);
}

//quicker because we make no sanity checks and also use pre-supplied cos/sin Decs 
double geom_fast_arc_dist (double ra1, double ra2,
                           double sin_dec1, double cos_dec1,
                           double sin_dec2, double cos_dec2)
{
  double result;
  //  if ( ra1 == ra2 && cos_dec1 == cos_dec2) return 0.0;
  if ( DBL_CMP(ra1,ra2) && DBL_CMP(cos_dec1,cos_dec2) && DBL_CMP(sin_dec1,sin_dec2) ) return 0.0; 
  result = sin_dec1*sin_dec2 + cos_dec1*cos_dec2*cosd(ra2 - ra1);
  if ( DBL_ISZERO(result)) return 0.0;
  //  if (result == (double) 0.0 ) return (double) 0.0;
  return (acosd(result));
}

//even quicker because we do not carry out the arccos operation - we actually return cos(arcdist)
double geom_very_fast_cos_arc_dist (double ra1, double ra2, double sin_dec1, double cos_dec1, double sin_dec2, double cos_dec2)
{
  if ( DBL_CMP(ra1,ra2) && DBL_CMP(cos_dec1,cos_dec2) && DBL_CMP(sin_dec1,sin_dec2) )
  {
    return 1.0;
  }
  else
  {
    return (double) (sin_dec1*sin_dec2 + cos_dec1*cos_dec2*cosd(ra2 - ra1));
  }
}

///calculate the bearing to object at position ra2,dec2 from ra1,dec1
//method taken from http://www.movable-type.co.uk/scripts/latlong.html
double geom_calc_bearing (double long1, double lat1, double long2, double lat2)
{
  double y = sind(long2 - long1) * cosd(lat2);
  double x = cosd(lat1)*sind(lat2) - sind(lat1)*cosd(lat2)*cosd(long2 - long1);
  double bearing = atan2d(y, x);
  return bearing;
}

///calculate the bearing to object at position ra2,dec2 from ra1,dec1
//method stolen from wcstools
double geom_fast_calc_bearing (double ra1, double dec1, double ra2, double dec2) 
{
  //  double dra  = fast_arc_dist(ra1, dec1, ra2, dec1);
  //  double ddec = fast_arc_dist(ra1, dec1, ra1, dec2);
  double dra  = (ra2-ra1)*cosd(dec1);
  double ddec = dec2 - dec1;
  double a;
  //  if (dec2 < dec1) ddec = -ddec;
  //  if (ra2 - ra1 > -180.0 && ra2 - ra1 < 0.0) dra = -dra;
  a = atan2d(dra, ddec); 
  while (a < 0.) a+= 360.0;
  return a;
}


//from http://www.movable-type.co.uk/scripts/latlong.html
int geom_calc_midpoint (double long1, double lat1, double long2, double lat2, double *plong0, double *plat0)
{
  double cos_lat1 = cosd(lat1);
  double cos_lat2 = cosd(lat2);
  double Bx = cos_lat2 * cosd(long2 - long1);
  double By = cos_lat2 * sind(long2 - long1);
  double lat0  = (double) atan2d(sind(lat1)+sind(lat2), sqrt(SQR(cos_lat1+Bx) + SQR(By)));
  double long0 = (double) long1 + atan2d(By, cos_lat1 + Bx);
  while (long0 <    0.0 ) long0 += 360.0; 
  while (long0 >= 360.0 ) long0 -= 360.0; 

  *plat0  = (double) lat0;
  *plong0 = (double) long0;
  return 0;
}

//calc new position from old position, bearing and distance
//assume motion along a greatcircle
// from  http://www.movable-type.co.uk/scripts/latlong.html
int geom_calc_destination (double long1, double lat1, double dist, double bearing, double *plong0, double *plat0)
{
  double lat0  = asind(sind(lat1)*cosd(dist) + cosd(lat1)*sind(dist)*cosd(bearing));
  double long0 = long1 + atan2d(sind(bearing)*sind(dist)*cosd(lat1), cosd(dist)-sind(lat1)*sind(lat0));
  while (long0 <    0.0 ) long0 += 360.0; 
  while (long0 >= 360.0 ) long0 -= 360.0; 
  *plat0  = (double) lat0;
  *plong0 = (double) long0;
  return 0;
}

//find the point that is (t0-t1)/(t2-t1) of the way along the great circle that joins 1 and 2
int geom_interpolate_long_lat (double t1, double long1,   double lat1,
                               double t2, double long2,   double lat2,
                               double t0, double *plong0, double *plat0)
{
  double dist;
  double bearing =  geom_calc_bearing (long1, lat1, long2, lat2);
  double dt21 = t2 - t1;
  double dt01 = t0 - t1;
  if ( DBL_ISZERO(dt01) )
  {
    *plong0 = (double) long1;
    *plat0  = (double) lat1;
    return 0;
  }
  else if (  DBL_ISZERO(dt21) )
  {
    fprintf(stderr, "%s Invalid delta t value t1=%.4f t2=%.4f t0=%.4f\n",
            ERROR_PREFIX,  t1, t2, t0);

    return 1;
  }
  dist = geom_arc_dist(long1, lat1, long2, lat2) * dt01/dt21;
  if ( geom_calc_destination (long1, lat1, dist, bearing, (double*) plong0, (double*) plat0))
  {
    fprintf(stderr, "%s Problem calling geom_calc_destination() at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
 
  return 0;
}

//http://www.robertmartinayers.org/tools/coordinates.html
// From Fundamental Astronomy by Karttunen et al:
// The galactic coordinates can be obtained from the equatorial
// ones with the transformation equations
//  sin (Ln - L) cos (B) = cos (delta) sin (alpha - AP)
//  cos (Ln - L) cos (B) = - cos(delta) sin(deltaP) cos(alpha-AP) + sin(delta) cos (deltaP)
//  sin(B) = cos(delta)* cos(deltaP)* cos(alpha-AP) + sin(delta)*sin(deltaP)
// where the direction of the Galactic north pole is
// alphaP = 12h 51.4m, deltaP = 27d 08', and the
// galactic longitude of the celestial pole Ln = 123.0 degrees.
//
//now, converting this to the J2000 epoch:
//the galactic N pole in J2000 coords is:
//skycoor -n 20 -d -j 180 +90. Gal
//192.85948098333332723087  27.12825125722222097124 J2000
//galactic longitude of the J2000 equatorial pole is Ln ~ 123.0 degrees.
//skycoor -n 20 -d -g 0 +90. J2000
//122.93191813706228288083  27.12825125793921543504 galactic
int geom_ra_dec_to_gal_l_b (double ra, double dec, double *pl, double *pb)
{
  const double sin_deltaP   =   0.45598379570377921495; //wcalc -P20 "sin(27.12825125722222097124)"
  const double cos_deltaP   =   0.88998807747945373245; //wcalc -P20 "cos(27.12825125722222097124)"
  const double alphaP       = 192.85948098333332723087;
  const double l_of_eq_pole = 122.93191813706228288083;
  double cos_dec       = cosd(dec);
  double sin_dec       = sind(dec);
  double cos_ra_alphaP = cosd(ra-alphaP);
  double b             = asind(sin_dec*sin_deltaP + cos_dec*cos_deltaP*cos_ra_alphaP);
  double sin_l1_cosb   = cos_dec*sind(ra-alphaP);
  double cos_l2_cosb   = sin_dec*cos_deltaP - cos_dec*sin_deltaP*cos_ra_alphaP;
  double l;
  //if we are at the poles, we will return zero longitude
  if ( b >= (double) +90.0 || b <= (double) -90.0 || DBL_ISZERO(cos_l2_cosb))
  {
    l = (double) 0.0;
  }
  else
  { 
    l = l_of_eq_pole - atan2d(sin_l1_cosb, cos_l2_cosb);
    while ( l < (double) 0.0 ) l += (double) 360.0;
  }
  *pb = (double) b;
  *pl = (double) l;
  return 0;
}

//now, converting this to the J2000 epoch:
//the J2000 N pole in Gal coords is:
//skycoor -n 20 -d -g 180 +90. J2000
//122.93191813706228288083  27.12825125793922254047 galactic
//RA of the Galactic N pole is:
//skycoor -n 20 -d -j 0 +90. Gal
//192.85948098333332723087  27.12825125722222097124 J2000
int geom_gal_l_b_to_ra_dec (double l, double b,  double *pra, double *pdec)
{
  const double sin_deltaP   =   0.45598379571491655969; //wcalc -P20 "sin(27.12825125793922254047)"
  const double cos_deltaP   =   0.88998807747945373245; //wcalc -P20 "cos(27.12825125793922254047)"
  const double alphaP       = 122.93191813706228288083;
  const double ra_of_gal_pole = 192.85948098333332723087;
  double cos_b       = cosd(b);
  double sin_b       = sind(b);
  double cos_l_alphaP = cosd(l-alphaP);

  double dec           = asind(sin_b*sin_deltaP + cos_b*cos_deltaP*cos_l_alphaP);
  double sin_l1_cosb   = cos_b*sind(l-alphaP);
  double cos_l2_cosb   = sin_b*cos_deltaP - cos_b*sin_deltaP*cos_l_alphaP;
  double ra;
  //if we are at the poles, we will return zero longitude
  if ( dec >= (double) +90.0 || dec <= (double) -90.0 || DBL_ISZERO(cos_l2_cosb))
  {
    ra = (double) 0.0;
  }
  else
  { 
    ra = ra_of_gal_pole - atan2d(sin_l1_cosb, cos_l2_cosb);
    while ( ra <  (double) 0.0   ) ra += (double) 360.0;
    while ( ra >= (double) 360.0 ) ra -= (double) 360.0;
  }
  *pdec = (double) dec;
  *pra = (double) ra;
  return 0;
}



//see http://mathworld.wolfram.com/GnomonicProjection.html
//this projects from ra0,dec0, RA,Dec to x,y (in arcsec), using a rotation matrix in the planar coordinates
//RA,Dec,PA in degrees, x,y in arcsec
int geom_project_gnomonic_to_arcsec (double ra0, double dec0, double pa0, double ra, double dec, double *px, double *py )
{
  double cos_dec0 = cosd(dec0);
  double sin_dec0 = sind(dec0);
  double cos_dec = cosd(dec);
  double sin_dec = sind(dec);
  double cos_c = sin_dec0*sin_dec + cos_dec0*cos_dec*cosd(ra-ra0);
  
  if ( DBL_ISZERO(cos_c) )
  {
    *px =(double) 0.;
    *py =(double) 0.;
  }
  else
  {
    double r2as_minv_cos_c = (double) -1.0 * (double) RAD_TO_ARCSEC / cos_c;
    double x = cos_dec*sind(ra-ra0) * r2as_minv_cos_c;
    double y = (cos_dec0*sin_dec - sin_dec0*cos_dec*cosd(ra-ra0)) * r2as_minv_cos_c;
    double sin_pa = sind(pa0);
    double cos_pa = cosd(pa0);
    *px = (double) (x*cos_pa - y*sin_pa);
    *py = (double) (x*sin_pa + y*cos_pa);
  }  
  return 0;
}

//platescale is in units of mm/arcsec
//px,py are in mm
int geom_project_gnomonic_to_mm (double ra0, double dec0, double pa0,
                               double ra, double dec, double platescale,
                               double *px, double *py )
{
  double temp_x;
  double temp_y;
  if ( geom_project_gnomonic_to_arcsec ((double) ra0, (double) dec0, (double) pa0,
                                      (double) ra, (double) dec,
                                      (double*) &temp_x, (double*) &temp_y)) return 1; 
  *px = (double) (temp_x * platescale);
  *py = (double) (temp_y * platescale);
  return 0;  
}

//http://mathworld.wolfram.com/GnomonicProjection.html
//go from x,y coords (in arcsec) back to RA,Dec, using a rotation matrix in the planar coordinates
int geom_project_inverse_gnomonic_from_arcsec (double ra0, double dec0, double pa0, double x, double y, double *pra, double *pdec )
{
  //  double xx = x * -1.0 * ARCSEC_TO_RAD;
  //  double yy = y * -1.0 * ARCSEC_TO_RAD;
  double sin_mpa = -1.0* sind(pa0);
  double cos_mpa =       cosd(pa0); //same as cos(-pa0)
  double xx = (x*cos_mpa - y*sin_mpa) * -1.0 * ARCSEC_TO_RAD;
  double yy = (x*sin_mpa + y*cos_mpa) * -1.0 * ARCSEC_TO_RAD;

  double rho = sqrt(SQR(xx) + SQR(yy));
  double c   = atan(rho);
  
  if ( DBL_ISZERO(rho) )
  {
    *pra  = (double)ra0;
    *pdec = (double) dec0;
  }
  else
  {
    double cos_c = cos(c);
    double sin_c = sin(c);
    double cos_dec0 = cosd(dec0);
    double sin_dec0 = sind(dec0);
    *pdec = (double) asind( cos_c*sin_dec0 + (yy*sin_c*cos_dec0)/rho ) ;
    *pra  = (double) ra0 + atan2d(xx*sin_c, rho*cos_dec0*cos_c - yy*sin_dec0*sin_c);
  }  
  return 0;
}

//inv_platescale is in units of arcsec/mm
//x,y are given in mm
int geom_project_inverse_gnomonic_from_mm (double ra0, double dec0, double pa0,
                                         double x, double y, double inv_platescale,
                                         double *pra, double *pdec )
{
  double temp_x, temp_y;
  temp_x =  (double) (x * inv_platescale);
  temp_y =  (double) (y * inv_platescale);

  if ( geom_project_inverse_gnomonic_from_arcsec ((double) ra0, (double) dec0, (double) pa0,
                                                (double) temp_x, (double) temp_y,
                                                (double*) pra, (double*) pdec)) return 1; 

  return 0;  
}


//use formula of http://2000clicks.com/mathhelp/GeometryConicSectionCircleIntersection.aspx
//assume a locally flat arrangement
//return number f intersections
//    K = (1/4)sqrt(((rA+rB)^2-d^2)(d^2-(rA-rB)^2))
//    d^2 = (xB-xA)^2 + (yB-yA)^2 
//
//    (K is the area of the triangle formed by the centers of the two circles and one of their points of intersection;
//    d is the distance between the circles centers;
//    rA, rB are the circles' radii;
//    (xA,yA) and (xB,yB) are the circles' centers)

//    x = (1/2)(xB+xA) + (1/2)(xB-xA)(rA^2-rB^2)/d^2 +-  2*(yB-yA)K/d^2 
//    y = (1/2)(yB+yA) + (1/2)(yB-yA)(rA^2-rB^2)/d^2 +- -2*(xB-xA)K/d^2  

 
int geom_find_intersection_of_circles_on_plane (double x1, double y1, double r1, double x2, double y2, double r2, double *px1, double *py1, double *px2, double *py2 )
{
  double d2 = SQR(x2-x1) + SQR(y2-y1); 
  double one_over_d2;
  double K;
  if ( DBL_CMP(x1,x2) && DBL_CMP(y1,y2) && DBL_CMP(r1,r2) ) return -1;
  if ( d2 > SQR(r1+r2) ) return 0;
  K = (double)0.25*sqrt((SQR(r1+r2) - d2)*(d2 - SQR(r1-r2)));

  one_over_d2 = (double) 1.0/(DBL_ISZERO(d2)?1.0:d2);
  *px1 = (double) (0.5*(x2+x1) + 0.5*(x2-x1)*(SQR(r1) - SQR(r2))*one_over_d2 +  2.0*(y2-y1)*K*one_over_d2);
  *px2 = (double) (0.5*(x2+x1) + 0.5*(x2-x1)*(SQR(r1) - SQR(r2))*one_over_d2 -  2.0*(y2-y1)*K*one_over_d2);

  *py1 = (double) (0.5*(y2+y1) + 0.5*(y2-y1)*(SQR(r1) - SQR(r2))*one_over_d2 + -2.0*(x2-x1)*K*one_over_d2);
  *py2 = (double) (0.5*(y2+y1) + 0.5*(y2-y1)*(SQR(r1) - SQR(r2))*one_over_d2 - -2.0*(x2-x1)*K*one_over_d2);

  if ( DBL_ISZERO(K) )
  {
    return 1;   
  }
  return 2;
}


//transfers ra,dec to a unit square coordinate system which has:
//        (0,0) @ (ra_min, dec_min)
//        (1,1) @ (ra_max, dec_max)
//no checks yet..prob need some
void geom_radec2unitsquare(double ra_min,  double ra_max,  double dec_min,  double dec_max, double ra, double dec, double *pux, double *puy  ) 
{
  *pux = (ra  - ra_min )/(ra_max  - ra_min);
  *puy = (dec - dec_min)/(dec_max - dec_min);
}

//no checks yet..prob need some
void geom_unitsquare2radec (double ra_min,  double ra_max,  double dec_min,  double dec_max, double ux, double uy, double *pra, double *pdec  ) 
{
  *pra  = ra_min + ux * (ra_max  - ra_min);
  *pdec = dec_min + uy * (dec_max - dec_min);
}


//convert from ra,dec coord to a location in x,y,z unit vector space
int geom_ra_dec_to_unit_vectors ( double ra, double dec, double *px, double *py, double *pz )
{
  //standard transformation from spherical polars to x,y,z 
  if ( px ) *px =  sind(90.0 - dec)*cosd(ra - 180.0);
  if ( py ) *py =  sind(90.0 - dec)*sind(ra - 180.0);
  if ( pz ) *pz =  cosd(90.0 - dec);
  return 0;
}

//convert from a location in x,y,z unit vector space to a ra,dec coord 
int geom_unit_vectors_to_ra_dec (double x, double y, double z, double *pra, double *pdec)
{
  //standard transformation from x,y,z to spherical polars 
  if ( pra )
  {
    double ra = (double) atan2d(y,x) + (double) 180.0;
    while ( ra < 0.0) ra += (double) 360.0;
    *pra  = (double) ra;
  }
  if ( pdec) *pdec = (double) 90.0 - acosd(z);
  
  return 0;
}



//////////////////////////3d rotations///////////////////////
//rotate x,y,z by a,b,c about x,y and z axes respectively
//x,y,z are unit vectors
//a,b,c are in degrees
//results are in uninvectors
//http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/
//also at http://en.wikipedia.org/wiki/Rotation_matrix
//the order of rotation is 1) a degrees around x-axis, 2) b degrees around y-axis, then 3) c degrees around z-axis
int geom3d_rotate_about_axes_fast (double x, double y, double z,
                                   double sin_a, double cos_a,
                                   double sin_b, double cos_b,
                                   double sin_c, double cos_c,
                                   double *px, double *py, double *pz )
{
  double matrix[3][3];
  matrix[0][0] = cos_b*cos_c;
  matrix[0][1] = sin_a*sin_b*cos_c - cos_a*sin_c;
  matrix[0][2] = cos_a*sin_b*cos_c + sin_a*sin_c;

  matrix[1][0] = cos_b*sin_c;
  matrix[1][1] = cos_a*cos_c + sin_a*sin_b*sin_c;
  matrix[1][2] = cos_a*sin_b*sin_c - sin_a*cos_c;

  matrix[2][0] = -1.0*sin_b;
  matrix[2][1] = sin_a*cos_b;
  matrix[2][2] = cos_a*cos_b;
  
  if ( px ) *px = (double) (x * matrix[0][0] + y*matrix[1][0] + z*matrix[2][0]) ;  
  if ( py ) *py = (double) (x * matrix[0][1] + y*matrix[1][1] + z*matrix[2][1]) ;  
  if ( pz ) *pz = (double) (x * matrix[0][2] + y*matrix[1][2] + z*matrix[2][2]) ;  
  return 0;
 
}

//this just calls the fast version
int geom3d_rotate_about_axes (double x, double y, double z, double a, double b, double c, double *px, double *py, double *pz )
{
  return ( geom3d_rotate_about_axes_fast ((double) x, (double) y, (double) z,
                                          (double) sind(a), (double) cosd(a),
                                          (double) sind(b), (double) cosd(b),
                                          (double) sind(c), (double) cosd(c),
                                          (double*) px, (double*) py, (double*) pz ));
  return 0;
}

//this just converts to unit vectors then calls the other functions
int geom3d_rotate_ra_dec_about_axes (double ra, double dec, double a, double b, double c, double *pra, double *pdec)
{
  double x, y , z, xx, yy, zz;
  geom_ra_dec_to_unit_vectors ( ra, dec, (double*) &x, (double*) &y, (double*) &z );

  geom3d_rotate_about_axes_fast ((double) x, (double) y, (double) z,
                                 (double) sind(a), (double) cosd(a),
                                 (double) sind(b), (double) cosd(b),
                                 (double) sind(c), (double) cosd(c),
                                 (double*) &xx, (double*) &yy, (double*) &zz);
  
  geom_unit_vectors_to_ra_dec ((double) xx, (double) yy, (double) zz, (double*) pra, (double*) pdec);
  return 0;
}


// simple algorithm to compute area of a general polygon
// requires that vertices are ordered either clockwise or anticlockwise
// uses http://www.mathopenref.com/coordpolygonarea2.html
int geom_calc_area_of_polygon (int npts, double *px, double *py, double *parea)
{
  int i,j;
  double result = 0;
  if ( px == NULL || py == NULL) return 1;
  if ( npts < 3 ) return 2;

  j = npts - 1;
  for (i=0;i<npts;i++)
  {
    result += (px[j] + px[i]) * (py[j] - py[i]);
    j = i;
  }
  *parea = (double) 0.5 * fabs(result);
  return 0;
}



//write a grid of lines that will be overlaid onto all sky plots
int geom_write_coordinate_grid_file_equatorial (const char *str_filename)
{
  const double ra_spacing = 30.0;
  const double dec_spacing  = 15.0;
  const double point_spacing = 1.0;
  double ra,dec;
  FILE* pFile = NULL;
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  //first print lines in constant ra
  for ( ra=0.0;ra<=360.0;ra+=ra_spacing)
  {
    for ( dec=-90.0;dec<=+90.0;dec+=point_spacing)
    {
      fprintf(pFile, "%12.8g %+12.8g %12.8g %+12.8g\n", ra, dec, ra, dec);
    }
    fprintf(pFile, "\n\n");
  }
  fprintf(pFile, "\n");
  //now print lines in constant dec
  for ( dec=-90.0;dec<=+90.0;dec+=dec_spacing)
  {
    for ( ra=0.0;ra<=360.0;ra+=point_spacing)
    {
      fprintf(pFile, "%12.8g %+12.8g %12.8g %+12.8g\n", ra, dec, ra, dec);
    }
    fprintf(pFile, "\n\n");
  }
  
  if ( pFile ) fclose(pFile) ; pFile = NULL;
  return 0;
}


//this requires a the geom_gal_l_b_to_ra_dec function
int geom_write_coordinate_grid_file_galactic (const char* str_filename)
{
  const double long_spacing = 30.0;
  const double lat_spacing  = 15.0;
  const double point_spacing = 1.0;
  FILE* pFile = NULL;
  double l,b,ra,dec;
  double last_ra = 0.0,last_dec = 0.0;
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  last_ra = 0.0;
  last_dec = 0.0;
  for ( l=0.0;l<360.0;l+=long_spacing)
  {
    for ( b=-90.0;b<=+90.0;b+=point_spacing)
    {
      //conversion from l,b to ra,dec
      if ( geom_gal_l_b_to_ra_dec ((double) l, (double) b,  (double*) &ra, (double*) &dec)) return 1;
      if ( fabs(ra-last_ra)>180.0) fprintf(pFile, "\n");
      if ( fabs(dec-last_dec)>90.0) fprintf(pFile, "\n");
      fprintf(pFile, "%12.8g %+12.8g %12.8g %+12.8g\n", ra, dec, l, b);
      last_ra = ra;
      last_dec = dec;
    }
    fprintf(pFile, "\n\n");
  }
  fprintf(pFile, "\n");

  last_ra = 0.0;
  last_dec = 0.0;
  for ( b=-90.0;b<=+90.0;b+=lat_spacing)
  {
    for ( l=0.0;l<360.0;l+=point_spacing)
    {
      if ( geom_gal_l_b_to_ra_dec ((double) l, (double) b,  (double*) &ra, (double*) &dec)) return 1;
      if ( fabs(ra-last_ra)>180.0) fprintf(pFile, "\n");
      if ( fabs(dec-last_dec)>90.0) fprintf(pFile, "\n");
      fprintf(pFile, "%12.8g %+12.8g %12.8g %+12.8g\n", ra, dec, l, b);
      last_ra = ra;
      last_dec = dec;
    }
    fprintf(pFile, "\n\n");
  }
    
  if ( pFile) fclose(pFile); pFile = NULL;
  return 0;
}



//write a list of notable fixed position sky objects
int geom_write_notable_location_file (const char *str_filename)
{
  FILE* pFile = NULL;
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  fprintf(pFile, "#%-11s %10s %10s %12s\n", "Name", "RA(deg)", "Dec(deg)", "Type");
  fprintf(pFile, "%-12s %10g %10g %12s\n", "SMC", GEOM_COORD_SMC_RA0, GEOM_COORD_SMC_DEC0, "NearbyGalaxy");
  fprintf(pFile, "%-12s %10g %10g %12s\n", "LMC", GEOM_COORD_LMC_RA0, GEOM_COORD_LMC_DEC0, "NearbyGalaxy");

  fprintf(pFile, "%-12s %10g %10g %12s\n", "GC",  GEOM_COORD_GC_RA0,  GEOM_COORD_GC_DEC0,  "PointOfInterest");
  fprintf(pFile, "%-12s %10g %10g %12s\n", "GaC", GEOM_COORD_GAC_RA0, GEOM_COORD_GAC_DEC0, "PointOfInterest");
  fprintf(pFile, "%-12s %10g %10g %12s\n", "SEP", GEOM_COORD_SEP_RA0, GEOM_COORD_SEP_DEC0, "PointOfInterest");

  fflush(pFile);
  
  if ( pFile ) fclose(pFile) ; pFile = NULL;
  return 0;
}



int geom_write_coordinate_grid_files ( const char *str_dirname )
{
  char str_filename[STR_MAX];
  if ( util_make_directory ((const char*) str_dirname))
  {
      return 1;
  }

  snprintf(str_filename, STR_MAX, "%s/%s", str_dirname, GEOM_COORDINATE_GRID_FILENAME_EQUATORIAL);
  if ( geom_write_coordinate_grid_file_equatorial ((const char*) str_filename))
    return 1;

  snprintf(str_filename, STR_MAX, "%s/%s", str_dirname,  GEOM_COORDINATE_GRID_FILENAME_GALACTIC);
  if ( geom_write_coordinate_grid_file_galactic   ((const char*) str_filename))
    return 1;

  snprintf(str_filename, STR_MAX, "%s/%s", str_dirname,  GEOM_NOTABLE_LOCATION_FILENAME);
  if ( geom_write_notable_location_file   ((const char*) str_filename))
    return 1;

  
  return 0;
}

int geom_write_long_lat_tics (FILE *pPlotfile,  const char *str_coord_label_colour)
{
  if ( pPlotfile == NULL ) return 1;
  fprintf(pPlotfile, "coord_label_font = \",14\"\n\
coord_label_colour = \"%s\"\n", str_coord_label_colour);

  fprintf(pPlotfile, "unset ytics\n\
unset xtics\n\
set label 10001 \"+30^{/Symbol \\260}\" at x(180,+30),y(180.,+30),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10002 \" +0^{/Symbol \\260}\" at x(180, +0),y(180., +0),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10003 \"-30^{/Symbol \\260}\" at x(180,-30),y(180.,-30),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10004 \"-60^{/Symbol \\260}\" at x(180,-60),y(180.,-60),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10005 \"-90^{/Symbol \\260}\" at x(180,-90),y(180.,-90),0.0 right front offset 0,0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10006  \"0^H\" at x( 0.*15.,0.),y( 0.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10007  \"6^H\" at x( 6.*15.,0.),y( 6.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10008 \"12^H\" at x(12.*15.,0.),y(12.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10009 \"18^H\" at x(18.*15.,0.),y(18.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour\n\
set label 10010 \"24^H\" at x(24.*15.,0.),y(24.*15.,0.),0.0 right front offset 0,-0.7 font coord_label_font tc rgb coord_label_colour\n");
  return 0;
}


int geom_write_mini_compass (FILE *pPlotfile, BOOL left_is_east)
{
  if ( pPlotfile == NULL ) return 1;

  if ( left_is_east )
  {
    fprintf(pPlotfile, "\n#This is the small compass\n\
set arrow 20001 from graph 0.98,0.10 to graph 0.98,0.18 lt 1 lw 3 lc rgb coord_label_colour head\n \
set arrow 20002 from graph 0.98,0.10 to graph 0.95,0.10 lt 1 lw 3 lc rgb coord_label_colour head\n\
set label 20001 \"N\" at graph 0.98,0.18 left front offset 0.3,0.0 font coord_label_font tc rgb coord_label_colour\n \
set label 20002 \"E\" at graph 0.95,0.10 left front offset 0.0,0.6 font coord_label_font tc rgb coord_label_colour\n");
  }
  else
  {
    fprintf(pPlotfile, "\n#This is the small compass\n\
set arrow 20001 from graph 0.95,0.10 to graph 0.95,0.18 lt 1 lw 3 lc rgb coord_label_colour head\n \
set arrow 20002 from graph 0.95,0.10 to graph 0.98,0.10 lt 1 lw 3 lc rgb coord_label_colour head\n \
set label 20001 \"N\" at graph 0.95,0.18 left front offset 0.3,0.0 font coord_label_font tc rgb coord_label_colour\n \
set label 20002 \"E\" at graph 0.98,0.10 left front offset 0.0,0.6 font coord_label_font tc rgb coord_label_colour\n");
  }
  return 0;
}



//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
int geom_write_gnuplot_map_projection_functions (FILE *pPlotfile, const char *str_griddirname, int map_projection,
                                                 BOOL left_is_east, double dec_min, double dec_max, int label_num,
                                                 const char *str_coord_label_colour)
{
  char *label_str = "";


  if ( pPlotfile == NULL ) return 1;

  
  if ( str_griddirname != NULL )
  {
    //get the working directory path
    char str_cwd[STR_MAX];
    if ( getcwd((char *) str_cwd, (size_t) STR_MAX) == NULL )
    {
      fprintf(stderr, "%s Could not get CWD at at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      (void) fflush(stderr); 
      return 1;
    }
    fprintf(pPlotfile, "infile_grid_eq  = \"%s/%s/%s\"\n\
infile_grid_gal = \"%s/%s/%s\"\n\
infile_grid_gal_l0_180 = \"< gawk 'NF == 0 {print $0;print $0} $3 == 0 || $3 == 180 {n++;if(n>360){exit};print $0}' %s/%s/%s\"\n\
notable_locations = \"%s/%s/%s\"\n\
notable_locations_gals = \"< grep NearbyGalaxy %s/%s/%s\"\n\
notable_locations_pnts = \"< grep PointOfInterest %s/%s/%s\"\n",
            str_cwd, str_griddirname, GEOM_COORDINATE_GRID_FILENAME_EQUATORIAL,
            str_cwd, str_griddirname, GEOM_COORDINATE_GRID_FILENAME_GALACTIC,
            str_cwd, str_griddirname, GEOM_COORDINATE_GRID_FILENAME_GALACTIC,
            str_cwd, str_griddirname, GEOM_NOTABLE_LOCATION_FILENAME ,
            str_cwd, str_griddirname, GEOM_NOTABLE_LOCATION_FILENAME ,
            str_cwd, str_griddirname, GEOM_NOTABLE_LOCATION_FILENAME );
  }
  
  fprintf(pPlotfile, "set angles degrees\n\
dec_min = %g\n\
dec_max = %g\n\n", dec_min, dec_max); 

  switch ( map_projection ) 
  {
  case GEOM_MAP_PROJECTION_RECTANGULAR :
    fprintf(pPlotfile, "# This is for a Cartesian projection\n\
x(ra,dec) = %s ra\n\
y(ra,dec) = dec\n\
set xlabel \"RA (J2000)\" offset 0,35\n\
set ylabel \"Dec (deg, J2000)\"\n", (left_is_east ? "360.0 - " : ""));
    //    if ( label_num > 0 ) fprintf(pPlotfile, "set label %d \"J2000 Coordinates, Cartesian Projection\" at graph 0.995,1.02 font \",10\" tc lt -1 right\n", label_num);
    label_str = "Cartesian Projection";
    break;

  case GEOM_MAP_PROJECTION_SINUSOIDAL :
  fprintf(pPlotfile, "# This is for a sinusoidal projection\n\
x(ra,dec) = %s ((ra-180.)*cos(dec)+180.)\n\
y(ra,dec) = (dec)\n\
set ytics -90,15,30\n\
set xtics out nomirror\n\
set xtics scale 0\n\
set ytics scale 0.5\n\
set format y \"%%+3g\"\n\
set xlabel \"RA (J2000)\" offset 0,35\n\
set ylabel \"Dec (deg, J2000)\"\n\
set xtics ( \"0^{/*0.8 H}\" x(0.0,dec_max), \"2^{/*0.8 H}\" x(30.,dec_max), \"4^{/*0.8 H}\" x(60.,dec_max), \"6^{/*0.8 H}\" x(90.,dec_max),\"8^{/*0.8 H}\" x(120.,dec_max),\"10^{/*0.8 H}\" x(150.,dec_max),\"12^{/*0.8 H}\" x(180.,dec_max),\"14^{/*0.8 H}\" x(210.,dec_max),\"16^{/*0.8 H}\" x(240.,dec_max),\"18^{/*0.8 H}\" x(270.,dec_max),\"20^{/*0.8 H}\" x(300.,dec_max),\"22^{/*0.8 H}\" x(330.,dec_max),\"24^{/*0.8 H}\" x(360.,dec_max) ) mirror  offset 0,32.6\n", (left_is_east ? "360.0 - " : ""));
  label_str = "Sinusoidal Projection";
    
  break;
    
  case GEOM_MAP_PROJECTION_AITOFF :
    fprintf(pPlotfile, "# This is for an Aitoff projection\n\
sinc(x) = (x==0.?1.:sin(x)/x)\n\
a(ra,dec) = acos(cos(dec)*cos((ra-180.)*0.5))\n\
x(ra,dec) = %s (180.+ 2.0*cos(dec)*sin((ra-180.)*0.5)/sinc(a(ra,dec)))\n\
y(ra,dec) = sin(dec)/sinc(a(ra,dec))\n\
unset xtics\n\
unset ytics\n\n", (left_is_east ? "360.0 - " : ""));
    label_str = "Aitoff Projection";
    break;
    
  case GEOM_MAP_PROJECTION_HAMMER_AITOFF :
    fprintf(pPlotfile, "# This is for a scaled Hammer-Aitoff projection (equal area)\n\
a(ra,dec) = sqrt(1. + cos(dec)*cos((ra-180.)*0.5))\n\
x(ra,dec) = %s (180.0+ 180.0*cos(dec)*sin((ra-180.)*0.5)/a(ra,dec))\n\
y(ra,dec) = 90.0*sin(dec)/a(ra,dec)\n\
unset xtics\n\
unset ytics\n\n", (left_is_east ? "360.0 - " : ""));
    label_str = "Hammer-Aitoff Projection";
    break;

  case GEOM_MAP_PROJECTION_PARABOLIC :
    fprintf(pPlotfile, "# This is for a Parabolic projection (equal area)\n\
x(ra,dec) = %s (ra*(2.0*cos(dec*2./3.) -1.))\n\
y(ra,dec) = 180.0 * sin(dec/3.)\n\
unset xtics\n\
unset ytics\n\n", (left_is_east ? "360.0 - " : ""));
    label_str = "Parabolic Projection";
    break;

  default:
    fprintf(stderr, "%s I do not know this type of map projection code: %d\n",
            ERROR_PREFIX, map_projection);fflush(stderr);
    return 1;
    break;
  }

  if ( TRUE )
  {
    if ( geom_write_long_lat_tics (pPlotfile, (const char*) str_coord_label_colour)) return 1;
    if ( geom_write_mini_compass  (pPlotfile, (BOOL) left_is_east)) return 1;
  }
  
  if ( label_num > 0 )
  {
    fprintf(pPlotfile,
            "set label %d \"J2000 Coordinates, %s\" at graph 0.995,1.02,1.0 font \"Times-Roman,10\" tc lt -1 right\n",
            label_num, label_str);
  }

  fprintf(pPlotfile, "\n\
min(a,b) = (a < b ? a : b)\n\
max(a,b) = (a > b ? a : b)\n\
y_min = min(y(0.,dec_min),y(180.,dec_min))\n\
y_max = max(y(0.,dec_max),y(180.,dec_max))\n\
set yrange [y_min:y_max]\n\
set xrange [0:360]\n");

  return 0;
}


//test if a coordinate is inside a polygon defined by an ordered list of its verticies
//return >0 if indide
//return 0  if outside
//return <0 if encounter an error
int geom_test_if_point_inside_polygon (int nvertex, double *px, double *py, double x, double y )
{
  int i;
  int sum = 0;
  double y_max = y;
  if ( px == NULL || py == NULL ) return -1;
  if ( nvertex < 3 ) return -1;

  //find the maximum y coordinate
  for ( i = 0; i< nvertex; i++) if ( py[i] > y_max) y_max = py[i];
  y_max = (double) fabs(y_max) * 2.0;

  //use the even-odd algorithm - 
  //cycle around the edges and count the intersections
  for(i=0; i<nvertex; i++)
  {
    int ii = (i+1) % nvertex; //second vertex of line
    if ( geom_test_if_intersect_line_seg_line_seg(px[i], py[i],
                                                  px[ii],py[ii],
                                                  x,y,
                                                  x,y_max)) sum ++;

    
  }
  if ( sum % 2 == 0 ) return 0;   //even number of crossings -> point is outside polygon
  else                return 1;   //odd  number of crossings -> point is inside polygon
}

//test if a coordinate is near any of the lines of a polygon defined by an ordered list of its verticies
//return >0 if near the edges
//return 0  if not near edges
//return <0 if we encounter an error
int geom_test_if_point_near_edges_of_polygon (int nvertex, double *px, double *py, double dist, double x, double y )
{
  int i;
  if ( px == NULL || py == NULL ) return -1;
  if ( nvertex < 3 ) return -1;

  //cycle around the edges 
  for(i=0; i<nvertex; i++)
  {
    int ii = (i+1) % nvertex; //second vertex of line
    int result;
    if ( (result = geom_test_if_near_line_seg ((double) x, (double) y,
                                               (double) px[i],  (double) py[i],
                                               (double) px[ii], (double) py[ii],
                                               (double) dist)) != 0)
      return result;
  }
  return 0;
}
