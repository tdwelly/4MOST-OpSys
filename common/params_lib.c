// -*-mode:C; compile-command:"gcc -Wall -O3 -c params_lib.c params_lib.h define.h"; -*- 
//----------------------------------------------------------------------------------
//--
//--    Filename: params_lib.c
#define CVS_REVISION "$Revision: 1.9 $"
#define CVS_DATE     "$Date: 2015/11/11 10:53:40 $"
//--    Description:
//--    Use: This module is used to organise input arguments
//--
//--
//--

#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>

#include "params_lib.h"



#define ERROR_PREFIX   "#-params_lib.c: Error:  "
#define WARNING_PREFIX "#-params_lib.c: Warning:"
#define COMMENT_PREFIX "#-params_lib.c: Comment:"
#define DEBUG_PREFIX   "#-params_lib.c: DEBUG:  "

int ParLib_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}

//safe strncpy function, makes sure that there is a null at the end of the destination buffer
//copies at most n-1 characters from src to dest
char* sstrncpy (char *dest, const char *src, size_t n)
{
  if ( dest == NULL || src == NULL || n < 1 ) return NULL;
  strncpy ((char*) dest, (const char*) src, (size_t) n-1);
  dest[n-1] = (char) '\0';
  return (char*) dest;
}

//----------------------------------------------------------------------------------------------
//-- params_str_to_BOOL
int ParLib_str_to_BOOL(const char *str, BOOL *pB)
{
  const char* pstart = str;
  int len = 0;
  
  if (str == NULL || pB == NULL ) return 1;

  if (strlen(str) == 0) return 1;
  //step past quotes and white space
  while ( *pstart == '\"' || *pstart == '\'' || *pstart == ' ' ) pstart ++;
  len = (int) strlen(pstart) -1;
  while ( pstart[len] == '\"' || pstart[len] == '\'' || pstart[len] == ' ') len --;
  
  if      (!strncasecmp(pstart, "yes",     len)) *pB = (BOOL) TRUE;
  else if (!strncasecmp(pstart, "true",    len)) *pB = (BOOL) TRUE;
  else if (!strncasecmp(pstart, "1",       len)) *pB = (BOOL) TRUE;
  else if (!strncasecmp(pstart, "y",       len)) *pB = (BOOL) TRUE;
  else if (!strncasecmp(pstart, "no",      len)) *pB = (BOOL) FALSE;
  else if (!strncasecmp(pstart, "false",   len)) *pB = (BOOL) FALSE;
  else if (!strncasecmp(pstart, "0",       len)) *pB = (BOOL) FALSE;
  else if (!strncasecmp(pstart, "n",       len)) *pB = (BOOL) FALSE;
  else
  {
    fprintf(stderr, "%s Bad value given as BOOLEAN argument str=->%s<-\n",ERROR_PREFIX, str); fflush(stderr);
    return 1;
  }
  return 0;
}///////////////////////////
//----------------------------------------------------------------------------------------------

////-- ParLib_BOOL_to_str
//char* ParLib_BOOL_to_str(BOOL B)
//{
//  if (B) return "yes";
//  else   return "no";
//  return 0;
//}///////////////////////////


//////////////////////////////////////////////
int ParLib_init_param_list(param_list_struct **ppParamList)
{
  if ( ppParamList == NULL ) return 1;

  if ( *ppParamList == NULL )
  {
    //make space for the struct
    if ( (*ppParamList = (param_list_struct*) malloc(sizeof(param_list_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  
  //now set some parameters to their default values 
  (*ppParamList)->pParam = NULL;

  //add the clobber and debug default parameters
  (*ppParamList)->num_params = 0;
  (*ppParamList)->longest_par_name_length = 0;
  //  (*ppParamList)->debug = FALSE;
//  if (ParLib_add_param_to_list ((*ppParamList), "debug",  PARLIB_ARGTYPE_BOOL, 1, &((*ppParamList)->debug),
//                               FALSE, "DEBUG", "Debug output? (yes or no)"))
//  {
//    return 1;
//  }
//
//  (*ppParamList)->clobber = FALSE;
//  if (ParLib_add_param_to_list ((*ppParamList), "clobber", PARLIB_ARGTYPE_BOOL, 1, &((*ppParamList)->clobber),
//                               FALSE, "CLOBBER", "Clobber any output files? (yes or no)"))
//  {
//    return 1;
//  }
//
//  (*ppParamList)->verbosity = PARLIB_VERBOSITY_LEVEL_NORMAL;
//  if (ParLib_add_param_to_list ((*ppParamList), "verbosity", PARLIB_ARGTYPE_INT, 1, &((*ppParamList)->verbosity),
//                               FALSE, "VERBOSITY", "How verbose to be on scale of 1(terse) to 5(verbose)"))
//  {
//    return 1;
//  }

  return 0;
}


//////////////////////////////////////////////
int ParLib_free_param_list(param_list_struct **ppParamList)
{
  if ( ppParamList == NULL ) return 1;

  if ( *ppParamList != NULL )
  {
    FREETONULL((*ppParamList)->pParam);
//    if ( (*ppParamList)->pParam )
//    {
//      free ( (*ppParamList)->pParam );
//      (*ppParamList)->pParam = NULL;
//    }
    free ( *ppParamList );
    *ppParamList = NULL;
  }
  
  return 0;
}




////------------------------------------------------------------------------------------------
////--
//int ParLib_tidy_param_list(param_list_struct *pParamList)
//{
//  if ( pParamList == NULL )
//  {
//    fprintf(stderr, "%s pParamList is NULL\n", ERROR_PREFIX);
//    return 1;
//  }
//  if ( pParamList->pParam ) free(pParamList->pParam);
//  pParamList->pParam = NULL;
//  pParamList->num_params = 0;
//  return 0;
//}


//----------------------------------------------------------------------------------
//-- ParLib_add_param_to_list_and_set_default
int ParLib_setup_param (param_list_struct *pParamList, const char *name, int argtype, int max_dim,
                        void *pValue, const char *default_value, BOOL compulsory,  const char *name_alt, const char *comment)
{
  if ( ParLib_add_param_to_list ((param_list_struct*) pParamList, (const char*) name, (int) argtype, (int) max_dim,
                                 (void*) pValue, (BOOL) compulsory, (const char*) name_alt, (const char*) comment))
  {
    fprintf(stderr, "%s Problem adding parameter ->%s<- to param_list_struct\n", ERROR_PREFIX, name);
    return 1;
  }
  if ( ParLib_set_default_value ((param_list_struct*) pParamList, (const char*) name, (const char*) default_value ))
  {
    fprintf(stderr, "%s Problem setting default value of parameter ->%s<- to ->%s<-\n", ERROR_PREFIX, name, default_value);
    return 1;
  }
  return 0;
}

//----------------------------------------------------------------------------------
//-- ParLib_add_param_to_list
int ParLib_add_param_to_list (param_list_struct *pParamList, const char *name, int argtype, int max_dim,
                              void *pValue, BOOL compulsory, const char *name_alt, const char *comment)
{
  param_struct *pP;
  if ( pParamList == NULL )
  {
    fprintf(stderr, "%s pParamList is NULL\n", ERROR_PREFIX);
    return 1;
  }
  if (pValue == NULL && argtype != PARLIB_ARGTYPE_SWITCH)
  {
    fprintf(stderr, "%s pValue doesn't point to a L-value\n", ERROR_PREFIX);
    return 1;
  }
  if (name == NULL)
  {
    fprintf(stderr, "%s name doesn't point to a string!\n", ERROR_PREFIX);
    return 1;
  }
  if (strlen(name) == 0)
  {
    fprintf(stderr, "%s name points to a zero length string!\n", ERROR_PREFIX);
    return 1;
  }

  pParamList->num_params++;
  if (pParamList->pParam) 
  {
    if (( pParamList->pParam = (param_struct*) realloc(pParamList->pParam, sizeof(param_struct) * (pParamList->num_params))) == NULL)
    {
      fprintf(stderr, "%s Couldn't reallocate memory for params list (%d params)\n", ERROR_PREFIX, pParamList->num_params);
      return 1;     
    }
  }
  else
  {
    if (( pParamList->pParam = (param_struct*) malloc(sizeof(param_struct) * (pParamList->num_params))) == NULL)
    {
      fprintf(stderr, "%s Couldn't allocate memory for params list (%d params)\n", ERROR_PREFIX, pParamList->num_params);
      return 1;     
    }
  }

  //make an abbreviation
  pP = (param_struct*) &(pParamList->pParam[pParamList->num_params-1]);
  strncpy(pP->name, name, STR_MAX);
  
  pP->compulsory  = (BOOL) compulsory;
  pP->argtype     = (unsigned) argtype;
  pP->supplied    = (BOOL) FALSE;
  pP->max_dim     = (int) max_dim;
  pP->pIval       = NULL;       
  pP->pLval       = NULL;       
  pP->pDval       = NULL;
  pP->pFval       = NULL;
  pP->pCval       = NULL;
  pP->pBval       = NULL;
  pP->supplied_from = PARLIB_SUPPLIED_FROM_NONE;
  pP->dim_supplied = (int) 0;

  switch (pP->argtype)
  {
  case PARLIB_ARGTYPE_INT          : pP->pIval = (int*)      pValue;  break;
  case PARLIB_ARGTYPE_LONG         : pP->pLval = (long*)     pValue;  break;
  case PARLIB_ARGTYPE_FLOAT        : pP->pFval = (float*)    pValue;  break;
  case PARLIB_ARGTYPE_DOUBLE       : pP->pDval = (double*)   pValue;  break;
  case PARLIB_ARGTYPE_STRING       : 
  case PARLIB_ARGTYPE_LSTRING      : pP->pCval = (char*)     pValue;  break;
  case PARLIB_ARGTYPE_BOOL         : pP->pBval = (BOOL*)     pValue;  break;
  case PARLIB_ARGTYPE_SWITCH       : break;
  default : fprintf(stderr, "%s Unknown argument type %d\n", ERROR_PREFIX, argtype); return 1; break;
  }

  if (comment)  strncpy(pP->comment,  comment,  STR_MAX);
  else          strncpy(pP->comment,  "",       STR_MAX);
  if (name_alt) strncpy(pP->name_alt, name_alt, STR_MAX);
  else          strncpy(pP->name_alt, "",       STR_MAX);        

  if ( (int) strlen(pP->name) > pParamList->longest_par_name_length )
  {
    pParamList->longest_par_name_length = (int) strlen(pP->name);
  }
  
  return 0;
}//ParLib_add_param_to_list


//ParLib_read_param_from_string
//-- if string matches the specified parameter name then this reads the value from the string
//return zero on success, >zero if we have errors, <0 if did not match param to string
int ParLib_read_param_from_string (param_struct *pP, const char *str) 
{
  int i, j = 0;
  char *pchar = NULL;
  char *pstr = NULL;
  char buffer1[LSTR_MAX];
  char buffer2[LSTR_MAX];
   char buffer3[LSTR_MAX]; //holds the param name in upper case
  BOOL matched = FALSE;
  BOOL is_switch = FALSE;
  if ( pP == NULL || str == NULL )
  {
    return 1;
  }
  //  fprintf(stdout, "DEBUG: line=%d str= >%s<\n", __LINE__, str); fflush(stdout);

  //take a local copy of string
  (void) sstrncpy(buffer1, str, LSTR_MAX);
  buffer1[LSTR_MAX-1] = '\0';

  //  fprintf(stdout, "DEBUG: line=%d buffer1= >%s<\n", __LINE__, buffer1); fflush(stdout);
  
  //strip off any trailing white space
  j = (int) strlen(buffer1)-1;
  while ((isspace(buffer1[j]) || iscntrl(buffer1[j])) && j>0)
  {
    buffer1[j] = '\0';
    if ( j > 0 )
      j--;
    else
      return -1;  //this is a blank string
  }

  //strip off any comments (anything including and after the first '#' symbol, unless it is within quotes)
  {
    int state_single_quote = 1;
    int state_double_quote = 1;
    for(j=0;j< strlen(buffer1);j++)
    {
      if      ( buffer1[j] == '\'' ) state_single_quote *= -1;
      else if ( buffer1[j] == '\"' ) state_double_quote *= -1;
      else if ( buffer1[j] == '#' &&  //if found a comment symbol
                state_single_quote > 0 &&
                state_double_quote > 0 ) //if outside a quoted string
      {
        buffer1[j] = '\0'; //terminate the string here
        break;
      }
    }
  }
  
  pstr = (char*) buffer1;
  //  fprintf(stdout, "DEBUG: line %d: pstr= >%s<\n", __LINE__, pstr); fflush(stdout);

  //step past any leading white space
  j = 0;
  while ( j < (int) strlen(pstr) && (pstr[j] == ' ' || pstr[j] == '\t')) j++;
  pstr = (char*) &(buffer1[j]);
  if ( strlen(pstr) <= 0 )  return -1;  //this is a blank string
  //  fprintf(stdout, "DEBUG: line %d: pstr= >%s<\n", __LINE__, pstr); fflush(stdout);

  if ( pstr[0] == '-'  )
  {
    //if this is a switch type argument then we don't need any equals signs
    //but we do need to flag this up
    is_switch = TRUE;
  }
  else
  {
    //count the number of equals signs in the string - it must be exactly 1 (unless the "=" is inside quotes)
    int n_eq = 0;
    //strip off any comments (anything including and after the first '#' symbol, unless it is within quotes)
    int state_single_quote = 1;
    int state_double_quote = 1;
    for(j=0;j< strlen(pstr);j++)
    {
      if      ( pstr[j] == '\'' ) state_single_quote *= -1;
      else if ( pstr[j] == '\"' ) state_double_quote *= -1;
      else if ( pstr[j] == '=' &&  //if found a comment symbol
                state_single_quote > 0 &&
                state_double_quote > 0 ) //if outside a quoted string
      {
        n_eq ++;
        break;
      }
    }

  
//    j = 0;
//    while (j < (int) strlen(pstr) ) //&& (pstr[j] != '\'' && pstr[j] != '\"'))
//    {
//      if (pstr[j] == '=') n_eq ++;
//      j ++;
//    }

    if ( n_eq != 1 )
    {
      fprintf(stdout, "%s Invalid format entry (num of equals signs=%d): >%s< => >%s<\n", WARNING_PREFIX, n_eq, str, pstr);
      return -1;
    }
  }

  //increment up to the end of the parameter name (first bit of next white space or equals sign )
  //and convert to upper case
  j = 0;
  while ( j < (int) strlen(pstr) && ( pstr[j] != '=' && pstr[j] != ' ' && pstr[j] != '\t'))
  {
    buffer2[j] = toupper(pstr[j]);
    j++;
  }
  buffer2[j] = '\0';

  //now compare with the parameter name string, upper case
  //pchar will point to a non null value if a match is not found
  pchar = NULL;
  if (j > 0)
  {
    //take a local copy of param name, and make it upper case - doing it this awkward way is safer
    (void) sstrncpy(buffer3, pP->name, LSTR_MAX);
    for(i=0;i<(int) strlen(buffer3);i++) buffer3[i]=toupper(buffer3[i]);

    //    if ( (strncmp(buffer2, buffer3, (size_t)j) == 0 ) && (int) strlen(pP->name) == j )
    if ( strcmp(buffer2, buffer3) == 0 )
    {
      pchar = (char*) &(pstr[strlen(pP->name)]);
    }
    else if (strlen(pP->name_alt))
    {
      //take a local copy of alt param name, and make it upper case
      (void) sstrncpy(buffer3, pP->name_alt, LSTR_MAX);
      for(i=0;i<(int) strlen(buffer3);i++) buffer3[i]=toupper(buffer3[i]);
      
      //      if ((strncmp(buffer2, buffer3, (size_t)j) == 0) && (int) strlen(pP->name_alt) == j )
      if ( strcmp(buffer2, buffer3) == 0 )
      {
        pchar = (char*) &(pstr[strlen(pP->name_alt)]);
      }
    }
  }
  else
  {
    fprintf(stderr, "%s Problem reading parameter name >%s< from string >%s<\n", ERROR_PREFIX, buffer3, buffer2);
    return -1;
  }

  //if we found a match to the param name, and if the found parameter is of switch type:
  if ( pchar && is_switch ) 
  {
    if ( pP->argtype != PARLIB_ARGTYPE_SWITCH )
    {
      fprintf(stdout, "%s Invalid format entry (suplied param is '-switch' type but we expected 'name=value' type): %s\n",
              WARNING_PREFIX, pstr);
      return -1;
    }
    pP->dim_supplied = 0;
    matched = TRUE;
  }
  else if (pchar)   //if we found a match to the param name
  {
    BOOL argtype_hex_flag;
    int i;
    //step past any '=' signs and white space
    j = 0;
    while ( j < (int) strlen(pchar) && (pchar[j] == '=' || pchar[j] == ' ' || pchar[j] == '\t')) j++;
    pchar = &(pchar[j]);

    //take a copy of the input parameter value string , for debug purposes mainly
    (void) sstrncpy(pP->str_value, pchar, LSTR_MAX);
    //but remove any trailing comments
    for(i=0;i<LSTR_MAX;i++)
    {
      if (pP->str_value[i] == '#')
      {
        pP->str_value[i] = '\0';
        break;
      }
    }
    //check that have actually been given a sensible value to interpret
    if (strlen(pchar) == 0 && pP->argtype != PARLIB_ARGTYPE_STRING && pP->argtype != PARLIB_ARGTYPE_LSTRING)
    {
      fprintf(stdout, "%s Invalid blank argument value: %s\n", WARNING_PREFIX,  str);
      return 1;
    }
    
    //check if value is a hexadecimal number -- only longs and ints are supported
    argtype_hex_flag = FALSE;
    if ( pP->argtype == PARLIB_ARGTYPE_LONG || pP->argtype == PARLIB_ARGTYPE_INT )
    {
      if (strlen(pchar) > 2)
      {
        if (pchar[0] == '0' && (pchar[1] == 'x' || pchar[1] == 'X' ))
        {
          argtype_hex_flag = TRUE;
          pchar = pchar + 2;
        }
      }
    }


    //how we read the value depends on the dimension of the parameter
    if ( pP->max_dim > 0 )  
    {
      //step through the input string and extract values one by one
      int n, t=0, s;
      //walk past any quotes
      while ((pchar[t]=='\"' || pchar[t]=='\'') && t<(int)strlen(pchar) ) t++;

      if ( pP->argtype == PARLIB_ARGTYPE_STRING && pP->max_dim == 1)
      {
        int len = 0;
        //strip off leading quotes
        while( *pchar == '\"' || *pchar == '\'' ) pchar ++;
        //strip off trailing quotes and new lines etc, so stop when read a non-printing character
        while( pchar[len] != '\"' && pchar[len] != '\'' && isprint(pchar[len])) len ++;
        
        strncpy(pP->pCval, pchar, (size_t) MIN(len,STR_MAX));
        pP->pCval[MIN(len,STR_MAX)] = '\0';
        matched = TRUE;
        pP->dim_supplied = 1;
        //        if ( len) {matched = TRUE; pP->dim_supplied = 1;}
        //        else      matched = FALSE;
        //        fprintf(stdout, "%s Here is the param value: %s=>%s< (orig=%s,len=%d)\n", DEBUG_PREFIX,pP->name,pP->pCval,str,len); 
      }
      else if ( pP->argtype == PARLIB_ARGTYPE_LSTRING && pP->max_dim == 1)
      {
        int len = 0;
        //strip off leading quotes
        while( *pchar == '\"' || *pchar == '\'' ) pchar ++;
        //strip off trailing quotes and new lines etc, so stop when read a non-printing character
        while( pchar[len] != '\"' && pchar[len] != '\'' && isprint(pchar[len])) len ++;
        strncpy(pP->pCval, pchar, (size_t) MIN(len,LSTR_MAX));
        pP->pCval[MIN(len,LSTR_MAX)] = '\0';
        matched = TRUE;
        pP->dim_supplied = 1;
      }
      else
      {
        char buffer3[STR_MAX];
        int ndim = 0;
        for (n=0;n<pP->max_dim;n++)
        {
          s = t;
          //walk past any punctuation to the beginning of this number
          while ((pchar[s]==' ' || pchar[s]==',' || pchar[s]==';') && s<(int)strlen(pchar))  s++;
          t = s;
          //walk past any punctuation to the end of this number
          while ((pchar[t]!=' ' && pchar[t]!=',' && pchar[t]!=';' && pchar[t]!='\"' && pchar[t]!='\'') && t<(int)strlen(pchar))  t++;
          if ( t > s )
          {
            strncpy(buffer3,&(pchar[s]), (size_t)MIN(STR_MAX,t-s));
            buffer3[MIN(STR_MAX,t-s)] = '\0';
  
            matched = TRUE;
            ndim ++;
            switch (pP->argtype)
            {
            case PARLIB_ARGTYPE_INT        :
              if (argtype_hex_flag)
              {
                unsigned int temp;
                sscanf(buffer3+2, "%x", &temp);
                pP->pIval[n] = (int) temp;
              }
              else sscanf(buffer3, "%d", &(pP->pIval[n]));
              break;
            case PARLIB_ARGTYPE_LONG        :
              if ( argtype_hex_flag)
              {
                unsigned long temp;
                sscanf(buffer3+2, "%lx", &temp);
                pP->pLval[n] = (long) temp;
              }
              else sscanf(buffer3, "%ld", &(pP->pLval[n]));
              break;
            case PARLIB_ARGTYPE_FLOAT       : sscanf(buffer3, "%f",  &(pP->pFval[n]));  break;
            case PARLIB_ARGTYPE_DOUBLE      : sscanf(buffer3, "%lf", &(pP->pDval[n]));  break;
            case PARLIB_ARGTYPE_STRING      : strncpy(&(pP->pCval[n*STR_MAX]), buffer3, STR_MAX); break;
            case PARLIB_ARGTYPE_LSTRING     : return 1; break;
            case PARLIB_ARGTYPE_BOOL        :
              {
                if (ParLib_str_to_BOOL(buffer3, &(pP->pBval[n])))
                {
                  fprintf(stderr, "%s Invalid BOOL argument value: %s=%s\n", ERROR_PREFIX,  pP->name, buffer3);
                  pP->pBval[n] = FALSE;
                  return 1;
                }
              }
              break;
            default:
              ndim --;
              matched = FALSE;
              break;
            }//switch
            pP->dim_supplied = ndim;

            //end of string
            if (t >= (int) strlen(pchar)) break;               //break out of loop 
          }//end of dim loop
        }
      }
    }
  }

  if ( matched )
  {
    pP->supplied = matched; //flag up that the parameter has been supplied
    return 0;
  }
  return -1;
}

//same as ParLib_read_params_from_string but tests against all params in list
int ParLib_read_params_from_string (param_list_struct *pParamList, const char* str, int *pWhichParamIndex) 
{
  int j;
  if ( pParamList == NULL )
  {
    fprintf(stderr, "%s pParamList is NULL\n", ERROR_PREFIX);
    return 1;
  }

  //for each param
  for (j=0;j<pParamList->num_params;j++)
  {
    param_struct *pP = &(pParamList->pParam[j]);
    //    pP->supplied = FALSE;
    int flag = ParLib_read_param_from_string ((param_struct*) pP, str);
    if (flag > 0 )
    {
      fprintf(stderr, "%s Problem reading params from string: %s\n", ERROR_PREFIX, str);
      return 1;
    }
    else if ( flag == 0 && pP->supplied)
    {
      if ( pWhichParamIndex ) *pWhichParamIndex = (int) j; 
      return 0; //success
    }
  }
  //  fprintf(stdout, "%s Problem reading params from string: %s\n", WARNING_PREFIX, str);
  if ( pWhichParamIndex ) *pWhichParamIndex = -1; 
  return -1;  //return -1 because string was not matched to any param
}

//return >0 if error
//return 0  if sucess
//return <0 if not found
int ParLib_read_params_from_command_line (param_list_struct *pParamList, int argc, char *argv[]) 
{
  int i;
  if ( pParamList == NULL )
  {
    fprintf(stderr, "%s pParamList is NULL\n", ERROR_PREFIX);
    return 1;
  }
  
  //for each command line arg
  for (i=1;i<argc;i++)
  {
    int which_param = -1;
    int flag = ParLib_read_params_from_string (pParamList, argv[i], &which_param);
    if (flag > 0)
    {
      fprintf(stderr, "%s Problem reading params from command line (argument %d): %s\n", ERROR_PREFIX, i, argv[i]);
      return 1;
    }
    else if ( flag < 0 )
    {
    }
    else
    {
      //success
      pParamList->pParam[which_param].supplied_from = PARLIB_SUPPLIED_FROM_CMDL;
    }
  }

  return 0;
}

//return >0 if error
//return 0  if sucess
//return <0 if not found
int ParLib_read_param_from_command_line (param_struct *pP, int argc, char *argv[]) 
{
  int i;
  if ( pP == NULL )
  {
    fprintf(stderr, "%s pP is NULL\n", ERROR_PREFIX);
    return 1;
  }
  
  //for each command line arg
  for (i=1;i<argc;i++)
  {
    //    fprintf(stdout, "DEBUG: line=%d: argv[%d] = >%s<\n", __LINE__, i, argv[i]); fflush(stdout);
    int flag = ParLib_read_param_from_string ((param_struct*) pP, argv[i]);
    if (flag > 0) //real error
    {
      fprintf(stderr, "%s Problem (flag=%d) reading param %s from command line (argument %d): %s\n", ERROR_PREFIX, flag, pP->name, i, argv[i]);
      return 1;
    }
    else if ( flag == 0 ) 
    {
      //success - stop when we have got what we wanted
      pP->supplied_from = PARLIB_SUPPLIED_FROM_CMDL;
      return 0;
    }
    else
    {
      // continue silently
    }
  }
  return 0;
}

/////////////////
int ParLib_read_params_from_file (param_list_struct *pParamList, char *str_filename ) 
{
  int line = 0;
  FILE *pfile = NULL;
  char buffer [LSTR_MAX];
  if ( pParamList == NULL || strlen(str_filename) == 0 )
    return 1;

  //open the param file
  if ((pfile = (FILE*) fopen(str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s Error, I had problems opening the parameter file: >%s<\n", ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  while (fgets(buffer,LSTR_MAX,pfile))
  {
    int j;
    int flag;
    int which_param = -1;
    line ++;

    //safety stop
    buffer[LSTR_MAX -1] = '\0'; 

    //strip off any trailing white space and junk
    j = (int) strlen(buffer)-1;
    while ((isspace(buffer[j]) || iscntrl(buffer[j])) && j>0) {buffer[j] = '\0'; j--;}

    j = 0;
    //step forward to first non-blank character
    while (isspace(buffer[j]) && j<(LSTR_MAX-1)) j++;
    //check for blank lines
    if ( strlen(&(buffer[j])) == 0) continue;


    if ( buffer[j] == '#')
    {
      //      fprintf(stdout, "This is a comment line: %d: j=%d, buffer=%s\n", line, j, &(buffer[j])); fflush(stdout);
      continue; //ignore any comment lines
    }    
    if ( (flag = ParLib_read_params_from_string (pParamList, &(buffer[j]), &which_param)) != 0 )
    {
      if (flag > 0)
      {
        fprintf(stderr, "%s Problem reading params from file %s (line %3d): %s\n", ERROR_PREFIX, str_filename, line, buffer);
        return 1;
      }
      else if ( flag < 0 )
      {
        //        if (pParamList->verbosity >= PARLIB_VERBOSITY_LEVEL_NORMAL )
        //        {      
          fprintf(stdout, "%s Unknown param in file %s (line %3d): %s\n", WARNING_PREFIX, str_filename, line, buffer);
          //        }
      }
    }
    else
    {
      //success
      pParamList->pParam[which_param].supplied_from = PARLIB_SUPPLIED_FROM_FILE;
    }
  }
  if (pfile) fclose(pfile); pfile = NULL;
  return 0;
}

/////////////////
//return 0 on success
//return >zero if we have errors and <0 if did not match param to string
int ParLib_read_param_from_file (param_struct *pP, char *str_filename ) 
{
  int line = 0;
  FILE *pfile = NULL;
  char buffer [LSTR_MAX];
  int result_flag = -1;
  if ( pP == NULL || strlen(str_filename) == 0 )   return 1;

  //open the param file
  if ((pfile = (FILE*) fopen(str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s Error, I had problems opening the parameter file: >%s<\n", ERROR_PREFIX, str_filename);fflush(stderr);
    return 1;
  }

  while (fgets(buffer,LSTR_MAX,pfile))
  {
    int j;
    int flag;
    line ++;

    //safety stop
    buffer[LSTR_MAX -1] = '\0'; 

    //strip off any trailing white space and junk
    j = (int) strlen(buffer)-1;
    while ((isspace(buffer[j]) || iscntrl(buffer[j])) && j>0) {buffer[j] = '\0'; j--;}

    j = 0;
    //step forward to first non-blank character
    while (isspace(buffer[j]) && j<(LSTR_MAX-1)) j++;
    //check for blank lines
    if ( strlen(&(buffer[j])) == 0) continue;

    if ( buffer[j] == '#')
    {
      continue; //ignore any comment lines
    }    

    if ( (flag = ParLib_read_param_from_string ((param_struct*) pP, &(buffer[j]))) == 0 )
    {
      //success
      pP->supplied_from = PARLIB_SUPPLIED_FROM_FILE;
      result_flag = 0;
      break;
    }
    else
    {
      if (flag > 0) //real error
      {
        fprintf(stderr, "%s Problem reading params from file %s (line %3d): %s\n", ERROR_PREFIX, str_filename, line, buffer);
        return 1;
      }
      else if ( flag < 0 ) //not a real error
      {
      }
    }
  }
  if (pfile) fclose(pfile); pfile = NULL;
  return result_flag;
}

//------------------------------------------------------------------------------------------
//--
int ParLib_check_param_list(param_list_struct *pParamList)
{
  int j;
  int num_missing = 0;
  if ( pParamList == NULL )
  {
    fprintf(stderr, "%s pParamList is NULL\n", ERROR_PREFIX);
    return 1;
  }
  for (j = 0; j < pParamList->num_params; j++)  //do  run through to check all params supplied
  {
    if (!(pParamList->pParam[j].supplied) && pParamList->pParam[j].compulsory )
    {
      //      if (pParamList->verbosity >= PARLIB_VERBOSITY_LEVEL_NORMAL )
      //      {
      fprintf(stderr, "%s Compulsory argument \'%12s\' was not supplied\n", ERROR_PREFIX, pParamList->pParam[j].name);
        //      }
      num_missing ++;
    }
  }
  //return >0 if some vital arguments not supplied 
  return num_missing;
}
//------------------------------------------------------------------------------------------

int ParLib_print_param_prefix(FILE *pfile, param_list_struct *pParamList, param_struct *pP, const char *str_prefix)
{
  int i;
  int total = 0;
  char buffer[LSTR_MAX];
  char format[STR_MAX];
  int print_dims;
  if (pfile == NULL ||
      pParamList == NULL ||
      pP == NULL ) return 1;
  snprintf(format, STR_MAX, "%%s%%7s %%-%ds ", pParamList->longest_par_name_length);

  switch (pP->argtype)
  {
  case PARLIB_ARGTYPE_INT    : fprintf(pfile, format, str_prefix, "INT",    pP->name);   break;
  case PARLIB_ARGTYPE_LONG   : fprintf(pfile, format, str_prefix, "LONG",   pP->name);   break;
  case PARLIB_ARGTYPE_FLOAT  : fprintf(pfile, format, str_prefix, "FLOAT",  pP->name);   break;
  case PARLIB_ARGTYPE_DOUBLE : fprintf(pfile, format, str_prefix, "DOUBLE", pP->name);   break;
  case PARLIB_ARGTYPE_STRING : fprintf(pfile, format, str_prefix, "STRING", pP->name);   break;
  case PARLIB_ARGTYPE_LSTRING: fprintf(pfile, format, str_prefix, "LSTRING",pP->name);   break;
  case PARLIB_ARGTYPE_BOOL   : fprintf(pfile, format, str_prefix, "BOOL",   pP->name);   break;
  case PARLIB_ARGTYPE_SWITCH : fprintf(pfile, format, str_prefix, "SWITCH", pP->name);   break;
  default : break;
  }

  print_dims = MAX(1,MIN(pP->max_dim,pP->dim_supplied));
  for(i=0;i<print_dims;i++)
    //  for(i=0;i<pP->max_dim;i++)
  //  for(i=0;i<pP->dim_supplied;i++)
  {
    switch (pP->argtype)
    {
    case PARLIB_ARGTYPE_INT    : snprintf(buffer,STR_MAX,"%d", pP->pIval[i]);  break;
    case PARLIB_ARGTYPE_LONG   : snprintf(buffer,STR_MAX,"%ld",pP->pLval[i]);  break;
    case PARLIB_ARGTYPE_FLOAT  : snprintf(buffer,STR_MAX,"%g", pP->pFval[i]);  break;
    case PARLIB_ARGTYPE_DOUBLE : snprintf(buffer,STR_MAX,"%g", pP->pDval[i]);  break;
    case PARLIB_ARGTYPE_STRING : snprintf(buffer,STR_MAX,"%s", &(pP->pCval[i*STR_MAX]));   break;
    case PARLIB_ARGTYPE_LSTRING: snprintf(buffer,STR_MAX,"%s", &(pP->pCval[i*LSTR_MAX]));   break;
    case PARLIB_ARGTYPE_BOOL   : snprintf(buffer,STR_MAX,"%s", BOOL2TF(pP->pBval[i]));   break;
    case PARLIB_ARGTYPE_SWITCH : snprintf(buffer,STR_MAX,"%s", BOOL2TF(pP->supplied));   break;
    default : break;
    }
    fprintf(pfile,"%s ", buffer);
    total += strlen(buffer)+1;
  }
  for(i=0;i<50-total;i++) {buffer[i] = ' ';};
  buffer[i] = '\0';
  //  fprintf(pfile,"%s ", buffer));
  
  fprintf(pfile,"%s %3d %3s %3s %3d %4s %s\n",
          buffer, pP->max_dim,BOOL2STR(pP->compulsory),BOOL2STR(pP->supplied),pP->dim_supplied,
          SUPP_FROM2STR(pP->supplied_from),pP->comment );
  //  fprintf(pfile,"%s %3d %3s %3s %4s %s\n", buffer, pP->dim,BOOL2STR(pP->compulsory),BOOL2STR(pP->supplied),
  //          (pP->supplied_from==PARLIB_SUPPLIED_FROM_CMDL?"CMDL":(pP->supplied_from==PARLIB_SUPPLIED_FROM_FILE?"FILE":"....")),pP->comment );
  //fprintf(pfile,"%s %3d %3d %3d %4d %s\n", buffer, pP->dim,pP->compulsory,pP->supplied,
  //        pP->supplied_from, pP->comment );
  return 0;
}

//-----------------------------------------------------------------------------------------------------------
int ParLib_print_param_list_prefix(FILE *pfile, param_list_struct *pParamList, const char *str_prefix)
{
  int i;
  char format[STR_MAX]; 
  if (pfile == NULL ) return 1;
  if ( pParamList == NULL )
  {
    fprintf(stderr, "%s pParamList is NULL\n", ERROR_PREFIX);
    return 1;
  }

      //fprintf(pfile, "%sInput parameter list: \n", str_prefix);
  snprintf(format, STR_MAX, "%%s%%-7s %%-%ds %%-50s %%3s %%3s %%3s %%3s %%4s %%s\n", pParamList->longest_par_name_length);
  //  fprintf(pfile, "%s", format);
  fprintf(pfile, format, str_prefix, "#Type", "Name", "Current Value", "DIM","Req", "Sup", "dim", "C/F?", "Comment");
  //  fprintf(pfile, "%s%6s %-40s %-50s %3s %3s %3s %4s %s\n", str_prefix, "Type", "Name", "Current Value", "Dim","Req", "Sup", "C/F?", "Comment");

  for (i = 0; i < pParamList->num_params; i++)
  {
    ParLib_print_param_prefix((FILE*) pfile, (param_list_struct*) pParamList,
                              (param_struct*) &(pParamList->pParam[i]), (const char*) str_prefix);
  }

    return 0;
}

//-----------------------------------------------------------------------------------------------------------
int ParLib_print_param_list(FILE *pfile, param_list_struct *pParamList)
{
  ParLib_print_param_list_prefix(pfile, pParamList, "");
  return 0;
}

//-----------------------------------------------------------------------------------------------------------
int ParLib_print_param_list_to_file(const char* str_filename, param_list_struct *pParamList)
{
  FILE* pFile = NULL;
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  if ( ParLib_print_param_list((FILE *) pFile, (param_list_struct*) pParamList))
    {
    fprintf(stderr, "%s  I had problems writing to the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  fflush(pFile);
  if (pFile) fclose(pFile);  pFile = NULL;
  return 0;
}


int ParLib_print_example_params_file(FILE *pfile, param_list_struct *pParamList)
{
  int i;
  if ( pfile == NULL || pParamList == NULL ) return 1;
  fprintf(pfile, "#Printing an example of a valid input parameter file, as requested:\n");


  for (i = 0; i < pParamList->num_params; i++)
  {
    int j, print_dims;
    param_struct *pP = &(pParamList->pParam[i]);
    char *str_equals = NULL;
    char *str_quotes = NULL;
    char *str_prefix = NULL;
    char str_buffer1[LSTR_MAX];
    char str_buffer2[LSTR_MAX];
    char str_buffer3[LSTR_MAX];
    if (pP->argtype == PARLIB_ARGTYPE_SWITCH) {str_equals = ""; str_prefix= "# ";}
    else                                      {str_equals = "= "; str_prefix= "";}
    
    if (pP->argtype == PARLIB_ARGTYPE_STRING || pP->dim_supplied > 1) str_quotes = "\'";
    else                                                              str_quotes = "";

    sstrncpy(str_buffer2, "", STR_MAX);
    print_dims = MAX(1,MIN(pP->max_dim,pP->dim_supplied));
    for(j=0;j<print_dims;j++)
    {
      switch (pP->argtype)
      {
      case PARLIB_ARGTYPE_INT    : snprintf(str_buffer1,LSTR_MAX,"%d", pP->pIval[j]);  break;
      case PARLIB_ARGTYPE_LONG   : snprintf(str_buffer1,LSTR_MAX,"%ld",pP->pLval[j]);  break;
      case PARLIB_ARGTYPE_FLOAT  : snprintf(str_buffer1,LSTR_MAX,"%g", pP->pFval[j]);  break;
      case PARLIB_ARGTYPE_DOUBLE : snprintf(str_buffer1,LSTR_MAX,"%g", pP->pDval[j]);  break;
      case PARLIB_ARGTYPE_STRING : snprintf(str_buffer1,LSTR_MAX,"%s", &(pP->pCval[j*STR_MAX]));   break;
      case PARLIB_ARGTYPE_LSTRING: snprintf(str_buffer1,LSTR_MAX,"%s", &(pP->pCval[j*LSTR_MAX]));   break;
      case PARLIB_ARGTYPE_BOOL   : snprintf(str_buffer1,LSTR_MAX,"%s", BOOL2TF(pP->pBval[j]));   break;
      case PARLIB_ARGTYPE_SWITCH : sstrncpy(str_buffer1, "", LSTR_MAX);break;
      default : break;
      }
      snprintf(str_buffer3, LSTR_MAX, "%s%s%s", str_buffer2, (j==0?"":" "),str_buffer1);
      sstrncpy(str_buffer2, str_buffer3, LSTR_MAX);
    }
    snprintf(str_buffer1, LSTR_MAX, "%s%s%s%s", str_equals,str_quotes,str_buffer2,str_quotes);
    {
      char str_format[STR_MAX];
      snprintf(str_format, STR_MAX, "%%s%%-%ds %%-50s # %%s\n", pParamList->longest_par_name_length);
      //    fprintf(pfile, "%s%-40s %-40s # %s\n", str_prefix, pP->name, str_buffer1, pP->comment);
      fprintf(pfile, str_format, str_prefix, pP->name, str_buffer1, pP->comment);
    }
    fflush(pfile);
  }


  return 0;
}


//supplement to char.h programs 
int ParLib_isnumeric (char c)
{
  if ( c == (char) 0) return 0;
  if ( (c >= '0' && c <= '9') || c == '.' || c == 'e' || c== 'E' || c== '-' || c== '+' ) return 1;
  return 0;
}


///////////////////////////////////////
//find the first parameter in the list that matches the given name
//return a pointer to the parameter struct, or null if any error 
param_struct* ParLib_get_param_by_name(param_list_struct* pParamList, const char* param_name)
{
  int i;
  if ( pParamList  == NULL ) return NULL; 
  for(i=0;i<pParamList->num_params;i++)
  {
    //    if ( strncmp(param_name, pParamList->pParam[i].name  ,strlen(param_name)) == 0 )
    //    if ( strncasecmp(param_name, pParamList->pParam[i].name  ,strlen(param_name)) == 0 )
    if ( strcasecmp(param_name, pParamList->pParam[i].name) == 0 )
    {
      return (param_struct*) &(pParamList->pParam[i]);
    }
  }
  return NULL;
}

//-----------------------------------------------------------------------------------------------------------
//return the dimesion of the array of values that were supplied on the CL, return negative value for error 
int ParLib_get_dim_supplied(param_list_struct *pParamList,  const char *param_name)
{
  param_struct *pP = NULL;
  if ( pParamList  == NULL ) return -1; 
  //first get the relevant parameter struct
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pParamList, (const char*) param_name)) == NULL)
  {
    //a parameter with this name was not found
    return -1;
  }
  
  return pP->dim_supplied;
}

//-----------------------------------------------------------------------------------------------------------
//return the current value of the parameter
//return value is < 0 for error, >0 if no parameter with this name exists, and =0 for success
//returns only the first member of any array params -
//TODO: function ParLib_get_param_value_by_name() should be able to handle arrays
int ParLib_get_param_value_by_name(param_list_struct *pParamList,  const char *param_name, void* pValue)
{
  param_struct *pP = NULL;
  void* pVal = NULL;
  size_t datasize = 0;
  if ( pParamList  == NULL || pValue == NULL ) return -1; 

  //first get the relevant parameter struct
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pParamList, (const char*) param_name)) == NULL)
  {
    //a parameter with this name was not found
    return 1;
  }

  switch (pP->argtype)
  {
  case PARLIB_ARGTYPE_INT          :   pVal = (void*) pP->pIval; datasize=(size_t) sizeof(int)   ; break;
  case PARLIB_ARGTYPE_LONG         :   pVal = (void*) pP->pLval; datasize=(size_t) sizeof(long)  ; break;
  case PARLIB_ARGTYPE_FLOAT        :   pVal = (void*) pP->pFval; datasize=(size_t) sizeof(float) ; break;
  case PARLIB_ARGTYPE_DOUBLE       :   pVal = (void*) pP->pDval; datasize=(size_t) sizeof(double); break;
  case PARLIB_ARGTYPE_STRING       :   pVal = (void*) pP->pCval; datasize=(size_t) sizeof(char)  ; break;
  case PARLIB_ARGTYPE_LSTRING      :   pVal = (void*) pP->pCval; datasize=(size_t) sizeof(char)  ; break;
  case PARLIB_ARGTYPE_BOOL         :   pVal = (void*) pP->pBval; datasize=(size_t) sizeof(BOOL)  ; break;
  case PARLIB_ARGTYPE_SWITCH       :   pVal = (void*) &(pP->supplied); datasize=(size_t) sizeof(BOOL)  ; break;
  default :
    fprintf(stderr, "%s Unknown argument type %d\n", ERROR_PREFIX, pP->argtype);
    return 1; 
  }
  //copy over the data
  memcpy((void*) pValue, (const void*) (BYTE*) pVal, (size_t) datasize);
  
  return 0;
}



int ParLib_get_supplied_flag(param_list_struct *pParamList,  const char *param_name)
{
  param_struct *pP = NULL;
  if ( pParamList  == NULL ) return -2; 
  //first get the relevant parameter struct
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pParamList, (const char*) param_name)) == NULL)
  {
    //a parameter with this name was not found
    return -1;
  }
  return (int) pP->supplied;
}


int ParLib_set_default_value(param_list_struct *pParamList,  const char *param_name, const char* param_value )
{
  param_struct *pP = NULL;
  if ( pParamList == NULL  ) return -2; 
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pParamList, (const char*) param_name)) == NULL)
  {
    fprintf(stderr, "%s Unknown param %s\n", ERROR_PREFIX, param_name);
    //a parameter with this name was not found
    return -1;
  }

  if ( pP->argtype != PARLIB_ARGTYPE_SWITCH )
  {
    char buffer[STR_MAX+LSTR_MAX]; 
    snprintf(buffer, STR_MAX+LSTR_MAX, "%s=%s", param_name, param_value);
    
    if ( ParLib_read_param_from_string ((param_struct *) pP, (const char *) buffer))
    {
      return 1;
    }
    pP->supplied = FALSE; //reset to false because default values do no count as being "supplied"
  }
  return 0;
}

int ParLib_write_param_html_table_entry(FILE* pFile, param_list_struct *pParamList, const char *param_name,  const char* str_style, const char* str_extra) 
{
  param_struct* pPar = NULL;
  if ( pParamList == NULL || pFile == NULL ) return 1;
  if ( strlen(param_name) <= 0 ) return 2;  
  pPar = (param_struct*) ParLib_get_param_by_name ((param_list_struct*) pParamList, (const char*) param_name);
  if ( pPar == NULL ) return 3;
  fprintf(pFile, "<tr %s>\n\
    <td>%s</td>\n\
    <td>%s</td>\n\
    <td>%s</td>\n\
  </tr>\n", str_style, pPar->name, pPar->str_value, str_extra);
  
  return 0;  
}

int ParLib_get_param_str_value (param_list_struct *pParamList, const char *param_name, char* str_result) 
{
  param_struct* pPar = NULL;
  if ( pParamList == NULL || str_result == NULL ) return 1;
  if ( strlen(param_name) <= 0 ) return 2;  
  pPar = (param_struct*) ParLib_get_param_by_name ((param_list_struct*) pParamList, (const char*) param_name);
  if ( pPar == NULL ) return -1;
  snprintf(str_result, LSTR_MAX, "%s", pPar->str_value);

  return 0;  
}


/*
//print all of the paramter values in nice html format
//TODO - complete function: ParLib_write_param_html_table
int ParLib_write_param_html_table(FILE* pFile, param_list_struct *pParamList) 
{
  if ( pParamList == NULL || pFile == NULL ) return 1;

  //print a header
  
  for ( i=0; i< pParamList->num_params; i++)
  {
    param_struct* pPar = (param_struct*) &(pParamList->pParam[i]);
    fprintf(pFile, "  <tr>\n\
    <td>%s</td>\n\
    <td>%s</td>\n\
    <td>%s</td>\n\
    <td>%s</td>\n\
  </tr>\n", pPar->name, pPar->str_value, pPar->supplied, pPar->comment);
  }
  
  return 0;  
}
*/
