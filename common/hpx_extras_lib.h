//----------------------------------------------------------------------------------
//--
//--    Filename: hpx_extras_lib.h
//--    Use: This is a header for the hpx_extras_lib.c C module
//--
//--    Notes:
//--

#ifndef HPX_EXTRAS_H
#define HPX_EXTRAS_H

//#define CVS_REVISION_HPX_EXTRAS_LIB_H "$Revision: 1.4 $"
//#define CVS_DATE_HPX_EXTRAS_LIB_H     "$Date: 2015/08/18 11:42:08 $"


#include "define.h"

//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.4 $"
#define CVS_DATE_H     "$Date: 2015/08/18 11:42:08 $"
inline static int hpx_extras_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN



typedef struct {
  long index;   //healpix pixel number
  double ra0;   //coords at pixel centre
  double dec0;
  double cos_dec0;
  double sin_dec0;
  int   array_length;   //the size of the array pointed to by pMembers
  int   nMembers;       //the number of entries in pMembers
  void **pMembers;      //allocated at runtime
  BOOL in_bounds_flag;  //set to true if within the survey limits
} hpx_pix_struct;



typedef struct {
  long nside;   //healpix nside param
  long npix;
  double pix_area_deg2; 
  double pix_size_deg; 
  hpx_pix_struct *pPix;
} hpx_pixlist_struct;


//functions

int    hpx_extras_print_version      (FILE* pFile);

long   hpx_extras_ring_num           (long nside, double z, long shift);
double hpx_extras_ring2z             (long nside, long ir);
double hpx_extras_fudge_query_radius (long nside, double radius_in, BOOL do_quadratic);
int    hpx_extras_discphirange_at_z  (double *pvector0, double radius_rad, double *pz, long nz,
                                      double *pdphi, double *pphi0);
int    hpx_extras_pixels_per_ring    (long nside, long ring, long *pnpr, long *pkshift, long *pnpnorth);
int    hpx_extras_pixels_on_edge     (long nside, long irmin, long irmax, double phi0, double *ppdphi,
                                      long *pringphi1, long *pringphi2, long *pringphi3, long *pngr);
int    hpx_extras_check_edge_pixels  (long nside, long nsboost, long irmin, long irmax, double phi0,
                                      double *pdphi,
                                      long *pringphi1, long *pringphi2, long *pringphi3,
                                      long *pngr);
int    hpx_extras_find_pixel_bounds  (long nside, long nsboost, long iring, long iphi, double *pphiw, double *pphie);
int    hpx_extras_discedge2fulldisc  (long nside, long *pringphi1, long *pringphi2, long *pringphi3,
                                      long ngr, long maxlist, long *plist, long *pnlist);
int    hpx_extras_query_disc         (long nside, double long0_deg, double lat0_deg, double radius_deg,
                                      long maxlist, long *plistpix, long *pnlist, BOOL is_nest, BOOL do_inclusive);

int    hpx_pixlist_init              (hpx_pixlist_struct *pPixlist);
int    hpx_pixlist_setup             (hpx_pixlist_struct *pPixlist, long nside);
int    hpx_pixlist_free              (hpx_pixlist_struct *pPixlist);



#endif
