//this is header file for the quadtree_lib.c source file

#ifndef QUADTREE_H
#define QUADTREE_H


//#define CVS_REVISION_QUADTREE_LIB_H "$Revision: 1.3 $"
//#define CVS_DATE_QUADTREE_LIB_H     "$Date: 2015/08/18 11:42:08 $"

//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.3 $"
#define CVS_DATE_H     "$Date: 2015/08/18 11:42:08 $"
inline static int quad_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define QUADTREE_MAX_DEPTH 16  //this would be 2*(4^16) quads ie ~8billion
//test..to make sure that we don't go too far
//#define QUADTREE_MAX_DEPTH 4   //this would be 2*(4^4) quads ie 512
//#define QUADTREE_MAX_MEMBERS 128 //
//#define QUADTREE_MAX_MEMBERS 1024 //
//#define QUADTREE_MAX_MEMBERS 8192 //
#define QUADTREE_MAX_MEMBERS 512 //

#define QUADTREE_NCHILD 4

#define QUADTREE_CHILD_BOTTOM_LEFT  0
#define QUADTREE_CHILD_BOTTOM_RIGHT 1
#define QUADTREE_CHILD_TOP_LEFT     2
#define QUADTREE_CHILD_TOP_RIGHT    3

//shorthand
#define QUAD_BL  QUADTREE_CHILD_BOTTOM_LEFT
#define QUAD_BR  QUADTREE_CHILD_BOTTOM_RIGHT
#define QUAD_TL  QUADTREE_CHILD_TOP_LEFT
#define QUAD_TR  QUADTREE_CHILD_TOP_RIGHT

//these index the 8 neighbours of a quad struct
#define QUADTREE_NNEIGHBOURS 8
#define QUADTREE_NEIGHBOUR_NN 0
#define QUADTREE_NEIGHBOUR_NE 1
#define QUADTREE_NEIGHBOUR_EE 2
#define QUADTREE_NEIGHBOUR_SE 3
#define QUADTREE_NEIGHBOUR_SS 4
#define QUADTREE_NEIGHBOUR_SW 5
#define QUADTREE_NEIGHBOUR_WW 6
#define QUADTREE_NEIGHBOUR_NW 7

//we can OR these together to show which neighbours each quad posesses
#define QUADTREE_NEIGHBOUR_MASK_NN 0x01
#define QUADTREE_NEIGHBOUR_MASK_NE 0x02
#define QUADTREE_NEIGHBOUR_MASK_EE 0x04
#define QUADTREE_NEIGHBOUR_MASK_SE 0x08
#define QUADTREE_NEIGHBOUR_MASK_SS 0x10
#define QUADTREE_NEIGHBOUR_MASK_SW 0x20
#define QUADTREE_NEIGHBOUR_MASK_WW 0x40
#define QUADTREE_NEIGHBOUR_MASK_NW 0x80


typedef struct quadtree_struct {
  //coords of the middle of the quad
  double x_mid;
  double y_mid;

  //coords of the corners of the quad
  double x_min;
  double x_max;
  double y_min;
  double y_max;
  //  double area;

  //pointers to the parent and the four children;
  struct quadtree_struct *pParent;
  struct quadtree_struct *pChild[QUADTREE_NCHILD];

  //this pointer lets us step through all quads at the same depth
  struct quadtree_struct *pNextPeer;

  //these pointers let us access all the neighbours of a quad at the same depth
  struct quadtree_struct *pNeighbours[QUADTREE_NNEIGHBOURS];
  unsigned int neighbours_mask;
  
  //number of children for this quad
  int          nChild;
  //  BOOL is_root; //true if this is the largest (root) quad
  //  BOOL is_tip;  //true if this is the smallest quad
  int          depth; // this gets set to 0 for root, 1 for next level, 2 for next etc etc
  //the following is used to attach a list of (generic) pointers to structures to a quad 
  int   nMembers;
  void *pMembers[QUADTREE_MAX_MEMBERS]; //TODO make this a run-time defined length
} quad_struct;

//struct quad_struct;
//typedef struct quad_struct quad_struct;



int          quad_print_version     (FILE* pFile);
int          quad_init_root                 (quad_struct *pQ, double x_min, double x_max, double y_min, double y_max );
int          quad_add_child                 (quad_struct *pQ, int index );
int          quad_add_children              (quad_struct *pQ );
int          quad_add_children_to_depth     (quad_struct *pQ, int depth );
quad_struct* quad_get_quad_from_position    (quad_struct *pQ, double x, double y);
quad_struct* quad_get_quad_from_position_and_depth  (quad_struct *pQ, double x, double y, int depth);
int          quad_assign_object_to_quad     (quad_struct *pQ, double x, double y, void *pObject);
int          quad_free_quad                 (quad_struct *pQ );
quad_struct* quad_get_quad_from_depth_index (quad_struct *pQ, int depth, int index );
int          quad_print_quad                (FILE* pFile, quad_struct *pQ);
int          quad_add_unique_member_to_quad (quad_struct *pQ, void *pVoid );
int          quad_write_vertices_file       (quad_struct *pQ, const char *str_filename);
int          quad_calc_neighbours           (quad_struct *pQ);








#endif
