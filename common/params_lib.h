//----------------------------------------------------------------------------------
//--
//--    Filename: params_lib.h
//--    Use: This is a header for the params C program
//--
//--    Notes:
//--

#ifndef PARAMS_H
#define PARAMS_H
//#define CVS_REVISION_PARAMS_LIB_H "$Revision: 1.4 $"
//#define CVS_DATE_PARAMS_LIB_H     "$Date: 2015/08/18 11:42:08 $"

#include <stdio.h>
#include "define.h"

//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.4 $"
#define CVS_DATE_H     "$Date: 2015/08/18 11:42:08 $"
inline static int ParLib_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//


#define STR_MAX DEF_STRLEN
#define LSTR_MAX DEF_LONGSTRLEN
//#define VERY_LONG_STR_MAX DEF_VERYLONGSTRLEN

#define PARLIB_ARGTYPE_SWITCH      0
#define PARLIB_ARGTYPE_INT         1   //argument type id
#define PARLIB_ARGTYPE_LONG        2                                  
#define PARLIB_ARGTYPE_FLOAT       3
#define PARLIB_ARGTYPE_DOUBLE      4
#define PARLIB_ARGTYPE_BOOL        5
#define PARLIB_ARGTYPE_STRING      6
#define PARLIB_ARGTYPE_LSTRING     7
//#define PARLIB_ARGTYPE_LONG_STRING 7

#define PARLIB_SUPPLIED_FROM_NONE 0
#define PARLIB_SUPPLIED_FROM_CMDL 1
#define PARLIB_SUPPLIED_FROM_FILE 2

#define SUPP_FROM2STR(x) ((x)==PARLIB_SUPPLIED_FROM_CMDL? "CMDL" :((x)==PARLIB_SUPPLIED_FROM_FILE?"FILE":"...."))

//#define PARLIB_VERBOSITY_LEVEL_SILENT 1
//#define PARLIB_VERBOSITY_LEVEL_QUIET  2
//#define PARLIB_VERBOSITY_LEVEL_NORMAL 3
//#define PARLIB_VERBOSITY_LEVEL_FULL   4
//#define PARLIB_VERBOSITY_LEVEL_DEBUG  5

//this is the structure that holds a single parameter definition
typedef struct {
  char      name[STR_MAX];         //name that the parameter referenced with
  char      name_alt[STR_MAX];     //alternative name that the param is referenced with
  int       max_dim;                      //max dimension of the input array, = 1 for a single value
  int       *pIval;                       //pointers to the parameter values
  long      *pLval;                       //
  double    *pDval;                       //
  float     *pFval;                       //
  char      *pCval;                       //
  BOOL      *pBval;                       //
  unsigned  argtype;                      //data type of variable
  BOOL      compulsory;                   //TRUE if parameter is absolutely required
  BOOL      supplied;                     //will be set to TRUE if param given on command line
  int       dim_supplied;                 //will be set to the number of dimensions supplied on the command line
  char      comment[STR_MAX];             //the help comment
  char      str_value[LSTR_MAX];          //stores the full string of the argument  -- debug use mainly
  int       supplied_from;
} param_struct;

//this holds the full list of parameters
typedef struct {
  int num_params;
  param_struct *pParam;
  //  BOOL clobber;
  //  BOOL debug;
  //  int verbosity;
  int longest_par_name_length;
} param_list_struct;




//----------------------------------------------------------------------------------------------
//-- public function declarations
int   ParLib_str_to_BOOL                    (const char *str, BOOL *pB);
int   ParLib_init_param_list                (param_list_struct **pParamList);
int   ParLib_free_param_list                (param_list_struct **ppParamList);
//int   ParLib_tidy_param_list                (param_list_struct *pParamList);
int   ParLib_add_param_to_list              (param_list_struct *pParamList, const char *name, int argtype, int max_dim,
                                             void *pValue, BOOL compulsory, const char *name_alt, const char *comment);
int   ParLib_read_param_from_string         (param_struct *pP, const char *str) ;
int   ParLib_read_params_from_string        (param_list_struct *pParamList, const char* str, int *pWhichParamIndex) ;
int   ParLib_read_param_from_command_line   (param_struct *pP, int argc, char *argv[]);
int   ParLib_read_params_from_command_line  (param_list_struct *pParamList, int argc, char *argv[]) ;
int   ParLib_read_params_from_file          (param_list_struct *pParamList, char *str_filename ) ;
int   ParLib_read_param_from_file           (param_struct *pP, char *str_filename ) ;
int   ParLib_check_param_list               (param_list_struct *pParamList);
int   ParLib_print_param_prefix             (FILE *pfile, param_list_struct *pParamList, param_struct *pP, const char *str_prefix);
int   ParLib_print_param_list_prefix        (FILE *pfile, param_list_struct *pParamList, const char *str_prefix);
int   ParLib_print_param_list               (FILE *pfile, param_list_struct *pParamList);
int   ParLib_print_param_list_to_file       (const char* str_filename, param_list_struct *pParamList);
int   ParLib_isnumeric                      (char c);
char* sstrncpy                              (char *dest, const char *src, size_t n);
param_struct* ParLib_get_param_by_name      (param_list_struct* pParamList, const char *param_name);
int   ParLib_get_dim_supplied               (param_list_struct *pParamList, const char *param_name);
int   ParLib_get_supplied_flag              (param_list_struct *pParamList, const char *param_name);
int   ParLib_set_default_value              (param_list_struct *pParamList, const char *param_name, const char* param_value );
int   ParLib_setup_param                    (param_list_struct *pParamList, const char *name, int argtype, int max_dim,
                                             void *pValue, const char *default_value, BOOL compulsory, const char *name_alt, const char *comment);
int   ParLib_print_version                  (FILE *pFile);
int   ParLib_print_example_params_file      (FILE *pfile, param_list_struct *pParamList);

int   ParLib_write_param_html_table_entry   (FILE *pFile, param_list_struct *pParamList, const char *param_name,  const char* str_style, const char* str_extra) ;
int   ParLib_get_param_str_value            (param_list_struct *pParamList, const char *param_name, char* str_result);

#endif
