//this will implement the quad tree functions
//see also quadtree.h

#define CVS_REVISION "$Revision: 1.1 $"
#define CVS_DATE     "$Date: 2014/01/17 10:06:35 $"


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "define.h"
#include "quadtree_lib.h"

#define COMMENT_PREFIX "#-quadtree_lib Comment: "
#define WARNING_PREFIX "#-quadtree_lib Warning: "
#define ERROR_PREFIX   "#-quadtree_lib Error:   "
#define DEBUG_PREFIX   "#-quadtree_lib Debug:   "
#define DATA_PREFIX    ""

int quad_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  return 0;
}

int quad_init_root( /*@null@*/ quad_struct *pQ, double x_min, double x_max, double y_min, double y_max )
{
  int i;
  if ( pQ == NULL )
  {
    //print error
    return 1; 
  }
  pQ->x_min = x_min;
  pQ->x_max = x_max;
  pQ->y_min = y_min;
  pQ->y_max = y_max;
  pQ->x_mid = 0.5*(pQ->x_min + pQ->x_max);
  pQ->y_mid = 0.5*(pQ->y_min + pQ->y_max);
  pQ->pParent = NULL;
  pQ->pNextPeer = NULL;
  for ( i=0;i<QUADTREE_NCHILD;i++) 
  {
    pQ->pChild[i] = NULL;
  }
  pQ->nChild = 0;
  //  pQ->is_root = TRUE;
  //  pQ->is_tip  = TRUE; //because it has no children

  for ( i=0;i<QUADTREE_MAX_MEMBERS;i++) pQ->pMembers[i] = (void*) NULL;
  for ( i=0;i<QUADTREE_NNEIGHBOURS;i++) pQ->pNeighbours[i] = (quad_struct*) NULL;
  pQ->neighbours_mask = (unsigned int) 0x0;
  pQ->nMembers = 0;
  pQ->depth = 0;
  return 0;
}




//////////////////////////////////////////
//create and initialise a child quad
//calculate coords from parent.
int quad_add_child(/*@null@*/ quad_struct *pQ, int index )
{
  int i;
  if ( pQ == NULL )
  {
    //print error
    return 1; 
  }
  if ( index<0 || index >= QUADTREE_NCHILD)
  {
    return 2;
  }
  if ( pQ->pChild[index] != NULL)
  {
    return 3;
  }
  
  if ( (pQ->pChild[index] = (quad_struct*) (malloc(sizeof(quad_struct)))) == NULL )
  {
    return 4;
  }

  {
    //make an abbreviation
    quad_struct *pQChild = (quad_struct*) pQ->pChild[index];
    
    pQChild->pParent = (quad_struct*) pQ;
    switch(index)
    {
    case QUADTREE_CHILD_BOTTOM_LEFT :
      pQChild->x_min = pQ->x_min;
      pQChild->x_max = pQ->x_mid;
      pQChild->y_min = pQ->y_min;
      pQChild->y_max = pQ->y_mid;
      break;
    case QUADTREE_CHILD_BOTTOM_RIGHT :
      pQChild->x_min = pQ->x_mid;
      pQChild->x_max = pQ->x_max;
      pQChild->y_min = pQ->y_min ;
      pQChild->y_max = pQ->y_mid;
      break;
    case QUADTREE_CHILD_TOP_LEFT :
      pQChild->x_min = pQ->x_min;
      pQChild->x_max = pQ->x_mid;
      pQChild->y_min = pQ->y_mid;
      pQChild->y_max = pQ->y_max ;
      break;
    case QUADTREE_CHILD_TOP_RIGHT :
      pQChild->x_min = pQ->x_mid;
      pQChild->x_max = pQ->x_max;
      pQChild->y_min = pQ->y_mid;
      pQChild->y_max = pQ->y_max ;
      break;
    default :
      return 5;
      //    break;
    }
    
    pQChild->x_mid = 0.5*(pQChild->x_min + pQChild->x_max);
    pQChild->y_mid = 0.5*(pQChild->y_min + pQChild->y_max);
    
    for ( i=0;i<QUADTREE_NCHILD;i++) 
    {
      pQChild->pChild[i] = NULL;
    }
    pQChild->nChild = 0;
    pQChild->pNextPeer = NULL;
    //  pQ->is_root = TRUE;
    //  pQ->is_tip  = TRUE; //because it has no children
    for ( i=0;i<QUADTREE_MAX_MEMBERS;i++) pQ->pMembers[i] = (void*) NULL;
    pQChild->nMembers = 0;
    pQChild->depth = pQ->depth + 1;
    for ( i=0;i<QUADTREE_NNEIGHBOURS;i++) pQ->pNeighbours[i] = (quad_struct*) NULL;
    pQ->neighbours_mask = (unsigned int) 0x0;
    
    pQ->nChild ++;
  }
  return 0;
}

//add four children to a quad
//assume that the pQ has been initialised
//but check that has no children already
int quad_add_children(/*@null@*/ quad_struct *pQ )
{
  int i;
  if ( pQ == NULL )
  {
    //print error
    return 1; 
  }
  //this is not acceptible
  if ( pQ->depth >= QUADTREE_MAX_DEPTH )
  {
    //print error
    return 2;
  }
  for ( i=0;i<QUADTREE_NCHILD;i++)
  {
    if (quad_add_child(pQ, i) != 0 )
    {
      return 1;
    }
  }
  return 0;
}

//recursively add child quads down to the given depth 
int quad_add_children_to_depth(/*@null@*/ quad_struct *pQ, int depth )
{
  int i;
  if ( pQ == NULL )
  {
    //print error
    return 1; 
  }
  //this is a strange but acceptible thing to do
  if ( depth == 0 ) return 0; 
  //this is not acceptible
  if ( depth < 0 || depth > QUADTREE_MAX_DEPTH )
  {
    //print error
    return 2; 
  }
  //this is not acceptible
  if ( pQ->depth >= QUADTREE_MAX_DEPTH )
  {
    //print error
    return 3;
  }
  
  //add the four quads
  if ( quad_add_children(pQ) != 0 ) return 4;
  //now add quads below each of those...and recursively
  for(i=0;i<QUADTREE_NCHILD;i++)
  {
    if ( quad_add_children_to_depth(pQ->pChild[i], depth -1 ) != 0 ) return 5;
  } 
  
  return 0;
}
  

//search the quad tree structure for the smallest quad that
//contains the position assume that each quad has exactly 4 children,
//otherwise it must be at the lowest level
/*@null@*/ quad_struct* quad_get_quad_from_position ( /*@null@*/ quad_struct *pQ, double x, double y)
{
  quad_struct *pq = NULL;
  if ( pQ == NULL )
  {
    //print error
    return NULL; 
  }

  //do an initial check to see if position is inside the supplied quad
  if ( x < pQ->x_min  ||
       x > pQ->x_max  ||
       y < pQ->y_min  ||
       y > pQ->y_max  )
  {
    return NULL;
  }
  
  pq = (quad_struct*) pQ;
  while (pq->nChild == QUADTREE_NCHILD )
  {
    //treat the midpoint as inclusive for the top and right hand side quadrants
    if ( x < pq->x_mid  )
    {
      if ( y < pq->y_mid  ) pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_BOTTOM_LEFT];
      else                  pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_TOP_LEFT];
    }
    else
    {
      if ( y < pq->y_mid  ) pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_BOTTOM_RIGHT];
      else                  pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_TOP_RIGHT];
    }
    if ( pq == NULL ) break;
  }
  return  (quad_struct*) pq;
}

/////////////////////////////////////////////////////////////////////////////////////////////
//search the quad tree structure for the quad at depth 'depth' that
//contains the position 
/*@null@*/ quad_struct* quad_get_quad_from_position_and_depth (/*@null@*/ quad_struct *pQ, double x, double y, int depth)
{
  quad_struct *pq = NULL;
  if ( pQ == NULL )
  {
    //print error
    return NULL; 
  }
  if ( pQ->depth > depth ) return NULL;
  
  //do an initial check to see if position is inside the supplied quad
  if ( x < pQ->x_min  ||
       x > pQ->x_max  ||
       y < pQ->y_min  ||
       y > pQ->y_max  )
  {
    return NULL;
  }
  
  pq = (quad_struct*) pQ;
  while (pq->nChild == QUADTREE_NCHILD)
  {
    //treat the midpoint as inclusive for the top and right hand side quadrants
    if ( x < pq->x_mid  )
    {
      if ( y < pq->y_mid  ) pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_BOTTOM_LEFT];
      else                  pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_TOP_LEFT];
    }
    else
    {
      if ( y < pq->y_mid  ) pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_BOTTOM_RIGHT];
      else                  pq = (quad_struct*) pq->pChild[QUADTREE_CHILD_TOP_RIGHT];
    }
    if ( pq == NULL ) return NULL;
    if ( pq->depth == depth ) break; 
  }
  return  (quad_struct*) pq;
}


///we find the smallest quad that holds the position x,y
//
int quad_assign_object_to_quad (/*@null@*/ quad_struct *pQ, double x, double y, void *pObject)
{
  quad_struct *pq = NULL;
  if ( pQ == NULL ) return 1;
  if ( (pq = (quad_struct*) quad_get_quad_from_position ((quad_struct*) pQ, x, y)) == NULL ) 
  {
    return 1;
  }
  if ( pq->nMembers >=  QUADTREE_MAX_MEMBERS )
  {
    return 2;
  }
  pq->pMembers[pq->nMembers] = (void*) pObject;
  pq->nMembers++;
  
  return 0; 
}




//recursively free all child quads below the given quad  
int quad_free_quad(/*@null@*/ quad_struct *pQ )
{
  int i;
  if ( pQ == NULL )
  {
    //print error
    return 1; 
  }

  if ( pQ->nChild > 0 )
  {
    //now free quads below each of those...and recursively
    for(i=0;i<pQ->nChild;i++)
    {
      if ( pQ->pChild[i] )
      {
        if (quad_free_quad(pQ->pChild[i]) != 0 )
          return 2;
        pQ->pChild[i] =  NULL;
      }
    }
    pQ->nChild = 0;
  }
  return 0;
}

//

////this is a function that applies the given function to all
////tip quads in the tree (tip quads have no children)
////is recursive
//int quad_apply_function_to_tip_quads(quad_struct *pQ, void(*func)(int*,int*), int *parg1)
//{
//  //  int i;
//  if ( pQ == NULL )
//  {
//    //print error
//    return 1; 
//  }
//
//  if ( pQ->nChild > 0 )
//  {
////todo
//    //this quad has children so is not at the tip
////    for(i=0;i<pQ->nChild;i++)
////    {
////      if ( pQ->pChild[i] )
////      {
////        if (quad_free(pQ->pChild[i]))
////          return 2;
////        pQ->pChild[i] =  NULL;
////      }
////    }
////    pQ->nChild = 0;
//  }
//  return 0;  
//}

// funtion that returns a pointer to the indexed quad at the given depth level
// quads at a given depth are numbered from
// index=0            is at pQ->pChild[0]->pChild[0]-> ... ->pChild[0]
// index=1            is at pQ->pChild[0]->pChild[0]-> ... ->pChild[1]
// ...
// index=(4^depth -1) is at pQ->pChild[3]->pChild[3]-> ... ->pChild[3]
// there are 4^N quads at depth N
//
/*@null@*/ quad_struct* quad_get_quad_from_depth_index(/*@null@*/ quad_struct *pQ, int depth, int index )
{
  quad_struct *pq;
  int rank;
  int divisor = 1;
  int i;
  if ( pQ == NULL )
  {
    //print error
    return NULL; 
  }
  if ( depth == 0 )
  {
    if (index == 0) return pQ;
    else            return NULL;
  }
    
  for(i=0;i<depth;i++) divisor *= QUADTREE_NCHILD;
  //
  pq = pQ;
  i = index;
  while (pq->nChild == QUADTREE_NCHILD && divisor >= 1 )
  {
    divisor /= QUADTREE_NCHILD;
    rank = i / divisor; //integer division
    pq = pq->pChild[rank];
    i -= (rank * divisor); //set i to be the remainder
    if ( pq == NULL ) return NULL;
  }
  return pq;
}


// set up the linked list between peers
// start at pQ->pChild[0]->pChild[0]-> ... ->pChild[0]
// next one pQ->pChild[0]->pChild[0]-> ... ->pChild[1]
// ...
// end   at pQ->pChild[3]->pChild[3]-> ... ->pChild[3]


int quad_print_quad (/*@null@*/ FILE* pFile, /*@null@*/ quad_struct *pQ)
{
  int i;
  if ( pQ == NULL )
  {
    //print error
    return 1; 
  }
  if ( pFile == NULL )
  {
    //print error
    return 2; 
  }
  //indent the quad print outputs by two columns per level
  for(i=0;i<pQ->depth;i++) fprintf(pFile, "  " );
  fprintf(pFile, "0x%08lx depth= %2d nChild= %d nMembers= %2d centre= %9.5g %9.5g [%9.5g:%9.5g][%9.5g:%9.5g] area= %.4g\n",
          (unsigned long) (void*) pQ,
          pQ->depth, pQ->nChild, pQ->nMembers,
          pQ->x_mid, pQ->y_mid,
          pQ->x_min, pQ->x_max,
          pQ->y_min, pQ->y_max,
          (pQ->x_max-pQ->x_min)*(pQ->y_max-pQ->y_min)); 
  return 0;
}

int quad_add_unique_member_to_quad(/*@null@*/ quad_struct *pQ, void *pVoid )
{
  int i;
  if ( pQ    == NULL ||
       pVoid == NULL )
  {
    //print error
    return 1; 
  }
  for ( i=0; i< pQ->nMembers; i++)
  {
    if ( ((void*) pQ->pMembers[i]) == ((void*) pVoid )) return 0; //already a member
  }

  if ( pQ->nMembers >= QUADTREE_MAX_MEMBERS )
  {
    //this is bad
    fprintf(stderr, "%s Error adding member to Quadtree structure: too many members\n", ERROR_PREFIX);
    (void) fflush(stderr);
    return 1;
  }

  pQ->pMembers[pQ->nMembers] = (void*) pVoid;
  pQ->nMembers++;
  return 0;
}



int quad_write_vertices_file (/*@null@*/ quad_struct *pQ, const char *str_filename)
{
  FILE* pquad_vertices_file = NULL;
  int quad_nleaves = 1;
  int i,j;
  if ( pQ == NULL ) return 1;
  for (i=0;i<pQ->depth;i++) quad_nleaves *= QUADTREE_NCHILD;
  
  if ((pquad_vertices_file = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the quad_vertices file: %s\n",
            ERROR_PREFIX, str_filename);
    (void) fflush(stderr);
    return 1;
  }
  
  for ( j = 0; j < quad_nleaves; j ++ )
  {
    quad_struct *pq = (quad_struct*) quad_get_quad_from_depth_index((quad_struct*) pQ, pQ->depth, j);
    if ( pq == NULL ) continue;
    fprintf( pquad_vertices_file, "# Quadtree %5d centre= %12.7f %12.7f depth=%d nMembers=%d\n",
             j, pq->x_mid, pq->y_mid, pQ->depth, pq->nMembers);
    fprintf( pquad_vertices_file, "%12.7f %12.7f\n", pq->x_min, pq->y_min);
    fprintf( pquad_vertices_file, "%12.7f %12.7f\n", pq->x_min, pq->y_max);
    fprintf( pquad_vertices_file, "%12.7f %12.7f\n", pq->x_max, pq->y_max);
    fprintf( pquad_vertices_file, "%12.7f %12.7f\n", pq->x_max, pq->y_min);
    //print the first vertex again
    fprintf( pquad_vertices_file, "%12.7f %12.7f\n", pq->x_min, pq->y_min);
    fprintf( pquad_vertices_file, "\n");
  }
  fprintf(stdout, "%s I wrote out the quadtree vertices to %s\n", COMMENT_PREFIX, str_filename);   
  if (pquad_vertices_file) fclose(pquad_vertices_file);
  pquad_vertices_file = NULL;
  
  return 0;
}
////////////////////////////////////////////////////////////////////////

//recursively calculate all neighbours below the current quad  
int quad_calc_neighbours_local(/*@null@*/ quad_struct *pQ, quad_struct *pQroot )
{
  int i;
  double x, y;
  double dx, dy;
  unsigned int mask;
  if ( pQ == NULL || pQroot == NULL )
  {
    fprintf(stderr, "%s Error setting up Quadtree neighbours (pQ=0x%08lx pQroot=0x%08lx) at file %s line %d\n",
            ERROR_PREFIX, (unsigned long)pQ, (unsigned long)pQroot, __FILE__, __LINE__);
    (void) fflush(stderr);
    //print error
    return 1; 
  }
  dx = pQ->x_max - pQ->x_min;
  dy = pQ->y_max - pQ->y_min;
  //north
  for(i=0;i<QUADTREE_NNEIGHBOURS;i++)
  {
   quad_struct *pq = NULL;
   switch ( (int) i )
    {
    case (int) QUADTREE_NEIGHBOUR_NN :
      x = pQ->x_mid     ;
      y = pQ->y_mid + dy;
      mask = QUADTREE_NEIGHBOUR_MASK_NN;
      break;
    case QUADTREE_NEIGHBOUR_NE : x = pQ->x_mid + dx; y = pQ->y_mid + dy; mask = QUADTREE_NEIGHBOUR_MASK_NE; break;
    case QUADTREE_NEIGHBOUR_EE : x = pQ->x_mid + dx; y = pQ->y_mid     ; mask = QUADTREE_NEIGHBOUR_MASK_EE; break;
    case QUADTREE_NEIGHBOUR_SE : x = pQ->x_mid + dx; y = pQ->y_mid - dy; mask = QUADTREE_NEIGHBOUR_MASK_SE; break;
    case QUADTREE_NEIGHBOUR_SS : x = pQ->x_mid     ; y = pQ->y_mid - dy; mask = QUADTREE_NEIGHBOUR_MASK_SS; break;
    case QUADTREE_NEIGHBOUR_SW : x = pQ->x_mid - dx; y = pQ->y_mid - dy; mask = QUADTREE_NEIGHBOUR_MASK_SW; break;
    case QUADTREE_NEIGHBOUR_WW : x = pQ->x_mid - dx; y = pQ->y_mid     ; mask = QUADTREE_NEIGHBOUR_MASK_WW; break;
    case QUADTREE_NEIGHBOUR_NW : x = pQ->x_mid - dx; y = pQ->y_mid + dy; mask = QUADTREE_NEIGHBOUR_MASK_NW; break;
    default :
      fprintf(stderr, "%s Error setting up Quadtree neighbours (i=%d) at file %s line %d\n",
              ERROR_PREFIX, i, __FILE__, __LINE__);
      (void) fflush(stderr);
      return 1;
    }
    pq = (quad_struct*) quad_get_quad_from_position_and_depth  ((quad_struct*) pQroot, (double) x, (double) y, pQ->depth);

    //    if ( pq ) fprintf(stdout, "Found neighbour %d at x,y= %7.2f %7.2f for 0x%08lx (depth=%d [%7.2f:%7.2f][%7.2f:%7.2f]) ==>  0x%08lx (depth=%d [%7.2f:%7.2f][%7.2f:%7.2f]) \n",
    //                      i,  x, y,
    //                      (unsigned long) pQ, pQ->depth, pQ->x_min, pQ->x_max, pQ->y_min, pQ->y_max,
    //                      (unsigned long) pq, pq->depth, pq->x_min, pq->x_max, pq->y_min, pq->y_max); fflush(stdout) ;

    if ( (quad_struct*) pq == (quad_struct*) pQ )
    {
      fprintf(stderr, "%s Error setting up Quadtree neighbour: self reference: i=%d depth=%d x,y= %10.5f %10.5f\n",
              ERROR_PREFIX, i, pQ->depth, x, y);
      (void) fflush(stderr);
      //      pq = NULL; //safety check
    }
    else
    {
      if ( pq == NULL ) pQ->pNeighbours[i] = NULL;
      else
      {
        pQ->neighbours_mask = (unsigned int) (pQ->neighbours_mask | (unsigned int) mask);
        pQ->pNeighbours[i] = (quad_struct*) pq;
      }
    }
  }

  //now calculate
  //the neighbours of all child quads ... easy cos recursive!
  for(i=0;i<pQ->nChild;i++)
  {
    if ( pQ->pChild[i] != NULL )
    {
      if (quad_calc_neighbours_local((quad_struct*) pQ->pChild[i], (quad_struct*) pQroot) != 0)
      {
        fprintf(stderr, "%s Error setting up Quadtree neighbours (pQ=0x%08lx pQroot=0x%08lx) at file %s line %d\n",
                ERROR_PREFIX, (unsigned long)pQ, (unsigned long)pQroot, __FILE__, __LINE__);
        (void) fflush(stderr);
        return 2;
      }
    }
  }
  return 0;
}

int quad_calc_neighbours(/*@null@*/ quad_struct *pQ)
{
  if ( pQ == NULL ) return 1;
  return (int) quad_calc_neighbours_local((quad_struct*) pQ, (quad_struct*) pQ );
}
