//----------------------------------------------------------------------------------
//--
//--    Filename: fits_helper_lib.h
//--    Use: This is a header for the fits_helper_lib.c C module
//--
//--    Notes:
//--

#ifndef FITS_HELPER_H
#define FITS_HELPER_H

//#define CVS_REVISION_FITS_HELPER_LIB_H "$Revision: 1.7 $"
//#define CVS_DATE_FITS_HELPER_LIB_H     "$Date: 2015/08/18 11:42:08 $"

#include <stdio.h>
#include <string.h>
#include <fitsio.h>

#include "define.h"

//-----------------Info for printing code revision-----------------//
#define CVS_REVISION_H "$Revision: 1.7 $"
#define CVS_DATE_H     "$Date: 2015/08/18 11:42:08 $"
inline static int fhelp_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
#undef CVS_REVISION_H
#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

////////////////////////////////////
#ifndef STR_MAX
#define STR_MAX DEF_STRLEN
#endif

#ifndef LSTR_MAX
#define LSTR_MAX DEF_LONGSTRLEN
#endif
////////////////////////////////////

#define FHELP_FILE_FORMAT_CODE_ERROR         -1
#define FHELP_FILE_FORMAT_CODE_FITS           0
#define FHELP_FILE_FORMAT_CODE_NOT_FITS       1
#define FHELP_FILE_FORMAT_CODE_NOT_CORRECT_FITS_TYPE 2


//functions

int fhelp_print_version             (FILE* pFile);
int fhelp_test_if_file_is_fits      (const char* str_filename);
int fhelp_test_if_file_is_fits_table(const char* str_filename);
int fhelp_test_if_file_is_fits_image(const char* str_filename);



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// fits_table_helper_struct routines
//TODO make this fully dynamic 
#ifndef FITS_TABLE_HELPER_MAX_COLS
#define FITS_TABLE_HELPER_MAX_COLS 512
#endif

//struct used when setting up output fits tables
typedef struct {
  fitsfile *pFile;
  char str_filename[LSTR_MAX+1];
  char str_extname[STR_MAX];
  
  char ttype[FITS_TABLE_HELPER_MAX_COLS][FLEN_CARD];   //TODO , malloc space for these at run time
  char tform[FITS_TABLE_HELPER_MAX_COLS][FLEN_CARD]; 
  char tunit[FITS_TABLE_HELPER_MAX_COLS][FLEN_CARD];
  char tcomm[FITS_TABLE_HELPER_MAX_COLS][FLEN_CARD];
  char *pttype[FITS_TABLE_HELPER_MAX_COLS]; //pointers to the above
  char *ptform[FITS_TABLE_HELPER_MAX_COLS]; 
  char *ptunit[FITS_TABLE_HELPER_MAX_COLS];
  int   colfmt[FITS_TABLE_HELPER_MAX_COLS];
  int   colnum[FITS_TABLE_HELPER_MAX_COLS];
  LONGLONG colwidth[FITS_TABLE_HELPER_MAX_COLS];
  void *pvoid[FITS_TABLE_HELPER_MAX_COLS];
  int  ncols;
  LONGLONG current_row;
} fits_table_helper_struct;



int fths_init       (fits_table_helper_struct *pTable);
int fths_addcol     (fits_table_helper_struct *pTable,
                     const char* ttype,
                     const char* tform,
                     const int   colfmt,
                     const LONGLONG colwidth,
                     const char* tunit,
                     const char* tcomm);
int fths_create     (fits_table_helper_struct *pTable,
                     char* str_filename,
                     char* str_extname,
                     LONGLONG num_rows,
                     BOOL clobber);
int fths_write_row  (fits_table_helper_struct *pTable,
                     BOOL increment);
int fths_close      (fits_table_helper_struct *pTable);


#endif
