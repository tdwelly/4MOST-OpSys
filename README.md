## 4MOST-OpSys - code for the Operations System of the 4MOST project

## Scope:

This repository contains the source code for the Operations System (OpSys) 
of the 4MOST project.

## Legal:

Copyright (2011-2018) 4MOST OpSys Team, MPE, Garching, Germany 

> 4MOST-OpSys is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.

> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.

> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Content:
The 4MOST-OpSys is a collection of software tools (most) implemented in C.
Build scripts are supplied that allow compilation of the source code on 
UNIX-like systems.

## Contact:

Please send comments, complaints and suggestions to 4most 'at' mpe.mpg.de    

