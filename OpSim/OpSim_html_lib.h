//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_html_lib.h
//--    Use: This is a header for the OpSim_html_lib.c C code file
//--
//--    Notes: 
//--

#ifndef OPSIM_HTML_H
#define OPSIM_HTML_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <unistd.h>

#include "define.h"
#include "4FS_OpSim.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.2 $"
//#define CVS_DATE_H     "$Date: 2015/08/18 11:44:15 $"
inline static int OpSim_html_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

int OpSim_html_print_version      (FILE* pFile);

int OpSim_html_write_summary_page (survey_struct *pSurvey,
                                   focalplane_struct *pFocal,
                                   param_list_struct *pParamList,
                                   cataloguelist_struct *pCatList,
                                   const char* str_filename);

#endif
