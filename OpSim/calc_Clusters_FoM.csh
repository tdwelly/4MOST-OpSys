#!/bin/tcsh -f

if ( $#argv == 0 ) then
  echo "Usage: calc_Clusters_FoM.csh DIR1 [DIR2] [DIR3] ..."
  exit 1
endif

if (! -e /home/tdwelly/4most/Clusters_in/Clusters_List.txt ) then
  gawk '$1!~/^#/ {cl=sprintf("%sz%6.4f",substr($1,1,11),$12);n[cl]++} END {for(cl in n) {printf("%-18s %6d\n", cl,n[cl])}}' /home/tdwelly/4most/Clusters_in/Clusters_cat_current.txt > /home/tdwelly/4most/Clusters_in/Clusters_List.txt
endif
set UNIQUE_CLUSTERS0 = `wc -l /home/tdwelly/4most/Clusters_in/Clusters_List.txt | gawk '//{print $1}'`

set FLAG = 1
set ODIR = `pwd`
foreach DIR ( $argv ) 
  cd $ODIR
  cd $DIR
  set PDIR = `pwd`

  if ( -e Clusters_FoM.txt ) mv Clusters_FoM.txt Clusters_FoM.txt~
  echo "\
############ Calculation of Clusters FoM #####################################################\
# z_min     = minimum limit of redshift bin                                                  #\
# z_max     = maximum limit of redshift bin                                                  #\
# z_mid     = arithmetic midpoint of redshift bin                                            #\
# N_clus    = number of unique clusters in the footprint of the survey                       #\
# N_gal     = number of cluster galaxies in the footprint of the survey                      #\
# N_obs     = number of cluster galaxies that were allocated fibers in the survey            #\
# f_obs     = fraction of cluster galaxies that were allocated fibers in the survey          #\
# N>=1obs   = number of unique clusters with   >=1  galaxies allocated fibers in the survey  #\
# f>=1obs   = fraction of unique clusters with >=1  galaxies allocated fibers in the survey  #\
# N>=4gal   = number of unique clusters with   >=4  members in the survey                    #\
# N>=4obs   = number of unique clusters with   >=4  galaxies allocated fibers in the survey  #\
# f>=4obs   = fraction of unique clusters with >=4  galaxies allocated fibers in the survey  #\
# N>=10gal  = number of unique clusters with   >=10 members in the survey                    #\
# N>=10obs  = number of unique clusters with   >=10 galaxies allocated fibers in the survey  #\
# f>=10obs  = fraction of unique clusters with >=10 galaxies allocated fibers in the survey  #\
# W/clus    = weight per successfully observed cluster                                       #\
# deltaFoM  = contribution to the total FoM from this redshift bin = (N>=4obs * W/clus)      #\
# Final FoM = sum_z[deltaFoM(z)]                                                             #\
# Note: Cluster fractions are relative to total no. clusters in redshift interval (N_clus)   #\
##############################################################################################" > Clusters_FoM.txt 
  if ( $FLAG == 1 ) cat Clusters_FoM.txt 
  set FLAG = 0


  if ( -e Clusters_List.txt ) rm Clusters_List.txt
  if ( -e assigned_objects.txt ) then
    echo "# Working on sim $DIR in $PDIR" | tee Clusters_FoM.txt 
    gawk 'BEGIN {last14 = "-"} $1!~/^#/ && $12 == 5 {if($14!=last14 && last14 !="-") {n[cl]++;if(ntiles>0){nobs[cl]++};ntiles=0;}; last14=$14; cl=sprintf("%sz%6.4f",substr($13,1,11),$24);ntiles+=$5;z[cl]=$24;} END {if(cl!=""){n[cl]++;if(ntiles>0){nobs[cl]++};}; for(cl in n) {printf("%-18s %6d %6d %6s\n", cl,n[cl],nobs[cl],z[cl])}} ' assigned_objects.txt > Clusters_List.txt
    echo "# There were "`wc -l Clusters_List.txt | gawk '//{print $1}'`" unique clusters in survey footprint (cf $UNIQUE_CLUSTERS0 in full input Clusters catalogue)" |& tee -a Clusters_FoM.txt 


#    echo "# Results for $DIR" | tee Clusters_FoM.txt 
#    gawk -v dz=0.2 -v sinvweight="300000.0 200000.0 60000.0 12000.0 2500.0 700.0" 'BEGIN{nw=split(sinvweight,invweight," "); printf("#%-5s %6s %6s %9s %9s %9s %9s %9s %10s %10s\n", "z_min","z_max","z_mid","Nclus","N(>4obs)","f(>4obs)","N(>10obs)","f(>10obs)","W/clus","deltaFoM")} //{iz=int($4/dz); nz[iz]++; if($3>=4){n4z[iz]++};if($3>=10){n10z[iz]++};} END {fom=0; for (iz=0;iz<=int(1.2/dz);iz++) {dfom=n4z[iz]/(0.0+invweight[iz+1]); fom+=dfom; printf("%6.1f %6.1f %6.1f %9d %9d %9.4f %9d %9.4f %10.3e %10.4f\n", iz*dz, (iz+1)*dz, (iz+0.5)*dz, nz[iz], n4z[iz], n4z[iz]/nz[iz], n10z[iz], n10z[iz]/nz[iz], 1./invweight[iz+1], dfom)}; printf("Final FoM = %8.4f\n", fom)} ' Clusters_List.txt |& tee -a Clusters_FoM.txt 
    gawk -v dz=0.2 -v sinvweight="300000.0 200000.0 60000.0 12000.0 2500.0 700.0" 'BEGIN{nw=split(sinvweight,invweight," "); printf("#%-5s %6s %6s %6s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %10s %8s\n", "z_min","z_max","z_mid","N_clus","N_gal","N_obs","f_obs","N>=1obs","f>=1obs","N>=4gal","N>=4obs","f>=4obs","N>=10gal","N>=10obs","f>=10obs","W/clus","deltaFoM")} //{iz=int($4/dz); nz[iz]++;gz[iz]+=$2; if($2>=4){g4z[iz]++};if($2>=10){g10z[iz]++}; nobsz[iz]+=$3;if($3>=1){n1z[iz]++};if($3>=4){n4z[iz]++};if($3>=10){n10z[iz]++};} END {fom=0; for (iz=0;iz<=int(1.2/dz);iz++) {dfom=n4z[iz]/(0.0+invweight[iz+1]); fom+=dfom; printf("%6.1f %6.1f %6.1f %6d %8d %8d %8.4f %8d %8.4f %8d %8d %8.4f %8d %8d %8.4f %10.3e %8.4f\n", iz*dz, (iz+1)*dz, (iz+0.5)*dz, nz[iz],gz[iz],nobsz[iz],nobsz[iz]/gz[iz], n1z[iz], n1z[iz]/nz[iz], g4z[iz], n4z[iz], n4z[iz]/nz[iz], g10z[iz], n10z[iz], n10z[iz]/nz[iz], 1./invweight[iz+1], dfom)}; printf("Final FoM = %8.4f\n", fom)} ' Clusters_List.txt |& tee -a Clusters_FoM.txt 
  else
    echo "# Skipping on $DIR in $PDIR"
  endif


end





exit


