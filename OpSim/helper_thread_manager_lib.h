//----------------------------------------------------------------------------------
//--
//--    Filename: helper_thread_manager_lib.h
//--    Use: This is a header for the helper_thread_manager_lib.c C program
//--
//--    Notes:
//--

#ifndef HELPER_THREAD_MANAGER_H
#define HELPER_THREAD_MANAGER_H

//#define CVS_REVISION_HELPER_THREAD_MANAGER_LIB_H "$Revision: 1.1 $"
//#define CVS_DATE_HELPER_THREAD_MANAGER_LIB_H     "$Date: 2013/02/08 16:03:52 $"

#include <stdio.h>

#include "define.h"

#define STR_MAX DEF_STRLEN

#define HELPER_THREAD_MANAGER_MAX_TASKS 1024



//----------------------------------------------------------------------------------------------
//-- public structure definitions
typedef struct {
  int  return_value;
  BOOL is_running;
  char str_task[STR_MAX];   //I should really make this dynamic
} task_struct;

typedef struct {
  int max_threads;
  int active_calls;
  int pending_calls;
  FILE *pLogfile;
  char str_logfile[STR_MAX];
  
  int num_tasks;
  int last_task_index;
  task_struct pTask[HELPER_THREAD_MANAGER_MAX_TASKS];

  
  
} helper_thread_manager_struct;



//----------------------------------------------------------------------------------------------
//-- public function declarations
