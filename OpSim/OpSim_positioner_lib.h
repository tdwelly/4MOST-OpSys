//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_positioner_lib.h
//--    Use: This is a header for the OpSim_folistioner_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_POSITIONER_H
#define OPSIM_POSITIONER_H

#define CVS_REVISION_OPSIM_POSITIONER_LIB_H "$Revision: 1.38 $"
#define CVS_DATE_OPSIM_POSITIONER_LIB_H     "$Date: 2015/09/03 15:14:43 $"

#include <stdio.h>


#include "define.h"
#include "4FS_OpSim.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.38 $"
//#define CVS_DATE_H     "$Date: 2015/09/03 15:14:43 $"
inline static int OpSim_positioner_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

#define HEXAGON_VERTICES        6

#define FIBER_ASSIGN_CODE_NONE     0 
#define FIBER_ASSIGN_CODE_TARGET   1 
#define FIBER_ASSIGN_CODE_SKY      3 
#define FIBER_ASSIGN_CODE_PITARGET 4 

#define FIBER_STATUS_FLAG_NOMINAL        ((uint) 0x0000)   //everything is working nominally
#define FIBER_STATUS_FLAG_STUCK_DIR1     ((uint) 0x0001)   //flags relating to fiber movement
#define FIBER_STATUS_FLAG_STUCK_DIR2     ((uint) 0x0002)   //for Echidna  DIR1=x, DIR2=y
#define FIBER_STATUS_FLAG_BROKEN         ((uint) 0x0100)   //flags relating to fiber throughput
#define FIBER_STATUS_FLAG_LOW_THROUGHPUT ((uint) 0x0200)   
#define FIBER_STATUS_FLAG_STUCK          ((uint) ((FIBER_STATUS_FLAG_STUCK_DIR1) | (FIBER_STATUS_FLAG_STUCK_DIR2)))   
//#define FIBER_STATUS_FLAG_ORPHAN         ((uint) 0x1000)   //this is an positioner that is not connected to a spectrograph
//                                                           // but can be moved out of the way of its active neighbours

enum positioner_family {
  POSITIONER_FAMILY_UNKNOWN =0,
  POSITIONER_FAMILY_ECHIDNA   ,
  POSITIONER_FAMILY_LAMOST    ,
  POSITIONER_FAMILY_POSB      ,         
  POSITIONER_FAMILY_POTZPOZ   ,         
  POSITIONER_FAMILY_MUPOZ     ,
  POSITIONER_FAMILY_OLD_MUPOZ ,
  POSITIONER_FAMILY_STARBUG   ,
  POSITIONER_FAMILY_AESOP
};

#define POSITIONER_FAMILY_CODE_TO_STR(pfc) \
  ((pfc)== POSITIONER_FAMILY_ECHIDNA   ? "ECHIDNA"   : \
  ((pfc)== POSITIONER_FAMILY_LAMOST    ? "LAMOST"    : \
  ((pfc)== POSITIONER_FAMILY_POSB      ? "Pos-B"     : \
  ((pfc)== POSITIONER_FAMILY_POTZPOZ   ? "PotsPoz"   : \
  ((pfc)== POSITIONER_FAMILY_MUPOS     ? "MuPoz"     : \
  ((pfc)== POSITIONER_FAMILY_OLD_MUPOZ ? "Old MuPoz" : \
  ((pfc)== POSITIONER_FAMILY_STARBUG   ? "Starbug"   : \
  ((pfc)== POSITIONER_FAMILY_AESOP     ? "AESOP"     : \
   "??" ))))))))

#define NVERTEX_POTZPOZ 4
#define NVERTEX_MUPOZ   4
#define NVERTEX_ECHIDNA 4
#define NVERTEX_STARBUG 0
#define NVERTEX_AESOP   4


#define HIGH_LOW_PATTERN_REGULAR  1
#define HIGH_LOW_PATTERN_ONE_IN_N 2
#define HIGH_LOW_PATTERN_RANDOM   3
#define HIGH_LOW_PATTERN_ALL_LORES 4
#define HIGH_LOW_PATTERN_ALL_HIRES 5

//this is handy as a shortcut comparison

#define COLLISION_NTIPS 2
#define COLLISION_TIP1 0
#define COLLISION_TIP2 1

#define COLLISION_NBASES 2
#define COLLISION_BASE1 0
#define COLLISION_BASE2 1

enum collision_code {
  COLLISION_CODE_ERROR3   =-3,
  COLLISION_CODE_ERROR2   =-2,
  COLLISION_CODE_ERROR    =-1,
  COLLISION_CODE_NONE     = 0,
  COLLISION_CODE_TIP_TIP   ,
  COLLISION_CODE_TIP_EDGE  ,
  COLLISION_CODE_EDGE_EDGE ,
  COLLISION_CODE_BASE_TIP  ,
  COLLISION_CODE_BASE_EDGE ,
  COLLISION_CODE_BASE_BASE ,
  COLLISION_CODE_CROSSING  ,
  COLLISION_CODE_CYLINDER_ROOT = 100 
};

#define COLLISION_CODE_TO_STR(cc) \
  ((cc)==COLLISION_CODE_ERROR3    ? "!3": \
  ((cc)==COLLISION_CODE_ERROR2    ? "!2": \
  ((cc)==COLLISION_CODE_ERROR     ? "!!": \
  ((cc)==COLLISION_CODE_NONE      ? "..": \
  ((cc)==COLLISION_CODE_TIP_TIP   ? "TT": \
  ((cc)==COLLISION_CODE_TIP_EDGE  ? "TE": \
  ((cc)==COLLISION_CODE_EDGE_EDGE ? "EE": \
  ((cc)==COLLISION_CODE_BASE_TIP  ? "BT": \
  ((cc)==COLLISION_CODE_BASE_EDGE ? "BE": \
  ((cc)==COLLISION_CODE_BASE_BASE ? "BB": \
  ((cc)==COLLISION_CODE_CROSSING  ? "CR": \
   "??")))))))))))


#define POSITIONER_IS_NOT_AT_EDGE_OF_TILE 0
#define POSITIONER_IS_AT_BORDER_OF_TILE   1
#define POSITIONER_IS_AT_EDGE_OF_TILE     2
#define POSITIONER_IS_AT_CORNER_OF_TILE   3

#define FIBERS_GEOMETRY_STEM "fibers_geometry"
//#define FIBERS_GEOMETRY_FILENAME "fibers_geometry.txt"
//#define FIBERS_GEOMETRY_PLOTFILE "fibers_geometry.plot"
//#define FIBERS_GEOMETRY_PLOTNAME "fibers_geometry.pdf"
#define TILE_VERTICES_FILENAME   "tile_vertices_mm.txt"


////public functions
int    OpSim_positioner_print_version                          (FILE* pFile);
char*  OpSim_positioner_fiber_status_string                    (fiber_struct *pFib);
int    OpSim_positioner_copy_fiber_struct                      (fiber_struct *pFibCopy, const fiber_struct *pFibOrig);
int    OpSim_positioner_copy_fibergeom_struct                  (fibergeom_struct *pFibGeomCopy, const fibergeom_struct *pFibGeomOrig);
int    OpSim_positioner_init_fibergeom_struct                  (fibergeom_struct *pFibGeom);

int    OpSim_positioner_reset_fiber_assignment_status          (fiber_struct *pFib); 
int    OpSim_positioner_repair_faulty_fibers                   (focalplane_struct *pFocal, double time_now);
int    OpSim_positioner_repair_fiber                           (focalplane_struct *pFocal, fiber_struct *pFib, double time_now);
int    OpSim_positioner_reset_fiber                            (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_reset_fibers                           (focalplane_struct *pFocal); 
int    OpSim_positioner_init_fibers                            (focalplane_struct *pFocal); 

int    OpSim_positioner_set_potzpoz_fiber_to_position          (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
int    OpSim_positioner_set_mupoz_fiber_to_position            (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
int    OpSim_positioner_set_spine_fiber_to_position            (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
//int    OpSim_positioner_set_echidna_fiber_to_position          (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
//int    OpSim_positioner_set_aesop_fiber_to_position            (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
int    OpSim_positioner_set_starbug_fiber_to_position          (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
int    OpSim_positioner_set_fiber_to_position                  (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_set_fibergeom_to_position              (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y);
int    OpSim_positioner_set_fiber_to_null_position             (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_set_fibers_to_null_position            (focalplane_struct *pFocal); 


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
int    OpSim_positioner_test_collision_tip_vs_tip              (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);
int    OpSim_positioner_test_collision_tips_vs_edges           (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2, float tip_radius);
int    OpSim_positioner_test_collision_edges_vs_edges          (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);
int    OpSim_positioner_test_collision_mupoz_bases_vs_all      (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);
int    OpSim_positioner_test_collision_potzpoz                 (focalplane_struct *pFocal, fibergeom_struct *pGeom1, fibergeom_struct *pGeom2, float main_axis_separation);
int    OpSim_positioner_test_collision_mupoz                   (focalplane_struct *pFocal, fibergeom_struct *pGeom1, fibergeom_struct *pGeom2);
int    OpSim_positioner_test_collisions_potzpoz                (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_test_collisions_mupoz                  (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_test_collisions                        (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_test_collisions_echidna                (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_test_collision_echidna                 (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);
int    OpSim_positioner_test_collisions_aesop                  (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_test_collision_aesop_AAO               (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);
int    OpSim_positioner_test_collision_aesop_AAO_crossing      (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);
int    OpSim_positioner_test_collision_aesop_AAO_pseudo3D      (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);

int    OpSim_positioner_test_collisions_starbug                (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y);
int    OpSim_positioner_test_collision_starbug                 (focalplane_struct *pFocal, fibergeom_struct *pFibGeom1, fibergeom_struct *pFibGeom2);

int    OpSim_positioner_write_instantaneous_positioner_geometry_potzpoz(FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_write_instantaneous_positioner_geometry_mupoz  (FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_write_instantaneous_positioner_geometry_starbug(FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_write_instantaneous_positioner_geometry_spine  (FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_write_instantaneous_positioners_geometry       (const char* str_filename, focalplane_struct *pFocal);
int    OpSim_positioner_write_instantaneous_positioners_geometry_plotfile (const char* str_filename, focalplane_struct *pFocal, survey_struct *pSurvey);

BOOL   OpSim_positioner_check_if_high_res                      (int i, int j, int hilo_pattern, int hilo_ratio);

int    OpSim_positioner_setup_fibers_geometry                  (focalplane_struct *pFocal, telescope_struct *pTele);
int    OpSim_positioner_read_fibers_geometry_from_file         (focalplane_struct *pFocal, const char *str_filename);
int    OpSim_positioner_write_fibers_geometry                  (focalplane_struct *pFocal, survey_struct *pSurvey, const char *str_filename);
int    OpSim_positioner_plot_fibers_geometry                   (focalplane_struct *pFocal, telescope_struct *pTele, survey_struct *pSurvey, const char *str_filename);
int    OpSim_positioner_determine_fiber_rivals                 (focalplane_struct *pFocal);
int    OpSim_positioner_count_fibers                           (focalplane_struct *pFocal );
int    OpSim_positioner_setup_fibers                           (focalplane_struct *pFocal, telescope_struct *pTele, survey_struct *pSurvey, BOOL do_plots);
int    OpSim_positioner_setup_fiber_mapping                    (focalplane_struct *pFocal);

int    OpSim_positioner_calc_tile_offaxis_max                  (focalplane_struct *pFocal);
int    OpSim_positioner_calc_tile_vertices                     (focalplane_struct *pFocal); 
int    OpSim_positioner_write_tile_vertices                    (focalplane_struct *pFocal, const char* str_filename );

int    OpSim_positioner_associate_fibers_with_quads            (focalplane_struct *pFocal); 

int    OpSim_positioner_check_for_fiber_failure_mupoz          (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_check_for_fiber_failure_potzpoz        (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_check_for_fiber_failure_echidna        (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_check_for_fiber_failure_aesop          (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_check_for_fiber_failure_starbug        (focalplane_struct *pFocal, fiber_struct *pFib);
int    OpSim_positioner_check_for_fiber_failures               (focalplane_struct *pFocal);

int    OpSim_positioner_write_fibers_report                    (focalplane_struct *pFocal, const char *str_filename);

float  OpSim_positioner_calc_relative_throughput_fiber_potzpoz (focalplane_struct *pFocal, fiber_struct *pFib );
float  OpSim_positioner_calc_relative_throughput_fiber_mupoz   (focalplane_struct *pFocal, fiber_struct *pFib );
float  OpSim_positioner_calc_relative_throughput_fiber_echidna (focalplane_struct *pFocal, fiber_struct *pFib );
float  OpSim_positioner_calc_relative_throughput_fiber_aesop   (focalplane_struct *pFocal, fiber_struct *pFib );
float  OpSim_positioner_calc_relative_throughput_fiber_starbug (focalplane_struct *pFocal, fiber_struct *pFib );
float  OpSim_positioner_calc_relative_throughput_fiber         (focalplane_struct *pFocal, fiber_struct *pFib );

//int    init_sky_fiber_sectors                 (focalplane_struct *pFocal );
int    OpSim_positioner_assign_fibers_to_sectors               (focalplane_struct *pFocal );
int    OpSim_positioner_add_fibers_to_offset_histos            (focalplane_struct *pFocal );
int    OpSim_positioner_assign_fibers_to_be_sky_fibers         (focalplane_struct *pFocal, survey_struct *pSurvey );
int    OpSim_positioner_record_fiber_assignment_stats          (focalplane_struct *pFocal );

int    OpSim_positioner_test_if_inside_tile_outer_perimeter    (focalplane_struct *pFocal, double x, double y);
int    OpSim_positioner_test_if_inside_tile_inner_perimeter    (focalplane_struct *pFocal, double x, double y);
int    OpSim_positioner_test_if_near_tile_inner_perimeter      (focalplane_struct *pFocal, double x, double y);
int    OpSim_positioner_test_if_inside_tile                    (focalplane_struct *pFocal, double x, double y);

////////////////////////////////////////////////////////////////////////////
int    OpSim_positioner_set_switchable_fibers_to_res           (focalplane_struct *pFocal, utiny resolution_code );

////////////////////////////////////////////////////////////////////////////
#endif
