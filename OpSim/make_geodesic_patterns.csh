#!/bin/tcsh -f

# CVS_REVISION "$Revision: 1.4 $"
# CVS_DATE     "$Date: 2014/07/31 14:48:51 $"

#see http://www.antiprism.com/programs/geodesic.html

set IMIN = 1
set IMAX = 40
set CLASS = 1

if ( $#argv == 1 ) then
  set IMAX = $argv[1]
else if ( $#argv == 2 ) then
  set IMIN = $argv[1]
  set IMAX = $argv[2]
else if ( $#argv == 3 ) then
  set IMIN = $argv[1]
  set IMAX = $argv[2]
  set CLASS = $argv[3]
else
endif

if ( ! -d ${HOME}/4most/ ) then
  echo "You must have a directory called: ${HOME}/4most/"
  exit 1
endif

if ( ! -d ${HOME}/4most/sky_tiling_patterns ) mkdir ${HOME}/4most/sky_tiling_patterns

set BASE = ${HOME}/4most/sky_tiling_patterns/geodesic
if ( ! -d $BASE ) mkdir $BASE

if ( ! -d $BASE ) then
  echo "I must be able to make or find a directory called: ${BASE}"
  exit 1
endif


cd $BASE
set TEMPFILE = geo_temp.txt
if ( -e $TEMPFILE ) rm $TEMPFILE

set INDEX_FILE =  ${BASE}/geodesic_index.txt

set I = $IMIN
while ( $I <= $IMAX ) 
  geodesic -c ${CLASS} -f $I ico | off2crds > $TEMPFILE 
  set NPTS = `wc -l $TEMPFILE | gawk '{print $1}'`
  set OUTFILE = "${BASE}/geodesic_tiling_pattern_npts${NPTS}.txt" 
  set AREA_PER_POINT = `wcalc -q -P4 "4*pi*(180./pi)**2/$NPTS"` 
  set FLAG = "   "
  if ( -e $OUTFILE ) set FLAG = "***"
  echo "Geodesic pattern $I class $CLASS npts $NPTS area_per_point= $AREA_PER_POINT -> $OUTFILE $FLAG"
  echo "#Geodesic pattern $I class $CLASS npts $NPTS area_per_point= $AREA_PER_POINT " > $OUTFILE
  gawk -f ~/.gawkrc --source '//{i++;x=$1;y=$2;z=$3;phi=atan2(y,x);theta=acos2(z,1.0);ra=(phi*180./pi)+180.; dec=90.0 - (theta*180./pi);printf("%05d %12.7f %12.7f\n", i, ra, dec)}' $TEMPFILE >> $OUTFILE
  
  if ( -e $TEMPFILE ) rm $TEMPFILE
  @ I ++
end

#reindex the files
gawk '$1~/^#/{print substr($0,2);nextfile}' geodesic_tiling_pattern_npts*.txt | sort -n -k 7 > $INDEX_FILE


set HTML_DATA_DIR = "${HOME}/www/4most/geodesic_pattern_data"
if ( ! -d $HTML_DATA_DIR ) mkdir $HTML_DATA_DIR

#make an html page

set HTML_FILE_TEMP = "geodesic_pattern_list.html"
echo "<\!DOCTYPE html>\
<html>\
<head>\
  <title>List of geodesic patterns</title>\
  <style type='text/css'>\
    body {font-size:80%;}\
    h1 {font-size:2.5em;}\
    h2 {font-size:1.875em;}\
    p  {font-size:0.875em;}\
    tr {background-color:#F0F0F0;text-align:center}\
    td {font-size:0.800em;}\
    #header-row  {background-color:#A0A0A0; font-size:1.20em;}\
    #special-row {background-color:#E0E0E0;}\
  </style>\
  <meta name='description' content='List of geodesic patterns'>\
    <link rel='icon' type='img/gif' href='http://www.mpe.mpg.de/~tdwelly/4MOSTlogo_icon.gif' />\
  </meta>\
</head>\
<body>\
<h1>List of Geodesic patterns</h1>\
The table below contains a list of geodesic patterns that can be used by the 4MOST Facility Simulator (4FS) to lay out Fields on the sky.<br>\
The patterns were generated using code from <a href='http://www.antiprism.com/programs/geodesic.html' target=_blank>antiprism.com</a><br>\
The patterns are ordered by ascending number of points (and hence in decending order of area per point).<br>\
Only patterns with fewer than 20000 points are shown.<br>\
<p>\
<b>Key:</b><br>\
Npts = Number of points on the sphere<br>\
Class = Class of face division pattern (I,II or III)<br>\
P<sub>freq</sub> = Pattern frequency (number of repetitions per icosahedral edge)<br>\
n,m = Class III Pattern description<br>\
Area/point = Average solid angle per point (deg<sup>2</sup>)<br>\
\
<table border=0>\
<tr id='header-row'>\
  <td>Npts</td>\
  <td>Class</td>\
  <td>P<sub>freq</sub></td>\
  <td>n,m</td>\
  <td>Area/point</td>\
  <td>antiprism.com command</td>\
  <td>Data file (ascii)</td>\
</tr>\
<tr id='header-row'>\
  <td></td>\
  <td></td>\
  <td></td>\
  <td></td>\
  <td>deg<sup>2</sup></td>\
  <td></td>\
  <td>Index Longitude(deg) Latitude(deg)</td>\
</tr>" > $HTML_FILE_TEMP

gawk -v data_path="${HTML_DATA_DIR:t}" '//{nm=$5;Class=($5=="1"?"I":($5=="2"?"II":"III")); \
printf("<tr>\n  <td>%d</td>\n  <td style=\"background-color:%s; text-align:center;\">%s</td>\n  <td>%s</td>\n  <td>%s</td>\n  <td>%.5g</td>\n  <td style=\"text-align:left\">geodesic -c %s -f %d ico </td>\n  <td> <a href=\"%s/geodesic_tiling_pattern_npts%d.txt\">geodesic_tiling_pattern_npts%d.txt</a></td>\n</tr>\n",\
$7,(Class=="I"?"red":(Class=="II"?"yellow":"green")), Class, $3, (Class=="I"?sprintf("0,%d",$3):(Class=="II"?sprintf("%d,%d",$3,$3):nm)), $9,\
nm, $3, data_path, $7, $7)}' $INDEX_FILE >> $HTML_FILE_TEMP

echo "</table>\
\
<p>\
Note: Pipe the antiprism.com outputs through <tt>off2crds</tt> to create a list of vertex coords.<br>\
To convert from unit sphere to RA,Dec using awk, follow this example: <br>\
<tt>geodesic -c 3,4 -f 5 ico | off2crds | gawk 'function acos2(a,h){return(atan2(((h)**2-(a)**2)**0.5,(a)))} BEGIN {pi=4.*atan2(1.,1.);} //{i++;x="'$'"1;y="'$'"2;z="'$'"3;phi=atan2(y,x);theta=acos2(z,1.);ra=(phi*180./pi)+180.;dec=90.-(theta*180./pi);print ra,dec}'</tt><br><br>\
</body>\
</html>" >> $HTML_FILE_TEMP

if ( -d ${HOME}/www/ ) then
  set HTML_FILE = "${HOME}/www/4most/geodesic_pattern_list.html"
  cp $HTML_FILE_TEMP $HTML_FILE
  echo "Web page describing geodesic pattern list has been installed to: $HTML_FILE"

  set NFILES = `wc -l $INDEX_FILE | gawk '{print $1}'`
  echo "Now copying new data files over to web directory: ${HTML_DATA_DIR}/"
  set I = 1
  while ( $I <= $NFILES ) 
    set FILE = `gawk -v i=$I '// {n++;if(n==i){printf("geodesic_tiling_pattern_npts%d.txt", $7);exit}}' $INDEX_FILE`
    if ( (-e "$FILE") && (! -e "${HTML_DATA_DIR}/$FILE")) then 
      cp -pv "$FILE" "${HTML_DATA_DIR}/"
    endif
    @ I ++
  end


else
  echo "Web page describing geodesic pattern list is at : $HTML_FILE_TEMP"
endif


exit


