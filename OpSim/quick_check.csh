#!/bin/tcsh -f
#make some crude statistical summaries
set CALC_FOM = "TRUE"

if ( $#argv <= 0 ) then
  exit 1
endif

set ORIG_DIR = `pwd`

foreach DIR ( $argv )
  cd $ORIG_DIR 

  if (! -d $DIR ) then
    echo "ERROR: $DIR does not exist"
    continue
  endif

  cd $DIR
 
  set LOGFILE = quick_check_stats.log
  if ( -e $LOGFILE ) mv $LOGFILE ${LOGFILE}~~

  #calculate from the assignment file the numbber of objects allocated fibers, and the number of fibers allocated to objects
  echo ""                                                                                  | tee -a $LOGFILE
  echo "#################################################################################" | tee -a $LOGFILE
  echo "### Summary stats for $DIR"                                                        | tee -a $LOGFILE
  echo "### Working in `pwd`"                                                              | tee -a $LOGFILE
  echo "#################################################################################" | tee -a $LOGFILE
  echo ""                                                                                  | tee -a $LOGFILE


  #First calculate the number of fiber allocations that could have been made - in theory
#  gawk 'BEGIN {moon_list["DarkGrey"]=0;moon_list["Bright"]=0;moon_list["Total"]=0;} /OVERHEADS.MINIMUM_SKY_FIBERS_LOW_RES/ {nsky_lo=$3} /OVERHEADS.MINIMUM_SKY_FIBERS_HIGH_RES/ {nsky_hi=$3} /fiber positions/{nhi=$8;nlo=$12;} /On-sky time is available for up to/ {ntiles[$10]=$9} /We have time to carry out up to/ {ntiles["Total"]=$10} END {printf("#Survey statistics (numbers of fibers available) - calculated from the extract_gal logfile: %s\n", FILENAME); printf("#%-11s %12s %12s %12s %12s %12s\n", "Moon_phase", "nTiles", "nHiRes","nLoRes", "nHiRes-sky","nLoRes-sky");for(m in moon_list) {printf("%-12s %12d %12d %12d %12d %12d\n", m, ntiles[m], ntiles[m]*nhi, ntiles[m]*nlo, ntiles[m]*(nhi-nsky_hi), ntiles[m]*(nlo-nsky_lo))}}' ${DIR}/extract_gal.log
  gawk 'BEGIN {moon_list["DarkGrey"]="dg";moon_list["Bright"]="bb";moon_list["Total"]="any";reslist["lo"]="lo";reslist["hi"]="hi";} /OVERHEADS.MINIMUM_SKY_FIBERS_LOW_RES/ {nsky["lo"]=$3} /OVERHEADS.MINIMUM_SKY_FIBERS_HIGH_RES/ {nsky["hi"]=$3} /fiber positions/{n["hi"]=$8;n["lo"]=$12;} /On-sky time is available for up to/ {ntiles[$10]=$9} /We have time to carry out up to/ {ntiles["Total"]=$10} END {printf("#Survey statistics (numbers of fibers available) - calculated from the OpSim logfile: %s\n", FILENAME); printf("#%-7s %8s %10s %10s %10s\n", "MOON", "RES", "nTiles", "nFibs", "nFibs-nSky"); for(r in reslist){for(m in moon_list){printf("%-8s %8s %10d %10d %10d\n", m, r, ntiles[m], ntiles[m]*n[r],ntiles[m]*(n[r]-nsky[r]))}}}' extract_gal.log | tee -a $LOGFILE


  echo ""                                                             | tee -a $LOGFILE
  echo "#Calculating source-centric stats from: assigned_objects.txt" | tee -a $LOGFILE
#  gawk -v cats="AGN BAO GalHaloLR GalDiskLR Clusters GalHaloHR GalDiskHR" -v res="lo lo lo lo lo hi hi" 'BEGIN {ncats=split(cats,c," ");split(res,r," ");moon["bb"]=0;moon["dg"]=0;reslist["lo"]=0;reslist["hi"]=0;printf("#%-9s %8s %8s %12s %12s %12s %12s\n","DRS","Moon","res","Ntotal","Nalloc","FracAlloc","FiberAlloc")}\
#$1!~/^#/{m=($9>0?"bb":"dg");n[m,$12]++;if($5>0){nalloc[m,$12]++;nfalloc[m,$12]+=$5}}\
#END {for(i=1;i<=ncats;i++){for(m in moon){tot_n[r[i],m]+=n[m,i];tot_nalloc[r[i],m]+=nalloc[m,i];tot_nfalloc[r[i],m]+=nfalloc[m,i];tot_n[m]+=n[m,i];tot_nalloc[m]+=nalloc[m,i];tot_nfalloc[m]+=nfalloc[m,i];tot_n[r[i]]+=n[m,i];tot_nalloc[r[i]]+=nalloc[m,i];tot_nfalloc[r[i]]+=nfalloc[m,i];\
#printf("%-10s %8s %8s %12d %12d %12.4f %12d\n",c[i],m,r[i],n[m,i],nalloc[m,i],nalloc[m,i]/(n[m,i]>0?n[m,i]:1.),nfalloc[m,i]);}};\
#for(m in moon) {for(rr in reslist){printf("%-10s %8s %8s %12d %12d %12.4f %12d\n","Total",m, rr, tot_n[rr,m],tot_nalloc[rr,m],tot_nalloc[rr,m]/(tot_n[rr,m]>0?tot_n[rr,m]:1.),tot_nfalloc[rr,m]);}\
#printf("%-10s %8s %8s %12d %12d %12.4f %12d\n","Total",m,"both",tot_n[m],tot_nalloc[m],tot_nalloc[m]/(tot_n[m]>0?tot_n[m]:1.),tot_nfalloc[m]);};\
#for(rr in reslist){printf("%-10s %8s %8s %12d %12d %12.4f %12d\n","Total","any",rr,tot_n[rr],tot_nalloc[rr],tot_nalloc[rr]/(tot_n[rr]>0?tot_n[rr]:1.),tot_nfalloc[rr]);}}' ${DIR}/assigned_objects.txt
#  gawk -f ~/4most/sim/quick_check.awk  extract_gal.log assigned_objects.txt | tee -a $LOGFILE

echo "#---KEY---\
#DRS        - Name of DRS or Total\
#Moon       - Moon phase (bb = Bright, dg = Dark/Grey, any = any moon phase)\
#res        - Fiber/Object resolution (lo=Low resolution, hi=High resolution)\
#Ntotal     - Total number of objects in the survey footprint (for this moon phase)\
#Nalloc     - Total number of objects that were allocated a fiber in at least one tile (for this moon phase)\
#FracAlloc  - Fraction of objects that were allocated a fiber in at least one tile (for this moon phase)\
#FiberAlloc - Total number of fiber-exposures that were allocated to an object (for this moon phase)"  | tee -a $LOGFILE
printf '#%-9s %8s %8s %12s %12s %12s %12s\n' "DRS" "Moon" "res" "Ntotal" "Nalloc" "FracAlloc" "FiberAlloc" | tee -a $LOGFILE

  gawk -v cats="AGN BAO GalHaloLR GalDiskLR Clusters GalHaloHR GalDiskHR" -v res="lo lo lo lo lo hi hi" 'BEGIN {\
    ncats=split(cats,c," ");\
    split(res,r," ");\
    moon["bb"]=0;\
    moon["dg"]=0;\
    reslist["lo"]=0;\
    reslist["hi"]=0;\
    last1 = "-";\
    nfibs = 0;\
}\
ARGIND == 1 && $0~/Assigning fibers for field/ {\
  moonf[$6]=($16=="bb"?"bb":"dg");\
}\
ARGIND == 2 && $1!~/^#/ {\
    if ( $14 != last14 && last1 != "-" )\
    {\
        m=moonf[last1];\
        n[m,last12]++;\
        if(nfibs>0){\
            nalloc[m,last12]++;\
            nfalloc[m,last12]+=nfibs;\
        }\
        nfibs = 0;\
        last1 = "-";\
    }\
    last1  = $1; last12 = $12; last14 = $14; nfibs += $5;}\
END {\
    if ( last1 != "-" )\
    {\
        m=moonf[last1];\
        n[m,last12]++;\
        if(nfibs>0){\
            nalloc[m,last12]++;\
            nfalloc[m,last12]+=nfibs;\
        }\
    }\
    for(i=1;i<=ncats;i++){\
      for(m in moon)\
      {\
        tot_n[r[i],m]+=n[m,i];\
        tot_nalloc[r[i],m]+=nalloc[m,i];\
        tot_nfalloc[r[i],m]+=nfalloc[m,i];\
        tot_n[i]+=n[m,i];\
        tot_nalloc[i]+=nalloc[m,i];\
        tot_nfalloc[i]+=nfalloc[m,i];\
        tot_n[m]+=n[m,i];\
        tot_nalloc[m]+=nalloc[m,i];\
        tot_nfalloc[m]+=nfalloc[m,i];\
        tot_n[r[i]]+=n[m,i];\
        tot_nalloc[r[i]]+=nalloc[m,i];\
        tot_nfalloc[r[i]]+=nfalloc[m,i];\
        printf("%-10s %8s %8s %12d %12d %12.4f %12d\n",c[i],m,r[i],n[m,i],nalloc[m,i],nalloc[m,i]/(n[m,i]>0?n[m,i]:1.),nfalloc[m,i]);\
      }\
      printf("%-10s %8s %8s %12d %12d %12.4f %12d\n",c[i],"any",r[i],tot_n[i],tot_nalloc[i],tot_nalloc[i]/(tot_n[i]>0?tot_n[i]:1.),tot_nfalloc[i]);\
    };\
    for(m in moon)\
    {\
      for(rr in reslist){\
        printf("%-10s %8s %8s %12d %12d %12.4f %12d\n","Total",m, rr, tot_n[rr,m],tot_nalloc[rr,m],tot_nalloc[rr,m]/(tot_n[rr,m]>0?tot_n[rr,m]:1.),tot_nfalloc[rr,m]);\
      }\
      printf("%-10s %8s %8s %12d %12d %12.4f %12d\n","Total",m,"any",tot_n[m],tot_nalloc[m],tot_nalloc[m]/(tot_n[m]>0?tot_n[m]:1.),tot_nfalloc[m]);\
    };\
    for(rr in reslist)\
    {printf("%-10s %8s %8s %12d %12d %12.4f %12d\n","Total","any",rr,tot_n[rr],tot_nalloc[rr],tot_nalloc[rr]/(tot_n[rr]>0?tot_n[rr]:1.),tot_nfalloc[rr]);}\
}' extract_gal.log assigned_objects.txt | tee -a $LOGFILE




  #now calculate the fiber-centric stats based on the fiber assignment file 
  set NSKY_LO = `gawk '/OVERHEADS.MINIMUM_SKY_FIBERS_LOW_RES/ {print $3;exit}'  extract_gal.log`
  set NSKY_HI = `gawk '/OVERHEADS.MINIMUM_SKY_FIBERS_HIGH_RES/ {print $3;exit}' extract_gal.log`
  echo ""                                                                                                                                                                | tee -a $LOGFILE
  echo "#Now calculating fiber-centric statistics based on the fiber assignment files: f*/assignment_??.tab.gz"			           				 | tee -a $LOGFILE
  echo "#-----KEY----"																			 | tee -a $LOGFILE
  echo "#MOON        = Moon phase"																	 | tee -a $LOGFILE
  echo "#RES         = Fiber resolution"																 | tee -a $LOGFILE
  echo "#NTILES      = Number of tiles that were executed over the survey for this moon-phase" 										 | tee -a $LOGFILE
  echo "#totFibs     = Total number of fiber-exposures that were executed over the survey for this moon-phase/resolution" 						 | tee -a $LOGFILE
  echo "#totAlloc    = Total number of fiber-exposures that were allocated to an input target, summed over the survey for this moon-phase/resolution" 			 | tee -a $LOGFILE
  echo "#totUnalloc  = Total number of unallocated fiber-exposures, summed over the survey for this moon-phase/resolution, including sky fiber-exposures" 		 | tee -a $LOGFILE
  echo "#meanFibs    = Mean number of fibers, per tile, averaged over the survey for this moon-phase/resolution" 							 | tee -a $LOGFILE
  echo "#meanAlloc   = Mean number of fibers that were allocated to an input target, per time, averaged over the survey for this moon-phase/resolution" 		 | tee -a $LOGFILE
  echo "#meanUnalloc = Mean number of unallocated fibers, per tile, averaged over the survey for this moon-phase/resolution, including sky fiber-exposures" 		 | tee -a $LOGFILE
  echo "#minSkyFibs  = Minimum number of sky fibers that must be reserved per tile for this resolution" 								 | tee -a $LOGFILE
																					 
  printf '#%-7s %4s %10s %10s %10s %10s %10s %10s %10s %10s\n' "MOON" "RES" "NTILES" "totFibs" "totAlloc" "totUnalloc" "meanFibs" "meanAlloc" "meanUnalloc" "minSkyFibs" | tee -a $LOGFILE
  foreach MOON ( "bb" "dg" ) 
    zcat -f --stdout f?????/assignment_${MOON}.tab* |  gawk -v moon="$MOON" -v nsky_lo=$NSKY_LO -v nsky_hi=$NSKY_HI 'BEGIN {nsky[1]=nsky_lo;nsky[2]=nsky_hi;} /Observation run #/ {ntiles++;} $0!~/#/ && NF >= 4 {n[$2]++; if(NF == 4){nunalloc[$2]++}else{nalloc[$2]++}} END {for(res in n) {printf("%-8s %4s %10d %10d %10d %10d %10g %10.2f %10.2f %10d\n", (moon=="bb"?"Bright":"DarkGrey"), (res==1?"lo":"hi"),ntiles,n[res],nalloc[res],nunalloc[res],n[res]/ntiles,nalloc[res]/ntiles,nunalloc[res]/ntiles, nsky[res])}}' | tee -a $LOGFILE
  end
  echo "" | tee -a $LOGFILE


  echo "#################################################################################" | tee -a $LOGFILE


  #calculate a rough FoM for each catalogue
  if ( "$CALC_FOM" == "nnnnnnnTRUE" ) then
    echo "################################ FOM START #################################################" | tee -a $LOGFILE
    


    echo "################################ FOM STOP  #################################################" | tee -a $LOGFILE
  endif


end





exit

#
#
#zcat NTT_MuPoz_test/f*/assignment_dg.tab.gz >   NTT_MuPoz_test/assignment_dg.tab
#zcat NTT_MuPoz_test/f*/assignment_bb.tab.gz >   NTT_MuPoz_test/assignment_bb.tab
#
#zcat NTT_PotzPoz_test/f*/assignment_dg.tab.gz >   NTT_PotzPoz_test/assignment_dg.tab
#zcat NTT_PotzPoz_test/f*/assignment_bb.tab.gz >   NTT_PotzPoz_test/assignment_bb.tab
#
#zcat VISTA_PotzPoz_test/f*/assignment_dg.tab.gz >   VISTA_PotzPoz_test/assignment_dg.tab
#zcat VISTA_PotzPoz_test/f*/assignment_bb.tab.gz >   VISTA_PotzPoz_test/assignment_bb.tab
#
#printf '#%-29s %4s %10s %10s %10s %10s %10s %10s %10s\n' "FILENAME" "RES" "NTILES" "totFIBS" "totALLOC" "totUNALLOC" "meanFIBS" "meanALLOC" "meanUNALLOC"  
#foreach FILE ( NTT_MuPoz_test/assignment_dg.tab NTT_MuPoz_test/assignment_bb.tab NTT_PotzPoz_test/assignment_dg.tab NTT_PotzPoz_test/assignment_bb.tab VISTA_PotzPoz_test/assignment_dg.tab VISTA_PotzPoz_test/assignment_bb.tab ) 
#  gawk '/Observation run #/ {ntiles++;} $0!~/#/ && NF >= 4 {n[$2]++; if(NF == 4){nunalloc[$2]++}else{nalloc[$2]++}} END {for(res in n) {printf("%-30s %4s %10d %10d %10d %10d %10.4f %10.4f %10.4f\n",FILENAME, (res==1?"low":"high"),ntiles,n[res],nalloc[res],nunalloc[res],n[res]/ntiles,nalloc[res]/ntiles,nunalloc[res]/ntiles)}} ' $FILE
#end
