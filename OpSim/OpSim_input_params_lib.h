
//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_input_params_lib.h
//--    Use: This is a header for the OpSim_input_params_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_INPUT_PARAMS_H
#define OPSIM_INPUT_PARAMS_H

//#define CVS_REVISION_OPSIM_INPUT_PARAMS_LIB_H "$Revision: 1.58 $"
//#define CVS_DATE_OPSIM_INPUT_PARAMS_LIB_H     "$Date: 2015/11/11 10:53:20 $"

#include <stdio.h>

#include "define.h"
#include "4FS_OpSim.h"
#include "params_lib.h"
#include "OpSim_timeline_lib.h"
#include "OpSim_telescope_site_lib.h"


//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.58 $"
//#define CVS_DATE_H     "$Date: 2015/11/11 10:53:20 $"
inline static int OpSim_input_params_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX  DEF_STRLEN
#define LSTR_MAX DEF_LONGSTRLEN

//the input parameters list handling  has got too long, so separate it to this
//new separate module

typedef struct {
  ////////////////////////////////////////////////////////////////////////////////////
  ///////////////Start by listing all the possible input parameters //////////////////
  ///////////////that are defined in the FSIFD document                 //////////////
  //general simulation params
  char   SIM_CODE_NAME[STR_MAX];
  char   SIM_COMMENTS[LSTR_MAX];

  //Telescope parameters
  char   TELESCOPE_CODE_NAME[STR_MAX];
  float  TELESCOPE_FOV_AREA;              // square degrees - area of the hexagon
  float  TELESCOPE_PLATE_SCALE; // mm/arcsec
         
  float  TELESCOPE_EXPOSURE_SCALING;    // ratio of input exposure times to exposure times
  //                                                                to be used for this telescope
         
  //Positioner parameters
  char   POSITIONER_CODE_NAME[STR_MAX];
  //all positioner measurements are in mm
  float  POSITIONER_PATROL_RADIUS_MAX;
  float  POSITIONER_PATROL_RADIUS_MIN;
  char   POSITIONER_LAYOUT_FILENAME[STR_MAX];
  char   POSITIONER_LAYOUT_SHAPE[STR_MAX];
  float  POSITIONER_COORDINATE_TWEAK_FACTOR;
  float  POSITIONER_SPACING_LOW_RES;
  int    POSITIONER_SPACING_HIGH_LOW_RATIO;
  char   POSITIONER_SPACING_HIGH_LOW_PATTERN[STR_MAX];
  float  POSITIONER_MINIMUM_SEPARATION;
  char   POSITIONER_FAMILY[STR_MAX];
  float  POSITIONER_ECHIDNA_SPINE_DIAMETER;
  float  POSITIONER_ECHIDNA_SPINE_LENGTH;
  float  POSITIONER_AESOP_SPINE_DIAMETER;
  float  POSITIONER_AESOP_SPINE_LENGTH;
//  float  POSITIONER_POS_B_TRAPEZIUM_WIDTH_AT_TIP;
//  float  POSITIONER_POS_B_TRAPEZIUM_WIDTH_AT_BASE;
//  float  POSITIONER_POS_B_TRAPEZIUM_PIVOT_OFFSET;
//  float  POSITIONER_POS_B_TIP_RADIUS;
//  float  POSITIONER_POS_B_TIP_WIDTH;
//  float  POSITIONER_LAMOST_ROTATOR_PARAM1;
//  float  POSITIONER_LAMOST_ROTATOR_PARAM2;
//  float  POSITIONER_LAMOST_TIP_RADIUS;
//  float  POSITIONER_LAMOST_TIP_WIDTH;
   
  //should include the following in the interfaces file v soon
  float  POSITIONER_POTZPOZ_WIDTH;
  float  POSITIONER_POTZPOZ_PIVOT_OFFSET;
  float  POSITIONER_POTZPOZ_RECTANGLE_LENGTH;
  float  POSITIONER_POTZPOZ_TIP_RADIUS;
  //  float  POSITIONER_POTZPOZ_TIP_WIDTH;

  float  POSITIONER_MUPOZ_TIP_WIDTH;   
  float  POSITIONER_MUPOZ_TIP_RADIUS; 
  float  POSITIONER_MUPOZ_ARM_WIDTH;   
  float  POSITIONER_MUPOZ_ARM_LENGTH; 
         
  float  POSITIONER_OLD_MUPOZ_TIP_WIDTH; 
  float  POSITIONER_OLD_MUPOZ_TIP_DIST;    

  float  POSITIONER_STARBUG_RADIUS;

  //Environmental effects - new default values interpretted from 
  //http://adsabs.harvard.edu//abs/2010SPIE.7737E..19B
  float  ENVIRONMENT_CLOUD_FRACTION_PHOTOMETRIC;
  float  ENVIRONMENT_CLOUD_FRACTION_CLEAR;
  float  ENVIRONMENT_CLOUD_FRACTION_THIN;
  float  ENVIRONMENT_CLOUD_FRACTION_THICK;
  float  ENVIRONMENT_CLOUD_FRACTION_CLOSED;

  float  ENVIRONMENT_MOON_FRACTION_DARK;
  float  ENVIRONMENT_MOON_FRACTION_GREY;
  float  ENVIRONMENT_MOON_FRACTION_BRIGHT;

  float  ENVIRONMENT_WIND_FRACTION_CONSTRAINED;  //wind speed >=12 m s^-1 - must point >90deg from wind
  float  ENVIRONMENT_WIND_FRACTION_DOME_CLOSED;  //wind speed >=18 m s^-1 - must close dome
  
  //new seeing method, these are the seeing distribution values taken from the ESO P2PP document
	//Bierwirth, T et al., 	2010, SPIE, 7737E, 19B,  "New observing concepts for ESO survey telescopes", 	
  //http://adsabs.harvard.edu//abs/2010SPIE.7737E..19B
  float  ENVIRONMENT_SEEING_PROBABILITY[ENVIRON_MAX_SEEING_GRADES];
  float  ENVIRONMENT_SEEING_THRESH_LOW[ENVIRON_MAX_SEEING_GRADES];
  float  ENVIRONMENT_SEEING_THRESH_HIGH[ENVIRON_MAX_SEEING_GRADES];

  
  //Overhead parameters                                
  float  OVERHEADS_FAULT_NIGHTS;// nights per year
  float  OVERHEADS_MAINTENANCE_NIGHTS;    // nights per year
  float  OVERHEADS_SLEW_SPEED;            //Telescope slew speed (deg s^-1)
  float  OVERHEADS_SLEW_ACCEL;            //Telescope slew acceleration rate (deg s^-2)
  float  OVERHEADS_SETTLING_PER_SLEW;     // time required to settle, mins per slew
  float  OVERHEADS_GUIDE_STAR_ACQUISITION_TIME;  // time required to acquire a guide star, mins
  float  OVERHEADS_POSITIONING_PER_TILE;  // minutes per tile
  float  OVERHEADS_READOUT_PER_TILE;      // minutes per tile
  float  OVERHEADS_TIME_PER_DITHER;       // minutes per dither
  float  OVERHEADS_CALIBRATION_PER_TILE;  // minutes per tile
  int    OVERHEADS_MINIMUM_SKY_FIBERS_LOW_RES;
  int    OVERHEADS_MINIMUM_SKY_FIBERS_HIGH_RES;
  int    OVERHEADS_MINIMUM_PI_FIBERS_LOW_RES;
  int    OVERHEADS_MINIMUM_PI_FIBERS_HIGH_RES;

  int    OVERHEADS_MINIMUM_SKY_FIBERS_NUM_SECTORS;
  int    OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_LOW_RES;
  int    OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_HIGH_RES;

  
  int  OVERHEADS_MAINTENANCE_CADENCE;              // time between shutdowns (days)
  int  OVERHEADS_MAINTENANCE_NIGHTS_PER_SHUTDOWN;  // time between shutdowns (days)

  //Tiling parameters
  char   TILING_METHOD[STR_MAX];
  int    TILING_NUM_POINTS;
  char   TILING_FILENAME[STR_MAX];
  float  TILING_DEC_MIN;
  float  TILING_DEC_MAX;
  int    TILING_NUM_DITHERS;
  double  TILING_DITHER_OFFSETS_X[MAX_DITHERS_PER_FIELD];
  double  TILING_DITHER_OFFSETS_Y[MAX_DITHERS_PER_FIELD];
  //  double  TILING_DITHER_OFFSETS_Z[MAX_DITHERS_PER_FIELD];
    
    //Survey strategy
  float  SURVEY_DURATION; // years
  double SURVEY_JD_START ;  //days
  //  float  SURVEY_MEAN_DARK_TIME ; //hours per night, only important for non-timeline surveys
  char   SURVEY_STRATEGY_TYPE[STR_MAX];          
  //  char   SURVEY_GOAL[STR_MAX];                   
  char   SURVEY_TILING_DESCRIPTION_FILE[STR_MAX]; 

  char   SURVEY_TILE_PLANNING_METHOD_DARKGREY[STR_MAX];                   
  char   SURVEY_TILE_PLANNING_METHOD_BRIGHT[STR_MAX];                   

  int    SURVEY_NUM_TILES_PER_FIELD_DARKGREY;
  int    SURVEY_NUM_TILES_PER_FIELD_BRIGHT;
  int    SURVEY_NUM_TILES_PER_PASS_DARKGREY;
  int    SURVEY_NUM_TILES_PER_PASS_BRIGHT  ;
  //  int    SURVEY_FLAT_NUM_PASSES_DARKGREY   ;
  //  int    SURVEY_FLAT_NUM_PASSES_BRIGHT     ;
  float  SURVEY_DBPDG_GAL_LAT_MIN_DARKGREY ; 
  float  SURVEY_DBPDG_GAL_LAT_MAX_BRIGHT   ;
  //  int    SURVEY_DBPDG_NUM_PASSES_BRIGHT    ; 
  //  int    SURVEY_DBPDG_NUM_PASSES_DARKGREY  ;
  float  SURVEY_EXPOSURE_TIME_BRIGHT ;//need to define units in documentation, default here to mins
  float  SURVEY_EXPOSURE_TIME_GREY   ;
  float  SURVEY_EXPOSURE_TIME_DARK   ;

  float  SURVEY_ZENITH_ANGLE_MIN     ;
  float  SURVEY_AIRMASS_MAX          ; 
  float  SURVEY_MOON_DIST_MIN        ;//absolute minimum moon distance
  float  SURVEY_MOON_DIST_FRAC_MIN   ;//product of moon distance and illum fraction
  //  float  SURVEY_SMC_AIRMASS_MAX;
  //  float  SURVEY_LMC_AIRMASS_MAX;
  //  int    SURVEY_SMC_MAX_TILES;        //max tiles to execute on the SMC - over full survey
  //  int    SURVEY_LMC_MAX_TILES;        //ditto for the LMC


  float  SURVEY_IQ_MAX         ;       //Maximum delivered IQ of any observed tile (arcsec,FWHM,V-band)
  //  float  SURVEY_CLOUD_COVER_CODE_MAX;  //Maximum cloud cover code for any observed tile


  char   SURVEY_FIELD_WEIGHTING_METHOD[STR_MAX];
  float  SURVEY_FIELD_WEIGHTING_PARAMS[MAX_FIELD_WEIGHTING_PARAMS];
  float  SURVEY_FIELD_WEIGHTING_LAST_FIELD_BONUS_FACTOR;
  float  SURVEY_FIELD_WEIGHTING_BLUR_SIGMA;
  float  SURVEY_FIELD_WEIGHTING_COMPLETE_FIELDS_EXPONENT;
  float  SURVEY_DELTA_MAG_FOR_PROBLEM_NEIGHBOUR;

  float SURVEY_OBJECT_WEIGHTING_COMPLETED_EXPONENT; 
  float SURVEY_OBJECT_WEIGHTING_STARTED_FACTOR;
  float SURVEY_OBJECT_WEIGHTING_HIRES_BONUS;
  float SURVEY_OBJECT_WEIGHTING_HARD_IN_BRIGHT_DECREMENT;
  float SURVEY_OBJECT_WEIGHTING_HARD_IN_BRIGHT_THRESHOLD;
  float SURVEY_OBJECT_WEIGHTING_CANNOT_COMPLETE_FACTOR;  

  //List here any more parameters that have not been added to the current
  //parameter file specification in the FSIFD document 
  char   SURVEY_MOON_SPLIT_METHOD[STR_MAX];
  float  SURVEY_MOON_SPLIT_MAG_LOW_RES;  //upper magnitude for low-res obect to be used in the bright survey  
  float  SURVEY_MOON_SPLIT_MAG_HIGH_RES;  //upper magnitude for high-res obect to be used in the bright survey  
  float  SURVEY_MOON_SPLIT_EXP_TOLERANCE;
  float  SURVEY_MAXIMUM_TEXPFRAC; 
  float  SURVEY_DISCARD_THRESHOLD_TEXP;
  float  SURVEY_FOM_SUCCESS_THRESHOLD;

  //set this to true if we will distribute spare tiles into the remainder of teh survey  
  BOOL   SURVEY_USE_UP_SPARE_TILES;
  //Factor by which Tiles should be over-allocated (planned) for Fields relative to the time available
  float  SURVEY_OVERALLOCATION_FACTOR_DARKGREY; 
  float  SURVEY_OVERALLOCATION_FACTOR_BRIGHT; 
  //this is how to decide which tile to put a source into
  char   SURVEY_TARGET_TO_FIELD_POLICY[STR_MAX];  


  //set the type of map projection to be used
  char   MAP_PROJECTION[STR_MAX]; 
  BOOL   MAP_LEFT_IS_EAST; 
  int    MAP_NPIX_RA; 
  int    MAP_NPIX_DEC; 
  int    MAP_HEALPIX_NSIDE; 

  //  // an undocumented parameter that controls the mode of operation - timeline or non-timeline
  //  BOOL   SURVEY_USE_TIMELINE;
  char SURVEY_MAIN_MODE[STR_MAX]; //the main mode of survey simulation

  
  //how many days observing to allow between each progress report plot, and incremental FoM report
  int    PROGRESS_REPORT_CADENCE; 
  int    FOM_REPORT_CADENCE; 
  //----------------------------------------
  //----------------------------------------

  //----------------------------------------
  //----------------------------------------
  //now some undocumented parameters which can be set on the command line

  char PARAM_FILENAME[STR_MAX];  //the name of the parameter file
  
  char SURVEY_SKYCALC_FILENAME[STR_MAX]; //the name of the skycalc details file


  //new cat params
  int   CAT_NUM_CATS;
  char  CAT_CODENAME[MAX_CATALOGUES][STR_MAX];
  char  CAT_DISPLAYNAME[MAX_CATALOGUES][STR_MAX];
  char  CAT_SNAME[MAX_CATALOGUES][STR_MAX];
  char  CAT_FILENAME[MAX_CATALOGUES][STR_MAX];
  char  CAT_PRIORITY_RESCALE_METHOD[MAX_CATALOGUES][STR_MAX];
  float CAT_PRIORITY_RESCALE_PARAM1[MAX_CATALOGUES];
  float CAT_PRIORITY_RESCALE_PARAM2[MAX_CATALOGUES];
  float CAT_EXPOSURE_SCALING[MAX_CATALOGUES];
  BOOL  CAT_IS_IN_UPDATED_FORMAT[MAX_CATALOGUES];
  int   CAT_CATS_TO_USE[MAX_CATALOGUES];
  //
  
  //the random number generator seed value, as supplied on the command line
  long SIM_RANDOM_SEED;

  //set this to true if you want to suppress the actual fiber-object assignment steps
  BOOL SIM_SUPPRESS_OUTPUT;
  //  char OUTPUT_CATALOGUE_MODE[STR_MAX]; //what type of catalogue output to emit?
  //whether to carry out stats on the input catalogues
  BOOL SIM_DO_STATS;
  //whether to make various plots+plotfiles
  BOOL SIM_DO_PLOTS;
  //whether to make sky density maps
  BOOL SIM_DO_MAPS;
  //which outputs to write
  BOOL SIM_WRITE_FIBER_ASSIGNMENTS;
  BOOL SIM_WRITE_FIBER_POSITIONS;
  int  SIM_MAX_FIBER_POSITION_FILES;
  int  SIM_MAX_FIBER_ASSIGNMENT_FILES;
  int  SIM_MAX_FIELD_RANKING_FILES;

  //--------------------------------------------------------------------------------------

  //maximum number of threads that this instance can use
  int  SIM_MAX_THREADS;
  BOOL SIM_GZIP_OUTPUT_CATALOGUES; //set to true if you want the output catalogue files to be gzipped

  //a debug parameter to limit the input sourcelist to a small number of objects
  int  DEBUG_MAX_OBJECTS;
  //a debug parameter to limit the input sourcelist to a small number of objects
  // only read in the first N lines from each catalogue
  int  DEBUG_MAX_OBJECTS_PER_CATALOGUE;
  //a debug parameter to stop the survey after a certain number of tiles have been carried out
  int  DEBUG_MAX_TILES;
  //a debug parameter to stop the survey after a certain number of nights operation
  int  DEBUG_MAX_NIGHTS;

  double DEBUG_RA_MIN;
  double DEBUG_RA_MAX;
  double DEBUG_DEC_MIN;
  double DEBUG_DEC_MAX;
//  double a;
//  double b;
//  double c;
//  double x;
//  double y;
//  double z;

  BOOL debug;
  BOOL SIM_CLOBBER;
  ///////////////End of the list of all the possible input parameters //////////////
  ////////////////////////////////////////////////////////////////////////////////////
}  OpSim_input_params_struct;



//----------------------------------------------------------------------------------------------
//-- public function declarations



int    OpSim_input_params_print_version              (FILE* pFile);
int    OpSim_input_params_print_usage_information    (FILE *pFile);

int    OpSim_input_params_struct_init                (OpSim_input_params_struct **ppIpar);
int    OpSim_input_params_struct_free                (OpSim_input_params_struct **ppIpar);
int    OpSim_input_params_handler                    (OpSim_input_params_struct **ppIpar, param_list_struct **ppParamList, int argc, char** argv);
int    OpSim_input_params_setup_ParamList            (OpSim_input_params_struct *pIpar, param_list_struct* pParamList,  int argc, char** argv); 

int    OpSim_input_params_interpret_map_projection   (OpSim_input_params_struct *pIpar, survey_struct *pSurvey ) ;
int    OpSim_input_params_interpret_tiling           (OpSim_input_params_struct *pIpar, fieldlist_struct *pFieldList) ;
int    OpSim_input_params_interpret_timeline         (OpSim_input_params_struct *pIpar, survey_struct *pSurvey, timeline_struct *pTime ) ;
int    OpSim_input_params_interpret_survey_strategy  (OpSim_input_params_struct *pIpar, param_list_struct* pParamList, survey_struct *pSurvey );
int    OpSim_input_params_interpret_tele             (OpSim_input_params_struct *pIpar, param_list_struct* pParamList, telescope_struct *pTele ) ;
int    OpSim_input_params_interpret_focalplane       (OpSim_input_params_struct *pIpar, focalplane_struct *pFocal ) ;

int    OpSim_input_params_interpret_catalogues       (OpSim_input_params_struct *pIpar, param_list_struct* pParamList, cataloguelist_struct *pCatList, survey_struct *pSurvey  ) ;
int    OpSim_input_params_interpret_objectlist       (OpSim_input_params_struct *pIpar, objectlist_struct *pObjList );

  


int    OpSim_input_params_interpret_everything       (OpSim_input_params_struct *pIpar, param_list_struct* pParamList, survey_struct *pSurvey, fieldlist_struct *pFieldList, timeline_struct *pTime, telescope_struct *pTele, focalplane_struct *pFocal, cataloguelist_struct *pCatList,  objectlist_struct *pObjList) ;


#endif
