#!/bin/csh -f

# CVS info "$Revision: 1.16 $"
# CVS info "$Date: 2012/12/12 18:05:56 $"


set OUTPUT_FORMAT = "MULTIFILE"
#set OUTPUT_FORMAT = "MONOFILE"

#set FILTER_DEC_MIN = -90.0
#set FILTER_DEC_MAX = +90.0
#set FILTER_DEC_MIN = -40.0
#set FILTER_DEC_MAX = -30.0

set MAIN_BASE =  ${HOME}/4most
set EXCLUDE_CATALOGUES = "FALSE"

if ( ! -d $MAIN_BASE ) then
  echo "Error: You MUST have a symbolic link in your home directory called '4most' that points to the 4FS main simulation directory"
  exit 1
endif

#set CATALOGUE_SHORTNAME_LIST = ( "AGN"  "BAO" "GalHaloLR"  "GalDiskLR"  "Clusters"  "GalHaloHR"  "GalDiskHR" "All" )
#set CATALOGUE_CODE_LIST = ( 1 2 3 4 5 6 7 ) 

## get command line args
if ( $#argv == 2 ) then
  set OUTPUT_BASE = "$argv[1]"
  set SIMULATION_CODE_NAME = "$argv[2]"
else if ( $#argv == 3 ) then
  set OUTPUT_BASE = "$argv[1]"
  set SIMULATION_CODE_NAME = "$argv[2]"
  set EXCLUDE_CATALOGUES = "$argv[3]"  
else
#  echo "Usage: INPUT_DIRECTORY SIMULATION_CODE_NAME" 
  echo "Usage: INPUT_DIRECTORY SIMULATION_CODE_NAME '[EXCLUDE_CATALOGUES_FLAG]'"
  exit 1
endif


#we will record pending rsync actions in this file
if ( ! -d ${MAIN_BASE}/sim/ ) mkdir ${MAIN_BASE}/sim/
set RSYNC_TODO_FILE = ${MAIN_BASE}/sim/rsync_todo.log


if ( ! -d $OUTPUT_BASE ) then
  echo "Error: cannot find $OUTPUT_BASE"
  exit 1
endif

if ( ! -d  ${MAIN_BASE}/MPE_out/ ) mkdir ${MAIN_BASE}/MPE_out/
set OUTDIR = ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}
if ( ! -d  $OUTDIR ) then
  mkdir $OUTDIR
endif

if ( ! -d  $OUTDIR ) then
  echo "Error: cannot make $OUTDIR"
  exit 1
endif

###completely new approach - supply everything except a selected list of outputs


#############################################################################################
#Make the output for Cambridge
if ( "TRUE" == "TRUE" ) then
  echo "--------------------------------------------------------------------"
  echo "------------------- Copying To MPE-out -----------------------------"
  echo "------------------- Will take some time ----------------------------"
  echo "  Copying associated files to ${OUTDIR}"

  #use rsync so that we can exclude some files/directories
  if ( "$EXCLUDE_CATALOGUES" == "TRUE" || "$EXCLUDE_CATALOGUES" == "true" || "$EXCLUDE_CATALOGUES" == "yes" || "$EXCLUDE_CATALOGUES" == "YES" ) then
    rsync -aq  --exclude 'field_ranking/' --exclude 'fiber_assignment_files/' --exclude 'fiber_position_files/' --exclude 'progress_reports/progress_report_night_*.txt*' --exclude 'object_summary_file*.txt*' ${OUTPUT_BASE}/ ${OUTDIR}/
  else
    rsync -aq  --exclude 'field_ranking/' --exclude 'fiber_assignment_files/' --exclude 'fiber_position_files/' --exclude 'progress_reports/progress_report_night_*.txt*' ${OUTPUT_BASE}/ ${OUTDIR}/
  endif

  echo "---------------------------------------------------------------------" | tee -a $RSYNC_TODO_FILE
  echo "# Commands written at `date`" >>& $RSYNC_TODO_FILE
  echo "#To copy the output to the rsync area you now need to issue the following command:"                  | tee -a $RSYNC_TODO_FILE
  echo "  rsync -azv --password-file=${HOME}/4most/.4mostsync ${OUTDIR}  4MMPE@4most.mpe.mpg.de::4MMPE-out/" | tee -a $RSYNC_TODO_FILE
  echo "#and do the above but without copying the big catalogues you should issue the following command:"    | tee -a $RSYNC_TODO_FILE
  echo "  rsync -azv --password-file=${HOME}/4most/.4mostsync ${OUTDIR} --exclude 'object_summary_file*.txt*'  4MMPE@4most.mpe.mpg.de::4MMPE-out/" | tee -a $RSYNC_TODO_FILE
  echo "---------------------------------------------------------------------" | tee -a $RSYNC_TODO_FILE
  echo "#To get a listing of the output in the rsync area you should issue the following command:"
  echo "  rsync -v --password-file=${HOME}/4most/.4mostsync 4MMPE@4most.mpe.mpg.de::4MMPE-out/${SIMULATION_CODE_NAME}/"
  echo "---------------------------------------------------------------------"
  echo "#To retreive the output from the rsync area you should issue the following command:"
  echo "  rsync -azv --password-file=${HOME}/4most/.4mostsync 4MMPE@4most.mpe.mpg.de::4MMPE-out/${SIMULATION_CODE_NAME} ./"
  echo "#and do the above but without copying the big catalogues you should issue the following command:"
  echo "  rsync -azv --password-file=${HOME}/4most/.4mostsync --exclude 'object_summary_file*.txt*' 4MMPE@4most.mpe.mpg.de::4MMPE-out/${SIMULATION_CODE_NAME} ./"
  echo "---------------Finished Copying To MPE-out---------------------------"
  echo "---------------------------------------------------------------------"

endif







exit











#
#
#
#
##set EXISTING_FILES = (`sls "${OUTDIR}/${SIMULATION_CODE_NAME}_*_output_source_catalogue.tab.gz"` )
#set EXISTING_FILES = (`/bin/ls -1 ${OUTDIR}/${SIMULATION_CODE_NAME}_*_output_source_catalogue.tab.gz |& gawk '$0!~/No such file or directory/ && $0!~/No match./ {print $0}'` )
#if ( $#EXISTING_FILES > 0 ) then
#  echo "Error: output files already exist - manually remove them like this: "
#  set I = 1 
#  while ( $I <= $#EXISTING_FILES ) 
#    echo "rm $EXISTING_FILES[$I]"
#    @ I ++
#  end
#  exit 1
#endif
#
#
#
#
#
#
##############################################################################################
##Make the output for Cambridge
#if ( "TRUE" == "TRUE" ) then
#  echo "-------------------------------------------------------------------"
#  echo "---------------COPYING TO MPE-out----------------------------------"
#  echo "---------------Will take some time---------------------------------"
#  echo "  Copying associated files to ${OUTDIR}"
#
#  #write the README file
#  echo "-----------------------------------------------------------\
#This directory contains the results of one run of the 4MOST\
#Facility Simulator (4FS). Here is a short description of the\
#simulator output files.\
#\
#Filename                                    Notes\
#------------------------------------------  -----------------------------\
#README                                      This file\
#4FS_input_params.txt                        Copy of the simulator input parameter file\
#FoM_final_results.txt                       File giving final FoM for each DRS, including number of completed targets per DRS\
#FoM_progress.pdf                            Plot of the accumulation of FoM as a function of time\
#fibers_geometry.pdf                         Diagram of the focal plane layout\
#sky_tiling_to_be_executed.pdf               Plot showing the desired depth of coverage over the sky\
#survey_progress_report.mp4                  Video charting the progress of the survey in covering the sky\
#${DRS}_output_source_catalogue.txt.gz       Output target catalogue for the ${DRS} catalogue (one line per input target),\
#                                            where ${DRS} is one of: AGN, BAO, GalHaloLR, GalDiskLR, Clusters, GalHaloHR, GalDiskHR\
#\
#The output target catalogues have the following column format:\
#\
#Column  Format    Name  Description\
#------------------------------------------------------------\
#1       I3         Cat  Internal catalogue code (AGN=1,BAO=2,GalHaloLR=3,GalDiskLR=4,Clusters=5,GalHaloHR=6,GalDiskHR=7)\
#2       I8     LineNum  Line number of this target in the input catalogue\
#3       F6.4    IntPri  Internal object priority (after rescaling etc)\
#4       4I        Nfie  Number of Fields in which this object lies\
#5       4I        Ntil  Number of Tiles in which this object was observed\
#6       4I        Nfib  Total number of fibers that could see this object\
#7       F6.1    Texp_D  Delivered 'dark' exposure time (mins)\
#8       F6.1    Texp_G  Delivered 'grey' exposure time (mins)\
#9       F6.1    Texp_B  Delivered 'bright' exposure time (mins)\
#10      F6.4    FrDone  Fraction of required exposure time that was delivered (>=1.0 indicates a successfully observed target)\
#11+                     A copy of the input columns from the input catalogue\
#\
#A number of sky maps are also provided within the sub-directory 'diagnostic_plots/'\
#\
#Filename                                                    Notes\
#-----------------------------------------------------       -----------------------------\
#input_sky_density_of_targets_${DRS}.png                     Sky map showing sky density of input targets for the ${DRS} catalogue\
#output_sky_density_of_observed_targets_${DRS}.png           Sky map showing sky density of targets that were observed for the ${DRS} catalogue\
#output_sky_density_of_completed_targets_${DRS}.png          Sky map showing sky density of targets that were successfully observed for the ${DRS} catalogue\
#output_sky_density_of_unobserved_targets_${DRS}.png         Sky map showing sky density of targets that were unobserved for the ${DRS} catalogue\
#output_sky_density_fraction_of_completed_targets_${DRS}.png Sky map showing fraction of input targets that were observed for the ${DRS} catalogue\
#output_sky_density_fraction_of_observed_targets_${DRS}.png  Sky map showing fraction of input targets that were successfully observed for the ${DRS} catalogue\
#\
#---\
#For further information please contact T. Dwelly (dwelly@mpe.mpg.de)" > ${OUTDIR}/README
#
#  # copy the input parameters file and plots first
#  if ( -e ${OUTPUT_BASE}/4FS_input_params.txt ) cp -p  ${OUTPUT_BASE}/4FS_input_params.txt ${OUTDIR}/4FS_input_params.txt
#  if ( -e ${OUTPUT_BASE}/fibers_geometry.pdf  ) cp -p  ${OUTPUT_BASE}/fibers_geometry.pdf  ${OUTDIR}/fibers_geometry.pdf
#  if ( -e ${OUTPUT_BASE}/sky_tiling_to_be_executed_graded.pdf ) cp -p  ${OUTPUT_BASE}/sky_tiling_to_be_executed_graded.pdf ${OUTDIR}/sky_tiling_to_be_executed.pdf
##  if ( -e ${OUTPUT_BASE}/quick_check_stats.log ) cp -p ${OUTPUT_BASE}/quick_check_stats.log ${OUTDIR}/quick_check_stats.log
#  if ( -e ${OUTPUT_BASE}/progress_report.mp4 ) cp -p  ${OUTPUT_BASE}/progress_report.mp4 ${OUTDIR}/survey_progress_report.mp4
#  if ( -e ${OUTPUT_BASE}/output_stats/FoM_results.txt ) then 
#    grep -v '^Final_FoM' ${OUTPUT_BASE}/output_stats/FoM_results.txt > ${OUTDIR}/FoM_detailed_results.txt
#    gawk 'BEGIN {printf("#%-11s %10s %10s\n","DRS","FinalFoM","Ncompleted")} /^Final_FoM/ {printf("%-12s %10s %10s\n",$2,$3,$4)}' ${OUTPUT_BASE}/output_stats/FoM_results.txt > ${OUTDIR}/FoM_final_results.txt
#  endif
#  if ( -e ${OUTPUT_BASE}/progress_reports/FoM_progress.pdf ) cp -p ${OUTPUT_BASE}/progress_reports/FoM_progress.pdf ${OUTDIR}/
#  if ( -e ${OUTPUT_BASE}/input_stats/input_texp_histos.pdf ) cp -p ${OUTPUT_BASE}/input_stats/input_texp_histos.pdf${OUTDIR}/
#
#
#  #copy the input sky density plots over as well
#  if ( ! -d ${OUTDIR}/diagnostic_plots/ ) mkdir ${OUTDIR}/diagnostic_plots/
#  foreach C ( $CATALOGUE_CODE_LIST 8 )
#    set CAT = ${CATALOGUE_SHORTNAME_LIST[$C]}
#    set INFILE = "${OUTPUT_BASE}/input_stats/input_sky_density_of_targets_${CAT}.png"
#    if ( -e $INFILE ) cp -p $INFILE ${OUTDIR}/diagnostic_plots/
#  end
#
#  #copy the output sky density plots over as well
#  foreach PLOT_TYPE ( "of_observed_targets" "of_completed_targets" "of_unobserved_targets" "fraction_of_observed_targets" "fraction_of_completed_targets" )  
#    foreach C ( $CATALOGUE_CODE_LIST 8 )
#      set CAT = ${CATALOGUE_SHORTNAME_LIST[$C]}
#      set INFILE = "${OUTPUT_BASE}/output_stats/output_sky_density_${PLOT_TYPE}_${CAT}.png"
#      if ( -e $INFILE ) cp -p $INFILE ${OUTDIR}/diagnostic_plots/
#    end
#  end
#
#  #old way: output is one massive file with all catalogues combined
#  if ( "$OUTPUT_FORMAT" == "MONOFILE" ) then
#    set INFILE = ${OUTPUT_BASE}/object_summary_file.txt
#    if ( -e  ${INFILE} ) then
#      foreach C ( $CATALOGUE_CODE_LIST )
#        set CAT = ${CATALOGUE_SHORTNAME_LIST[$C]}
#        set OUTFILE = "${OUTDIR}/${CAT}_output_source_catalogue.txt.gz"
#        echo "  Making ${OUTFILE}"
#        if ( -e ${OUTFILE} ) rm ${OUTFILE}
#        gawk -v cat_code=$C '$1~/^#/ || $12 == cat_code'   ${INFILE} | gzip - > ${OUTFILE}
#      end
#    else
#      echo "Error: could not find the OpSim results file: ${INFILE}"
#    endif
#  #new way: output is one file per input catalogues
#  else if ( "$OUTPUT_FORMAT" == "MULTIFILE" ) then
#    foreach C ( $CATALOGUE_CODE_LIST )
#     set CAT = ${CATALOGUE_SHORTNAME_LIST[$C]}
##     set INFILE = ${OUTPUT_BASE}/assigned_objects_${CAT}.txt
#     set INFILE = ${OUTPUT_BASE}/object_summary_file_${CAT}.txt
#      if ( -e  ${INFILE} ) then
#        set OUTFILE = "${OUTDIR}/${CAT}_output_source_catalogue.txt.gz"
#        echo "  Making ${OUTFILE}"
#        if ( -e ${OUTFILE} ) rm ${OUTFILE}
#	#some extra debug filtering here to select a subset of fields
#        gawk -v filter_dec_min=$FILTER_DEC_MIN -v filter_dec_max=$FILTER_DEC_MAX  '$1~/^#/ || ($3 >= filter_dec_min && $3 <= filter_dec_max)'   ${INFILE} | gzip - > ${OUTFILE}
##	cat ${INFILE} | gzip - > ${OUTFILE}
#      else
#        echo "Error: could not find the OpSim results file: ${INFILE}"
#      endif
#
#    end
#  else
#    echo "Error, I do not know this type of output format: $OUTPUT_FORMAT"
#    exit 1 
#  endif
#
#
#  echo "#To copy the output to the rsync area you now need to issue the following command:"
#  echo "  rsync -r -v --password-file=${HOME}/4most/.4mostsync ${OUTDIR}  4MMPE@4most.mpe.mpg.de::4MMPE-out/" | tee -a $RSYNC_TODO_FILE
#  echo "-------------------------------------------------------------------"
#
#  echo "-------------------------------------------------------------------"
#  echo "#To retreive the output from the rsync area you should issue the following command:"
#  echo "  rsync -r -v --password-file=${HOME}/4most/.4mostsync 4MMPE@4most.mpe.mpg.de::4MMPE-out/${SIMULATION_CODE_NAME} ./"
#  echo "-------------------------------------------------------------------"
#
#endif
##############################################################################################
#
#
#
#
#
#
#
#
#
#exit
