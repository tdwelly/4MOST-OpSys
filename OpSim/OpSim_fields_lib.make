# This is the makefile for OpSim_fields_lib.c
include ../arch.make

COMMON_DIR = ../common

IDIR = -I./ -I$(COMMON_DIR)/ -I$(HEALPIX_PATH)/include -I$(CFITSIO_PATH)/include

OUTSTEM = OpSim_fields_lib

OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))

DEP_LIBS = $(COMMON_DIR)/utensils_lib 4FS_OpSim  OpSim_OB_lib

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) $(COMMON_DIR)/define.h


#No main() so only make object library

$(OFILE)	:	$(DEP_HFILES) $(CFILE)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE)

clean   : 
	rm -f $(OFILE)


#end
