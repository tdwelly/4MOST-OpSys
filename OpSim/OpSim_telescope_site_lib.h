//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_telescope_site_lib.h
//--    Use: This is a header for the OpSim_telescope_site_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_TELESCOPE_SITE_H
#define OPSIM_TELESCOPE_SITE_H

//#define CVS_REVISION_OPSIM_TELESCOPE_SITE_LIB_H "$Revision: 1.14 $"
//#define CVS_DATE_OPSIM_TELESCOPE_SITE_LIB_H     "$Date: 2015/08/18 11:44:16 $"


#include <stdio.h>
#include "define.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.14 $"
//#define CVS_DATE_H     "$Date: 2015/08/18 11:44:16 $"
inline static int OpSim_telescope_site_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

#define TELESCOPE_CODE_VISTA   1
#define TELESCOPE_CODE_NTT     2
#define TELESCOPE_CODE_UKST    3

#define ENVIRON_MAX_SEEING_GRADES 16   ///normally no more than 7 are needed

#define CLOUD_COVER_CODE_PHOTOMETRIC 1
#define CLOUD_COVER_CODE_CLEAR       2
#define CLOUD_COVER_CODE_THIN        3
#define CLOUD_COVER_CODE_THICK       4
#define CLOUD_COVER_CODE_BAD         5

#define CLOUD_COVER_NAME_PHOTOMETRIC "PHOTO"
#define CLOUD_COVER_NAME_CLEAR       "CLEAR"
#define CLOUD_COVER_NAME_THIN        "THIN"
#define CLOUD_COVER_NAME_THICK       "THICK"
#define CLOUD_COVER_NAME_BAD         "BAD"

#define TELESCOPE_SITE_DARK_SKY_BRIGHTNESS_VISTA 21.61 /// this from Patat. F., (2003), A&A, 400, 1183, "UBVRI night sky brightness during sunspot maximum at ESO-Paranal".
#define TELESCOPE_SITE_DARK_SKY_BRIGHTNESS_NTT   21.7  /// this from Table 5, Patat. F., (2003), A&A, 400, 1183, "UBVRI night sky brightness during sunspot maximum at ESO-Paranal".
#define TELESCOPE_SITE_DARK_SKY_BRIGHTNESS_UKST  21.5  /// this guessed

#define TELESCOPE_SITE_EXTINCTION_VISTA          0.11 /// this is from the ESO ETC documentation http://www.eso.org/observing/etc/doc/elt/etc_img_model.pdf
#define TELESCOPE_SITE_EXTINCTION_NTT            0.15 /// this is a guess - TODO
#define TELESCOPE_SITE_EXTINCTION_UKST           0.20 /// this is a guess - TODO

typedef struct {
    //Environmental effects
  float cloud_fraction_photometric;
  float cloud_fraction_clear;
  float cloud_fraction_thin;
  float cloud_fraction_thick;
  float cloud_fraction_closed;

//  //old way of specifying the seeing grades
//  float seeing_fraction_good;
//  float seeing_fraction_medium;
//  float seeing_fraction_bad;
//  float seeing_fraction_unusable;
  //new way of specifying the seeing grades
  int   seeing_num_grades;
  float seeing_probability[ENVIRON_MAX_SEEING_GRADES];
  float seeing_cumulative_fraction[ENVIRON_MAX_SEEING_GRADES];
  float seeing_thresh_low[ENVIRON_MAX_SEEING_GRADES];
  float seeing_thresh_high[ENVIRON_MAX_SEEING_GRADES];

  float moon_fraction_dark;
  float moon_fraction_grey;
  float moon_fraction_bright;

  float wind_fraction_constrained;
  float wind_fraction_dome_closed;
  float wind_fraction_nominal;

} environment_struct;


/////////////////////////////////////////////////////////////////////////////
// This structure contains all the parameters for a particular Telescope/site
typedef struct {
  char   code_name[STR_MAX];
  int    code;
  float  plate_scale;            /// in units of mm/arcsec
  float  invplate_scale;         /// in units of arcsec/mm

  float  fov_radius_deg;         /// degrees
  float  fov_radius_mm;          /// mm
  float  fov_area_deg;
  char   str_longitude[STR_MAX];
  char   str_latitude[STR_MAX];
  double longitude_deg;          /// decimal degrees 
  double latitude_deg;           /// decimal degrees 
  double sin_latitude;
  double cos_latitude;        

  double altitude;               /// metres

  float  extinction_coeff;       /// mag/airmass in V band
  float  dark_sky_brightness;    /// zenith sky brightness in V-band in AB mags/arcsec2, when no moon.
  float  Bzen;                   /// zenith sky brightness in V-band in nanoLamberts, when no moon.

  
  environment_struct environ;    /// The structure containing the environmental parameters for this telescope site.
  
} telescope_struct;
/////////////////////////////////////////////////////////////////////////////

int    OpSim_telescope_site_print_version     (FILE* pFile);
int    telescope_struct_init                  (telescope_struct **ppTele);
int    telescope_struct_free                  (telescope_struct **ppTele);
int    telescope_set_site                     (telescope_struct *pTele, const char *str_tele );

float  weather_seeing_at_airmass              (float seeing_zen, float airmass);
float  weather_seeing_at_zenith               (float seeing_airmass, float airmass);
float  weather_IQ_prob                        (environment_struct *pEnviron, float airmass, float IQ);
int    weather_random_zenith_IQ               (environment_struct *pEnviron, float *pResult);
int    weather_random_cloud                   (environment_struct *pEnviron, int *pCode, const char **ppCodename);

//float  weather_calc_sky_brightness            (timeline_struct *pTime, double ra, double dec);
//float  weather_calc_relative_sky_brightness   (timeline_struct *pTime, double moon_dist);

#endif
