//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_FoMs_lib.h
//--    Use: This is a header for the OpSim_FoMs_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_FOMS_H
#define OPSIM_FOMS_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <time.h>

#include "define.h"
#include "4FS_OpSim.h"
//#include "OpSim_positioner_lib.h"
#include "utensils_lib.h"


#define STR_MAX DEF_STRLEN

//-----------------Info for printing code revision-----------------//
inline static int OpSim_FoMs_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//-----------------Info for printing code revision-----------------//



#define OPSIM_FOMS_FOM_S1_PIXEL_SIZE 2.0
#define OPSIM_FOMS_FOM_S1_MIN_PER_PIXEL  1  //arbitary number made up by me
#define OPSIM_FOMS_FOM_S1_AREA_GOAL      10000.0 //square degrees
#define OPSIM_FOMS_FOM_S1_FRACTION_GOAL  0.8 //

#define OPSIM_FOMS_FOM_S1_HPX_NSIDE 32


//These are numbers from M2 documentation, same as before apart from NUM_MIN_ESN
// #define OPSIM_FOMS_FOM_S3_NUM_ESN        13000000      
// #define OPSIM_FOMS_FOM_S3_NUM_MIN_ESN     2300000      //2600000      //
// #define OPSIM_FOMS_FOM_S3_NUM_GOAL_ESN    3900000      //
// #define OPSIM_FOMS_FOM_S3_NUM_DB1         5800000      //
// #define OPSIM_FOMS_FOM_S3_NUM_MIN_DB1     3500000      //
// #define OPSIM_FOMS_FOM_S3_NUM_GOAL_DB1    4640000      //
// #define OPSIM_FOMS_FOM_S3_NUM_DB2         2500000      //
// #define OPSIM_FOMS_FOM_S3_NUM_MIN_DB2     1500000      //
// #define OPSIM_FOMS_FOM_S3_NUM_GOAL_DB2    2000000      //
// #define OPSIM_FOMS_FOM_S3_NUM_DB3          640000      //
// #define OPSIM_FOMS_FOM_S3_NUM_MIN_DB3      380000      //
// #define OPSIM_FOMS_FOM_S3_NUM_GOAL_DB3     640000      //
// #define OPSIM_FOMS_FOM_S3_NUM_DB4          840000      //
// #define OPSIM_FOMS_FOM_S3_NUM_MIN_DB4      670000      //
// #define OPSIM_FOMS_FOM_S3_NUM_GOAL_DB4     840000      //
// #define OPSIM_FOMS_FOM_S3_NUM_TYPES             5      //

// New values from S3 document: Requirements_S3_20March.docx
#define OPSIM_FOMS_FOM_S3_NUM_MIN_ESN       2500000      //
#define OPSIM_FOMS_FOM_S3_NUM_GOAL_ESN      4500000      //
#define OPSIM_FOMS_FOM_S3_NUM_MIN_DYNDISK1  3500000      //
#define OPSIM_FOMS_FOM_S3_NUM_GOAL_DYNDISK1 4500000      //
#define OPSIM_FOMS_FOM_S3_NUM_MIN_DYNDISK2   150000      //
#define OPSIM_FOMS_FOM_S3_NUM_GOAL_DYNDISK2  284517      //
#define OPSIM_FOMS_FOM_S3_NUM_MIN_CHEMDISK  1500000      //
#define OPSIM_FOMS_FOM_S3_NUM_GOAL_CHEMDISK 2500000      //
#define OPSIM_FOMS_FOM_S3_NUM_MIN_BULGE      800000      //
#define OPSIM_FOMS_FOM_S3_NUM_GOAL_BULGE    1000000      //



//These are from M2 selection report
// #define OPSIM_FOMS_FOM_S4_NUM_D         2092482        
// #define OPSIM_FOMS_FOM_S4_NUM_MIN_D     ((int)(OPSIM_FOMS_FOM_S4_NUM_D*0.50))        //
// #define OPSIM_FOMS_FOM_S4_NUM_GOAL_D    ((int)(OPSIM_FOMS_FOM_S4_NUM_D*0.70))        //
// #define OPSIM_FOMS_FOM_S4_NUM_B          245839        //
// #define OPSIM_FOMS_FOM_S4_NUM_MIN_B     ((int)(OPSIM_FOMS_FOM_S4_NUM_B*0.50))        //
// #define OPSIM_FOMS_FOM_S4_NUM_GOAL_B    ((int)(OPSIM_FOMS_FOM_S4_NUM_B*0.70))        //
// #define OPSIM_FOMS_FOM_S4_NUM_H          725250        //
// #define OPSIM_FOMS_FOM_S4_NUM_MIN_H           0        //
// #define OPSIM_FOMS_FOM_S4_NUM_GOAL_H     OPSIM_FOMS_FOM_S4_NUM_H //((int)(OPSIM_FOMS_FOM_GALDISKHR_NUM_H*0.70))
// #define OPSIM_FOMS_FOM_S4_NUM_TYPES           3        //


// These are new values for round9
// FromIWG2wiki: The Mock catalogue contains about 4.4M targets out of which
// FromIWG2wiki: ~1.7M are outer disk (SUB_CAT_ID=1),
// FromIWG2wiki: ~1.7M are inner disk (SUB_CAT_ID=2),
// FromIWG2wiki: and ~0.8M are bulge (SUB_CAT_ID=3).
// FromIWG2wiki: Nmin for each of these are 0.6M, 0.6M, and 0.3M respectively,
// FromIWG2wiki: and Ngoal are 0.8M, 0.8M, and 0.4M, respectively.
// #define OPSIM_FOMS_FOM_S4_NUM_MIN_OUTERDISK     600000
// #define OPSIM_FOMS_FOM_S4_NUM_GOAL_OUTERDISK    800000
// #define OPSIM_FOMS_FOM_S4_NUM_MIN_INNERDISK     600000
// #define OPSIM_FOMS_FOM_S4_NUM_GOAL_INNERDISK    800000
// #define OPSIM_FOMS_FOM_S4_NUM_MIN_BULGE         300000
// #define OPSIM_FOMS_FOM_S4_NUM_GOAL_BULGE        400000

// FromIWG2wiki:  Update 2016-08-15:
// FromIWG2wiki:  FoM = 1/4 (FoM1 + FoM2 + FoM3 + FoM4)
// FromIWG2wiki:  where
// FromIWG2wiki:  FoMi = Nobs,i / Ngoal,i
// FromIWG2wiki:
// FromIWG2wiki:  Update 2016-08-15:
// FromIWG2wiki:  Ngoal for the different sub-surveys are:
// FromIWG2wiki:  Ngoal,1=1.21M,
// FromIWG2wiki:  Ngoal,2=1.32M,
// FromIWG2wiki:  Ngoal,3=0.28M,
// FromIWG2wiki:  Ngoal,4=1.45M.
// FromIWG2wiki:  The Mock catalogue contains about 4.4M targets
// Note that:  gump ~/4most/survey_team_inputs/S4/S4_GalDiskHR_latest_input_cat.fits SUB_CAT_ID no | gawk '//{n[$0]++;} END {for (r in n){print r,n[r]}} '
// Note that:          1  1211107
// Note that:          2  1322312
// Note that:          3  288587
// Note that:          4  1457712
// To avoid ambiguity of what happens when Nobs,i > Ngoal,i we will use the exact numbers of objects in each catalogue here

#define OPSIM_FOMS_FOM_S4_NUM_GOAL_BULGE      1211107 
#define OPSIM_FOMS_FOM_S4_NUM_GOAL_INNERDISK  1322312
#define OPSIM_FOMS_FOM_S4_NUM_GOAL_OUTERDISK   288587
#define OPSIM_FOMS_FOM_S4_NUM_GOAL_UNBIASED   1457712


#define OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS  5
#define OPSIM_FOMS_FOM_S5_NUM_GALS_FOR_SUCCESS  4
#define OPSIM_FOMS_FOM_S5_REDSHIFT_THRESH  0.7  //at z>thresh a single completed galaxy gives success for the cluster

//the choice of dec limits is important
#define OPSIM_FOMS_FOM_S6_NUMERATOR_DEC_MIN  -70.0
#define OPSIM_FOMS_FOM_S6_NUMERATOR_DEC_MAX   +20.0
#define OPSIM_FOMS_FOM_S6_DENOMINATOR_DEC_MIN  -70.0
#define OPSIM_FOMS_FOM_S6_DENOMINATOR_DEC_MAX   +20.0
#define OPSIM_FOMS_FOM_S6_MIN_MAG  16.0
#define OPSIM_FOMS_FOM_S6_NUM_MAG_BINS  7
#define OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS 5


//#define OPSIM_FOMS_FOM_S7_EXPONENT  2.0
#define OPSIM_FOMS_FOM_S7_EXPONENT  3.0  //changed on 13.09.2016 at request of Aaron Robotham.



// //new definition from phase A.
// #define OPSIM_FOMS_FOM_S8_PIXEL_SIZE 1.0
// #define OPSIM_FOMS_FOM_S8_NUM_DENSITY_LEVELS 4
// #define OPSIM_FOMS_FOM_S8_AREA_MIN   16.0e3
// #define OPSIM_FOMS_FOM_S8_AREA_MAX   20.0e3
// #define OPSIM_FOMS_FOM_S8_NUM_MIN    10.0e6
// #define OPSIM_FOMS_FOM_S8_NUM_MAX    20.0e6

// see update to S8 FoM definition 10/09/2016
// /home/tdwelly/4most/survey_team_inputs/S8/info_2016-09-10/FOM.txt
#define OPSIM_FOMS_FOM_S8_FOM1_MIN    5.0e3
#define OPSIM_FOMS_FOM_S8_FOM1_MAX   15.0e3
#define OPSIM_FOMS_FOM_S8_FOM2_MIN    7.0e6
#define OPSIM_FOMS_FOM_S8_FOM2_MAX   15.0e6
#define OPSIM_FOMS_FOM_S8_MIN_DENISTY_PER_SQDEG 1000.0  //
#define OPSIM_FOMS_FOM_S8_HPX_NSIDE 128




// http://wiki.4most.eu/drs9-magellanic-clouds
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET1  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET2  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET3  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET4  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET5  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET6  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET7  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET8  0.5
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET9  0.5
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET10 0.0


#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET1  0.04
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET2  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET3  1.0
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET4  0.1
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET5  0.1
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET6  0.06
#define OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET7  0.06
  //TODO - fix this properly



#define OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,name,FoM,ncompleted,ntotal,fiber_hours) fprintf((pFile),"Final_FoM %-10s %10.6f %10d %10d %10.7g\n",(name),(FoM),(ncompleted),(ntotal),(fiber_hours))


int OpSim_FoMs_print_version         (FILE* pFile);

//--------------------------------FoMs----------------------------------------------------//
int calc_FoM_simple_headcount    (objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile, int num_required);
//int calc_FoM_S1_old              (survey_struct* pSurvey, objectlist_struct *pObjList,
//                                  catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S1                  (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S2                  (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S3                  (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S3_subcat           (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S4                  (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S5                  (survey_struct* pSurvey, objectlist_struct *pObjList,
                                  catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S6                  (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S7                  (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S8                  (survey_struct* pSurvey, objectlist_struct *pObjList,
                                  catalogue_struct *pCat, FILE *pFile);
int calc_FoM_S9                  (objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile);
int calc_FoM_EXTRA               (objectlist_struct *pObjList, catalogue_struct *pCat, FILE *pFile);
//float calc_modified_FoM_S3_S4    (FILE* pFile, int Nok, int Nmin, int Ngoal, int Ntotal,
//                                  const char* str_name, const char *str_subcat );

int calc_fiber_hours_completeness(objectlist_struct* pObjList, catalogue_struct* pCat) ;
int calc_FoMs                    (survey_struct* pSurvey, objectlist_struct *pObjList,
                                  cataloguelist_struct *pCatList, const char *str_filename, BOOL verbose);
int plot_FoM_progress            (cataloguelist_struct *pCatList, survey_struct *pSurvey, const char* str_dirname );
//--------------------------------end-FoMs-----------------------------------------------//


#endif
