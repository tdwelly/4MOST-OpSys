#!/bin/tcsh -f

# CVS_REVISION "$Revision: 1.2 $"
# CVS_DATE     "$Date: 2013/01/30 10:50:46 $"

#use the methods of http://www2.research.att.com/~njas/icosahedral.codes/
#remember to refer to this usage restriction:

# "If you use any of these arrangements, please acknowledge this source. 
# For example, you might say something like the following.
# This arrangement of points is taken from Reference [1], where Reference [1] is:
# R. H. Hardin, N. J. A. Sloane and W. D. Smith, Tables of spherical codes with icosahedral symmetry, 
# published electronically at http://www.research.att.com/~njas/icosahedral.codes/"

# standard outputs from icover.sh are are x,y,z coords of on the unit sphere
# format is one coordinate per line
# need to reformat to make readable and useful.

# Icosahedral best coverings
# dim numpts  deepest hole (or covering radius, in degrees)
#   3   4092   1.988228662936 15,8
#   3   6912   1.528963079847 19,11
#   3   7002   1.519288003357 20,10
#   3   7682   1.449774552065 16,16
#   3   8192   1.403881943807 18,15
#   3   8192   1.405300351294 27,3
#   3   8672   1.364413944909 17,17
#   3   9722   1.288545767384 18,18
# ....  
#   3  10832   1.220670083820 19,19
#   3  12002   1.159587155975 20,20
#   3  13232   1.104325930573 21,21
#   3  14522   1.054091991326 22,22
# ...
#   3  21872   0.858770306968 27,27

#the whole 4MOST sky:
set RA_MIN = "0.0"
set RA_MAX = "360.0"
set DEC_MIN = "-75.0"
set DEC_MAX = "+25.0"

#do some reformatting and make region files
set CODE_BASE = ${HOME}/my_4most/code
set BASE = ${HOME}/4most/sky_tiling_patterns/hardin-sloane
cd $BASE

if ( $#argv == 1 ) then
  set TILING_NUM_POINTS = ${argv[1]}
else
  echo "Error, Useage: make_hardin.csh TILING_NUM_POINTS"
  exit 1
endif


  # we have been supplied with a Hardin number
if ( `gawk -v tnp=$TILING_NUM_POINTS 'BEGIN {flag="FALSE"} $2 == tnp {flag="TRUE";exit} END {print flag}' ${BASE}/hardin_coverings_list.txt`  == "TRUE" ) then
  set HARDIN_FILE = "${BASE}/pointings_hardin${TILING_NUM_POINTS}.txt"
  if ( ! -e $HARDIN_FILE ) then
    cd ${CODE_BASE}/hardin/
    ./icover.sh $TILING_NUM_POINTS > $HARDIN_FILE 
    cd $BASE
  else
  endif
else
  echo "Error, the Hardin et al. pattern $TILING_NUM_POINTS does not exist"
  exit 1
endif

#test the Hardin file validity
if ( ! -e $HARDIN_FILE ) then
  echo "Error, the Hardin et al. pattern file $HARDIN_FILE does not exist"
  exit 1
else if ( `egrep -c 'header number not followed by blank' $HARDIN_FILE` > 0 ) then
  echo "Error, the Hardin et al. pattern file $HARDIN_FILE is bad:"
#  rm $HARDIN_FILE
  exit 1
else if ( `cat $HARDIN_FILE | wc -l` < $TILING_NUM_POINTS ) then
  echo "Error, the Hardin et al. pattern file $HARDIN_FILE is bad"
  rm $HARDIN_FILE
  exit 1
else
endif


if ( -e ${HARDIN_FILE} ) then
#  gawk -f ~/.gawkrc --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){printf("%05d %12.7f %12.7f %12.7f\n", i, x[i],y[i],z[i])}}' ${HARDIN_FILE} > ${HARDIN_FILE:r}_unitsphere.txt

  #useful to also calc ra and dec at same time
#  gawk -f ~/.gawkrc --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],sqrt((x[i]*x[i])+(y[i]*y[i])+(z[i]*z[i])));ra=(phi*180./pi)+180.; dec=(theta*180./pi)-90.;printf("%05d %12.7f %12.7f %12.7f %12.7f %12.7f\n", i, x[i],y[i],z[i], ra, dec)}}' ${HARDIN_FILE} > ${HARDIN_FILE:r}_unitsphere.txt
  gawk -f ~/.gawkrc --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],1.0);ra=(phi*180./pi)+180.; dec=90.0 - (theta*180./pi);printf("%05d %12.7f %12.7f %12.7f %12.7f %12.7f\n", i, x[i],y[i],z[i], ra, dec)}}' ${HARDIN_FILE} > ${HARDIN_FILE:r}_unitsphere.txt


#  gawk -f ~/.gawkrc -v ra_min=$RA_MIN -v dec_min=$DEC_MIN -v ra_max=$RA_MAX -v dec_max=$DEC_MAX --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],sqrt((x[i]*x[i])+(y[i]*y[i])+(z[i]*z[i])));ra=(phi*180./pi)+180.; dec=(theta*180./pi)-90.;if(ra>=ra_min&&dec>=dec_min&&ra<=ra_max&&dec<=dec_max) {printf("%05d %12.6f %12.6f\n", i, ra, dec)}}}' ${HARDIN_FILE} > ${HARDIN_FILE:r}_format_4MOST.txt
  gawk -f ~/.gawkrc -v ra_min=$RA_MIN -v dec_min=$DEC_MIN -v ra_max=$RA_MAX -v dec_max=$DEC_MAX --source '// {i=$1;ra=$5;dec=$6;if(ra>=ra_min&&dec>=dec_min&&ra<=ra_max&&dec<=dec_max) {printf("%05d %12.6f %12.6f\n", i, ra, dec)}}' ${HARDIN_FILE:r}_unitsphere.txt > ${HARDIN_FILE:r}_format_4MOST.txt

  gawk '//{printf("fk5; cross point %12s %12s\n", $2, $3)}' ${HARDIN_FILE:r}_format_4MOST.txt > ${HARDIN_FILE:r}.reg

#  gawk -f ~/.gawkrc -v ra_min=0 -v dec_min=-90 -v ra_max=360 -v dec_max=+90 --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],sqrt((x[i]*x[i])+(y[i]*y[i])+(z[i]*z[i])));ra=(phi*180./pi)+180.; dec=(theta*180./pi)-90.;if(ra>=ra_min&&dec>=dec_min&&ra<=ra_max&&dec<=dec_max) {printf("%05d %12.6f %12.6f\n", i, ra, dec)}}}' ${HARDIN_FILE} > ${HARDIN_FILE:r}_format.txt

  gawk -f ~/.gawkrc -v ra_min=0.0 -v dec_min=-90.0 -v ra_max=360.0 -v dec_max=+90.0 --source '// {i=$1;ra=$5;dec=$6;if(ra>=ra_min&&dec>=dec_min&&ra<=ra_max&&dec<=dec_max) {printf("%05d %12.6f %12.6f\n", i, ra, dec)}}' ${HARDIN_FILE:r}_unitsphere.txt > ${HARDIN_FILE:r}_format.txt

endif

  echo "Hardin file for $TILING_NUM_POINTS points is at ${HARDIN_FILE:r}_format.txt" 

exit


