//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_html_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.4 $"
//#define CVS_DATE     "$Date: 2015/11/16 17:07:35 $"
//--    Note: Definitions are in the corresponding header file OpSim_html_lib.h
//--    Description:
//--      Used to produce HTML output from the 4FS OpSim
//--

#include "OpSim_html_lib.h"
#include "OpSim_statistics_lib.h"


#define ERROR_PREFIX   "#-OpSim_html_lib.c:Error  :"
#define WARNING_PREFIX "#-OpSim_html_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_html_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_html_lib.c:DEBUG  :"
#define RESULTS_PREFIX "#-OpSim_html_lib.c:RESULTS:"


////////////////////////////////////////////////////////////////////////
//functions


int OpSim_html_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------------------//
//write a handy html summary page
int OpSim_html_write_summary_page (survey_struct *pSurvey,
                                   focalplane_struct *pFocal,
                                   param_list_struct *pParamList,
                                   cataloguelist_struct *pCatList,
                                   const char* str_filename)
{
  int c;
  int total_num_objects = 0;
  FILE *pFile = NULL;
  char str_cwd[STR_MAX];
  if ( pSurvey == NULL || pFocal == NULL || pParamList == NULL || pCatList == NULL ) return 1;

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  //get the working directory path
  if ( getcwd((char *) str_cwd, (size_t) STR_MAX) == NULL )
  {
    fprintf(stderr, "%s Could not get CWD at at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  fprintf(stdout, "%s Writing HTML summary file to: file://%s/%s\n",
          COMMENT_PREFIX, str_cwd, str_filename); fflush(stdout);

  //write HTML header 
  fprintf(pFile, "<!DOCTYPE html>\n\
<html>\n\
<head>\n\
  <link rel='stylesheet' type='text/css' href='https://www.mpe.mpg.de/~4most/css/4FS.css'>\n\
  <title>4FS OpSim results summary page</title>\n\
  <meta name='description' content='A simple summary page detailing one run of the 4MOST Facility Simulator Operations Simulator (4FS OpSim)' />\n\
  <meta http-equiv='Content-type' content='text/html;charset=UTF-8' />\n\
  <link rel='icon' type='img/gif' href='http://www.mpe.mpg.de/~4most/icons/4MOSTlogo_icon.gif' />\n\
</head>\n");

  fprintf(pFile, "<body>\n\
<h1>\n\
  <img src='http://www.mpe.mpg.de/~4most/icons/4MOSTLogo.png' height='120' alt='4MOST logo' style='float:left;' />\n\
4FS OpSim results summary page\n\
  <img src='http://www.mpe.mpg.de/~4most/icons/logo-mpe-transparent.gif' height='100' alt='MPE logo' style='float:right;' />\n\
<br><hr><br>\n");

  //============Title etc============//
  /*  {
    time_t rawtime;
    struct tm * timeinfo;
    struct utsname unameinfo;
    char str_hostname[STR_MAX] = {'\0'};
    //    char str_domainname[STR_MAX] = {'\0'};
    
    if ( time ( &rawtime ) < 0) return 1;
    timeinfo = localtime ( &rawtime );

    if ( gethostname((char*) str_hostname, (size_t) STR_MAX)) return 1;
    //    if ( getdomainname((char*) str_domainname, (size_t) STR_MAX)) return 1;
    if ( uname((struct utsname*) &unameinfo)) return 1;

    
    fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "General parameters", OPSIM_OUTPUT_HTML_GUIDE, "GeneralParameters");
        fprintf(pFile, "<table>\n             \
  <tr class='header-row'>\n\
    <td>Name</td>\n\
    <td>Value</td>\n\
  </tr>\n\
  <tr>\n\
    <td>Simulation name</td>\n\
    <td>%s</td>\n\
  </tr>\n\
  <tr>\n\
    <td>User-supplied comments</td>\n\
    <td>%s</td>\n\
  </tr>\n\
  <tr>\n\
    <td>Simulation finish date/time</td>\n\
    <td>%s</td>\n\
  </tr>\n\
  <tr>\n\
    <td>Simulation run on host</td>\n\
    <td>%s (%s %s %s %s)</td>\n\
  </tr>\n\
</table>\n",
            pSurvey->sim_code_name,
            pSurvey->sim_comments,
            asctime(timeinfo),
            str_hostname, unameinfo.sysname, unameinfo.release, unameinfo.version, unameinfo.machine);
  }
    */  
    /*
<h3>Simulation name:</h3>\n\
<p>%s</p><br>\n\
<h3>Simulation finish date/time:</h3>\n\
<p>%s</p>\n\
<h3>Simulation run on host:</h3>\n\
<p>%s (%s %s %s %s)</p>\n",
            pSurvey->sim_code_name,
            asctime(timeinfo),
            str_hostname, 
            unameinfo.sysname, unameinfo.release, unameinfo.version, unameinfo.machine);
  }

  fprintf(pFile, "<h3>User-supplied comments: </h3>\n\
<p>%s</p>\n", pSurvey->sim_comments);

    */
    //============Title etc============//

  //============Input files/parameters============//
  {
    char buffer[STR_MAX] = {'\0'};
    if ( ParLib_get_param_str_value ((param_list_struct*) pParamList, (const char*) "PARAM_FILENAME", (char*) buffer) > 0 ) return 1;
    fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Input files/parameters", OPSIM_OUTPUT_HTML_GUIDE, "InputFilesParameters");
    fprintf(pFile,"\
<a href='%s' target='_blank' type='text/plain' \
  title='The input parameter file that was supplied to the OpSim'>Full Input Parameter file</a><br>\n\
<a href='%s' target='_blank' type='text/plain' \
  title='The input tiling description file that was supplied to the OpSim'>Input tiling description file</a><br>\n\
<a href='%s' target='_blank' type='text/plain' \
  title='The values of the full set of input parameters after interpretation by the OpSim'>Input parameter values after interpretation</a>\n\
<br><br>\n",
            buffer,
            pSurvey->str_tiling_description_file,
            "OpSim_interpreted_parameters.txt");
  }

  fprintf(pFile, "<table  >\n\
  <tr class='header-row'>\n\
    <td>Selected Parameter Name</td>\n\
    <td>Value</td>\n\
    <td>Units</td>\n\
  </tr>\n");

  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "SIM.CODE_NAME",                       "",  ""))      return 1;
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "SIM.COMMENTS",                        "",  ""))      return 1;
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "TELESCOPE.CODE_NAME",                 "",  ""))      return 1;
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "TELESCOPE.FOV_AREA",                  "",  "deg<sup>2</sup>")) return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "SURVEY.JD_START",                     "",  "days"))  return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "SURVEY.DURATION",                     "",  "years")) return 1; 
  fprintf(pFile, "</table><br>\n");

  {
    time_t rawtime;
    struct tm * timeinfo;
    struct utsname unameinfo;
    char str_hostname[STR_MAX] = {'\0'};
    //    char str_domainname[STR_MAX] = {'\0'};
    
    if ( time ( &rawtime ) < 0) return 1;
    timeinfo = localtime ( &rawtime );

    if ( gethostname((char*) str_hostname, (size_t) STR_MAX)) return 1;
    //    if ( getdomainname((char*) str_domainname, (size_t) STR_MAX)) return 1;
    if ( uname((struct utsname*) &unameinfo)) return 1;
    
    fprintf(pFile, "<table>\n\
  <tr class='header-row'>\n\
    <td>System Parameter</td>\n\
    <td>Value</td>\n\
  </tr>\n\
  <tr>\n\
    <td>Software version</td>\n\
    <td>%s %s</td>\n\
  </tr>\n\
  <tr>\n\
    <td>Simulation finish date/time</td>\n\
    <td>%s</td>\n\
  </tr>\n\
  <tr>\n\
    <td>Simulation run on host</td>\n\
    <td>%s (%s %s %s %s)</td>\n\
  </tr>\n\
</table>\n",
            GIT_VERSION, GIT_DATE,
            asctime(timeinfo),
            str_hostname, unameinfo.sysname, unameinfo.release, unameinfo.version, unameinfo.machine);
  }
  //============Input files/parameters============//
 
  
  //============Focal Plane details============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Focal Plane details", OPSIM_OUTPUT_HTML_GUIDE, "FocalPlaneDetails");
  fprintf(pFile, "<table  >\n\
  <tr class='header-row'>\n\
    <td>Selected Parameter Name</td>\n\
    <td>Value</td>\n\
    <td>Units</td>\n\
  </tr>\n");
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.CODE_NAME",                "",  ""))      return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.FAMILY",                   "",  ""))      return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.PATROL_RADIUS_MAX",        "",  "mm"))    return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.PATROL_RADIUS_MIN",        "",  "mm"))    return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.LAYOUT_FILENAME",          "",  ""))      return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.MINIMUM_SEPARATION",       "",  "mm"))    return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.SPACING_LOW_RES",          "",  "mm"))    return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.SPACING_HIGH_LOW_PATTERN", "",  ""))      return 1; 
  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "POSITIONER.SPACING_HIGH_LOW_RATIO",   "",  ""))      return 1; 
  fprintf(pFile, "</table><br>\n");

  fprintf(pFile, "\
<table   style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Fiber resolution</td>\n\
    <td>Nfibers</td>\n\
    <td colspan='2'>Min. Sky fibers</td>\n\
    <td>Min. Community fibers</td>\n\
    <td colspan='2'>Focal Plane Layout</td>\n\
  </tr>\n\
  <tr class='header-row'>\n\
    <td></td>\n\
    <td>per Tile</td>\n\
    <td>per Tile</td>\n\
    <td>per Sector</td>\n\
    <td>per Tile</td>\n\
    <td>Plot</td>\n\
    <td>Data</td>\n\
   </tr>\n");
  
  fprintf(pFile, "  <tr>\n\
    <td>High res</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td rowspan='3'><a href='%s/%s.pdf' target='_top'   type='application/pdf' title='The layout of positioners over the focal plane - plot'>\n\
      <img src='%s/%s_thumb.png' alt='A plot' /></a></td>\n\
    <td rowspan='3'><a href='%s/%s.txt' target='_blank' type='text/plain'      title='The layout of positioners over the focal plane - datafile'>.txt</a></td>\n\
  </tr>\n",
          pFocal->num_fibers_hi,
          pSurvey->minimum_sky_fibers_high_res,
          pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_HIGH],
          pSurvey->minimum_PI_fibers_high_res,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM);
  
  fprintf(pFile, "  <tr>\n\
    <td>Low res</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
  </tr>\n",
          pFocal->num_fibers_lo,
          pSurvey->minimum_sky_fibers_low_res,
          pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_LOW],
          pSurvey->minimum_PI_fibers_low_res);

  fprintf(pFile, "  <tr class='special-row'>\n\
    <td>Total</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
    <td>%d</td>\n\
  </tr>\n",
          pFocal->num_fibers,
          pSurvey->minimum_sky_fibers_high_res+pSurvey->minimum_sky_fibers_low_res,
          (pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_HIGH]+
           pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_LOW]),
          pSurvey->minimum_PI_fibers_high_res+ pSurvey->minimum_PI_fibers_low_res);

  fprintf(pFile, "</table>\n");

  fprintf(pFile,"<br>\n\
The focal plane has %d sector%s\n\
<br><br>\n", pFocal->num_sectors, (pFocal->num_sectors==1?"":"s"));
  //============Focal Plane details============//


  //============Input catalogues============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Input Catalogues", OPSIM_OUTPUT_HTML_GUIDE, "InputCatalogues");
  fprintf(pFile, "\
<table   style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Survey/Catalogue name</td>\n\
    <td title='The original path+filename of the input catalogue that was supplied to the OpSim (not generally supplied with output dataset)'>Input<br>Catalogue</td>\n\
    <td>File Size</td>\n\
    <td>N<sub>targets</sub></td>\n\
    <td title='Fraction of targets that cannot be completed beceause their required/requested exposure times are too long'>F<sub>imposs</sub></td>\n\
    <td title='Total fiber hours required to successfully observe all non-impossible targets, assuming that the observations are all to be carried out within the given moon phase' >Requested<br>fiber-hours</td>\n\
    <td colspan='3'>Sky distributions of:</td>\n\
    <td colspan='7'>1D distributions in:</td>\n\
  </tr>\n\
\n\
  <tr class='header-row'>\n\
    <td></td>\n\
    <td>Big Files!</td>\n\
    <td>MB</td>\n\
    <td>&#215;10<sup>6</sup></td>\n\
    <td>percent</td>\n\
    <td>Mfhr</td>\n\
    <td>Number density</td>\n\
    <td>Fiber-hours</td>\n\
    <td>Impossible</td>\n\
    <td>RA</td>\n\
    <td>T<sub>req-dark</sub></td>\n\
    <td>T<sub>req-grey</sub></td>\n\
    <td>T<sub>req-bright</sub></td>\n\
    <td>Magnitude</td>\n\
    <td>Priority</td>\n\
    <td>Redshift</td>\n\
  </tr>\n");

  for ( c=0;c<pCatList->num_cats+3;c++)
  {
    int m;
    catalogue_struct* pCat = NULL;
    float size_MB = NAN;
    struct stat st;
    char *filename = "";
    char *codename = "";
    char *displayname = "";
    char str_stub[STR_MAX] = {'\0'}; 
    int num_objects = 0;
    int num_objects_impossible = 0;
    BOOL plot_exgal = FALSE;
    double req_fhrs[MAX_NUM_MOON_PHASES];

    if ( c==pCatList->num_cats )  //AllLoRes
    {
      if ( pCatList->num_cats_lores <= 0 ||  pCatList->num_cats_hires <= 0) continue;
      codename    = (char*) CATALOGUE_CODENAME_ALLLORES;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALLLORES;
      snprintf(str_stub, STR_MAX, "_%s",codename);
      if ( pCatList->num_cats_exgal_lores > 0 ) plot_exgal = TRUE;
      num_objects = pSurvey->total_objects_lores;
      num_objects_impossible = pSurvey->total_objects_impossible_lores;
      for(m=0;m<MAX_NUM_MOON_PHASES;m++) req_fhrs[m] = pCatList->req_fhrs_lores[m];
    }
    else if ( c==pCatList->num_cats+1 )//AllHiRes
    {
      if ( pCatList->num_cats_lores <= 0 ||  pCatList->num_cats_hires <= 0) continue;
      codename    = (char*) CATALOGUE_CODENAME_ALLHIRES;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALLHIRES;
      snprintf(str_stub, STR_MAX, "_%s",codename);
      if ( pCatList->num_cats_exgal_hires > 0 ) plot_exgal = TRUE;
      num_objects = pSurvey->total_objects_hires;
      num_objects_impossible = pSurvey->total_objects_impossible_hires;
      for(m=0;m<MAX_NUM_MOON_PHASES;m++) req_fhrs[m] = pCatList->req_fhrs_hires[m];
    }
    else if ( c==pCatList->num_cats+2 ) //All
    {
      if ( pCatList->num_cats <= 1 ) continue;
      codename    = (char*) CATALOGUE_CODENAME_ALL;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALL;
      if ( pCatList->num_cats_exgal > 0 ) plot_exgal = TRUE;
      num_objects = total_num_objects;
      num_objects_impossible = pSurvey->total_objects_impossible;
      for(m=0;m<MAX_NUM_MOON_PHASES;m++) req_fhrs[m] = pCatList->req_fhrs[m];
    } 
    else    //normal catalogue
    {
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename    = (char*) pCat->codename;
      displayname = (char*) pCat->displayname;
      filename    = (char*) pCat->filename;
      num_objects = pCat->num_objects;
      num_objects_impossible = pCat->num_objects_impossible;
      for(m=0;m<MAX_NUM_MOON_PHASES;m++) req_fhrs[m] = pCat->req_fhrs[m];
      snprintf(str_stub, STR_MAX, "_%s",codename);
      if ( stat(pCat->filename, &st) == 0)
      {
        size_MB = (float) st.st_size / (float) MEGABYTE;
      }
      if ( pCat->is_extragalactic ) plot_exgal = TRUE;
      total_num_objects += pCat->num_objects;
    }
      


    fprintf(pFile, "  <tr%s>\n\
    <td style='text-align:left;'>%s</td>\n",
            (c>=pCatList->num_cats?" class='special-row'":""), displayname);

    if ( c<pCatList->num_cats )  //only write filenames and file sizes for real input files
      fprintf(pFile, "\
    <td><a href='file://%s' target='_blank' type='text/plain'>link</a></td>\n \
    <td>%.1f</td>\n", filename, size_MB);
    else
      fprintf(pFile, "    <td></td>\n    <td></td>\n");

    fprintf(pFile, "    <td>%.4g</td>\n\
    <td>%.3g%%</td>\n\
    <td>Dark:%.3g<br>Grey:%.3g<br>Bright:%.3g</td>\n",
            (double) num_objects/1e6,
            (double) 100.0*(num_objects_impossible/(double)(MAX(1,num_objects))),
            (double) req_fhrs[MOON_PHASE_DARK]/1e6,
            (double) req_fhrs[MOON_PHASE_GREY]/1e6,
            (double) req_fhrs[MOON_PHASE_BRIGHT]/1e6);
      
    //now the sky distribution plots and links
    {
      int s;
      const char* str_type[]  = {"targets",
                                 "targets_fhrs",
                                 "impossible_targets"}; 
      const char* str_title[] = {"The sky distribution of the input targets",
                                 "The sky distribution of the required fiber-hours for the input targets",
                                 "The sky distribution of input targets with too long exposure times"};
      for (s=0;s<sizeof(str_type)/sizeof(const char*);s++)
      {
        fprintf(pFile, "\
    <td>\n\
      <a href='%s/input_sky_density_of_%s_%s.png'             target='_blank' type='image/png'        title='%s - figure'>\n\
        <img alt='a figure' src='%s/input_sky_density_of_%s_%s_thumb.png' /></a><br>\n\
      <a href='%s/input_sky_density_of_%s_%s.txt.gz'          target='_blank' type='application/gzip' title='%s - ascii datafile'>.txt.gz</a>,\n\
      <a href='%s/input_sky_density_of_%s_%s_healpix.fits.gz' target='_blank' type='application/fits' title='%s - HEALPIX datafile'>healpix.fits.gz</a>\n\
    </td>\n",
                INPUT_STATS_SUBDIRECTORY, str_type[s], codename, str_title[s],
                INPUT_STATS_SUBDIRECTORY, str_type[s], codename,
                INPUT_STATS_SUBDIRECTORY, str_type[s], codename, str_title[s],
                INPUT_STATS_SUBDIRECTORY, str_type[s], codename, str_title[s]);
      }
    }

    //now the 1D distribution plots and links
    {
      int s;
      const char* str_type[]  = { OPSIM_STATISTICS_HISTO_NAME_RA,
                                  //                                  OPSIM_STATISTICS_HISTO_NAME_TREQ,
                                  OPSIM_STATISTICS_HISTO_NAME_TREQ_D,
                                  OPSIM_STATISTICS_HISTO_NAME_TREQ_G,
                                  OPSIM_STATISTICS_HISTO_NAME_TREQ_B,
                                  OPSIM_STATISTICS_HISTO_NAME_MAG,
                                  OPSIM_STATISTICS_HISTO_NAME_PRIORITY,
                                  OPSIM_STATISTICS_HISTO_NAME_REDSHIFT }; 
      const char* str_title[] = {"Distribution of Right Ascension (deg) for the input targets",
                                 //                                 "Distribution of requested/required exposure times for the input targets",
                                 "Distribution of requested/required exposure times for the input targets - Dark time",
                                 "Distribution of requested/required exposure times for the input targets - Grey time",
                                 "Distribution of requested/required exposure times for the input targets - Bright time",
                                 "Distribution of magnitudes (r-band,AB) for the input targets",
                                 "Distribution of adjusted priorities for the input targets",
                                 "Distribution of redshifts for the input targets"};
      int smax =sizeof(str_type)/sizeof(const char*);
      for (s=0;s<smax;s++)
      {
        if ( s<smax-1 || plot_exgal )
        {
          fprintf(pFile, "\
    <td>\n\
      <a href='%s/input_%s_histo%s.pdf' target='_top'   type='application/pdf' title='%s - plot'>\n\
        <img src='%s/input_%s_histo%s_thumb.png' alt='A plot' /><br></a>\n\
      <a href='%s/input_%s_histo_%s.txt' target='_blank' type='text/plain'      title='%s - datafile'>.txt</a>\n\
    </td>\n",
                  INPUT_STATS_SUBDIRECTORY, str_type[s], str_stub, str_title[s],
                  INPUT_STATS_SUBDIRECTORY, str_type[s], str_stub,
                  INPUT_STATS_SUBDIRECTORY, str_type[s], codename, str_title[s]);
        }
        else fprintf(pFile, "    <td></td>\n");
      }
      fprintf(pFile, "</tr>\n");
    }
  }    
  fprintf(pFile, "</table>\n");
  //============Input catalogues============//



  //============Sky tiling/progress/FoM plots============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Sky Tiling/Survey Strategy/FoM progress", OPSIM_OUTPUT_HTML_GUIDE, "SkyTilingSurveyStrategyFoMProgress");
  fprintf(pFile, "<table  >\n\
  <tr class='header-row'>\n\
    <td>Selected Parameter Name</td>\n\
    <td>Value</td>\n\
    <td>Units</td>\n\
  </tr>\n");
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "TILING.METHOD",                        "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "TILING.FILENAME",                      "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "TILING.NUM_POINTS",                    "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "TILING.DEC_MIN",                       "",  "deg")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "TILING.DEC_MAX",                       "",  "deg")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "TILING.NUM_DITHER_OFFSETS",            "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.STRATEGY_TYPE",                 "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.MOON_SPLIT.METHOD",             "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.DBPDG.GAL_LAT_MIN_DARKGREY",    "",  "deg")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.DBPDG.GAL_LAT_MAX_BRIGHT",      "",  "deg")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.TILE_PLANNING_METHOD_DARKGREY", "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.TILE_PLANNING_METHOD_BRIGHT",   "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.NUM_TILES_PER_PASS_DARKGREY",   "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.NUM_TILES_PER_PASS_BRIGHT",     "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.NUM_TILES_PER_FIELD_DARKGREY",  "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.NUM_TILES_PER_FIELD_BRIGHT",    "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.USE_UP_SPARE_TILES",            "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.OVERALLOCATION_FACTOR.BRIGHT",  "",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.OVERALLOCATION_FACTOR.DARKGREY","",  "")) return 1;
  if ( ParLib_write_param_html_table_entry(pFile,pParamList, "SURVEY.SKYCALC.FILENAME",              "",  "")) return 1;
  //  if ( ParLib_write_param_html_table_entry(pFile, pParamList, "",    "",  "")) return 1;
  fprintf(pFile, "</table><br>\n");

  fprintf(pFile, "\
<table   style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Description</td>\n\
    <td>Plot</td>\n\
    <td>Data</td>\n\
  </tr>\n");


  fprintf(pFile, "  <tr>\n\
    <td>Sky visibility</td>\n\
    <td><a href='%s/%s.pdf' target='_top' type='application/pdf' title='The relative visibility of each Field'>\n\
      <img src='%s/%s_thumb.png' alt='Thumbnail image' /></a>\n\
    </td>\n\
    <td></td>\n\
  </tr>\n",
          PLOTFILES_SUBDIRECTORY, SKY_VISIBILITY_STEM,
          PLOTFILES_SUBDIRECTORY, SKY_VISIBILITY_STEM);

  //sky tiling plots, RA pressure plot
  {
    int s;
    const char* str_stem[]  = {SKY_TILING_REQUESTED_STEM,
                               RA_PRESSURE_STEM,
                               SKY_TILING_PLANNED_STEM,
                               SKY_TILING_EXECUTED_STEM,
                               TILES_USED_WASTED_STEM};
    const char* str_label[] = {"Requested sky tiling",
                               "Predicted tile pressure vs RA",
                               "Planned sky tiling",
                               "Executed sky tiling",
                               "Tiles used and wasted vs RA"};
    const char* str_title[] = {"The sky tiling that was requested before the start of the survey",
                               "The distribution of predicted tile availability compared to requested tiles as a function of RA",
                               "The sky tiling planned at the start of the survey",
                               "The sky tiling that was actually delivered by the end of the survey",
                               "The distribution of usefully used and wasted tiles as a function of RA"};
    const char* str_filename[] = {FIELD_VERTICES_EXTENDED_FILENAME,
                                  TILING_STATS_VS_RA_FILENAME,
                                  FIELD_VERTICES_EXTENDED_FILENAME,
                                  FIELD_VERTICES_EXTENDED_FILENAME,
                                  WASTED_TILES_VS_RA_FILENAME};
    
    int smax =sizeof(str_stem)/sizeof(const char*);
    for (s=0;s<smax;s++)
    {
      fprintf(pFile, "\
  <tr>\n\
    <td>%s</td>\n\
    <td><a href='%s/%s.pdf' target='_top' type='application/pdf' title='%s - plot'>\n\
      <img src='%s/%s_thumb.png' alt='Thumbnail image' /></a>\n\
    </td>\n\
    <td><a href='%s/%s' target='_blank' type='text/plain' title='%s - data'>.txt</a></td>\n\
  </tr>\n",
              str_label[s],
              PLOTFILES_SUBDIRECTORY, str_stem[s],     str_title[s],
              PLOTFILES_SUBDIRECTORY, str_stem[s],
              PLOTFILES_SUBDIRECTORY, str_filename[s], str_title[s]);
    }
  }
    
  //hide this diagnostic as it is no longer one of the usual simulator products
  /*
    fprintf(pFile, "  <tr>\n\
    <td>Survey progress animation</td>\n\
    <td><a href='%s/progress_report.mp4' target='_top' type='video/mp4'       title='An animated representation of the survey progression'>\n\
          <img src='%s/progress_report_frame_final_thumb.png' alt='Thumbnail image' /></a>\n\
</td>\n\
<td></td>\n\
</tr>\n", PROGRESS_REPORT_SUBDIRECTORY, PROGRESS_REPORT_SUBDIRECTORY);
  */

  //FoM progress
  fprintf(pFile, "  <tr>\n\
    <td>Survey FoM Progress</td>\n\
    <td><a href='%s/%s.pdf' target='_top' type='application/pdf' title='%s - plot'>\n\
          <img src='%s/%s_thumb.png' alt='Thumbnail image' /></a></td>\n\
     <td><a href='%s/%s' target='_blank' type='text/plain' title='%s - datafile'>.txt</a></td>\n\
  </tr>\n",
          PROGRESS_REPORT_SUBDIRECTORY, FOM_PROGRESS_PLOT_STEM, "The accumulation of FoM as the survey progresses",
          PROGRESS_REPORT_SUBDIRECTORY, FOM_PROGRESS_PLOT_STEM,
          OUTPUT_STATS_SUBDIRECTORY,    FOM_RESULTS_FILENAME, "The final FoM report at the end of the survey");

  fprintf(pFile, "</table>\n\n");
  //============Sky tiling/progress/FoM plots============//


  //============Fiber usage statistics============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Fiber usage statistics", OPSIM_OUTPUT_HTML_GUIDE, "FiberUsageStatistics");
  fprintf(pFile, "\
<table   style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Description</td>\n\
    <td>Plot</td>\n\
  </tr>\n");
  fprintf(pFile, "  <tr>\n\
    <td>Fiber offset patterns</td>\n\
    <td><a href='%s/%s.pdf'    target='_top' type='application/pdf' title='The distribution of fiber offsets'>\n\
      <img src='%s/%s_thumb.png' alt='Thumbnail image' /></a>\n\
    </td></tr>\n",
          FIBER_OFFSET_SUBDIRECTORY, FIBER_OFFSETS_STEM,
          FIBER_OFFSET_SUBDIRECTORY, FIBER_OFFSETS_STEM);
  fprintf(pFile, "  <tr>\n\
    <td>Fiber usage patterns</td>\n\
    <td><a href='%s/%s.pdf'                target='_top' type='application/pdf' title='Various measures of fiber utilisation'>\n\
      <img src='%s/%s_thumb.png' alt='Thumbnail image' /></a>\n\
    </td></tr>\n",
          PLOTFILES_SUBDIRECTORY,FIBER_USAGE_STEM,
          PLOTFILES_SUBDIRECTORY, FIBER_USAGE_STEM);
  fprintf(pFile, "  <tr>\n\
    <td>Fiber efficiency trends</td>\n\
    <td><a href='%s/%s.pdf' target='_top' type='application/pdf' title='The fractional fiber utilisation vs survey progress'>\n\
      <img src='%s/%s_thumb.png' alt='Thumbnail image' /></a>\n\
    </td></tr>\n",
          PLOTFILES_SUBDIRECTORY, FIBER_EFFICIENCY_TRENDS_STEM,
          PLOTFILES_SUBDIRECTORY, FIBER_EFFICIENCY_TRENDS_STEM);
  fprintf(pFile, "</table>\n\n");
  //============Fiber usage statistics============//


  //============Fiber efficiency maps============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Fiber efficiency maps", OPSIM_OUTPUT_HTML_GUIDE, "FiberEfficiencyMaps");

  fprintf(pFile, "\
<table  style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Year of survey</td>\n\
    <td colspan='2' title=''>Low resolution fibers</td>\n\
    <td colspan='2' title=''>High resolution fibers</td>\n\
  </tr>\n\
  <tr class='header-row'>\n\
    <td></td>\n\
    <td>Dark/Grey</td>\n\
    <td>Bright</td>\n\
    <td>Dark/Grey</td>\n\
    <td>Bright</td>\n\
  </tr>\n\n");

  {
    int y;
    for(y=0;y<=(int)ceil(pSurvey->duration);y++)
    {
      if ( y==0 ) fprintf(pFile, "<tr class='special-row'>\n<td>%d-%d</td>\n", (int) 0, (int) ceil(pSurvey->duration));
      else        fprintf(pFile, "<tr>\n<td>%d</td>\n", y);
      
      fprintf(pFile, "  <td>\n\
      <a href='%s/fiber_efficiency_maps_res_LO_moon_dg_year_%d.png' target='_blank' type='image/png' title='higher resolution plot'>\n\
        <img src='%s/fiber_efficiency_maps_res_LO_moon_dg_year_%d_thumb.png' alt='Thumbnail preview' /><br></a>\n\
    </td>\n\
    <td>\n\
      <a href='%s/fiber_efficiency_maps_res_LO_moon_bb_year_%d.png' target='_blank' type='image/png' title='higher resolution plot'>\n\
        <img src='%s/fiber_efficiency_maps_res_LO_moon_bb_year_%d_thumb.png' alt='Thumbnail preview' /><br></a>\n\
    </td>\n\
    <td>\n\
      <a href='%s/fiber_efficiency_maps_res_HI_moon_dg_year_%d.png' target='_blank' type='image/png' title='higher resolution plot'>\n\
        <img src='%s/fiber_efficiency_maps_res_HI_moon_dg_year_%d_thumb.png' alt='Thumbnail preview' /><br></a>\n\
    </td>\n\
    <td>\n\
      <a href='%s/fiber_efficiency_maps_res_HI_moon_bb_year_%d.png' target='_blank' type='image/png' title='higher resolution plot'>\n\
        <img src='%s/fiber_efficiency_maps_res_HI_moon_bb_year_%d_thumb.png' alt='Thumbnail preview' /><br></a>\n\
    </td>\n\
  </tr>\n",
              PLOTFILES_SUBDIRECTORY, y, PLOTFILES_SUBDIRECTORY, y,
              PLOTFILES_SUBDIRECTORY, y, PLOTFILES_SUBDIRECTORY, y,
              PLOTFILES_SUBDIRECTORY, y, PLOTFILES_SUBDIRECTORY, y,
              PLOTFILES_SUBDIRECTORY, y, PLOTFILES_SUBDIRECTORY, y);
    }
  }
  fprintf(pFile, "</table>\n");
  //============Fiber efficiency maps============//

  
  //============Output catalogues/statistics============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Output catalogues/statistics", OPSIM_OUTPUT_HTML_GUIDE, "OutputCatalogues");
  fprintf(pFile, "\
<table  style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Catalogue/Survey name</td>\n\
    <td>Output<br>Catalogue</td>\n\
    <td title='Total number of input targets'                             >N<sub>targets</sub></td>\n\
    <td title='Total number of successfully observed targets'             >N<sub>completed_targets</sub></td>\n\
    <td title='Survey Figure of Merit'                                    >Survey FoM</td>\n\
    <td title='Number of completed targets/Total number of input targets' >Completeness</td>\n\
    <td colspan='3' title='Sum of executed fiber-hours'                   >Executed Fiber-hours (&#215;10<sup>6</sup>)</td>\n\
    <td colspan='9' title='Diagnostic plots and data'                     >Observed/Completed fraction data/plots</td>\n\
 </tr>\n\
  <tr class='header-row'>\n\
    <td></td>\n\
    <td>(Big Files!)</td>\n\
    <td>&#215;10<sup>6</sup></td>\n\
    <td>&#215;10<sup>6</sup></td>\n\
    <td></td>\n\
    <td></td>\n\
    <td>Dark/Grey</td>\n\
    <td>Bright</td>\n\
    <td>Total</td>\n\
    <td>RA</td>\n\
    <td>T<sub>req-dark</sub></td>\n\
    <td>T<sub>req-grey</sub></td>\n\
    <td>T<sub>req-bright</sub></td>\n\
    <td>T<sub>exp-done</sub></td>\n\
    <td>T<sub>expFrac</sub></td>\n\
    <td>Magnitude</td>\n\
    <td>Priority</td>\n\
    <td>Redshift</td>\n\
  </tr>\n\n");

  {
    float total_fiber_hours = 0.0;
    float total_fiber_hours_dg = 0.0;
    float total_fiber_hours_bb = 0.0;
    float arithmetic_mean_FoM = 0.0;
    float geometric_mean_FoM = 1.0;
    float arithmetic_mean_completeness = 0.0;
    float geometric_mean_completeness = 1.0;
    int total_num_completed_objects = 0;

    float total_fiber_hours_lores = 0.0;
    float total_fiber_hours_dg_lores = 0.0;
    float total_fiber_hours_bb_lores = 0.0;
    //    float arithmetic_mean_FoM_lores = 0.0;
    //    float geometric_mean_FoM_lores = 1.0;
    float arithmetic_mean_completeness_lores = 0.0;
    float geometric_mean_completeness_lores = 1.0;
    int total_num_completed_objects_lores = 0;

    float total_fiber_hours_hires = 0.0;
    float total_fiber_hours_dg_hires = 0.0;
    float total_fiber_hours_bb_hires = 0.0;
    //    float arithmetic_mean_FoM_hires = 0.0;
    //    float geometric_mean_FoM_hires = 1.0;
    float arithmetic_mean_completeness_hires = 0.0;
    float geometric_mean_completeness_hires = 1.0;
    int total_num_completed_objects_hires = 0;

    for ( c=0;c<pCatList->num_cats+3;c++)
    {
      catalogue_struct* pCat = NULL;
      char *codename = "";
      char *displayname = "";
      char str_stub[STR_MAX] = {'\0'}; 
      char str_FoM[STR_MAX]; 
      char str_completeness[STR_MAX];
      int num_objects = 0;
      BOOL plot_exgal = FALSE;
      int num_completed_objects;
      double fiber_hours_dg;
      double fiber_hours_bb;
      double fiber_hours;
      float FoM_am=0.0, FoM_gm=0.0;
      if ( c==pCatList->num_cats )  //AllLoRes
      {
        if ( pCatList->num_cats_lores <= 0 || pCatList->num_cats_hires <= 0) continue;
        codename    = (char*) CATALOGUE_CODENAME_ALLLORES;
        displayname = (char*) CATALOGUE_DISPLAYNAME_ALLLORES;
        snprintf(str_stub, STR_MAX, "_%s",codename);
        if ( pCatList->num_cats_exgal_lores > 0 ) plot_exgal = TRUE;
        num_objects = pSurvey->total_objects_lores;
        num_completed_objects = total_num_completed_objects_lores;
        //        FoM_am = (float) arithmetic_mean_FoM_lores/(float)MAX(1,pCatList->num_cats_lores);
        //        FoM_gm = (float) pow(geometric_mean_FoM_lores,1.0/(float)MAX(1,pCatList->num_cats_lores));
        FoM_am = NAN;
        FoM_gm = NAN;
        snprintf(str_completeness, STR_MAX, "ArithMean:<br>%.4f<br>GeomMean:<br>%.4f",
                 (float) arithmetic_mean_completeness_lores/(float)MAX(1,pCatList->num_cats_lores),
                 (float) pow(geometric_mean_completeness_lores,1.0/(float)MAX(1,pCatList->num_cats_lores)));
        fiber_hours    = total_fiber_hours_lores;
        fiber_hours_dg = total_fiber_hours_dg_lores;
        fiber_hours_bb = total_fiber_hours_bb_lores;
      }
      else if ( c==pCatList->num_cats+1 )//AllHiRes
      {
        if ( pCatList->num_cats_lores <= 0 || pCatList->num_cats_hires <= 0) continue;
        codename    = (char*) CATALOGUE_CODENAME_ALLHIRES;
        displayname = (char*) CATALOGUE_DISPLAYNAME_ALLHIRES;
        snprintf(str_stub, STR_MAX, "_%s",codename);
        if ( pCatList->num_cats_exgal_hires > 0 ) plot_exgal = TRUE;
        num_objects = pSurvey->total_objects_hires;
        num_completed_objects = total_num_completed_objects_hires;
        //        FoM_am = (float) arithmetic_mean_FoM_hires/(float)MAX(1,pCatList->num_cats_hires);
        //        FoM_gm = (float) pow(geometric_mean_FoM_hires,1.0/(float)MAX(1,pCatList->num_cats_hires));
        FoM_am = NAN;
        FoM_gm = NAN;
        snprintf(str_completeness, STR_MAX, "ArithMean:<br>%.4f<br>GeomMean:<br>%.4f",
                 (float) arithmetic_mean_completeness_hires/(float)MAX(1,pCatList->num_cats_hires),
                 (float) pow(geometric_mean_completeness_hires,1.0/(float)MAX(1,pCatList->num_cats_hires)));
        fiber_hours    = total_fiber_hours_hires;
        fiber_hours_dg = total_fiber_hours_dg_hires;
        fiber_hours_bb = total_fiber_hours_bb_hires;
      }
      else if ( c==pCatList->num_cats+2 ) //All
      {
        if ( pCatList->num_cats <= 1 ) continue;
        codename    = (char*) CATALOGUE_CODENAME_ALL;
        displayname = (char*) CATALOGUE_DISPLAYNAME_ALL;
        if ( pCatList->num_cats_exgal > 0 ) plot_exgal = TRUE;
        num_objects = pSurvey->total_objects_hires + pSurvey->total_objects_lores;
        num_completed_objects = total_num_completed_objects;
        FoM_am = (float) arithmetic_mean_FoM/(float)MAX(1,pCatList->num_cats);
        FoM_gm = (float) pow(geometric_mean_FoM,1.0/(float)MAX(1,pCatList->num_cats));
        snprintf(str_completeness, STR_MAX, "ArithMean:<br>%.4f<br>GeomMean:<br>%.4f",
                 (float) arithmetic_mean_completeness/(float)MAX(1,pCatList->num_cats),
                 (float) pow(geometric_mean_completeness,1.0/(float)MAX(1,pCatList->num_cats)));
        fiber_hours    = total_fiber_hours;
        fiber_hours_dg = total_fiber_hours_dg;
        fiber_hours_bb = total_fiber_hours_bb;
      } 
      else    //normal catalogue
      {
        pCat = (catalogue_struct*) &(pCatList->pCat[c]);
        codename    = (char*) pCat->codename;
        displayname = (char*) pCat->displayname;
        num_objects = pCat->num_objects;
        num_completed_objects  = (double) num_objects*pCat->completeness;
        snprintf(str_stub, STR_MAX, "_%s",codename);
        if ( pCat->is_extragalactic ) plot_exgal = TRUE;
        snprintf(str_completeness, STR_MAX, "%.4f", pCat->completeness);
        fiber_hours    = pCat->fiber_hours;
        fiber_hours_dg = pCat->fiber_hours_moon[MOON_GROUP_DG];
        fiber_hours_bb = pCat->fiber_hours_moon[MOON_GROUP_BB];

        total_num_objects            += pCat->num_objects;
        total_num_completed_objects  += num_completed_objects;
        total_fiber_hours            += pCat->fiber_hours;
        total_fiber_hours_dg         += pCat->fiber_hours_moon[MOON_GROUP_DG];
        total_fiber_hours_bb         += pCat->fiber_hours_moon[MOON_GROUP_BB];
        arithmetic_mean_FoM          += pCat->FoM;
        geometric_mean_FoM           *= pCat->FoM;
        arithmetic_mean_completeness += pCat->completeness;
        geometric_mean_completeness  *= pCat->completeness;
        if ( pCat->num_objects_hires > 0 )
        {
          total_num_completed_objects_hires  += (double) pCat->num_objects_hires*pCat->completeness_hires;
          total_fiber_hours_hires            += pCat->fiber_hours_hires;
          total_fiber_hours_dg_hires         += pCat->fiber_hours_hires_moon[MOON_GROUP_DG];
          total_fiber_hours_bb_hires         += pCat->fiber_hours_hires_moon[MOON_GROUP_BB];
          //          arithmetic_mean_FoM_hires          += pCat->FoM;
          //          geometric_mean_FoM_hires           *= pCat->FoM;
          arithmetic_mean_completeness_hires += pCat->completeness_hires;
          geometric_mean_completeness_hires  *= pCat->completeness_hires;
        }

        if ( pCat->num_objects_lores > 0 )
        {
          total_num_completed_objects_lores  += (double) pCat->num_objects_lores*pCat->completeness_lores;
          total_fiber_hours_lores            += pCat->fiber_hours_lores;
          total_fiber_hours_dg_lores         += pCat->fiber_hours_lores_moon[MOON_GROUP_DG];
          total_fiber_hours_bb_lores         += pCat->fiber_hours_lores_moon[MOON_GROUP_BB];
          //          arithmetic_mean_FoM_lores          += pCat->FoM;
          //          geometric_mean_FoM_lores           *= pCat->FoM;
          arithmetic_mean_completeness_lores += pCat->completeness_lores;
          geometric_mean_completeness_lores  *= pCat->completeness_lores;
        }
      }
      
      if ( c==pCatList->num_cats+2 ) // all cats
      {
        snprintf(str_FoM, STR_MAX, "\
ArithMean:<br><font color='%s'>%.4f</font><br>\n\
GeomMean:<br><font  color='%s'>%.4f</font>",
                 FOM_TO_COLOUR(FoM_am), (float) FoM_am, 
                 FOM_TO_COLOUR(FoM_gm), (float) FoM_gm );
      }
      else if ( c==pCatList->num_cats || c==pCatList->num_cats+1 )  //Alllores or Allhires
      {
        snprintf(str_FoM, STR_MAX, "-");
      }
      else
      {
        snprintf(str_FoM, STR_MAX, "<font color='%s'>%.4f</font>",
                 FOM_TO_COLOUR(pCat->FoM), pCat->FoM);
      }


      if ( c < pCatList->num_cats )
        fprintf(pFile, "  <tr>\n\
    <td style='text-align:left;'>%s</td>\n\
    <td><a href='%s_%s.%s%s' target='_blank' type='%s' title='The output results catalogue'>link</a></td>\n",
              displayname, 
              OUTPUT_CATALOGUE_STEM,
              codename,
              (pCat->format_code==CATALOGUE_FORMAT_FITS?"fits":"txt"),
              (pSurvey->gzip_output_catalogues?".gz":""),
                (pSurvey->gzip_output_catalogues?"application/gzip":(pCat->format_code==CATALOGUE_FORMAT_FITS?"application/fits":"text/plain")));
      else
        fprintf(pFile, "  <tr class='special-row'>\n\
    <td style='text-align:left;'>%s</td>\n\
    <td></td>\n", displayname );


      fprintf(pFile, "<td>%.4g</td>\n\
    <td>%.4g</td>\n\
    <td>%s</td>\n\
    <td>%s</td>\n\
    <td>%.3g</td>\n\
    <td>%.3g</td>\n\
    <td>%.3g</td>\n\n",
              (double) num_objects/1e6,
              (double) num_completed_objects/1e6,
              (char*)  str_FoM,
              (char*)  str_completeness ,
              (double) fiber_hours_dg/1e6,
              (double) fiber_hours_bb/1e6,
              (double) fiber_hours/1e6);


      
      //now the 1D distribution plots and links
      {
        int s;
        const char* str_type[]  = { OPSIM_STATISTICS_HISTO_NAME_RA,
                                    OPSIM_STATISTICS_HISTO_NAME_TREQ_D,
                                    OPSIM_STATISTICS_HISTO_NAME_TREQ_G,
                                    OPSIM_STATISTICS_HISTO_NAME_TREQ_B,
                                    OPSIM_STATISTICS_HISTO_NAME_TOBS_ED,
                                    OPSIM_STATISTICS_HISTO_NAME_TEXPFRAC,
                                    OPSIM_STATISTICS_HISTO_NAME_MAG,
                                    OPSIM_STATISTICS_HISTO_NAME_PRIORITY,
                                    OPSIM_STATISTICS_HISTO_NAME_REDSHIFT};
        const char* str_title[] = {"Fraction of observed/completed targets in bins of Right Ascension",
                                   "Fraction of observed/completed targets in bins of requested/required exposure time",
                                   "Fraction of observed/completed targets in bins of requested/required exposure time",
                                   "Fraction of observed/completed targets in bins of requested/required exposure time",
                                   "Fraction of observed/completed targets in bins of executed exposure time",
                                   "Fraction of observed/completed targets in bins of fractional required exposure time executed",
                                   "Fraction of observed/completed targets in bins of target magnitude",
                                   "Fraction of observed/completed targets in bins of adjusted target priority",
                                   "Fraction of observed/completed targets in bins of target redshift" };
        int smax =sizeof(str_type)/sizeof(const char*);
        for (s=0;s<smax;s++)
        {
          if ( s<smax-1 || plot_exgal )
          {
            fprintf(pFile, "\
    <td>\n\
      <a href='%s/output_%s_histo%s.pdf' target='_top'    type='application/pdf' title='%s - plot'>\n\
        <img src='%s/output_%s_histo%s_thumb.png' alt='Thumbnail preview' /><br></a>\n\
      <a href='%s/output_%s_histo_%s.txt' target='_blank' type='text/plain'      title='%s - datafile'>.txt</a>\n\
    </td>\n",
                    OUTPUT_STATS_SUBDIRECTORY, str_type[s], str_stub, str_title[s],
                    OUTPUT_STATS_SUBDIRECTORY, str_type[s], str_stub,
                    OUTPUT_STATS_SUBDIRECTORY, str_type[s], codename, str_title[s]);
          }
          else fprintf(pFile, "    <td></td>\n");
        }
        fprintf(pFile, "</tr>\n");
      }
       
    }
  }
  fprintf(pFile, "</table>\n");
  //============Output catalogues/statistics============//

 //============Output Sky Distributions ============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Output Sky Distributions", OPSIM_OUTPUT_HTML_GUIDE, "OutputSkyDistributions");
  fprintf(pFile, "\
<table style='text-align:center;'>\n\
  <tr class='header-row'>\n\
    <td>Catalogue name</td>\n\
    <td colspan='3'>Observed Targets</td>\n\
    <td colspan='3'>Unobserved Targets</td>\n\
    <td colspan='3'>Completed Targets</td>\n\
    <td colspan='3'>Non-completed Targets</td>\n\
    <td colspan='1'>Completed/Observed</td>\n\
  </tr>\n\
  <tr class='header-row'>\n\
    <td></td>\n\
    <td>Number<br>Density</td>\n<td>Required<br>Fiber-hours</td>\n<td>Fraction</td>\n\n\n\
    <td>Number<br>Density</td>\n<td>Required<br>Fiber-hours</td>\n<td>Fraction</td>\n\n\n\
    <td>Number<br>Density</td>\n<td>Required<br>Fiber-hours</td>\n<td>Fraction</td>\n\n\n\
    <td>Number<br>Density</td>\n<td>Required<br>Fiber-hours</td>\n<td>Fraction</td>\n\n\n\
    <td>Ratio</td>\n\n\
  </tr>\n\n");
 
  for ( c=0;c<=pCatList->num_cats + 2;c++)
  {
    char *codename = "";
    char *displayname = "";
    uint num_objects = 0;
    char str_prefix [STR_MAX];
    char *str_special = " class='special-row'";
    if ( c < pCatList->num_cats )  //normal
    {
      catalogue_struct* pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      displayname = pCat->displayname;
      num_objects = (uint) pCat->num_objects;
      str_special = (char*) "";
    }
    else if ( c == pCatList->num_cats )  // AllLoRes
    {
      if ( pSurvey->total_objects_lores <= 0 || pSurvey->total_objects_lores <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLLORES;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALLLORES;
      
      num_objects = pSurvey->total_objects_lores;
    }
    else if ( c == pCatList->num_cats + 1 ) // AllHiRes
    {
      if ( pSurvey->total_objects_lores <= 0 || pSurvey->total_objects_lores <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALLHIRES;
      num_objects = pSurvey->total_objects_hires;
    }   
    else if ( c == pCatList->num_cats + 2 ) // All
    {
      if ( pCatList->num_cats == 1 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALL;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALL;
      num_objects = total_num_objects;
    }
    else continue;
    snprintf(str_prefix, STR_MAX, "  <tr%s>\n\
    <td style='text-align:left;'>%s</td>\n", str_special, displayname); //codename);

    
    if ( num_objects > 0 )
    {
      int i;
      const char *str_type[] = { "observed",
                                 "unobserved",
                                 "completed",
                                 "incomplete" } ;
      const char *str_key[]  = { "observed targets",
                                 "unobserved targets",
                                 "completed targets",
                                 "non-completed targets" } ;
      fprintf(pFile, "%s", str_prefix);
      for ( i = 0; i < sizeof(str_type)/sizeof(const char*); i++ )
      {
        int s;
        const char *str_frac[] = { "density", "density", "density_fraction" } ;
        const char *str_fhrs[] = { "",        "_fhrs",   "" } ;
        const char *str_sub[]  = { "",        "required fiber-hours for the ", "fraction of " };   
        for ( s = 0; s < sizeof(str_frac)/sizeof(const char*); s++ )
        {
          fprintf(pFile, "\
    <td><a href='%s/output_sky_%s_of_%s_targets%s_%s.png' target='_blank' type='image/png' title='The sky distribution of the %s%s'>\n\
        <img alt='a figure' src='%s/output_sky_%s_of_%s_targets%s_%s_thumb.png' /></a><br>\n\
        <a href='%s/output_sky_%s_of_%s_targets%s_%s.txt.gz' target='_blank' type='application/gzip' title='The sky distribution of the %s%s - ascii datafile'>.txt.gz</a>\n",
                  OUTPUT_STATS_SUBDIRECTORY, str_frac[s], str_type[i], str_fhrs[s], codename, str_sub[s], str_key[i],
                  OUTPUT_STATS_SUBDIRECTORY, str_frac[s], str_type[i], str_fhrs[s], codename,
                  OUTPUT_STATS_SUBDIRECTORY, str_frac[s], str_type[i], str_fhrs[s], codename, str_sub[s], str_key[i]);

          if ( pSurvey->healpix_nside > 0 )
          {
fprintf(pFile, "    <a href='%s/output_sky_%s_of_%s_targets%s_%s_healpix.fits.gz' target='_blank' type='application/fits' title='The sky distribution of the %s%s - HEALPIX map file'>.fits.gz</a>",
                  OUTPUT_STATS_SUBDIRECTORY, str_frac[s], str_type[i], str_fhrs[s], codename, str_sub[s], str_key[i]);
          }
          fprintf(pFile, "</td>\n\n");

        }


      }
     
      fprintf(pFile, "    <td><a href='%s/output_sky_density_ratio_of_completed_to_observed_targets_%s.png' target='_blank' type='image/png' title='The sky distribution of the ratio of completed to observed targets'>\n\
      <img alt='a figure' src='%s/output_sky_density_ratio_of_completed_to_observed_targets_%s_thumb.png' /></a><br>\n\
      <a href='%s/output_sky_density_ratio_of_completed_to_observed_targets_%s.txt.gz' target='_blank' type='application/gzip' title='The sky distribution of the ratio of completed to observed targets - ascii datafile'>.txt.gz</a>\n",
              OUTPUT_STATS_SUBDIRECTORY, codename,
              OUTPUT_STATS_SUBDIRECTORY, codename,
              OUTPUT_STATS_SUBDIRECTORY, codename);
      if ( pSurvey->healpix_nside > 0 )
      {
        fprintf(pFile, "    <a href='%s/output_sky_density_ratio_of_completed_to_observed_targets_%s_healpix.fits.gz' \n\
target='_blank' type='application/fits' title='The sky distribution of the ratio of completed to observed targets - HEALPIX datafile'>.fits.gz</a>",
                OUTPUT_STATS_SUBDIRECTORY, codename);
      }
      fprintf(pFile, "</td>\n</tr>\n\n");
    }
  }
  fprintf(pFile, "</table><br>\n");
  //============Output Sky Distributions============//

  //============Logs and meta-data ============//
  fprintf(pFile, "<h2>%s<sup><a href='%s#%s' target='_blank' title='User guide'>?</a></sup></h2>\n",
            "Logs, meta-data and sub-directories", OPSIM_OUTPUT_HTML_GUIDE, "LogsMetaData");
  fprintf(pFile, "\
Master simulation log (<a      href='%s' target='_blank' type='text/plain' title='The master logfile of the simulator'>.log</a>)<br>\n\
Software version info (<a      href='%s' target='_blank' type='text/plain' title='A record of the software module versions that were used for this simulation'>.txt</a>)<br>\n\
Night Reports (<a              href='%s' target='_blank' type='text/plain' title='A simple log of twilight-twilight hours, one record per night'>.txt</a>)<br>\n\
Timeline Reports (<a           href='%s' target='_blank' type='text/plain' title='An almanac, one instantaneous almanac record per tile'>.txt</a>)<br>\n\
Tile Reports (<a               href='%s' target='_blank' type='text/plain' title='An observation log, one line per tile'>.txt</a>)<br>\n\
Supplementary Tile Reports (<a href='%s' target='_blank' type='text/plain' title='Additional observation information, one entry per tile'>.txt</a>)<br>\n\
Field Summary Report (<a       href='%s' target='_blank' type='text/plain' title='Summary report of Field success, one entry per Field'>.txt</a>,\n\
                      <a       href='%s' target='_blank' type='application/fits' title='Summary report of Field success, one entry per Field - FITS format'>.fits</a>)<br>\n \
Fibers Summary Report (<a      href='%s' target='_blank' type='text/plain' title='Summary report of fiber usage, one entry per Fiber'>.txt</a>)<br>\n\
Sky brightness histogram (<a   href='%s' target='_blank' type='text/plain' title='A histogram of the realised zenith sky brightness'>.txt</a>)<br>\n",
          "survey.log",
          VERSION_INFO_FILENAME,
          NIGHT_REPORTS_LOG_FILENAME,
          TIMELINE_REPORTS_LOG_FILENAME,
          TILE_REPORTS_LOG_FILENAME,
          PER_TILE_LOG_FILENAME,
          PER_FIELD_LOG_FILENAME, PER_FIELD_LOG_FILENAME_FITS,
          FIBERS_REPORT_LOG_FILENAME,
          SKY_BRIGHTNESS_HISTO_FILENAME);

  fprintf(pFile, "<hr>\
<a href='./' target='_blank' type='' title='Directory'>View the results directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the input statistics sub-directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the output statistics sub-directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the plotfiles sub-directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the progress reports sub-directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the field ranking sub-directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the fiber-to-target assignment sub-directory</a><br>\n\
<a href='%s' target='_blank' type='' title='Directory'>View the fiber offset log sub-directory</a><br>\n",
          INPUT_STATS_SUBDIRECTORY,
          OUTPUT_STATS_SUBDIRECTORY,
          PLOTFILES_SUBDIRECTORY,
          PROGRESS_REPORT_SUBDIRECTORY,
          FIELD_RANKING_SUBDIRECTORY,
          FIBER_ASSIGNMENT_SUBDIRECTORY,
          FIBER_OFFSET_SUBDIRECTORY);

  fprintf(pFile, "<br><br>\n");
  
  {
    time_t rawtime;
    struct tm *timeinfo;
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, " <hr>\n\
\n\
A standard product of the <span style='color:blue;'>%s</span>. Written at %s. \n\
Contact <a href='mailto:%s?Subject=4FS%%20OpSim'>%s</a> for further information.\n\
</body>\n\
</html>\n\n\n",
            OPSIM_BRANDING_STRING,
            asctime(timeinfo), CONTACT_EMAIL_ADDRESS, CONTACT_EMAIL_ADDRESS);
  }
  fflush(pFile);

  if ( pFile ) fclose(pFile); pFile = NULL;
  
  return 0;
}  //end of write_summary_page_html()
//------------------------------------------------------------------------------------------//
