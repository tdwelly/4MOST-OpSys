//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_FoMs_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//--    Note: Definitions are in the corresponding header file OpSim_FoMs_lib.h
//--    Description:
//--      This module is used to calculate various statistics
//--

#include <chealpix.h>   //needed for nice map making
#include <assert.h>   //needed for debug

#include "OpSim_FoMs_lib.h"
#include "geometry_lib.h"


#define ERROR_PREFIX   "#-OpSim_FoMs_lib.c:Error  :"
#define WARNING_PREFIX "#-OpSim_FoMs_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_FoMs_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_FoMs_lib.c:DEBUG  :"
#define RESULTS_PREFIX "#-OpSim_FoMs_lib.c:RESULTS:"


////////////////////////////////////////////////////////////////////////
//functions



int OpSim_FoMs_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////FOMS/////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/////calculate and write FOMs for each Survey

///////Generic fractional completenesss FoM
int calc_FoM_simple_headcount(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile, int num_required)
{
  int i;
  int nok = 0;
  int ntot = 0;
  float FoM;
  if (pObjList == NULL || pCat == NULL || pFile == NULL )  return 1;
  
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    if ( pObj->TexpFracDone >= 1.0 )  nok ++;
    ntot ++;
  }
  if ( num_required > 0 ) FoM = ((float) nok) / ((float) num_required);  //very simple FoM calculation!!
  else                    FoM = (float) NAN;

  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d Nrequired= %8d FoM= %10.6f\n",
          RESULTS_PREFIX, pCat->sname, nok, num_required, FoM);
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
  fflush(pFile);

  pCat->FoM = FoM;
  //  pCat->completeness = (float) nok / (float) MAX(1,ntot);
  return 0;
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////// Survey-specific FoMs ////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////
/////////////////// FoM S1 (hpx method) ////////////////////////
////////////////////////////////////////////////////////////////
// This is proportional to the fractional completeness within each fields, summed over all fields  
// Relies on the pObj->sub_code to equal the HPX pixel index for the target (calculated once when reading in catalogue) 
// TODO - should deal correctly with pixels that are on the boundary 
int calc_FoM_S1(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  long   npix = (long) nside2npix((long) OPSIM_FOMS_FOM_S1_HPX_NSIDE); //work out how many pixels we will need
  double hpx_pixarea = (double) FULL_SKY_AREA_SQDEG / (double) npix;
  int    *phisto_nobj =  NULL;
  int    *phisto_nok  =  NULL;
  int    i, pix;
  int    nok = 0;
  int    ntot = 0;
  double FoM = 0.0;
  double FoM_thresholded;

  if (pObjList == NULL ||
      pCat == NULL ||
      pFile == NULL ) return 1;

  
  if ((phisto_nobj = (int*) calloc((size_t) npix, sizeof(int))) == NULL ||
      (phisto_nok  = (int*) calloc((size_t) npix, sizeof(int))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for (i=pCat->index_of_first_object;i<=pCat->index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    assert(pObj->sub_code >= 0 && pObj->sub_code < npix);
    phisto_nobj[pObj->sub_code] ++;
    if ( pObj->TexpFracDone >= 1.0 )
    {
      phisto_nok[pObj->sub_code] ++;
      nok ++;
    }
    ntot ++;
  }

  
  //now calc the statistics
  for(pix=0;pix<npix;pix++)
  {
    double delta_FoM = 0;
    if (phisto_nobj[pix]>=OPSIM_FOMS_FOM_S1_MIN_PER_PIXEL )
    {
      delta_FoM = (double) phisto_nok[pix] / ((double) phisto_nobj[pix]);
      delta_FoM = MIN(1.0,delta_FoM/(double)OPSIM_FOMS_FOM_S1_FRACTION_GOAL);
      FoM += hpx_pixarea * delta_FoM;
    }
  }
  FoM = FoM / OPSIM_FOMS_FOM_S1_AREA_GOAL;
  FoM_thresholded = MIN(FoM,1.0);
  
  //print in the human readable format:
  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoM_raw= %10.6f FoM_final= %10.6f\n",
          RESULTS_PREFIX, pCat->sname, nok, FoM, FoM_thresholded);
  //print in the machine readable format:
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,FoM_thresholded,nok,ntot,pCat->fiber_hours); 
  pCat->FoM = FoM_thresholded;
  //  pCat->completeness = (double) nok / (double) MAX(1,ntot);

  FREETONULL(phisto_nok);
  FREETONULL(phisto_nobj);

  return 0;
}
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////
/////////////////// FoM S2 /////////////////////////////////////
////////////////////////////////////////////////////////////////
//this is just a simple fractional completeness check, measured over all successful targets
int calc_FoM_S2(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;
  if (calc_FoM_simple_headcount((objectlist_struct*) pObjList,
                                     (catalogue_struct*) pCat,
                                     (FILE*) pFile,
                                     (MAX(1,pCat->num_objects))))
  {
    return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////
/////////////////// FoM helper S3/S4 //////////////////////////
////////////////////////////////////////////////////////////////
//this just calculates a generic FoM where the FoM has a sqrt term above the minimum completeness 
float calc_modified_FoM_S3_S4(FILE* pFile, int Nok, int Nmin, int Ngoal, int Ntotal,
                              const char* str_name, const char *str_subcat )
{
  float FoM;
  if ( Nok >= Ngoal )
  {
    FoM = (float) 1.0;
  }
  else if ( Nmin <= 0 )
  {
    FoM = (float) Nok / (float) MAX(1,Ntotal);  //only used for GalDiskHR_h
  }
  else if ( Nok >= Nmin )
  {
    FoM = 0.5 * (1.0 + sqrt((float)(Nok - Nmin) /
                            (float) MAX(1,(Ngoal - Nmin))));
  }
  //  else  FoM = (float) Nok / (float) MAX(1,Ntotal); //original versioon found to be incorrect
  else  if ( Nok > 0 )
  {
    FoM = 0.5 * (float) Nok / (float) MAX(1,Nmin); //revised version that should give a continuous FoM(completeness)
  }
  else FoM = 0.0; //keep this case to avoid poorly defined behaviour when Nok=0,Nmin=0

  if ( pFile )
  {
    fprintf(pFile, "%s CalcFoM survey= %10s subFoM=%s Nmin= %8d Ngoal= %8d Ntot= %8d Nok= %8d subFoM= %10.6f\n",
            RESULTS_PREFIX, str_name, str_subcat, Nmin, Ngoal, Ntotal, Nok, FoM);
  }
  
  return (float) FoM; 
}

////////////////////////////////////////////////////////////////
/////////////////// FoM S3 /////////////////////////////////////
////////////////////////////////////////////////////////////////
//updated with new definition for round9
//depends on success in several sub FoMs based on object type
int calc_FoM_S3(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  int   nok;
  int   ntot = 0;
  int   nok_subcat[NUM_SUBCATS_S3];
  int   ntot_subcat[NUM_SUBCATS_S3];
  int i, j;
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  for(j=0;j<NUM_SUBCATS_S3;j++)
  {
    nok_subcat[j]  = (int) 0;
    ntot_subcat[j] = (int) 0;
  }
  
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    assert(pObj->sub_code>=0 && pObj->sub_code< NUM_SUBCATS_S3);
    ntot_subcat[pObj->sub_code] ++;
    ntot ++;
    if ( pObj->TexpFracDone >= 1.0 )
    {
      nok_subcat[pObj->sub_code]++;
    }
  }

  //---------------------------------------------------------------
  //calculate the FoM for each subcat
  for(j=1;j<NUM_SUBCATS_S3;j++)
  {
    int nmin, ngoal;
    const char * pstr = NULL;
    switch (j)
    {
    case SUBCATALOGUE_CODE_S3_ESN:
      nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_ESN;
      ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_ESN;
      pstr = (const char*) "ESN     ";
      break;
    case SUBCATALOGUE_CODE_S3_DYNDISK1:
      nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_DYNDISK1;
      ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_DYNDISK1;
      pstr = (const char*) "DynDisk1";
      break;
    case SUBCATALOGUE_CODE_S3_DYNDISK2:
      nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_DYNDISK2;
      ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_DYNDISK2;
      pstr = (const char*) "DynDisk2";
      break;
    case SUBCATALOGUE_CODE_S3_CHEMDISK:
      nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_CHEMDISK;
      ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_CHEMDISK;
      pstr = (const char*) "ChemDisk";
      break;
    case SUBCATALOGUE_CODE_S3_BULGE:
      nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_BULGE;
      ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_BULGE;
      pstr = (const char*) "Bulge   ";
      break;

    default:
      continue;
      break;
    }

    pCat->subFoM[j] =
      calc_modified_FoM_S3_S4( (FILE*) pFile,
                               (int) nok_subcat[j],
                               (int) nmin,
                               (int) ngoal,
                               (int) ntot_subcat[j],
                               (const char*) pCat->sname,
                               (const char*) pstr);

  }
  //---------------------------------------------------------------


  //  pCat->FoM = (float) (1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S3_ESN] +
  //                       1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S3_DYNDISK1] +
  //                       1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S3_DYNDISK2] +
  //                       1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S3_CHEMDISK] +
  //                       3.0 * pCat->subFoM[SUBCATALOGUE_CODE_S3_BULGE] ) / 7.0;


  // new definition from Cristina 13/09/2016
  pCat->FoM = (float) MIN5(pCat->subFoM[SUBCATALOGUE_CODE_S3_ESN],
                           pCat->subFoM[SUBCATALOGUE_CODE_S3_DYNDISK1],
                           pCat->subFoM[SUBCATALOGUE_CODE_S3_DYNDISK2],
                           pCat->subFoM[SUBCATALOGUE_CODE_S3_CHEMDISK],
                           pCat->subFoM[SUBCATALOGUE_CODE_S3_BULGE]);

  nok = (int)   (nok_subcat[SUBCATALOGUE_CODE_S3_ESN] +
                 nok_subcat[SUBCATALOGUE_CODE_S3_DYNDISK1] +
                 nok_subcat[SUBCATALOGUE_CODE_S3_DYNDISK2] +
                 nok_subcat[SUBCATALOGUE_CODE_S3_CHEMDISK] +
                 nok_subcat[SUBCATALOGUE_CODE_S3_BULGE] );

  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoM_ESN= %8.5f FoM_DynDisk1= %8.5f FoM_DynDisk2= %8.5f FoM_ChemDisk= %8.5f FoM_Bulge= %8.5f FoM_total= %10.6f\n",
          RESULTS_PREFIX,
          pCat->sname,
          nok,
          pCat->subFoM[SUBCATALOGUE_CODE_S3_ESN],
          pCat->subFoM[SUBCATALOGUE_CODE_S3_DYNDISK1],
          pCat->subFoM[SUBCATALOGUE_CODE_S3_DYNDISK2],
          pCat->subFoM[SUBCATALOGUE_CODE_S3_CHEMDISK],
          pCat->subFoM[SUBCATALOGUE_CODE_S3_BULGE],
          pCat->FoM); 

  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,pCat->FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
  //  pCat->completeness = (float) nok / (float) MAX(1,ntot);
 
  return 0;
}

////////////////////////////////////////////////////////////////
/////////////////// FoM S3  Subcat /////////////////////////////
////////////////////////////////////////////////////////////////
//assumes that each S3 sub-cat is run as an independent survey
int calc_FoM_S3_subcat(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  int   nok = 0;
  int   ntot = 0;
  int i;
  int sub_code;
  int nmin, ngoal;
  const char *pstr = NULL;
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  // take sub-cat code from first object in catalogue
  sub_code = (int) pObjList->pObject[pCat->index_of_first_object].sub_code;

  switch (sub_code)
  {
  case SUBCATALOGUE_CODE_S3_ESN:
    nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_ESN;
    ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_ESN;
    pstr = (const char*) "ESN     ";
    break;
  case SUBCATALOGUE_CODE_S3_DYNDISK1:
    nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_DYNDISK1;
    ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_DYNDISK1;
    pstr = (const char*) "DynDisk1";
    break;
  case SUBCATALOGUE_CODE_S3_DYNDISK2:
    nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_DYNDISK2;
    ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_DYNDISK2;
    pstr = (const char*) "DynDisk2";
    break;
  case SUBCATALOGUE_CODE_S3_CHEMDISK:
    nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_CHEMDISK;
    ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_CHEMDISK;
    pstr = (const char*) "ChemDisk";
    break;
  case SUBCATALOGUE_CODE_S3_BULGE:
    nmin  = (int) OPSIM_FOMS_FOM_S3_NUM_MIN_BULGE;
    ngoal = (int) OPSIM_FOMS_FOM_S3_NUM_GOAL_BULGE;
    pstr = (const char*) "Bulge   ";
    break;
    
  default:
    return 1;
    break;
  }
  

  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    ntot ++;
    if ( pObj->TexpFracDone >= 1.0 )
    {
      nok++;
    }
  }

  pCat->FoM =
    calc_modified_FoM_S3_S4( (FILE*) pFile,
                             (int) nok,
                             (int) nmin,
                             (int) ngoal,
                             (int) ntot,
                             (const char*) pCat->sname,
                             (const char*) pstr);

  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoM= %10.6f\n",
          RESULTS_PREFIX,
          pCat->sname,
          nok,
          pCat->FoM); 

  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,pCat->FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
 
  return 0;
}

////////////////////////////////////////////////////////////////
/////////////////// FoM S4 /////////////////////////////////////
////////////////////////////////////////////////////////////////
// updated for round9
// depends on success in several sub FoMs based on object type
int calc_FoM_S4(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  int   nok;
  int   ntot = 0;
  int   nok_subcat[NUM_SUBCATS_S4];
  int   ntot_subcat[NUM_SUBCATS_S4];
  int i, j;
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  for(j=0;j<NUM_SUBCATS_S4;j++)
  {
    nok_subcat[j]  = (int) 0;
    ntot_subcat[j] = (int) 0;
  }

  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    assert(pObj->sub_code>=0 && pObj->sub_code< NUM_SUBCATS_S4);
    ntot_subcat[pObj->sub_code] ++;
    ntot ++;
    if ( pObj->TexpFracDone >= 1.0 )
    {
      nok_subcat[pObj->sub_code]++;
    }
  }

  //---------------------------------------------------------------
  //calculate the FoM for each subcat
  for(j=1;j<NUM_SUBCATS_S4;j++)
  {
    int nmin, ngoal;
    const char * pstr = NULL;
    nmin = 0;
    switch (j)
    {
    case SUBCATALOGUE_CODE_S4_BULGE:
      //      nmin  = (int) OPSIM_FOMS_FOM_S4_NUM_MIN_BULGE;
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_BULGE;
      pstr = (const char*) "Bulge        ";
      break;
    case SUBCATALOGUE_CODE_S4_INNERDISK:
      //      nmin  = (int) OPSIM_FOMS_FOM_S4_NUM_MIN_INNERDISK;
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_INNERDISK;
      pstr = (const char*) "InnerDisk    ";
      break;
    case SUBCATALOGUE_CODE_S4_OUTERDISK:
      //      nmin  = (int) OPSIM_FOMS_FOM_S4_NUM_MIN_OUTERDISK;
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_OUTERDISK;
      pstr = (const char*) "OuterDisk    ";
      break;
    case SUBCATALOGUE_CODE_S4_UNBIASED:
      //      nmin  = (int) OPSIM_FOMS_FOM_S4_NUM_MIN_INNERDISK;
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_UNBIASED;
      pstr = (const char*) "ZUnbiasedDisk";
      break;

    default:
      continue;
      break;
    }

    pCat->subFoM[j] = (float) (double) nok_subcat[j] / (double) ngoal;
      //      calc_modified_FoM_S3_S4( (FILE*) pFile,
      //                               (int) nok_subcat[j],
      //                               (int) nmin,
      //                               (int) ngoal,
      //                               (int) ntot_subcat[j],
      //                               (const char*) pCat->sname,
      //                               (const char*) pstr);

    fprintf(pFile, "%s CalcFoM survey= %10s subFoM=%s Nmin= %8d Ngoal= %8d Ntot= %8d Nok= %8d subFoM= %10.6f\n",
            RESULTS_PREFIX, pCat->sname, pstr, nmin, ngoal, ntot_subcat[j], nok_subcat[j], pCat->subFoM[j]);

  }
  //---------------------------------------------------------------


  pCat->FoM = (float) (1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S4_BULGE] +
                       1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S4_INNERDISK] +
                       1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S4_OUTERDISK] +
                       1.0 * pCat->subFoM[SUBCATALOGUE_CODE_S4_UNBIASED] ) / (double) 4.0;
  nok = (int)   (nok_subcat[SUBCATALOGUE_CODE_S4_BULGE]     +
                 nok_subcat[SUBCATALOGUE_CODE_S4_INNERDISK] +
                 nok_subcat[SUBCATALOGUE_CODE_S4_OUTERDISK] +
                 nok_subcat[SUBCATALOGUE_CODE_S4_UNBIASED] );

  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoM_Bulge= %8.5f FoM_InnerDisk= %8.5f FoM_OuterDisk= %8.5f FoM_ZUnbiasedDisk= %8.5f FoM_total= %10.6f\n",
          RESULTS_PREFIX,
          pCat->sname,
          nok,
          pCat->subFoM[SUBCATALOGUE_CODE_S4_BULGE],
          pCat->subFoM[SUBCATALOGUE_CODE_S4_INNERDISK],
          pCat->subFoM[SUBCATALOGUE_CODE_S4_OUTERDISK],
          pCat->subFoM[SUBCATALOGUE_CODE_S4_UNBIASED],
          pCat->FoM); 

  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,pCat->FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
  //  pCat->completeness = (float) nok / (float) MAX(1,ntot);
  return 0;
}


////////////////////////////////////////////////////////////////
/////////////////// FoM S4 Subcat///////////////////////////////
////////////////////////////////////////////////////////////////
//assumes that each S4 sub-cat is run as an independent survey
int calc_FoM_S4_subcat(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  int nok = 0;
  int ntot = 0;
  int i;
  int sub_code;
  int ngoal;
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  // take sub-cat code from first object in catalogue
  sub_code = (int) pObjList->pObject[pCat->index_of_first_object].sub_code;

  switch (sub_code)
  {
    case SUBCATALOGUE_CODE_S4_BULGE:
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_BULGE;
      break;
    case SUBCATALOGUE_CODE_S4_INNERDISK:
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_INNERDISK;
      break;
    case SUBCATALOGUE_CODE_S4_OUTERDISK:
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_OUTERDISK;
      break;
    case SUBCATALOGUE_CODE_S4_UNBIASED:
      ngoal = (int) OPSIM_FOMS_FOM_S4_NUM_GOAL_UNBIASED;
      break;
    default:
      return 1;
      break;
  }
  
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    ntot ++;
    if ( pObj->TexpFracDone >= 1.0 )
    {
      nok++;
    }
  }

  pCat->FoM = (float) ((double) nok / (double) ngoal);

  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoM= %10.6f\n",
          RESULTS_PREFIX,
          pCat->sname,
          nok,
          pCat->FoM); 

  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,pCat->FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
  return 0;
}

////////////////////////////////////////////////////////////////
/////////////////// FoM S5 /////////////////////////////////////
////////////////////////////////////////////////////////////////
// tweaked for round9 - allow for missing clusters
// Verified with M1 documentation
// calculate the number of successful observations in each
// cluster, then count how many clusters with >4 successful objects
int calc_FoM_S5(survey_struct *pSurvey, objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  int i;
  int *pnobj = NULL;
  float *pz  = NULL;
  int *pnok  = NULL;
  float FoM = 0.0;
  int nok = 0;
  int nclus_completed = 0;
  int ntot = 0;

  int nclus[OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS+1];
  int nclus_ok[OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS+1];
  const float clus_weight[OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS+1] = {0.15, 0.15, 0.20, 0.25, 0.25, 0.0};
  const float zbin_min[OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS+1]    = { 0.0,  0.2,  0.4,  0.6,  0.8, 1.0};
  const float zbin_max[OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS+1]    = { 0.2,  0.4,  0.6,  0.8,  1.0, 1e30};

  if (pSurvey     == NULL ||
      pObjList == NULL ||
      pCat        == NULL ||
      pFile       == NULL ) return 1;

  //  if ( (pnobj = (int*)   calloc((size_t) pSurvey->num_unique_clusters, sizeof(int))) == NULL ||
  //       (pz    = (float*) calloc((size_t) pSurvey->num_unique_clusters, sizeof(float))) == NULL ||
  //       (pnok  = (int*)   calloc((size_t) pSurvey->num_unique_clusters, sizeof(int))) == NULL )
  if ( (pnobj = (int*)   calloc((size_t) (pCat->sub_code_max+1), sizeof(int))) == NULL ||
       (pz    = (float*) calloc((size_t) (pCat->sub_code_max+1), sizeof(float))) == NULL ||
       (pnok  = (int*)   calloc((size_t) (pCat->sub_code_max+1), sizeof(int))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for(i=0;i<OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS+1;i++)
  {
    nclus[i] = 0;
    nclus_ok[i] = 0;
  }
    
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    if ( pObj->sub_code >= 0 &&
         //         pObj->sub_code < pSurvey->num_unique_clusters )
         pObj->sub_code <= pCat->sub_code_max )
    {
      pnobj[pObj->sub_code] ++;
      pz[pObj->sub_code] = pObj->redshift;
      if ( pObj->TexpFracDone >= 1.0 )   
      {
        pnok[pObj->sub_code] ++;
        nok ++;
      }
      ntot ++;
    }
    else
    {
      fprintf(stderr, "%s Cluster associated to object %d,z=%g has index %d (max=%d)\n", ERROR_PREFIX,
              pObj->id, pObj->redshift, pObj->sub_code, pSurvey->num_unique_clusters);
      return 1;
    }
  }
  for(i=0;i<pSurvey->num_unique_clusters;i++)
  {
    cluster_struct *pClus = (cluster_struct*) &(pSurvey->pClusterList->pCluster[i]);
    object_struct  *pBGC  = (object_struct*) &(pObjList->pObject[pClus->objid_BCG]);
    int zbin = 0;
    int id = pClus->id;
    if ( pz[id] < 0.0 ) zbin = OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS;
    else
    {
      while ( pz[id] > zbin_max[zbin] &&
              zbin < OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS)
        zbin ++;
    }
    nclus[zbin] ++;
    pClus->redshift_bin_for_FoM = zbin;
    if ( (pnok[id] >= OPSIM_FOMS_FOM_S5_NUM_GALS_FOR_SUCCESS ||
         (pz[id]   >= OPSIM_FOMS_FOM_S5_REDSHIFT_THRESH && pnok[id] >= 1))  && //at z>thresh just need BCG for success
         pBGC->TexpFracDone >= 1.0 )                                                     //BCG must always be completed
    {
      nclus_ok[zbin]++ ;
      nclus_completed ++;
      pClus->successfully_observed = TRUE;
    }
    else pClus->successfully_observed = FALSE;
  }

  //record the deltaFoMs in the cluster structs
  for(i=0;i<pSurvey->num_unique_clusters;i++)
  {
    cluster_struct *pClus = (cluster_struct*) &(pSurvey->pClusterList->pCluster[i]);
    if ( pClus->successfully_observed ) 
      pClus->deltaFoM = clus_weight[pClus->redshift_bin_for_FoM] / ((float) MAX(nclus[pClus->redshift_bin_for_FoM],1));
    else pClus->deltaFoM = 0.0;
  }
  
  
  for(i=0;i<OPSIM_FOMS_FOM_S5_NUM_REDSHIFT_BINS;i++)
  {
    float dFoM = ((float) nclus_ok[i]) * clus_weight[i] / ((float) MAX(nclus[i],1)); 
    FoM += dFoM;
    fprintf(pFile, "%s CalcFoM survey= %10s delatFoM zbin=%d z=[%7.3f:%7.3f] NClus=%8d NClus_ok=%8d Weight=%8.4g dFoM=%10.6f\n",
             RESULTS_PREFIX, pCat->sname, i, zbin_min[i], zbin_max[i],  nclus[i], nclus_ok[i], clus_weight[i], dFoM);
  }
  
  fprintf(pFile, "%s CalcFoM survey= %10s NGal_ok= %8d NClus_ok= %8d FoM= %10.6f\n",
          RESULTS_PREFIX, pCat->sname, nok,nclus_completed, FoM);
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
  FREETONULL(pnobj);
  FREETONULL(pz);
  FREETONULL(pnok);
  pCat->FoM = FoM;
  //  pCat->completeness = (float) nok / (float) MAX(1,ntot);
  return 0;
}

////////////////////////////////////////////////////////////////
/////////////////// FoM S6 /////////////////////////////////////
////////////////////////////////////////////////////////////////
//Verified with M1 documentation
float modify_FoM_S6 ( float FoM_in ) 
{
  float FoM_out;
  if ( FoM_in >= 0.75 ) FoM_out = FoM_in*(10./3.) - 2.0;
  else                  FoM_out = FoM_in*(2./3.);
  //  return (float) FoM_out; //allow FOM to go above 1.0
  return ((float) MIN(1.0,FoM_out));  // clip FoM at 1
}

//############### AGN ################
//Verified with M1 documentation
//
// * steps to calculate AGN FoM:
//   1) Measure completeness (=dsubFoM_i) in each of:
//      a) 5 redshift  bins z=[0:5,1]   (=dsubFoM_z_i)
//      b) 7 magnitude bins r=[16:23,1] (=dsubFoM_mag_i)
//      c) overall (=subFoM_overall)
//   2) Calc intermediate subFoMs:
//      a) subFoM_z   = mean{dsubFoM_z_i}
//      b) subFoM_mag = mean{dsubFoM_mag_i}
//   3) Modify each subFoM using subFoM' = (subFoM > 0.75 ? subFoM*3.3333 - 2 : subFoM*0.666667)
//   4) Calc final survey FoM = mean{subFoM_overall',subFoM_z',subFoM_mag'}
//
//   Only consider targets in the Dec range -70<Dec<0deg
//
int calc_FoM_S6(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  int i;
  int n_fullsky_tot = 0;
  int n_fullsky_ok  = 0;
  int ntot = 0;
  int nok = 0;
  float FoM_all = 0.0;
  float FoM_z   = 0.0;
  float FoM_mag = 0.0;
  float FoM_total = 0.0;
  int ntot_mag[OPSIM_FOMS_FOM_S6_NUM_MAG_BINS];
  int nok_mag[OPSIM_FOMS_FOM_S6_NUM_MAG_BINS];
  int ntot_z[OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS];
  int nok_z[OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS];
  float FoM_fullsky_all = 0.0;
  float FoM_fullsky_z   = 0.0;
  float FoM_fullsky_mag = 0.0;
  float FoM_fullsky_total = 0.0;
  int ntot_fullsky_mag[OPSIM_FOMS_FOM_S6_NUM_MAG_BINS];
  int nok_fullsky_mag[OPSIM_FOMS_FOM_S6_NUM_MAG_BINS];
  int ntot_fullsky_z[OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS];
  int nok_fullsky_z[OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS];
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  if ( pCat->code != (utiny ) CATALOGUE_CODE_S6 ) return 1;
  
  //initialise
  for(i=0;i<OPSIM_FOMS_FOM_S6_NUM_MAG_BINS;i++)
  {
    ntot_mag[i] = 0;
    nok_mag[i]  = 0;
    ntot_fullsky_mag[i] = 0;
    nok_fullsky_mag[i]  = 0;
  }
  for(i=0;i<OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS;i++)
  {
    ntot_z[i] = 0;
    nok_z[i]  = 0;
    ntot_fullsky_z[i] = 0;
    nok_fullsky_z[i]  = 0;
  }

//  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
//  {
//    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
//    if ( pObj->dec <= OPSIM_FOMS_FOM_S6_DEC_MAX &&
//         pObj->dec >= OPSIM_FOMS_FOM_S6_DEC_MIN )
//    {
//      if (pObj->redshift < 5.0 ) ntot++;
//      mag_bin = (int) floor(pObj->mag - 16.0);
//      z_bin   = (int) floor(pObj->redshift);
//      if ( mag_bin >= 0 && mag_bin < OPSIM_FOMS_FOM_S6_NUM_MAG_BINS ) ntot_mag[mag_bin] ++;
//      if ( z_bin   >= 0 && z_bin   < OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS ) ntot_z[z_bin] ++;
//      if ( pObj->TexpFracDone >= 1.0 )
//      {
//        if (pObj->redshift < 5.0) nok ++;
//        if ( mag_bin >= 0 && mag_bin < OPSIM_FOMS_FOM_S6_NUM_MAG_BINS ) nok_mag[mag_bin] ++;
//        if ( z_bin   >= 0 && z_bin   < OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS ) nok_z[z_bin] ++;
//      }
//    }
//  }

  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    int mag_bin = (int) floor(pObj->mag - OPSIM_FOMS_FOM_S6_MIN_MAG);
    int z_bin   = (int) floor(pObj->redshift);
    n_fullsky_tot++;
    if ( pObj->TexpFracDone >= 1.0 ) n_fullsky_ok++;

    if ( mag_bin >= 0 && mag_bin < OPSIM_FOMS_FOM_S6_NUM_MAG_BINS ) ntot_fullsky_mag[mag_bin] ++;
    if ( z_bin   >= 0 && z_bin   < OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS ) ntot_fullsky_z[z_bin] ++;

    if ( pObj->TexpFracDone >= 1.0 )
    {
      if ( mag_bin >= 0 && mag_bin < OPSIM_FOMS_FOM_S6_NUM_MAG_BINS ) nok_fullsky_mag[mag_bin] ++;
      if ( z_bin   >= 0 && z_bin   < OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS ) nok_fullsky_z[z_bin] ++;
    }

    //now do the same for just the objects in the valid dec range
    if ( pObj->dec <= OPSIM_FOMS_FOM_S6_DENOMINATOR_DEC_MAX &&
         pObj->dec >= OPSIM_FOMS_FOM_S6_DENOMINATOR_DEC_MIN )
    {
      ntot++;
      if ( mag_bin >= 0 && mag_bin < OPSIM_FOMS_FOM_S6_NUM_MAG_BINS ) ntot_mag[mag_bin] ++;
      if ( z_bin   >= 0 && z_bin   < OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS ) ntot_z[z_bin] ++;
    }
    
    if ( pObj->dec <= OPSIM_FOMS_FOM_S6_NUMERATOR_DEC_MAX &&
         pObj->dec >= OPSIM_FOMS_FOM_S6_NUMERATOR_DEC_MIN )
    {
      if ( pObj->TexpFracDone >= 1.0 )
      {
        nok ++;
        if ( mag_bin >= 0 && mag_bin < OPSIM_FOMS_FOM_S6_NUM_MAG_BINS ) nok_mag[mag_bin] ++;
        if ( z_bin   >= 0 && z_bin   < OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS ) nok_z[z_bin] ++;
      }
    }
  }

  
  //calc the overall FoM - FoM_all

  if ( ntot > 0 )
  {
    FoM_all = modify_FoM_S6 ((float) nok / (float) MAX(1,ntot));
  }
  else FoM_all = NAN;

  if ( n_fullsky_tot > 0 )
  {
    FoM_fullsky_all = modify_FoM_S6 ((float) n_fullsky_ok / (float) MAX(1,n_fullsky_tot));
  }
  else FoM_fullsky_all = NAN;
  
  //now calc the redshift dependent FoM - FoM_z
  for(i=0;i<OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS;i++)
  {
    float dSubFoM = ((float) nok_z[i] / (float) MAX(1,ntot_z[i]))/(float) MAX(1,OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS); 
    float z_min = (float) i;
    float z_max = (float) i + 1.0;
    FoM_z += dSubFoM;
    fprintf(pFile, "%s CalcFoM survey= %10s deltaFoM_z zbin=%d z=[%7.3f:%7.3f] Ntot=%8d Nok=%8d dSubFoM=%10.6f\n",
            RESULTS_PREFIX, pCat->sname, i, z_min, z_max,  ntot_z[i], nok_z[i], dSubFoM);
  }
  FoM_z = modify_FoM_S6 ((float) FoM_z);

  for(i=0;i<OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS;i++)
  {
    float dSubFoM = ((float) nok_fullsky_z[i] / (float) MAX(1,ntot_fullsky_z[i]))/(float) MAX(1,OPSIM_FOMS_FOM_S6_NUM_REDSHIFT_BINS); 
    float z_min = (float) i;
    float z_max = (float) i + 1.0;
    FoM_fullsky_z += dSubFoM;
    fprintf(pFile, "%s CalcFoM survey= %10s fullsky deltaFoM_z zbin=%d z=[%7.3f:%7.3f] Ntot=%8d Nok=%8d dSubFoM=%10.6f\n",
            RESULTS_PREFIX, pCat->sname, i, z_min, z_max,  ntot_fullsky_z[i], nok_fullsky_z[i], dSubFoM);
  }
  FoM_fullsky_z = modify_FoM_S6 ((float) FoM_fullsky_z);
  
  //now calc the magnitude dependent FoM - FoM_mag
  for(i=0;i<OPSIM_FOMS_FOM_S6_NUM_MAG_BINS;i++)
  {
    float dSubFoM = ((float) nok_mag[i] / (float) MAX(1,ntot_mag[i]))/(float) MAX(1,OPSIM_FOMS_FOM_S6_NUM_MAG_BINS); 
    float mag_min = 16.0  + (float) i;
    float mag_max = mag_min + 1.0;;
    FoM_mag += dSubFoM;
    fprintf(pFile, "%s CalcFoM survey= %10s deltaFoM_mag magbin=%d mag=[%7.1f:%7.1f] Ntot=%8d Nok=%8d dSubFoM=%10.6f\n",
            RESULTS_PREFIX, pCat->sname, i, mag_min, mag_max,  ntot_mag[i], nok_mag[i], dSubFoM);
  }
  FoM_mag = modify_FoM_S6 ((float) FoM_mag);

  //now calc the magnitude dependent FoM - FoM_mag
  for(i=0;i<OPSIM_FOMS_FOM_S6_NUM_MAG_BINS;i++)
  {
    float dSubFoM = ((float) nok_fullsky_mag[i] / (float) MAX(1,ntot_fullsky_mag[i]))/(float) MAX(1,OPSIM_FOMS_FOM_S6_NUM_MAG_BINS); 
    float mag_min = 16.0  + (float) i;
    float mag_max = mag_min + 1.0;;
    FoM_fullsky_mag += dSubFoM;
    fprintf(pFile, "%s CalcFoM survey= %10s fullsky deltaFoM_mag magbin=%d mag=[%7.1f:%7.1f] Ntot=%8d Nok=%8d dSubFoM=%10.6f\n",
            RESULTS_PREFIX, pCat->sname, i, mag_min, mag_max,  ntot_fullsky_mag[i], nok_fullsky_mag[i], dSubFoM);
  }
  FoM_fullsky_mag = modify_FoM_S6 ((float) FoM_fullsky_mag);



  FoM_total = (FoM_all + FoM_z + FoM_mag) / 3.0;
  FoM_fullsky_total = (FoM_fullsky_all + FoM_fullsky_z + FoM_fullsky_mag) / 3.0;
  //report the results
  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoM_overall= %10.6f FoM_z= %10.6f FoM_mag= %10.6f FoM_total= %10.6f\n", RESULTS_PREFIX,
          pCat->sname, nok, FoM_all, FoM_z, FoM_mag, FoM_total);
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,FoM_total,n_fullsky_ok,n_fullsky_tot,pCat->fiber_hours); //print in the machine readable format
  fprintf(pFile, "%s CalcFoM survey= %10s fullsky Ncompleted= %8d FoM_overall= %10.6f FoM_z= %10.6f FoM_mag= %10.6f FoM_total= %10.6f\n", RESULTS_PREFIX,
          pCat->sname, n_fullsky_ok, FoM_fullsky_all, FoM_fullsky_z, FoM_fullsky_mag, FoM_fullsky_total);

  pCat->FoM = FoM_total;
  pCat->subFoM[SUBFOM_CODE_S6_MAG] = FoM_mag;
  pCat->subFoM[SUBFOM_CODE_S6_Z]   = FoM_z;
  pCat->subFoM[SUBFOM_CODE_S6_ALL] = FoM_all;
  //  pCat->completeness = (float) n_fullsky_ok / (float) MAX(1,n_fullsky_tot);

  return 0;
}


int calc_FoM_S7 ( objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  const double WAVES_FoM_exp = OPSIM_FOMS_FOM_S7_EXPONENT; //exponent for WAVES FoM completeness -> FoM calculation 
  int nok_w  = 0;
  int nok_d  = 0;
  //  int nok_ud = 0;
  int nok    = 0;
  int ntot   = 0;
  int ntot_w = 0;
  int ntot_d = 0;
  int i;
  float FoM_w;
  float FoM_d;
  //  float FoM_ud;
  float FoM;
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    if      ( pObj->sub_code == SUBCATALOGUE_CODE_S7_WIDE )
    {
      ntot_w ++;
      if ( pObj->TexpFracDone >= (float) 1.0 ) nok_w ++;
    }
    else if ( pObj->sub_code == SUBCATALOGUE_CODE_S7_DEEP )
    {
      ntot_d ++;
      if ( pObj->TexpFracDone >= (float) 1.0 ) nok_d ++;
    }
  }
  nok =  nok_w + nok_d; // + nok_ud;   // Note that the nok_ud was missing before
  ntot = ntot_d + ntot_w;

  //  ntot = pCat->num_valid_objects_subcat[SUBCATALOGUE_CODE_S7_WIDE] +
  //    pCat->num_valid_objects_subcat[SUBCATALOGUE_CODE_S7_DEEP] ;
  //         pCat->num_valid_objects_subcat[SUBCATALOGUE_CODE_S7_ULTRADEEP];

  FoM_w  = pow((double)nok_w /(double)MAX(1,ntot_w), WAVES_FoM_exp);
  FoM_d  = pow((double)nok_d /(double)MAX(1,ntot_d), WAVES_FoM_exp);
  //  FoM_ud = pow((double)nok_ud/(double)MAX(1,pCat->num_valid_objects_subcat[SUBCATALOGUE_CODE_S7_ULTRADEEP]), WAVES_FoM_exp);

  FoM = FoM_w * FoM_d;// * FoM_ud;

  pCat->FoM = FoM;
  //  pCat->completeness = (float) (nok) / (float) MAX(1,ntot);

  pCat->subFoM[SUBFOM_CODE_S7_WIDE]      = FoM_w;
  pCat->subFoM[SUBFOM_CODE_S7_DEEP]      = FoM_d;
  //  pCat->subFoM[SUBFOM_CODE_S7_ULTRADEEP] = FoM_ud;

  //  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoMw= %8.5f FoMd= %8.5f FoMud= %8.5f FoM_total= %10.6f\n",
  //          RESULTS_PREFIX, pCat->sname, nok, FoM_w, FoM_d, FoM_ud, FoM);
  fprintf(pFile, "%s CalcFoM survey= %10s Deep Ncompleted= %8d Ntotal= %8d subFoM= %10.6f\n",
          RESULTS_PREFIX, pCat->sname, nok_d, ntot_d, FoM_d);
  fprintf(pFile, "%s CalcFoM survey= %10s Wide Ncompleted= %8d Ntotal= %8d subFoM= %10.6f\n",
          RESULTS_PREFIX, pCat->sname, nok_w, ntot_w, FoM_w);
  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d FoMw= %8.5f FoMd= %8.5f FoM_total= %10.6f\n",
          RESULTS_PREFIX, pCat->sname, nok, FoM_w, FoM_d, FoM);
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format

  return 0;
}

// see update to S8 FoM definition 10/09/2016
// /home/tdwelly/4most/survey_team_inputs/S8/info_2016-09-10/FOM.txt
// norm_FoM_S8() is unchanged
float norm_FoM_S8 ( float FoM_in, float FoM_min, float FoM_max ) 
{
  float result;
  if      ( FoM_max - FoM_min <= 0.0 ) result = -1.0;
  else if ( FoM_in <= FoM_min )
  {
    if ( FoM_min > 0. ) result = 0.5*FoM_in/FoM_min;
    else                result = 0.0;
  }
  else if ( FoM_in >= FoM_max )  result = (float) 1.0;
  else                           result = (float) 0.5 + 0.5*(FoM_in - FoM_min )/(FoM_max - FoM_min);
  return (float) result;
}

\
// copied big chunck of this from calc_FoM_S1()
// new version that first computes the density in each HEALPIX pixel
// Relies on the pObj->sub_code being set equal to the HPX pixel index for the target (calculated once when reading in catalogue) 
int calc_FoM_S8(survey_struct* pSurvey, objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{

  long   npix = (long) nside2npix((long) OPSIM_FOMS_FOM_S8_HPX_NSIDE); //work out how many pixels we will need
  double hpx_pixarea = (double) FULL_SKY_AREA_SQDEG / (double) npix;
  double min_targets_per_pix = (double) OPSIM_FOMS_FOM_S8_MIN_DENISTY_PER_SQDEG * hpx_pixarea;
  //  int    *phisto_nobj =  NULL;
  int    *phisto_nok  =  NULL;
  int    i, pix;
  int    nok = 0;
  int    ntot = 0;
  double raw_subFoM1;
  double raw_subFoM2;

  if (pObjList == NULL ||
      pCat == NULL ||
      pFile == NULL ) return 1;
  
  if (//(phisto_nobj = (int*) calloc((size_t) npix, sizeof(int))) == NULL ||
      (phisto_nok  = (int*) calloc((size_t) npix, sizeof(int))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for (i=pCat->index_of_first_object;i<=pCat->index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    assert(pObj->sub_code >= 0 && pObj->sub_code < npix);
    //    phisto_nobj[pObj->sub_code] ++;
    if ( pObj->TexpFracDone >= 1.0 )
    {
      phisto_nok[pObj->sub_code] ++;
      nok ++;
    }
    ntot ++;
  }


  //now calc the statistics
  raw_subFoM1 = 0.0;
  raw_subFoM2 = 0.0;
  for(pix=0;pix<npix;pix++)
  {
    if ( ((double) phisto_nok[pix]) >= min_targets_per_pix )
    {
      raw_subFoM1 += hpx_pixarea;
      raw_subFoM2 += (double) phisto_nok[pix];
    }
  }
  pCat->subFoM[SUBFOM_CODE_S8_AREA]   = norm_FoM_S8(raw_subFoM1, OPSIM_FOMS_FOM_S8_FOM1_MIN, OPSIM_FOMS_FOM_S8_FOM1_MAX);
  pCat->subFoM[SUBFOM_CODE_S8_NUMBER] = norm_FoM_S8(raw_subFoM2, OPSIM_FOMS_FOM_S8_FOM2_MIN, OPSIM_FOMS_FOM_S8_FOM2_MAX);
  
  pCat->FoM = 0.5*(pCat->subFoM[SUBFOM_CODE_S8_AREA] + pCat->subFoM[SUBFOM_CODE_S8_NUMBER]);


  fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d subFoM1= %10.6f subFoM2= %10.6f rawsubFoM1= %10.6f rawsubFoM2= %10.6f FoM= %10.6f \n",
          RESULTS_PREFIX, pCat->sname, nok,
          pCat->subFoM[SUBFOM_CODE_S8_AREA], pCat->subFoM[SUBFOM_CODE_S8_NUMBER],
          raw_subFoM1, raw_subFoM2,
          pCat->FoM);
  
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,pCat->FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format

  FREETONULL(phisto_nok);
  //  FREETONULL(phisto_nobj);
  return 0; 
}



/*
///////////////////////////////////////////////////////
//TODO write an explanation
//TODO replace with HEALPIX version
int calc_FoM_S8_old(survey_struct* pSurvey, objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  const double ra_min    =   0.0;
  const double ra_max    = 360.0;
  const double dec_min   = DEFAULT_SURVEY_DEC_LIMIT_MIN; //
  const double dec_max   = DEFAULT_SURVEY_DEC_LIMIT_MAX; //
  const float  density_list[OPSIM_FOMS_FOM_S8_NUM_DENSITY_LEVELS] = {300.0, 400.0, 500.0, 600.0}; 
  int          npix_ra   = (int) (ra_max  -  ra_min)/OPSIM_FOMS_FOM_S8_PIXEL_SIZE;  //1x1deg pixels
  int          npix_dec  = (int) (dec_max - dec_min)/OPSIM_FOMS_FOM_S8_PIXEL_SIZE;  //1deg pixels over range -70<dec<20

  int nok = 0;
  int ntot = 0;
  int     i,j,k;
  int    *p2dhisto_nobj =  NULL;
  int    *p2dhisto_nok  =  NULL;
  float  *parea    =  NULL;
  double *pdra     =  NULL;
  double *pinv_dra =  NULL;
  int    *pnra     =  NULL;
  double dra0;   //width at the equator
  double ddec0;
  double inv_ddec0;

  if (pObjList == NULL ||
      pCat == NULL ||
      pFile == NULL ||
      pSurvey == NULL) return 1;

  dra0      = (double) ( ra_max -  ra_min) / ((double) npix_ra) ;   //pixel width at the equator
  ddec0     = (double) (dec_max - dec_min) / ((double) npix_dec) ;
  if ( ddec0 > 0.0 ) inv_ddec0 = (double) 1.0 /ddec0;
  else               return 1;
 
  if ((p2dhisto_nobj = (int*)    calloc((size_t) npix_ra*npix_dec, sizeof(int))) == NULL ||
      (p2dhisto_nok  = (int*)    calloc((size_t) npix_ra*npix_dec, sizeof(int))) == NULL ||
      (parea    = (float*)  malloc(sizeof(float)  * npix_dec))     == NULL ||
      (pdra     = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pinv_dra = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pnra     = (int*)    malloc(sizeof(int)    * npix_dec))     == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //set up pixels
  for(i=0;i<npix_dec;i++)
  {
    double dec0  = dec_min + ddec0*((double) i + 0.5);
    double dec1  = dec0 - ddec0*0.5;
    double dec2  = dec0 + ddec0*0.5;
    double width = (ra_max - ra_min)*cosd(dec0);
    double npix  = floor(width / dra0);
    pnra[i] = (int) npix;
    pdra[i] = (ra_max - ra_min) / npix;
    parea[i] = 0.5*FULL_SKY_AREA_SQDEG * (sind(dec2) - sind(dec1)) / (double) npix; 
    pinv_dra[i] = ( pdra[i] > 0.0 ? (double)  1.0 / pdra[i] : NAN);
  }

  for (i=pCat->index_of_first_object;i<=pCat->index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    {
      int pix_y = (int) ((pObj->dec - dec_min)*inv_ddec0);
      if ( pix_y >= 0 && pix_y < npix_dec )
      {
        int pix_x = (int) ((pObj->ra  - ra_min)*pinv_dra[pix_y]);
        if ( pix_x >= 0 && pix_x < pnra[pix_y] )
        {
          //        fprintf(stderr, "%s badpix %8d %12.7f %12.7f %6d %6d\n", ERROR_PREFIX, i, pObj->ra, pObj->dec, pix_y, pix_x);
          p2dhisto_nobj[pix_x + pix_y*npix_ra] ++;
          if ( pObj->TexpFracDone >= 1.0 )
          {
            p2dhisto_nok[pix_x + pix_y*npix_ra] ++;
            nok ++;
          }
          ntot ++;
        }
      }
    }
  }

  for (k=0;k<OPSIM_FOMS_FOM_S8_NUM_DENSITY_LEVELS;k++)
  {
    float FoM = 0.0;
    float FoM_area = 0.0;
    float FoM_num  = 0.0;
    
    //now calc the statistics
    for(i=0;i<npix_dec;i++)
    {
      double dec1  = dec_min + ddec0*((double) i);
      if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
      for(j=0;j<pnra[i];j++)
      {
        int pix = j + i*npix_ra;
        if ((float) p2dhisto_nok[pix] >= (float) density_list[k] * parea[i] )
        {
          FoM_area += parea[i];
          FoM_num  += p2dhisto_nok[pix];
        }
      }
    }

    FoM_area = norm_FoM_S8 ( (float) FoM_area, (float) OPSIM_FOMS_FOM_S8_AREA_MIN, (float) OPSIM_FOMS_FOM_S8_AREA_MAX );
    FoM_num  = norm_FoM_S8 ( (float) FoM_num,  (float) OPSIM_FOMS_FOM_S8_NUM_MIN,  (float) OPSIM_FOMS_FOM_S8_NUM_MAX );

    FoM = 0.5*(FoM_area + FoM_num);

    if ( k == 0 )
    {  
      OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,FoM,nok,ntot,pCat->fiber_hours); //print in the machine readable format
      pCat->FoM = FoM;
      pCat->subFoM[SUBFOM_CODE_S8_AREA] = FoM_area;
      pCat->subFoM[SUBFOM_CODE_S8_NUM]  = FoM_num;
      //      pCat->completeness = (float) nok / (float) MAX(1,ntot);
    }
    fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d density_level= %6g FoM_area= %10.6f FoM_num= %10.6f FoM= %10.6f\n",
            RESULTS_PREFIX, pCat->sname, nok, density_list[k], FoM_area, FoM_num, FoM);
    
  }
  
  if ( pnra )     free(pnra);      pnra     =NULL;
  if ( pdra )     free(pdra);      pdra     =NULL;
  if ( pinv_dra ) free(pinv_dra);  pinv_dra =NULL;
  if ( parea )    free(parea);     parea    =NULL;
  if ( p2dhisto_nobj)  free(p2dhisto_nobj);  p2dhisto_nobj =NULL;
  if ( p2dhisto_nok)  free(p2dhisto_nok);  p2dhisto_nok =NULL;


  return 0;
}
//////////////////////////////////////////////////
*/



////////////////////////////////////////////////////////////////
/////////////////// FoM S9 /////////////////////////////////////
////////////////////////////////////////////////////////////////
// new for round9
// depends on success in several sub FoMs based on object type
int calc_FoM_S9(objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  const float goal_frac_HR_subcat[NUM_HR_SUBCATS_S9] = {0.0,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET1,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET2,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET3,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET4,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET5,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET6,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET7,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET8,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET9,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_HR_SUBSET10};
  const float weight_HR_subcat[NUM_HR_SUBCATS_S9] = {0.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     1.0,
                                                     0.0};
  const float goal_frac_LR_subcat[NUM_LR_SUBCATS_S9] = {0.0,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET1,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET2,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET3,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET4,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET5,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET6,
                                                        OPSIM_FOMS_FOM_S9_FRAC_GOAL_LR_SUBSET7};

  int   nok = 0;
  int   ntot = 0;
  
  int   nok_HR = 0;
  int   ntot_HR = 0;
  int   nok_HR_subcat[NUM_HR_SUBCATS_S9];
  int   ntot_HR_subcat[NUM_HR_SUBCATS_S9];
  float subFoM_HR_subcat[NUM_HR_SUBCATS_S9];
  float subFoM_HR = 0.0;

  int   nok_LR = 0;
  int   ntot_LR = 0;
  int   nok_LR_subcat[NUM_LR_SUBCATS_S9];
  int   ntot_LR_subcat[NUM_LR_SUBCATS_S9];
  float subFoM_LR_subcat[NUM_LR_SUBCATS_S9];
  float subFoM_LR = 0.0;
  int i, j;
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;

  for(j=0;j<NUM_LR_SUBCATS_S9;j++)
  {
    nok_LR_subcat[j]  = (int) 0;
    ntot_LR_subcat[j] = (int) 0;
  }
  for(j=0;j<NUM_HR_SUBCATS_S9;j++)
  {
    nok_HR_subcat[j]  = (int) 0;
    ntot_HR_subcat[j] = (int) 0;
  }
  
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    if ( pObj->res == RESOLUTION_CODE_LOW )
    {
      assert(pObj->sub_code>=0 && pObj->sub_code< NUM_HR_SUBCATS_S9);
      ntot_LR_subcat[pObj->sub_code] ++;
      if ( pObj->TexpFracDone >= 1.0 )
      {
        nok_LR_subcat[pObj->sub_code]++;
      }
    }
    else if ( pObj->res == RESOLUTION_CODE_HIGH )
    {
      assert(pObj->sub_code>=0 && pObj->sub_code< NUM_HR_SUBCATS_S9);
      ntot_HR_subcat[pObj->sub_code] ++;
      if ( pObj->TexpFracDone >= 1.0 )
      {
        nok_HR_subcat[pObj->sub_code]++;
      }
    }
  }
  
  //---------------------------------------------------------------
  //calculate the FoM for each subcat and overall
  {
    float sum_HR_weight = 0.0;
    for(j=1;j<NUM_HR_SUBCATS_S9;j++)
    {
      int ngoal = (int) ((float) ntot_HR_subcat[j] * goal_frac_HR_subcat[j]);
      subFoM_HR_subcat[j] = MIN(1.0, nok_HR_subcat[j]/((float) MAX(1,ngoal))) ; 
      subFoM_HR += weight_HR_subcat[j] * subFoM_HR_subcat[j];
      fprintf(pFile, "%s CalcFoM survey= %10s Res=%s subcat= %2d subcat_Ncompleted= %8d subcat_Ntotal= %8d subcat_Ngoal= %8d subcat_FoM= %8.5f\n",
              RESULTS_PREFIX,
              pCat->sname,
              RESOLUTION_CODE_STRING(RESOLUTION_CODE_HIGH), j,
              nok_HR_subcat[j],
              ntot_HR_subcat[j],
              ngoal,
              subFoM_HR_subcat[j]);
      sum_HR_weight += weight_HR_subcat[j];
      if ( weight_HR_subcat[j] > 0.0 )
      {
        nok_HR += nok_HR_subcat[j];
        ntot_HR += ntot_HR_subcat[j];
      }
    }
    subFoM_HR = subFoM_HR / ((float) MAX(1.0,sum_HR_weight));
  }
  
  for(j=1;j<NUM_LR_SUBCATS_S9;j++)
  {
    int ngoal = (int) ((float) ntot_LR_subcat[j] * goal_frac_LR_subcat[j]);
    subFoM_LR_subcat[j] = MIN(1.0, nok_LR_subcat[j]/((float) MAX(1,ngoal))) ; 
    subFoM_LR += subFoM_LR_subcat[j];
    fprintf(pFile, "%s CalcFoM survey= %10s Res=%s subcat= %2d subcat_Ncompleted= %8d subcat_Ntotal= %8d subcat_Ngoal= %8d subcat_FoM= %8.5f\n",
            RESULTS_PREFIX,
            pCat->sname,
            RESOLUTION_CODE_STRING(RESOLUTION_CODE_LOW), j,
            nok_LR_subcat[j],
            ntot_LR_subcat[j],
            ngoal,
            subFoM_LR_subcat[j]); 
    nok_LR += nok_LR_subcat[j];
    ntot_LR += ntot_LR_subcat[j];
  }
  subFoM_LR = subFoM_LR / ((float) MAX(1,NUM_LR_SUBCATS_S9));

  pCat->FoM = (float) 0.5 * (subFoM_HR + subFoM_LR) ;
  
  nok = nok_HR + nok_LR;
  ntot = ntot_HR + ntot_LR;
  //  pCat->completeness = (float) nok / (float) MAX(1,ntot);
  //---------------------------------------------------------------

  //TODO: This is a bodge:
  pCat->subFoM[1]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET1];
  pCat->subFoM[2]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET2];
  pCat->subFoM[3]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET3];
  pCat->subFoM[4]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET4];
  pCat->subFoM[5]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET5];
  pCat->subFoM[6]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET6];
  pCat->subFoM[7]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET7];
  pCat->subFoM[8]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET8];
  pCat->subFoM[9]  = subFoM_HR_subcat[SUBCATALOGUE_CODE_S9_HR_SUBSET9];

  pCat->subFoM[10+1] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET1];
  pCat->subFoM[10+2] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET2];
  pCat->subFoM[10+3] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET3];
  pCat->subFoM[10+4] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET4];
  pCat->subFoM[10+5] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET5];
  pCat->subFoM[10+6] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET6];
  pCat->subFoM[10+7] = subFoM_LR_subcat[SUBCATALOGUE_CODE_S9_LR_SUBSET7];

  

  fprintf(pFile, "%s CalcFoM survey= %10s Res=%s Ncompleted= %8d Ntotal= %8d FoM= %8.5f\n",
          RESULTS_PREFIX,
          pCat->sname,
          RESOLUTION_CODE_STRING(RESOLUTION_CODE_HIGH),
          nok_HR,
          ntot_HR,
          subFoM_HR); 
 fprintf(pFile, "%s CalcFoM survey= %10s Res=%s Ncompleted= %8d Ntotal= %8d FoM= %8.5f\n",
          RESULTS_PREFIX,
          pCat->sname,
          RESOLUTION_CODE_STRING(RESOLUTION_CODE_LOW),
          nok_LR,
          ntot_LR,
          subFoM_LR); 
 fprintf(pFile, "%s CalcFoM survey= %10s Ncompleted= %8d Ntotal= %8d FoM= %8.5f\n",
          RESULTS_PREFIX,
          pCat->sname,
          nok,
          ntot,
          pCat->FoM); 

  //print in the machine readable format
  OPSIM_FOMS_PRINT_SIMPLE_FOM(pFile,pCat->sname,pCat->FoM,nok,ntot,pCat->fiber_hours); 
  return 0;
}



//arbitrary
int calc_FoM_EXTRA(  objectlist_struct* pObjList, catalogue_struct* pCat, FILE* pFile) 
{
  if (pObjList == NULL || pCat == NULL || pFile == NULL ) return 1;
  if ( calc_FoM_simple_headcount((objectlist_struct*) pObjList,
                                      (catalogue_struct*) pCat,
                                      (FILE*) pFile,
                                      (int) MAX(1,pCat->num_objects) ))
  {
    return 1;
  }
  return 0;
}




//calculate the numbers of fiber-hours executed and fractional completeness
int calc_fiber_hours_completeness ( objectlist_struct* pObjList, catalogue_struct* pCat) 
{
  long i;
  long nok_hires = 0, ntot_hires = 0;
  long nok_lores = 0, ntot_lores = 0;
  if (pObjList == NULL || pCat == NULL  ) return 1;
  pCat->fiber_hours_moon[MOON_GROUP_DG] = 0.0;
  pCat->fiber_hours_moon[MOON_GROUP_BB] = 0.0;
  pCat->fiber_hours_lores_moon[MOON_GROUP_DG] = 0.0;
  pCat->fiber_hours_lores_moon[MOON_GROUP_BB] = 0.0;
  pCat->fiber_hours_hires_moon[MOON_GROUP_DG] = 0.0;
  pCat->fiber_hours_hires_moon[MOON_GROUP_BB] = 0.0;

  //  pCat->completeness       = 0.0;
  //  pCat->completeness_lores = 0.0;
  //  pCat->completeness_hires = 0.0;
  
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
  {
    int t;
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    for(t=0;t<pObj->num_fields;t++)
    {
      //      pCat->fiber_hours_moon[MOON_GROUP_BB] += pObj->ptexp[MOON_PHASE_BRIGHT][t];
      //      pCat->fiber_hours_moon[MOON_GROUP_DG] += (pObj->ptexp[MOON_PHASE_DARK][t] + pObj->ptexp[MOON_PHASE_GREY][t]);
      if ( pObj->res == RESOLUTION_CODE_LOW )
      {
        pCat->fiber_hours_lores_moon[MOON_GROUP_BB] += pObj->pObjField[t].ptexp[MOON_PHASE_BRIGHT];
        pCat->fiber_hours_lores_moon[MOON_GROUP_DG] += (pObj->pObjField[t].ptexp[MOON_PHASE_DARK] +
                                                        pObj->pObjField[t].ptexp[MOON_PHASE_GREY]);
      }
      else if ( pObj->res == RESOLUTION_CODE_HIGH )
      {
        pCat->fiber_hours_hires_moon[MOON_GROUP_BB] += pObj->pObjField[t].ptexp[MOON_PHASE_BRIGHT];
        pCat->fiber_hours_hires_moon[MOON_GROUP_DG] += (pObj->pObjField[t].ptexp[MOON_PHASE_DARK] +
                                                        pObj->pObjField[t].ptexp[MOON_PHASE_GREY]);
      }
      pCat->fiber_hours_moon[MOON_GROUP_BB] += pObj->pObjField[t].ptexp[MOON_PHASE_BRIGHT];
      pCat->fiber_hours_moon[MOON_GROUP_DG] += (pObj->pObjField[t].ptexp[MOON_PHASE_DARK] +
                                                pObj->pObjField[t].ptexp[MOON_PHASE_GREY]);
    }

    //this is for completeness calcs
    if ( pObj->res == RESOLUTION_CODE_LOW )
    {
      ntot_lores ++;
      if ( pObj->TexpFracDone >= 1.0 )  nok_lores ++;
    }
    else if ( pObj->res == RESOLUTION_CODE_HIGH )
    {
      ntot_hires ++;
      if ( pObj->TexpFracDone >= 1.0 )  nok_hires ++;
    }
  }
  pCat->fiber_hours_moon[MOON_GROUP_DG] *= MINS_TO_HOURS;
  pCat->fiber_hours_moon[MOON_GROUP_BB] *= MINS_TO_HOURS;
  pCat->fiber_hours_lores_moon[MOON_GROUP_DG] *= MINS_TO_HOURS;
  pCat->fiber_hours_lores_moon[MOON_GROUP_BB] *= MINS_TO_HOURS;
  pCat->fiber_hours_hires_moon[MOON_GROUP_DG] *= MINS_TO_HOURS;
  pCat->fiber_hours_hires_moon[MOON_GROUP_BB] *= MINS_TO_HOURS;
  pCat->fiber_hours_lores = pCat->fiber_hours_lores_moon[MOON_GROUP_DG] + pCat->fiber_hours_lores_moon[MOON_GROUP_BB];
  pCat->fiber_hours_hires = pCat->fiber_hours_hires_moon[MOON_GROUP_DG] + pCat->fiber_hours_hires_moon[MOON_GROUP_BB];
  pCat->fiber_hours = pCat->fiber_hours_moon[MOON_GROUP_DG] + pCat->fiber_hours_moon[MOON_GROUP_BB];

  pCat->completeness_lores =  (float) nok_lores / (float) MAX(1,ntot_lores);
  pCat->completeness_hires =  (float) nok_hires / (float) MAX(1,ntot_hires);
  pCat->completeness       =  (float) (nok_lores + nok_hires) / (float) MAX(1,ntot_lores+ntot_hires);
  return 0;
}




//////////////
int calc_FoMs (survey_struct* pSurvey, objectlist_struct* pObjList,
               cataloguelist_struct* pCatList, const char* str_filename, BOOL verbose)
{
  FILE* pFile = NULL;
  int i;
  clock_t clocks_start = clock();
  if (pObjList == NULL || pCatList == NULL || pSurvey == NULL ) return 1;
  
  //open up the output file
  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the FoM output file: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }
  if ( verbose )
  {
    fprintf(stdout, "%s Calculating FoMs for each catalogue, output at: %s ... ", COMMENT_PREFIX, str_filename);
    fflush(stdout);
  }
  
  for ( i=0;i<pCatList->num_cats;i++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[i]);
    int status = 0;
    //sum up the fiber hours that have been consumed
    if ( calc_fiber_hours_completeness ((objectlist_struct*) pObjList, (catalogue_struct*) pCat))
    {
      fprintf(stderr, "%s There was a problem calculating fiber hours for %s\n", ERROR_PREFIX,pCat->codename); 
      return 1;
    }

    
    switch (pCat->code)
    {
    case CATALOGUE_CODE_S1 :
      status = calc_FoM_S1((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S2 :
      status = calc_FoM_S2((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S3 :
      status = calc_FoM_S3((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S3_SUBCAT :
      status = calc_FoM_S3_subcat((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S4 :
      status = calc_FoM_S4((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S4_SUBCAT :
      status = calc_FoM_S4_subcat((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S5 :
      status = calc_FoM_S5 ((survey_struct*) pSurvey, (objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S6 :
      status = calc_FoM_S6 ((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S7 :
      status = calc_FoM_S7((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S8 :
      status = calc_FoM_S8 ((survey_struct*) pSurvey, (objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_S9 :
      status = calc_FoM_S9 ((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    case CATALOGUE_CODE_EXTRA :
      status = calc_FoM_EXTRA   ((objectlist_struct*) pObjList, (catalogue_struct*) pCat, (FILE*) pFile);
      break;
    default :
      fprintf(pFile, "%s I do not know how to calculate the FoM for the %s catalogue yet\n", WARNING_PREFIX, pCat->codename);
      status = 1;
      break;
    }

    if ( status)
    {
      fprintf(stderr, "%s There was a problem calculating output FoM statistics for %s\n", ERROR_PREFIX,pCat->codename); 
      return 1;
    }
  }

  if ( pFile ) fclose (pFile); pFile = NULL;
  if ( verbose )
  {
    fprintf(stdout, " done (that took %.3fs)\n", (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
    fflush(stdout);
  }
  return 0;
}


int plot_FoM_progress (cataloguelist_struct *pCatList, survey_struct *pSurvey, const char* str_dirname )
{
  FILE *pPlotfile = NULL;
  int c;
  char str_filename[STR_MAX];
  int line_colour;
  int point_type;
  float pointsize;

  snprintf(str_filename, STR_MAX, "%s/plot_%s.plot", str_dirname, FOM_PROGRESS_PLOT_STEM);
  //  fprintf(stdout, "%s Making plotfile of FoM progress: %s\n", COMMENT_PREFIX, str_filename); fflush(stdout);

  if ((pPlotfile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) str_dirname, ""))  return 1;
  
  if      ( pSurvey->FoM_report_cadence >= 60.0 ) pointsize = 1.3;
  else if ( pSurvey->FoM_report_cadence >= 30.0 ) pointsize = 1.0;
  else if ( pSurvey->FoM_report_cadence >= 14.0 ) pointsize = 0.8;
  else if ( pSurvey->FoM_report_cadence >= 7.0  ) pointsize = 0.6;
  else pointsize = 0.4;

  fprintf(pPlotfile, "reset\n\
set terminal pdfcairo enhanced colour font \"Times,18\" size 29.7cm,21.0cm\n\
set out \"%s.pdf\"\n\
survey_duration_days = %g\n",
          FOM_PROGRESS_PLOT_STEM,  ceil(pSurvey->JD_stop - pSurvey->JD_start));

  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    fprintf(pPlotfile, "!gawk -v splt='_' 'BEGIN {print \"#DayOfSurvey SurveyName FoM Ncomplete Ntotal Nfibre-hours\"} $1~/^Final_FoM/ && $2~/^%s/ {n=split(FILENAME,a,splt); print substr(a[n],1,4),\"Survey\",$2,$3,$4,$5,$6}' FoM_report_night_*.txt > FoM_progress_with_time_%s.txt\n",
            pCat->sname, pCat->sname);
    fprintf(pPlotfile, "infile_%-10s = \"FoM_progress_with_time_%s.txt\"\n",
            pCat->sname, pCat->sname);
    fprintf(pPlotfile, "final_FoM_%-10s = %g\n", pCat->sname, pCat->FoM);
  }

  fprintf(pPlotfile, "%s\n\
set key top left Left reverse samplen 2 font \",16\" \n\
set xlabel \"Day of survey\" font \",20\"\n\
set xtics 0,200,5000 nomirror\n\
set ytics 0,0.2,2.0\n\
set mytics 2\n\
set mxtics 2\n\
set x2tics 0,365.25,5000 nomirror\n\
set mx2tics 12\n\
set format x2 \"\"\n\
set x2tics scale 2,0.5\n\
set lmargin 12\n\
set rmargin 12\n\
set tmargin  8\n\
set bmargin  8\n\
set xrange [0:1.02*survey_duration_days]\n\
set arrow 1 from survey_duration_days,graph 0 to survey_duration_days,graph 1 nohead lt 1 dt 2 lc -1\n\
set pointsize %g\n", OPSIM_BRANDING_PLOTSTRING_SCREEN, pointsize);

   fprintf(pPlotfile, "set title \"4FS - Survey FoM progress\" font \",18\"\n\
set yrange [0:1.05]\n\
set ylabel \"FoM\" offset 0.5,0 font \",20\"\n\
plot [][]\\\n\
    (1.0) with lines lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines lt 1 dt 2 lc -1 not,\\\n");

  point_type = 1;
  line_colour = 1;
  
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    fprintf(pPlotfile, "    infile_%s using 1 : 4 with linespoints lc %d lw 3 pt %d t \"%s\" ",
            pCat->sname, line_colour, point_type, pCat->displayname);
    line_colour ++;
    point_type ++;
    while ( line_colour >= 10 && line_colour <= 12 ) line_colour ++; //avoid grey lines
    if ( c < pCatList->num_cats - 1 )
    {
      fprintf(pPlotfile, ",\\");
    }
    fprintf(pPlotfile, "\n");
  }
  fprintf(pPlotfile, "\n\n\n");

  //now plot the same again, but with progress normalised to goal FoM 
  fprintf(pPlotfile, "set title \"4FS - Survey FoM progress - Normalised by linear progress to 0.5\" font \",18\"\n\
set ylabel \"FoM_{Norm}(t) = [FoM(t)/0.5]*[t/T_{survey}]\" offset 0.5,0 font \",20\"\n\
set key top right Left reverse\n\
set yrange [0:5]\n\
set ytics autofreq\n\
plot [][]\\\n\
    (1.0) with lines lt 1 dt 2 lc -1 not,\\\n");
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    fprintf(pPlotfile, "    infile_%s using 1 : ($4/(($1*0.5)/survey_duration_days)) with linespoints lc %d lw 3 pt %d t \"%s\" ",
            pCat->sname, line_colour, point_type, pCat->displayname);
    line_colour ++;
    point_type ++;
    while ( line_colour >= 10 && line_colour <= 12 ) line_colour ++; //avoid grey lines
    if ( c < pCatList->num_cats - 1 )
    {
      fprintf(pPlotfile, ",\\");
    }
    fprintf(pPlotfile, "\n");
  }
  fprintf(pPlotfile, "\n\n\n");

   //now plot the same again, but with progress normalised to final FoM 
  fprintf(pPlotfile, "set title \"4FS - Survey FoM progress - Normalised by linear progress to FoM_{final}\" font \",18\"\n\
set ylabel \"FoM_{Norm}(t) = [FoM(t)/FoM_{final}]*[t/T_{survey}]\" offset 0.5,0 font \",20\"\n\
set key top right Left reverse\n\
set yrange [0:2.5]\n\
set ytics autofreq\n\
plot [][]\\\n\
    (1.0) with lines lt 1 dt 2 lc -1 not,\\\n");
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    fprintf(pPlotfile, "    infile_%s using 1 : ($4/(($1*final_FoM_%s)/survey_duration_days)) with linespoints lc %d lw 3 pt %d t \"%s\" ",
            pCat->sname, pCat->sname, line_colour, point_type, pCat->displayname);
    line_colour ++;
    point_type ++;
    while ( line_colour >= 10 && line_colour <= 12 ) line_colour ++; //avoid grey lines
    if ( c < pCatList->num_cats - 1 )
    {
      fprintf(pPlotfile, ",\\");
    }
    fprintf(pPlotfile, "\n");
  }
  fprintf(pPlotfile, "\n\n\n");

 
  //now write the completeness curves
  fprintf(pPlotfile, "set title \"4FS - Survey completeness progress\" font \",18\"\n\
set ylabel \"Completeness (N_{success}/N_{total})\" offset 0.5,0 font \",20\"\n\
set key top left Left reverse \n\
set yrange [0:1.05]\n\
set ytics 0.0,0.2,2\n\
plot [][]\\\n\
    (1.0) with lines lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines lt 1 dt 2 lc -1 not,\\\n");

  point_type = 1;
  line_colour = 1;
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    fprintf(pPlotfile, "    infile_%s using 1 : ($5/$6) with linespoints lc %d lw 3 pt %d t \"%s\" ",
            pCat->sname, line_colour, point_type, pCat->displayname);
    line_colour ++;
    point_type ++;
    while ( line_colour >= 10 && line_colour <= 12 ) line_colour ++; //avoid grey lines
    if ( c < pCatList->num_cats - 1 )
    {
      fprintf(pPlotfile, ",\\");
    }
    fprintf(pPlotfile, "\n");
  }
  fprintf(pPlotfile, "\n\n\n\n\n");


  
  //now write the sub-FoM plot instructions
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    if ( pCat->code == CATALOGUE_CODE_S3  ||
         pCat->code == CATALOGUE_CODE_S4  ||
         pCat->code == CATALOGUE_CODE_S5  ||
         pCat->code == CATALOGUE_CODE_S6  ||
         pCat->code == CATALOGUE_CODE_S7  ||
         pCat->code == CATALOGUE_CODE_S8  ||
         pCat->code == CATALOGUE_CODE_S9  )
    {

      fprintf(pPlotfile, "set ylabel \"FoM, subFoM\" offset 0.5,0 font \",20\"\n\
set title \"Survey subFoM progress - %s\" font \",20\"\n", pCat->displayname);
      

      if ( pCat->code == CATALOGUE_CODE_S3 )
      {
        //print subset infiles as dayofsurvey,subFoM,Ncomplete,Ntotal,Ngoal
        
        fprintf(pPlotfile,"\n\
!gawk -v splt='FoM_report_night_' '$0~/ S3 subFoM=ESN/      {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S3_subcat_ESN.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S3 subFoM=DynDisk1/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S3_subcat_DynDisk1.txt\n \
!gawk -v splt='FoM_report_night_' '$0~/ S3 subFoM=DynDisk2/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S3_subcat_DynDisk2.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S3 subFoM=ChemDisk/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S3_subcat_ChemDisk.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S3 subFoM=Bulge/    {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S3_subcat_Bulge.txt\n");

        fprintf(pPlotfile, "\n\
infile_S3_ESN          = \"FoM_progress_with_time_S3_subcat_ESN.txt\"\n\
infile_S3_DynDisk1     = \"FoM_progress_with_time_S3_subcat_DynDisk1.txt\"\n\
infile_S3_DynDisk2     = \"FoM_progress_with_time_S3_subcat_DynDisk2.txt\"\n\
infile_S3_ChemDisk     = \"FoM_progress_with_time_S3_subcat_ChemDisk.txt\"\n\
infile_S3_Bulge        = \"FoM_progress_with_time_S3_subcat_Bulge.txt\"\n");

        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S3          using 1 : 4 with lines       lw 3 lc -1 lt 1      t \"Survey FoM\" ,\\\n\
    infile_S3_ESN      using 1 : 2 with linespoints lw 2 lc  1 dt 2 pt 2 t \"subcat: ESN\" ,\\\n\
    infile_S3_DynDisk1 using 1 : 2 with linespoints lw 2 lc  2 dt 3 pt 3 t \"subcat: DynDisk1\" ,\\\n\
    infile_S3_DynDisk2 using 1 : 2 with linespoints lw 2 lc  3 dt 4 pt 4 t \"subcat: DynDisk2\" ,\\\n\
    infile_S3_ChemDisk using 1 : 2 with linespoints lw 2 lc  4 dt 5 pt 5 t \"subcat: ChemDisk\" ,\\\n\
    infile_S3_Bulge    using 1 : 2 with linespoints lw 2 lc  5 dt 6 pt 6 t \"subcat: Bulge\" \n\n\n");

        fprintf(pPlotfile, "set ylabel \"N_{ok}/N_{goal} for each subcat\" offset 0.5,0 font \",20\"\n\
set title \"Survey sub-catalogue progress to N_{goal} - %s\" font \",20\"\n", pCat->displayname);
      
        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S3_ESN      using 1 : ($3/$5) with linespoints lw 2 lc  1 dt 2 pt 2 t \"subcat: ESN\" ,\\\n\
    infile_S3_DynDisk1 using 1 : ($3/$5) with linespoints lw 2 lc  2 dt 3 pt 3 t \"subcat: DynDisk1\" ,\\\n\
    infile_S3_DynDisk2 using 1 : ($3/$5) with linespoints lw 2 lc  3 dt 4 pt 4 t \"subcat: DynDisk2\" ,\\\n\
    infile_S3_ChemDisk using 1 : ($3/$5) with linespoints lw 2 lc  4 dt 5 pt 5 t \"subcat: ChemDisk\" ,\\\n\
    infile_S3_Bulge    using 1 : ($3/$5) with linespoints lw 2 lc  5 dt 6 pt 6 t \"subcat: Bulge\" \n\n\n");
      }

      if ( pCat->code == CATALOGUE_CODE_S4 )
      {
        //print subset infiles as dayofsurvey,subFoM,Ncomplete,Ntotal,Ngoal
        fprintf(pPlotfile, "\n\
!gawk -v splt='FoM_report_night_' '$0~/ S4 subFoM=Bulge/         {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S4_subcat_Bulge.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S4 subFoM=InnerDisk/     {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S4_subcat_InnerDisk.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S4 subFoM=OuterDisk/     {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S4_subcat_OuterDisk.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S4 subFoM=ZUnbiasedDisk/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$13,$11,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S4_subcat_ZUnbiasedDisk.txt\n");

        fprintf(pPlotfile, "\n\
infile_S4_Bulge          = \"FoM_progress_with_time_S4_subcat_Bulge.txt\"\n\
infile_S4_InnerDisk      = \"FoM_progress_with_time_S4_subcat_InnerDisk.txt\"\n\
infile_S4_OuterDisk      = \"FoM_progress_with_time_S4_subcat_OuterDisk.txt\"\n\
infile_S4_ZUnbiasedDisk  = \"FoM_progress_with_time_S4_subcat_ZUnbiasedDisk.txt\"\n\n");

        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S4               using 1 : 4 with lines       lw 3 lc -1 lt 1      t \"Survey FoM\",\\\n\
    infile_S4_InnerDisk     using 1 : 2 with linespoints lw 2 lc  1 dt 3 pt 3 t \"subcat: InnerDisk\",\\\n\
    infile_S4_OuterDisk     using 1 : 2 with linespoints lw 2 lc  2 dt 2 pt 2 t \"subcat: OuterDisk\",\\\n\
    infile_S4_Bulge         using 1 : 2 with linespoints lw 2 lc  3 dt 4 pt 4 t \"subcat: Bulge\",\\\n\
    infile_S4_ZUnbiasedDisk using 1 : 2 with linespoints lw 2 lc  4 dt 3 pt 3 t \"subcat: ZUnbiasedDisk\"\n");

        fprintf(pPlotfile, "set ylabel \"N_{ok}/N_{goal} for each subcat\" offset 0.5,0 font \",20\"\n\
set title \"Survey sub-catalogue progress to N_{goal} - %s\" font \",20\"\n", pCat->displayname);

        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S4_Bulge         using 1 : ($3/$5) with linespoints lw 2 lc  1 dt 4 pt 4 t \"subcat: Bulge\",\\\n\
    infile_S4_InnerDisk     using 1 : ($3/$5) with linespoints lw 2 lc  2 dt 3 pt 3 t \"subcat: InnerDisk\",\\\n\
    infile_S4_OuterDisk     using 1 : ($3/$5) with linespoints lw 2 lc  3 dt 2 pt 2 t \"subcat: OuterDisk\",\\\n\
    infile_S4_ZUnbiasedDisk using 1 : ($3/$5) with linespoints lw 2 lc  4 dt 2 pt 2 t \"subcat: ZUnbiasedDisk\" \n");
      }

      if (pCat->code == CATALOGUE_CODE_S5 )
      {
        
        fprintf(pPlotfile, "\n\
!gawk -v splt='FoM_report_night_' '$0~/ S5 delatFoM zbin=0 / {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$17}' FoM_report_night_*.txt > FoM_progress_with_time_S5_subcat_z0.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S5 delatFoM zbin=1 / {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$17}' FoM_report_night_*.txt > FoM_progress_with_time_S5_subcat_z1.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S5 delatFoM zbin=2 / {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$17}' FoM_report_night_*.txt > FoM_progress_with_time_S5_subcat_z2.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S5 delatFoM zbin=3 / {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$17}' FoM_report_night_*.txt > FoM_progress_with_time_S5_subcat_z3.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S5 delatFoM zbin=4 / {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$17}' FoM_report_night_*.txt > FoM_progress_with_time_S5_subcat_z4.txt\n");
        
        fprintf(pPlotfile, "\n\
infile_S5_z0  = \"FoM_progress_with_time_S5_subcat_z0.txt\"\n\
infile_S5_z1  = \"FoM_progress_with_time_S5_subcat_z1.txt\"\n\
infile_S5_z2  = \"FoM_progress_with_time_S5_subcat_z2.txt\"\n\
infile_S5_z3  = \"FoM_progress_with_time_S5_subcat_z3.txt\"\n\
infile_S5_z4  = \"FoM_progress_with_time_S5_subcat_z4.txt\"\n");

        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S5    using 1 : 4 with lines       lw 3 lc -1      lt 1 t \"Survey FoM\" ,\\\n\
    infile_S5_z0 using 1 : 2 with linespoints lw 2 lc  1 pt 2 dt 2 t \"subFoM: 0.0<z<0.2\" ,\\\n\
    infile_S5_z1 using 1 : 2 with linespoints lw 2 lc  2 pt 3 dt 3 t \"subFoM: 0.2<z<0.4\" ,\\\n\
    infile_S5_z2 using 1 : 2 with linespoints lw 2 lc  3 pt 4 dt 4 t \"subFoM: 0.4<z<0.6\" ,\\\n\
    infile_S5_z3 using 1 : 2 with linespoints lw 2 lc  4 pt 5 dt 5 t \"subFoM: 0.6<z<0.8\" ,\\\n\
    infile_S5_z4 using 1 : 2 with linespoints lw 2 lc  5 pt 6 dt 6 t \"subFoM: 0.8<z<1.0\"\n\n\n");
      }
      
      if (pCat->code == CATALOGUE_CODE_S6 )
      {
        fprintf(pPlotfile, "\n\
!gawk -v splt='FoM_report_night_' '$0~/ S6 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$8}' FoM_report_night_*.txt  > FoM_progress_with_time_S6_subcat_overall.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S6 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$10}' FoM_report_night_*.txt > FoM_progress_with_time_S6_subcat_z.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S6 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$12}' FoM_report_night_*.txt > FoM_progress_with_time_S6_subcat_mag.txt\n");
        fprintf(pPlotfile, "\n\
infile_S6_overall = \"FoM_progress_with_time_S6_subcat_overall.txt\"\n\
infile_S6_z       = \"FoM_progress_with_time_S6_subcat_z.txt\"\n\
infile_S6_mag     = \"FoM_progress_with_time_S6_subcat_mag.txt\"\n");

        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S6         using 1 : 4 with lines       lw 3 lc -1      lt 1 t \"Survey FoM\" ,\\\n\
    infile_S6_overall using 1 : 2 with linespoints lw 2 lc  1 pt 2 dt 2 t \"subFoM: Completeness overall\" ,\\\n\
    infile_S6_z       using 1 : 2 with linespoints lw 2 lc  2 pt 3 dt 3 t \"subFoM: Completeness by redshift bins\",\\\n\
    infile_S6_mag     using 1 : 2 with linespoints lw 2 lc  3 pt 4 dt 4 t \"subFoM: Completeness by magnitude bins\"\n\n\n");
      }

      if ( pCat->code == CATALOGUE_CODE_S7 )
      {
        fprintf(pPlotfile, "\n\
!gawk -v splt='FoM_report_night_' '$0~/ S7 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$8}' FoM_report_night_*.txt > FoM_progress_with_time_S7_subcat_wide.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S7 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$10}' FoM_report_night_*.txt > FoM_progress_with_time_S7_subcat_deep.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S7 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$12}' FoM_report_night_*.txt > FoM_progress_with_time_S7_subcat_overall.txt\n");

        fprintf(pPlotfile, "\n\
infile_S7_overall = \"FoM_progress_with_time_S7_subcat_overall.txt\"\n\
infile_S7_wide    = \"FoM_progress_with_time_S7_subcat_wide.txt\"\n\
infile_S7_deep    = \"FoM_progress_with_time_S7_subcat_deep.txt\"\n");
        
        fprintf(pPlotfile, "\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S7_overall using 1 : 2 with lines       lw 3 lc -1 lt 1      t \"Survey FoM\" ,\\\n\
    infile_S7_wide    using 1 : 2 with linespoints lw 2 lc  1 pt 2 dt 2 t \"subcat: Wide\",\\\n\
    infile_S7_deep    using 1 : 2 with linespoints lw 2 lc  2 pt 3 dt 3 t \"subcat: Deep\"\n\n\n");
      }

      if ( pCat->code == CATALOGUE_CODE_S8  )
      {
        fprintf(pPlotfile, "\
infile_S8_area  = \"< gawk -v splt='FoM_report_night_' '$0~/ S8 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$8}' FoM_report_night_*.txt\"\n \
infile_S8_num   = \"< gawk -v splt='FoM_report_night_' '$0~/ S8 Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$10}' FoM_report_night_*.txt\"\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S8       using 1 : 4 with lines       lw 3 lc -1      lt 1 t \"Survey FoM\" ,\\\n\
    infile_S8_area  using 1 : 2 with linespoints lw 2 lc  1 pt 2 dt 2 t \"subFoM: Completeness by area\",\\\n\
    infile_S8_num   using 1 : 2 with linespoints lw 2 lc  2 pt 3 dt 3 t \"subFoM: Completeness by number\"\n\n\n");
        /*
          fprintf(pPlotfile, "infile_S8_area  = \"< gawk -v splt='FoM_report_night_' '$0~/ S8 Ncompleted/ && $8==300 {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$10}' FoM_report_night_*.txt\"\n \
infile_S8_num   = \"< gawk -v splt='FoM_report_night_' '$0~/ S8 Ncompleted/ && $8==300 {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$12}' FoM_report_night_*.txt\"\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S8       using 1 : 4 with lines       lw 3 lc -1      lt 1 t \"Survey FoM\" ,\\\n\
    infile_S8_area  using 1 : 2 with linespoints lw 2 lc  1 pt 2 dt 2 t \"subFoM: Completeness by area\",\\\n\
    infile_S8_num   using 1 : 2 with linespoints lw 2 lc  2 pt 3 dt 3 t \"subFoM: Completeness by number\"\n\n\n");
        */
      }
          
      if ( pCat->code == CATALOGUE_CODE_S9  )
      {
        
        fprintf(pPlotfile, "\n\
!gawk -v splt='FoM_report_night_' '$0~/ S9 Res=HR Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$11,$7,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR.txt\n\
!gawk -v splt='FoM_report_night_' '$0~/ S9 Res=LR Ncompleted/ {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$11,$7,$9}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR.txt\n");
        
        fprintf(pPlotfile, "\n\
!gawk -v subcat=1  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat1.txt\n\
!gawk -v subcat=2  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat2.txt\n\
!gawk -v subcat=3  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat3.txt\n\
!gawk -v subcat=4  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat4.txt\n\
!gawk -v subcat=5  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat5.txt\n\
!gawk -v subcat=6  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat6.txt\n\
!gawk -v subcat=7  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat7.txt\n\
!gawk -v subcat=8  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat8.txt\n\
!gawk -v subcat=9  -v splt='FoM_report_night_' '$0~/ S9 Res=HR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_HR_subcat9.txt\n");
        fprintf(pPlotfile, "\n\
!gawk -v subcat=1  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat1.txt\n\
!gawk -v subcat=2  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat2.txt\n\
!gawk -v subcat=3  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat3.txt\n\
!gawk -v subcat=4  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat4.txt\n\
!gawk -v subcat=5  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat5.txt\n\
!gawk -v subcat=6  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat6.txt\n\
!gawk -v subcat=7  -v splt='FoM_report_night_' '$0~/ S9 Res=LR/ && $7==subcat {split(FILENAME,a,splt);print (0.+substr(a[2],1,4)),$15,$9,$11,$13}' FoM_report_night_*.txt > FoM_progress_with_time_S9_LR_subcat7.txt\n");


        fprintf(pPlotfile, "\n\
infile_S9_HR           = \"FoM_progress_with_time_S9_HR.txt\"\n\
infile_S9_HR_subcat1   = \"FoM_progress_with_time_S9_HR_subcat1.txt\"\n\
infile_S9_HR_subcat2   = \"FoM_progress_with_time_S9_HR_subcat2.txt\"\n\
infile_S9_HR_subcat3   = \"FoM_progress_with_time_S9_HR_subcat3.txt\"\n\
infile_S9_HR_subcat4   = \"FoM_progress_with_time_S9_HR_subcat4.txt\"\n\
infile_S9_HR_subcat5   = \"FoM_progress_with_time_S9_HR_subcat5.txt\"\n\
infile_S9_HR_subcat6   = \"FoM_progress_with_time_S9_HR_subcat6.txt\"\n\
infile_S9_HR_subcat7   = \"FoM_progress_with_time_S9_HR_subcat7.txt\"\n\
infile_S9_HR_subcat8   = \"FoM_progress_with_time_S9_HR_subcat8.txt\"\n\
infile_S9_HR_subcat9   = \"FoM_progress_with_time_S9_HR_subcat9.txt\"\n\
infile_S9_LR           = \"FoM_progress_with_time_S9_LR.txt\"\n\
infile_S9_LR_subcat1   = \"FoM_progress_with_time_S9_LR_subcat1.txt\"\n\
infile_S9_LR_subcat2   = \"FoM_progress_with_time_S9_LR_subcat2.txt\"\n\
infile_S9_LR_subcat3   = \"FoM_progress_with_time_S9_LR_subcat3.txt\"\n\
infile_S9_LR_subcat4   = \"FoM_progress_with_time_S9_LR_subcat4.txt\"\n\
infile_S9_LR_subcat5   = \"FoM_progress_with_time_S9_LR_subcat5.txt\"\n\
infile_S9_LR_subcat6   = \"FoM_progress_with_time_S9_LR_subcat6.txt\"\n\
infile_S9_LR_subcat7   = \"FoM_progress_with_time_S9_LR_subcat7.txt\"\n\n\
set key font \",12\" samplen 3\n\
set key opaque box maxrows 10\n\
set key right at graph 0.9,0.05\n\
set pointsize %g\n\n",
                pointsize*0.7);


        
        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    (0.5) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S9             using 1 : 4 with linespoints lw 2 lc -1 lt 1 pt 1 t \"Survey FoM\",\\\n\
    infile_S9_HR          using 1 : 2 with linespoints lw 2 lc -1 dt 2 pt 2 t \"subFoM: HR\",\\\n\
    infile_S9_LR          using 1 : 2 with linespoints lw 2 lc -1 dt 3 pt 3 t \"subFoM: LR\",\\\n\
    infile_S9_HR_subcat1  using 1 : 2 with linespoints lw 1 lc  1 dt 2 pt 4 t \"subFoM: HR SubCat 1\",\\\n\
    infile_S9_HR_subcat2  using 1 : 2 with linespoints lw 1 lc  2 dt 2 pt 4 t \"subFoM: HR SubCat 2\",\\\n\
    infile_S9_HR_subcat3  using 1 : 2 with linespoints lw 1 lc  3 dt 2 pt 4 t \"subFoM: HR SubCat 3\",\\\n\
    infile_S9_HR_subcat4  using 1 : 2 with linespoints lw 1 lc  4 dt 2 pt 4 t \"subFoM: HR SubCat 4\",\\\n\
    infile_S9_HR_subcat5  using 1 : 2 with linespoints lw 1 lc  5 dt 2 pt 4 t \"subFoM: HR SubCat 5\",\\\n\
    infile_S9_HR_subcat6  using 1 : 2 with linespoints lw 1 lc  6 dt 2 pt 4 t \"subFoM: HR SubCat 6\",\\\n\
    infile_S9_HR_subcat7  using 1 : 2 with linespoints lw 1 lc  7 dt 2 pt 4 t \"subFoM: HR SubCat 7\",\\\n\
    infile_S9_HR_subcat8  using 1 : 2 with linespoints lw 1 lc  8 dt 2 pt 4 t \"subFoM: HR SubCat 8\",\\\n\
    infile_S9_HR_subcat9  using 1 : 2 with linespoints lw 1 lc  9 dt 2 pt 4 t \"subFoM: HR SubCat 9\",\\\n\
    infile_S9_LR_subcat1  using 1 : 2 with linespoints lw 1 lc  1 dt 3 pt 6 t \"subFoM: LR SubCat 1\",\\\n\
    infile_S9_LR_subcat2  using 1 : 2 with linespoints lw 1 lc  2 dt 3 pt 6 t \"subFoM: LR SubCat 2\",\\\n\
    infile_S9_LR_subcat3  using 1 : 2 with linespoints lw 1 lc  3 dt 3 pt 6 t \"subFoM: LR SubCat 3\",\\\n\
    infile_S9_LR_subcat4  using 1 : 2 with linespoints lw 1 lc  4 dt 3 pt 6 t \"subFoM: LR SubCat 4\",\\\n\
    infile_S9_LR_subcat5  using 1 : 2 with linespoints lw 1 lc  5 dt 3 pt 6 t \"subFoM: LR SubCat 5\",\\\n\
    infile_S9_LR_subcat6  using 1 : 2 with linespoints lw 1 lc  6 dt 3 pt 6 t \"subFoM: LR SubCat 6\",\\\n\
    infile_S9_LR_subcat7  using 1 : 2 with linespoints lw 1 lc  7 dt 3 pt 6 t \"subFoM: LR SubCat 7\"\n\n");

        fprintf(pPlotfile, "\nset ylabel \"N_{ok}/N_{goal} for each subcat\" offset 0.5,0 font \",20\"\n\
set title \"Survey sub-catalogue progress to N_{goal} - %s\" font \",20\"\n", pCat->displayname);
      
        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S9_HR_subcat1  using 1 : ($3/$5) with linespoints lw 1 lc  1 dt 2 pt 4 t \"HR SubCat 1\",\\\n\
    infile_S9_HR_subcat2  using 1 : ($3/$5) with linespoints lw 1 lc  2 dt 2 pt 4 t \"HR SubCat 2\",\\\n\
    infile_S9_HR_subcat3  using 1 : ($3/$5) with linespoints lw 1 lc  3 dt 2 pt 4 t \"HR SubCat 3\",\\\n\
    infile_S9_HR_subcat4  using 1 : ($3/$5) with linespoints lw 1 lc  4 dt 2 pt 4 t \"HR SubCat 4\",\\\n\
    infile_S9_HR_subcat5  using 1 : ($3/$5) with linespoints lw 1 lc  5 dt 2 pt 4 t \"HR SubCat 5\",\\\n\
    infile_S9_HR_subcat6  using 1 : ($3/$5) with linespoints lw 1 lc  6 dt 2 pt 4 t \"HR SubCat 6\",\\\n\
    infile_S9_HR_subcat7  using 1 : ($3/$5) with linespoints lw 1 lc  7 dt 2 pt 4 t \"HR SubCat 7\",\\\n\
    infile_S9_HR_subcat8  using 1 : ($3/$5) with linespoints lw 1 lc  8 dt 2 pt 4 t \"HR SubCat 8\",\\\n\
    infile_S9_HR_subcat9  using 1 : ($3/$5) with linespoints lw 1 lc  9 dt 2 pt 4 t \"HR SubCat 9\",\\\n\
    infile_S9_LR_subcat1  using 1 : ($3/$5) with linespoints lw 1 lc  1 dt 3 pt 6 t \"LR SubCat 1\",\\\n\
    infile_S9_LR_subcat2  using 1 : ($3/$5) with linespoints lw 1 lc  2 dt 3 pt 6 t \"LR SubCat 2\",\\\n\
    infile_S9_LR_subcat3  using 1 : ($3/$5) with linespoints lw 1 lc  3 dt 3 pt 6 t \"LR SubCat 3\",\\\n\
    infile_S9_LR_subcat4  using 1 : ($3/$5) with linespoints lw 1 lc  4 dt 3 pt 6 t \"LR SubCat 4\",\\\n\
    infile_S9_LR_subcat5  using 1 : ($3/$5) with linespoints lw 1 lc  5 dt 3 pt 6 t \"LR SubCat 5\",\\\n\
    infile_S9_LR_subcat6  using 1 : ($3/$5) with linespoints lw 1 lc  6 dt 3 pt 6 t \"LR SubCat 6\",\\\n\
    infile_S9_LR_subcat7  using 1 : ($3/$5) with linespoints lw 1 lc  7 dt 3 pt 6 t \"LR SubCat 7\"\n\n");

        fprintf(pPlotfile, "\nset ylabel \"N_{ok}/N_{total} for each subcat\" offset 0.5,0 font \",20\"\n\
set title \"Survey sub-catalogue progress to N_{total} - %s\" font \",20\"\n", pCat->displayname);
      
        fprintf(pPlotfile, "\n\
plot []\\\n\
    (1.0) with lines  lt 1 dt 2 lc -1 not,\\\n\
    infile_S9_HR_subcat1  using 1 : ($3/$4) with linespoints lw 1 lc  1 dt 2 pt 4 t \"HR SubCat 1\",\\\n\
    infile_S9_HR_subcat2  using 1 : ($3/$4) with linespoints lw 1 lc  2 dt 2 pt 4 t \"HR SubCat 2\",\\\n\
    infile_S9_HR_subcat3  using 1 : ($3/$4) with linespoints lw 1 lc  3 dt 2 pt 4 t \"HR SubCat 3\",\\\n\
    infile_S9_HR_subcat4  using 1 : ($3/$4) with linespoints lw 1 lc  4 dt 2 pt 4 t \"HR SubCat 4\",\\\n\
    infile_S9_HR_subcat5  using 1 : ($3/$4) with linespoints lw 1 lc  5 dt 2 pt 4 t \"HR SubCat 5\",\\\n\
    infile_S9_HR_subcat6  using 1 : ($3/$4) with linespoints lw 1 lc  6 dt 2 pt 4 t \"HR SubCat 6\",\\\n\
    infile_S9_HR_subcat7  using 1 : ($3/$4) with linespoints lw 1 lc  7 dt 2 pt 4 t \"HR SubCat 7\",\\\n\
    infile_S9_HR_subcat8  using 1 : ($3/$4) with linespoints lw 1 lc  8 dt 2 pt 4 t \"HR SubCat 8\",\\\n\
    infile_S9_HR_subcat9  using 1 : ($3/$4) with linespoints lw 1 lc  9 dt 2 pt 4 t \"HR SubCat 9\",\\\n\
    infile_S9_LR_subcat1  using 1 : ($3/$4) with linespoints lw 1 lc  1 dt 3 pt 6 t \"LR SubCat 1\",\\\n\
    infile_S9_LR_subcat2  using 1 : ($3/$4) with linespoints lw 1 lc  2 dt 3 pt 6 t \"LR SubCat 2\",\\\n\
    infile_S9_LR_subcat3  using 1 : ($3/$4) with linespoints lw 1 lc  3 dt 3 pt 6 t \"LR SubCat 3\",\\\n\
    infile_S9_LR_subcat4  using 1 : ($3/$4) with linespoints lw 1 lc  4 dt 3 pt 6 t \"LR SubCat 4\",\\\n\
    infile_S9_LR_subcat5  using 1 : ($3/$4) with linespoints lw 1 lc  5 dt 3 pt 6 t \"LR SubCat 5\",\\\n\
    infile_S9_LR_subcat6  using 1 : ($3/$4) with linespoints lw 1 lc  6 dt 3 pt 6 t \"LR SubCat 6\",\\\n\
    infile_S9_LR_subcat7  using 1 : ($3/$4) with linespoints lw 1 lc  7 dt 3 pt 6 t \"LR SubCat 7\"\n\n");


      }
    }
  }
  
  //add a line to make a thumbnail image
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, FOM_PROGRESS_PLOT_STEM, FOM_PROGRESS_PLOT_STEM);
  
  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;

  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_filename)) return 1;

  return 0;
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
///////////////////////END OF FOMS///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
