//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_OB_lib.h
//--    Use: This is a header for the OpSim_OB_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_OB_H
#define OPSIM_OB_H

#define CVS_REVISION_OPSIM_OB_LIB_H "$Revision: 1.1 $"
#define CVS_DATE_OPSIM_OB_LIB_H     "$Date: 2015/09/09 10:10:31 $"

#include <stdio.h>

#include "define.h"
#include "utensils_lib.h"
#include "4FS_OpSim.h"
#include "OpSim_timeline_lib.h"
#include "OpSim_telescope_site_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.1 $"
//#define CVS_DATE_H     "$Date: 2015/09/09 10:10:31 $"
inline static int OpSim_OB_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

//stuff for ESO OB scheduling
#define MAX_OBS_IN_POOL    1000            //!< TODO migrate this to a variable
#define MAX_TILES_PER_OB   3               //!< TODO migrate this to a variable

#define OB_STATUS_NULL       0
#define OB_STATUS_READY      1
#define OB_STATUS_OBSERVED   2
#define OB_STATUS_FAILED     3

#define OB_STATUS_STR_NULL       "NULL"
#define OB_STATUS_STR_READY      "READY"
#define OB_STATUS_STR_OBSERVED   "OBSERVED"
#define OB_STATUS_STR_FAILED     "FAILED"

////////////////////////////////////////////////////////////////////////////
//      The following are structures used to define a pool of OBs         //
////////////////////////////////////////////////////////////////////////////


typedef struct {
  int UID;                   //universal ID code
  int array_length;          //actual length of the array pointed to by pStep
  int nTiles;                //number of tiles to be executed in this OB 
  tile_struct *pTile;        //pointer to array of tile_structs

  float time_gross;          //total time required to execute all tiles+cal for this OB (seconds)

  int status;                //a flag giving the status of the OB
  double JD_observed;        //the JD date when the OB was (started) observed.
} OB_struct;



typedef struct {
  int array_length;          //actual length of the array pointed to by pOB
  int nOBs;                  //number of OBs currently in the pOB array
  OB_struct *pOB;            //pointer to array of OB_structs

  //need to include some kind of sorting array structure here
  sortinglist_struct SortingList;
} OBlist_struct;
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int OB_struct_init         (OB_struct *pOB, int tiles_per_OB, int assigns_per_tile);
int OB_struct_free         (OB_struct **ppOB);
int OB_struct_write        (OB_struct *pOB, FILE *pFile, const char *str_prefix);
int OB_struct_write_file   (OB_struct *pOB, const char *str_filename);

int OBlist_struct_init     (OBlist_struct **ppOBlist, int total_num_OBs, int tiles_per_OB, int assigns_per_tile);
int OBlist_struct_free     (OBlist_struct **ppOBlist);
int OBlist_struct_choose_OB   (OBlist_struct *pOBlist, survey_struct *pSurvey, timeline_struct *pTime );
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


float ESO_OT_calc_Pseeing         (environment_struct *pEnviron, float airmass_min, float seeing_requested );
float ESO_OT_calc_hz              (double latitude, double dec_field, float airmass_max );
float ESO_OT_calc_Pz              (double latitude, float hz);
float ESO_OT_calc_Psky            (environment_struct *pEnviron, int cloud_cover_code );
float ESO_OT_calc_Psetting        (double ra_field, double st_dusk, float hz );


#endif
