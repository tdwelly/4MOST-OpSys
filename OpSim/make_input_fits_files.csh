#!/bin/tcsh -f

#convert the input ascii catalogue files into FITS format


set BASE = ${HOME}/4most

set CATLIST = ( AGN BAO GalHaloLR GalDiskLR Clusters GalHaloHR GalDiskHR RANDOM ) 
#set CATLIST = ( GalDiskLR GalDiskHR ) 
#set CATLIST = (GalHaloHR  ) 

#if ( $#argv > 0 ) 
#  set CATLIST = ( $argv ) 
#endif
echo "I will make .fits catalogues for the following DRSs: $CATLIST"

foreach CAT ( $CATLIST ) 

  cd ${BASE}/${CAT}_in/
  #write a ftcreate header file
  echo "NAME       15A \
NUMBER      1J \
RA          1D [deg,J2000]\
DEC         1D [deg,J2000]\
PRIORITY    1I \
TEXP_DARK   1I [min]\
TEXP_GREY   1I [min]\
TEXP_BRIGHT 1I [min]\
RESOLUTION  1B \
MAG         1E [mag,AB]\
SPECTRUM   30A " > fcreate_headfile.txt

  if ( "$CAT" == "AGN" ) then
    echo "XRAY_FLUX     1E [cgs,0.5-2keV]\
REDSHIFT     1E \
LOG_XRAY_LUM 1E [log10(erg/s)]\
LOG_GAL_NH   1E [log10(cm^-2)]\
TYPE_FLAG    1B " >> fcreate_headfile.txt
  else if  ("$CAT" == "Clusters" ) then
    echo "REDSHIFT     1E" >> fcreate_headfile.txt
  else if  ("$CAT" == "GalDiskHR" || "$CAT" == "GalDiskLR" ) then
    echo "GAL_X 1E [kpc]\
GAL_Y 1E [kpc]\
GAL_Z 1E [kpc]\
POP_ID 1B \
SCHLEGEL 1E [mag]\
METALICITY 1E " >> fcreate_headfile.txt
  else if  ("$CAT" == "GalHaloHR" ) then
    echo "T_EFF 1E [K]\
LOG_G 1E [cgs]\
METALICITY 1E \
UNRED_V_MAG 1E [mag,V]\
SCHLEGEL 1E [mag]\
POP_ID 1B \
RADIAL_VEL 1E [km/s]" >> fcreate_headfile.txt

  endif
  set INFILE = ${BASE}/${CAT}_in/${CAT}_cat_current.txt
  set OUTFILE = ${BASE}/${CAT}_in/${CAT}_cat_current.fits.gz
  echo "Making fits files: ${INFILE} --> ${OUTFILE}"
  ftcreate fcreate_headfile.txt ${INFILE} ${OUTFILE} clobber=yes mode=q


end


exit
