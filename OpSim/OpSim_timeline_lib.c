// -*-mode:C; compile-command:"make -f OpSim_timeline_lib.make"; -*- 
//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_timeline_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.6 $"
//#define CVS_DATE     "$Date: 2015/08/18 11:44:16 $"
//--    Description:
//--    Use: This module is for functions that operate on timeline structures
//--         Also is used for skycalc stuff
//--
//--



#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <fitsio.h>    //needed for fits catalogues etc
#include <time.h>

#include "OpSim_timeline_lib.h"
#include "utensils_lib.h"
#include "geometry_lib.h"
#include "fits_helper_lib.h"



#define ERROR_PREFIX   "#-OpSim_timeline_lib.c : Error   :"
#define WARNING_PREFIX "#-OpSim_timeline_lib.c : Warning :"
#define COMMENT_PREFIX "#-OpSim_timeline_lib.c : Comment :"
#define DEBUG_PREFIX   "#-OpSim_timeline_lib.c : DEBUG   :"



////////////////////////////////////////////////////////////////////////
//functions
int OpSim_timeline_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

////////////////////////////////////////////////////////////////////////
//moondeets functions
int moondeets_init(moondeets_struct *pMoonDeets)
{
  const double null_val = NAN;
  if ( pMoonDeets == NULL ) return 1;
  pMoonDeets->JD           = null_val;
  pMoonDeets->ra           = null_val;
  pMoonDeets->dec          = null_val;
  pMoonDeets->Z            = null_val;
  pMoonDeets->HA           = null_val;
  pMoonDeets->phase_angle  = null_val;
  pMoonDeets->illum_frac   = null_val;
  return 0;
}

////////////////////////////////////////////////////////////////////////
//sundeets functions
int sundeets_init(sundeets_struct *pSunDeets)
{
  const double null_val = NAN;
  if ( pSunDeets == NULL ) return 1;
  pSunDeets->JD           = null_val;
  pSunDeets->ra           = null_val;
  pSunDeets->dec          = null_val;
  pSunDeets->Z            = null_val;
  pSunDeets->HA           = null_val;
  return 0;
}



///////////////////////////////////////////////////////////////////////
////////////Skycalc/Skycalcrecord struct functions

//one line print out
int skyrecord_print_short(FILE* pFile, skycalcrecord_struct *pRecord, BOOL print_header)
{
  if ( pFile   == NULL ||
       pRecord == NULL ) return 1;

//  fprintf(pFile, "SkycalcRecord: JD= %12.4f %-5s  Moon is %3.0f%% full and is %4s at RA,Dec= %7.3f %+7.3f  Zenith is at RA,Dec= %7.3f %+7.3f \n",
//          pRecord->JD,
//          (pRecord->nighttime_flag?"Night":"Day"),
//          pRecord->moon.illum_frac*100.,
//          (pRecord->moon.Z < 90. ? "Up" : "Down"),
//          pRecord->moon.ra,
//          pRecord->moon.dec,
//          pRecord->ra_zen,
//          pRecord->dec_zen);
  if ( print_header)
    fprintf(pFile, "TimelineNow: %12s %-5s %8s %8s %4s %4s %8s %8s %8s %8s %8s %6s\n",
            "JD(days)", "N/D", "E_twi-T", "M_twi-T",
            "Moon", "U/D", "RA_Moon","Dec_Moon","Z_Moon",
            "RA_Zen","Dec_Zen", "LenNgt");

    fprintf(pFile,
            "TimelineNow: %12.4f %-5s %+8.3f %+8.3f %4.0f %4s %8.3f %+8.3f %8.4f %8.3f %+8.3f %6.2f\n",
            pRecord->JD,
            (pRecord->nighttime_flag?"Night":"Day"),
            (pRecord->JD_twi_eve  - pRecord->JD)*DAYS_TO_HOURS,
            (pRecord->JD_twi_morn - pRecord->JD)*DAYS_TO_HOURS,
            pRecord->moon.illum_frac*100.,
            (pRecord->moon.Z < 90. ? "Up" : "Down"),
            pRecord->moon.ra,
            pRecord->moon.dec,
            pRecord->moon.Z,
            pRecord->ra_zen,
            pRecord->dec_zen,
            (pRecord->JD_twi_morn - pRecord->JD_twi_eve)*DAYS_TO_HOURS);
  return 0;
}

//one line print out
int timeline_print_short(FILE* pFile, timeline_struct *pTime, BOOL print_header)
{
  if ( pFile  == NULL ||
       pTime  == NULL ) return 1;

  if ( print_header)
    fprintf(pFile, "TimelineNow: %12s %15s %-5s %8s %8s %5s %4s %8s %8s %8s %8s %8s %8s %6s\n",
            "JD(days)", "InterpWind(hrs)", "N/D", "E_twi-T", "M_twi-T",
            "Moon", "U/D", "RA_Moon","Dec_Moon","Z_Moon",
            "RA_Zen","Dec_Zen", "SkyBrZen", "LenNgt");

    fprintf(pFile,
            "TimelineNow: %12.4f [%+6.3f:%+6.3f] %-5s %+8.3f %+8.3f %5.3f %4s %8.3f %+8.3f %8.4f %8.3f %+8.3f %8.2f %6.2f\n",
            pTime->Rnow.JD,
            (pTime->pRlo->JD - pTime->Rnow.JD)*DAYS_TO_HOURS,
            (pTime->pRhi->JD - pTime->Rnow.JD)*DAYS_TO_HOURS,
            (pTime->Rnow.nighttime_flag?"Night":"Day"),
            (pTime->Rnow.JD_twi_eve  - pTime->Rnow.JD)*DAYS_TO_HOURS,
            (pTime->Rnow.JD_twi_morn - pTime->Rnow.JD)*DAYS_TO_HOURS,
            pTime->Rnow.moon.illum_frac,
            (pTime->Rnow.moon.Z < 90. ? "Up" : "Down"),
            pTime->Rnow.moon.ra,
            pTime->Rnow.moon.dec,
            pTime->Rnow.moon.Z,
            pTime->Rnow.ra_zen,
            pTime->Rnow.dec_zen,
            //weather_calc_sky_brightness((timeline_struct*) pTime, pTime->Rnow.ra_zen, pTime->Rnow.dec_zen),
            //            99.9,
            timeline_calc_sky_brightness((timeline_struct*) pTime, pTime->Rnow.ra_zen, pTime->Rnow.dec_zen),
            (pTime->Rnow.JD_twi_morn - pTime->Rnow.JD_twi_eve)*DAYS_TO_HOURS);

  return 0;
}

//
int skycalcrecord_init(skycalcrecord_struct *pRecord)
{
  if ( pRecord == NULL ) return 1;
  pRecord->JD          = NAN;
  pRecord->JD_midnight = NAN;
  pRecord->JD_twi_eve  = NAN;     
  pRecord->JD_twi_morn = NAN;    
  pRecord->ra_zen      = NAN;
  pRecord->dec_zen     = NAN;
  pRecord->nighttime_flag = FALSE;    
  pRecord->moon_illum_midnight = (float) NAN;
  if ( moondeets_init((moondeets_struct*) &(pRecord->moon)))
  {
    return 1;
  }
  if ( sundeets_init((sundeets_struct*) &(pRecord->sun)))
  {
    return 1;
  }
  return 0;
}
//

int skycalc_struct_init(skycalc_struct **ppSkycalc)
{
  if ( ppSkycalc == NULL ) return 1;

  if ( (*ppSkycalc) == NULL )
  {
    //make space for the skycalc struct
    if ( (*ppSkycalc = (skycalc_struct*) malloc(sizeof(skycalc_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  //else we assume that space was already allocated

  //now initialise the elements of the skycalc struct
  (*ppSkycalc)->pRecord = NULL;
  (*ppSkycalc)->num_records = 0;
  (*ppSkycalc)->array_length = 0;
  (*ppSkycalc)->JD_min = DBL_MAX;
  (*ppSkycalc)->JD_max = DBL_MIN;
  return 0;
}

//read contents of one line from a skycalc input file 
int skycalc_read_record_from_string(skycalc_struct *pSkycalc, const char *str_line)
{
  int i = 0;
  int npars;
  skycalcrecord_struct *pRecord = NULL;

  float moon_alt, moon_az;
  float sun_alt, sun_az;
  float night_length;
  if ( pSkycalc == NULL ) return 1;
  if ( str_line == NULL ) return 1;
  while (i < (int) strlen(str_line) &&
         i < STR_MAX &&
         str_line[i] == ' ') i++; //step over leading white space
  if ( str_line[i] == '#' ||  str_line[i] == '\n' ) return 0;  //ignore any comment lines and any empty lines


  //check that we have enough space available
  if ( pSkycalc->num_records >= pSkycalc->array_length)
  {
    fprintf(stderr, "%s No space remaining to record the %dth skycalc entry: %s\n",
            ERROR_PREFIX, pSkycalc->num_records+1, str_line);

    return 1;
  }
  
  //read the values from the line
  //should be in the following format:
  // JDnow JD_midnight JD_eve_twi JD_morn_twi twi2twi moon_illum  moon_ra moon_dec  moon_alt  moon_az
  pRecord = (skycalcrecord_struct*) &(pSkycalc->pRecord[pSkycalc->num_records]);
  npars = sscanf(&(str_line[i]),
                 "%lf %lf %lf %lf %f %f %lf %lf %f %f  %lf %lf %f %f  %lf %lf",
                 (double*) &(pRecord->JD),
                 (double*) &(pRecord->JD_midnight),
                 (double*) &(pRecord->JD_twi_eve),
                 (double*) &(pRecord->JD_twi_morn),
                 (float*)  &(night_length),
                 (float*)  &(pRecord->moon_illum_midnight),
                 (double*) &(pRecord->moon.ra),
                 (double*) &(pRecord->moon.dec),
                 (float*)  &moon_alt,
                 (float*)  &moon_az,
                 (double*) &(pRecord->sun.ra),
                 (double*) &(pRecord->sun.dec),
                 (float*)  &sun_alt,
                 (float*)  &sun_az,
                 (double*) &(pRecord->ra_zen),
                 (double*) &(pRecord->dec_zen) );
  
  if ( npars < 16 )
  {
    fprintf(stdout, "%s Problem reading skycalc record %d: %s\n",
            WARNING_PREFIX, pSkycalc->num_records+1, str_line);
    
  }
  else
  {
    pRecord->moon.Z = (double) 90.0 - moon_alt;
    pRecord->sun.Z  = (double) 90.0 - sun_alt;
    pRecord->moon.JD = (double) pRecord->JD;
    pRecord->sun.JD  = (double) pRecord->JD;

    pSkycalc->num_records++;
    if (  pSkycalc->JD_min > pRecord->JD )   pSkycalc->JD_min = pRecord->JD ;
    if (  pSkycalc->JD_max < pRecord->JD )   pSkycalc->JD_max = pRecord->JD ;

    //work out if we are in the nighttime or the daytime
    if ( pRecord->JD >= pRecord->JD_twi_eve &&
         pRecord->JD <= pRecord->JD_twi_morn ) pRecord->nighttime_flag = TRUE;
    else                                       pRecord->nighttime_flag = FALSE;

  }
  
  return 0;
}


/////////////////////////////////////////////////////////////////////////////////////
//Skycalc gives Moon illum fractions correct for midnight
//Now go through and set the moon illum fractions to the correct instantaneous values
//do by interpolation
//The few values at each end will be a bit inaccurate
int skycalc_interp_moon_illum(skycalc_struct *pSkycalc)
{
  int i;
  
  if ( pSkycalc == NULL ) return 1;
  for(i=0;i<pSkycalc->num_records;i++)
  {
    skycalcrecord_struct *pR = (skycalcrecord_struct*) &(pSkycalc->pRecord[i]);
    skycalcrecord_struct *pRlo = NULL;
    skycalcrecord_struct *pRhi = NULL;
    int j;
    if ( (double) pR->JD == (double) pR->JD_midnight )
    {
      pR->moon.illum_frac = pR->moon_illum_midnight;
    }
    else
    {
      if ( pR->JD < pR->JD_midnight )
      {
        //search backwards for the previous midnight
        j = i;
        while ( pR->JD_midnight <= pSkycalc->pRecord[j].JD_midnight && j > 0 ) j --;
        
        pRlo = (skycalcrecord_struct*) &(pSkycalc->pRecord[j]);
        pRhi = (skycalcrecord_struct*) &(pSkycalc->pRecord[i]);
      }
      else
      {
        //search forwards for the following midnight
        j = i;
        while ( pR->JD_midnight >= pSkycalc->pRecord[j].JD_midnight && j < pSkycalc->num_records -1 ) j ++;
        
        pRlo = (skycalcrecord_struct*) &(pSkycalc->pRecord[i]);
        pRhi = (skycalcrecord_struct*) &(pSkycalc->pRecord[j]);
        
      }
    
      pR->moon.illum_frac = util_interp_linear_1D_double(pRlo->JD_midnight, pRlo->moon_illum_midnight,
                                                    pRhi->JD_midnight, pRhi->moon_illum_midnight,
                                                    pR->JD);
    }
  }

  return 0;
}

int skycalc_read_records_from_file(skycalc_struct *pSkycalc, const char *str_filename)
{
  int fhelp_file_type;
  if ( pSkycalc == NULL ) return 1;
  fhelp_file_type = (int) fhelp_test_if_file_is_fits((const char*) str_filename);

  switch ( fhelp_file_type )
  {
  case FHELP_FILE_FORMAT_CODE_NOT_FITS:
    if ( skycalc_read_records_from_file_ascii((skycalc_struct*) pSkycalc, (const char*) str_filename))
    {
      return 1;
    }
    break;
    
  case FHELP_FILE_FORMAT_CODE_FITS:
    if ( skycalc_read_records_from_file_fits((skycalc_struct*) pSkycalc, (const char*) str_filename))
    {
      return 1;
    }
    break;
    
  default:
    fprintf(stderr, "%s I had problems opening the file: %s (fhelp_file_type=%d)\n",
            ERROR_PREFIX, str_filename, fhelp_file_type);
      return 1;
    break;

  }
  
  

  return 0;
}


/////////////////////////////////////////////////////////////////////////////////////
//read all the records from a skycalc input file
int skycalc_read_records_from_file_ascii(skycalc_struct *pSkycalc, const char *str_filename)
{
  clock_t clocks_start = clock();
  FILE* pInfile = NULL;
  char buffer[STR_MAX];
  int i;
  
  if ( pSkycalc == NULL ) return 1;

  if ( pSkycalc->pRecord != NULL )
  {
    fprintf(stderr, "%s pSkycalc->pRecord is already non-null\n", ERROR_PREFIX);
    return 2;
  }
  
  //open up the file
  if ((pInfile = fopen(str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the skycalc file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  //count the number of entries in the file
  pSkycalc->array_length = 0;
  while (fgets(buffer,STR_MAX,pInfile) && pSkycalc->array_length++ < MAX_SKYCALC_RECORDS);
  rewind(pInfile);
  if ( pSkycalc->array_length <= 0 )
  {
    fprintf(stderr, "%s I had problems reading from the skycalc file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  else if ( pSkycalc->array_length >= MAX_SKYCALC_RECORDS )
  {
    fprintf(stderr, "%s Have reached the safety threshold (%d>=%d) reading skycalc records from file %s\n",
            ERROR_PREFIX, pSkycalc->array_length, MAX_SKYCALC_RECORDS, str_filename);
    return 1;
  }
  
  //now make some space
  if ((pSkycalc->pRecord = (skycalcrecord_struct*) malloc(pSkycalc->array_length * sizeof(skycalcrecord_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);fflush(stderr);
    return 1;
  }

  //and initialise each record
  for(i=0;i<pSkycalc->num_records;i++)
  {
    if ( skycalcrecord_init((skycalcrecord_struct*) &(pSkycalc->pRecord[i])))
    {
      return 1;
    }  
  }

  
  //Now, actually read in the records
  pSkycalc->num_records = 0;
  while (fgets(buffer,STR_MAX,pInfile) && pSkycalc->num_records < pSkycalc->array_length )
  {
    if (skycalc_read_record_from_string((skycalc_struct*) pSkycalc, (const char*) buffer))
    {
      return 1;
    }
  }

  //now tidy up the space
  if ( pSkycalc->num_records <= 0 )
  {
    fprintf(stderr, "%s I had problems reading from the skycalc file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  else if ( pSkycalc->num_records < pSkycalc->array_length)
  {
    pSkycalc->array_length = pSkycalc->num_records;
    if ((pSkycalc->pRecord = (skycalcrecord_struct*) realloc(pSkycalc->pRecord, pSkycalc->array_length * sizeof(skycalcrecord_struct))) == NULL )
    {
      fprintf(stderr, "%s Error re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  fclose(pInfile);
  pInfile = NULL;

  if ( skycalc_interp_moon_illum((skycalc_struct*) pSkycalc) )
  {
    fprintf(stderr, "%s Error interpolating moon illumination fraction\n", ERROR_PREFIX);
    return 1;
  }

  fprintf(stdout, "%s I read %6d skycalc records (JD spanning [%.1f:%.1f]) from this file: %s (that took %.2f seconds)\n",
          COMMENT_PREFIX, pSkycalc->num_records , pSkycalc->JD_min, pSkycalc->JD_max, str_filename,
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);

  return 0;
}



/////////////////////////////////////////////////////////////////////////////////////
//read all the records from a skycalc input file - FITS version
int skycalc_read_records_from_file_fits(skycalc_struct *pSkycalc, const char *str_filename)
{
  clock_t clocks_start = clock();
  fitsfile* fptr = NULL;
  int status = 0;    // CFITSIO status value
  long i,j;
  long nrows;
  long nrows_per_chunk;

  int colnum_JD          = 0 ;
  int colnum_JD_MIDNIGHT = 0 ;
  int colnum_JD_EVE_TWI  = 0 ;
  int colnum_JD_MORN_TWI = 0 ;
  int colnum_TWI2TWI     = 0 ;
  int colnum_MOON_ILLUM  = 0 ;
  int colnum_MOON_RA     = 0 ;
  int colnum_MOON_DEC    = 0 ;
  int colnum_MOON_ALT    = 0 ;
  int colnum_SUN_RA      = 0 ;
  int colnum_SUN_DEC     = 0 ;
  int colnum_SUN_ALT     = 0 ;
  int colnum_ZEN_RA      = 0 ;
  int colnum_ZEN_DEC     = 0 ;
  int ncols = 0;

  double *ptemp_JD          = NULL;
  double *ptemp_JD_MIDNIGHT = NULL;
  double *ptemp_JD_EVE_TWI  = NULL;
  double *ptemp_JD_MORN_TWI = NULL;
  double *ptemp_TWI2TWI     = NULL;
  double *ptemp_MOON_ILLUM  = NULL;
  double *ptemp_MOON_RA     = NULL;
  double *ptemp_MOON_DEC    = NULL;
  double *ptemp_MOON_ALT    = NULL;
  double *ptemp_SUN_RA      = NULL;
  double *ptemp_SUN_DEC     = NULL;
  double *ptemp_SUN_ALT     = NULL;
  double *ptemp_ZEN_RA      = NULL;
  double *ptemp_ZEN_DEC     = NULL;
  
  if ( pSkycalc == NULL ) return 1;

  if ( pSkycalc->pRecord != NULL )
  {
    fprintf(stderr, "%s pSkycalc->pRecord is already non-null\n", ERROR_PREFIX);
    return 2;
  }

  if (fits_open_table((fitsfile**) &fptr, str_filename, READONLY, &status))
  {
    fits_report_error(stderr, status); // print any error messages
    return 1;
  }
  
  fits_get_num_rows(fptr, &nrows, &status);
  fits_get_num_cols(fptr, &ncols, &status);
  fits_get_rowsize(fptr, &nrows_per_chunk, &status);
  if (status)
  {
    fits_report_error(stderr, status); // print any error messages
    return 1;
  }

  fits_get_colnum ((fitsfile*) fptr, CASESEN, "JD"         , &colnum_JD         , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "JD_MIDNIGHT", &colnum_JD_MIDNIGHT, &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "JD_EVE_TWI" , &colnum_JD_EVE_TWI , &status);
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "JD_MORN_TWI", &colnum_JD_MORN_TWI, &status);
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "TWI2TWI"    , &colnum_TWI2TWI    , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "MOON_ILLUM" , &colnum_MOON_ILLUM , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "MOON_RA"    , &colnum_MOON_RA    , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "MOON_DEC"   , &colnum_MOON_DEC   , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "MOON_ALT"   , &colnum_MOON_ALT   , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "SUN_RA"     , &colnum_SUN_RA     , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "SUN_DEC"    , &colnum_SUN_DEC    , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "SUN_ALT"    , &colnum_SUN_ALT    , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "ZEN_RA"     , &colnum_ZEN_RA     , &status); 
  fits_get_colnum ((fitsfile*) fptr, CASESEN, "ZEN_DEC"    , &colnum_ZEN_DEC    , &status); 

  if ( colnum_JD         <= 0 || colnum_JD         > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "JD         "   );return 1;};
  if ( colnum_JD_MIDNIGHT<= 0 || colnum_JD_MIDNIGHT> ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "JD_MIDNIGHT"   );return 1;};
  if ( colnum_JD_EVE_TWI <= 0 || colnum_JD_EVE_TWI > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "JD_EVE_TWI "   );return 1;};
  if ( colnum_JD_MORN_TWI<= 0 || colnum_JD_MORN_TWI> ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "JD_MORN_TWI"   );return 1;};
  if ( colnum_TWI2TWI    <= 0 || colnum_TWI2TWI    > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "TWI2TWI    "   );return 1;};
  if ( colnum_MOON_ILLUM <= 0 || colnum_MOON_ILLUM > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "MOON_ILLUM "   );return 1;};
  if ( colnum_MOON_RA    <= 0 || colnum_MOON_RA    > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "MOON_RA    "   );return 1;};
  if ( colnum_MOON_DEC   <= 0 || colnum_MOON_DEC   > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "MOON_DEC   "   );return 1;};
  if ( colnum_MOON_ALT   <= 0 || colnum_MOON_ALT   > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "MOON_ALT   "   );return 1;};
  if ( colnum_SUN_RA     <= 0 || colnum_SUN_RA     > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "SUN_RA     "   );return 1;};
  if ( colnum_SUN_DEC    <= 0 || colnum_SUN_DEC    > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "SUN_DEC    "   );return 1;};
  if ( colnum_SUN_ALT    <= 0 || colnum_SUN_ALT    > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "SUN_ALT    "   );return 1;};
  if ( colnum_ZEN_RA     <= 0 || colnum_ZEN_RA     > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "ZEN_RA     "   );return 1;};
  if ( colnum_ZEN_DEC    <= 0 || colnum_ZEN_DEC    > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "ZEN_DEC    "   );return 1;};

  if (status)
  {
    fits_report_error(stderr, status); // print any error messages
    return 1;
  }

  //make some space for the temporary arrays 
  if ( nrows_per_chunk > 0 )
  {
    if ((ptemp_JD                 = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_JD_MIDNIGHT        = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_JD_EVE_TWI         = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_JD_MORN_TWI        = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_TWI2TWI            = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_MOON_ILLUM         = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_MOON_RA            = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_MOON_DEC           = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_MOON_ALT           = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_SUN_RA             = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_SUN_DEC            = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_SUN_ALT            = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_ZEN_RA             = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
        (ptemp_ZEN_DEC            = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL )

    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);fflush(stderr);
      return 1;
    }
  }
  else
  {
    //nrows_per_chunk is zero so exit with error...
    return 1;
  }

  //make some space for the skycalc records
  pSkycalc->array_length = nrows;
  pSkycalc->num_records = 0;
  if ( pSkycalc->array_length <= 0 )
  {
    fprintf(stderr, "%s I had problems reading from the skycalc file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  else if ( pSkycalc->array_length >= MAX_SKYCALC_RECORDS )  // not sure that this is really necessary
  {
    fprintf(stderr, "%s Have reached the safety threshold (%d>=%d) reading skycalc records from file %s\n",
            ERROR_PREFIX, pSkycalc->array_length, MAX_SKYCALC_RECORDS, str_filename);
    return 1;
  }
  
  //now make some space
  if ((pSkycalc->pRecord = (skycalcrecord_struct*) malloc(pSkycalc->array_length * sizeof(skycalcrecord_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);fflush(stderr);
    return 1;
  }

  //and initialise each record   - maybe can do this faster with a calloc?
  for(i=0;i<pSkycalc->array_length;i++)
  {
    if ( skycalcrecord_init((skycalcrecord_struct*) &(pSkycalc->pRecord[i])))
    {
      return 1;
    }  
  }
    
   
  //now read through the file a "nrows_per_chunk" chunk at a time - to maximise efficiency
  for ( i=0; i<nrows; i+=nrows_per_chunk )
  {
    long nrows_this_chunk = MIN(nrows_per_chunk, nrows-i);
    fits_read_col(fptr, TDOUBLE, colnum_JD         , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_JD         , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_JD_MIDNIGHT, i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_JD_MIDNIGHT, NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_JD_EVE_TWI , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_JD_EVE_TWI , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_JD_MORN_TWI, i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_JD_MORN_TWI, NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_TWI2TWI    , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_TWI2TWI    , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_MOON_ILLUM , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_MOON_ILLUM , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_MOON_RA    , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_MOON_RA    , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_MOON_DEC   , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_MOON_DEC   , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_MOON_ALT   , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_MOON_ALT   , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_SUN_RA     , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_SUN_RA     , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_SUN_DEC    , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_SUN_DEC    , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_SUN_ALT    , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_SUN_ALT    , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_ZEN_RA     , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_ZEN_RA     , NULL, &status);
    fits_read_col(fptr, TDOUBLE, colnum_ZEN_DEC    , i+1, 1, nrows_this_chunk, NULL, (double*) ptemp_ZEN_DEC    , NULL, &status);

    if (status)
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }
    // copy the values from the temp arrays into the pObj structures
    for ( j=0; j<nrows_this_chunk; j++)
    {
      skycalcrecord_struct* pRecord = (skycalcrecord_struct*) &(pSkycalc->pRecord[pSkycalc->num_records]);
      pRecord->JD                  = ptemp_JD[j];
      pRecord->JD_midnight         = ptemp_JD_MIDNIGHT[j];
      pRecord->JD_twi_eve          = ptemp_JD_EVE_TWI[j];
      pRecord->JD_twi_morn         = ptemp_JD_MORN_TWI[j];
      pRecord->moon_illum_midnight = ptemp_MOON_ILLUM[j];
      pRecord->moon.ra             = ptemp_MOON_RA[j];
      pRecord->moon.dec            = ptemp_MOON_DEC[j];
      pRecord->moon.Z              = (double) 90.0 - ptemp_MOON_ALT[j];
      pRecord->sun.ra              = ptemp_SUN_RA[j];
      pRecord->sun.dec             = ptemp_SUN_DEC[j];
      pRecord->sun.Z               = (double) 90.0 - ptemp_SUN_ALT[j];
      pRecord->ra_zen              = ptemp_ZEN_RA[j];
      pRecord->dec_zen             = ptemp_ZEN_DEC[j];
  
      pRecord->moon.JD = (double) pRecord->JD;
      pRecord->sun.JD  = (double) pRecord->JD;

      pSkycalc->num_records++;
      if (  pSkycalc->JD_min > pRecord->JD )   pSkycalc->JD_min = pRecord->JD ;
      if (  pSkycalc->JD_max < pRecord->JD )   pSkycalc->JD_max = pRecord->JD ;
        
      //work out if we are in the nighttime or the daytime
      if ( pRecord->JD >= pRecord->JD_twi_eve &&
           pRecord->JD <= pRecord->JD_twi_morn ) pRecord->nighttime_flag = TRUE;
      else                                       pRecord->nighttime_flag = FALSE;

    }  
  }

  
  //tidy up temp arrays
  if ( ptemp_JD          ) free (ptemp_JD         ) ; ptemp_JD          = NULL;
  if ( ptemp_JD_MIDNIGHT ) free (ptemp_JD_MIDNIGHT) ; ptemp_JD_MIDNIGHT = NULL;
  if ( ptemp_JD_EVE_TWI  ) free (ptemp_JD_EVE_TWI ) ; ptemp_JD_EVE_TWI  = NULL;
  if ( ptemp_JD_MORN_TWI ) free (ptemp_JD_MORN_TWI) ; ptemp_JD_MORN_TWI = NULL;
  if ( ptemp_TWI2TWI     ) free (ptemp_TWI2TWI    ) ; ptemp_TWI2TWI     = NULL;
  if ( ptemp_MOON_ILLUM  ) free (ptemp_MOON_ILLUM ) ; ptemp_MOON_ILLUM  = NULL;
  if ( ptemp_MOON_RA     ) free (ptemp_MOON_RA    ) ; ptemp_MOON_RA     = NULL;
  if ( ptemp_MOON_DEC    ) free (ptemp_MOON_DEC   ) ; ptemp_MOON_DEC    = NULL;
  if ( ptemp_MOON_ALT    ) free (ptemp_MOON_ALT   ) ; ptemp_MOON_ALT    = NULL;
  if ( ptemp_SUN_RA      ) free (ptemp_SUN_RA     ) ; ptemp_SUN_RA      = NULL;
  if ( ptemp_SUN_DEC     ) free (ptemp_SUN_DEC    ) ; ptemp_SUN_DEC     = NULL;
  if ( ptemp_SUN_ALT     ) free (ptemp_SUN_ALT    ) ; ptemp_SUN_ALT     = NULL;
  if ( ptemp_ZEN_RA      ) free (ptemp_ZEN_RA     ) ; ptemp_ZEN_RA      = NULL;
  if ( ptemp_ZEN_DEC     ) free (ptemp_ZEN_DEC    ) ; ptemp_ZEN_DEC     = NULL;


  status = 0;
  fits_close_file(fptr, &status);
  fptr = NULL;
  
  if ( skycalc_interp_moon_illum((skycalc_struct*) pSkycalc) )
  {
    fprintf(stderr, "%s Error interpolating moon illumination fraction\n", ERROR_PREFIX);
    return 1;
    }
  fprintf(stdout, "%s I read %6d skycalc records (JD spanning [%.1f:%.1f]) from this file: %s (that took %.2f seconds)\n",
          COMMENT_PREFIX, pSkycalc->num_records , pSkycalc->JD_min, pSkycalc->JD_max, str_filename,
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
  fflush(stdout);
  
  return 0;
}




int sundeets_interpolate (const sundeets_struct *pSlo, const sundeets_struct *pShi, sundeets_struct *pSmid)
{
  if ( pSlo  == NULL ||
       pShi  == NULL ||
       pSmid == NULL ) return 1;
  
  //find the interpolated point between the positions
  if ( geom_interpolate_long_lat(pSlo->JD,  pSlo->ra, pSlo->dec,
                                 pShi->JD,  pShi->ra, pShi->dec,
                                 pSmid->JD, (double*) &(pSmid->ra), (double*) &(pSmid->dec)))
  {
    fprintf(stderr, "%s Problem calling geom_interpolate_long_lat() at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  pSmid->Z   = util_interp_linear_1D_double(pSlo->JD, pSlo->Z  ,  pShi->JD, pShi->Z  ,  pSmid->JD);
  pSmid->HA  = util_interp_linear_1D_double(pSlo->JD, pSlo->HA ,  pShi->JD, pShi->HA ,  pSmid->JD);

  return 0;
}

int moondeets_interpolate (const moondeets_struct *pMlo, const moondeets_struct *pMhi, moondeets_struct *pMmid)
{
  if ( pMlo  == NULL ||
       pMhi  == NULL ||
       pMmid == NULL ) return 1;
  
  //find the interpolated point between the positions
  if ( geom_interpolate_long_lat(pMlo->JD,  pMlo->ra, pMlo->dec,
                                 pMhi->JD,  pMhi->ra, pMhi->dec,
                                 pMmid->JD, &(pMmid->ra), &(pMmid->dec)))
  {
    fprintf(stderr, "%s Problem calling geom_interpolate_long_lat() at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  pMmid->Z   = util_interp_linear_1D_double(pMlo->JD, pMlo->Z  ,  pMhi->JD, pMhi->Z  ,  pMmid->JD);
  pMmid->HA  = util_interp_linear_1D_double(pMlo->JD, pMlo->HA ,  pMhi->JD, pMhi->HA ,  pMmid->JD);
  pMmid->phase_angle = (float) util_interp_linear_1D_double(pMlo->JD, (double) pMlo->phase_angle,
                                                            pMhi->JD, (double) pMhi->phase_angle,  pMmid->JD);
  pMmid->illum_frac  = (float) util_interp_linear_1D_double(pMlo->JD, (double) pMlo->illum_frac,
                                                            pMhi->JD, (double) pMhi->illum_frac,  pMmid->JD);

  return 0;
}

//interpolate values between two skycalcrecord structs
//assume that pRmid->JD has already been set;
int skycalcrecord_interpolate (const skycalcrecord_struct *pRlo,
                               const skycalcrecord_struct *pRhi,
                               skycalcrecord_struct *pRmid)
{
  if ( pRlo == NULL ||
       pRhi == NULL ||
       pRmid == NULL ) return 1;

  if ( isnan(pRmid->JD) )
  {
    fprintf(stderr, "%s Cannot interpolate because requested JD=%.3f is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  pRmid->JD, __FILE__, __LINE__);
    return 1;
  }
  
  if ( pRmid->JD < pRlo->JD ||
       pRmid->JD > pRhi->JD )
  {
    fprintf(stderr, "%s Cannot interpolate because requested JD=%.3f is outside brackets: [%.3f:%.3f]\n",
            ERROR_PREFIX,  pRmid->JD, pRlo->JD, pRhi->JD);
    
    return 1;
  }
  pRmid->JD_midnight = util_interp_linear_1D_double(pRlo->JD, pRlo->JD_midnight,  pRhi->JD, pRhi->JD_midnight,  pRmid->JD);
  //use the closest twilight values
  if ( fabs(pRlo->JD-pRmid->JD) < fabs(pRhi->JD-pRmid->JD))
  {
    pRmid->JD_twi_eve  = pRlo->JD_twi_eve;
    pRmid->JD_twi_morn = pRlo->JD_twi_morn;
  }
  else
  {
    pRmid->JD_twi_eve  = pRhi->JD_twi_eve;
    pRmid->JD_twi_morn = pRhi->JD_twi_morn;
  }
  //find the interpolated point between the positions
  if ( geom_interpolate_long_lat(pRlo->JD,  pRlo->ra_zen, pRlo->dec_zen,
                                 pRhi->JD,  pRhi->ra_zen, pRhi->dec_zen,
                                 pRmid->JD, (double*) &(pRmid->ra_zen), (double*) &(pRmid->dec_zen)))
  {
    fprintf(stderr, "%s Problem calling geom_interpolate_long_lat() at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
 
  pRmid->moon.JD = pRmid->JD;
  pRmid->sun.JD  = pRmid->JD;
  if ( moondeets_interpolate ((const moondeets_struct*) &(pRlo->moon),
                              (const moondeets_struct*) &(pRhi->moon),
                              (moondeets_struct*)       &(pRmid->moon)))
  {
    fprintf(stderr, "%s Problem calling moondeets_interpolate\n", ERROR_PREFIX);
    return 1;
  }
  
  if ( sundeets_interpolate  ((const sundeets_struct*)  &(pRlo->sun),
                              (const sundeets_struct*)  &(pRhi->sun),
                              (sundeets_struct*)        &(pRmid->sun)))
  {
    fprintf(stderr, "%s Problem calling sundeets_interpolate\n", ERROR_PREFIX);
    return 1;
  }
  
  //work out if we are in the nighttime or the daytime
  if ( pRmid->JD >= pRmid->JD_twi_eve &&
       pRmid->JD <= pRmid->JD_twi_morn ) pRmid->nighttime_flag = TRUE;
  else                                   pRmid->nighttime_flag = FALSE;
  
  return 0;
}

//free all memory associated with a skycalc struct, including the struct itself
int skycalc_struct_free ( skycalc_struct **ppSkycalc )
{
  if ( ppSkycalc == NULL ) return 1;
  if ( *ppSkycalc )
  {
    if ( (*ppSkycalc)-> pRecord) free((*ppSkycalc)-> pRecord);
    (*ppSkycalc)-> pRecord = NULL;
    free(*ppSkycalc);
    *ppSkycalc = NULL;
  }
  return 0;
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
//////////timeline functions
//int timeline_struct_init(timeline_struct **ppTime, struct telescope_struct *pTele, skycalc_struct *pSkycalc)
//int timeline_struct_init(timeline_struct **ppTime, void *pTele, skycalc_struct *pSkycalc)
int timeline_struct_init(timeline_struct **ppTime, telescope_struct *pTele, skycalc_struct *pSkycalc)
{
  if ( ppTime == NULL ) return 1;

  if ((*ppTime) == NULL )
  {
    //make space for the Timeline struct
    if ( (*ppTime = (timeline_struct*) malloc(sizeof(timeline_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  
  //  (*ppTime)->pTele    = (void*) pTele; //(struct telescope_struct*) pTele; 
  (*ppTime)->pTele    = (telescope_struct*) pTele; 
  (*ppTime)->pSkycalc = (skycalc_struct*)   pSkycalc;

  //  if (moondeets_init((moondeets_struct*) &((*ppTime)->moon))) return 1;
  (*ppTime)->JD          = NAN;
  (*ppTime)->iJD         = -1;
  (*ppTime)->fJD         = NAN;
  //  (*ppTime)->night_start = NAN;
  //  (*ppTime)->night_stop  = NAN;
  //  (*ppTime)->ra_zen      = NAN;
  (*ppTime)->pRlo = NULL;
  (*ppTime)->pRhi = NULL;
  if (skycalcrecord_init((skycalcrecord_struct*) &((*ppTime)->Rnow))) return 1;

  return 0;
}

int timeline_struct_free(timeline_struct **ppTime)
{
  if ( ppTime == NULL ) return 1;
  if ( *ppTime )
  {
    //now free the timeline struct itself
    free(*ppTime);
    *ppTime = NULL;
  }
  return 0;
}


//find the location of the current time in the skycalc outputs info
//set the appropriate variables in the pTime structure
int timeline_update_from_skycalc(timeline_struct *pTime)
{
  skycalc_struct *pSkycalc;
  int i,j;
  if ( pTime == NULL ) return 1;
  if ( pTime->pSkycalc == NULL ) return 1;
  if ( isnan(pTime->JD) )
  {
    fprintf(stderr, "%s Cannot interpolate because requested time (=%.3f) is a NaN (at file %s line %d)\n",
            ERROR_PREFIX,  pTime->JD, __FILE__, __LINE__);
    return 1;
  }

  pSkycalc = (skycalc_struct*) pTime->pSkycalc;

  if ( pTime->JD < pSkycalc->JD_min ||
       pTime->JD > pSkycalc->JD_max )
  {
    fprintf(stderr, "%s Requested JD=%.3f is outside skycalc bounds: [%.3f:%.3f]\n",
            ERROR_PREFIX,  pTime->JD, pSkycalc->JD_min, pSkycalc->JD_max);
    return 1;
  }
//  fprintf(stdout, "%s Requested JD=%.3f is inside skycalc bounds: [%.3f:%.3f] (by [%.3f:%.3f] days)\n",
//          COMMENT_PREFIX,
//          pTime->JD, pSkycalc->JD_min, pSkycalc->JD_max,
//          pSkycalc->JD_min - pTime->JD, pSkycalc->JD_max - pTime->JD); fflush(stdout);

  //search through and find the two skycalc records that bound the requested JD
  //make a guess assuming that the input values are already sorted (in ascending order) and evenly spaced.
  if ( (pSkycalc->JD_max - pSkycalc->JD_min) <= (double) 0.0)
  {
    fprintf(stderr, "%s Problem because pSkycalc->JD_min > pSkycalc->JD_max\n", ERROR_PREFIX);
    return 1;
  }

  i = (int) (pSkycalc->num_records * (pTime->JD - pSkycalc->JD_min)/(pSkycalc->JD_max - pSkycalc->JD_min));
  if ( i < 0 ) i = 0;
  else if ( i >= pSkycalc->num_records ) i = pSkycalc->num_records - 1;

  //  fprintf(stdout, "%s First guess is i=%d giving JD=%.3f\n", COMMENT_PREFIX, i, pSkycalc->pRecord[i].JD); fflush(stdout);
  if (  pTime->JD < pSkycalc->pRecord[i].JD )
  {
    //    fprintf(stdout, "%s First guess too high so counting i down\n", COMMENT_PREFIX); fflush(stdout);
    while (pTime->JD < pSkycalc->pRecord[i].JD && i > 0) i--;  
    j = i+1;
  }
  else
  {
    //    fprintf(stdout, "%s First guess too low so counting j up\n", COMMENT_PREFIX); fflush(stdout);
    j = i;
    while (pTime->JD > pSkycalc->pRecord[j].JD && j < (pSkycalc->num_records-1)) j++;  
    i = j - 1;
  }

  if ( i < 0 ) i = 0;

  //  fprintf(stdout, "%s Resulting bounds are [%d:%d] at [%.3f:%.3f]\n", COMMENT_PREFIX, i, j, pSkycalc->pRecord[i].JD, pSkycalc->pRecord[j].JD); fflush(stdout);

  pTime->pRlo = (skycalcrecord_struct*) &(pSkycalc->pRecord[i]);
  pTime->pRhi = (skycalcrecord_struct*) &(pSkycalc->pRecord[j]);
  //so we should now have the two records that span the required JD
  //interpolate between them
  pTime->Rnow.JD = pTime->JD;
  if ( skycalcrecord_interpolate((skycalcrecord_struct*) pTime->pRlo,
                                 (skycalcrecord_struct*) pTime->pRhi,
                                 (skycalcrecord_struct*) &(pTime->Rnow)))
  {
    fprintf(stderr, "%s Problem calling skycalcrecord_interpolate (between records %d and %d) \n", ERROR_PREFIX, i, j);
    return 1;
  }

  return 0;
}
/////////////////////////////////////////

int timeline_update(timeline_struct *pTime)
{
  if ( pTime == NULL ) return 1;

  if ( isnan(pTime->JD) )
  {
    fprintf(stderr, "%s Cannot update timeline because requested JD=%.3f is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  pTime->JD, __FILE__, __LINE__);
    return 1;
  }
  
  pTime->iJD = (int) floor(pTime->JD);
  pTime->fJD = (float) (pTime->JD - (double) pTime->iJD);
  if (timeline_update_from_skycalc((timeline_struct*) pTime))
  {
    fprintf(stderr, "%s Problem updating timeline from skycalc\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}


int timeline_set_time_from_JD (timeline_struct *pTime, double JD)
{
  if ( pTime == NULL ) return 1;
  if ( isnan(JD) )
  {
    fprintf(stderr, "%s Cannot set time because requested JD=%.3f is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  JD, __FILE__, __LINE__);
    return 1;
  }
  
  pTime->JD = JD;
  if ( timeline_update((timeline_struct*) pTime))
  {
    fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}

int timeline_increment_time_by_secs (timeline_struct *pTime, double delta_t)
{
  if ( pTime == NULL ) return 1;
  if ( isnan(delta_t) )
  {
    fprintf(stderr, "%s Cannot increment time because requested delta_t=%g is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  delta_t, __FILE__, __LINE__);
    return 1;
  }

  
  pTime->JD += (double) (delta_t * SECS_TO_DAYS);
  if ( timeline_update((timeline_struct*) pTime))
  {
    fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}
int timeline_increment_time_by_mins (timeline_struct *pTime, double delta_t)
{
  if ( pTime == NULL ) return 1;
  if ( isnan(delta_t) )
  {
    fprintf(stderr, "%s Cannot increment time because requested delta_t=%g is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  delta_t, __FILE__, __LINE__);
    return 1;
  }
  pTime->JD += (double) (delta_t * MINS_TO_DAYS);
  if ( timeline_update((timeline_struct*) pTime))
  {
    fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}
int timeline_increment_time_by_hours (timeline_struct *pTime, double delta_t)
{
  if ( pTime == NULL ) return 1;
  if ( isnan(delta_t) )
  {
    fprintf(stderr, "%s Cannot increment time because requested delta_t=%g is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  delta_t, __FILE__, __LINE__);
    return 1;
  }
  pTime->JD += (double) (delta_t * HOURS_TO_DAYS);
  if ( timeline_update((timeline_struct*) pTime))
  {
    fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}

int timeline_increment_time_by_days (timeline_struct *pTime, double delta_t)
{
  if ( pTime == NULL ) return 1;
  if ( isnan(delta_t) )
  {
    fprintf(stderr, "%s Cannot increment time because requested delta_t=%g is NaN (at file %s line %d)\n",
            ERROR_PREFIX,  delta_t, __FILE__, __LINE__);
    return 1;
  }
  pTime->JD += delta_t;
  if ( timeline_update((timeline_struct*) pTime))
  {
    fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}


//move forward to the beginning of the next evening
//if we start during night time then we skip past the remainder of the current night
int timeline_increment_time_to_next_night( timeline_struct *pTime)
{
  if ( pTime == NULL ) return 1;

  // If we are sometime before the end of evening twilight, ie in daytime,
  // so just move forward to the next evening twilight 
  if (pTime->JD < pTime->Rnow.JD_twi_eve )
  {
    if ( timeline_increment_time_by_days((timeline_struct*) pTime,  (double) pTime->Rnow.JD_twi_eve - pTime->JD))
    {
      fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
      return 1;
    }
  }
  else  //we are in night time, so first need to move forward to end of this night, then onto the next day
  {
    double old_twilight = pTime->Rnow.JD_twi_eve;
    //walk onwards until there is a change in the twilight times
    while ( (double) pTime->Rnow.JD_twi_eve <= (double) old_twilight )
    {
      if ( timeline_increment_time_by_hours((timeline_struct*) pTime,  (double) 1.0))
      {
        fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
        return 1;
      }
    }
    // then just move forward to the next evening twilight 
    if ( timeline_increment_time_by_days((timeline_struct*) pTime,  (double) pTime->Rnow.JD_twi_eve - pTime->JD))
    {
      fprintf(stderr, "%s Problem updating timeline\n", ERROR_PREFIX);
      return 1;
    }
  }  
  return 0;
}



//derived from method of "Krisciunas, K., and Schaefer, B.E. (1991,PASP,103,1033)
// "A model of the brightness of moonlight"
// http://adsabs.harvard.edu/abs/1991PASP..103.1033K

//calculate sky brightness at a given position on the sky at a given time
float timeline_calc_sky_brightness (timeline_struct *pTime, double ra, double dec)
{
  //from my calc_sky_brightness.plot file
  //Istar(alpha) = (10.0**(-0.4*(3.84+0.026*(alpha>0.?alpha:-alpha) + 4e-9*(alpha**4))))
  //f(rho)       = ((10.0**5.36)*(1.06 + cos(rho)**2) + 10.**(6.15 - rho/40.0))
  //X(Z)         = ((1. - 0.96*(sin(Z)**2))**-0.5)
  //B_0(Zsky)    = (Bzen*(10.**(-0.4*k*(X(Zsky)-1.)))*X(Zsky))
  //B_moon(alpha,rho,Zmoon,Zsky) = (f(rho)*Istar(alpha)*(10.**(-0.4*k*X(Zmoon)))*(1. - 10.**(-0.4*k*X(Zsky))))
  //delta_V_moon(alpha,rho,Zmoon,Zsky) = (-2.5*log10((B_moon(alpha,rho,Zmoon,Zsky) + B_0(Zsky))/B_0(Zsky)))
  //delta_V_moon2(alpha,rho,Zmoon,Zsky) = (Zsky >= 90. ? +1.0 : delta_V_moon(alpha,rho,Zmoon,Zsky) )

  float result;
  //moon phase (0=full, 180=new) measured as angle between earth and sun as seen from the moon
  //good approximation of alpha = 180deg - sun_moon_angle_measured_from_earth
  float alpha;
  //the sky-moon separation (degrees)
  float rho; 
  //a meaure of the brightness of the moon for this moon phase (mag units)
  float Istar;
  //a meaure of the scattered flux - a fractional term
  float f_rho;
  //airmasses for the moon and the sky positions
  float X_Zmoon, X_Zsky;
  //this gives the dark sky brightness at the sky position, given a zenith dark sky brightness  (ie no moon contribution)
  // - in linear units (nonolamberts)
  float B_0_Zsky;
  //this is the sky brightness due to the moon only
  // - in linear units (nonolamberts)
  float B_moon;

  float Zsky; //zenith distance of sky, degrees

  telescope_struct *pTele = NULL;  //just a shortcut to the telescope structure pointer in the Tile structure
  
  if ( pTime == NULL ) return 1;
  pTele = (telescope_struct*) pTime->pTele;
  if ( pTele == NULL ) return 1;


  Zsky         = geom_arc_dist(pTime->Rnow.ra_zen,pTime->Rnow.dec_zen,ra,dec);
  X_Zsky       = util_airmass_kriscicunas(Zsky);
  B_0_Zsky     = pTele->Bzen*X_Zsky*pow(10.0, -0.4*pTele->extinction_coeff*(X_Zsky - 1.));
//  fprintf(stdout, "Debug: ra,dec           = %9.3f %9.3f\n",ra,dec);
//  fprintf(stdout, "Debug: ra_zen,dec_zen   = %9.3f %9.3f\n",pTime->Rnow.ra_zen,pTime->Rnow.dec_zen);
//  fprintf(stdout, "Debug: Zsky             = %9.3f\n",Zsky);
//  fprintf(stdout, "Debug: X_Zsky           = %9.3f\n",X_Zsky);
//  fprintf(stdout, "Debug: Bzen             = %9.3f nL\n",pTele->Bzen);
//  fprintf(stdout, "Debug: extinction_coeff = %9.3f \n",pTele->extinction_coeff);
//  fprintf(stdout, "Debug: B_0_Zsky         = %9.3f nL\n",B_0_Zsky);

  if ( pTime->Rnow.moon.Z < 108.0 )  //consider moon contribution only if above -18deg elevation
  {
    //so, we calculate alpha from the moon and sun positions
    alpha        = (float) 180.0 - geom_arc_dist(pTime->Rnow.moon.ra,pTime->Rnow.moon.dec, pTime->Rnow.sun.ra,pTime->Rnow.sun.dec);
    //and rho from the moon and sky positions
    rho          = (float) geom_arc_dist(pTime->Rnow.moon.ra,pTime->Rnow.moon.dec,ra,dec);
    // intensity of the moon light 
    Istar        = (float) pow(10.0, -0.4*(3.84 + 0.026*fabs(alpha) + 4e-9*(SQR(SQR(alpha)))));
    // scattering fraction
    // 10^5.36 = 229086.765
    f_rho        = 229086.765*(1.06 + SQR(cosd(rho))) + pow(10., 6.15 - rho*0.025);
    
    X_Zmoon      = util_airmass_kriscicunas(pTime->Rnow.moon.Z);

    B_moon =  f_rho * Istar*
      pow(10.0, -0.4*pTele->extinction_coeff*X_Zmoon) *
      (1.0 - pow(10.0,-0.4*pTele->extinction_coeff*X_Zsky));
  
    //    result = pTele->dark_sky_brightness + -2.5*log10((B_moon + B_0_Zsky)/B_0_Zsky);
    result = util_nanoLambert_to_magsqarcsec(B_0_Zsky + B_moon);
  }
  else
  {//the moon is down, so use the 
    result = util_nanoLambert_to_magsqarcsec(B_0_Zsky);
  }
  return result;
}

//very abbreviated version of the part of the above that is dependent on moon distance
float timeline_calc_relative_sky_brightness (timeline_struct *pTime, double moon_dist)
{
  return (4.0e-6*(2.29e5*(1.06 + SQR(cosd(moon_dist))) + pow(10., 6.15-0.025*moon_dist)));
}


