// -*-mode:C; compile-command:"make -f helper_thread_manager_lib.make"; -*- 
//----------------------------------------------------------------------------------
//--
//--    Filename: helper_thread_manager_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.1 $"
//#define CVS_DATE     "$Date: 2013/02/08 16:03:52 $"
//--    Description:
//--    Use: This module provides tools to to allow a 
//--         program to make many "fire and forget" system calls that will run in  
//--         the background wrt the main thread
//--         A maximum limit to the number of concurrent threads will be obeyed


#include <stdlib.h>


#define ERROR_PREFIX   "#-helper_thread_manager_lib.c : Error   :"
#define WARNING_PREFIX "#-helper_thread_manager_lib.c : Warning :"
#define COMMENT_PREFIX "#-helper_thread_manager_lib.c : Comment :"
#define DEBUG_PREFIX   "#-helper_thread_manager_lib.c : DEBUG   :"
