//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_positioner_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.82 $"
//#define CVS_DATE     "$Date: 2015/09/03 15:45:53 $"
//--    Note: Definitions are in the corresponding header file OpSim_positioner_lib.h
//--    Description:
//--      This module is used to determine colisions between positioners etc
//--
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <time.h>
#include <unistd.h> //needed for getcwd
#include <ctype.h>  //for toupper

#include "OpSim_positioner_lib.h"
#include "quadtree_lib.h"
#include "OpSim_timeline_lib.h"


#define ERROR_PREFIX   "#-OpSim_positioner_lib.c:Error:  "
#define WARNING_PREFIX "#-OpSim_positioner_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_positioner_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_positioner_lib.c:DEBUG:  "

#include <assert.h>   //needed for debugging

////////////////////////////////////////////////////////////////////////
//functions
int OpSim_positioner_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}


// return a 1 character string 
char* OpSim_positioner_fiber_status_string ( fiber_struct *pFib ) 
{
  if      ( pFib == NULL )                      return "?";
  else if ( pFib->is_empty )                    return "E";
  else if ( pFib->is_switchable )               return "S";
  else if ( pFib->res == RESOLUTION_CODE_LOW  ) return "1";
  else if ( pFib->res == RESOLUTION_CODE_HIGH ) return "2";
  else if ( pFib->res == RESOLUTION_CODE_DUAL ) return "D";
  //odd
  return "-";
}
  
 
//take a simple copy of all data elements of a fiber_struct
int OpSim_positioner_copy_fiber_struct (fiber_struct *pFibCopy, const fiber_struct *pFibOrig )
{
  //  int i;
  if ( pFibCopy == NULL ||
       pFibOrig == NULL ) return 1;

  if ( memcpy((void*) pFibCopy, (void*) pFibOrig , (size_t) sizeof(fiber_struct)) == NULL) return 1;
  return 0;
}

//take a simple copy of all data elements of a fibergeom_struct
int OpSim_positioner_copy_fibergeom_struct (fibergeom_struct *pFibGeomCopy, const fibergeom_struct *pFibGeomOrig )
{
  //  int i;
  if ( pFibGeomCopy == NULL ||
       pFibGeomOrig == NULL ) return 1; 

  if ( memcpy((void*) pFibGeomCopy, (void*) pFibGeomOrig , (size_t) sizeof(fibergeom_struct)) == NULL ) return 1;
  return 0;
}


//fix a fiber and make it ready for assignment
int OpSim_positioner_repair_fiber (focalplane_struct *pFocal, fiber_struct *pFib, double time_now) 
{
  if ( pFocal == NULL || pFib == NULL ) return -1;
  pFib->status = FIBER_STATUS_FLAG_NOMINAL;
  pFib->Geom.number_of_movements = 0;
  pFib->Geom.distance_moved_dir1 = 0.0;
  pFib->Geom.distance_moved_dir2 = 0.0;
  pFib->number_of_repairs ++;
  pFib->time_of_last_repair = time_now;

  if ( OpSim_positioner_set_fiber_to_null_position ((focalplane_struct*) pFocal,
                                                    (fiber_struct*) pFib))
    return 1;

  return 0;
}

int OpSim_positioner_repair_faulty_fibers (focalplane_struct *pFocal, double time_now)
{
  int i;
  if ( pFocal == NULL ) return -1;
  for(i=0;i<pFocal->num_fibers;i++)
  {
    if ( pFocal->pFib[i].status != FIBER_STATUS_FLAG_NOMINAL )
    {
      if ( OpSim_positioner_repair_fiber ((focalplane_struct*) pFocal, (fiber_struct*) &(pFocal->pFib[i]), (double) time_now))
        return 1;
      
    }
  }
  return 0;
}


int OpSim_positioner_reset_fiber_assignment_status (fiber_struct *pFib) 
{
  if ( pFib == NULL ) return -1;
  pFib->pObj = NULL;
  pFib->assign_flag = (int) FIBER_ASSIGN_CODE_NONE;
  pFib->assignment_order = -1;
  return 0;
}

// reset a fiber -> reset position and attributes
int OpSim_positioner_reset_fiber (focalplane_struct *pFocal, fiber_struct *pFib ) 
{
  if ( pFocal == NULL ||
       pFib == NULL ) return -1;

  if ( OpSim_positioner_set_fiber_to_null_position ((focalplane_struct*) pFocal,
                                   (fiber_struct*) pFib))
    return 1;

  if ( OpSim_positioner_reset_fiber_assignment_status ((fiber_struct*) pFib))
    return 1;

  return 0;
}

// reset all fibers in a tile, reset positions and attributes
int OpSim_positioner_reset_fibers (focalplane_struct *pFocal) 
{
  int i;
  if ( pFocal == NULL ) return -1;
  if ( pFocal->pFib == NULL ) return -2;
  for (i=0;i<pFocal->num_fibers;i++)
  {
    if ( OpSim_positioner_reset_fiber ((focalplane_struct*) pFocal, (fiber_struct*) &(pFocal->pFib[i])))
      return 1;
  }
  return 0;
}

// initialise all fibers in a tile, reset positions and attributes, etc
int OpSim_positioner_init_fibers (focalplane_struct *pFocal) 
{
  int i;
  if ( pFocal == NULL ) return -1;
  if ( pFocal->pFib == NULL ) return -2;
  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    //    if ( repair_fiber ((focalplane_struct*) pFocal,(fiber_struct*) pFib ))    return 1;
    pFib->status = FIBER_STATUS_FLAG_NOMINAL;

    pFib->Geom.number_of_movements_total = 0;
    pFib->Geom.distance_moved_dir1_total = 0.0;
    pFib->Geom.distance_moved_dir2_total = 0.0;
    pFib->Geom.number_of_movements = 0;
    pFib->Geom.distance_moved_dir1 = 0.0;
    pFib->Geom.distance_moved_dir2 = 0.0;

    pFib->total_assignments_to_target = 0;
    pFib->total_assignments_to_sky    = 0;
    pFib->total_assignments_to_spare  = 0;
    pFib->total_assignments_to_PItarget = 0;
    pFib->total_num_tiles_spent_in_broken_state = 0;
    pFib->total_assignments_as_lores = 0;
    pFib->total_assignments_as_hires = 0;

    pFib->number_of_repairs = 0;
    pFib->number_of_candidate_targets = 0;
    pFib->number_of_candidate_targets_weighted = 0.0;
    pFib->time_of_last_repair = NAN;

    //    if ( OpSim_positioner_init_fibergeom_struct ((fibergeom_struct*) &(pFib->Geom))) return 1;

    switch (pFocal->positioner_family)
    {
    case POSITIONER_FAMILY_POTZPOZ :
      pFib->Geom.num_vertex = NVERTEX_POTZPOZ;
      pFib->Geom.z0 = (float) 0.0;
      pFib->Geom.z  = (float) 0.0;
      break;
    case POSITIONER_FAMILY_MUPOZ :
      pFib->Geom.num_vertex = NVERTEX_MUPOZ;
      pFib->Geom.z0 = (float) 0.0;
      pFib->Geom.z  = (float) 0.0;
      break;
    case POSITIONER_FAMILY_ECHIDNA :
      pFib->Geom.num_vertex = NVERTEX_ECHIDNA;
      pFib->Geom.z0 = (float) -1.0 * pFocal->spine_length;
      pFib->Geom.z  = (float) 0.0;
      break;
    case POSITIONER_FAMILY_AESOP :
      pFib->Geom.num_vertex = NVERTEX_AESOP;
      pFib->Geom.z0 = (float) -1.0 * pFocal->spine_length;
      pFib->Geom.z  = (float) 0.0;
      break;
    case POSITIONER_FAMILY_STARBUG :
      pFib->Geom.num_vertex = NVERTEX_STARBUG;
      pFib->Geom.z0 = (float) 0.0;
      pFib->Geom.z  = (float) 0.0;
      break;

    default :
    //bad
      return 1;
      break;
    }

    if ( OpSim_positioner_reset_fiber  ((focalplane_struct*) pFocal,(fiber_struct*) pFib ))    return 1;

    
  }
  return 0;
}


int OpSim_positioner_init_fibergeom_struct  (fibergeom_struct *pFibGeom)
{
  int i;
  if ( pFibGeom == NULL ) return 1;
  pFibGeom->x0 = 0.0; 
  pFibGeom->y0 = 0.0;
  pFibGeom->z0 = 0.0; 
  pFibGeom->x  = 0.0;  
  pFibGeom->y  = 0.0;
  pFibGeom->z  = 0.0;  
  pFibGeom->num_vertex = 0; 
  for(i=0;i<MAX_VERTICES_PER_POSITIONER;i++)
  {
    pFibGeom->pos_vertices_x[i] = 0;
    pFibGeom->pos_vertices_y[i] = 0;
  }
  pFibGeom->radial_dist = NAN; 
  pFibGeom->angle1 = NAN;      
  pFibGeom->angle2 = NAN;      
  pFibGeom->axis2_x = NAN;     
  pFibGeom->axis2_y = NAN;
  pFibGeom->number_of_movements_total = 0;      
  pFibGeom->distance_moved_dir1_total = NAN;                 
  pFibGeom->distance_moved_dir2_total = NAN;
  pFibGeom->number_of_movements = 0;            
  pFibGeom->distance_moved_dir1 = NAN;                 
  pFibGeom->distance_moved_dir2 = NAN;

  return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
///////                Geometric collision tests etc                                    ////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collisions(focalplane_struct *pFocal, fiber_struct *pFib, float x, float y )
{
  int result;
  if ( pFocal == NULL ||
       pFib  == NULL  ) return COLLISION_CODE_ERROR;

  switch (pFocal-> positioner_family)
  {
  case POSITIONER_FAMILY_POTZPOZ :
    result = OpSim_positioner_test_collisions_potzpoz( (focalplane_struct*) pFocal,
                                                       (fiber_struct*) pFib,
                                                       (float) x, (float) y);
    break;

  case POSITIONER_FAMILY_MUPOZ :
    result = OpSim_positioner_test_collisions_mupoz( (focalplane_struct*) pFocal,
                                                     (fiber_struct*) pFib,
                                                     (float) x, (float) y);
    break;

  case POSITIONER_FAMILY_ECHIDNA :
    result = OpSim_positioner_test_collisions_echidna( (focalplane_struct*) pFocal,
                                                       (fiber_struct*) pFib,
                                                       (float) x, (float) y);
    break;

  case POSITIONER_FAMILY_AESOP :
    result = OpSim_positioner_test_collisions_aesop( (focalplane_struct*) pFocal,
                                                     (fiber_struct*) pFib,
                                                     (float) x, (float) y);
    break;

  case POSITIONER_FAMILY_STARBUG :
    result = OpSim_positioner_test_collisions_starbug( (focalplane_struct*) pFocal,
                                                       (fiber_struct*) pFib,
                                                       (float) x, (float) y);

    break;
//  case POSITIONER_FAMILY_OLD_MUPOZ :
//    result = test_collisions_old_mupoz( (focalplane_struct*) pFocal,
//                                       (fiber_struct*) pFib,
//                                       (float) x, (float) y);
//    break;
  default :
    //bad
    result = COLLISION_CODE_ERROR2;
    break;
  }
  return result;
}



// test for collision between a potzpoz positioner and all neighbouring positioners
// specifically ask: if we tried to put pFib onto object at x,y then would it collide with any neighbouring fibs? 
// return 0 for no collisions, 1 for >=1 collsions, <0 for error;

int OpSim_positioner_test_collisions_potzpoz( focalplane_struct *pFocal, fiber_struct *pFib, float x, float y)
{
  fibergeom_struct  FibGeom1;
  int i;
  int result;
  if ( pFocal == NULL ||
       pFib  == NULL ) return COLLISION_CODE_ERROR;
  //take a copy of the input fiber geom structure -
  if ( OpSim_positioner_copy_fibergeom_struct((fibergeom_struct*) &FibGeom1,
                                              (const fibergeom_struct*) &(pFib->Geom))) return COLLISION_CODE_ERROR2;

  // and move it to the proposed position
  if ( OpSim_positioner_set_potzpoz_fiber_to_position ((focalplane_struct*) pFocal,
                                                       (fibergeom_struct*) &FibGeom1,
                                                       (float) x, (float) y)) return COLLISION_CODE_ERROR3;

  //now loop through all neighbours that could possibly collide
  for(i=0;i<pFib->nrivals;i++)
  {
    fiber_struct*     pFib2     = (fiber_struct*)     &(pFocal->pFib[pFib->rival_list[i]]);
    fibergeom_struct* pFibGeom2 = (fibergeom_struct*) &(pFib2->Geom);
#ifdef ALLOCATOR_DEBUG_OUTPUT 
    fprintf(stdout, "%s Testing collisions: %2d/%2d dist=%7.3f fib1= %5d %7.3f %7.3f -> %7.3f %7.3f   fib2= %5d %7.3f %7.3f -> %7.3f %7.3f : Collision? ... ",
            DEBUG_PREFIX, i, pFib->nrivals, pFib->rival_dist[i],
            pFib->id,  FibGeom1.x0, FibGeom1.y0, FibGeom1.x, FibGeom1.y,
            pFib2->id, pFibGeom2->x0, pFibGeom2->y0, pFibGeom2->x, pFibGeom2->y); fflush(stdout);

#endif        
    //so now do the full collision testing
    if ( (result = OpSim_positioner_test_collision_potzpoz( (focalplane_struct*) pFocal,
                                                            (fibergeom_struct*) &FibGeom1,
                                                            (fibergeom_struct*) pFibGeom2,
                                                            (float) pFib->rival_dist[i])) != COLLISION_CODE_NONE )
    {
      ////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////
      //this section is clumsy but is intended to improve situation for high res fibers
      //test if the problem fiber is as-yet unassigned and of a different resolution to
      //the fiber we wish to place on a target
      if ((pFib->res != pFib2->res ) &&
          pFib2->assign_flag == FIBER_ASSIGN_CODE_NONE &&
          pFib2->is_empty == FALSE )  //cannot move empty fibers
      {
        //see if we can shift the problematic positioner out of the way
        fibergeom_struct FibGeom2a;
        int result2;
        int j;
        float test_x, test_y;
        //take a copy of the offending fiber geom structure -
        if ( OpSim_positioner_copy_fibergeom_struct((fibergeom_struct*) &FibGeom2a,
                                                    (const fibergeom_struct*) pFibGeom2)) return COLLISION_CODE_ERROR2;
        // just move it 90degrees - we can prob do better than this 
        // depending on the target location wrt to the offending fiber
        test_x = FibGeom2a.x0 + 0.5*(pFocal->potzpoz_rectangle_length - pFocal->potzpoz_tip_radius);
        test_y = FibGeom2a.y0;
        if ( OpSim_positioner_set_potzpoz_fiber_to_position ((focalplane_struct*) pFocal,
                                                             (fibergeom_struct*) &FibGeom2a,
                                                             (float) test_x, (float) test_y))  return COLLISION_CODE_ERROR2;
        
        //check if the new position will improve matters - ie does the new fiber2a position now avoid a collision?
        if ( (result2 = OpSim_positioner_test_collision_potzpoz( (focalplane_struct*) pFocal,
                                                                 (fibergeom_struct*) &FibGeom1,
                                                                 (fibergeom_struct*) &FibGeom2a,
                                                                 (float) pFib->rival_dist[i])) != COLLISION_CODE_NONE )
        {
          return result2;
        }
        
        //double check if the new FibGeom2a position will collide FibGeom2a with any of its neighbours
        for(j=0;j<pFib2->nrivals;j++)
        {
          fiber_struct*     pFib3     = (fiber_struct*)     &(pFocal->pFib[pFib2->rival_list[j]]);
          fibergeom_struct* pFibGeom3 = (fibergeom_struct*) &(pFib3->Geom);
          int result3;
          if ( (fiber_struct*) pFib3 == (fiber_struct*) pFib ) continue;  
          if ( (result3 = OpSim_positioner_test_collision_potzpoz( (focalplane_struct*) pFocal,
                                                                   (fibergeom_struct*) &FibGeom2a,
                                                                   (fibergeom_struct*) pFibGeom3,
                                                                   (float) pFib2->rival_dist[j])) != COLLISION_CODE_NONE )
          {
            return result3;
            
          }
        }
        //if got here then all is well, and we can go ahead with the new positions
        if ( OpSim_positioner_set_potzpoz_fiber_to_position ((focalplane_struct*) pFocal,
                                                             (fibergeom_struct*) pFibGeom2,
                                                             (float) test_x, (float) test_y))  return COLLISION_CODE_ERROR2;
        
#ifdef ALLOCATOR_DEBUG_OUTPUT 
        fprintf(stdout, " Shifted rival %d ", i); fflush(stdout);
#endif        

      }
      ////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////
      else
      {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
        fprintf(stdout, " Yes! (coll_code %2s)\n", COLLISION_CODE_TO_STR(result) );fflush(stdout);
#endif        
        return result;
      }
    }
#ifdef ALLOCATOR_DEBUG_OUTPUT 
      fprintf(stdout, "No\n"); fflush(stdout);
#endif        
  }
  return COLLISION_CODE_NONE;
}


// test for collision between a mupoz positioner and all neighbouring positioners
// specifically ask: if we tried to put pFib onto object at x,y then would it collide with any neighbouring fibs? 
// return 0 for no collisions, 1 for >=1 collsions, <0 for error;
int OpSim_positioner_test_collisions_mupoz( focalplane_struct *pFocal, fiber_struct *pFib, float x, float y)
{
  int result;
  fibergeom_struct  FibGeom1;
  int i;
  //  float sq_min_dist_between_axis2;
  if (   pFocal == NULL || pFib == NULL ) return COLLISION_CODE_ERROR;
  //take a copy of the input fiber geom structure -
  if ( OpSim_positioner_copy_fibergeom_struct((fibergeom_struct*) &FibGeom1,
                                              (const fibergeom_struct*) &(pFib->Geom))) return COLLISION_CODE_ERROR2;

  // and move it to the proposed position
  if ( OpSim_positioner_set_mupoz_fiber_to_position ((focalplane_struct*) pFocal,
                                                     (fibergeom_struct*) &FibGeom1,
                                                     (float) x, (float) y)) return COLLISION_CODE_ERROR3;

    
    //now loop through all neighbours that could possibly collide
  for(i=0;i<pFib->nrivals;i++)
  {
    fiber_struct*     pFib2     = (fiber_struct*)     &(pFocal->pFib[pFib->rival_list[i]]);
    fibergeom_struct* pFibGeom2 = (fibergeom_struct*) &(pFib2->Geom);
    //need to add something here which evaluates the radial distance from
    //the x0,y0 point of the most protruding vertex of the positioner. 
    //then can speed things up by checking if the separation+lengths of the two positioners allows for a collision;
    //    if ( pFib->rival_dist[i] > (FibGeom1.radial_dist + pFibGeom2->radial_dist + pFocal->min_sep_mm))
    //    {
    //      //i.e. this pair of fibers is sufficiently far enough apart and/or
    //      //retracted enough so that no collision is possible
    //      continue; 
    //    }


//    //we can say with certainty that the positioners do not extend further than
//    // mupoz_half_patrol_radius + mupoz_tip_radius from their axis2_x,y points
//    if ( (SQR(pFibGeom2->axis2_x - FibGeom1.axis2_x) +
//          SQR(pFibGeom2->axis2_y - FibGeom1.axis2_y)) > pFocal->mupoz_sq_min_dist_between_axis2)
//      continue;
    
    //there is still a chance of a collision so now do the full collision testing
    if ( (result = OpSim_positioner_test_collision_mupoz( (focalplane_struct*) pFocal,
                                                          (fibergeom_struct*) &FibGeom1,
                                                          (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE )
    {
      return result;
    }
  }
  return COLLISION_CODE_NONE;
}




//test for collision between two potzpoz positioners
// specifically ask: does pFibGeom collide with pFibGeom2? 
//all measurements and positions are in mm!!!!!!!!!!!!!!!!!!!!!!!!
//assume that the fiber vertices have already been calculated by set_potzpoz_fiber_to_position()
// return 0 for no collisions, 1 for >=1 collsions, <0 for error;
//base separation is the previously calculated distance between the fiber base (main rotation axis) positions
int OpSim_positioner_test_collision_potzpoz( focalplane_struct *pFocal,
                                             fibergeom_struct *pFibGeom1,
                                             fibergeom_struct *pFibGeom2,
                                             float main_axis_separation )
{
  int result;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  //speed things up by checking if the separation+lengths of the two fibers allows a collision;
  if ( main_axis_separation > (pFibGeom1->radial_dist + pFibGeom2->radial_dist + 2.0*pFocal->potzpoz_tip_radius + pFocal->min_sep_mm) )
  {
    //i.e. this pair of fibers is sufficiently far enough apart and
    //positioners are sufficiently retracted such that no collision is possible
#ifdef ALLOCATOR_DEBUG_OUTPUT 
    fprintf(stdout, " @ "); fflush(stdout);
#endif    
    return COLLISION_CODE_NONE; 
  }
  
  //   First we can test if the two positioner tips are too close to each other
  if ( (result = OpSim_positioner_test_collision_tip_vs_tip ( (focalplane_struct*) pFocal,
                                                              (fibergeom_struct*) pFibGeom1,
                                                              (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE) return result;
  
  // We can now make tests of collision between each fiber tip, and the straight edges of the 
  // other positioner arm
  if ( (result = OpSim_positioner_test_collision_tips_vs_edges ( (focalplane_struct*) pFocal,
                                                                 (fibergeom_struct*) pFibGeom1,
                                                                 (fibergeom_struct*) pFibGeom2,
                                                                 (float) pFocal->potzpoz_tip_radius)) != COLLISION_CODE_NONE) return result;

  // If we got to here then the tips did not collide with the positioner arms
  // We now need to consider the collisons between the straight edges of the positioner arms 
  if ( (result = OpSim_positioner_test_collision_edges_vs_edges ( (focalplane_struct*) pFocal,
                                                                  (fibergeom_struct*) pFibGeom1,
                                                                  (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE) return result;

  return COLLISION_CODE_NONE;
}


//test for collision between two MUPOZ positioners
// specifically ask: if we put pFib1 onto object ay x,y then would it collide with pFib2? 
//all measurements and positions are in mm!!!!!!!!!!!!!!!!!!!!!!!!
int OpSim_positioner_test_collision_mupoz( focalplane_struct *pFocal,
                                           fibergeom_struct *pFibGeom1,
                                           fibergeom_struct *pFibGeom2)
{
  int result;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  //we can say with certainty that the positioners do not extend further than
  // mupoz_half_patrol_radius + mupoz_tip_radius from their axis2_x,y points
  if ( (SQR(pFibGeom2->axis2_x - pFibGeom1->axis2_x) +
        SQR(pFibGeom2->axis2_y - pFibGeom1->axis2_y)) >
       pFocal->mupoz_sq_min_dist_between_axis2)
  {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
    fprintf(stdout, " @ "); fflush(stdout);
#endif    
    return COLLISION_CODE_NONE;
  }
  
  //   First we can test if the two positioner tips are too close to each other
  if ( (result = OpSim_positioner_test_collision_tip_vs_tip ( (focalplane_struct*) pFocal,
                                             (fibergeom_struct*) pFibGeom1,
                                             (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE) return result;
  
  // We can now make tests of collision between each fiber tip, and the straight edges of the 
  // other positioner arm
  if ( (result = OpSim_positioner_test_collision_tips_vs_edges ( (focalplane_struct*) pFocal,
                                                (fibergeom_struct*) pFibGeom1,
                                                (fibergeom_struct*) pFibGeom2,
                                                (float) pFocal->mupoz_tip_radius)) != COLLISION_CODE_NONE) return result;

  // We can now make tests of collision between the rounded part of each fiber arm,
  // and the straight edges, tips and bases of the other positioner arm
  if ( (result = OpSim_positioner_test_collision_mupoz_bases_vs_all ((focalplane_struct*) pFocal,
                                                    (fibergeom_struct*) pFibGeom1,
                                                    (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE) return result;

  // If we got to here then the tips did not collide with the positioner arms
  // We now need to consider the collisons between the straight edges of the positioner arms 
  if ( (result = OpSim_positioner_test_collision_edges_vs_edges ( (focalplane_struct*) pFocal,
                                                 (fibergeom_struct*) pFibGeom1,
                                                 (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE) return result;
  
  return (int) COLLISION_CODE_NONE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collisions_echidna( focalplane_struct *pFocal, fiber_struct *pFib, float x, float y)
{
  int result;
  fibergeom_struct  FibGeom1;
  int i;
  if (   pFocal == NULL || pFib == NULL ) return COLLISION_CODE_ERROR;

  //take a copy of the input fiber geom structure -
  if ( OpSim_positioner_copy_fibergeom_struct((fibergeom_struct*) &FibGeom1,
                                              (const fibergeom_struct*) &(pFib->Geom))) return COLLISION_CODE_ERROR2;
  
  // and move it to the proposed position
  if ( OpSim_positioner_set_spine_fiber_to_position ((focalplane_struct*) pFocal,
                                                     (fibergeom_struct*) &FibGeom1,
                                                     (float) x, (float) y)) return COLLISION_CODE_ERROR3;

    
    //now loop through all neighbours that could possibly collide
  for(i=0;i<pFib->nrivals;i++)
  {
    fiber_struct*     pFib2     = (fiber_struct*)     &(pFocal->pFib[pFib->rival_list[i]]);
    fibergeom_struct* pFibGeom2 = (fibergeom_struct*) &(pFib2->Geom);
    
    if ( (result = OpSim_positioner_test_collision_echidna ( (focalplane_struct*) pFocal,
                                                             (fibergeom_struct*) &FibGeom1,
                                                             (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE )
    {
      return result;
    }
  }

  return (int) COLLISION_CODE_NONE;
}
///////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////
//TODO - replace test_collision_echidna() with 3d collisions
int OpSim_positioner_test_collision_echidna( focalplane_struct *pFocal,
                                             fibergeom_struct *pFibGeom1,
                                             fibergeom_struct *pFibGeom2)
{
  int result;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;


  //   then we can test if the two positioner tips are too close to each other
  if ( (result = OpSim_positioner_test_collision_tip_vs_tip ( (focalplane_struct*) pFocal,
                                                              (fibergeom_struct*) pFibGeom1,
                                                              (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE) return result;

  //and now test if the spines are crossing each other
  //simple 2D approach:
  //need at least pFocal->min_sep_mm + 2*pFocal->echidna_spine_radius spacing
  if ( geom_collision_line_seg_line_seg(pFibGeom1->x0, pFibGeom1->y0,
                                        pFibGeom1->x,  pFibGeom1->y,
                                        pFibGeom2->x0, pFibGeom2->y0,
                                        pFibGeom2->x,  pFibGeom2->y,
                                        pFocal->min_sep_mm + pFocal->spine_diameter))
  {
    return COLLISION_CODE_EDGE_EDGE;
  }

  
  return COLLISION_CODE_NONE;
}
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collisions_aesop( focalplane_struct *pFocal, fiber_struct *pFib, float x, float y)
{
  int result;
  fibergeom_struct  FibGeom1;
  int i;
  if (   pFocal == NULL || pFib == NULL ) return COLLISION_CODE_ERROR;

  //take a copy of the input fiber geom structure -
  if ( OpSim_positioner_copy_fibergeom_struct((fibergeom_struct*) &FibGeom1,
                                              (const fibergeom_struct*) &(pFib->Geom))) return COLLISION_CODE_ERROR2;
  
  // and move it to the proposed position
  if ( OpSim_positioner_set_spine_fiber_to_position ((focalplane_struct*) pFocal,
                                                     (fibergeom_struct*) &FibGeom1,
                                                     (float) x, (float) y)) return COLLISION_CODE_ERROR3;
    
  //now loop through all neighbours that could possibly collide
  for(i=0;i<pFib->nrivals;i++)
  {
    fiber_struct*     pFib2     = (fiber_struct*)     &(pFocal->pFib[pFib->rival_list[i]]);
    fibergeom_struct* pFibGeom2 = (fibergeom_struct*) &(pFib2->Geom);
    
    if ( (result = OpSim_positioner_test_collision_aesop_AAO_pseudo3D ( (focalplane_struct*) pFocal,
                                                                        (fibergeom_struct*) &FibGeom1,
                                                                        (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE )
    {
      return result;
    }
    
    //    if ( (result = OpSim_positioner_test_collision_aesop_AAO ( (focalplane_struct*) pFocal,
    //                                                               (fibergeom_struct*) &FibGeom1,
    //                                                               (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE )
    //    {
    //      return result;
    //    }
    //
    //    if ( (result = OpSim_positioner_test_collision_aesop_AAO_crossing ( (focalplane_struct*) pFocal,
    //                                                                   (fibergeom_struct*) &FibGeom1,
    //                                                                   (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE )
    //    {
    //      return result;
    //    }
  }
  return (int) COLLISION_CODE_NONE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// AESOP collision - AAO method from FMOS
// consider as collisions any pair of fibre->target assignments where
// swapping the fibres results in a smaller sum of distances from the base positions
// Note that this is apparently only 100% reliable up to r_patrol < 0.9*r_pitch,
// so use the older Echidna collision test as well
int OpSim_positioner_test_collision_aesop_AAO( focalplane_struct *pFocal,
                                               fibergeom_struct *pFibGeom1,
                                               fibergeom_struct *pFibGeom2)
{
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  //Call the normal ECHIDNA collsiion test: test collisions at final positions
  {
    int result; 
    if ( (result = OpSim_positioner_test_collision_echidna( (focalplane_struct*) pFocal,
                                                            (fibergeom_struct*) pFibGeom1,
                                                            (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE)
    {
      return result;
    }
  }

  { // Now Do the AAO AESOP crossing test
    //measure the distances from each fibre base position to each target
    double sq_dist_f1_t1 = SQR(pFibGeom1->x - pFibGeom1->x0) + SQR(pFibGeom1->y - pFibGeom1->y0);
    double sq_dist_f2_t2 = SQR(pFibGeom2->x - pFibGeom2->x0) + SQR(pFibGeom2->y - pFibGeom2->y0);
    double sq_dist_f1_t2 = SQR(pFibGeom2->x - pFibGeom1->x0) + SQR(pFibGeom2->y - pFibGeom1->y0);
    double sq_dist_f2_t1 = SQR(pFibGeom1->x - pFibGeom2->x0) + SQR(pFibGeom1->y - pFibGeom2->y0);
    //first check that both fibres can reach both targets
    //do nested checks to reduce total number of checks needed
    if ( sq_dist_f1_t1 <= pFocal->sq_patrol_radius_max_mm )
    {
      if ( sq_dist_f2_t2 <= pFocal->sq_patrol_radius_max_mm )
      {
        if ( sq_dist_f1_t2 <= pFocal->sq_patrol_radius_max_mm )
        {
          if ( sq_dist_f2_t1 <= pFocal->sq_patrol_radius_max_mm )
          {
            double dist_f1_t1 = sqrt(sq_dist_f1_t1);
            double dist_f2_t2 = sqrt(sq_dist_f2_t2);
            double dist_f1_t2 = sqrt(sq_dist_f1_t2);
            double dist_f2_t1 = sqrt(sq_dist_f2_t1);
            if ( dist_f1_t1 + dist_f2_t2 > dist_f1_t2 + dist_f2_t1 )
            {
              return COLLISION_CODE_CROSSING;      
            }
          }
        }
      }
    }
  }

  return COLLISION_CODE_NONE;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
// AESOP collision - pure AAO crossing method from FMOS
// consider as collisions any pair of fibre->target assignments where
// swapping the fibres results in a smaller sum of distances from the base positions
// Note that this is apparently only 100% reliable up to r_patrol < 0.9*r_pitch,
// Do not use the older Echidna collision test as well
int OpSim_positioner_test_collision_aesop_AAO_crossing( focalplane_struct *pFocal,
                                                        fibergeom_struct *pFibGeom1,
                                                        fibergeom_struct *pFibGeom2)
{
  if ( pFocal    == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  { // Now Do the AAO AESOP crossing test
    //measure the distances from each fibre base position to each target
    double sq_dist_f1_t1 = SQR(pFibGeom1->x - pFibGeom1->x0) + SQR(pFibGeom1->y - pFibGeom1->y0);
    double sq_dist_f2_t2 = SQR(pFibGeom2->x - pFibGeom2->x0) + SQR(pFibGeom2->y - pFibGeom2->y0);
    double sq_dist_f1_t2 = SQR(pFibGeom2->x - pFibGeom1->x0) + SQR(pFibGeom2->y - pFibGeom1->y0);
    double sq_dist_f2_t1 = SQR(pFibGeom1->x - pFibGeom2->x0) + SQR(pFibGeom1->y - pFibGeom2->y0);
    //first check that both fibres can reach both targets
    //do nested checks to reduce total number of checks needed
    if ( sq_dist_f1_t1 <= pFocal->sq_patrol_radius_max_mm )
    {
      if ( sq_dist_f2_t2 <= pFocal->sq_patrol_radius_max_mm )
      {
        if ( sq_dist_f1_t2 <= pFocal->sq_patrol_radius_max_mm )
        {
          if ( sq_dist_f2_t1 <= pFocal->sq_patrol_radius_max_mm )
          {
            double dist_f1_t1 = sqrt(sq_dist_f1_t1);
            double dist_f2_t2 = sqrt(sq_dist_f2_t2);
            double dist_f1_t2 = sqrt(sq_dist_f1_t2);
            double dist_f2_t1 = sqrt(sq_dist_f2_t1);
            if ( dist_f1_t1 + dist_f2_t2 > dist_f1_t2 + dist_f2_t1 )
            {
              return COLLISION_CODE_CROSSING;      
            }
          }
        }
      }
    }
  }

  return COLLISION_CODE_NONE;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
// AESOP collision - AAO pseudo-3D method
// Derived from emails and code sent by Scott Smedly between 01/09/2015 and 03/09/2015 
//
// Restrictions AESOP imposes on the assignment of a spine to a target are:
// a) a minimum separation between assigned targets
// b) a maximum patrol radius for all spines
// c) at the target position a spine must not physically occupy the same 3D space as any of its closest neighbours
// 
// The 3D space restriction is approximated by breaking the positioner into a stack of
// cylinders. It is assumed that only cylinders on the same 'tier' as each other can potentially interact.  
// Only consider collsions between (circular) ends of the cylinders closest to the fibre tip.
//
int OpSim_positioner_test_collision_aesop_AAO_pseudo3D( focalplane_struct *pFocal,
                                                        fibergeom_struct *pFibGeom1,
                                                        fibergeom_struct *pFibGeom2)
{

  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  // TODO (maybe) => move some or all of these to input parameters  
  const double AESOP_length = 250.0; //mm, dist from ball to fibre tip
  //AESOP_cylinders_z[] is array of heights above ball of positioner of each cylinder (as fraction of spine length)
  const double AESOP_cylinders_fz[] = {(190.0 + 42.0 + 18.0)/AESOP_length,   // spine tip                          
                                       (190.0 + 42.0       )/AESOP_length,   // end of taper, beginning of ferrule 
                                       (190.0 + 23.3       )/AESOP_length,   // middle of taper                    
                                       (190.0              )/AESOP_length,   // end of carbon fibre, start of taper
                                       (170.0              )/AESOP_length,   // carbon fibre                       
                                       (150.0              )/AESOP_length,   // carbon fibre                       
                                       (130.0              )/AESOP_length,   // carbon fibre                       
                                       (110.0              )/AESOP_length};  // carbon fibre                       
  //AESOP_cylinders_d2[] is array of squares of diameters at each height (in mm^2)
  const double AESOP_cylinders_d2[] = {SQR(0.5 + 0.1),             // spine tip                          
                                       SQR(0.8 + 0.1),             // end of taper, beginning of ferrule 
                                       SQR(1.6 + 0.2),             // middle of taper                    
                                       SQR(2.5 + 0.2),             // end of carbon fibre, start of taper
                                       SQR(2.5 + 0.2),             // carbon fibre                       
                                       SQR(2.5 + 0.2),             // carbon fibre                       
                                       SQR(2.5 + 0.2),             // carbon fibre                       
                                       SQR(2.5 + 0.2)};            // carbon fibre                       
  //                                             ^^^ tolerance/contingency
  const int ncylinders = (int) (sizeof(AESOP_cylinders_fz)/sizeof(double));
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  
  int i;
  //  int result;

  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;


  // test the conditions in series

  // a) a minimum separation between assigned targets
  //   then we can test if the two positioner tips are too close to each other

  //   We test as a squared distance to avoid a call to sqrt() 
  //   The minimum allowed distance is two times the tip radius plus the small 
  //   additional clearance (min_sep+2*tip_radius) - pre calculated as sq_min_object_sep_mm
  if ( SQR(pFibGeom1->x - pFibGeom2->x) + SQR(pFibGeom1->y - pFibGeom2->y) <= pFocal->sq_min_object_sep_mm )
    return (int) COLLISION_CODE_TIP_TIP;

  //  if ( (result = OpSim_positioner_test_collision_tip_vs_tip ((focalplane_struct*) pFocal,
  //                                                             (fibergeom_struct*)  pFibGeom1,
  //                                                             (fibergeom_struct*)  pFibGeom2)) != COLLISION_CODE_NONE)
  //  {
  //    return result;
  //  }

  // b) a maximum patrol radius for all spines
  // should already be prevented

  
  // c) at the target position a spine must not physically occupy the same 3D space as any of its closest neighbours
  //step backwards through cylinder array, i.e. in order of largest diameter (and greatest likelihood of collisions)
  for(i=ncylinders-1;i>=0;i--)   
  //  for(i=0;i<ncylinders;i++)
  {
    //calculate the central (x,y) coords of the two cylinder ends
    double x1 = (AESOP_cylinders_fz[i] * (pFibGeom1->x - pFibGeom1->x0)) + pFibGeom1->x0;
    double y1 = (AESOP_cylinders_fz[i] * (pFibGeom1->y - pFibGeom1->y0)) + pFibGeom1->y0;
    double x2 = (AESOP_cylinders_fz[i] * (pFibGeom2->x - pFibGeom2->x0)) + pFibGeom2->x0;
    double y2 = (AESOP_cylinders_fz[i] * (pFibGeom2->y - pFibGeom2->y0)) + pFibGeom2->y0;
    if ( (SQR(x1-x2) + SQR(y1-y2)) <= AESOP_cylinders_d2[i] )
      return (int) (COLLISION_CODE_CYLINDER_ROOT + i);  //return a useful code to identify which cylinders intersected
  } 
  
  return (int) COLLISION_CODE_NONE;  //no collisions
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

int OpSim_positioner_test_collisions_starbug( focalplane_struct *pFocal, fiber_struct *pFib, float x, float y)
{
  int result;
  fibergeom_struct  FibGeom1;
  int i;
  if (   pFocal == NULL || pFib == NULL ) return COLLISION_CODE_ERROR;

  //take a copy of the input fiber geom structure -
  if ( OpSim_positioner_copy_fibergeom_struct((fibergeom_struct*) &FibGeom1,
                                              (const fibergeom_struct*) &(pFib->Geom))) return COLLISION_CODE_ERROR2;
  
  // and move it to the proposed position
  if ( OpSim_positioner_set_starbug_fiber_to_position ((focalplane_struct*) pFocal,
                                                       (fibergeom_struct*) &FibGeom1,
                                                       (float) x, (float) y)) return COLLISION_CODE_ERROR3;

  //now loop through all neighbours that could possibly collide
  for(i=0;i<pFib->nrivals;i++)
  {
    fiber_struct*     pFib2     = (fiber_struct*)     &(pFocal->pFib[pFib->rival_list[i]]);
    fibergeom_struct* pFibGeom2 = (fibergeom_struct*) &(pFib2->Geom);
    
    if ( (result = OpSim_positioner_test_collision_starbug ( (focalplane_struct*) pFocal,
                                                             (fibergeom_struct*) &FibGeom1,
                                                             (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE )
    {
      return result;
    }
  }

  return (int) COLLISION_CODE_NONE;
}

int OpSim_positioner_test_collision_starbug( focalplane_struct *pFocal,
                                             fibergeom_struct *pFibGeom1,
                                             fibergeom_struct *pFibGeom2)
{
  int result;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  //   then we can test if the two starbugs are too close to each other
  if ( (result = OpSim_positioner_test_collision_tip_vs_tip ( (focalplane_struct*) pFocal,
                                                              (fibergeom_struct*) pFibGeom1,
                                                              (fibergeom_struct*) pFibGeom2)) != COLLISION_CODE_NONE)
  {
    return result;
  }
  return COLLISION_CODE_NONE;
}









////////////////////////////////////////////////////////////////////////////
//http://softsurfer.com/Archive/algorithm_0106/algorithm_0106.htm#dist3D_Segment_to_Segment
//TODO - complete the test_collision3D_cylinder_vs_cylinder() function
// see also http://www.geometrictools.com/Documentation/IntersectionOfCylinders.pdf
int OpSim_positioner_test_collision3D_cylinder_vs_cylinder ( focalplane_struct *pFocal,
                                                             fibergeom_struct  *pFibGeom1,
                                                             fibergeom_struct  *pFibGeom2 )
{
  if ( pFocal    == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return (int) COLLISION_CODE_ERROR;
  

  return (int) COLLISION_CODE_ERROR;
}




////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collision_tip_vs_tip ( focalplane_struct *pFocal,
                                                 fibergeom_struct  *pFibGeom1,
                                                 fibergeom_struct  *pFibGeom2 )
{
  if ( pFocal    == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return (int) COLLISION_CODE_ERROR;

  // We can now make tests of collision between the two fiber tips
  //   We test as a squared distance to avoid a call to sqrt() 
  //   The minimum allowed distance is two times the tip radius plus the small 
  //   additional clearance (min_sep+2*tip_radius) - pre calculated as sq_min_object_sep_mm
  if ( SQR(pFibGeom1->x - pFibGeom2->x) + SQR(pFibGeom1->y - pFibGeom2->y) <= pFocal->sq_min_object_sep_mm )
    return (int) COLLISION_CODE_TIP_TIP;

  return (int) COLLISION_CODE_NONE;
}


////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collision_tips_vs_edges ( focalplane_struct      *pFocal,
                                   fibergeom_struct *pFibGeom1,
                                   fibergeom_struct *pFibGeom2,
                                   float tip_radius)
{
  int tip,num_vertex;
  int l1,l1pp;
  fibergeom_struct *pArm = NULL;
  fibergeom_struct *pTip = NULL;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  // We can now make tests of collision between each fiber tip, and the straight edges of the 
  // other positioner arm
  // the outer perimeter of the tip must be separated from another
  // positioner by a minimum of min_sep 
  //
  // Loop over each positioner tip - call it "tip"
  // compare with the other positioner's arm - call it "arm"
  for(tip=0;tip<COLLISION_NTIPS;tip++)
  {
    if ( tip == COLLISION_TIP1 )
    {
      pTip = (fibergeom_struct*) pFibGeom1;
      pArm = (fibergeom_struct*) pFibGeom2;
      num_vertex = pFibGeom2->num_vertex;
    }
    else
    {
      pTip = (fibergeom_struct*) pFibGeom2;
      pArm = (fibergeom_struct*) pFibGeom1;
      num_vertex = pFibGeom1->num_vertex;
    }
    
    // l1 and l1+1 define the start and stop vertex of each line segment of the arm
    for(l1=0;l1<num_vertex;l1++)
    {
      l1pp = (l1+1) % num_vertex;
      if ( geom_test_if_near_line_seg( pTip->x,                   pTip->y,
                                       pArm->pos_vertices_x[l1],  pArm->pos_vertices_y[l1],
                                       pArm->pos_vertices_x[l1pp],pArm->pos_vertices_y[l1pp],
                                       pFocal->min_sep_mm + tip_radius))
      {
        return COLLISION_CODE_TIP_EDGE;
      }
    }
  }
  return COLLISION_CODE_NONE;
}


////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collision_mupoz_bases_vs_all ( focalplane_struct      *pFocal,
                                        fibergeom_struct *pFibGeom1,
                                        fibergeom_struct *pFibGeom2)
{
  int base;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  // We can now make tests of collision between the rounded 'base' part of each
  // fiber positioner arm, and the straight edges of the other positioner arm,
  // the tip of the other arm, and the base of the other arm.
  // The outer perimeter of the base must be separated from another
  // positioner by a minimum of min_sep 
  // just test as if it were a full circle, rather than a semicircle
  // assume that base is centred at 2nd rotation axis...this is a litle bit pessimistic.
  //
  // Loop over each positioner base - call it "base"
  // compare with the other positioner's arm - call it "arm"
  for(base=0;base<COLLISION_NBASES;base++)
  {
    int l1;
    int num_vertex;
    fibergeom_struct *pThis = NULL; //the positioner with the base
    fibergeom_struct *pThat = NULL; //the other positioner
    if ( base == COLLISION_BASE1 )
    {
      pThis = (fibergeom_struct*) pFibGeom1;
      pThat = (fibergeom_struct*) pFibGeom2;
      num_vertex = pFibGeom2->num_vertex;
    }
    else
    {
      pThis = (fibergeom_struct*) pFibGeom2;
      pThat = (fibergeom_struct*) pFibGeom1;
      num_vertex = pFibGeom1->num_vertex;
    }

    //test against the tip
    //   We test as a squared distance to avoid a call to sqrt() 
    if (SQR(pThis->axis2_x - pThat->x) + SQR(pThis->axis2_y - pThat->y) <
        SQR(pFocal->min_sep_mm + pFocal->mupoz_half_arm_width + pFocal->mupoz_tip_radius))
      return COLLISION_CODE_BASE_TIP;

    
    // l1 and l1+1 define the start and stop vertex of each line segment of the arm
    for(l1=0;l1<num_vertex;l1++)
    {
      int l1pp = (l1+1) % num_vertex;
      if ( geom_test_if_near_line_seg( pThis->axis2_x,             pThis->axis2_y,
                                       pThat->pos_vertices_x[l1],  pThat->pos_vertices_y[l1],
                                       pThat->pos_vertices_x[l1pp],pThat->pos_vertices_y[l1pp],
                                       pFocal->min_sep_mm + pFocal->mupoz_half_arm_width))
      {
        return COLLISION_CODE_BASE_EDGE;
      }
    }

    //test against the other basep
    //   We test as a squared distance to avoid a call to sqrt() 
    if (SQR(pThis->axis2_x - pThat->axis2_x) + SQR(pThis->axis2_y - pThat->axis2_y) <
        SQR(pFocal->min_sep_mm + pFocal->mupoz_arm_width))
      return COLLISION_CODE_BASE_BASE;
    
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////
int OpSim_positioner_test_collision_edges_vs_edges ( focalplane_struct      *pFocal,
                                                     fibergeom_struct *pFibGeom1,
                                                     fibergeom_struct *pFibGeom2)
{
  int l1;
  if ( pFocal     == NULL ||
       pFibGeom1 == NULL ||
       pFibGeom2 == NULL ) return COLLISION_CODE_ERROR;

  // We now need to consider the collisons between the straight edges of the positioner arms 
  // The perimeter of each positioner arm must be separated from another
  // positioner arm by a minimum of min_sep 
  // We test each line segment in positioner 1 against every line segment in positioner 2
  //
  // l1 and l2 define the start and stop vertex of each line segment of the arm
  // l1=0 -> line between vertex 1 and 2 of positioner 1 
  // l1=1 -> line between vertex 2 and 3 of positioner 1 
  // l1=2 -> line between vertex 3 and 4 of positioner 1 
  // l1=3 -> line between vertex 4 and 1 of positioner 1 
  // l2=0 -> line between vertex 1 and 2 of positioner 2 
  // l2=1 -> line between vertex 2 and 3 of positioner 2 
  // l2=2 -> line between vertex 3 and 4 of positioner 2 
  // l2=3 -> line between vertex 4 and 1 of positioner 2 
  // for each line segment of positioner 1
  for(l1=0;l1<pFibGeom1->num_vertex;l1++)
  {
    int l2;
    int l1pp = (l1+1) % pFibGeom1->num_vertex;
    // for each line segment of positioner 2
    for(l2=0;l2<pFibGeom2->num_vertex;l2++)
    {
      int l2pp = (l2+1) % pFibGeom2->num_vertex;
      // test for a collision - between the two line segs

      if ( geom_collision_line_seg_line_seg(pFibGeom1->pos_vertices_x[l1],   pFibGeom1->pos_vertices_y[l1],
                                            pFibGeom1->pos_vertices_x[l1pp], pFibGeom1->pos_vertices_y[l1pp],
                                            pFibGeom2->pos_vertices_x[l2],   pFibGeom2->pos_vertices_y[l2],
                                            pFibGeom2->pos_vertices_x[l2pp], pFibGeom2->pos_vertices_y[l2pp],
                                            pFocal->min_sep_mm))

      {
        return COLLISION_CODE_EDGE_EDGE;
      }
    }
  }
  return COLLISION_CODE_NONE;
}



////////////////
int OpSim_positioner_set_fiber_to_position  (focalplane_struct *pFocal, fiber_struct *pFib, float x, float y)
{
  if ( pFocal == NULL ||  pFib  == NULL  ) return -1;
  if ( pFib->is_empty ) return -1;///TODO better here?
  return OpSim_positioner_set_fibergeom_to_position ((focalplane_struct*) pFocal,
                                                     (fibergeom_struct*) &(pFib->Geom),
                                                     (float) x, (float) y);
}

////////////////
int OpSim_positioner_set_fibergeom_to_position  (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y)
{
  int result = 0;
  if ( pFocal     == NULL ||
       pFibGeom  == NULL  ) return -1;

  //check if we are moving the positioner to its current location 
  if ( FLT_CMP(x,pFibGeom->x) &&
       FLT_CMP(y,pFibGeom->y) )
  {
    return 0; //get here if new_x==old_x and new_y==old_y
  }

  switch (pFocal-> positioner_family)
  {
  case POSITIONER_FAMILY_POTZPOZ :
    result = OpSim_positioner_set_potzpoz_fiber_to_position ((focalplane_struct*) pFocal, (fibergeom_struct*) pFibGeom, (float) x, (float) y);
    break;
  case POSITIONER_FAMILY_MUPOZ :
    result = OpSim_positioner_set_mupoz_fiber_to_position   ((focalplane_struct*) pFocal, (fibergeom_struct*) pFibGeom, (float) x, (float) y);
    break;
  case POSITIONER_FAMILY_ECHIDNA :
  case POSITIONER_FAMILY_AESOP :
    result = OpSim_positioner_set_spine_fiber_to_position   ((focalplane_struct*) pFocal, (fibergeom_struct*) pFibGeom, (float) x, (float) y);
    break;
  case POSITIONER_FAMILY_STARBUG :
    result = OpSim_positioner_set_starbug_fiber_to_position ((focalplane_struct*) pFocal, (fibergeom_struct*) pFibGeom, (float) x, (float) y);
    break;
  default :
    //bad
    result = 1;
    break;
  }
  return result;
}

//set a potzpoz fiber to a given position
// x,y are in in mm and are relative to the centre of the focal plane
// no checking if x,y is a sensible position
// rely on a previous check that one can place a potzpoz fiber at that location
// without collisions etc
int OpSim_positioner_set_potzpoz_fiber_to_position (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y)
{
  float u[NVERTEX_POTZPOZ];  //these are the unrotated coords of the vertices
  float v[NVERTEX_POTZPOZ];
  float nominal_offset;
  int q;
  float  cos_phi;  //the cos and dec of the angle of the positioner
  float  sin_phi;
  float  old_radius;
  float  old_angle;
  
  if ( pFocal == NULL || pFibGeom == NULL ) return -1;
  if ( pFocal->positioner_family !=  POSITIONER_FAMILY_POTZPOZ) return -2;

  old_radius = pFibGeom->radial_dist;
  old_angle  = pFibGeom->angle1;
  pFibGeom->x = (float) x;
  pFibGeom->y = (float) y;

  //  pFibGeom->num_vertex = NVERTEX_POTZPOZ;
  //now calc the other stuff
  pFibGeom->radial_dist = (float) sqrt(SQR(pFibGeom->x - pFibGeom->x0) + SQR(pFibGeom->y - pFibGeom->y0));  
  if ( pFibGeom->radial_dist > (float) 0.0 )
  {
    cos_phi = (pFibGeom->y-pFibGeom->y0)/pFibGeom->radial_dist;
    sin_phi = (pFibGeom->x-pFibGeom->x0)/pFibGeom->radial_dist;
    pFibGeom->angle1 = (float) acosd((float) cos_phi);
    if ( (pFibGeom->x-pFibGeom->x0) < 0.0 ) pFibGeom->angle1 += (float) 180.0; //make sure that we account for angles in 3rd and 4th quadrants
  }
  else if ( pFibGeom->radial_dist < (float) 0.0 )
  {
    //this should never happen!
    return -1;
  }
  else
  {
    //this should never happen if there is a minimum patrol radius
    //but leave it in just in case
    cos_phi = (float) 1.0;
    sin_phi = (float) 0.0;
    pFibGeom->angle1 = (float) 0.0;
  }

  //update the log of movements for this fiber
  {
    float dist_moved_dir1 = fabsf(old_radius - pFibGeom->radial_dist);
    float dist_moved_dir2 = fabsf(old_angle  - pFibGeom->angle1);//assume that can just go the quickest way around
    if ( dist_moved_dir1 > 0.0 || dist_moved_dir2 > 0.0 )
    {
      pFibGeom->number_of_movements++;
      pFibGeom->distance_moved_dir1 += dist_moved_dir1;
      pFibGeom->distance_moved_dir2 += dist_moved_dir2; 
      pFibGeom->number_of_movements_total++;
      pFibGeom->distance_moved_dir1_total += dist_moved_dir1;
      pFibGeom->distance_moved_dir2_total += dist_moved_dir2; 
      //should probably code up a better solution
    }
  }
  
  //now calculate the unrotated locations of the vertices.
  //Now, work out the location of each vertex in "unrotated" coordinates
  // vertex 1
  u[0] = pFocal->potzpoz_half_width;
  v[0] = pFibGeom->radial_dist;
  // vertex 2
  u[1] = pFocal->potzpoz_half_width;
  // The v coordinate of the rear of the positioner depends on the radial extension
  // There is a minimum offset of potzpoz_pivot_offset behind the rotation axis, 
  // and a maximum offset of potzpoz_rectangle_length - r_patrol_min
  nominal_offset = pFocal->potzpoz_rectangle_length - pFibGeom->radial_dist;
  if ( nominal_offset < pFocal->potzpoz_pivot_offset )  
  {
    //in this case, the positioner is extended sufficiently to use the minumum offset.            
    v[1] = pFocal->potzpoz_pivot_offset * (float) -1.0;
  }
  else
  {
    // in this case, part of the potzpoz_rectangle_length extends
    //an additional amount behind the rotation axis
    v[1] = nominal_offset * (float) -1.0;
  }
  //     vertex 3
  u[2] = -1.0 * pFocal->potzpoz_half_width;
  v[2] = v[1];
  //     vertex 4
  u[3] = -1.0 * pFocal->potzpoz_half_width;
  v[3] = pFibGeom->radial_dist;


  // We now calculate the coordinates of the vertices in the focal plane
  // Use a standard rotation matrix - here a +ve phi rotates anticlockwise:  
  // and don't forget to add the starting coords!
  // in matrix notation:
  // (xx) = (cos_phi -sin_phi) (u)  + (xa)
  // (yy)   (sin_phi  cos_phi) (v)  + (ya)
  // for each positioner
  //for each vertex
  for(q=0;q<NVERTEX_POTZPOZ;q++)
  {
    pFibGeom->pos_vertices_x[q] = -1.*(cos_phi*u[q] - sin_phi*v[q]) + pFibGeom->x0;
    pFibGeom->pos_vertices_y[q] = (sin_phi*u[q] + cos_phi*v[q]) + pFibGeom->y0;
  }
  return 0;
}
//////////////////////////////////////////

//set a mupoz fiber to a given position
// x,y are in in mm and are relative to the centre of the focal plane
// no checking if x,y is a sensible position
// rely on a previous check that one can actually place a muzpoz fiber at
// that location without collisions etc
int OpSim_positioner_set_mupoz_fiber_to_position (focalplane_struct *pFocal,
                                                  fibergeom_struct *pFibGeom,
                                                  float x, float y)
{
  float omega, phi, theta;// cos_theta, sin_theta;
  float cos_phi, sin_phi, cos_pt, sin_pt;
  float old_phi;
  float old_theta;
  if ( pFocal == NULL || pFibGeom == NULL ) return -1;
  if ( pFocal->positioner_family !=  POSITIONER_FAMILY_MUPOZ) return -2;

  old_theta = pFibGeom->angle1;
  old_phi   = pFibGeom->angle2;
  pFibGeom->x = (float) x;
  pFibGeom->y = (float) y;
  //  pFibGeom->num_vertex = NVERTEX_MUPOZ;

  //now calc the other stuff
  pFibGeom->radial_dist = (float) sqrt(SQR(pFibGeom->x - pFibGeom->x0) + SQR(pFibGeom->y - pFibGeom->y0));  
  if ( pFibGeom->radial_dist > (float) 0.0       &&
       ((float) (pFibGeom->y-pFibGeom->y0) != (float) 0.0))
  {
    //note that the normal sense of the x and y values have been swapped to give the correct value
      // of omega=0 at dx=0,dy=1
      //    omega  = (float) atan2(pFibGeom->x-pFibGeom->x0,pFibGeom->y-pFibGeom->y0);
    omega  = (float) atan2(pFibGeom->x0-pFibGeom->x,pFibGeom->y-pFibGeom->y0);
    theta  = (float) (2.0*asin(pFibGeom->radial_dist/pFocal->patrol_radius_max_mm) - (float) M_PI);
  }
  else
  {
    omega  = (float) 0.0;  
    theta  = (float) M_PI;
    //    phi    = (float) M_PI;
  }
  phi    = (float) (omega - theta * 0.5);
  pFibGeom->angle1 = theta;
  pFibGeom->angle2 = phi;
  
  //update the log of movements for this fiber
  {
    float dist_moved_dir1 = fabsf(old_theta - pFibGeom->angle1);
    float dist_moved_dir2 = fabsf(old_phi   - pFibGeom->angle2);//assume that can just go the quickest way around
    if ( dist_moved_dir1 > 0.0 || dist_moved_dir2 > 0.0 )
    {
      pFibGeom->number_of_movements++;
      pFibGeom->distance_moved_dir1 += dist_moved_dir1;
      pFibGeom->distance_moved_dir2 += dist_moved_dir2; 
      pFibGeom->number_of_movements_total++;
      pFibGeom->distance_moved_dir1_total += dist_moved_dir1;
      pFibGeom->distance_moved_dir2_total += dist_moved_dir2;
      //should code up a better solution  
    }
  }
  //  cos_theta = cos(theta);
  //  sin_theta = sin(theta);
  
  cos_phi = cos(phi);
  sin_phi = sin(phi);
  cos_pt  = cos(phi+theta);
  sin_pt  = sin(phi+theta);
  //  this is the location of the 2nd optical axis
  pFibGeom->axis2_x = pFibGeom->x0 - pFocal->mupoz_half_patrol_radius*sin_phi;
  pFibGeom->axis2_y = pFibGeom->y0 + pFocal->mupoz_half_patrol_radius*cos_phi;
  
  //     We now calculate the coordinates of the vertices in the focal plane
  //     vertex 1, bottom left
  geomf_fast_rotate_about_axis(pFibGeom->axis2_x,pFibGeom->axis2_y,sin_pt,cos_pt,
                               pFibGeom->axis2_x - pFocal->mupoz_half_arm_width,
                               pFibGeom->axis2_y + pFocal->mupoz_arm_start,
                               &(pFibGeom->pos_vertices_x[0]), &(pFibGeom->pos_vertices_y[0]));
  
  //    vertex 2, top left 
  geomf_fast_rotate_about_axis(pFibGeom->axis2_x,pFibGeom->axis2_y,sin_pt,cos_pt,
                               pFibGeom->axis2_x - pFocal->mupoz_half_tip_width,
                               pFibGeom->axis2_y + pFocal->mupoz_half_patrol_radius,
                               &(pFibGeom->pos_vertices_x[1]), &(pFibGeom->pos_vertices_y[1]));
  
  //     vertex 3, top right 
  geomf_fast_rotate_about_axis(pFibGeom->axis2_x,pFibGeom->axis2_y,sin_pt,cos_pt,
                               pFibGeom->axis2_x + pFocal->mupoz_half_tip_width,
                               pFibGeom->axis2_y + pFocal->mupoz_half_patrol_radius,
                               &(pFibGeom->pos_vertices_x[2]), &(pFibGeom->pos_vertices_y[2]));
    
  //     vertex 4, bottom right
  geomf_fast_rotate_about_axis(pFibGeom->axis2_x,pFibGeom->axis2_y,sin_pt,cos_pt,
                               pFibGeom->axis2_x + pFocal->mupoz_half_arm_width,
                               pFibGeom->axis2_y + pFocal->mupoz_arm_start,
                               &(pFibGeom->pos_vertices_x[3]), &(pFibGeom->pos_vertices_y[3]));
  
  //done
  return 0;
}
//////////////////////////////////////////

int OpSim_positioner_set_spine_fiber_to_position (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y)
{
  float  old_x;
  float  old_y;
  float  cos_phi;  //the cos and dec of the angle of the positioner
  float  sin_phi;
  if ( pFocal == NULL || pFibGeom == NULL ) return -1;
  if ( pFocal->positioner_family != POSITIONER_FAMILY_AESOP &&
       pFocal->positioner_family != POSITIONER_FAMILY_ECHIDNA) return -2;

  old_x = (float) pFibGeom->x;
  old_y = (float) pFibGeom->y;
  pFibGeom->x = (float) x;
  pFibGeom->y = (float) y;
  pFibGeom->radial_dist = (float) sqrt(SQR(pFibGeom->x - pFibGeom->x0) + SQR(pFibGeom->y - pFibGeom->y0));  
  if ( pFibGeom->radial_dist > 0.0 )
  {
    cos_phi = (float) (pFibGeom->y-pFibGeom->y0)/pFibGeom->radial_dist;
    sin_phi = (float) (pFibGeom->x-pFibGeom->x0)/pFibGeom->radial_dist;
    pFibGeom->angle1 = (float) atan2fd(pFibGeom->y-pFibGeom->y0,pFibGeom->x-pFibGeom->x0);
    pFibGeom->z = (float) sqrt(SQR(pFocal->spine_length) - SQR(pFibGeom->radial_dist)) - pFocal->spine_length;
  }
  else
  {
    cos_phi = (float) 1.0;
    sin_phi = (float) 0.0;
    pFibGeom->angle1 = (float) 0.0;
    pFibGeom->z = (float) 0.0;
  }
  
  //set the vertex positions
  //TODO - triple-check the following equations 
  {
    float cos_phi_radius = cos_phi * pFocal->spine_radius;
    float sin_phi_radius = sin_phi * pFocal->spine_radius;
    pFibGeom->pos_vertices_x[0] = pFibGeom->x0 - cos_phi_radius;
    pFibGeom->pos_vertices_y[0] = pFibGeom->y0 + sin_phi_radius;
    pFibGeom->pos_vertices_x[1] = pFibGeom->x  - cos_phi_radius;
    pFibGeom->pos_vertices_y[1] = pFibGeom->y  + sin_phi_radius;
    pFibGeom->pos_vertices_x[2] = pFibGeom->x  + cos_phi_radius;
    pFibGeom->pos_vertices_y[2] = pFibGeom->y  - sin_phi_radius;
    pFibGeom->pos_vertices_x[3] = pFibGeom->x0 + cos_phi_radius;
    pFibGeom->pos_vertices_y[3] = pFibGeom->y0 - sin_phi_radius;
  }
  //update the log of movements for this fiber
  {
    //    float dist_moved_dir1 = fabsf(old_radius - pFibGeom->radial_dist);
    //    float dist_moved_dir2 = fabsf(old_angle  - pFibGeom->angle1); //assume that can just go the quickest way around
    float dist_moved_dir1 = (float) 0.0;
    float dist_moved_dir2 = (float) 0.0;
    dist_moved_dir1 = (float) fabsf(old_x - pFibGeom->x);
    dist_moved_dir2 = (float) fabsf(old_y - pFibGeom->y); //assume that can just move directly from x1,y1 to x2,y2
    if ( dist_moved_dir1 > (float) 0.0 || dist_moved_dir2 > (float) 0.0 )
    {
      pFibGeom->number_of_movements++;
      pFibGeom->distance_moved_dir1 += dist_moved_dir1;
      pFibGeom->distance_moved_dir2 += dist_moved_dir2;
      pFibGeom->number_of_movements_total++;
      pFibGeom->distance_moved_dir1_total += dist_moved_dir1;
      pFibGeom->distance_moved_dir2_total += dist_moved_dir2;
    }
  }
  return 0;
}

//int OpSim_positioner_set_echidna_fiber_to_position (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y)
//{
//  if ( pFocal == NULL || pFibGeom == NULL ) return -1;
//  if ( pFocal->positioner_family != POSITIONER_FAMILY_ECHIDNA ) return -2;
//
//  if ( OpSim_positioner_set_spine_fiber_to_position ((focalplane_struct*) pFocal, (fibergeom_struct*) pFibGeom,
//                                                     (float) x, (float) y)) return 1;
//
//  return 0;
//}
//
//
//
//int OpSim_positioner_set_aesop_fiber_to_position (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y)
//{
//  float  old_x;
//  float  old_y;
//  float  cos_phi;  //the cos and dec of the angle of the positioner
//  float  sin_phi;
//  if ( pFocal == NULL || pFibGeom == NULL ) return -1;
//  if ( pFocal->positioner_family != POSITIONER_FAMILY_AESOP ) return -2;
//
//  if ( OpSim_positioner_set_spine_fiber_to_position ((focalplane_struct*) pFocal, (fibergeom_struct*) pFibGeom,
//                                                     (float) x, (float) y)) return 1;
//
//  return 0;
//}







int OpSim_positioner_set_starbug_fiber_to_position (focalplane_struct *pFocal, fibergeom_struct *pFibGeom, float x, float y)
{
  float  old_x;
  float  old_y;
  if ( pFocal == NULL || pFibGeom == NULL ) return -1;
  if ( pFocal->positioner_family != POSITIONER_FAMILY_STARBUG) return -2;

  old_x = pFibGeom->x;
  old_y = pFibGeom->y;
  pFibGeom->x = (float) x;
  pFibGeom->y = (float) y;
  pFibGeom->radial_dist = (float) sqrt(SQR(pFibGeom->x - pFibGeom->x0) + SQR(pFibGeom->y - pFibGeom->y0));  
  pFibGeom->angle1 = (float) NAN;
  pFibGeom->z = (float) 0.0;
  {
    float dist_moved_dir1 = fabsf(old_x - pFibGeom->x);
    float dist_moved_dir2 = fabsf(old_y - pFibGeom->y); //assume that can just move directly from x1,y1 to x2,y2
    if ( dist_moved_dir1 > 0.0 || dist_moved_dir2 > 0.0 )
    {
      pFibGeom->number_of_movements++;
      pFibGeom->distance_moved_dir1 += dist_moved_dir1;
      pFibGeom->distance_moved_dir2 += dist_moved_dir2;
      pFibGeom->number_of_movements_total++;
      pFibGeom->distance_moved_dir1_total += dist_moved_dir1;
      pFibGeom->distance_moved_dir2_total += dist_moved_dir2;
    }
  }
  return 0;
}
  







//set a positioner to its base position
int OpSim_positioner_set_fiber_to_null_position (focalplane_struct *pFocal, fiber_struct *pFib) 
{
  if ( pFocal == NULL ||
       pFib == NULL ) return -1;
  //  pFib->pObj = NULL;
  //  pFib->assign_flag = FIBER_ASSIGN_CODE_NONE;
  //  pFib->assignment_order = -1;

  //only move the fiber if it is not stuck
  //  if ( (pFib->status & (uint) FIBER_STATUS_FLAG_STUCK) == (uint) FIBER_STATUS_FLAG_NOMINAL )
  if ( (pFib->status & (uint) FIBER_STATUS_FLAG_STUCK) == (uint) 0 )
  {
    switch ( pFocal->positioner_family )
    {
    case POSITIONER_FAMILY_POTZPOZ :
      //     POTZPOZ - stored so that fiber arm is symmetric about rotation axis
      //     - and pointing along y-axis
      //Maybe improve this so that low res fibers start off pointing out of the way of neighbouring
      //high res fibers - hard to do if hi-lo pattern is 1in6
      if ( OpSim_positioner_set_potzpoz_fiber_to_position ((focalplane_struct*) pFocal,
                                                           (fibergeom_struct*) &(pFib->Geom),
                                                           (float) pFib->Geom.x0,
                                                           (float) (pFib->Geom.y0 +
                                                                    0.5*(pFocal->potzpoz_rectangle_length - pFocal->potzpoz_tip_radius))))
        return 1;
      break;
    case POSITIONER_FAMILY_MUPOZ :
      if ( OpSim_positioner_set_mupoz_fiber_to_position ((focalplane_struct*) pFocal,
                                                         (fibergeom_struct*) &(pFib->Geom),
                                                         (float) pFib->Geom.x0,
                                                         (float) pFib->Geom.y0)) return 1;
      break;
    case POSITIONER_FAMILY_ECHIDNA :
    case POSITIONER_FAMILY_AESOP :
      if ( OpSim_positioner_set_spine_fiber_to_position    ((focalplane_struct*) pFocal,
                                                           (fibergeom_struct*) &(pFib->Geom),
                                                           (float) pFib->Geom.x0,
                                                           (float) pFib->Geom.y0)) return 1;
      break;
    case POSITIONER_FAMILY_STARBUG :
      if ( OpSim_positioner_set_starbug_fiber_to_position ((focalplane_struct*) pFocal,
                                                           (fibergeom_struct*) &(pFib->Geom),
                                                           (float) pFib->Geom.x0,
                                                           (float) pFib->Geom.y0)) return 1;
      break;
      //  case POSITIONER_FAMILY_OLD_MUPOZ :
      //    if ( set_old_mupoz_fiber_to_position ((focalplane_struct*) pFocal,
      //                                          (fibergeom_struct*) &(pFib->Geom),
      //                                          (float) pFib->Geom.x0,
      //                                          (float) pFib->Geom.y0)) return 1;
      //    break;
    default :
      //unknown positioner type
      return -1;
      break;
    }
  }
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

//set all positioners to their base position
int OpSim_positioner_set_fibers_to_null_position (focalplane_struct *pFocal) 
{
  int i;
  if ( pFocal == NULL ) return -1;
  if ( pFocal->pFib == NULL ) return -2;
  for (i=0;i<pFocal->num_fibers;i++)
  {
    if ( OpSim_positioner_set_fiber_to_null_position ((focalplane_struct*) pFocal,
                                                      (fiber_struct*) &(pFocal->pFib[i])))
      return 1;
  }
  return 0;
}








/////////////////////////////////////////////////
int OpSim_positioner_write_instantaneous_positioner_geometry_potzpoz (FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib)
{
  int i;
  if ( pFile == NULL ||
       pFocal == NULL ||
       pFib  == NULL ) return 1;

  //draw a vector from the main rotation axis to the fiber
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "vector",
          pFib->Geom.x0, pFib->Geom.y0,
          pFib->Geom.x - pFib->Geom.x0, pFib->Geom.y - pFib->Geom.y0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //draw circles at the fiber end position, and at the base position
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x, pFib->Geom.y, pFocal->potzpoz_tip_radius, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x0, pFib->Geom.y0, pFocal->patrol_radius_min_mm, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //print out the vertices of the main arm
  //print the starting one twice.
  fprintf(pFile, "\n");
  for(i=0;i<=pFib->Geom.num_vertex;i++)
  {
    int j = (i == pFib->Geom.num_vertex ? 0 : i);
    fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
            "vertex",
            pFib->Geom.pos_vertices_x[j], pFib->Geom.pos_vertices_y[j],
            0.0, 0.0,
            pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  }
  fprintf(pFile, "\n");
  
  return 0;
}
/////////////////////////////////////////////////
int OpSim_positioner_write_instantaneous_positioner_geometry_mupoz (FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib)
{
  int i;
  if ( pFile == NULL ||
       pFocal == NULL ||
       pFib  == NULL ) return 1;

  //draw a vector from the main rotation axis to the secondary rotation axis
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "vector",
          pFib->Geom.x0, pFib->Geom.y0,
          pFib->Geom.axis2_x - pFib->Geom.x0, pFib->Geom.axis2_y - pFib->Geom.y0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  //draw a vector from the secondary rotation axis to the fiber
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "vector",
          pFib->Geom.axis2_x, pFib->Geom.axis2_y,
          pFib->Geom.x - pFib->Geom.axis2_x , pFib->Geom.y - pFib->Geom.axis2_y,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //draw a circle at the fiber end position
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x, pFib->Geom.y, pFocal->mupoz_tip_radius, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  //and draw the circle defining the secondary rotation axis
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x0, pFib->Geom.y0, pFocal->mupoz_half_patrol_radius, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  //and draw the circle at the location of the secondary rotation axis that represents the base of the arm
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.axis2_x, pFib->Geom.axis2_y, pFocal->mupoz_half_arm_width, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //print out the vertices of the secondary rotating part
  //print the starting one twice.
  fprintf(pFile, "\n");
  for(i=0;i<=pFib->Geom.num_vertex;i++)
  {
    int j = (i == pFib->Geom.num_vertex ? 0 : i);
    fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
            "vertex",
            pFib->Geom.pos_vertices_x[j], pFib->Geom.pos_vertices_y[j],
            0.0, 0.0,
            pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
            BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  }
  fprintf(pFile, "\n");
  
  return 0;
}

int OpSim_positioner_write_instantaneous_positioner_geometry_spine (FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib)
{
  int i;
  if ( pFile == NULL ||
       pFocal == NULL ||
       pFib  == NULL ) return 1;

  //draw a vector from the main rotation axis to the fiber
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "vector",
          pFib->Geom.x0, pFib->Geom.y0,
          pFib->Geom.x - pFib->Geom.x0, pFib->Geom.y - pFib->Geom.y0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //draw a circle at the fiber end position
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x, pFib->Geom.y, pFocal->spine_radius, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  //draw a circle at the fiber start position
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x0, pFib->Geom.y0, pFocal->spine_radius, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //print out the vertices of the main spine
  //print the starting one twice.
  fprintf(pFile, "\n");
  for(i=0;i<=pFib->Geom.num_vertex;i++)
  {
    int j = (i == pFib->Geom.num_vertex ? 0 : i);
    fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
            "vertex",
            pFib->Geom.pos_vertices_x[j], pFib->Geom.pos_vertices_y[j],
            0.0, 0.0,
            pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
            BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));
  }
  fprintf(pFile, "\n");

  return 0;
}
int OpSim_positioner_write_instantaneous_positioner_geometry_starbug (FILE *pFile, focalplane_struct *pFocal, fiber_struct *pFib)
{
  if ( pFile == NULL ||
       pFocal == NULL ||
       pFib  == NULL ) return 1;

  //draw a vector from the base position to the fiber
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "vector",
          pFib->Geom.x0, pFib->Geom.y0,
          pFib->Geom.x - pFib->Geom.x0, pFib->Geom.y - pFib->Geom.y0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  //draw a circle at the fiber end position
  fprintf(pFile, "%-8s %8.3f %8.3f %8.3f %8.3f %5d %3s %2d 0x%08x %6s %7s\n",
          "circle", pFib->Geom.x, pFib->Geom.y, pFocal->starbug_radius, 0.0,
          pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib), pFib->assign_flag, pFib->status,
          BOOL2TF(pFib->is_empty), BOOL2TF(pFib->is_switchable));

  return 0;
}


//write out the locations of the fiber vertices for plotting purposes
int OpSim_positioner_write_instantaneous_positioners_geometry (const char* str_filename, focalplane_struct *pFocal)
{
  FILE * pFile = NULL;
  int i;
  if ( str_filename == NULL ||
       pFocal        == NULL ) return 1;

  //open up the output file
  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the instantaneous positioner geometry file, filename: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  //print a header
  fprintf(pFile, "#%-7s %8s %8s %8s %8s %5s %3s %2s %10s %6s %7s\n",
          "Shape", "x", "y", "par1", "par2", "id", "res", "AF", "StatusFlag", "Empty?", "Switch?");

  //shape can be point, vector, vertex or circle
  for(i=0;i<pFocal->num_fibers;i++)
  {
    
    //now print extra output depending on positioner
    switch ( pFocal->positioner_family )
    {
    case POSITIONER_FAMILY_POTZPOZ :
      if ( OpSim_positioner_write_instantaneous_positioner_geometry_potzpoz ((FILE*) pFile,
                                                            (focalplane_struct*) pFocal,
                                                            (fiber_struct*) &(pFocal->pFib[i]))) return 1;
      break;
    case POSITIONER_FAMILY_MUPOZ :
      if ( OpSim_positioner_write_instantaneous_positioner_geometry_mupoz ((FILE*) pFile,
                                                          (focalplane_struct*) pFocal,
                                                          (fiber_struct*) &(pFocal->pFib[i]))) return 1;
      break;
    case POSITIONER_FAMILY_ECHIDNA :
    case POSITIONER_FAMILY_AESOP :
      if ( OpSim_positioner_write_instantaneous_positioner_geometry_spine ((FILE*) pFile,
                                                            (focalplane_struct*) pFocal,
                                                            (fiber_struct*) &(pFocal->pFib[i]))) return 1;
      break;
    case POSITIONER_FAMILY_STARBUG :
      if ( OpSim_positioner_write_instantaneous_positioner_geometry_starbug ((FILE*) pFile,
                                                            (focalplane_struct*) pFocal,
                                                            (fiber_struct*) &(pFocal->pFib[i]))) return 1;
      break;
      break;
    default :
      //unknown positioner type
      return -1;
      break;
    }
  }
  
  if ( fclose(pFile) ) return 1;
  return 0;
}


int OpSim_positioner_write_instantaneous_positioners_geometry_plotfile (const char* str_filename, focalplane_struct *pFocal, survey_struct *pSurvey)
{
  FILE * pFile = NULL;
  if ( str_filename == NULL ||
       pFocal        == NULL ||
       pSurvey == NULL ) return 1;

  //open up the output file
  if ((pFile = (FILE*) fopen((const char*) str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the instantaneous positioner geometry plotfile, filename: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  if ( util_write_cd_line_to_plotfile (pFile, FIBER_ASSIGNMENT_SUBDIRECTORY, ""))  return 1;
  
  //assume that we will be running this from the FIBER_ASSIGNMENT_SUBDIRECTORY sub-directory
  fprintf(pFile, "reset\n\
set term wxt enhanced size 1400,1000\n\
\n\
field = \"00001\"\n\
tile  = 1\n\
\n\
ft = sprintf(\"f%%5s_t%%02d\", field, tile)\n\
infile           = sprintf(\"< gawk '$12~/TARGET/           '  %s_%%s.txt\", ft)\n\
infile_lo        = sprintf(\"< gawk '$12~/TARGET/ && $2 == 1'  %s_%%s.txt\", ft)\n\
infile_hi        = sprintf(\"< gawk '$12~/TARGET/ && $2 == 2'  %s_%%s.txt\", ft)\n\
infile_sw        = sprintf(\"< gawk '$12~/TARGET/ && $2~/^S$/' %s_%%s.txt\", ft)\n\
infile_sky_lo    = sprintf(\"< gawk '$12~/SKY/    && $2 == 1'  %s_%%s.txt\", ft)\n\
infile_sky_hi    = sprintf(\"< gawk '$12~/SKY/    && $2 == 2'  %s_%%s.txt\", ft)\n\
infile_sky_sw    = sprintf(\"< gawk '$12~/SKY/    && $2~/^S$/' %s_%%s.txt\", ft)\n\
infile_spare_lo  = sprintf(\"< gawk '$12~/SPARE/  && $2 == 1'  %s_%%s.txt\", ft)\n\
infile_spare_hi  = sprintf(\"< gawk '$12~/SPARE/  && $2 == 2'  %s_%%s.txt\", ft)\n\
infile_spare_sw  = sprintf(\"< gawk '$12~/SPARE/  && $2~/^S$/' %s_%%s.txt\", ft)\n",
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM,
          FIBER_ASSIGNMENT_STEM);
  
  fprintf(pFile, "infile_targets   = sprintf(\"%s_%%s.txt\", ft)\n\
infile_good_lo   = sprintf(\"< gawk '$5 == 1 && $15 <  %g' %s_%%s.txt\", ft)\n\
infile_good_hi   = sprintf(\"< gawk '$5 == 2 && $15 <  %g' %s_%%s.txt\", ft)\n\n",
          TARGET_DETAILS_STEM,
          MAX_EXPOSURE_TIME,
          TARGET_DETAILS_STEM,
          MAX_EXPOSURE_TIME,
          TARGET_DETAILS_STEM);
  
  fprintf(pFile, "geom_file_circle = sprintf(\"< gawk '/^circle/ ' %s_%%s.txt\", ft)\n\
geom_file_vector = sprintf(\"< gawk '/^vector/ || NF == 0' %s_%%s.txt\", ft)\n\
geom_file_vertex = sprintf(\"< gawk '/^vertex/ || NF == 0' %s_%%s.txt\", ft)\n\n",
          FIBER_POSITIONS_STEM,
          FIBER_POSITIONS_STEM,
          FIBER_POSITIONS_STEM);
  
  fprintf(pFile,"tile_file        = \"../%s/%s\"\n\
base_geom_lo     = \"< gawk '$4==1' ../%s/%s.txt\"\n\
base_geom_hi     = \"< gawk '$4==2' ../%s/%s.txt\"\n\
base_geom_sw     = \"< gawk '$4~/^S$/' ../%s/%s.txt\"\n\
\n\
rgb_hi = \"#0000bb\"\n\
rgb_lo = \"#dd4000\"\n\
rgb_sw = \"#dd40bb\"\n\
rgba_hi = \"#440000bb\"\n\
rgba_lo = \"#44dd4000\"\n\
rgba_sw = \"#44dd40bb\"\n\
set xlabel \"Offset x (mm)\"\n\
set ylabel \"Offset y (mm)\"\n\
unset colorbox\n\
set size square\n\
patrol_diam = %.6g*2.0\n\n",
          PLOTFILES_SUBDIRECTORY, TILE_VERTICES_FILENAME,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          pFocal->patrol_radius_max_mm);
  
  fprintf(pFile,"plot \\\n\
     base_geom_lo     using   2 :  3 : (patrol_diam) : (patrol_diam) with ellipses fs transparent solid 0.1 lc rgb rgb_lo lt 1 lw 0.5 t \"\",\\\n\
     base_geom_hi     using   2 :  3 : (patrol_diam) : (patrol_diam) with ellipses fs transparent solid 0.1 lc rgb rgb_hi lt 1 lw 0.5 t \"\",\\\n\
     base_geom_sw     using   2 :  3 : (patrol_diam) : (patrol_diam) with ellipses fs transparent solid 0.1 lc rgb rgb_sw lt 1 lw 0.5 t \"\",\\\n\
     tile_file        using   3 :  4                     with lines lt 1 lc 9 lw 0.7 t \"\",\\\n\
     infile_good_lo   using   9 : 10                     with points pt 7  ps 0.9 lc rgb rgba_lo t \"\",\\\n\
     infile_good_hi   using   9 : 10                     with points pt 5  ps 0.9 lc rgb rgba_hi t \"\",\\\n\
     infile           using   6 :  7                     with points pt 6  ps 0.7 lc -1 t \"\",\\\n\
     infile           using  19 : 20                     with points pt 1  ps 0.7 lc -1 t \"\",\\\n\
     infile_sky_lo    using   6 :  7                     with points pt 8  ps 1.2 lc  2 t \"\",\\\n\
     infile_sky_hi    using   6 :  7                     with points pt 10 ps 1.2 lc  2 t \"\",\\\n\
     infile_sky_sw    using   6 :  7                     with points pt 12 ps 1.2 lc  2 t \"\",\\\n\
     infile_spare_lo  using   6 :  7                     with points pt 8  ps 1.6 lc  5 t \"\",\\\n\
     infile_spare_hi  using   6 :  7                     with points pt 10 ps 1.6 lc  5 t \"\",\\\n\
     infile_spare_sw  using   6 :  7                     with points pt 12 ps 1.6 lc  5 t \"\",\\\n\
     geom_file_circle using   2 :  3 : ($4*2.) : ($4*2.) with ellipses lw 0.5 lc 7 t \"\",\\\n\
     geom_file_vector using ($7==1?$2:1/0) : 3 : 4 : 5   with vectors nohead lw 1.0 lc 1 t \"\",\\\n\
     geom_file_vector using ($7==2?$2:1/0) : 3 : 4 : 5   with vectors nohead lw 1.0 lc 3 t \"\",\\\n\
     geom_file_vertex using   2 :  3                     with lines    lw 0.5 lc 7 t \"\",\\\n\
     base_geom_lo     using   2 :  3 : (sprintf(\"lo\\n%%d\",$1)) with labels font \",6\" tc lt 1 t \"\",\\\n\
     base_geom_hi     using   2 :  3 : (sprintf(\"hi\\n%%d\",$1)) with labels font \",6\" tc lt 3 t \"\",\\\n\
     base_geom_sw     using   2 :  3 : (sprintf(\"sw\\n%%d\",$1)) with labels font \",6\" tc lt 4 t \"\"\n\n");

  if ( fclose((FILE*) pFile) ) return 1;
  return 0;
}
////////////////////////////


//return TRUE if high res, FALSE if not
BOOL OpSim_positioner_check_if_high_res (int i, int j, int hilo_pattern, int hilo_ratio )
{
  //default to LOW RES
  const int max_rows = 1000000;
  BOOL hi_res_flag = FALSE;

  int ii =     ii = i +  max_rows;  //could be better
  int jj =     jj = j +  max_rows;

  //test if this is a high res fiber
  //it depends on the pattern of course
  //regular pattern where hilo_ratio is linear ratio between high and low res
  //the "one in N" patterns will also work nicely with this method when N is a square number
  switch ( hilo_pattern )
  {
    //////////////////////////////////////////////
  case HIGH_LOW_PATTERN_REGULAR : 
    if ( (abs(j%(2*hilo_ratio)) == 0 && abs(i%hilo_ratio) == 0) ||
         (abs(j%(2*hilo_ratio)) == hilo_ratio &&
          ((i+max_rows*hilo_ratio)%hilo_ratio)  == hilo_ratio/2 ))
    {
      hi_res_flag = TRUE; 
    }
    break;
    //////////////////////////////////////////////
          
    //////////////////////////////////////////////
    //regular pattern where hilo_ratio is ratio of high res fibers to total fibers
  case HIGH_LOW_PATTERN_ONE_IN_N :
    //here the hilo_ratio becomes fraction of all fibers that are high res
    // so just choose every "hilo_ratio"th fiber to be hi res
    if ( hilo_ratio == 6)
    {
      //pattern is 
      //  0 x x 0 x x 0
      // x x x x x x x 
      //  x 0 x x 0 x x 
      // x x x x x x x 
      //  0 x x 0 x x 0
      
      if ( ((jj % 4 == 0) && (ii % 3 == 2)) ||
           ((jj % 4 == 1) && (ii % 3 == 0)) )
      {
        hi_res_flag = TRUE; 
      }
    }
    else if ( hilo_ratio == 3)  //regular layout - 1 in three rows is hi res
    {
      //pattern is 
      // 0     0     0     0     0     0     0     0     0
      //    x     x     x     x     x     x     x     x     x
      // x     x     x     x     x     x     x     x     x
      //    0     0     0     0     0     0     0     0     0
      // x     x     x     x     x     x     x     x     x
      //    x     x     x     x     x     x     x     x     x
      // 0     0     0     0     0     0     0     0     0
      if (jj % 3 == 0)
      {
        hi_res_flag = TRUE; 
      }
    }
    else if ( hilo_ratio == 2)  //regular layout - every other row is hi res
    {
      //pattern is 
      // 0     0     0     0     0     0     0     0     0
      //    x     x     x     x     x     x     x     x     x
      // 0     0     0     0     0     0     0     0     0
      //    x     x     x     x     x     x     x     x     x
      // 0     0     0     0     0     0     0     0     0
      //    x     x     x     x     x     x     x     x     x
      if (jj % 2 == 0)
      {
        hi_res_flag = TRUE; 
      }
    }
    else
    {
      fprintf(stderr, "%s I cannot handle this particular Hi-Lo res pattern (%d %d)\n",
              ERROR_PREFIX, hilo_pattern, hilo_ratio);
      return  (BOOL) 1;
    }
    break;
    //////////////////////////////////////////////
    
    //////////////////////////////////////////////
  case HIGH_LOW_PATTERN_RANDOM :
    if (drand48() * (float) (hilo_ratio+1.0) < (float) 1.0 )  hi_res_flag = TRUE; 
    break;
    //////////////////////////////////////////////
    //////////////////////////////////////////////
  case HIGH_LOW_PATTERN_ALL_LORES :
    hi_res_flag = FALSE; 
    break;
    //////////////////////////////////////////////
    //////////////////////////////////////////////
  case HIGH_LOW_PATTERN_ALL_HIRES :
    hi_res_flag = TRUE; 
    break;
    //////////////////////////////////////////////
    //////////////////////////////////////////////
  default :
    //
      fprintf(stderr, "%s I cannot handle this particular Hi-Lo res pattern (%d %d)\n",
              ERROR_PREFIX, hilo_pattern, hilo_ratio);
      return (BOOL) 1;
      break;
    //////////////////////////////////////////////
  }
  return (BOOL) hi_res_flag;
}



/////////////////////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_setup_fibers_geometry (focalplane_struct *pFocal, telescope_struct *pTele)
{
  //make our own positioner layout
  //base this on previous work in ~/my_4most/fiber_pos/run_create_fibers.csh
  //put results into a positioner_struct
  //write a copy out to file even if we are running internally
  
  //we will centre the fibers about 0,0
  //and make sure that we actually have have a (low-res) fiber at x,y = 0,0 
  
  //let fiber-fiber spacing = sp
  //then:
  //  spacing between rows   is sp/2
  //  spacing between columns is sp*sqrt(3)
  //  every 'n'th fiber is a high res one, where 'n' is given by pIpar->POSITIONER_HIGH_LO_RATIO
  
  //we do not yet write any info about the fiber positions to the Tile struct.
  int i,j,ncols, nrows;
  double x,y, dx, dy;
  //      int res;
  double sp_lo   = pFocal->positioner_spacing;
  int hilo_ratio = pFocal->positioner_hilo_ratio;
  int data_length;
  
  if ( pFocal == NULL ||
       pTele == NULL ) return 1;
  if (pFocal->positioner_hilo_pattern == HIGH_LOW_PATTERN_ONE_IN_N &&  
      (hilo_ratio == 4 || hilo_ratio == 9 || hilo_ratio == 16 || hilo_ratio == 25))
  {
    //then we are dealing with a regular pattern
    pFocal->positioner_hilo_pattern = HIGH_LOW_PATTERN_REGULAR;
    fprintf(stdout, "%s Your choice of a ONE_IN_N pattern and a HIGH_LOW_RATIO of %d reduces to ", COMMENT_PREFIX, hilo_ratio);
    hilo_ratio = (int) floor(sqrt((double) hilo_ratio));
    fprintf(stdout, "a REGULAR pattern and a HIGH_LOW_RATIO of %d\n", hilo_ratio);
  }
  
  //let's tell the user what they should have chosen.
  //we want to calculate the exact fiber spacing that would mean that the outer perimeter of fibers lies 
  //on a hexagon having area FOV_AREA.
  //hexagon side length Lhex = sqrt(FOV_AREA/(1.5*sqrt(3)))
  //the number of fibers goes in steps as each progressively larger hexagon is added
  //N_fib = 1 + sum_i{6i}   - where i is number of fibers along a side (including 1 vertex per side)
  //ideal spacing is thus Lhex / N_fib
  //print out the two fiber spacings either side of the given value that give goood answers  
  if ( pFocal->layout_shape_code == GEOM_SHAPE_HEXAGON )
  {
    float guess_nperside  = pTele->fov_radius_mm/((float)pFocal->positioner_spacing);
    int npos = 1;
    int nperside = 0;
    float spacing = pTele->fov_radius_mm/((float)guess_nperside);
    fprintf(stdout, "%s I will now suggest an optimal fiber spacing using your input parameters\n", COMMENT_PREFIX);
    
    npos = 1;
    nperside = 0;
    while ( (float) (nperside +1) < guess_nperside )
    {
      nperside++;
      npos += 6 * nperside;
    }
    
    fprintf(stdout, "%s   For the specified POSITIONER.SPACING_LOW_RES= %9.5f mm we will  have %3d positioners per side = %6d positioners total\n",
            COMMENT_PREFIX, pFocal->positioner_spacing, nperside, npos);
    npos = 1;
    nperside = 0;
    while ( (float) (nperside+1) < guess_nperside )
    {
      nperside++;
      npos += 6 * nperside;
      spacing = pTele->fov_radius_mm/((float)nperside);
    }
    fprintf(stdout, "%s But, for a possible POSITIONER.SPACING_LOW_RES= %9.5f mm we would have %3d positioners per side = %6d positioners total\n",
            COMMENT_PREFIX, spacing, nperside, npos);
    nperside++;
    npos += 6 * nperside;
    spacing = pTele->fov_radius_mm/((float)nperside);
    fprintf(stdout, "%s and, for a possible POSITIONER.SPACING_LOW_RES= %9.5f mm we would have %3d positioners per side = %6d positioners total\n",
            COMMENT_PREFIX, spacing, nperside, npos);

  }

  
  //use a standard hexagonal packing of fibers whatever the outer perimeter shape
  dy = sp_lo*0.5;
  dx = sp_lo*SQRT_3;
  nrows=(int)(1.0 + pTele->fov_radius_mm*2.0/dy);
  ncols=(int)(1.0 + pTele->fov_radius_mm*2.0/dx);

  //make some space - guess how big this will be
  data_length = MAX(FIBERS_PER_CHUNK,SQR(MAX(nrows,ncols)));
  if ((pFocal->pFib = (fiber_struct*) malloc(data_length * sizeof(fiber_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  for(j=(int)((nrows+1)*-0.5);j<=(int)((nrows+1)*0.5);j++)
  {
    //    jj = j +  nrows*10;
    y=((j+0.0)*dy);
    for(i=(int)((ncols+1)*-0.5);i<=(int)((ncols+1)*0.5);i++)
    {
      int in_FoV_flag;
      //      ii = i +  ncols*10;
      //make sure to have half-integer offsets in x between cols
      x=((i+(j%2==0?0:0.5))*dx);

      switch ( pFocal->layout_shape_code )
      {
      case GEOM_SHAPE_HEXAGON : 
        in_FoV_flag = (int) geom_test_if_in_hexagon(pTele->fov_radius_mm,x,y);
        break;
      case GEOM_SHAPE_CIRCLE :
        in_FoV_flag = (int) (SQR(x) + SQR(y) <= SQR(pTele->fov_radius_mm) ? 1 : 0); 
        break;
      default :
        fprintf(stderr, "%s I do not know how to set up this positioner layout shape: %s \n",
              ERROR_PREFIX, pFocal->str_layout_shape);
      return 1;
        break;
      }
      
      if ( in_FoV_flag )
      {
        fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[pFocal->num_fibers]);
        BOOL hi_res_flag = (BOOL) OpSim_positioner_check_if_high_res ((int) i, (int) j,
                                                                      (int) pFocal->positioner_hilo_pattern,
                                                                      (int) hilo_ratio);
        if ( hi_res_flag == TRUE )
        {
          pFib->res = (utiny) RESOLUTION_CODE_HIGH;
          pFocal->num_fibers_hi++;
          pFocal->num_fibers_really_hi++;
        }
        else
        {
          pFib->res = (utiny) RESOLUTION_CODE_LOW;
          pFocal->num_fibers_lo++;
          pFocal->num_fibers_really_lo++;
        }
        
        pFib->Geom.x0 = x;
        pFib->Geom.y0 = y;
        pFib->Geom.x = pFib->Geom.x0;
        pFib->Geom.y = pFib->Geom.y0;
        pFib->id = pFocal->num_fibers+1 ;
        pFib->index = pFocal->num_fibers ;
        pFib->is_switchable = FALSE;
        pFib->is_empty = FALSE;
        
        pFib->status = FIBER_STATUS_FLAG_NOMINAL;
        pFib->edge_flag = -1;
        pFib->sector = -1;
        
        pFocal->num_fibers++;
        
        if ( pFocal->num_fibers >= data_length )
        {
          //allocate some more space if necessary - this shouldn't happen
          data_length += FIBERS_PER_CHUNK;
          if ((pFocal->pFib = (fiber_struct*) realloc(pFocal->pFib, data_length * sizeof(fiber_struct))) == NULL )
          {
            fprintf(stderr, "%s Error re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
          }
        }
      }        
    }
  }

  //tidy up spare space
  if (pFocal->num_fibers < data_length)
  {
    if ((pFocal->pFib = (fiber_struct*) realloc(pFocal->pFib, pFocal->num_fibers * sizeof(fiber_struct))) == NULL )
    {
      fprintf(stderr, "%s Error re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  return 0;
} 
/////////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_read_fibers_geometry_from_file (focalplane_struct *pFocal, const char *str_filename)
{
  //read positioner layout from file
  //format should be approx the same as output(which is required by field_mbr)
  //but we will make sure that the precise column widths are ok.
  //we will read in the positioner layout, and then copy to the FIBERS_GEOMETRY_FILENAME file
  //      fprintf(stderr, "%s I do not understand how to read a position from file yet\n", ERROR_PREFIX);
  //      return 1;
  FILE* pInput_Fibers_file = NULL;
  char buffer[STR_MAX];
  int i,j;
  int n_with_sectors = 0;
  int n_with_edge_flags = 0;

  if ( pFocal == NULL || str_filename == NULL ) return 1;

  if ( strlen(str_filename) <= 0 ) return 1;

  //open up the file
  if ((pInput_Fibers_file = (FILE*) fopen(str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the fibers geometry file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  pFocal->num_fibers = 0;
  //count the number of entries in the file
  while (fgets(buffer,STR_MAX,pInput_Fibers_file)) pFocal->num_fibers++; // && pFocal->num_fibers++ < MAX_FIBERS);
  rewind(pInput_Fibers_file);

  if ( pFocal->num_fibers <= 0 )
  {
    fprintf(stderr, "%s I had problems understanding (nfibers=%d) the fibers geometry file: %s\n",
            ERROR_PREFIX, pFocal->num_fibers, str_filename);
    return 1;
  }
  
  //make some space
  if ((pFocal->pFib = (fiber_struct*) malloc(pFocal->num_fibers * sizeof(fiber_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);fflush(stderr);
    return 1;
  }
  //Now, actually read the file
  i = 0;
  while (fgets(buffer,STR_MAX,pInput_Fibers_file) && i < pFocal->num_fibers )
  {
    int nitems = 0;
    int sector = -1;
    int edge_flag = -1;
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    char res_buffer[STR_MAX];

    if ( buffer[0] == '#' ) continue;
    pFib->index = i;
    res_buffer[0] = '\0';
    nitems = sscanf(buffer,"%d %f %f %s %d %d",
                    (int*)    &(pFib->id),
                    (float*)  &(pFib->Geom.x0),
                    (float*)  &(pFib->Geom.y0),
                    (char*)   res_buffer,
                    (int*)    &sector,
                    (int*)    &edge_flag);
    if ( nitems == 0 ) continue;
    else if ( nitems < 4 )
    {
      fprintf(stderr, "%s Error reading line from fibers geometry file (nitems=%d): ->%s<-\n",
              ERROR_PREFIX, nitems, buffer);fflush(stderr);
      return 1;
    }
    pFib->Geom.x = pFib->Geom.x0;
    pFib->Geom.y = pFib->Geom.y0;
    
    for (j=0;j<(int)strlen(res_buffer);j++) res_buffer[j] = toupper(res_buffer[j]);
    pFib->res = (utiny) RESOLUTION_CODE_NULL;
    pFib->is_switchable = (BOOL) FALSE;
    pFib->is_empty = (BOOL) FALSE;
    if (strcmp(res_buffer, "HIGH") == 0 ||
        strcmp(res_buffer, "HI"  ) == 0 ||
        strcmp(res_buffer, "H"   ) == 0 ||
        strcmp(res_buffer, "2"   ) == 0 )
    {
      pFib->res = (utiny) RESOLUTION_CODE_HIGH;
      pFocal->num_fibers_hi++;
      pFocal->num_fibers_really_hi++;
    }
    else if (strcmp(res_buffer, "LOW") == 0 ||
             strcmp(res_buffer, "LO" ) == 0 ||
             strcmp(res_buffer, "L"  ) == 0 ||
             strcmp(res_buffer, "1"  ) == 0 )
    {
      pFib->res = (utiny) RESOLUTION_CODE_LOW;
      pFocal->num_fibers_lo++;
      pFocal->num_fibers_really_lo++;
    }
    else if (strcmp(res_buffer, "DUAL") == 0 ||
             strcmp(res_buffer, "BOTH") == 0 ||
             strcmp(res_buffer, "D"   ) == 0 ||
             strcmp(res_buffer, "3"   ) == 0 )
    {
      pFib->res = (utiny) RESOLUTION_CODE_DUAL;
      pFocal->num_fibers_dual++;
    }
    else if (strcmp(res_buffer, "SWITCHABLE") == 0 ||
             strcmp(res_buffer, "S"    ) == 0 ||
             strcmp(res_buffer, "4"    ) == 0 )
    {
      pFocal->num_fibers_switchable++;
      pFib->is_switchable = (BOOL) TRUE;
    }
    else if (strcmp(res_buffer, "EMPTY") == 0 ||
             strcmp(res_buffer, "E"    ) == 0 ||
             strcmp(res_buffer, "5"    ) == 0 )
    {
      pFocal->num_fibers_empty++;
      pFib->is_empty = (BOOL) TRUE;
    }
    else
    {
      fprintf(stderr, "%s Odd resolution/status code >%s< for fiber %d read from file %s - ignoring\n",
              WARNING_PREFIX, res_buffer, i, str_filename);
      continue;
    }
    //see if we have been supplied with a sector number as well
    //if the supplied sector number is negative, then treat it as blank.
    if ( nitems >= 5 )  pFib->sector = sector;
    else                pFib->sector = -1;
    if ( pFib->sector >= 0 )  n_with_sectors++;


    //see if we have been supplied with an edge_flag as well
    if ( nitems >= 6 )
    {
      pFib->edge_flag = (int) edge_flag;
      n_with_edge_flags++;
    }
    else pFib->edge_flag = -1;

     //everything seems ok so increment the counter and continue.
    i ++;
  }

  if ( i <= 0 )
  {
    fprintf(stderr, "%s Error reading fiber positions from file %s : we found %d valid fibers!\n",
            ERROR_PREFIX, str_filename, i);
    return 1;
  }

  //check that were supplied a sensible number of entries
  if ( n_with_sectors == i)
  {
    pFocal->flag_sectors_supplied_in_input_file = TRUE;
  }
  else if ( n_with_sectors == 0)
  {
    pFocal->flag_sectors_supplied_in_input_file = FALSE;
  }
  else
  {
    fprintf(stderr, "%s Only some (%d/%d) of the fibers in file %s were supplied with sector numbers!\n",
            ERROR_PREFIX, n_with_sectors, i, str_filename);
    return 1;
  }

  //check that were supplied a sensible number of entries
  if ( n_with_edge_flags == i)
  {
    pFocal->flag_edge_flags_supplied_in_input_file = TRUE;
  }
  else if ( n_with_edge_flags == 0)
  {
    pFocal->flag_edge_flags_supplied_in_input_file = FALSE;
  }
  else
  {
    fprintf(stderr, "%s Only some (%d/%d) of the fibers in file %s were supplied with edge_flags!\n",
            ERROR_PREFIX, n_with_edge_flags, i, str_filename);
    return 1;
  }

  
  if (i < pFocal->num_fibers)
  {
    pFocal->num_fibers = i;
    if ((pFocal->pFib = (fiber_struct*) realloc(pFocal->pFib, pFocal->num_fibers * sizeof(fiber_struct))) == NULL )
    {
      fprintf(stderr, "%s Error re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  fclose(pInput_Fibers_file);
  pInput_Fibers_file = NULL;


  //Tweak the positions of the fibers 
  //shink or expand the spacing of the fibers on the focal plane 
  for(i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    pFib->Geom.x0 *= (float) pFocal->coordinate_tweak_factor;
    pFib->Geom.y0 *= (float) pFocal->coordinate_tweak_factor;
  }
  
  
  fprintf(stdout, "%s I read  %5d fiber positions from this file: %s\n",
          COMMENT_PREFIX, pFocal->num_fibers, str_filename);
  fprintf(stdout, "%s I read  %4d hi-res, %4d lo-res, %4d dual-res, %4d switchable, and %4d empty positioners\n",
          COMMENT_PREFIX,
          pFocal->num_fibers_really_hi, pFocal->num_fibers_really_lo, pFocal->num_fibers_dual,
          pFocal->num_fibers_switchable, pFocal->num_fibers_empty);
  fprintf(stdout, "%s The positioners were suppied with%s sector numbers, and with%s edge_flags\n",
          COMMENT_PREFIX,
          (pFocal->flag_sectors_supplied_in_input_file?"":"out"),
          (pFocal->flag_edge_flags_supplied_in_input_file?"":"out"));
  return 0;
}
/////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
    


/////////////////////////////////////////////////////////////////////////////////////////////
//write the fiber layout out to the standard file, with the standard format.
int OpSim_positioner_write_fibers_geometry (focalplane_struct *pFocal, survey_struct *pSurvey, const char *str_filename)
{
  FILE* pFile = NULL;
  int i;
  if ( pFocal == NULL || pSurvey == NULL ) return 1;
  //open up the file
  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the fibers geometry file: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }

  //print a full header
  {
    time_t rawtime;
    struct tm *timeinfo;
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, "#This is the fibers geometry file for simulation: \"%s\"\n\
#This file was first written at: %s\
#Original filename: %s\n\
#This file contains one entry per Fiber/Positioner, and gives the coordinates and details of each fiber\n\
#Description of columns:\n\
#Index  Name        Format    Description\n\
#-----  ----------- --------- -------------------------------------------------------------------------------- \n\
#1      id          integer   Fiber identification number\n\
#2      x0(mm)      float     Offset of the fiber base position from the FoV centre in the x-direction (mm)\n\
#3      y0(mm)      float     Offset of the fiber base position from the FoV centre in the y-direction (mm)\n\
#4      res         character Resolution code of fiber (1=low-res;2=high-res;D=dual-res;S=switchable;E=empty)\n\
#5      sector      integer   ID of the sky fiber allocation sector that this fiber lies within\n\
#6      EdgeFlag    integer   Flag set if the fiber is at edge of field (0=middle;1=border;2=edge;3=corner)\n\
#-----  ----------- --------- -------------------------------------------------------------------------------- \n",
            pSurvey->sim_code_name,
            asctime(timeinfo),
            str_filename);
  }

  fprintf(pFile, "#%-3s %11s %11s %3s %6s %8s\n", "id", "x0(mm)", "y0(mm)", "res", "sector", "EdgeFlag");
  for(i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    fprintf(pFile, "%4d %11.4f %11.4f %3s %6d %8d\n",
            pFib->id, pFib->Geom.x0, pFib->Geom.y0,
            (char*) OpSim_positioner_fiber_status_string ((fiber_struct*) pFib),
            pFocal->Sector[pFib->sector].id, pFib->edge_flag);
  }
  if ( pFile ) fclose ( pFile) ;
  pFile = NULL;
  
  fprintf(stdout, "%s I wrote %5d fiber positions (Nlo-res= %d; Nhi-res= %d; Ndual-res= %d; Nswitchable= %d; Nempty= %d) to   this file: %s\n",
          COMMENT_PREFIX, pFocal->num_fibers,
          pFocal->num_fibers_lo,
          pFocal->num_fibers_hi,
          pFocal->num_fibers_dual,
          pFocal->num_fibers_switchable,
          pFocal->num_fibers_empty,
          str_filename);
  fflush(stdout);
  return 0;
}
/////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////
int OpSim_positioner_plot_fibers_geometry   (focalplane_struct *pFocal, telescope_struct *pTele, survey_struct* pSurvey, const char *str_filename)
{
  
  //make a standard plot of the fiber layout
  FILE* pFibers_plotfile = NULL;
  //  char * strFibers_plotfilename = "fibers_geometry.plot";
  char str_gnuplot_code_name[STR_MAX];
  char str_gnuplot_pattern_name[STR_MAX];
  int i;
  //  char strSystem[STR_MAX];
  char str_cwd[STR_MAX];

  
  //open up the file
  if ((pFibers_plotfile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the fibers geometry plotfile: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }
      
  //remove underscores from the tile code name
  (void) sstrncpy(str_gnuplot_code_name,pFocal->code_name , STR_MAX);
  for(i=0;i<=(int) strlen(str_gnuplot_code_name);i++)
  {
    if(str_gnuplot_code_name[i] == '_' ) str_gnuplot_code_name[i] = ' ';
  }
  //remove underscores from the pattern name
  (void)  sstrncpy(str_gnuplot_pattern_name,pFocal->str_positioner_hilo_pattern, STR_MAX);
  for(i=0;i<=(int) strlen(str_gnuplot_pattern_name);i++)
  {
    if(str_gnuplot_pattern_name[i] == '_' ) str_gnuplot_pattern_name[i] = ' ';
  }

  //get the working directory path
  if ( getcwd((char *) str_cwd, (size_t) STR_MAX) == NULL )
  {
    fprintf(stderr, "%s Could not get CWD at at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    (void) fflush(stderr); 
    return 1;
  }

  //assume that will be plotting from within the PLOTFILES_SUBDIRECTORY directory
  fprintf(pFibers_plotfile, "reset\n\
cd \"%s/%s\"\n\
vertex_file   = \"< egrep -C1 Normal %s\"\n\
evertex_file  = \"< egrep -C1 Extended %s\"\n\
fibers_file   = \"%s.txt\"\n\
fibers_lo     = \"< gawk '$4~/^1$|^L$|^l$/' %s.txt\"\n\
fibers_hi     = \"< gawk '$4~/^2$|^H$|^h$/' %s.txt\"\n\
fibers_dual   = \"< gawk '$4~/^D$|^d$/' %s.txt\"\n\
fibers_empty  = \"< gawk '$4~/^E$|^e$/' %s.txt\"\n\
fibers_switch = \"< gawk '$4~/^S$|^s$/' %s.txt\"\n\
fibers_lo_any = \"< gawk '$4~/^1$|^L$|^l$|^D$|^d$^S$|^s$/' %s.txt\"\n\
fibers_hi_any = \"< gawk '$4~/^2$|^H$|^h$|^D$|^d$^S$|^s$/' %s.txt\"\n\
null_file     = \"< echo 0.0 0.0\" \n\
sectors_file  = \"< gawk '$1!~/^#/{sec=$5; n[sec]++; x[sec]+=$2; y[sec]+=$3; if($4==1){n_lo[sec]++}; if($4==2){n_hi[sec]++};} END {for (sec in n) {n_lo[sec]=0+n_lo[sec];n_hi[sec]=0+n_hi[sec]; print sec,x[sec]/n[sec],y[sec]/n[sec],n[sec],n_lo[sec],n_hi[sec]}} ' %s.txt \"\n\
\n\
set terminal pdfcairo enhanced colour font \"Times,12\" size 21.0cm,29.7cm\n\
set out \"%s.pdf\"\n",
          str_cwd,PLOTFILES_SUBDIRECTORY,
          TILE_VERTICES_FILENAME,
          TILE_VERTICES_FILENAME,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM,
          FIBERS_GEOMETRY_STEM);
  
  fprintf(pFibers_plotfile, "\n\
plate_scale = %g\n\
arcmin_to_mm = 60.*plate_scale\n\
fov_radius = %g\n\
hsize = fov_radius * 1.1\n\
dpatrol = 2.0*%g\n\
a2m(a) = (a*arcmin_to_mm)\n\
\n\
colour_lo      = \"#dd4400\"\n\
colour_hi      = \"#0000bb\"\n\
colour_tint_lo = \"#dd44bb\"\n\
colour_tint_hi = \"#dd44bb\"\n\
colour_dual    = \"#448800\"\n\
colour_empty   = \"#aaaaaa\"\n\
colour_switch  = \"#aa00aa\"\n\
colour_all     = \"#333333\"\n\
key_lo        = \"%d Low Res Fibers\"\n\
key_hi        = \"%d High Res Fibers\"\n\
key_dual      = \"%d Dual Res Fibers\"\n\
key_switch    = \"%d Switchable Fibers\"\n\
key_empty     = \"%d Empty Fibers\"\n\
pt_all        = 7\n\
pt_lo         = 7\n\
pt_hi         = 5\n\
pt_dual       = 3\n\
pt_switch     = 9\n\
pt_empty      = 6\n\
\n\
set size square\n\
set lmargin 10\n\
set rmargin 10\n\
set key samplen 2\n\
set xlabel \"Offset (mm)\"\n\
set ylabel \"Offset (mm)\" offset 1,0\n\
set y2label \"Offset (arcmin)\" offset -1,0\n\
set x2label \"Offset (arcmin)\" offset 0,-0.5\n\
set mytics 5\n\
set mxtics 5\n\
set xtics nomirror\n\
set ytics nomirror\n\
set y2tics (\"-90\" a2m(-90.), \"-75\" a2m(-75.), \"-60\" a2m(-60.), \"-45\" a2m(-45.), \"-30\" a2m(-30.), \"-15\" a2m(-15.), \"0\" a2m(0.), \"+15\" a2m(15.), \"+30\" a2m(30.), \"+45\" a2m(45.), \"+60\" a2m(60.), \"+75\" a2m(75.), \"+90\" a2m(90.))\n\
set x2tics (\"-90\" a2m(-90.), \"-75\" a2m(-75.), \"-60\" a2m(-60.), \"-45\" a2m(-45.), \"-30\" a2m(-30.), \"-15\" a2m(-15.), \"0\" a2m(0.), \"+15\" a2m(15.), \"+30\" a2m(30.), \"+45\" a2m(45.), \"+60\" a2m(60.), \"+75\" a2m(75.), \"+90\" a2m(90.))\n\
set format x \"%%+g\"\n\
set format y \"%%+g\"\n\
set xrange [-hsize:hsize]\n\
set yrange [-hsize:hsize]\n\
set title \"{/*1.3 Telescope=%s Instrument=4MOST Positioner=\\\"%s\\\" FOV=%gdeg^2}\"\n\
set label 1 \"Plate scale   = %.4f mm/arcsec\" at graph 0.01,0.900\n\
set label 2 \"Patrol radius = %6.3fmm (=%.1farcsec)\" at graph 0.01,0.925\n\
set label 3 \"Fiber pitch   = %6.3fmm (=%.1farcsec)\" at graph 0.01,0.950\n\
set label 100 \"\" at graph 0.01,0.02 font \",16\"\n\
set arrow 1 from 0,-hsize to 0,hsize nohead lt 2 lc -1 lw 0.5\n\
set arrow 2 from -hsize,0 to hsize,0 nohead lt 2 lc -1 lw 0.5\n\
%s\n\n",
          pFocal->plate_scale,
          pTele->fov_radius_mm,
          pFocal->patrol_radius_max_mm,
          pFocal->num_fibers_lo,
          pFocal->num_fibers_hi,
          pFocal->num_fibers_dual,
          pFocal->num_fibers_switchable,
          pFocal->num_fibers_empty,
          pTele->code_name, str_gnuplot_code_name, pTele->fov_area_deg,
          pFocal->plate_scale,
          pFocal->patrol_radius_max_mm, pFocal->patrol_radius_max_mm * pFocal->invplate_scale,
          pFocal->positioner_spacing,   pFocal->positioner_spacing   * pFocal->invplate_scale,
          OPSIM_BRANDING_PLOTSTRING_GRAPH);

  if ( pFocal->layout_from_file == FALSE )
  {
    fprintf(pFibers_plotfile, "\n\
set key title \"Hi/Lo Res Fibre Pattern: \\\"%s\\\" (N=%d)\" samplen 2\n\
set label 4 \"Layout from recipe\" at graph 0.01,0.975 noenhanced \n",
            str_gnuplot_pattern_name,
            pFocal->positioner_hilo_ratio);
  }
  else
  {
    fprintf(pFibers_plotfile, "\n\
set key title \"Hi/Lo Res Fibre Pattern: from file\" samplen 2\n\
set label 4 \"Layout from file: %s\" at graph 0.01,0.975 noenhanced\n",
            pFocal->positioner_layout_filename);

  }
    

  
  {
    char *pstr_2tics = "";
    if ( pTele->fov_radius_deg <= 1.5 )
    {
      pstr_2tics = (char*) "(\"-90\" a2m(-90.), \"-75\" a2m(-75.), \"-60\" a2m(-60.), \"-45\" a2m(-45.), \"-30\" a2m(-30.), \"-15\" a2m(-15.), \"0\" a2m(0.), \"+15\" a2m(15.), \"+30\" a2m(30.), \"+45\" a2m(45.), \"+60\" a2m(60.), \"+75\" a2m(75.), \"+90\" a2m(90.))";
    }
    else if ( pTele->fov_radius_deg <= 3.0 )
    {
      pstr_2tics = (char*) "(\"-180\" a2m(-180.), \"-150\" a2m(-150.), \"-120\" a2m(-120.), \"-90\" a2m(-90.), \"-60\" a2m(-60.), \"-30\" a2m(-30.), \"0\" a2m(0.), \"+30\" a2m(30.), \"+60\" a2m(60.), \"+90\" a2m(90.), \"120\" a2m(120.), \"+150\" a2m(150.), \"+180\" a2m(180.))"; 
    }
    else
    {
      pstr_2tics = (char*) "(\"-360\" a2m(-360.), \"-300\" a2m(-300.), \"-240\" a2m(-240.), \"-180\" a2m(-180.), \"-120\" a2m(-120.), \"-60\" a2m(-60.), \"0\" a2m(0.), \"+60\" a2m(60.), \"+120\" a2m(120.), \"+180\" a2m(180.), \"240\" a2m(240.), \"+300\" a2m(300.), \"+360\" a2m(360.))"; 
    }
    fprintf(pFibers_plotfile, "set y2tics %s\n\
set x2tics %s\n\n", pstr_2tics, pstr_2tics);
  }
  
  //a basic plot showing fiber base positions
  fprintf(pFibers_plotfile, "set label 100 \"Fiber base positions\"\n\
plot  \\\n\
     null_file     using 1 : 2 : (fov_radius*2.0) : (fov_radius*2.0) with ellipses lt 1 lc 9 t \"\",\\\n\
     vertex_file   using 3 : 4 with lines lt 1 lc -1 t \"\",\\\n\
     evertex_file  using 3 : 4 with lines lt 1 lc  9 t \"\",\\\n\
     fibers_lo     using 2 : 3 with points pt pt_lo     ps 0.4 lc rgb colour_lo     t key_lo,\\\n\
     fibers_hi     using 2 : 3 with points pt pt_hi     ps 0.4 lc rgb colour_hi     t key_hi,\\\n\
     fibers_dual   using 2 : 3 with points pt pt_dual   ps 0.4 lc rgb colour_dual   t key_dual,\\\n\
     fibers_switch using 2 : 3 with points pt pt_switch ps 0.4 lc rgb colour_switch t key_switch,\\\n\
     fibers_empty  using 2 : 3 with points pt pt_empty  ps 0.4 lc rgb colour_empty  t key_empty\n\n");

  //a plot showing pure hi-res fiber coverage of the focal plane
  if ( pFocal->num_fibers_really_hi > 0 )
  {
    fprintf(pFibers_plotfile, "set label 100 \"High-res fiber coverage\"\n\
plot \\\n\
     null_file     using 1 : 2 : (fov_radius*2.0) : (fov_radius*2.0) with ellipses lt 1 lc 9 t \"\",\\\n\
     vertex_file   using 3 : 4 with lines lt 1 lc -1 t \"\",\\\n\
     evertex_file  using 3 : 4 with lines lt 1 lc  9 t \"\",\\\n\
     fibers_hi     using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.08 lc rgb colour_hi lt 1 lw 0.2 t \"\",\\\n\
     fibers_file   using 2 : 3 with points pt pt_all ps 0.3 lc rgb colour_all t \"\",\\\n\
     fibers_hi     using 2 : 3 with points pt pt_hi  ps 0.4 lc rgb colour_hi t key_hi\n\n\n");
  }

  //a plot showing all hi-res fiber coverage of the focal plane
  if ( pFocal->num_fibers_really_hi  > 0 &&
       (pFocal->num_fibers_dual       > 0 ||
        pFocal->num_fibers_switchable > 0 ))
  {
    fprintf(pFibers_plotfile, "set label 100 \"High-res fiber coverage - inc. dual/switchable\"\n\
plot \\\n\
     null_file     using 1 : 2 : (fov_radius*2.0) : (fov_radius*2.0) with ellipses lt 1 lc 9 t \"\",\\\n\
     vertex_file   using 3 : 4 with lines lt 1 lc -1 t \"\",\\\n\
     evertex_file  using 3 : 4 with lines lt 1 lc  9 t \"\",\\\n\
     fibers_hi     using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.08 lc rgb colour_hi lt 1 lw 0.2 t \"\",\\\n\
     fibers_dual   using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.05 lc rgb colour_tint_hi lt 1 lw 0.2 t \"\",\\\n\
     fibers_switch using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.05 lc rgb colour_tint_hi lt 1 lw 0.2 t \"\",\\\n\
     fibers_file   using 2 : 3 with points pt pt_all    ps 0.3 lc rgb colour_all t \"\",\\\n\
     fibers_hi     using 2 : 3 with points pt pt_hi     ps 0.4 lc rgb colour_hi     t key_hi,\\\n\
     fibers_dual   using 2 : 3 with points pt pt_dual   ps 0.4 lc rgb colour_dual   t key_dual,\\\n\
     fibers_switch using 2 : 3 with points pt pt_switch ps 0.4 lc rgb colour_switch t key_switch\n\n\n");
  }

  
  //a plot showing pure low-res fiber coverage of the focal plane
  if ( pFocal->num_fibers_really_lo > 0 )
  {
    fprintf(pFibers_plotfile, "set label 100 \"Low-res fiber coverage\"\n\
plot \\\n\
     null_file     using 1 : 2 : (fov_radius*2.0) : (fov_radius*2.0) with ellipses lt 1 lc 9 t \"\",\\\n\
     vertex_file   using 3 : 4 with lines lt 1 lc -1 t \"\",\\\n\
     evertex_file  using 3 : 4 with lines lt 1 lc  9 t \"\",\\\n\
     fibers_lo     using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.08 lc rgb colour_lo lt 1 lw 0.2 t \"\",\\\n\
     fibers_file   using 2 : 3 with points pt pt_all ps 0.3 lc rgb colour_all t \"\",\\\n\
     fibers_lo     using 2 : 3 with points pt pt_lo  ps 0.4 lc rgb colour_lo t key_lo\n\n\n");
  }
  
  //a plot showing all low-res fiber coverage of the focal plane
  if ( pFocal->num_fibers_really_hi  > 0 &&
       (pFocal->num_fibers_dual       > 0 ||
        pFocal->num_fibers_switchable > 0 ))
  {
    fprintf(pFibers_plotfile, "set label 100 \"Low-res fiber coverage - inc. dual/switchable\"\n\
plot \\\n\
     null_file     using 1 : 2 : (fov_radius*2.0) : (fov_radius*2.0) with ellipses lt 1 lc 9 t \"\",\\\n\
     vertex_file   using 3 : 4 with lines lt 1 lc -1 t \"\",\\\n\
     evertex_file  using 3 : 4 with lines lt 1 lc  9 t \"\",\\\n\
     fibers_lo     using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.08 lc rgb colour_lo lt 1 lw 0.2 t \"\",\\\n\
     fibers_dual   using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.05 lc rgb colour_tint_lo lt 1 lw 0.2 t \"\",\\\n\
     fibers_switch using 2 : 3 : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.05 lc rgb colour_tint_lo lt 1 lw 0.2 t \"\",\\\n\
     fibers_file   using 2 : 3 with points pt pt_all    ps 0.3 lc rgb colour_all t \"\",\\\n\
     fibers_lo     using 2 : 3 with points pt pt_lo     ps 0.4 lc rgb colour_lo     t key_lo,\\\n\
     fibers_dual   using 2 : 3 with points pt pt_dual   ps 0.4 lc rgb colour_dual   t key_dual,\\\n\
     fibers_switch using 2 : 3 with points pt pt_switch ps 0.4 lc rgb colour_switch t key_switch\n\n\n");
  }


  if ( pSurvey->tiling_num_dithers > 1 )
  {
    for (i=0;i<pSurvey->tiling_num_dithers; i++)
    {
      fprintf(pFibers_plotfile, "dither_dx_%02d = %g\n\
dither_dy_%02d = %g\n",
              i, pSurvey->tiling_dither_offsets_x[i],
              i, pSurvey->tiling_dither_offsets_y[i]);
    }

    //a plot showing pure hi-res fiber coverage of the focal plane - dithered version
    if ( pFocal->num_fibers_really_hi > 0 )
    {
      fprintf(pFibers_plotfile, "set label 100 \"High-res fiber coverage - Dithered\"\n\
plot \\\n");
      for (i=0;i<pSurvey->tiling_num_dithers; i++)
      {
        if ( i>0 )  fprintf(pFibers_plotfile, ",\\\n");
        fprintf(pFibers_plotfile, "     vertex_file   using ($3+dither_dx_%02d) : ($4+dither_dy_%02d) with lines lt 1 lc -1 t \"\",\\\n\
     fibers_hi     using ($2+dither_dx_%02d) : ($3+dither_dy_%02d) : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.05 lc rgb colour_hi lt 1 lw 0.1 t \"\",\\\n\
     fibers_hi     using ($2+dither_dx_%02d) : ($3+dither_dy_%02d) with dots lc rgb colour_hi not,\\\n\
     null_file     using ($1+dither_dx_%02d) : ($2+dither_dy_%02d) with points pt 1 ps 1.5 lc -1 not",
                i,i, i,i, i,i, i,i);
      }
      fprintf(pFibers_plotfile, "\n\n\n");
    }


    //a plot showing pure lo-res fiber coverage of the focal plane - dithered version
    if ( pFocal->num_fibers_really_lo > 0 )
    {
      fprintf(pFibers_plotfile, "set label 100 \"Low-res fiber coverage - Dithered\"\n\
plot \\\n");
      for (i=0;i<pSurvey->tiling_num_dithers; i++)
      {
        if ( i>0 )  fprintf(pFibers_plotfile, ",\\\n");
        fprintf(pFibers_plotfile, "     vertex_file   using ($3+dither_dx_%02d) : ($4+dither_dy_%02d) with lines lt 1 lc -1 t \"\",\\\n\
     fibers_lo     using ($2+dither_dx_%02d) : ($3+dither_dy_%02d) : (dpatrol):(dpatrol) with ellipses fs transparent solid 0.04 lc rgb colour_lo lt 1 lw 0.1 t \"\",\\\n\
     fibers_lo     using ($2+dither_dx_%02d) : ($3+dither_dy_%02d) with dots lc rgb colour_lo not,\\\n\
     null_file     using ($1+dither_dx_%02d) : ($2+dither_dy_%02d) with points pt 1 ps 1.5 lc -1 not",
                i,i, i,i, i,i, i,i);
      }
      fprintf(pFibers_plotfile, "\n\n\n");
    }
  }
    

  fprintf(pFibers_plotfile, "set label 100 \"Sector layout\"\n\
set key title \"Sector Layout for Sky Fibers\"\n\
unset colorbox\n\
max_colours = 9\n\
set palette model RGB\n\
set palette defined ( 0*max_colours 0.5 0.25 0.2, 0.1*max_colours 0 0 1, 0.25*max_colours 0.7 0.85 0.9, 0.4*max_colours 0 0.75 0, 0.5*max_colours 1 1 0, 0.7*max_colours 1 0 0, 0.9*max_colours 0.6 0.6 0.6,1*max_colours 0.95 0.95 0.95 )\n\
plot\\\n\
     null_file     using 1 : 2 : (fov_radius*2.0) : (fov_radius*2.0) with ellipses lt 1 lc 9 t \"\",\\\n\
     vertex_file   using 3 : 4 with lines lt 1 lc -1 t \"\",\\\n\
     evertex_file  using 3 : 4 with lines lt 1 lc  9 t \"\",\\\n\
     fibers_lo     using 2 : 3 : ((int($5) %% max_colours)) with points pt pt_lo     ps 0.8 lw 1 lc palette t key_lo,\\\n\
     fibers_hi     using 2 : 3 : ((int($5) %% max_colours)) with points pt pt_hi     ps 0.8 lw 1 lc palette t key_hi,\\\n\
     fibers_dual   using 2 : 3 : ((int($5) %% max_colours)) with points pt pt_dual   ps 0.8 lw 1 lc palette t key_dual,\\\n\
     fibers_switch using 2 : 3 : ((int($5) %% max_colours)) with points pt pt_switch ps 0.8 lw 1 lc palette t key_switch,\\\n\
     fibers_empty  using 2 : 3 : ((int($5) %% max_colours)) with points pt pt_empty  ps 0.8 lw 1 lc palette t key_empty,\\\n\
     sectors_file using  2 : 3 : (sprintf(\"{/*1.5 %%d}\\nN_{lo}=%%d\\nN_{hi}=%%d\", $1,$5,$6)) with labels tc lt -1 font \",16\" t \"\"\n\n");

  
  //add a line to make thumbnail image
  fprintf(pFibers_plotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, FIBERS_GEOMETRY_STEM, FIBERS_GEOMETRY_STEM);

  if ( pFibers_plotfile ) fclose ( pFibers_plotfile) ;
  pFibers_plotfile = NULL;

  //now actually plot the fiber geometry file
  fprintf(stdout, "%s Making plot of fiber starting layout: %s -> %s/%s.pdf ...",
          COMMENT_PREFIX, str_filename, PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM);
  
  //  snprintf(strSystem, STR_MAX, "gnuplot %s &> /dev/null", str_filename);
  //  if ( system(strSystem))
  //  {
  //    //don't worry if it fails to run correctly, we'll just have to cope
  //  }
  fprintf(stdout, " done\n");

  //  if (util_run_gnuplot_on_plotfile( (const char*) str_filename, (BOOL) TRUE)) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_filename)) return 1;

  
  return 0; 
}





//go through a previously populated tile structure and determine which
//positioners can interact with which other positioners
//will only be done once per run, so no need to make this ultra-efficient.
int OpSim_positioner_determine_fiber_rivals (focalplane_struct *pFocal )
{
  int i,j;
  //  fiber_struct *pFib1 = NULL;
  //  fiber_struct *pFib2 = NULL;
  float sq_d_collision;
  float sq_d_neighbour;
  float sq_dist;
  int max_rivals = 0;
  int min_rivals = INT_MAX;
  int max_neighbours = 0;
  int min_neighbours = INT_MAX;

  if ( pFocal == NULL ) return 1;

  switch (pFocal->positioner_family)
  {
  case POSITIONER_FAMILY_POTZPOZ :
    sq_d_collision = (float) SQR(2.0*(pFocal->patrol_radius_max_mm + pFocal->min_sep_mm + pFocal->potzpoz_tip_radius));
    break;
  case POSITIONER_FAMILY_MUPOZ :
    sq_d_collision = (float) SQR(2.0*(pFocal->patrol_radius_max_mm + pFocal->min_sep_mm + pFocal->mupoz_tip_radius));
    break;
  case POSITIONER_FAMILY_ECHIDNA :
  case POSITIONER_FAMILY_AESOP :
    sq_d_collision = (float) SQR(2.0*(pFocal->patrol_radius_max_mm + pFocal->min_sep_mm + pFocal->spine_radius));
    break;
  case POSITIONER_FAMILY_STARBUG :
    sq_d_collision = (float) SQR(2.0*(pFocal->patrol_radius_max_mm + pFocal->min_sep_mm + pFocal->starbug_radius));
    break;
  default :
    return 1;
    break;
  }
  sq_d_neighbour = SQR(1.01*pFocal->positioner_spacing); //1% more than nominal spacing
  //initialise the counters
  for(i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    pFib->nrivals = (int) 0;
    pFib->nneighbours = (int) 0;
    for(j=0;j<MAX_RIVALS_PER_FIBER;j++)
    {
      pFib->rival_list[j] = (int) 0;
      pFib->rival_dist[j] = (float) -1.0;
    }
  }

  for(i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib1 = (fiber_struct*) &(pFocal->pFib[i]);
    for(j=i+1;j<pFocal->num_fibers;j++)
    {
      fiber_struct *pFib2 = (fiber_struct*) &(pFocal->pFib[j]);
      assert(!isnan(pFib1->Geom.x0));
      assert(!isnan(pFib1->Geom.y0));
      assert(!isnan(pFib2->Geom.x0));
      assert(!isnan(pFib2->Geom.y0));
      sq_dist = SQR(pFib1->Geom.x0 - pFib2->Geom.x0) + SQR(pFib1->Geom.y0 - pFib2->Geom.y0);
      if ( sq_dist <= sq_d_collision)
      {
        float dist = (float) sqrt(sq_dist);
        if ( pFib1->nrivals >= MAX_RIVALS_PER_FIBER )
        {
          fprintf(stderr, "%s Too many rivals (%d) for fiber number %d (x0,y0=%.7g,%.7g) \n",
                  ERROR_PREFIX, pFib1->nrivals, i, pFib1->Geom.x0, pFib1->Geom.y0);
          return 1;
        }
        pFib1->rival_list[pFib1->nrivals] = j;
        pFib1->rival_dist[pFib1->nrivals] = dist;
        pFib1->nrivals ++;
        
        if ( pFib2->nrivals >= MAX_RIVALS_PER_FIBER )
        {
          fprintf(stderr, "%s Too many rivals (%d) for fiber number %d\n",
                  ERROR_PREFIX, pFib2->nrivals, j);
          return 1;
        }
        pFib2->rival_list[pFib2->nrivals] = i;
        pFib2->rival_dist[pFib2->nrivals] = dist;
        pFib2->nrivals ++;
      }
      if ( sq_dist <= sq_d_neighbour)
      {
        pFib1->nneighbours++;
        pFib2->nneighbours++;
      }
    }
  }
  
  //now go through and sort the lists so that we always test
  //for collisions with the nearest neighbours first
  for(i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    if ( pFib->nrivals > 0 )
    {
      if ( util_fisort((ulong) pFib->nrivals, (float*) pFib->rival_dist, (int*) pFib->rival_list)) return 1;
    }

    if ( pFib->nrivals > max_rivals )  max_rivals = pFib->nrivals; //this is used in next step
    if ( pFib->nrivals < min_rivals )  min_rivals = pFib->nrivals; //this is used in next step
    if ( pFib->nneighbours > max_neighbours )  max_neighbours = pFib->nneighbours; //this is used in next step
    if ( pFib->nneighbours < min_neighbours )  min_neighbours = pFib->nneighbours; //this is used in next step
  }

  //set the flags to show which fibers are at the edge of the tile
  //base this on the number of neighbours that they have
  //edge means any fiber that has less than the full complement of neighbours
  //this all assumes that we have a filled focal plane.
  //only adjust edge_flags for targets with current edge flag < 0 
  //  if ( pFocal->flag_edge_flags_supplied_in_input_file == FALSE )
  {
    for(i=0;i<pFocal->num_fibers;i++)
    {
      fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
      if ( pFib->edge_flag < 0 )
      {
        if      ( pFib->nrivals     == max_rivals )     pFib->edge_flag = (int) POSITIONER_IS_NOT_AT_EDGE_OF_TILE;
        else if ( pFib->nneighbours == max_neighbours ) pFib->edge_flag = (int) POSITIONER_IS_AT_BORDER_OF_TILE;
        else if ( pFib->nneighbours == min_neighbours ) pFib->edge_flag = (int) POSITIONER_IS_AT_CORNER_OF_TILE; //this is not always true, better to use convex hull - TODO
        else                                            pFib->edge_flag = (int) POSITIONER_IS_AT_EDGE_OF_TILE;
      }
    }
  }
  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////

int OpSim_positioner_count_fibers (focalplane_struct *pFocal )
{
  int i;
  if ( pFocal == NULL ) return 1;
  //count up the numbers of fibers definitively
  pFocal->num_fibers_lo         = 0;
  pFocal->num_fibers_hi         = 0;
  pFocal->num_fibers_really_lo  = 0;
  pFocal->num_fibers_really_hi  = 0;
  pFocal->num_fibers_dual       = 0;
  pFocal->num_fibers_switchable = 0;
  pFocal->num_fibers_empty      = 0;
  for(i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    if      ( pFib->is_empty )  pFocal->num_fibers_empty ++;
    else if ( pFib->res == RESOLUTION_CODE_DUAL ) pFocal->num_fibers_dual ++;
    else if ( pFib->is_switchable )
    {
      pFocal->num_fibers_switchable ++;
      if      ( pFib->res == RESOLUTION_CODE_LOW  ) pFocal->num_fibers_lo ++;
      else if ( pFib->res == RESOLUTION_CODE_HIGH ) pFocal->num_fibers_hi ++;
    }
    else if ( pFib->res == RESOLUTION_CODE_LOW  )
    {
      pFocal->num_fibers_lo ++;
      pFocal->num_fibers_really_lo ++;
    }
    else if ( pFib->res == RESOLUTION_CODE_HIGH )
    {
      pFocal->num_fibers_hi ++;
      pFocal->num_fibers_really_hi ++;
    }
    else
    {
      fprintf(stderr, "%s Problem determining fiber status for fiber id=%d index=%d \n",
              ERROR_PREFIX, pFib->id, pFib->index);
      return 1;    
    }
  }
  return 0;
  
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_setup_fibers (focalplane_struct *pFocal, telescope_struct *pTele, survey_struct *pSurvey, BOOL do_plots)
{
  if ( pFocal == NULL || pTele == NULL || pSurvey == NULL) return 1;
  //either set up fibers internally, or read from an external file
  //////////////////////////
  if (strncmp(pFocal->positioner_layout_filename, "NONE",  strlen(pFocal->positioner_layout_filename)) == 0 )
  {
    //make our own positioner layout
    if ( OpSim_positioner_setup_fibers_geometry ((focalplane_struct*) pFocal,
                                                 (telescope_struct*) pTele))
    {
      fprintf(stderr, "%s Problem setting up hexagonal fibers geometry internally (%s %d)\n",
              ERROR_PREFIX, pFocal->str_positioner_hilo_pattern, pFocal->positioner_hilo_ratio);
      return 1;
    }
  }
  else
  {
    //read positioner layout from file
    if ( OpSim_positioner_read_fibers_geometry_from_file((focalplane_struct*) pFocal,
                                                         (const char *) pFocal->positioner_layout_filename))
    {
        fprintf(stderr, "%s Problem reading fibers geometry from file: %s\n",
                ERROR_PREFIX, pFocal->positioner_layout_filename);
        return 1;
    }
  }

  /////////////////////////////
  //count up the numbber of fibers of each class
  if ( OpSim_positioner_count_fibers ((focalplane_struct*) pFocal )) return 1;


  // Now adjust the minimum numbers of sky/PI fibers (i.e. if they have been supplied as fractions)
  // if supplied as negative numbers then treat as percentages
  if ( pSurvey->minimum_sky_fibers_low_res < 0 )
    pSurvey->minimum_sky_fibers_low_res  = (int) (pFocal->num_fibers_lo*0.01*abs(pSurvey->minimum_sky_fibers_low_res)); 
  if ( pSurvey->minimum_sky_fibers_high_res < 0 )
    pSurvey->minimum_sky_fibers_high_res = (int) (pFocal->num_fibers_hi*0.01*abs(pSurvey->minimum_sky_fibers_high_res)); 

  if ( pSurvey->minimum_PI_fibers_low_res < 0 )
    pSurvey->minimum_PI_fibers_low_res  = (int) (pFocal->num_fibers_lo*0.01*abs(pSurvey->minimum_PI_fibers_low_res)); 
  if ( pSurvey->minimum_PI_fibers_high_res < 0 )
    pSurvey->minimum_PI_fibers_high_res = (int) (pFocal->num_fibers_hi*0.01*abs(pSurvey->minimum_PI_fibers_high_res)); 

  if ( pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_LOW] < 0 )
    pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_LOW] = (int) (pFocal->num_fibers_lo*0.01*abs(pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_LOW])/((float) MAX(1,pFocal->num_sectors)) ); 
  if ( pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_HIGH] < 0 )
    pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_HIGH] = (int) (pFocal->num_fibers_hi*0.01*abs(pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_HIGH])/((float) MAX(1,pFocal->num_sectors)) ); 


  /////////////////////////////
  //reset the positioners and all their attributes
  if ( OpSim_positioner_init_fibers((focalplane_struct*) pFocal )) return 1;

  //now do some processing on the fiber layout
  if ( OpSim_positioner_determine_fiber_rivals ((focalplane_struct*) pFocal))
  {
    fprintf(stderr, "%s Problem determining fiber rivals\n", ERROR_PREFIX);
    return 1;
  }
  
  if ( OpSim_positioner_calc_tile_offaxis_max ((focalplane_struct*) pFocal ))
  {
    fprintf(stderr, "%s I had problems calculating the radius of the furthest positioner\n", ERROR_PREFIX);
    return 1;
  }

  if ( OpSim_positioner_calc_tile_vertices ( (focalplane_struct*) pFocal ))
  {
    fprintf(stderr, "%s I had problems calculating the vertices of the focal plane\n", ERROR_PREFIX);
    fflush(stderr);
    return 1;  
  }
  
  {
    char buffer[STR_MAX];
    snprintf(buffer, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, TILE_VERTICES_FILENAME);
    if ( OpSim_positioner_write_tile_vertices ( (focalplane_struct*) pFocal, (const char*) buffer))
    {
      fprintf(stderr, "%s I had problems writing the focal plane vertices file: %s\n", ERROR_PREFIX, buffer);
      fflush(stderr);
      return 1;  
    }
  }

  if ( OpSim_positioner_associate_fibers_with_quads ((focalplane_struct*) pFocal))
  {
    fprintf(stderr, "%s Error setting up tile quad struct\n", ERROR_PREFIX);
    return 1;
  }
  /////////////////////////////
  if ( quad_calc_neighbours ((quad_struct*) &(pFocal->Quad)))
  {
    fprintf(stderr, "%s Problem when calculating quad neighbours at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  ///////////////////////////////////
  if ( OpSim_positioner_assign_fibers_to_sectors ( (focalplane_struct*) pFocal ) )
  {
    fprintf(stderr, "%s Error assigning fibers to sectors\n", ERROR_PREFIX);
    return 1;
  }
  ///////////////////////////////////

  ///////////////////////////////////
  if ( OpSim_positioner_setup_fiber_mapping ( (focalplane_struct*) pFocal ))
  {
    fprintf(stderr, "%s Error setting up fiber <--> spectrograph mapping\n", ERROR_PREFIX);
    return 1;
  }

  ///////////////////////////////////


  
  //now write the fiber layout out to the standard file, with the standard format.
  {
    char buffer[STR_MAX];
    snprintf(buffer, STR_MAX, "%s/%s.txt", PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM);
    if ( OpSim_positioner_write_fibers_geometry ((focalplane_struct*) pFocal, (survey_struct*) pSurvey, (const char*) buffer))
    {
      return 1;
    }
  }
  
  if ( do_plots )
  {
    char buffer[STR_MAX];
    snprintf(buffer, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM);
    if ( OpSim_positioner_plot_fibers_geometry  ((focalplane_struct*) pFocal, (telescope_struct*) pTele, (survey_struct*) pSurvey, (const char*) buffer))
      return 1;
  }

  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////
//create the mapping between fibers and spectrographs
int OpSim_positioner_setup_fiber_mapping ( focalplane_struct *pFocal )
{
  int i;
  int n_lo = 0, n_hi = 0, n_sw = 0; 
  int spectro_lo = -1, spectro_hi = -1, spectro_sw = -1 ;
  float pos_lo = FLT_MAX;
  float pos_hi = FLT_MAX;
  float pos_sw = FLT_MAX;
  if ( pFocal == NULL ) return 1;

  pFocal->crossdisp_pixels_per_fiber_hires = NAN;
  pFocal->crossdisp_pixels_per_fiber_lores = NAN;
  pFocal->crossdisp_pixels_per_fiber_swres = NAN;
  
  //remember that we need extra 0.5*spacing at beginning of each spectrograph
  if ( pFocal->num_fibers_lo + pFocal->num_fibers_dual > 0 )
  pFocal->crossdisp_pixels_per_fiber_lores =
    pFocal->crossdisp_pixels_per_spectrograph_lores * pFocal->number_of_spectrographs_lores /
    (float) ((float)(pFocal->num_fibers_lo + pFocal->num_fibers_dual) + 0.5*(float) pFocal->number_of_spectrographs_lores);
  if ( pFocal->num_fibers_hi + pFocal->num_fibers_dual > 0 )
    pFocal->crossdisp_pixels_per_fiber_hires =
      pFocal->crossdisp_pixels_per_spectrograph_hires * pFocal->number_of_spectrographs_hires /
      (float) ((float)(pFocal->num_fibers_hi + pFocal->num_fibers_dual) + 0.5*(float) pFocal->number_of_spectrographs_hires);
  if ( pFocal->num_fibers_switchable > 0 )
    pFocal->crossdisp_pixels_per_fiber_swres =
      pFocal->crossdisp_pixels_per_spectrograph_swres * pFocal->number_of_spectrographs_swres /
      (float) ((float)(pFocal->num_fibers_switchable) + 0.5*(float) pFocal->number_of_spectrographs_swres);

  for (i=0; i<pFocal->num_fibers; i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);

    if ( pFib->is_empty ) continue; //no spectrograph connection for this positioner
      
    //check to see if we should start on a new low res spectrograph
    if ( pos_lo >= (float) pFocal->crossdisp_pixels_per_spectrograph_lores - 0.5*pFocal->crossdisp_pixels_per_fiber_lores )
    {
      pos_lo = 0.5*pFocal->crossdisp_pixels_per_fiber_lores; //allow a small buffer at beginning of CCD
      n_lo = 0;
      spectro_lo ++;
    }
    //check to see if we should start on a new high res spectrograph
    if ( pos_hi >= (float) pFocal->crossdisp_pixels_per_spectrograph_hires - 0.5*pFocal->crossdisp_pixels_per_fiber_hires )
    {
      pos_hi = 0.5*pFocal->crossdisp_pixels_per_fiber_hires; //allow a small buffer at beginning of CCD
      n_hi = 0;
      spectro_hi ++;
    }
    //check to see if we should start on a new switchable spectrograph
    if ( pos_sw >= (float) pFocal->crossdisp_pixels_per_spectrograph_swres - 0.5*pFocal->crossdisp_pixels_per_fiber_swres )
    {
      pos_sw = 0.5*pFocal->crossdisp_pixels_per_fiber_swres; //allow a small buffer at beginning of CCD
      n_sw = 0;
      spectro_sw ++;
    }

    //init the values
    pFib->spectrograph_lores    = -1;
    pFib->streak_number_lores   = -1;
    pFib->crossdisp_pixel_lores = NAN;
    pFib->spectrograph_hires    = -1;
    pFib->streak_number_hires   = -1;
    pFib->crossdisp_pixel_hires = NAN;
    pFib->spectrograph_swres    = -1;
    pFib->streak_number_swres   = -1;
    pFib->crossdisp_pixel_swres = NAN;

    
    if ( pFib->is_switchable )  
    {
      pFib->spectrograph_swres    = spectro_sw;
      pFib->streak_number_swres   = n_sw;
      pFib->crossdisp_pixel_swres = pos_sw;
      n_sw ++;
      pos_sw += (float) pFocal->crossdisp_pixels_per_fiber_swres;
    }
    else
    {
      switch (pFib->res)
      {
      case RESOLUTION_CODE_HIGH :
        pFib->spectrograph_hires    = spectro_hi;
        pFib->streak_number_hires   = n_hi;
        pFib->crossdisp_pixel_hires = pos_hi;
        n_hi ++;
        pos_hi += (float) pFocal->crossdisp_pixels_per_fiber_hires;
        break;
      case RESOLUTION_CODE_LOW :
        pFib->spectrograph_lores    = spectro_lo;
        pFib->streak_number_lores   = n_lo;
        pFib->crossdisp_pixel_lores = pos_lo;
        n_lo ++;
        pos_lo += (float) pFocal->crossdisp_pixels_per_fiber_lores;
        break;
      case RESOLUTION_CODE_DUAL :
        pFib->spectrograph_hires    = spectro_hi;
        pFib->streak_number_hires   = n_hi;
        pFib->crossdisp_pixel_hires = pos_hi;
        pFib->spectrograph_lores    = spectro_lo;
        pFib->streak_number_lores   = n_lo;
        pFib->crossdisp_pixel_lores = pos_lo;
        n_hi ++;
        pos_hi += (float) pFocal->crossdisp_pixels_per_fiber_hires;
        n_lo ++;                          
        pos_lo += (float) pFocal->crossdisp_pixels_per_fiber_lores;
        break;
      default :
        return 1; //error
        break;
      }
    }
  }
  
  return 0;
}


//set up a quad struct in x,y space on the instrument focal plane
//we want quads to be a little bigger than the patrol radius. This ensures that
//by searching in the central quad and its 8 neighbours we find all the positioners
//that can reach a given point.
int OpSim_positioner_associate_fibers_with_quads  (focalplane_struct *pFocal) 
{
  quad_struct *pQuad = NULL;
  double half_size;
  int quad_depth;
  int i;
  if ( pFocal == NULL ) return 1;
  pQuad = (quad_struct*) &(pFocal->Quad);
  
  //determine the most efficient size of quad
  half_size = pFocal->patrol_radius_max_mm;
  quad_depth = 1;
  while ( half_size < pFocal->fiber_offaxis_max_mm)
  {
    half_size *= 2.0;
    quad_depth ++;
  }

  fprintf(stdout, "%s Setting up the quadtree structure associations for each fiber with half size %.5g depth= %d\n", COMMENT_PREFIX, half_size, quad_depth);
  if ( quad_init_root ((quad_struct*) pQuad, (double) -1.*half_size,  (double) half_size,  (double) -1.*half_size,  (double) half_size))
  {
    fprintf(stderr, "%s Error setting up Quadtree structure at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);fflush(stderr);
    return 1;
  } //this is freed in focalplane_struct_free() 
  
  if ( quad_add_children_to_depth ((quad_struct*) pQuad, quad_depth))
  {
    fprintf(stderr, "%s Problem when adding %d layers of children to Quadtree structure at file %s line %d\n",
            ERROR_PREFIX, quad_depth, __FILE__, __LINE__);
    return 1;
  }

  //now associate the fibers with the quads
  for ( i=0;i<pFocal->num_fibers;i++)
  {
    quad_struct* pq = NULL;
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    pq = (quad_struct*) quad_get_quad_from_position((quad_struct*)pQuad,
                                                    (double) pFib->Geom.x0,
                                                    (double) pFib->Geom.y0);
    if ( pq  == NULL )
    {
      //this is definitely not good 
      fprintf(stderr, "%s Error finding Quadtree structure for fiber %4d coords= %10.4f %10.4f\n",
              ERROR_PREFIX, pFib->id, pFib->Geom.x0, pFib->Geom.y0);
      fflush(stderr);
      return 1;
    }
    else
    {
      if ( quad_add_unique_member_to_quad((quad_struct*)pq, (void*) pFib))
      {
        //this is bad
        fprintf(stderr, "%s Error adding fiber to Quadtree structure for fiber %4d coords= %10.4f %10.4f : too many members\n",
                ERROR_PREFIX, pFib->id, pFib->Geom.x0, pFib->Geom.y0);
        fflush(stderr);
        return 1;
      }
    }
  }
  
  return 0;
} 

////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_check_for_fiber_failure_mupoz (focalplane_struct *pFocal, fiber_struct *pFib)
{
  if ( pFocal == NULL || pFib == NULL ) return -1;
  if ( pFib->status == (uint) FIBER_STATUS_FLAG_NOMINAL )
  {
  //TODO - fill in the details for check_for_fiber_failure_mupoz()
    
  }
  else
  {
    //a broken fiber cannot become any more broken
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_check_for_fiber_failure_potzpoz (focalplane_struct *pFocal, fiber_struct *pFib)
{
  if ( pFocal == NULL || pFib == NULL ) return -1;
  if ( pFib->status == (uint) FIBER_STATUS_FLAG_NOMINAL )
  {
  //TODO - fill in the details for check_for_fiber_failure_potzpoz()
    
  }
  else
  {
    //a broken fiber cannot become any more broken
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_check_for_fiber_failure_echidna (focalplane_struct *pFocal, fiber_struct *pFib)
{
  if ( pFocal == NULL || pFib == NULL ) return -1;
  if ( pFib->status == (uint) FIBER_STATUS_FLAG_NOMINAL )
  {
//  //TODO - fill in the details for check_for_fiber_failure_echidna()
//    if (drand48() < (1e-6 + 1e-10*pFib->Geom.number_of_movements))  //This is total experimentation!
//    {
//      pFib->status = FIBER_STATUS_FLAG_BROKEN;
//    }
  }
  else
  {
    //a broken fiber cannot become any more broken
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_check_for_fiber_failure_aesop (focalplane_struct *pFocal, fiber_struct *pFib)
{
  if ( pFocal == NULL || pFib == NULL ) return -1;
  if ( pFib->status == (uint) FIBER_STATUS_FLAG_NOMINAL )
  {
//  //TODO - fill in the details 
  }
  else
  {
    //a broken fiber cannot become any more broken
  }
  return 0;
}

////////////////////////////////////////////////////////////////////////////
int OpSim_positioner_check_for_fiber_failure_starbug (focalplane_struct *pFocal, fiber_struct *pFib)
{
  if ( pFocal == NULL || pFib == NULL ) return -1;
  if ( pFib->status == (uint) FIBER_STATUS_FLAG_NOMINAL )
  {
//  //TODO - fill in the details for check_for_fiber_failure_starbug()
//    {
//      pFib->status = FIBER_STATUS_FLAG_BROKEN;
//    }
  }
  else
  {
    //a broken fiber cannot become any more broken
  }
  return 0;
}


//go through the tile and see if we have had any failures of positioners or fibers
int OpSim_positioner_check_for_fiber_failures (focalplane_struct *pFocal)
{
  int i;
  if ( pFocal == NULL ) return -1;

  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    switch (pFocal->positioner_family)
    {
    case POSITIONER_FAMILY_POTZPOZ :
      if ( OpSim_positioner_check_for_fiber_failure_potzpoz((focalplane_struct*) pFocal, (fiber_struct*) pFib)) return 1;
      break;
    case POSITIONER_FAMILY_MUPOZ :
      if ( OpSim_positioner_check_for_fiber_failure_mupoz((focalplane_struct*) pFocal, (fiber_struct*) pFib)) return 1;
      break;
    case POSITIONER_FAMILY_ECHIDNA :
      if ( OpSim_positioner_check_for_fiber_failure_echidna((focalplane_struct*) pFocal, (fiber_struct*) pFib)) return 1;
      break;
    case POSITIONER_FAMILY_AESOP :
      if ( OpSim_positioner_check_for_fiber_failure_aesop((focalplane_struct*) pFocal, (fiber_struct*) pFib)) return 1;
      break;
    case POSITIONER_FAMILY_STARBUG :
      if ( OpSim_positioner_check_for_fiber_failure_starbug((focalplane_struct*) pFocal, (fiber_struct*) pFib)) return 1;
      break;
    default :
      return 1;
      break;
    }
  }
  
  return 0;
}

float OpSim_positioner_calc_relative_throughput_fiber_potzpoz (focalplane_struct *pFocal, fiber_struct *pFib )
{
  if ( pFocal == NULL || pFib == NULL ) return NAN;
  return 1.0;
}
float OpSim_positioner_calc_relative_throughput_fiber_mupoz   (focalplane_struct *pFocal, fiber_struct *pFib )
{
  if ( pFocal == NULL || pFib == NULL ) return NAN;
  return 1.0;
}
float OpSim_positioner_calc_relative_throughput_fiber_echidna (focalplane_struct *pFocal, fiber_struct *pFib )
{
  if ( pFocal == NULL || pFib == NULL ) return NAN;
  return 1.0;
}
float OpSim_positioner_calc_relative_throughput_fiber_aesop (focalplane_struct *pFocal, fiber_struct *pFib )
{
  if ( pFocal == NULL || pFib == NULL ) return NAN;
  return 1.0;
}
float OpSim_positioner_calc_relative_throughput_fiber_starbug (focalplane_struct *pFocal, fiber_struct *pFib )
{
  if ( pFocal == NULL || pFib == NULL ) return NAN;
  return 1.0;
}

float OpSim_positioner_calc_relative_throughput_fiber (focalplane_struct *pFocal, fiber_struct *pFib )
{
  float result = 1.0;
  if ( pFocal == NULL || pFib == NULL ) return NAN;
  switch (pFocal->positioner_family)
  {
  case POSITIONER_FAMILY_POTZPOZ :
    result = OpSim_positioner_calc_relative_throughput_fiber_potzpoz((focalplane_struct*) pFocal, (fiber_struct*) pFib);
    break;
  case POSITIONER_FAMILY_MUPOZ :
    result = OpSim_positioner_calc_relative_throughput_fiber_mupoz  ((focalplane_struct*) pFocal, (fiber_struct*) pFib);
    break;
  case POSITIONER_FAMILY_ECHIDNA :
    result = OpSim_positioner_calc_relative_throughput_fiber_echidna((focalplane_struct*) pFocal, (fiber_struct*) pFib);
    break;
  case POSITIONER_FAMILY_AESOP :
    result = OpSim_positioner_calc_relative_throughput_fiber_aesop ((focalplane_struct*) pFocal, (fiber_struct*) pFib);
    break;
  case POSITIONER_FAMILY_STARBUG :
    result = OpSim_positioner_calc_relative_throughput_fiber_starbug((focalplane_struct*) pFocal, (fiber_struct*) pFib);
    break;
  default :
      return NAN;
      break;
  }
  return result;
}


//write out a summary of the details, history and stats of each fiber
int OpSim_positioner_write_fibers_report (focalplane_struct *pFocal, const char *str_filename)
{
  FILE* pFile = NULL;
  int i;
  if ( pFocal == NULL ) return 1;

  //open up the output file
  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the fibers report file: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }
  fprintf(stdout, "%s I am writing the fibers report file, output at: %s\n", COMMENT_PREFIX, str_filename);
  (void) fflush(stdout);

  //
  {
    time_t rawtime;
    struct tm *timeinfo;
    
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, "#This is the fibers report logfile\n\
#This log was first written at: %s\
#Original filename: %s\n\
#This logfile contains one entry per fiber/positioner\n\
#The file gives summary info about how that fiber performed\n\
#Description of columns:\n\
#Index  Name        Format    Description\n\
#-----  ----------- --------- ------------------------------------------------------------------------ \n\
#1      index       int       Running index number of fiber (1st fiber=0)\n\
#2      id          int       Unique ID number of fiber\n\
#3      res         character Resolution code of fiber(1=low-res;2=high-res;D=dual-res;S=switchable;E=empty)\n\
#4      sect        int       Sector number for this fiber \n\
#5      x0          float     x coordinate of base position of fiber in focal plane (mm)\n\
#6      y0          float     y coordinate of base position of fiber in focal plane (mm)\n\
#7      offaxis     float     distance of fiber base position from FOV centre (mm)\n\
#8      rmin        float     minimum patrol radius of fiber (mm)\n\
#9      rmax        float     maximum patrol radius of fiber (mm)\n\
#10     nriv        int       number of rival fibers (potential colliders) for this fiber\n\
#11     spec        int       index number of spectrograph fed by this fiber\n\
#12     streak      int       index number of spectral 'streak' produced by this fiber\n\
#13     Xpixel      float     CCD pixel number (in cross dispersion direction) for this fiber\n\
#14     nmove       int       Total number of movements for this fiber positioner\n\
#15     dmove1      float     Total distance moved by tip of positioner in 1st orthogonal sense\n\
#16     dmove2      float     Total distance moved by tip of positioner in 2nd orthogonal sense\n\
#17     nRep        int       Number of times that this positioner was repaired\n\
#18     nTarget     int       Number of Tiles in which this fiber was assigned to a science target\n\
#19     nSky        int       Number of Tiles in which this fiber was assigned to be a sky fiber\n\
#20     nSpare      int       Number of Tiles in which this fiber was unassigned\n\
#21     nBroken     int       Number of Tiles in which this fiber was faulty\n\
#22     nCandTot    int       Number of candidate targets falling in patrol area of this fiber\n\
#23     wCandTot    float     Weighted number of candidate targets falling in patrol area of this fiber\n\
#24     nAssHi      int       Number of Tiles in which this fiber was assigned in low res mode\n\
#25     nAssLo      int       Number of Tiles in which this fiber was assigned in high res mode\n\
#-----  ----------- --------- ------------------------------------------------------------------------ \n",
          asctime(timeinfo),
          str_filename);
    
  }

  //write a simple column header line
  fprintf(pFile, "#%-5s %6s %3s %4s %10s %10s %10s \
%8s %8s %4s %4s %6s %8s \
%8s %10s %10s %4s \
%8s %8s %8s %8s \
%10s %10s %6s %6s\n",
          "index", "id", "res", "sect", "x0", "y0", "offaxis",
          "rmin", "rmax", "nriv", "spec", "streak", "Xpixel",
          "nmove", "dmove1", "dmove2", "nRep",
          "nTarget", "nSky", "nSpare", "nBroken",
          "nCandTot","wCandTot", "nAssHi", "nAssLo");

  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    int   spectro = -1;
    int   streak = -1;
    float pixel = NAN;
    if ( pFib->is_empty == FALSE )
    {
      if ( pFib->is_switchable )
      {
        spectro = pFib->spectrograph_swres;
        streak  = pFib->streak_number_swres;
        pixel   = pFib->crossdisp_pixel_swres; 
      }
      else if ( pFib->res==RESOLUTION_CODE_HIGH ||
                pFib->res==RESOLUTION_CODE_DUAL )
      {
        spectro = pFib->spectrograph_hires;
        streak  = pFib->streak_number_hires;
        pixel   = pFib->crossdisp_pixel_hires; 
      }
      else if ( pFib->res==RESOLUTION_CODE_LOW )
      {
        spectro = pFib->spectrograph_lores;
        streak  = pFib->streak_number_lores;
        pixel   = pFib->crossdisp_pixel_lores; 
      }
    }
    
    fprintf(pFile, "%6d %6d %3s %4d %10.4f %10.4f %10.4f \
%8.3f %8.3f %4d %4d %6d %8.2f \
%8d %10.1f %10.1f %4d \
%8d %8d %8d %8d \
%10d %10.2f %6d %6d\n",
            pFib->index, pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib),
            pFocal->Sector[pFib->sector].id,
            pFib->Geom.x0, pFib->Geom.y0, sqrt(SQR(pFib->Geom.x0) + SQR(pFib->Geom.y0)),
            pFocal->patrol_radius_min_mm, pFocal->patrol_radius_max_mm, 
            pFib->nrivals,
            (int) spectro,
            (int) streak,
            (float) pixel,
            pFib->Geom.number_of_movements_total,
            pFib->Geom.distance_moved_dir1_total,
            pFib->Geom.distance_moved_dir2_total,
            pFib->number_of_repairs,
            pFib->total_assignments_to_target,
            pFib->total_assignments_to_sky,
            pFib->total_assignments_to_spare,
            pFib->total_num_tiles_spent_in_broken_state,
            pFib->number_of_candidate_targets,
            pFib->number_of_candidate_targets_weighted,
            pFib->total_assignments_as_hires,
            pFib->total_assignments_as_lores);
  }

  
  if ( pFile ) fclose (pFile); pFile = NULL;
  return 0;
}



//we can calculate the offaxis angle  of the positioner that is furthest from the centre
//we will use this for our measures of what is inside and outside the hexagon.
int OpSim_positioner_calc_tile_offaxis_max ( focalplane_struct *pFocal )
{
  float max_sq_fiber_offaxis = 0.0;
  float sq_offaxis = NAN;
  int i;
  if ( pFocal == NULL ) return 1;

  for(i=0;i<pFocal->num_fibers;i++)
  {
    sq_offaxis = SQR(pFocal->pFib[i].Geom.x0) + SQR(pFocal->pFib[i].Geom.y0);
    if ( sq_offaxis > max_sq_fiber_offaxis ) max_sq_fiber_offaxis = sq_offaxis;
  }
  pFocal->sq_fiber_offaxis_max_mm  = sq_offaxis;
  pFocal->fiber_offaxis_max_mm     = sqrt(sq_offaxis);
  pFocal->fiber_offaxis_max_deg    = pFocal->fiber_offaxis_max_mm * pFocal->invplate_scale * ARCSEC_TO_DEG;
  
  return 0;
}

//new method that calculates the vertices from the coords of the extremal positioners 
//assumes that the corner positioners have been previously identified and flagged
int OpSim_positioner_calc_tile_vertices ( focalplane_struct *pFocal )
{
  int i, j;
  int num_vertex = 0;
  double *px = NULL;
  double *py = NULL;
  double *pa = NULL;
  if ( pFocal == NULL ) return 1;

  if ( pFocal->layout_shape_code == GEOM_SHAPE_CIRCLE )
  {
    pFocal->num_vertex = 0;
    pFocal->num_ext_vertex = 0;
  }
  else
  {
    for(i=0;i<pFocal->num_fibers;i++)
      if ( pFocal->pFib[i].edge_flag == POSITIONER_IS_AT_CORNER_OF_TILE ) num_vertex++;
    
    if ( num_vertex < 3 || num_vertex > MAX_VERTICES_PER_TILE)
    {
      fprintf(stderr, "%s Problem with tile vertices, num_vertex= %d (valid range=[3:%d])\n",
              ERROR_PREFIX, num_vertex, MAX_VERTICES_PER_TILE);
      return 1;
    }
    
    if ((px = (double*) malloc(num_vertex * sizeof(double))) == NULL ||
        (py = (double*) malloc(num_vertex * sizeof(double))) == NULL ||
        (pa = (double*) malloc(num_vertex * sizeof(double))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);fflush(stderr);
      return 1;
    }
    
    j = 0;
    for(i=0;i<pFocal->num_fibers;i++)
    {
      fiber_struct* pFib = (fiber_struct*) &(pFocal->pFib[i]);
      if ( pFib->edge_flag == POSITIONER_IS_AT_CORNER_OF_TILE )
      {
        px[j] = pFib->Geom.x0;
        py[j] = pFib->Geom.y0;
        pa[j] = atan2d(py[j], px[j]); //calc angle of vertex from origin
        while( pa[j]  < 0.0   ) pa[j] += (double) 360.0;
        while( pa[j] >= 360.0 ) pa[j] -= (double) 360.0;
        j++;
      }
    }
    //sort on basis of angle from centre
    if ( util_dddsort ((unsigned long) num_vertex, (double*) pa, (double*) px, (double*) py)) return 1;
    
    //put a copy of the vertices in the Tile struct
    pFocal->num_vertex = num_vertex;  //will need this later
    for(j=0;j<num_vertex;j++)
    {
      pFocal->vertex_x[j] = px[j];
      pFocal->vertex_y[j] = py[j];
    }
    
    
    //let's ditch the extended vertices stuff for now:
    //just copy the normal vertices
    pFocal->num_ext_vertex = num_vertex;  
    for(j=0;j<num_vertex;j++)
    {
      pFocal->ext_vertex_x[j] = px[j];
      pFocal->ext_vertex_y[j] = py[j];
    }
    
    ///////////TODO - set up the extended vertices of the focal plane
    ///////////////see http://mathworld.wolfram.com/PerpendicularVector.html
    //now do the extended vertices
    //move each vector outwards parallel to itself by an amount equal to the patrol radius
    //perpendicular direction vector
    //if original vector is:
    //  a = [x2-x1]
    //      [y2-y1]
    //then
    //  a_| = [y1-y2]  or  [y2-y1]
    //        [x2-x1]      [x1-x2]
    //  //first calculate the interim points
    ////////////////////////////////////////////////
    
    if ( px ) free (px); px = NULL;
    if ( py ) free (py); py = NULL;
    if ( pa ) free (pa); pa = NULL;
  }
    
  fprintf(stdout, "%s I found %d vertices and %d extended vertices (max positioner radius= %.5g deg)\n",
          COMMENT_PREFIX, pFocal->num_vertex, pFocal->num_ext_vertex, pFocal->fiber_offaxis_max_deg);
  fflush(stdout);
  return 0;
}
  
// old method that assumes perfect hexagonal geometry
//  //////////////////////////////////////////////////////////////////////////////////////////////////////////
//  //we can now calculate the vertices of the hexagonal tile
//  //will be done once, in mm coords
//  int calc_tile_vertices_old ( focalplane_struct *pFocal )
//  {
//    int i;
//    double radius_to_use;
//    if ( pFocal == NULL ) return 1;
//    
//    radius_to_use = (double) pFocal->fiber_offaxis_max_mm;
//  
//    //set the coords of the vertices
//    //start at top index = 0 and go clockwise
//    //a
//    pFocal->num_vertex = HEXAGON_VERTICES;
//    pFocal->vertex_x[0] = 0.0;
//    pFocal->vertex_y[0] = radius_to_use;
//    //b
//    pFocal->vertex_x[1] = radius_to_use * SQRT_3_OVER_2;
//    pFocal->vertex_y[1] = radius_to_use *  0.5;
//    //c                     
//    pFocal->vertex_x[2] = radius_to_use * SQRT_3_OVER_2;
//    pFocal->vertex_y[2] = radius_to_use * -0.5;
//    //d                     
//    pFocal->vertex_x[3] = 0.0; 
//    pFocal->vertex_y[3] = radius_to_use * -1.0;
//    //e                     
//    pFocal->vertex_x[4] = radius_to_use * MSQRT_3_OVER_2;
//    pFocal->vertex_y[4] = radius_to_use * -0.5;
//    //f                     
//    pFocal->vertex_x[5] = radius_to_use * MSQRT_3_OVER_2;
//    pFocal->vertex_y[5] = radius_to_use * 0.5;
//    
//  //midpoints of faces of hexagon are simply the halfway points between the vertices.
//  for (i = 0; i <  pFocal->num_vertex; i++) 
//  {
//    int i1 = (i + 1 ) % pFocal->num_vertex;
//    pFocal->midface_x[i] = 0.5*(pFocal->vertex_x[i] +  pFocal->vertex_x[i1]);
//    pFocal->midface_y[i] = 0.5*(pFocal->vertex_y[i] +  pFocal->vertex_y[i1]);
//  }
//    
//    //now the extended ones - two per real vertex, go clockwise again
//    pFocal->num_ext_vertex = 12;
//    //a
//    pFocal->ext_vertex_x[0] = pFocal->vertex_x[0] - pFocal->patrol_radius_max_mm * 0.5;
//    pFocal->ext_vertex_y[0] = pFocal->vertex_y[0] + pFocal->patrol_radius_max_mm * SQRT_3_OVER_2;
//    pFocal->ext_vertex_x[1] = pFocal->vertex_x[0] + pFocal->patrol_radius_max_mm * 0.5;
//    pFocal->ext_vertex_y[1] = pFocal->vertex_y[0] + pFocal->patrol_radius_max_mm * SQRT_3_OVER_2;
//    
//    //b
//    pFocal->ext_vertex_x[2] = pFocal->vertex_x[1] + pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    pFocal->ext_vertex_y[2] = pFocal->vertex_y[1] + pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    pFocal->ext_vertex_x[3] = pFocal->vertex_x[1] + pFocal->patrol_radius_max_mm;
//    pFocal->ext_vertex_y[3] = pFocal->vertex_y[1] + 0.0;
//    
//    //c
//    pFocal->ext_vertex_x[4] = pFocal->vertex_x[2] + pFocal->patrol_radius_max_mm;
//    pFocal->ext_vertex_y[4] = pFocal->vertex_y[2] + 0.0;
//    pFocal->ext_vertex_x[5] = pFocal->vertex_x[2] + pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    pFocal->ext_vertex_y[5] = pFocal->vertex_y[2] - pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    
//    //d
//    pFocal->ext_vertex_x[6] = pFocal->vertex_x[3] + pFocal->patrol_radius_max_mm * 0.5;
//    pFocal->ext_vertex_y[6] = pFocal->vertex_y[3] - pFocal->patrol_radius_max_mm * SQRT_3_OVER_2;
//    pFocal->ext_vertex_x[7] = pFocal->vertex_x[3] - pFocal->patrol_radius_max_mm * 0.5;
//    pFocal->ext_vertex_y[7] = pFocal->vertex_y[3] - pFocal->patrol_radius_max_mm * SQRT_3_OVER_2;
//    
//    //e
//    pFocal->ext_vertex_x[8] = pFocal->vertex_x[4] - pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    pFocal->ext_vertex_y[8] = pFocal->vertex_y[4] - pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    pFocal->ext_vertex_x[9] = pFocal->vertex_x[4] - pFocal->patrol_radius_max_mm;
//    pFocal->ext_vertex_y[9] = pFocal->vertex_y[4] + 0.0;
//    
//    //f
//    pFocal->ext_vertex_x[10] = pFocal->vertex_x[5] - pFocal->patrol_radius_max_mm;
//    pFocal->ext_vertex_y[10] = pFocal->vertex_y[5] + 0.0;
//    pFocal->ext_vertex_x[11] = pFocal->vertex_x[5] - pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    pFocal->ext_vertex_y[11] = pFocal->vertex_y[5] + pFocal->patrol_radius_max_mm * SQRT_2_OVER_2;
//    
//    return 0;
//  }
//  //////////////////////////////////////////////////////////


int OpSim_positioner_write_tile_vertices ( focalplane_struct *pFocal, const char* str_filename )
{
  int kk;
  FILE* pFile = NULL;
  if ( pFocal == NULL ) return 1;

  if ( pFocal->num_vertex > 0 )
  {
    //open up the file
    if ((pFile = fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s  I had problems opening the vertices file: %s\n", ERROR_PREFIX, str_filename); fflush(stderr);
      return 1;
    }
    //write a header
    fprintf(pFile, "#This file contains the locations of the tile vertices in focal plane coordinates\n");
    fprintf(pFile, "#%-11s %6s %12s %12s\n", "VertexType", "Index", "x_coord(mm)", "y_coord(mm)");
    
    for (kk=0;kk<=pFocal->num_vertex;kk++)
    {
      int k = kk % pFocal->num_vertex;
      fprintf(pFile, "%-12s %6d %12.5f %12.5f\n", "Normal", k+1, pFocal->vertex_x[k], pFocal->vertex_y[k]);
    }
    fprintf(pFile, "\n\n");
    
    for (kk=0;kk<=pFocal->num_ext_vertex;kk++)
    {
      int k = kk % pFocal->num_vertex;
      fprintf(pFile, "%-12s %6d %12.5f %12.5f\n", "Extended", k+1, pFocal->ext_vertex_x[k], pFocal->ext_vertex_y[k]);
    }
    
    if ( pFile ) fclose(pFile);
    pFile = NULL;
  }
  return 0;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//work out which fibers should be associated with which sectors 
// num_sectors = 1         -> all fibers in one sector
// 2 <= num_sectors <= 9   -> one circular central sector, and a ring of (num_sectors-1) outer sectors
// 10 <= num_sectors <= 21 -> one circular central sector, an inner ring of (num_sectors-1)/2 sectors
//                            and an outer ring of (num_sectors-1)/2 sectors
//if sectors have already been assigned in the input fiber geometry file, then we will just accept that layout.
#define MAX_SECTOR_RINGS 3
#define SECTORS_LAYOUT_CIRC 0
#define SECTORS_LAYOUT_CIRC_RING 1
#define SECTORS_LAYOUT_CIRC_RING_RING 2
#define SECTORS_LAYOUT_USER 3

int OpSim_positioner_assign_fibers_to_sectors ( focalplane_struct *pFocal )
{
  int i;
  int sectors_layout; 
  int num_sectors_per_ring[MAX_SECTOR_RINGS];
  float radius_max[MAX_SECTOR_RINGS];
  float angle_per_sector[MAX_SECTOR_RINGS];
  float area_per_sector;
  
  if ( pFocal == NULL )  return 1;

  //////////////////////
  //set some bits and bobs to default values:
  for(i=0;i<MAX_SECTOR_RINGS;i++)
  {
    num_sectors_per_ring[i] = 0;
    radius_max[i] = NAN;
    angle_per_sector[i] = NAN;
  }
  //TODO: change the pFocal->Sectors array to be malloc'ed? 
  for(i=0;i<MAX_SECTORS_PER_TILE;i++)
  {
    sector_struct *pSector = (sector_struct*) &(pFocal->Sector[i]);
    pSector->id = i+1;
    pSector->ring = 0;
    pSector->num_fibers = 0;
    pSector->num_fibers_per_res[RESOLUTION_CODE_LOW] = 0;
    pSector->num_fibers_per_res[RESOLUTION_CODE_HIGH] = 0;
    pSector->num_fibers_per_res[RESOLUTION_CODE_DUAL] = 0;
    pSector->num_fibers_switchable = 0;
    pSector->num_fibers_empty      = 0;
    pSector->ppFib = NULL;
  }
  //////////////////////
  
  //first check to see if sectors were read from the input file
  //if so do some intitialising, sorting etc
  //note that we read the sector numbers in as "id"s but then change them to indices
  if ( pFocal->flag_sectors_supplied_in_input_file == TRUE )
  {
    sectors_layout = SECTORS_LAYOUT_USER;
    pFocal->num_sectors = 0;
    //determine how many unique sector numbers we have
    for ( i=0;i<pFocal->num_fibers; i++)
    {
      int j;
      fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
      for(j=0;j<pFocal->num_sectors;j++)
      {
        if (pFib->sector == pFocal->Sector[j].id)
        {          
          pFib->sector = j; //set the pFib->sector to the index rather than the id
          break;
        }
      }
      if ( j>= pFocal->num_sectors ) //this must be a new sector id
      {
        if ( pFocal->num_sectors >=  MAX_SECTORS_PER_TILE )
        {
          fprintf(stderr, "%s Prevented an increment beyond the max number of sectors: num_sectors=%d (max=%d)\n",
                  ERROR_PREFIX, pFocal->num_sectors+1, MAX_SECTORS_PER_TILE );
          return 1;
        }
        pFocal->Sector[pFocal->num_sectors].id = pFib->sector;
        pFib->sector = j; //set the pFib->sector to the index rather than the id
        pFocal->num_sectors ++;
      }
    }
  }



  if ( pFocal->num_sectors < 1 || pFocal->num_sectors > MAX_SECTORS_PER_TILE )
  {
    fprintf(stderr, "%s Bad number of sectors: %d (valid range=[%d:%d])\n",
            ERROR_PREFIX, pFocal->num_sectors, 1,MAX_SECTORS_PER_TILE );
    return 1;
  }
  
  //allocate some space for each fiber list
  for ( i=0;i< pFocal->num_sectors; i++)
  {
    if ( (pFocal->Sector[i].ppFib = (fiber_struct**) malloc (sizeof(fiber_struct*) * pFocal->num_fibers)) == 0 )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  
  if ( pFocal->flag_sectors_supplied_in_input_file == FALSE )
  {
    if      ( pFocal->num_sectors == 1  ) sectors_layout = SECTORS_LAYOUT_CIRC;
    else if ( pFocal->num_sectors <= 11 ) sectors_layout = SECTORS_LAYOUT_CIRC_RING;
    else                                 sectors_layout = SECTORS_LAYOUT_CIRC_RING_RING;

    fprintf(stdout, "%s Assigning fibers to %d sector%s (%d ring%s)\n",
            COMMENT_PREFIX, pFocal->num_sectors, (pFocal->num_sectors==1?"":"s"),
            sectors_layout, (sectors_layout==1?"":"s") );
  
    area_per_sector = 1.5*sqrt(3.)* SQR(pFocal->fiber_offaxis_max_mm) / (float) MAX(1,pFocal->num_sectors);
    //  fprintf(stdout, "%s Area_total=%g nsectors=%d, area per sector=%g\n",
    //          COMMENT_PREFIX, 1.5*sqrt(3.)* SQR(pFocal->fiber_offaxis_max_mm), pFocal->num_sectors, area_per_sector);

    radius_max[SECTORS_LAYOUT_CIRC] = FLT_MAX;
    radius_max[SECTORS_LAYOUT_CIRC_RING] =  FLT_MAX;
    radius_max[SECTORS_LAYOUT_CIRC_RING_RING] =  FLT_MAX;

    switch (sectors_layout)
    {
    case SECTORS_LAYOUT_CIRC :
      num_sectors_per_ring[SECTORS_LAYOUT_CIRC] = 1;
      radius_max[SECTORS_LAYOUT_CIRC] = pFocal->fiber_offaxis_max_mm * 1.1;
      angle_per_sector[SECTORS_LAYOUT_CIRC] = 360.0;
      break;
    case SECTORS_LAYOUT_CIRC_RING :
      num_sectors_per_ring[SECTORS_LAYOUT_CIRC] = 1;
      num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING] = pFocal->num_sectors - 1;
      radius_max[SECTORS_LAYOUT_CIRC] = sqrt(area_per_sector/M_PI);
      radius_max[SECTORS_LAYOUT_CIRC_RING] = pFocal->fiber_offaxis_max_mm * 1.1;
      angle_per_sector[SECTORS_LAYOUT_CIRC] = 360.0;
      angle_per_sector[SECTORS_LAYOUT_CIRC_RING] = 360.0/(float) num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING];
      break;
    case SECTORS_LAYOUT_CIRC_RING_RING :
      num_sectors_per_ring[SECTORS_LAYOUT_CIRC] = 1;
      num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING] = (pFocal->num_sectors - 1)/2;
      num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING_RING] = pFocal->num_sectors - num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING] - 1;
      radius_max[SECTORS_LAYOUT_CIRC] = sqrt(area_per_sector/M_PI);
      radius_max[SECTORS_LAYOUT_CIRC_RING] = sqrt(area_per_sector*(1.+(float)num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING])/M_PI);
      radius_max[SECTORS_LAYOUT_CIRC_RING_RING] = pFocal->fiber_offaxis_max_mm * 1.1;
      angle_per_sector[SECTORS_LAYOUT_CIRC] = 360.0;
      angle_per_sector[SECTORS_LAYOUT_CIRC_RING] = 360.0/(float) num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING];
      angle_per_sector[SECTORS_LAYOUT_CIRC_RING_RING] = 360.0/(float) num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING_RING];
      break;
    default: return 1; 
    }
  }

  if ( pFocal->flag_sectors_supplied_in_input_file == FALSE )
  {
  
    for ( i=0;i< pFocal->num_fibers; i++)
    {
      fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
      float radius = sqrt(SQR(pFib->Geom.x0)+SQR(pFib->Geom.y0));
      float angle  = 180.0 + atan2d(pFib->Geom.y0, pFib->Geom.x0);
      int sector;
      int   ring   = (radius <= radius_max[SECTORS_LAYOUT_CIRC] ? SECTORS_LAYOUT_CIRC :
                      (radius <= radius_max[SECTORS_LAYOUT_CIRC_RING] ? SECTORS_LAYOUT_CIRC_RING :
                       (radius <= radius_max[SECTORS_LAYOUT_CIRC_RING_RING] ? SECTORS_LAYOUT_CIRC_RING_RING : -1)));
      if ( angle >= 360.0 ) angle = 0.0;
      switch ( ring )
      {
      case SECTORS_LAYOUT_CIRC :
        sector = 0;
        break;
      case SECTORS_LAYOUT_CIRC_RING :
        sector = num_sectors_per_ring[SECTORS_LAYOUT_CIRC] +
          (int) (angle / angle_per_sector[SECTORS_LAYOUT_CIRC_RING]);
        break;
      case SECTORS_LAYOUT_CIRC_RING_RING :
        sector = num_sectors_per_ring[SECTORS_LAYOUT_CIRC] +
          num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING] +
          (int) (angle / angle_per_sector[SECTORS_LAYOUT_CIRC_RING_RING]);
        break;
      default:
        fprintf(stderr, "%s Bad ring number: %d (valid range=[%d:%d]) radius=%g (valid range=[%g:%g])\n",
                ERROR_PREFIX, ring, SECTORS_LAYOUT_CIRC,SECTORS_LAYOUT_CIRC_RING_RING,
                radius, 0.0, radius_max[SECTORS_LAYOUT_CIRC_RING_RING]);
        return 1; 
      }

      pFib->sector = sector;
    }
  }

  for ( i=0;i< pFocal->num_fibers; i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    pFocal->Sector[pFib->sector].ppFib[pFocal->Sector[pFib->sector].num_fibers] = (fiber_struct*) pFib;
    pFocal->Sector[pFib->sector].num_fibers ++;
    if ( pFib->is_empty ) pFocal->Sector[pFib->sector].num_fibers_switchable ++;
    else
    {
      if ( pFib->is_switchable ) pFocal->Sector[pFib->sector].num_fibers_switchable ++;
      else                       pFocal->Sector[pFib->sector].num_fibers_per_res[pFib->res] ++;
    }
    
    //    fprintf(stdout, "%s Fiber details %6d %8.3f %8.3f %8.1f %8.2f %6d %4d\n",
    //            COMMENT_PREFIX, i, pFib->Geom.x0, pFib->Geom.y0, radius, angle, sector, ring);
  }
  
//  fprintf(stdout, "%s %6s %4s %8s %8s\n", COMMENT_PREFIX, "sector", "ring", "nfibs", "fracfibs");
//  for(i=0;i<pFocal->num_sectors;i++)
//  {
//    int ring = (i < num_sectors_per_ring[SECTORS_LAYOUT_CIRC] ?  SECTORS_LAYOUT_CIRC :
//                (i < num_sectors_per_ring[SECTORS_LAYOUT_CIRC] + num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING] ?  SECTORS_LAYOUT_CIRC_RING :
//                 (i < num_sectors_per_ring[SECTORS_LAYOUT_CIRC] + num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING] + num_sectors_per_ring[SECTORS_LAYOUT_CIRC_RING_RING] ?  SECTORS_LAYOUT_CIRC_RING_RING : -1)));
//    fprintf(stdout, "%s %6d %4d %8d %8.5f\n", COMMENT_PREFIX, i, ring, num_fibers_per_sector[i], num_fibers_per_sector[i]/(float) MAX(1,pFocal->num_fibers));
//  }
//  fflush(stdout);

  //now tidy up the spare memory
  for ( i=0;i< pFocal->num_sectors; i++)
  {
    if ( pFocal->Sector[i].num_fibers > 0 )
    {
      if ( (pFocal->Sector[i].ppFib = (fiber_struct**) realloc (pFocal->Sector[i].ppFib, sizeof(fiber_struct*) * pFocal->Sector[i].num_fibers )) == 0 )
      {
        fprintf(stderr, "%s Error re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
    }
    else if ( pFocal->Sector[i].ppFib )
    {
      free (pFocal->Sector[i].ppFib);
      pFocal->Sector[i].ppFib = NULL;
    }
  }
  
  return 0;
}




int OpSim_positioner_add_fibers_to_offset_histos ( focalplane_struct *pFocal )
{
  int i;
  if ( pFocal == NULL ) return 1;

  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    if ( pFib->assign_flag == FIBER_ASSIGN_CODE_TARGET &&
         pFib->edge_flag   == POSITIONER_IS_NOT_AT_EDGE_OF_TILE &&
         pFib->is_empty    == FALSE )
    {
      twoDhisto_struct *pHisto = NULL;
      switch ( pFib->res ) //this will include switchable fibers
      {
      case RESOLUTION_CODE_LOW :   pHisto = (twoDhisto_struct*) &(pFocal->fiberXY_histo_lores);  break;
      case RESOLUTION_CODE_HIGH :  pHisto = (twoDhisto_struct*) &(pFocal->fiberXY_histo_hires);  break;
      case RESOLUTION_CODE_DUAL :
        if (  pFib->pObj != NULL )
        {
          switch ( pFib->pObj->res )
          {
          case RESOLUTION_CODE_LOW :   pHisto = (twoDhisto_struct*) &(pFocal->fiberXY_histo_lores);  break;
          case RESOLUTION_CODE_HIGH :  pHisto = (twoDhisto_struct*) &(pFocal->fiberXY_histo_hires);  break;
          default :  break;
          }
        }
      default :  break;
      }

      if ( pHisto )
      {
        if ( twoDhisto_struct_add_count ((twoDhisto_struct*) pHisto,
                                         (double) pFib->Geom.x - pFib->Geom.x0,
                                         (double) pFib->Geom.y - pFib->Geom.y0 )) return 1;
      }

      //also record the switchable fibers behaviour separately
      if ( pFib->is_switchable )
      {
        if ( twoDhisto_struct_add_count ((twoDhisto_struct*) &(pFocal->fiberXY_histo_swres),
                                         (double) pFib->Geom.x - pFib->Geom.x0,
                                         (double) pFib->Geom.y - pFib->Geom.y0 )) return 1;        
      }
      
    }
  }
  return 0;
}


int OpSim_positioner_record_fiber_assignment_stats (focalplane_struct *pFocal )
{
  int i;
  if ( pFocal == NULL ) return 1;
  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    if ( pFib->is_empty == FALSE )
    {
      if ( pFib->status == FIBER_STATUS_FLAG_NOMINAL )
      {
        switch ( pFib->assign_flag )
        {
        case  FIBER_ASSIGN_CODE_TARGET :
          pFib->total_assignments_to_target ++;
          if      ( pFib->res == RESOLUTION_CODE_LOW  ) pFib->total_assignments_as_lores ++;
          else if ( pFib->res == RESOLUTION_CODE_HIGH ) pFib->total_assignments_as_hires ++;
          else if ( pFib->res == RESOLUTION_CODE_DUAL )
          {
            if ( pFib->pObj )
            {
              if      ( pFib->pObj->res == RESOLUTION_CODE_LOW)  pFib->total_assignments_as_lores ++;
              else if ( pFib->pObj->res == RESOLUTION_CODE_HIGH) pFib->total_assignments_as_hires ++;
            }
          }
          break ;
        case  FIBER_ASSIGN_CODE_SKY    :
          pFib->total_assignments_to_sky    ++;
          if      ( pFib->res == RESOLUTION_CODE_LOW  ) pFib->total_assignments_as_lores ++;
          else if ( pFib->res == RESOLUTION_CODE_HIGH ) pFib->total_assignments_as_hires ++;
          else if ( pFib->res == RESOLUTION_CODE_DUAL )
          {
            pFib->total_assignments_as_lores ++;
            pFib->total_assignments_as_hires ++;
          }
          break ;
        case  FIBER_ASSIGN_CODE_NONE   :
          pFib->total_assignments_to_spare  ++;
          break ;
        case FIBER_ASSIGN_CODE_PITARGET :
          //no need to record this in the total_assignments_as_??res counters
          pFib->total_assignments_to_PItarget  ++;
          break;
        }
      }
      else
      {
        pFib->total_num_tiles_spent_in_broken_state ++;
      }
      pFib->number_of_candidate_targets += pFib->nobjects;
      pFib->number_of_candidate_targets_weighted += pFib->nobjects_weighted;
    }
  }
  return 0;
}



int OpSim_positioner_assign_fibers_to_be_sky_fibers (focalplane_struct *pFocal, survey_struct *pSurvey )
{
  int i;

  fiber_struct **ppFib = NULL;
  //first work out how many sky fibers to allocate in each sector
  int r,s;
  int n_sky_fibs_required[NRESOLUTION_CODES];
  int n_sky_fibs_assigned[NRESOLUTION_CODES];
  int n_sky_fibs_assigned_per_sector[NRESOLUTION_CODES][MAX_SECTORS_PER_TILE];
  int n_sky_fibs_to_assign_per_sector[NRESOLUTION_CODES][MAX_SECTORS_PER_TILE];
  if ( pFocal == NULL || pSurvey == NULL ) return 1;
  
  n_sky_fibs_required[RESOLUTION_CODE_LOW] = pSurvey->minimum_sky_fibers_low_res;
  n_sky_fibs_required[RESOLUTION_CODE_HIGH] = pSurvey->minimum_sky_fibers_high_res;
  for ( r=RESOLUTION_CODE_LOW; r<=RESOLUTION_CODE_HIGH;r++)
  {
    int n_per_sector = MAX(pFocal->num_fibers_reserved_per_sector[r],
                           (n_sky_fibs_required[r] / pFocal->num_sectors));
    int n_allocated = 0;
    for( s=0;s<pFocal->num_sectors;s++)
    {
      n_sky_fibs_assigned_per_sector[r][s] = 0;
      n_sky_fibs_to_assign_per_sector[r][s] = n_per_sector;
      n_allocated += n_per_sector;
    }
    //check for any more sky fibs (because of rounding)
    while ( n_allocated < n_sky_fibs_required[r] )
    {
      s = MIN(pFocal->num_sectors-1,(int) floor(drand48()*pFocal->num_sectors));
      n_sky_fibs_to_assign_per_sector[r][s] ++;
      n_allocated ++;
    }
  }
  
  //assign some memory for shuffling
  if ( (ppFib = (fiber_struct**) malloc (sizeof(fiber_struct*) * pFocal->num_fibers)) == 0 )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;      
  }
  
  
  //now assign sky fibers evenly between sectors 
  for (s=0;s<pFocal->num_sectors;s++)
  {
    sector_struct *pSect = (sector_struct*) &(pFocal->Sector[s]);
    //shuffle the fibers in this sector - shuffle a copy to avoid disturbing original sector_struct
    for (i=0;i<pSect->num_fibers;i++) ppFib[i] = (fiber_struct*) pSect->ppFib[i];
    if ( util_Fisher_Yates_shuffle_array((unsigned long) pSect->num_fibers, (size_t) sizeof(fiber_struct*), (void*) ppFib))
    {
      fprintf(stderr, "%s Error shuffling sector->fiber arrays\n",ERROR_PREFIX);
      return 1;
    }
    
    for (i=0;i<pSect->num_fibers;i++)
    {
      fiber_struct *pFib = (fiber_struct*) ppFib[i];
      if ( pFib->assign_flag == FIBER_ASSIGN_CODE_NONE &&
           pFib->is_empty    == FALSE)
      {
        if ( n_sky_fibs_assigned_per_sector[pFib->res][s] < n_sky_fibs_to_assign_per_sector[pFib->res][s] ) 
        {
          pFib->assign_flag = FIBER_ASSIGN_CODE_SKY;
          if ( pFib->res == RESOLUTION_CODE_DUAL )
          {
            n_sky_fibs_assigned_per_sector[RESOLUTION_CODE_LOW][s] ++;
            n_sky_fibs_assigned[RESOLUTION_CODE_LOW] ++;            
            n_sky_fibs_assigned_per_sector[RESOLUTION_CODE_HIGH][s] ++;
            n_sky_fibs_assigned[RESOLUTION_CODE_HIGH] ++;            
          }
          else
          {
            n_sky_fibs_assigned_per_sector[pFib->res][s] ++;
            n_sky_fibs_assigned[pFib->res] ++;
          }
        }
      }
    }
  }
  if ( ppFib ) free(ppFib) ; ppFib = NULL;
  return 0;
}


//int OpSim_positioner_assign_fibers_to_be_PItarget_fibers (focalplane_struct *pFocal, survey_struct *pSurvey )
//{
//  int i;
//  fiber_struct **ppFib = NULL;
//  if ( pFocal == NULL || pSurvey == NULL ) return 1;
//  return 0;
//}



//this is an alternative method to test if a
//coordinate is inside the tile outer perimeter,
//as defined by the set of straight
//lines connecting the tile extended vertices.
//requires that the ext_vertices are sorted correctly
//return >0 if true
//return 0  if false
//return <0 if encounter an error
int OpSim_positioner_test_if_inside_tile_outer_perimeter (focalplane_struct *pFocal, double x, double y )
{
  if ( pFocal == NULL ) return -1;

  return geom_test_if_point_inside_polygon ((int) pFocal->num_ext_vertex,
                                            (double*) pFocal->ext_vertex_x, (double*) pFocal->ext_vertex_y,
                                            (double) x, (double) y);
}


//test if a coordinate is inside the tile inner perimeter defined by the line connecting the
//tile vertices
//return >0 if true
//return 0  if false
//return <0 if encounter an error
int OpSim_positioner_test_if_inside_tile_inner_perimeter (focalplane_struct *pFocal, double x, double y )
{
  if ( pFocal == NULL ) return -1;

  return geom_test_if_point_inside_polygon ((int) pFocal->num_vertex,
                                            (double*) pFocal->vertex_x, (double*) pFocal->vertex_y,
                                            (double) x, (double) y);
}

//test if a coordinate is inside the tile inner perimeter defined by the line connecting the
//tile vertices
//return >0 if true
//return 0  if false
//return <0 if encounter an error
int OpSim_positioner_test_if_near_tile_inner_perimeter (focalplane_struct *pFocal, double x, double y )
{
  if ( pFocal == NULL ) return -1;

  return geom_test_if_point_near_edges_of_polygon ((int) pFocal->num_vertex,
                                                   (double*) pFocal->vertex_x, (double*) pFocal->vertex_y,
                                                   (double) pFocal->patrol_radius_max_mm,
                                                   (double) x, (double) y);
}

//test if a coordinate is inside a tile (either inner or outer perimeters)
//return 1 if inside inner perimeter
//return 2 if near perimeter (but not inside inner perimeter)
//return 0  if outside tile
//return <0 if encounter an error
int OpSim_positioner_test_if_inside_tile (focalplane_struct *pFocal, double x, double y )
{
  int result = 0;
  if ( pFocal == NULL ) return -1;

  if ( (result = geom_test_if_point_inside_polygon ((int) pFocal->num_vertex,
                                                    (double*) pFocal->vertex_x, (double*) pFocal->vertex_y,
                                                    (double) x, (double) y)) != 0)
    return result;

  if ( (result = geom_test_if_point_near_edges_of_polygon ((int) pFocal->num_vertex,
                                                           (double*) pFocal->vertex_x, (double*) pFocal->vertex_y,
                                                           (double) pFocal->patrol_radius_max_mm,
                                                           (double) x, (double) y) != 0))
    return result;

  return 0;
}



//set all the switchable fibers to the given resolution code
int OpSim_positioner_set_switchable_fibers_to_res (focalplane_struct *pFocal, utiny resolution_code )
{
  int i;
  int num_lo = 0;
  int num_hi = 0;
  int num_dual = 0;
  if ( pFocal == NULL ) return 1;
  if ( (utiny) resolution_code != (utiny) RESOLUTION_CODE_LOW &&
       (utiny) resolution_code != (utiny) RESOLUTION_CODE_HIGH ) return 1;

  for(i=0;i<MAX_SECTORS_PER_TILE;i++)
  {
    sector_struct *pSector = (sector_struct*) &(pFocal->Sector[i]);
    pSector->num_fibers_per_res[RESOLUTION_CODE_LOW] = 0;
    pSector->num_fibers_per_res[RESOLUTION_CODE_HIGH] = 0;
    pSector->num_fibers_per_res[RESOLUTION_CODE_DUAL] = 0;
  }

  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[i]);
    if ( pFib->is_empty == FALSE )
    {
      if ( pFib->is_switchable )
      {
        pFib->res = (utiny) resolution_code;
      }
      switch ( pFib->res )
      {
      case RESOLUTION_CODE_LOW  : num_lo ++;  break;
      case RESOLUTION_CODE_HIGH : num_hi ++;   break;
      case RESOLUTION_CODE_DUAL : num_dual ++; break;
      default : break;
      }
      if ( pFib->sector >= 0 && pFib->sector < pFocal->num_sectors )
        pFocal->Sector[pFib->sector].num_fibers_per_res[pFib->res] ++;
    }
  }
  pFocal->num_fibers_lo   = num_lo;
  pFocal->num_fibers_hi   = num_hi;
  pFocal->num_fibers_dual = num_dual;


  return 0;
}


