#!/bin/tcsh -f

#this is a better way to do things - can now step throuh in julian date 
#and get moon position throughout the survey.

#xJ JulianDate  - set teh JD
# m             - print planet positions (inc moon)
# a             - night's almanac

#step through a year, hour by hour, to look at observable time available

##JD = 2458120.0 is 12 noon on Jan 1st 2018 UT 
set JD_START = 2458120
##JD = 2458485.00000000 is 12 noon on Jan 1st 2019 UT 
#set JD_START = 2458485 


#@ N_DAYS = 365 * 5
#set N_DAYS = 10
#set N_DAYS = 100
#this is long enough to allow for a Jan 1st 2019 start
set N_DAYS = 2500
set STEP = 0.05

#set TELE_LIST = ( VISTA NTT )
set TELE_LIST = ( VISTA )

foreach TELE ( $TELE_LIST ) 
  
  if ( $TELE == "NTT" ) then
    set SITE_CODE = "e"
  else if ( $TELE == "VISTA" ) then
    set SITE_CODE = "v"
  else
    exit 1
  endif
   
  set LOGFILE = skycalc_out2_${TELE}.log
  if ( -e $LOGFILE ) rm $LOGFILE 
  if ( -e ${LOGFILE}.gz ) rm ${LOGFILE}.gz 

  #write the full skycalc script first
  gawk -v site_code="$SITE_CODE" -v JD_start=$JD_START -v n_days=$N_DAYS -v step=$STEP 'BEGIN{print site_code; for(d=0;d<=n_days;d+=step){printf("xJ %.3f\nxZ\na\nm\n", JD_start+d)}; print "Q"; exit}' >  run.txt
#  gawk -v site_code="$SITE_CODE" -v JD_start=$JD_START -v n_days=$N_DAYS -v step=$STEP 'BEGIN{print site_code; for(d=0;d<=n_days;d+=step){printf("xJ %.3f\nxZ\na\n=\nm\n", JD_start+d)}; print "Q"; exit}' >  run.txt

  skycalc < run.txt >>& $LOGFILE

  rm run.txt

#  gawk '/Local midnight /{jd=$NF} /Moon at civil midnight/ {moon_illum=$NF} /The sun is down for/ {twi_to_twi=$8} /Goodbye/ {printf("%12s %12s %12s\n",jd,moon_illum,twi_to_twi)} ' ${LOGFILE}  > ${LOGFILE:r}_proc.txt

  echo "Now converting to a useable format: ${LOGFILE} > ${LOGFILE:r}_proc.txt" 

#  gawk 'BEGIN {printf("#%-13s %14s %14s %14s %10s %10s %12s %12s %12s %12s  %12s %12s %12s %12s\n","JDnow", "JD_midnight", "JD_eve_twi","JD_morn_twi","twi2twi", "moon_illum", "moon_ra", "moon_dec", "moon_alt", "moon_az", "sun_ra", "sun_dec", "sun_alt", "sun_az")} $0~/^Julian date:/{jd=$3} $0~/^Moon   :/ {moon_ra=($3+$4/60.)*15.; moon_dec=$5 + $6/(substr($5,1,1)=="-"?-60.:-60.);moon_alt=$10;moon_az=$11;} $0~/^Local midnight/ {jd_mid=$NF} /Moon at civil midnight/ {moon_illum=$NF;} /The sun is down for/ {twi2twi=$8} $0~/^Evening twilight:/{jd_etwi = jd_mid + (($3 + $4/60.0)/24.) - 1.0;} $0~/^Morning twilight:/{jd_mtwi = jd_mid + (($3 + $4/60.0)/24.)} $0~/^Sun    :/ {sun_ra=($3+$4/60.)*15.;sun_dec=$5 + $6/(substr($5,1,1)=="-"?-60.:-60.);sun_alt=$10;sun_az=$11} /Pluto/ {printf("%14.6f %14.6f %14.6f %14.6f %10.4f %10.5f %12.7f %+12.7f %12.4f %12.4f  %12.7f %+12.7f %12.4f %12.4f\n",jd,jd_mid,jd_etwi,jd_mtwi,twi2twi,moon_illum,moon_ra,moon_dec,moon_alt,moon_az,sun_ra,sun_dec,sun_alt,sun_az)} ' ${LOGFILE} > ${LOGFILE:r}_proc.txt

  gawk 'BEGIN {printf("#%-13s %14s %14s %14s %10s %10s %12s %12s %12s %12s  %12s %12s %12s %12s  %12s %12s\n","JDnow", "JD_midnight", "JD_eve_twi","JD_morn_twi","twi2twi", "moon_illum", "moon_ra", "moon_dec", "moon_alt", "moon_az", "sun_ra", "sun_dec", "sun_alt", "sun_az", "zen_ra", "zen_dec")} $0~/^Julian date:/{jd=$3} $0~/^Moon   :/ {moon_ra=($3+$4/60.)*15.; moon_dec=$5 + $6/(substr($5,1,1)=="-"?-60.:+60.);moon_alt=$10;moon_az=$11;} $0~/^Local midnight/ {jd_mid=$NF} /Moon at civil midnight/ {moon_illum=$NF;} /The sun is down for/ {twi2twi=$8} $0~/^Evening twilight:/{jd_etwi = jd_mid + (($3 + $4/60.0)/24.) - 1.0;} $0~/^Morning twilight:/{jd_mtwi = jd_mid + (($3 + $4/60.0)/24.)} $0~/^Sun    :/ {sun_ra=($3+$4/60.)*15.;sun_dec=$5 + $6/(substr($5,1,1)=="-"?-60.:+60.);sun_alt=$10;sun_az=$11} /COORDINATES SET TO ZENITH/ {zen_ra=15.0*($5 + ($6 + $7/60.0)/60.0);sign=(substr($8,1,1)=="-"?-1.0:+1.0);zen_dec=($8+sign*(($9+$10/60.)/60.0))} /Pluto/ {printf("%14.6f %14.6f %14.6f %14.6f %10.4f %10.5f %12.7f %+12.7f %12.4f %12.4f  %12.7f %+12.7f %12.4f %12.4f  %12.7f %+12.7f\n",jd,jd_mid,jd_etwi,jd_mtwi,twi2twi,moon_illum,moon_ra,moon_dec,moon_alt,moon_az,sun_ra,sun_dec,sun_alt,sun_az,zen_ra,zen_dec)} ' ${LOGFILE} > ${LOGFILE:r}_proc.txt

gzip -v ${LOGFILE} 
#  gawk 'BEGIN {printf("#%-13s %14s %14s %14s %10s %10s %12s %12s %12s %12s  %12s %12s %12s %12s  %12s %12s\n","JDnow", "JD_midnight", "JD_eve_twi","JD_morn_twi","twi2twi", "moon_illum", "moon_ra", "moon_dec", "moon_alt", "moon_az", "sun_ra", "sun_dec", "sun_alt", "sun_az", "zen_ra", "zen_dec")} $0~/^Julian date:/{jd=$3} $0~/^Moon: / {moon_ra=($2+($3 + $4/60.)/60.)*15.; moon_dec=$5 + $6/(substr($5,1,1)=="-"?-60.:-60.);moon_alt=$8;moon_az=$10; moon_illum=$11} $0~/^Local midnight/ {jd_mid=$NF} /The sun is down for/ {twi2twi=$8} $0~/^Evening twilight:/{jd_etwi = jd_mid + (($3 + $4/60.0)/24.) - 1.0;} $0~/^Morning twilight:/{jd_mtwi = jd_mid + (($3 + $4/60.0)/24.)} $0~/^Sun    :/ {sun_ra=($3+$4/60.)*15.;sun_dec=$5 + $6/(substr($5,1,1)=="-"?-60.:-60.);sun_alt=$10;sun_az=$11} /COORDINATES SET TO ZENITH/ {zen_ra=15.0*($5 + ($6 + $7/60.0)/60.0);sign=(substr($8,1,1)=="-"?-1.0:+1.0);zen_dec=($8+sign*(($9+$10/60.)/60.0))} /Pluto/ {printf("%14.6f %14.6f %14.6f %14.6f %10.4f %10.5f %12.7f %+12.7f %12.4f %12.4f  %12.7f %+12.7f %12.4f %12.4f  %12.7f %+12.7f\n",jd,jd_mid,jd_etwi,jd_mtwi,twi2twi,moon_illum,moon_ra,moon_dec,moon_alt,moon_az,sun_ra,sun_dec,sun_alt,sun_az,zen_ra,zen_dec)} ' ${LOGFILE} > ${LOGFILE:r}_proc.txt

end



exit
