#!/bin/tcsh -f

#construct a tarball of the 4FS_OpSim code and documentation

#expect this to be run from the correct directory
set VERSION = "v1.0.0"

if ( $#argv == 1 ) then
  set VERSION = "$argv[1]"
else
  echo "Usage: build_tarball_for_release.csh 'version-string'"
  exit 1
endif

set OUTDIR = ${HOME}/4most/code/releases

if ( ! -d $OUTDIR ) mkdir $OUTDIR

if ( ! -d $OUTDIR ) then
  echo "Problem creating/finding directory: $OUTDIR"
  exit 1
endif

set PWD = `pwd`
if ( "${PWD:t}" == "OpSim" && "${PWD:h:t}" == "4most" ) cd ../../

if ( ! -d 4most/OpSim ) then
  echo "Problem finding directory: 4most/OpSim"
  exit 1
endif


set OUTFILE = "4FS_OpSim_src_${VERSION}.tar.gz"

echo "Building ${OUTDIR}/${OUTFILE}"
tar -czf ${OUTDIR}/${OUTFILE} --exclude="*~" --exclude="CVS" --exclude="*.o" --exclude="4FS_OpSim" --no-recursion 4most/OpSim/* 4most/OpSim/doc/4FS_OpSim_user_manual.tex 4most/OpSim/doc/4FS_OpSim_user_manual.pdf


exit




