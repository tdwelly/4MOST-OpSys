//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_telescope_site_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.14 $"
//#define CVS_DATE     "$Date: 2015/05/26 15:33:14 $"
//--    Note: Definitions are in the corresponding header file OpSim_telescope_site_lib.h
//--    Description:
//--      This module is used for functions related to telescope parameters and weather
//--      conditions at the telescope site etc.

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "utensils_lib.h"
#include "OpSim_telescope_site_lib.h"

#define ERROR_PREFIX   "#-OpSim_telescope_site_lib.c:Error  :"
#define WARNING_PREFIX "#-OpSim_telescope_site_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_telescope_site_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_telescope_site_lib.c:DEBUG  :"


////////////////////////////////////////////////////////////////////////
//functions


int OpSim_telescope_site_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}


int telescope_struct_init (telescope_struct **ppTele)
{
  if ( ppTele == NULL ) return 1;
  if ( *ppTele == NULL )
  {
    //make space for the struct
    if ( (*ppTele = (telescope_struct*) malloc(sizeof(telescope_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  //init the data values
  strncpy((*ppTele)->code_name, "", STR_MAX);
  return 0;
}

int telescope_struct_free (telescope_struct **ppTele)
{
  if ( ppTele == NULL ) return 1;

  if ( *ppTele )
  {
    // free the telescope struct itself
    free(*ppTele);
    *ppTele = NULL;
  }
  return 0;
}



///////////////////////////////////////////////////////////////////
int telescope_set_site (telescope_struct *pTele, const char *str_tele )
{
  if ( pTele == NULL ) return 1;

  {
    const char *plist_names[]  = {              "VISTA",              "NTT",            "UKST" };
    const int   plist_values[] = {TELESCOPE_CODE_VISTA, TELESCOPE_CODE_NTT, TELESCOPE_CODE_UKST};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    
    if (util_select_option_from_string(len, plist_names, plist_values, str_tele,  &(pTele->code)))
    {
      fprintf(stderr, "%s  I do not understand this TELESCOPE.CODE_NAME: >%s<\n",
              ERROR_PREFIX, str_tele);
      fflush(stderr);
      return 1;
    }
  }
    
  switch ( pTele->code)
  {
  case TELESCOPE_CODE_VISTA :
    strncpy(pTele->str_longitude, "70:23:51.0 W", STR_MAX); //from skycalc + web
    strncpy(pTele->str_latitude, "-24:36:57.0", STR_MAX);
    pTele->longitude_deg =  (double) 289.5972; //from ING star tools - can improve 
    pTele->latitude_deg  =  (double) -24.6253; //TODO - improve Site coordinates
    pTele->altitude = (double) 2635.0;
    pTele->extinction_coeff    = (float) TELESCOPE_SITE_EXTINCTION_VISTA;  
    pTele->dark_sky_brightness = (float) TELESCOPE_SITE_DARK_SKY_BRIGHTNESS_VISTA; 
    pTele->Bzen = util_magsqarcsec_to_nanoLambert(pTele->dark_sky_brightness);
    break;
  case TELESCOPE_CODE_NTT :
    strncpy(pTele->str_longitude, "70:43:54.272 W", STR_MAX); //from skycalc + web
    strncpy(pTele->str_latitude, "-29:15:18.440", STR_MAX);
    pTele->longitude_deg =  (double) 289.270; //from ING star tools - can improve
    pTele->latitude_deg  =  (double) -29.2567;//TODO - improve Site coordinates
    pTele->altitude =  (double) 2375.0;
    pTele->extinction_coeff    = (float) TELESCOPE_SITE_EXTINCTION_NTT; 
    pTele->dark_sky_brightness = (float) TELESCOPE_SITE_DARK_SKY_BRIGHTNESS_NTT;   
    pTele->Bzen = util_magsqarcsec_to_nanoLambert(pTele->dark_sky_brightness);
    break;
  case TELESCOPE_CODE_UKST :
    //http://rsaa.anu.edu.au/observatories/telescopes/united-kingdom-schmidt-telescope-ukst
    strncpy(pTele->str_longitude, "+149:04:45.0 E", STR_MAX); //from skycalc + web
    strncpy(pTele->str_latitude, "-31:16:23.88", STR_MAX);
    pTele->longitude_deg =  (double) 149.0617; //from ING star tools - can improve
    pTele->latitude_deg  =  (double) -31.2733;//TODO - improve Site coordinates
    pTele->altitude =  (double) 1145.0;
    pTele->extinction_coeff    = (float) TELESCOPE_SITE_EXTINCTION_UKST; 
    pTele->dark_sky_brightness = (float) TELESCOPE_SITE_DARK_SKY_BRIGHTNESS_UKST;   
    pTele->Bzen = util_magsqarcsec_to_nanoLambert(pTele->dark_sky_brightness);
    break;
  default :
    fprintf(stderr, "%s  I do not understand this TELESCOPE.CODE_NAME: >%s<\n",
            ERROR_PREFIX, str_tele);
    return 1;
    break;
  }
  pTele->sin_latitude = sind(pTele->latitude_deg);
  pTele->cos_latitude = cosd(pTele->latitude_deg);
  
  fprintf(stdout, "%s Set telescope site to: %s \n", COMMENT_PREFIX, str_tele);
  return 0;
}

////////////////////////////////////////////////////////////////




//this is same power law used in VISTA P2PP
// seeing(airmass) = seeing(zenith)*airmass^0.6
float weather_seeing_at_airmass (float seeing_zen, float airmass)
{
  if ( airmass <= (float) 1.0 )
    return (float) seeing_zen;
  else
    return (float) (seeing_zen * powf(airmass, (float) 0.6));
}

//this is same power law used in VISTA P2PP
//  seeing(zenith) = seeing(airmass)*airmass^-0.6
float weather_seeing_at_zenith (float seeing_airmass, float airmass)
{
  if ( airmass <= (float) 1.0 )
    return (float) seeing_airmass;
  else
    return (float) (seeing_airmass * powf(airmass, (float) -0.6));
}

//calc the probability of having IQ or better at a given airmass 
float  weather_IQ_prob (environment_struct *pEnviron, float airmass, float IQ)
{
  int i = 0;
  float seeing_zen;
  if ( pEnviron == NULL ) return -1.0;
  seeing_zen = (float) weather_seeing_at_zenith(IQ, airmass); 

  if ( seeing_zen < pEnviron->seeing_thresh_low[0] )
    return (float) 0.0;

  while (pEnviron->seeing_thresh_high[i] < seeing_zen &&
          i < (pEnviron->seeing_num_grades - 1)) i++;

  return (float) pEnviron->seeing_cumulative_fraction[i];
}

//assume that random number generator has been seeded
//and that the pEnviron->seeing parameters have been set up
int weather_random_zenith_IQ (environment_struct *pEnviron, float *pResult)
{
  float rand;
  int i = 0;
  if ( pEnviron == NULL || pResult == NULL ) return 1;
  rand = (float) drand48();
  
  for ( i=0; i< pEnviron->seeing_num_grades; i++)
  {
    if (rand <= pEnviron->seeing_cumulative_fraction[i]) break;
  }
  if ( i >= pEnviron->seeing_num_grades )
  {
    *pResult = (float) -1.0* rand;
    return 1; // the cumulative seeing grades must add up to 1
  }
  *pResult = (float) (pEnviron->seeing_thresh_low[i] +
                      drand48()*(pEnviron->seeing_thresh_high[i] - pEnviron->seeing_thresh_low[i])) ;
  return 0;
}

//
int weather_random_cloud (environment_struct *pEnviron, int *pCode, const char **ppCodename)
{
  float rand;
  if ( pEnviron == NULL ) return 1;
  rand = (float) drand48();
  if ( rand <= pEnviron->cloud_fraction_photometric )
  {
    if ( pCode      ) *pCode      = (int)         CLOUD_COVER_CODE_PHOTOMETRIC;
    if ( ppCodename ) *ppCodename = (const char*) CLOUD_COVER_NAME_PHOTOMETRIC;
  }
  else if ( rand <= pEnviron->cloud_fraction_photometric + 
            + pEnviron->cloud_fraction_clear )
  {
    if ( pCode      ) *pCode      = (int)         CLOUD_COVER_CODE_CLEAR;
    if ( ppCodename ) *ppCodename = (const char*) CLOUD_COVER_NAME_CLEAR;
  }
  else if ( rand <= pEnviron->cloud_fraction_photometric
            + pEnviron->cloud_fraction_clear
            + pEnviron->cloud_fraction_thin )
  {
    if ( pCode      ) *pCode      = (int)         CLOUD_COVER_CODE_THIN;
    if ( ppCodename ) *ppCodename = (const char*) CLOUD_COVER_NAME_THIN;
  }
  else if ( rand <= pEnviron->cloud_fraction_photometric
            + pEnviron->cloud_fraction_clear
            + pEnviron->cloud_fraction_thick
            + pEnviron->cloud_fraction_thin )
  {
    if ( pCode      ) *pCode      = (int)         CLOUD_COVER_CODE_THICK;
    if ( ppCodename ) *ppCodename = (const char*) CLOUD_COVER_NAME_THICK;
  }
  else
  {
    if ( pCode      ) *pCode      = (int)         CLOUD_COVER_CODE_BAD;
    if ( ppCodename ) *ppCodename = (const char*) CLOUD_COVER_NAME_BAD;
  }
  return 0;
}


//
//
////calculate sky brightness at a given position on the sky at a given time
//float weather_calc_sky_brightness (timeline_struct *pTime, double ra, double dec)
//{
//  //from my calc_sky_brightness.plot file
//  //Istar(alpha) = (10.0**(-0.4*(3.84+0.026*(alpha>0.?alpha:-alpha) + 4e-9*(alpha**4))))
//  //f(rho)       = ((10.0**5.36)*(1.06 + cos(rho)**2) + 10.**(6.15 - rho/40.0))
//  //X(Z)         = ((1. - 0.96*(sin(Z)**2))**-0.5)
//  //B_0(Zsky)    = (Bzen*(10.**(-0.4*k*(X(Zsky)-1.)))*X(Zsky))
//  //B_moon(alpha,rho,Zmoon,Zsky) = (f(rho)*Istar(alpha)*(10.**(-0.4*k*X(Zmoon)))*(1. - 10.**(-0.4*k*X(Zsky))))
//  //delta_V_moon(alpha,rho,Zmoon,Zsky) = (-2.5*log10((B_moon(alpha,rho,Zmoon,Zsky) + B_0(Zsky))/B_0(Zsky)))
//  //delta_V_moon2(alpha,rho,Zmoon,Zsky) = (Zsky >= 90. ? +1.0 : delta_V_moon(alpha,rho,Zmoon,Zsky) )
//
//  float result;
//  //moon phase (0=full, 180=new) measured as angle between earth and sun as seen from the moon
//  //good approximation of alpha = 180deg - sun_moon_angle_measured_from_earth
//  float alpha;
//  //the sky-moon separation (degrees)
//  float rho; 
//  //a meaure of the brightness of the moon for this moon phase (mag units)
//  float Istar;
//  //a meaure of the scattered flux - a fractional term
//  float f_rho;
//  //airmasses for the moon and the sky positions
//  float X_Zmoon, X_Zsky;
//  //this gives the dark sky brightness at the sky position, given a zenith dark sky brightness  (ie no moon contribution)
//  // - in linear units (nonolamberts)
//  float B_0_Zsky;
//  //this is the sky brightness due to the moon only
//  // - in linear units (nonolamberts)
//  float B_moon;
//
//  float Zsky; //zenith distance of sky, degrees
//
//  telescope_struct *pTele = NULL;  //just a shortcut to the telescope structure pointer in the Tile structure
//  
//  if ( pTime == NULL ) return 1;
//  pTele = (telescope_struct*) pTime->pTele;
//  if ( pTele == NULL ) return 1;
//
//
//  Zsky         = geom_arc_dist(pTime->Rnow.ra_zen,pTime->Rnow.dec_zen,ra,dec);
//  X_Zsky       = util_airmass_kriscicunas(Zsky);
//  B_0_Zsky     = pTele->Bzen*X_Zsky*pow(10.0, -0.4*pTele->extinction_coeff*(X_Zsky - 1.));
////  fprintf(stdout, "Debug: ra,dec           = %9.3f %9.3f\n",ra,dec);
////  fprintf(stdout, "Debug: ra_zen,dec_zen   = %9.3f %9.3f\n",pTime->Rnow.ra_zen,pTime->Rnow.dec_zen);
////  fprintf(stdout, "Debug: Zsky             = %9.3f\n",Zsky);
////  fprintf(stdout, "Debug: X_Zsky           = %9.3f\n",X_Zsky);
////  fprintf(stdout, "Debug: Bzen             = %9.3f nL\n",pTele->Bzen);
////  fprintf(stdout, "Debug: extinction_coeff = %9.3f \n",pTele->extinction_coeff);
////  fprintf(stdout, "Debug: B_0_Zsky         = %9.3f nL\n",B_0_Zsky);
//
//  if ( pTime->Rnow.moon.Z < 108.0 )  //consider moon contribution only if above -18deg elevation
//  {
//    //so, we calculate alpha from the moon and sun positions
//    alpha        = (float) 180.0 - geom_arc_dist(pTime->Rnow.moon.ra,pTime->Rnow.moon.dec, pTime->Rnow.sun.ra,pTime->Rnow.sun.dec);
//    //and rho from the moon and sky positions
//    rho          = (float) geom_arc_dist(pTime->Rnow.moon.ra,pTime->Rnow.moon.dec,ra,dec);
//    // intensity of the moon light 
//    Istar        = (float) pow(10.0, -0.4*(3.84 + 0.026*fabs(alpha) + 4e-9*(SQR(SQR(alpha)))));
//    // scattering fraction
//    // 10^5.36 = 229086.765
//    f_rho        = 229086.765*(1.06 + SQR(cosd(rho))) + pow(10., 6.15 - rho*0.025);
//    
//    X_Zmoon      = util_airmass_kriscicunas(pTime->Rnow.moon.Z);
//
//    B_moon =  f_rho * Istar*
//      pow(10.0, -0.4*pTele->extinction_coeff*X_Zmoon) *
//      (1.0 - pow(10.0,-0.4*pTele->extinction_coeff*X_Zsky));
//  
//    //    result = pTele->dark_sky_brightness + -2.5*log10((B_moon + B_0_Zsky)/B_0_Zsky);
//    result = util_nanoLambert_to_magsqarcsec(B_0_Zsky + B_moon);
//  }
//  else
//  {//the moon is down, so use the 
//    result = util_nanoLambert_to_magsqarcsec(B_0_Zsky);
//  }
//  return result;
//}
//
////very abbreviated version of the part of the above that is dependent on moon distance
//float weather_calc_relative_sky_brightness (timeline_struct *pTime, double moon_dist)
//{
//  return (4.0e-6*(2.29e5*(1.06 + SQR(cosd(moon_dist))) + pow(10., 6.15-0.025*moon_dist)));
//}

