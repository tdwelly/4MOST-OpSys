//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_fields_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//--    Note: Definitions are in the corresponding header file OpSim_fields_lib.h
//--    Description:
//--      This module contains functions mainly focussed on operations related to fields
//--      and the objects they contain

#include <chealpix.h>   //needed for nice map making
#include <assert.h>   //needed for debug

#include "OpSim_fields_lib.h"
#include "OpSim_OB_lib.h"   //for ESO_OT mode survey (not yet operational)


#define ERROR_PREFIX   "#-OpSim_fields_lib.c:Error  :"
#define WARNING_PREFIX "#-OpSim_fields_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_fields_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_fields_lib.c:DEBUG  :"
#define RESULTS_PREFIX "#-OpSim_fields_lib.c:RESULTS:"


////////////////////////////////////////////////////////////////////////
//functions



int OpSim_fields_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}


//////////////////////////////////////////////
// test if a object is within the outer bounds of  a field pField
// return 1 if success
int field_test_if_object_is_in_bounds ( field_struct *pField, object_struct *pObj )
{
  if ( pField == NULL || pObj == NULL ) return -1;
  return geom_test_if_in_long_lat_range((double) pObj->ra,
                                        (double) pObj->dec,
                                        (double) pField->ra_min,  (double) pField->ra_max,
                                        (double) pField->dec_min, (double) pField->dec_max);
}
//////////////////////////////////////////////

//////////////////////////////////////////////
// test if point given by ra0,dec0 is within the inner bounds of a field pField
// return 1 if success
//int test_if_field_is_in_bounds ( field_struct *pF, field_struct *p2 )
int field_test_if_point_is_in_bounds ( field_struct *pField, double ra0, double dec0 )
{
  if ( pField == NULL ) return -1;
  return geom_test_if_in_long_lat_range((double) ra0,
                                        (double) dec0,
                                        (double) pField->ra_min,  (double) pField->ra_max,
                                        (double) pField->dec_min, (double) pField->dec_max);
}

//////////////////////////////////////////////
// test if field p2 is within the outer bounds of another field pF
// return 1 if success
//int test_if_field_is_in_bounds2 ( field_struct *pF, field_struct *p2 )
int field_test_if_point_is_in_bounds2 ( field_struct *pField, double ra0, double dec0 )
{
  if ( pField == NULL ) return -1;
  return geom_test_if_in_long_lat_range((double) ra0,
                                        (double) dec0,
                                        (double) pField->ra_min2,  (double) pField->ra_max2,
                                        (double) pField->dec_min2, (double) pField->dec_max2);
}
//////////////////////////////////////////////








////////////////////////////////////////////////////////////////////////////
/////////////////////// Fieldlist stuff ///////////////////////////////////

int fieldlist_struct_init (fieldlist_struct **ppFieldList)
{
  if ( ppFieldList == NULL ) return 1;
  if ( *ppFieldList == NULL )
  {
    //make space for the struct
    if ( (*ppFieldList = (fieldlist_struct*) malloc(sizeof(fieldlist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  //init the data values
  strncpy((*ppFieldList)->str_filename, "", STR_MAX);
  (*ppFieldList)->nFields = 0;
  (*ppFieldList)->num_allocated_fields = 0;
  (*ppFieldList)->pF = NULL;

  
  hpx_pixlist_init ((hpx_pixlist_struct*) &((*ppFieldList)->Pixlist));

  
  return 0;
}

//int free_fields (fieldlist_struct *pFieldList)
int fieldlist_struct_free (fieldlist_struct **ppFieldList)
{
  int j;
  if ( ppFieldList == NULL ) return 1;
  if ( *ppFieldList )
  {
    //free all the memory in the pointing pid arrays
    for ( j = 0; j < (*ppFieldList)->nFields; j++)
    {
      field_struct *pF = (field_struct*) &((*ppFieldList)->pF[j]);
      if ( pF->pid )
      {
        free(pF->pid) ;
        pF->pid = NULL;
      }
      if ( pF->pobject_sub_index )
      {
        free(pF->pobject_sub_index) ;
        pF->pobject_sub_index = NULL;
      }
    }
    if ( (*ppFieldList)->pF ) free((*ppFieldList)->pF) ;
    (*ppFieldList)->pF = NULL;

    //free the quad associated with this struct
    //    quad_free_quad((quad_struct*) &((*ppFieldList)->Quad));

    hpx_pixlist_free ((hpx_pixlist_struct*) &((*ppFieldList)->Pixlist));
    
    
  //now free the fieldlist itself
    if ( *ppFieldList ) free(*ppFieldList) ;
    *ppFieldList = NULL;
  }
  return 0;
}
////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
int fieldlist_read_from_file (fieldlist_struct *pFieldList,
                              survey_struct *pSurvey,
                              focalplane_struct *pFocal,
                              const char* strField_filename)
{
  //read in the field centre coordinates file
  FILE* pField_file = NULL;
  char buffer[STR_MAX];
  int m;
  int num_PAs = 0;
  int num_entries = 0;
  
  if ( pFieldList == NULL ||
       pSurvey == NULL    || 
       pFocal == NULL     ||
       strField_filename == NULL ) return 1;
  pFieldList->all_PAs_were_supplied_in_input_file = FALSE;

  if ( strlen(strField_filename) <= 0 )
  {
    fprintf(stderr, "%s There were problems opening the field coordinates file: %s\n", ERROR_PREFIX, strField_filename);
    
    return 1;
  }

  fprintf(stdout, "%s Reading in the field coordinates file: %s\n", COMMENT_PREFIX, strField_filename);
  if ((pField_file = fopen(strField_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s There were problems opening the field coordinates file: %s\n", ERROR_PREFIX, strField_filename);
    
    return 1;
  }

  //scan through the field list file and count the non null rows
  pFieldList->num_allocated_fields = 0;
  while (fgets(buffer,STR_MAX,pField_file))
  {
    if ( strlen(buffer) == 0 ) continue;  //ignore blank lines
    if ( buffer[0] == '#' ) continue;  //ignore comment lines
    pFieldList->num_allocated_fields ++;
  }
  rewind(pField_file);

  
  if ( pFieldList->num_allocated_fields <= 0 )
  {
    fprintf(stderr, "%s There were problems finding any valid lines in the field coordinates file: %s\n",
            ERROR_PREFIX, strField_filename);
    
    return 1;
  }
  
  
  //make some space for the field data structures - allocate more than we will actually need.
  //  pFieldList->num_allocated_fields = (int) FIELDS_PER_CHUNK;
  //  if ((pFieldList->pF = (field_struct*) malloc(sizeof(field_struct) * pFieldList->num_allocated_fields)) == NULL )
  if ((pFieldList->pF = (field_struct*) calloc((size_t)pFieldList->num_allocated_fields,
                                               (size_t) sizeof(field_struct))) == NULL )
  {
    //    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    fprintf(stderr, "%s Problem assigning %ld bytes of memory at file %s line %d\n",
            ERROR_PREFIX, sizeof(field_struct) * pFieldList->num_allocated_fields, __FILE__, __LINE__);
    return 1;
  }

//  //initialise the fields - not needed as now done by changing the malloc to a calloc above
//  {
//    int i;
//    for(i=0;i<pFieldList->num_allocated_fields;i++)
//    {
//      field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
//    }
//  }
  
  //now read in and process the field information
  //  pFieldList->nFields = 0;
  
  pSurvey->iNum_field_in_any_survey      = 0;
  pSurvey->iNum_field_in_bright_survey   = 0;
  pSurvey->iNum_field_in_darkgrey_survey = 0;
  pSurvey->iNum_field_used_in_any_survey = 0;
  
  while (fgets(buffer,STR_MAX,pField_file))
  {
    field_struct *pF = NULL;
    int n_tokens = 0;
    if ( buffer[0] == '#') continue;  //ignore comment lines

    if ( pFieldList->nFields >= pFieldList->num_allocated_fields )
    {
//      //allocate some more space
//      pFieldList->num_allocated_fields += FIELDS_PER_CHUNK;
//      if ((pFieldList->pF = (field_struct*) realloc(pFieldList->pF, sizeof(field_struct) * pFieldList->num_allocated_fields)) == NULL )
//      {
//        fprintf(stderr, "%s Problem re-assigning memory at file %s line %d\n",
//                ERROR_PREFIX, __FILE__, __LINE__);
//        
//        return 1;
//      }
      fprintf(stderr, "%s I found more than the expected number of valid lines (%d) in the field coordinates file: %s\n",
              ERROR_PREFIX,  pFieldList->num_allocated_fields, strField_filename);
      
      return 1;
    }

    pF = (field_struct*) &(pFieldList->pF[pFieldList->nFields]);
    
    n_tokens = sscanf(buffer, "%s %lf %lf %lf",
                      (char*) pF->name,
                      (double*) &(pF->ra0),
                      (double*) &(pF->dec0),
                      (double*) &(pF->pa0));
    //note that will have to take account of field files that do have valid PA values
    //- we can define that that if PA in the file is set to nan then we will set it ourselves 
    if      ( n_tokens == 4 ) {}  //normal
    else if ( n_tokens == 3 ) {pF->pa0 = NAN;} //ok but need to set PA to null
    else
    {
      fprintf(stderr, "%s Problem reading line from input fieldlist file %s, ntokens=%d\n", ERROR_PREFIX, strField_filename, n_tokens);
      fprintf(stderr, "%s Problem line was: %s\n", ERROR_PREFIX, buffer);
      
      return 1;
    }

    if (!isnan(pF->pa0)) num_PAs ++;
    num_entries ++;
    
    //      pF->radius_deg    = pTele->fov_radius_deg;
    //      pF->radius_arcsec = pTele->fov_radius_arcsec;
    pF->radius_deg    = pFocal->fiber_offaxis_max_deg;
    pF->id            = (uint) pFieldList->nFields;
    pF->n_object      = 0;
    pF->n_object_lores= 0;
    pF->n_object_hires= 0;
    pF->pid           = NULL;
    pF->pid_data_length = 0;
    pF->num_neighbours  = 0;
    pF->in_survey       = (uint) SURVEY_NONE;

    pF->hpx_pix = 0;             //pixel containing field centre
    pF->hpx_npix = 0;           //number of pixels overlapping field
    
//    //rotate the field centre about the unit axes by the given amounts
//    if ( geom3d_rotate_ra_dec_about_axes ((double) pF->ra0, (double) pF->dec0,
//                                          (double) pSurvey->tiling_dither_offsets_x[0],
//                                          (double) pSurvey->tiling_dither_offsets_y[0],
//                                          (double) pSurvey->tiling_dither_offsets_z[0],
//                                          (double*) &(pF->ra0), (double*) &(pF->dec0)))
//    {
//      return 1;
//    }

    //standard transformation from spherical polars to x,y,z 
    geom_ra_dec_to_unit_vectors((double) pF->ra0, (double) pF->dec0,
                                (double*) &(pF->unit_coords[0]),
                                (double*) &(pF->unit_coords[1]),
                                (double*) &(pF->unit_coords[2]));
    //    pF->ntiles = 0;
    for(m=0;m<MAX_NUM_MOON_PHASES;m++)
    {
      pF->ntiles_done[m]            = 0;
    }
    for(m=0;m<MAX_NUM_MOON_GROUPS;m++)
    {
      int a;
      pF->ntiles_req[m]             = 0;
      pF->ntiles_todo[m]            = 0;
      pF->ntiles_per_dither[m]      = 0;
      pF->ntiles_visible[m]         = 0;
      pF->ntiles_visible_predict[m] = 0.0;
      pF->ntiles_visible_predict_weighted[m] = 0.0;
      for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
      {
        pF->ntiles_visible_am[m][a]         = 0;
        pF->ntiles_visible_am_predict[m][a] = 0.0;
      }
    }
    
    //initialise the record of the weight in each field
    pF->object_priority_total     = 0.0;
    pF->object_priority_completed = 0.0;
    pF->object_priority_remaining = 0.0;
    pF->object_priority_total_weighted     = 0.0;
    pF->object_priority_completed_weighted = 0.0;
    pF->object_priority_remaining_weighted = 0.0;

    pF->texp_total_todo     = 0.0;
    pF->texp_total_done     = 0.0;

    pF->object_dark_fhrs_lores = 0.0;
    pF->object_dark_fhrs_hires = 0.0;
    
    pF->last_dither_index   = -1;

    pFieldList->nFields ++;
    
    while ( pF->ra0 >= (double) 360.0) pF->ra0 -= (double) 360.0;
    while ( pF->ra0 <  (double)   0.0) pF->ra0 += (double) 360.0;

    //some more bits that will be useful to speed things up later
    pF->cos_dec0 = cosd(pF->dec0);
    pF->sin_dec0 = sind(pF->dec0);

    //calc the Galactic latitude of the field centre
    geom_ra_dec_to_gal_l_b(pF->ra0, pF->dec0, &(pF->l0), &(pF->b0));

    //check if we are constraining the output to a smaller area
    if ( pSurvey->debug )
    {
      if (pF->ra0  < pSurvey->debug_ra_min)  continue ;
      if (pF->ra0  > pSurvey->debug_ra_max)  continue ;
      if (pF->dec0 < pSurvey->debug_dec_min) continue ;
      if (pF->dec0 > pSurvey->debug_dec_max) continue ;
    }
    
    
    if ( pF->dec0 <= pSurvey->tiling_dec_max &&
         pF->dec0 >= pSurvey->tiling_dec_min )
    {
      if (pSurvey->strategy_code == SURVEY_STRATEGY_FLAT )
      {
        pF->in_survey |= (unsigned int) (SURVEY_BRIGHT   | SURVEY_IN_AREA_BRIGHT |
                                         SURVEY_DARKGREY | SURVEY_IN_AREA_DARKGREY);
        pSurvey->iNum_field_in_bright_survey ++;
        pSurvey->iNum_field_in_darkgrey_survey ++;
        pSurvey->iNum_field_in_any_survey ++;
      }
      else if ( pSurvey->strategy_code == SURVEY_STRATEGY_DBPDG )
      {
        int flag = 0;
        if ( fabs(pF->b0) <= pSurvey->dbpdg_gal_lat_max_bright ) 
        {
          pF->in_survey |= (unsigned int) (SURVEY_BRIGHT | SURVEY_IN_AREA_BRIGHT);
          pSurvey->iNum_field_in_bright_survey ++;
          flag ++;
        }
        if ( fabs(pF->b0) >= pSurvey->dbpdg_gal_lat_min_darkgrey ) 
        {
          pF->in_survey |= (unsigned int) (SURVEY_DARKGREY | SURVEY_IN_AREA_DARKGREY);
          pSurvey->iNum_field_in_darkgrey_survey ++;
          flag ++;
        }
        if ( flag > 0 ) pSurvey->iNum_field_in_any_survey ++;
      }
      else if ( pSurvey->strategy_code == SURVEY_STRATEGY_MANUAL )
      {
        // TODO 
      }
      else
      {
        
      }
    }
  }
  //close the file
  if ( pField_file ) fclose ( pField_file) ;
  pField_file = NULL;

  
  if ( pFieldList->nFields <= 0 )
  {
    fprintf(stderr, "%s I read zero field centres from the field coordinates file: %s\n",
            ERROR_PREFIX, strField_filename);
    
    return 1;
  }
  //free the unused space
  if ((pFieldList->pF = (field_struct*) realloc(pFieldList->pF, sizeof(field_struct)* pFieldList->nFields)) == NULL )
  {
    fprintf(stderr, "%s Problem re-assigning memory at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);
    
    return 1;
  }

  //double check that we have read the Position Angles as well
  if ( num_PAs > 0 )
  {
    pFieldList->all_PAs_were_supplied_in_input_file = TRUE;
    if ( num_PAs < num_entries )
    {
      fprintf(stderr, "%s I only read %d PAs (out of %d entries) from the field coordinates file: %s\n",
              WARNING_PREFIX, num_PAs, num_entries, strField_filename);
      pFieldList->all_PAs_were_supplied_in_input_file = FALSE;
    }

  }
 
  //report the results
  fprintf(stdout, "%s I read in   %9d pointings %sfrom pointings file %s\n",
          COMMENT_PREFIX, pFieldList->nFields,
          (pFieldList->all_PAs_were_supplied_in_input_file?"(and PAs) ":""), pFieldList->str_filename);
  fprintf(stdout, "%s             %9d were in the Combined  survey area\n",
          COMMENT_PREFIX, pSurvey->iNum_field_in_any_survey);
  fprintf(stdout, "%s             %9d were in the Bright    survey area\n",
          COMMENT_PREFIX, pSurvey->iNum_field_in_bright_survey);
  fprintf(stdout, "%s             %9d were in the DarkGrey  survey area\n",
          COMMENT_PREFIX, pSurvey->iNum_field_in_darkgrey_survey);
  fprintf(stdout, "%s             %9d were outside the main survey area\n",
          COMMENT_PREFIX, pFieldList->nFields - pSurvey->iNum_field_in_any_survey);

  //and finally, take copies of these variables
  pSurvey->pmoon_bb->num_fields  = pSurvey->iNum_field_in_bright_survey;
  pSurvey->pmoon_dg->num_fields  = pSurvey->iNum_field_in_darkgrey_survey;


  
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////
// set up field neighbours - just use the undithered central ra0,dec0 positions and the "furthest_radius" 
int fieldlist_setup_field_neighbours_old (fieldlist_struct *pFieldList, const double tolerance)
{
  int mean_neighbours = 0;
  int max_field_neighbours = 0;
  int min_field_neighbours = INT_MAX;
  int i,j;
  clock_t clocks_start = clock();
  if ( pFieldList == NULL )
  {
    return 1;
  }
  fprintf(stdout, "%s Setting up the neighbouring fields for each field (may take some time) ... ", COMMENT_PREFIX);
  fflush(stdout);
  //initialise evrything first
  for (i=0; i<pFieldList->nFields; i++)
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
    pF->id_nearest_neighbour = -1;
    pF->dist_nearest_neighbour = (float) FLT_MAX;
  }

  for (i=0;i<pFieldList->nFields;i++)  
  {
    double dist;
    double cos_dist;
    field_struct *pF1 = (field_struct*) &(pFieldList->pF[i]);
    double cos_dist_test = cosd(tolerance * pF1->furthest_radius_deg);

    for (j=i+1; j<pFieldList->nFields; j++)
    {
      field_struct *pF2 = (field_struct*) &(pFieldList->pF[j]);
      if (field_test_if_point_is_in_bounds2 ((field_struct*) pF1, pF2->ra0, pF2->dec0))
      {

        //        dist = arc_dist(pF1->ra0,pF1->dec0,pF2->ra0,pF2->dec0);
        //        dist = fast_arc_dist(pF1->ra0,pF2->ra0,pF1->sin_dec0,pF1->cos_dec0,pF2->sin_dec0,pF2->cos_dec0);
        cos_dist = geom_very_fast_cos_arc_dist(pF1->ra0,pF2->ra0,pF1->sin_dec0,pF1->cos_dec0,pF2->sin_dec0,pF2->cos_dec0);
          
        //        if (dist <= (pF1->new_radius_deg + pF2->new_radius_deg) )
        if (cos_dist >= cos_dist_test ) // ie if (dist < dist_test)
        {
          //calculate actual dist
          dist = acosd(cos_dist);
            
          pF1->id_neighbour[pF1->num_neighbours] = j;
          pF1->dist_neighbour[pF1->num_neighbours] = dist;
          pF1->bearing_neighbour[pF1->num_neighbours] = geom_calc_bearing(pF1->ra0,pF1->dec0,pF2->ra0,pF2->dec0);
          
          pF2->id_neighbour[pF2->num_neighbours] = i;
          pF2->dist_neighbour[pF2->num_neighbours] = dist;
          //            pF2->bearing_neighbour[pF2->num_neighbours] = calc_bearing(pF2->ra0,pF2->dec0,pF1->ra0,pF1->dec0);
          //recycle the bearing to reduce the amount of trig needed.
          pF2->bearing_neighbour[pF2->num_neighbours] = pF1->bearing_neighbour[pF1->num_neighbours] + 180.;
          while ( pF2->bearing_neighbour[pF2->num_neighbours] >= 360.0 ) pF2->bearing_neighbour[pF2->num_neighbours] -= 360.0;

          //increment and check
          pF1->num_neighbours ++;
          if (pF1->num_neighbours >= MAX_FIELD_NEIGHBOURS)
          {
            fprintf(stderr, "%s Error finding pointing neighbours for pointing %d, too many neighbours (%d>=%d)!\n",
                    ERROR_PREFIX, i, pF1->num_neighbours,MAX_FIELD_NEIGHBOURS-1);
            return 1;
          }
          
          pF2->num_neighbours ++;
          if (pF2->num_neighbours >= MAX_FIELD_NEIGHBOURS)
          {
            fprintf(stderr, "%s Error finding pointing neighbours for pointing %d, too many neighbours (%d>=%d)!\n",
                    ERROR_PREFIX, j, pF2->num_neighbours, MAX_FIELD_NEIGHBOURS-1);
            return 1;
          }
          if ( dist < pF1->dist_nearest_neighbour )
          {
            pF1->dist_nearest_neighbour = dist;
            pF1->id_nearest_neighbour = j;
          }
          if ( dist < pF2->dist_nearest_neighbour )
          {
            pF2->dist_nearest_neighbour = dist;
            pF2->id_nearest_neighbour = i;
          }
        }
      }          
    }
    mean_neighbours += pF1->num_neighbours;
    if ( pF1->num_neighbours > max_field_neighbours) max_field_neighbours = pF1->num_neighbours;
    if ( pF1->num_neighbours < min_field_neighbours) min_field_neighbours = pF1->num_neighbours;
    
//    if ( pF1->num_neighbours == 0 && (pF1->in_survey & SURVEY_ANY) )
//    {
//      if( ParamList.debug )
//        fprintf(stdout, "%s Pointing %5d %5s at %11.4f %11.4f has no neighbours within 2x %.3f deg nearest= %.3f deg\n",
//                COMMENT_PREFIX, i,  pF1->name, pF1->ra0, pF1->dec0, pF1->new_radius_deg, pF1->dist_nearest_neighbour);
//    }
  }

//  if(FALSE)
//  {
//    for (i = 0; i < pFieldList->nFields; i++)
//    {
//      field_struct *pF1 = (field_struct*) &(pFieldList->pF[i]);
//      fprintf(stdout, "%s FieldNeighbourCheck %5d %5s at %11.4f %11.4f has %2d neighbours within 2x %.3f deg ",
//              COMMENT_PREFIX, i,  pF1->name, pF1->ra0, pF1->dec0,
//              pF1->num_neighbours,
//              pF1->new_radius_deg);
//      if (pF1->num_neighbours> 0)
//      {
//        field_struct *pF2 = (field_struct*) &(pFieldList->pF[pF1->id_nearest_neighbour]);
//        fprintf(stdout, "nearest: %5u %5s at %11.4f %11.4f dist=%7.3f deg\n",
//                pF2->id, pF2->name, pF2->ra0, pF2->dec0, pF1->dist_nearest_neighbour);
//      }
//      else fprintf(stdout, "\n");
//    }
//  }
  
  fprintf(stdout, " done (that took %.3fs)\n",
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC); 
  fprintf(stdout, "%s Neighbour stats for fields inside the survey: mean=%.4f min=%d max=%d\n",
          COMMENT_PREFIX,
          (float)(mean_neighbours/((float)(pFieldList->nFields>0?pFieldList->nFields:1))),
          min_field_neighbours,
          max_field_neighbours);
  fflush(stdout);
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////
// set up field neighbours - just use the undithered central ra0,dec0 positions and the "furthest_radius" 
// use the hpx indexing to speed things up
int fieldlist_setup_field_neighbours (fieldlist_struct *pFieldList, const double tolerance)
{
  int mean_neighbours = 0;
  int max_field_neighbours = 0;
  int min_field_neighbours = INT_MAX;
  int i;
  clock_t clocks_start = clock();
  hpx_pixlist_struct *pPixlist;
  uint    *pField_ids = NULL;

  
  if ( pFieldList == NULL )  return 1;
  pPixlist = (hpx_pixlist_struct*) &(pFieldList->Pixlist);
  if (pPixlist == NULL)  return 1;

  fprintf(stdout, "%s Setting up the neighbouring fields for each field (hpx way) ... ",
          COMMENT_PREFIX);  fflush(stdout);
  //initialise everything first
  for (i=0; i<pFieldList->nFields; i++)
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
    pF->id_nearest_neighbour = -1;
    pF->dist_nearest_neighbour = (float) FLT_MAX;
  }

  //make some space
  if ((pField_ids = (uint*) calloc((size_t) pFieldList->nFields, sizeof(uint)))   == NULL)
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  //TODO - only associate a field with those other fields which overlap with
  // any of the hpx pixels in the fields's own hpx_pixlist[]
  //  fprintf(stdout, "DEBUG:\n"); fflush(stdout);
  for (i=0;i<pFieldList->nFields;i++)  
  {
    //    double dist;
    double cos_dist;
    field_struct *pF1 = (field_struct*) &(pFieldList->pF[i]);
    double cos_dist_test = cosd(tolerance * pF1->furthest_radius_deg);
    int num_fields = 0;
    int j, k;

    //    fprintf(stdout, "DEBUG: Field %d pF1->hpx_npix=%ld pFieldList->nFields=%d\n",
    //            i, pF1->hpx_npix, pFieldList->nFields); fflush(stdout);
    // build a unique list of potentially overlapping fields
    for (j=0; j<pF1->hpx_npix; j++)
    {
      uint pix_index = (uint) pF1->hpx_pixlist[j];
      assert(pix_index >=0 && pix_index < pPixlist->npix); 
      {
        hpx_pix_struct *pPix = (hpx_pix_struct*) &(pPixlist->pPix[pix_index]);
        //        fprintf(stdout, "DEBUG: Field %d j=%d pix_index=%d pPix->nMembers=%d num_fields=%d\n",
        //                i, j, pix_index, pPix->nMembers, num_fields); fflush(stdout);
        for (k=0; k<pPix->nMembers; k++)
        {
          field_struct *pF2 = (field_struct*) pPix->pMembers[k];
          assert( pF2 != NULL);
          if ( (uint) pF1->id != (uint) pF2->id )
          {
            assert(num_fields < pFieldList->nFields);  //same as below
            pField_ids[num_fields] = (uint) pF2->id;
            num_fields++;
            if ( num_fields >= pFieldList->nFields )  //this should never happen
            {
              fprintf(stderr, "%s Bad number of potential neighbouring fields for field %d (num_fields=%d)\n",
                      ERROR_PREFIX, pF1->id, num_fields);
              return 1;
            }
          }
        }
      }
    }
    //    fprintf(stdout, "DEBUG: about to call util_uniqueify_u()\n");fflush(stdout);
    if ( num_fields > 0 )
    {
      ulong num_unique_fields;
      if ( util_uniqueify_u((ulong)num_fields, (uint*) pField_ids, (ulong*) &num_unique_fields)) return 1;
      assert(num_unique_fields >= 0);
      assert(num_unique_fields < pFieldList->nFields);
      num_fields = (int) num_unique_fields;
    }
    //    fprintf(stdout, "DEBUG: ok after util_uniqueify_u()\n");fflush(stdout);

    for (j=0; j<num_fields; j++)
    {
      //      field_struct *pF2 = (field_struct*) &(pFieldList->pF[j]);
      assert ( pField_ids[j] < pFieldList->nFields );
      field_struct *pF2 = (field_struct*) &(pFieldList->pF[pField_ids[j]]);
      if (field_test_if_point_is_in_bounds2 ((field_struct*) pF1, pF2->ra0, pF2->dec0))
      {

        //        dist = arc_dist(pF1->ra0,pF1->dec0,pF2->ra0,pF2->dec0);
        //        dist = fast_arc_dist(pF1->ra0,pF2->ra0,pF1->sin_dec0,pF1->cos_dec0,pF2->sin_dec0,pF2->cos_dec0);
        cos_dist = geom_very_fast_cos_arc_dist(pF1->ra0,pF2->ra0,
                                               pF1->sin_dec0,pF1->cos_dec0,
                                               pF2->sin_dec0,pF2->cos_dec0);
          
        //        if (dist <= (pF1->new_radius_deg + pF2->new_radius_deg) )
        if (cos_dist >= cos_dist_test ) // ie if (dist < dist_test)
        {
          //calculate actual dist
          double dist = acosd(cos_dist);
            
          pF1->id_neighbour[pF1->num_neighbours] = j;
          pF1->dist_neighbour[pF1->num_neighbours] = dist;
          pF1->bearing_neighbour[pF1->num_neighbours] = geom_calc_bearing(pF1->ra0,pF1->dec0,pF2->ra0,pF2->dec0);
          
//          pF2->id_neighbour[pF2->num_neighbours] = i;
//          pF2->dist_neighbour[pF2->num_neighbours] = dist;
//          //            pF2->bearing_neighbour[pF2->num_neighbours] = calc_bearing(pF2->ra0,pF2->dec0,pF1->ra0,pF1->dec0);
//          //recycle the bearing to reduce the amount of trig needed.
//          pF2->bearing_neighbour[pF2->num_neighbours] = pF1->bearing_neighbour[pF1->num_neighbours] + 180.;
//          while ( pF2->bearing_neighbour[pF2->num_neighbours] >= 360.0 ) pF2->bearing_neighbour[pF2->num_neighbours] -= 360.0;
//
          //increment and check
          pF1->num_neighbours ++;
          if (pF1->num_neighbours >= MAX_FIELD_NEIGHBOURS)
          {
            fprintf(stderr, "%s Error finding pointing neighbours for pointing %d, too many neighbours (%d>=%d)!\n",
                    ERROR_PREFIX, i, pF1->num_neighbours,MAX_FIELD_NEIGHBOURS-1);
            return 1;
          }
          
//          pF2->num_neighbours ++;
//          if (pF2->num_neighbours >= MAX_FIELD_NEIGHBOURS)
//          {
//            fprintf(stderr, "%s Error finding pointing neighbours for pointing %d, too many neighbours (%d>=%d)!\n",
//                    ERROR_PREFIX, j, pF2->num_neighbours, MAX_FIELD_NEIGHBOURS-1);
//            return 1;
//          }
          if ( dist < pF1->dist_nearest_neighbour )
          {
            pF1->dist_nearest_neighbour = dist;
            pF1->id_nearest_neighbour = j;
          }
//          if ( dist < pF2->dist_nearest_neighbour )
//          {
//            pF2->dist_nearest_neighbour = dist;
//            pF2->id_nearest_neighbour = i;
//          }
        }
      }          
    }
    mean_neighbours += pF1->num_neighbours;
    if ( pF1->num_neighbours > max_field_neighbours) max_field_neighbours = pF1->num_neighbours;
    if ( pF1->num_neighbours < min_field_neighbours) min_field_neighbours = pF1->num_neighbours;
    
//    if ( pF1->num_neighbours == 0 && (pF1->in_survey & SURVEY_ANY) )
//    {
//      if( ParamList.debug )
//        fprintf(stdout, "%s Pointing %5d %5s at %11.4f %11.4f has no neighbours within 2x %.3f deg nearest= %.3f deg\n",
//                COMMENT_PREFIX, i,  pF1->name, pF1->ra0, pF1->dec0, pF1->new_radius_deg, pF1->dist_nearest_neighbour);
//    }
  }

//  if(FALSE)
//  {
//    for (i = 0; i < pFieldList->nFields; i++)
//    {
//      field_struct *pF1 = (field_struct*) &(pFieldList->pF[i]);
//      fprintf(stdout, "%s FieldNeighbourCheck %5d %5s at %11.4f %11.4f has %2d neighbours within 2x %.3f deg ",
//              COMMENT_PREFIX, i,  pF1->name, pF1->ra0, pF1->dec0,
//              pF1->num_neighbours,
//              pF1->new_radius_deg);
//      if (pF1->num_neighbours> 0)
//      {
//        field_struct *pF2 = (field_struct*) &(pFieldList->pF[pF1->id_nearest_neighbour]);
//        fprintf(stdout, "nearest: %5u %5s at %11.4f %11.4f dist=%7.3f deg\n",
//                pF2->id, pF2->name, pF2->ra0, pF2->dec0, pF1->dist_nearest_neighbour);
//      }
//      else fprintf(stdout, "\n");
//    }
//  }
  
  FREETONULL(pField_ids);

  fprintf(stdout, " done (that took %.3fs)\n",
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC); 
  fprintf(stdout, "%s Neighbour stats for fields inside the survey: mean=%.4f min=%d max=%d\n", 
          COMMENT_PREFIX,
          (float)(mean_neighbours/((float)(pFieldList->nFields>0?pFieldList->nFields:1))),
          min_field_neighbours,
          max_field_neighbours);
  fflush(stdout);
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////













//////////////////////////////////////////////////////////////////////////////////////
int fieldlist_write_vertex_file ( fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream )
{
  int j,k;
  FILE* pFile = NULL;
  if ( pFieldList == NULL ) return 1;

  //////////////////////////////////////////////////////////////////////////////////////
  //print out the pointing vertices
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the field vertices file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  for ( j = 0; j < pFieldList->nFields; j ++ )
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
    if (!(pF->in_survey & ( SURVEY_ANY | SURVEY_IN_AREA_ANY)) )
    {
      continue;
    }
    
    {
      int dither_index = 0;
      fprintf( pFile, "# Pointing %5s centre= %12.7f %12.7f PA= %8.3f flags= 0x%08x ntiles_req_bb= %d ntiles_req_dg= %d ntiles_todo_bb= %d ntiles_todo_dg= %d Gal_l= %12.7f Gal_b= %+12.7f ntiles_predict_vis_bb= %.1f ntiles_predict_vis_dg= %.1f ntiles_done_bb= %d ntiles_done_dg= %d ntiles_vis_bb= %d ntiles_vis_dg= %d num_neighbours= %d hpx_npix= %ld\n",
               pF->name, pF->pra0[dither_index], pF->pdec0[dither_index], pF->pa0, pF->in_survey,
               pF->ntiles_req[MOON_GROUP_BB],  pF->ntiles_req[MOON_GROUP_DG],
               pF->ntiles_todo[MOON_GROUP_BB], pF->ntiles_todo[MOON_GROUP_DG],
               pF->l0, pF->b0,
               pF->ntiles_visible_predict[MOON_GROUP_BB], pF->ntiles_visible_predict[MOON_GROUP_DG],
               pF->ntiles_done[MOON_PHASE_BRIGHT], pF->ntiles_done[MOON_PHASE_GREY] + pF->ntiles_done[MOON_PHASE_DARK],
               pF->ntiles_visible[MOON_GROUP_BB], pF->ntiles_visible[MOON_GROUP_DG],
               pF->num_neighbours, pF->hpx_npix);
      fprintf( pFile, "#%-11s %12s %10s %6s %6s %6s %6s %6s %6s %6s %6s %6s %6s\n",
               "RA(deg)", "Dec(deg)", "SurveyCode", "ReqBB", "ReqDG","TodoBB", "TodoDG","PvisBB", "PvisDG","DoneBB", "DoneDG","VisBB", "VisDG");
      for (k=0;k<=pF->num_vertex;k++)
      {
        int kk = (k < pF->num_vertex ? k : 0); 
        fprintf( pFile, "%12.7f %12.7f 0x%08x %6d %6d %6d %6d %6.1f %6.1f %6d %6d %6d %6d\n",
                 pF->vertex_ra[dither_index][kk], pF->vertex_dec[dither_index][kk],pF->in_survey,
                 pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG],
                 pF->ntiles_todo[MOON_GROUP_BB], pF->ntiles_todo[MOON_GROUP_DG],
                 pF->ntiles_visible_predict[MOON_GROUP_BB], pF->ntiles_visible_predict[MOON_GROUP_DG],
                 pF->ntiles_done[MOON_PHASE_BRIGHT], pF->ntiles_done[MOON_PHASE_GREY] + pF->ntiles_done[MOON_PHASE_DARK],
                 pF->ntiles_visible[MOON_GROUP_BB], pF->ntiles_visible[MOON_GROUP_DG]);
      }
      fprintf( pFile, "\n\n");
    }
  }
  if (pFile) fclose(pFile);
  pFile = NULL;
  if ( pStream )
  {
    fprintf(pStream, "%s I wrote out the field vertices/progress report file to %s\n", COMMENT_PREFIX, str_filename);
    (void) fflush(pStream);
  }
  
  return 0;  
}

//////////////////////////////////////////////////////////////////////////////////////
int fieldlist_write_dithered_vertex_file ( fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream )
{
  int j,k;
  FILE* pFile = NULL;
  if ( pFieldList == NULL ) return 1;

  //////////////////////////////////////////////////////////////////////////////////////
  //print out the pointing vertices
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the dithered field vertices file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  for ( j = 0; j < pFieldList->nFields; j ++ )
  {
    int dither_index = 0;
    field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
    if (!(pF->in_survey & ( SURVEY_ANY | SURVEY_IN_AREA_ANY)) )
    {
      continue;
    }
    
    for (dither_index=0;dither_index<pF->num_dithers;dither_index++)
    {
      fprintf( pFile, "# Pointing %5s dither %2d centre= %12.7f %12.7f PA= %8.3f flags= 0x%08x Gal_l= %12.7f Gal_b= %+12.7f\n",
               pF->name, dither_index,
               pF->pra0[dither_index], pF->pdec0[dither_index], pF->pa0, pF->in_survey,
               pF->pl0[dither_index], pF->pb0[dither_index]);
      fprintf( pFile, "#%-11s %12s %6s\n", "RA(deg)", "Dec(deg)", "Dither");
      for (k=0;k<=pF->num_vertex;k++)
      {
        int kk = (k < pF->num_vertex ? k : 0); 
        fprintf( pFile, "%12.7f %12.7f %6d\n",
                 pF->vertex_ra[dither_index][kk], pF->vertex_dec[dither_index][kk],dither_index);
      }
      fprintf( pFile, "\n\n");
    }
  }
  if (pFile) fclose(pFile);
  pFile = NULL;
  if ( pStream )
  {
    fprintf(pStream, "%s I wrote out the dithered field vertices to %s\n", COMMENT_PREFIX, str_filename);
    (void) fflush(pStream);
  }
  
  return 0;  
}
//////////////////////////////////////////////////////////////////////////////////////
int fieldlist_write_dithered_extended_vertex_file ( fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream )
{
  int j,k;
  FILE* pFile = NULL;
  if ( pFieldList == NULL ) return 1;

  //////////////////////////////////////////////////////////////////////////////////////
  //print out the pointing vertices
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the dithered field extended vertices file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  for ( j = 0; j < pFieldList->nFields; j ++ )
  {
    int dither_index = 0;
    field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
    if (!(pF->in_survey & ( SURVEY_ANY | SURVEY_IN_AREA_ANY)) )
    {
      continue;
    }
    
    for (dither_index=0;dither_index<pF->num_dithers;dither_index++)
    {
      fprintf( pFile, "# Pointing %5s dither %2d centre= %12.7f %12.7f PA= %8.3f flags= 0x%08x Gal_l= %12.7f Gal_b= %+12.7f\n",
               pF->name, dither_index,
               pF->pra0[dither_index], pF->pdec0[dither_index], pF->pa0, pF->in_survey,
               pF->pl0[dither_index], pF->pb0[dither_index]);
      fprintf( pFile, "#%-11s %12s %6s\n", "RA(deg)", "Dec(deg)", "Dither");
      for (k=0;k<=pF->num_ext_vertex;k++)
      {
        int kk = (k < pF->num_ext_vertex ? k : 0); 
        fprintf( pFile, "%12.7f %12.7f %6d\n",
                 pF->ext_vertex_ra[dither_index][kk], pF->ext_vertex_dec[dither_index][kk], dither_index);
      }
      fprintf( pFile, "\n\n");
    }
  }
  if (pFile) fclose(pFile);
  pFile = NULL;
  if ( pStream )
  {
    fprintf(pStream, "%s I wrote out the dithered field extended vertices to %s\n", COMMENT_PREFIX, str_filename);
    (void) fflush(pStream);
  }
  
  return 0;  
}




//////////////////////////////////////////////////////////////////////////////////////
int fieldlist_write_extended_vertex_file ( fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream )
{
  int j,k;
  FILE* pFile = NULL;
  if ( pFieldList == NULL ) return 1;

  //////////////////////////////////////////////////////////////////////////////////////
  //print out the pointing vertices
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the field extended vertex file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  for ( j = 0; j < pFieldList->nFields; j ++ )
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
    if (!(pF->in_survey & ( SURVEY_ANY | SURVEY_IN_AREA_ANY)) )
    {
      continue;
    }
    
    {
      int dither_index = 0;
      fprintf( pFile, "# Pointing %5s centre= %12.7f %12.7f PA= %8.3f flags= 0x%08x ntiles_req_bb= %d ntiles_req_dg= %d ntiles_todo_bb= %d ntiles_todo_dg= %d Gal_l= %12.7f Gal_b= %+12.7f ntiles_predict_vis_bb= %.1f ntiles_predict_vis_dg= %.1f ntiles_done_bb= %d ntiles_done_dg= %d ntiles_vis_bb= %d ntiles_vis_dg= %d num_neighbours= %d\n",
               pF->name, pF->pra0[dither_index], pF->pdec0[dither_index], pF->pa0, pF->in_survey,
               pF->ntiles_req[MOON_GROUP_BB],  pF->ntiles_req[MOON_GROUP_DG],
               pF->ntiles_todo[MOON_GROUP_BB], pF->ntiles_todo[MOON_GROUP_DG],
               pF->l0, pF->b0,
               pF->ntiles_visible_predict[MOON_GROUP_BB], pF->ntiles_visible_predict[MOON_GROUP_DG],
               pF->ntiles_done[MOON_PHASE_BRIGHT], pF->ntiles_done[MOON_PHASE_GREY] + pF->ntiles_done[MOON_PHASE_DARK],
               pF->ntiles_visible[MOON_GROUP_BB], pF->ntiles_visible[MOON_GROUP_DG],
               pF->num_neighbours);
      fprintf( pFile, "#%-11s %12s %10s %6s %6s %6s %6s %6s %6s %6s %6s %6s %6s\n",
               "RA(deg)", "Dec(deg)", "SurveyCode", "ReqBB", "ReqDG","TodoBB", "TodoDG","PvisBB", "PvisDG","DoneBB", "DoneDG","VisBB", "VisDG");
      for (k=0;k<=pF->num_ext_vertex;k++)
      {
        int kk = (k < pF->num_ext_vertex ? k : 0); 
        fprintf( pFile, "%12.7f %12.7f 0x%08x %6d %6d %6d %6d %6.1f %6.1f %6d %6d %6d %6d\n",
                 pF->ext_vertex_ra[dither_index][kk], pF->ext_vertex_dec[dither_index][kk],pF->in_survey,
                 pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG],
                 pF->ntiles_todo[MOON_GROUP_BB], pF->ntiles_todo[MOON_GROUP_DG],
                 pF->ntiles_visible_predict[MOON_GROUP_BB], pF->ntiles_visible_predict[MOON_GROUP_DG],
                 pF->ntiles_done[MOON_PHASE_BRIGHT], pF->ntiles_done[MOON_PHASE_GREY] + pF->ntiles_done[MOON_PHASE_DARK],
                 pF->ntiles_visible[MOON_GROUP_BB], pF->ntiles_visible[MOON_GROUP_DG]);
      }
      fprintf( pFile, "\n\n");
    }
  }
  if (pFile) fclose(pFile);
  pFile = NULL;
  if ( pStream ) {
    fprintf(pStream, "%s I wrote out the field extended vertices file to %s\n", COMMENT_PREFIX, str_filename);
    (void) fflush(pStream);
  }
  
  return 0;  
}







//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
int fieldlist_write_vertex_files ( fieldlist_struct *pFieldList )
{
  //  int j,k;
  char str_filename[STR_MAX];
  if ( pFieldList == NULL ) return 1;

  //print out the original pointing vertices
  snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_FILENAME);
  if ( fieldlist_write_vertex_file ( (fieldlist_struct*) pFieldList, (const char*) str_filename , stdout ))
  {
    return 1;    
  }

  //print out the original pointing extended vertices
  snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_EXTENDED_FILENAME);
  if ( fieldlist_write_extended_vertex_file ( (fieldlist_struct*) pFieldList, (const char*) str_filename , stdout ))
  {
    return 1;    
  }

  //print out the dithered pointing vertices
  snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, FIELD_DITHERED_VERTICES_FILENAME);
  if ( fieldlist_write_dithered_vertex_file ( (fieldlist_struct*) pFieldList, (const char*) str_filename , stdout ))
  {
    return 1;    
  }

  //print out the dithered extended pointing vertices
  snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, FIELD_DITHERED_EXTENDED_VERTICES_FILENAME);
  if ( fieldlist_write_dithered_extended_vertex_file ( (fieldlist_struct*) pFieldList, (const char*) str_filename , stdout ))
  {
    return 1;    
  }
//  //now output the vertices of the initial RA,Dec cut per field
//  {
//    char* pointing_vertices_filename = "pointing_vertices_initial.txt";
//    FILE* ppointing_vertices_file = NULL;
//    field_struct *pF;
//
//    if ((ppointing_vertices_file = fopen(pointing_vertices_filename, "w")) == NULL )
//    {
//      fprintf(stderr, "%s  There were problems opening the initial pointing_vertices file: %s\n",
//              ERROR_PREFIX, pointing_vertices_filename);
//      return 1;
//    }
//
//    for ( j = 0; j < pFieldList->nFields; j ++ )
//    {
//      pF = (field_struct*) &(pFieldList->pF[j]);
//
//      if (!(pF->in_survey & (SURVEY_ANY | SURVEY_IN_AREA_ANY)) ) continue;
//      fprintf( ppointing_vertices_file, "# Pointing %5s centre= %12.7f %12.7f PA= %8.3f flags= 0x%08x ntiles_bb= %d ntiles_dg= %d  Gal_l= %12.7f Gal_b= %+12.7f\n",
//               pF->name, pF->ra0, pF->dec0, pF->pa0, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG], pF->l0, pF->b0);
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_min, pF->dec_min, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_min, pF->dec_max, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_max, pF->dec_max, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_max, pF->dec_min, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      //print the first vertex again
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_min, pF->dec_min, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "\n");
//    }
//    fprintf(stdout, "%s I wrote out the initial pointing vertices to %s\n",
//            COMMENT_PREFIX, pointing_vertices_filename);    fflush(stdout);
//    if (ppointing_vertices_file) fclose(ppointing_vertices_file);
//    ppointing_vertices_file = NULL;
//  }

//  //now output the extended vertices of the initial RA,Dec cut per field
//  {
//    char* pointing_vertices_filename = "pointing_vertices_initial2.txt";
//    FILE* ppointing_vertices_file = NULL;
//    field_struct *pF;
//
//    if ((ppointing_vertices_file = fopen(pointing_vertices_filename, "w")) == NULL )
//    {
//      fprintf(stderr, "%s  There were problems opening the initial pointing_vertices2 file: %s\n",
//              ERROR_PREFIX, pointing_vertices_filename);
//      return 1;
//    }
//
//    for ( j = 0; j < pFieldList->nFields; j ++ )
//    {
//      pF = (field_struct*) &(pFieldList->pF[j]);
//
//      if (!(pF->in_survey & (SURVEY_ANY | SURVEY_IN_AREA_ANY)) ) continue;
//      fprintf( ppointing_vertices_file, "# Pointing %5s centre= %12.7f %12.7f PA= %8.3f flags= 0x%08x ntiles_bb= %d ntiles_dg= %d  Gal_l= %12.7f Gal_b= %+12.7f\n",
//               pF->name, pF->ra0, pF->dec0, pF->pa0, pF->in_survey,
//               pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG], pF->l0, pF->b0);
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_min2, pF->dec_min2, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_min2, pF->dec_max2, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_max2, pF->dec_max2, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_max2, pF->dec_min2, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      //print the first vertex again
//      fprintf( ppointing_vertices_file, "%12.7f %12.7f 0x%08x %6d %6d\n",
//               pF->ra_min2, pF->dec_min2, pF->in_survey, pF->ntiles_req[MOON_GROUP_BB], pF->ntiles_req[MOON_GROUP_DG] );
//      fprintf( ppointing_vertices_file, "\n");
//    }
//    fprintf(stdout, "%s I wrote out the initial pointing vertices2 to %s\n",
//            COMMENT_PREFIX, pointing_vertices_filename);    fflush(stdout);
//    if (ppointing_vertices_file) fclose(ppointing_vertices_file);
//    ppointing_vertices_file = NULL;
//  }



//  //now the output the intersection points of the FOV radii
//  if (TRUE)
//  {
//    char* pointing_vertices_filename = "pointing_intersections_plane_approx.txt";
//    FILE* ppointing_vertices_file = NULL;
//
//    if ((ppointing_vertices_file = fopen(pointing_vertices_filename, "w")) == NULL )
//    {
//      fprintf(stderr, "%s  There were problems opening the pointing_intersections file: %s\n", ERROR_PREFIX, pointing_vertices_filename);
//      return 1;
//    }
//
//    for ( j = 0; j < ppFieldList->nFields; j ++ )
//    {
//      field_struct *p = &(pField[j]);
//      double temp_ra[MAX_VERTICES_PER_TILE];
//      double temp_dec[MAX_VERTICES_PER_TILE];
//      double temp_bearing[MAX_VERTICES_PER_TILE];
//      for (k=0;k<MAX_VERTICES_PER_TILE;k++)
//      {
//        if ( isnan(p->intersect_ra[k])   || isnan(p->intersect_dec[k]) ||
//             p->intersect_ra[k] < 0.     || p->intersect_ra[k] > 360. ||
//             p->intersect_dec[k] <= -90. || p->intersect_ra[k] >= +90. )
//        {
//          temp_bearing[k] = DBL_MAX; 
//        }
//        else
//        {
//          temp_bearing[k] = calc_bearing(p->ra0,p->dec0,p->intersect_ra[k],p->intersect_dec[k]); 
//          temp_ra[k]  = p->intersect_ra[k];
//          temp_dec[k] = p->intersect_dec[k];
//        }
//      }
//      util_dddsort((unsigned long)k, temp_bearing, temp_ra, temp_dec);
//
//      fprintf( ppointing_vertices_file, "# Pointing %5s centre %12.7f %12.7f\n", p->name, p->ra0, p->dec0);
//      for (k=0;k<MAX_VERTICES_PER_TILE;k++)
//      {
//        //        if ( !isnan(p->intersect_ra[k]) && ! isnan(p->intersect_dec[k]) ) fprintf( ppointing_vertices_file, "%12.7f %12.7f\n", p->intersect_ra[k], p->intersect_dec[k] );
//        if (temp_bearing[k] < DBL_MAX) fprintf(ppointing_vertices_file, "%12.7f %12.7f\n", temp_ra[k], temp_dec[k] );
//      }
//      fprintf( ppointing_vertices_file, "\n");
//    }
//    fprintf(stdout, "%s I wrote out the pointing_intersections to %s\n", COMMENT_PREFIX, pointing_vertices_filename);    fflush(stdout);
//    if (ppointing_vertices_file) fclose(ppointing_vertices_file);
//    ppointing_vertices_file = NULL;
//
//  }


  return 0;
}
int fieldlist_calc_survey_bounds (fieldlist_struct *pFieldList, survey_struct *pSurvey )
{
  int j;
  if ( pFieldList == NULL ||
       pSurvey    == NULL   ) return 1;

  //reset the bounds - we will calc these again
  pSurvey->bounds_ra_min  = (double) 360.0;
  pSurvey->bounds_ra_max  = (double)   0.0;
  pSurvey->bounds_dec_min = (double) +90.0;
  pSurvey->bounds_dec_max = (double) -90.0;

  for ( j = 0; j < pFieldList->nFields; j++)
  {
    field_struct *pField = (field_struct*) &(pFieldList->pF[j]);
    if ( pField->in_survey & SURVEY_ANY )
    {
      if ( pField->dec_min < pSurvey->bounds_dec_min )
      {
        pSurvey->bounds_dec_min  = pField->dec_min;
        pSurvey->boundsA_dec_min = pField->dec_max;
      }
      if ( pField->dec_max > pSurvey->bounds_dec_max )
      {
        pSurvey->bounds_dec_max  = pField->dec_max;
        pSurvey->boundsA_dec_max = pField->dec_min;
      }
      if ( pField->ra_min > pField->ra_max )
      {
        pSurvey->bounds_ra_min  = (double) 0.0;
        pSurvey->bounds_ra_max  = (double) 360.0;
      }
      else
      {
        if ( pField->ra_min < pSurvey->bounds_ra_min  ) pSurvey->bounds_ra_min = pField->ra_min;
        if ( pField->ra_max > pSurvey->bounds_ra_max  ) pSurvey->bounds_ra_max = pField->ra_max;
      }
    }
  }

  
  fprintf(stdout, "%s The boundary of the survey is given by: RA,DEC = [%9.5f:%9.5f][%+9.5f:%+9.5f] (boundsA=[%+9.5f:%+9.5f]) \n",
          COMMENT_PREFIX,
          pSurvey->bounds_ra_min, pSurvey->bounds_ra_max,
          pSurvey->bounds_dec_min, pSurvey->bounds_dec_max,
          pSurvey->boundsA_dec_min, pSurvey->boundsA_dec_max);

  return 0;
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
int fieldlist_calc_dithers_by_linear_offsets (fieldlist_struct* pFieldList, survey_struct *pSurvey,  focalplane_struct* pFocal)
{
  int i;
  if ( pSurvey    == NULL ||
       pFieldList == NULL || 
       pFocal      == NULL ) return 1;

  fprintf(stdout, "%s Setting up the dithering pattern for each field: using the linear offsets method\n", COMMENT_PREFIX);
  for ( i = 0; i < pFieldList->nFields; i++)
  {
    field_struct *pField = (field_struct*) &(pFieldList->pF[i]);
    int j;
    pField->num_dithers = pSurvey->tiling_num_dithers;
    //    pField->max_dither_radius_deg = 0.0;

    for(j=0;j<pField->num_dithers;j++)
    {
      double ra,dec;
      //      double radius;


      if ( geom_project_inverse_gnomonic_from_mm (pField->ra0, pField->dec0, pField->pa0,
                                                  pSurvey->tiling_dither_offsets_x[j],
                                                  pSurvey->tiling_dither_offsets_y[j],
                                                  pFocal->invplate_scale,
                                                  (double*) &(ra),
                                                  (double*) &(dec)))
      {
        return 1;
      }
      pField->pra0[j] = ra;
      pField->pdec0[j] = dec;
      geom_ra_dec_to_gal_l_b(ra, dec, &(pField->pl0[j]), &(pField->pb0[j]));
      
      //      radius = geom_arc_dist((double) pField->ra0, (double) pField->dec0, ra, dec);
      //      if ( radius > pField->max_dither_radius_deg ) pField->max_dither_radius_deg = radius;
    }
  }
  return 0;
}




/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
//int fieldlist_calc_dithers_by_rotations (fieldlist_struct* pFieldList, survey_struct *pSurvey,  focalplane_struct* pFocal)
//{
//  int i;
//  if ( pSurvey    == NULL ||
//       pFieldList == NULL || 
//       pFocal      == NULL ) return 1;
//
//  fprintf(stdout, "%s Setting up the dithering pattern for each field: using the rotation method\n", COMMENT_PREFIX);
//  for ( i = 0; i < pFieldList->nFields; i++)
//  {
//    field_struct *pField = (field_struct*) &(pFieldList->pF[i]);
//    int j;
//    pField->num_dithers = pSurvey->tiling_num_dithers;
//    pField->max_dither_radius_deg = 0.0;
//
//    for(j=0;j<pField->num_dithers;j++)
//    {
//      double ra,dec;
//      double radius;
//      if ( geom3d_rotate_ra_dec_about_axes ((double) pField->ra0, (double) pField->dec0,
//                                            (double) pSurvey->tiling_dither_offsets_x[j], 
//                                            (double) pSurvey->tiling_dither_offsets_y[j], 
//                                            (double) pSurvey->tiling_dither_offsets_z[j], 
//                                            (double*) &ra, (double*) &dec))
//      {
//        return 1;
//      }
//      pField->pra0[j] = ra;
//      pField->pdec0[j] = dec;
//      geom_ra_dec_to_gal_l_b(ra, dec, &(pField->pl0[j]), &(pField->pb0[j]));
//      
//      radius = geom_arc_dist((double) pField->ra0, (double) pField->dec0, ra, dec);
//      if ( radius > pField->max_dither_radius_deg ) pField->max_dither_radius_deg = radius;
//    }
//  }
//  return 0;
//}



/////////////////////////////////////////////////////////////
//calculate the (conservatively large) bounds of each pointing,
//allow for the patrol radius + the dither radius, and allow a small extra tolerance,
//(should probably scrap the tolerance as we are calculating things exactly here).
//take care to deal with the discontinuities at RA=0,360 Dec=-90,+90
//be extra conservative near the poles
int fieldlist_calc_bounds (fieldlist_struct *pFieldList, focalplane_struct *pFocal, telescope_struct *pTele, survey_struct* pSurvey)
{
  int j;
  double max_dither_radius_deg;
  if ( pFieldList == NULL || 
       pFocal      == NULL ||
       pTele      == NULL ||
       pSurvey    == NULL ) return 1;
  fprintf(stdout, "%s Setting up the bounds of each field\n", COMMENT_PREFIX);
  max_dither_radius_deg = pSurvey->max_dither_radius_mm * pFocal->invplate_scale * ARCSEC_TO_DEG;
  for ( j = 0; j < pFieldList->nFields; j++)
  {
    field_struct *pField = (field_struct*) &(pFieldList->pF[j]);
    double delta_dec = FIELD_RADIUS_TOLERANCE_FACTOR*(pField->radius_deg +
                                                      pFocal->patrol_radius_max_deg +
                                                      max_dither_radius_deg);

    //if we not reading tiles from a file then start with null angle, and set it later from neighbours
    if ( pFieldList->tiling_method != TILING_METHOD_FILE ) pField->pa0 = NAN; 

    pField->dec_min = pField->dec0 - delta_dec;
    pField->dec_max = pField->dec0 + delta_dec;
    if ( pField->dec_min <= (double) -90.0 )
    {
      pField->dec_min = (double) -90.0;
      pField->ra_min  = (double)   0.0;
      pField->ra_max  = (double) 360.0;
    }
    else if ( pField->dec_max >= (double) +90.0 )
    {
      pField->dec_max = (double) +90.0;
      pField->ra_min  = (double) 0.0;
      pField->ra_max  = (double) 360.0;
    }
    else
    {
      double dec_poleward;
      double delta_ra;
      if ( fabs(pField->dec_min) > fabs(pField->dec_max) )   dec_poleward = pField->dec_min;
      else                                           dec_poleward = pField->dec_max;
      delta_ra = delta_dec / cosd(dec_poleward);
      if ( delta_ra >= 360. ) //we were obviously very close to the pole
      {
        pField->ra_min  = (double)   0.0;
        pField->ra_max  = (double) 360.0;
      }
      else
      {
        pField->ra_min = pField->ra0 - delta_ra;
        pField->ra_max = pField->ra0 + delta_ra;
        while ( pField->ra_min <   0.0 )  pField->ra_min +=  360.0;
        while ( pField->ra_max > 360.0 )  pField->ra_max -=  360.0;
      }
    }
    
    //now calc the even larger box, for neighbour finding
    delta_dec = FIELD_NEIGHBOUR_TOLERANCE_MULTIPLIER*(pField->radius_deg+max_dither_radius_deg);
    pField->dec_min2 = pField->dec0 - delta_dec;//will definitely find any neighbour pointings with this range
    pField->dec_max2 = pField->dec0 + delta_dec;
    if ( pField->dec_min2 <= (double) -90.0 )
    {
      pField->dec_min2 = (double) -90.0;
      pField->ra_min2  = (double)   0.0;
      pField->ra_max2  = (double) 360.0;
    }
    else if ( pField->dec_max2 >= (double) +90.0 )
    {
      pField->dec_max2 = (double) +90.0;
      pField->ra_min2  = (double) 0.0;
      pField->ra_max2  = (double) 360.0;
    }
    else
    {
      double dec_poleward;
      double delta_ra;
      if ( fabs(pField->dec_min2) > fabs(pField->dec_max2) ) dec_poleward = pField->dec_min2;
      else                                                   dec_poleward = pField->dec_max2;
      delta_ra = delta_dec / cosd(dec_poleward);
      if ( delta_ra >= (double) 360. ) //we were obviously very close to the pole
      {
        pField->ra_min2  = (double)   0.0;
        pField->ra_max2  = (double) 360.0;
      }
      else
      {
        pField->ra_min2 = pField->ra0 - delta_ra;
        pField->ra_max2 = pField->ra0 + delta_ra;
        while ( pField->ra_min2 < (double)   0.0 )  pField->ra_min2 += (double) 360.0;
        while ( pField->ra_max2 > (double) 360.0 )  pField->ra_max2 -= (double) 360.0;
      }
    }

    //our fibers can reach outside the radius determined by their starting positions so adjust accordingly
    pField->furthest_radius_deg         = pField->radius_deg + max_dither_radius_deg + pFocal->patrol_radius_max_deg;
    pField->cos_furthest_radius_deg     = cosd(pField->furthest_radius_deg);

    //calculate Field's minimum airmass (as sec(Z))
    pField->airmass_min = util_airmass_secz(fabs(pField->dec0 - pTele->latitude_deg));  

  }
  return 0;  
}

int  fieldlist_setup_pid_arrays (fieldlist_struct *pFieldList)
{
  int j;
  if ( pFieldList == NULL ) return 1;

  for ( j = 0; j < pFieldList->nFields; j++)
  {
    field_struct *pField = (field_struct*) &(pFieldList->pF[j]);
    if ( pField->in_survey & SURVEY_ANY)
    {
      //make enough space for the pid arrays
      pField->pid = NULL;
      pField->pid_data_length = OBJECTS_PER_PID_CHUNK;
      if ((pField->pid               = (uint*) malloc(sizeof(uint) * pField->pid_data_length)) == NULL ||
          (pField->pobject_sub_index = (int*)  malloc(sizeof(int)  * pField->pid_data_length)) == NULL )
      {
        fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
                ERROR_PREFIX, __FILE__, __LINE__);
        
        return 1;
      }
    }
  }
  return 0;
}


// ///////////////////////////////////////////////////////////////////////////////////
// //set up the Quad struct - determine required depth from field of view radius
// //we want quads to be a little bigger than the fields (so each quad contains at least one field),
// //so we use twice the diameter, and round up
// //we will work out which quad the centre of each field lies within, based on the field centre 
// int fieldlist_associate_fields_with_quads (fieldlist_struct *pFieldList, telescope_struct *pTele)
// {
//   int quad_depth_dec;
//   int quad_depth_ra;
//   int quad_depth;
//   double furthest_radius_deg;
//   quad_struct *pQuad = NULL;
//   int j;
//   if ( pFieldList == NULL ) return 1;
//   pQuad = (quad_struct*) &(pFieldList->Quad);
//   if ( pQuad == NULL ) return 1;
//   if ( pFieldList->nFields < 1 ) return 1;
// 
//   fprintf(stdout, "%s Setting up the quadtree structure associations for each field\n", COMMENT_PREFIX);
//   //may want to transfer the ra,dec limits to the unit square - deal with this later though
//   //    if ( quad_init_root (&Quad, bounds_ra_min,  bounds_ra_max,  bounds_dec_min,  bounds_dec_max))
//   if ( quad_init_root ((quad_struct*) pQuad, (double) 0.0,  (double) 360.0,  (double) -90.,  (double) +90.))
//   {
//     fprintf(stderr, "%s Error setting up Quadtree structure at file %s line %d\n",
//             ERROR_PREFIX, __FILE__, __LINE__);
//     return 1;
//   }
//   
//   //  worst case is likely to be at the equator, ie cos(dec)=1 so we use that
//   //this is an ok assumption if we do not want to go to high latitudes (>80deg)
//   //    quad_depth_ra  = (int) (-0.5 + log((bounds_ra_max  - bounds_ra_min )/(4.0*pTele->fov_radius_deg))/log(2.));
//   //    quad_depth_dec = (int) (-0.5 + log((bounds_dec_max - bounds_dec_min)/(4.0*pTele->fov_radius_deg))/log(2.));
// 
//   //old scheme - too pessimistic - relies on field neighbours - computationally intensive later
//   //dodgy near the poles
//   quad_depth_ra  = (int) (-0.5 + log(360./(4.0*pTele->fov_radius_deg))/log(2.));
//   quad_depth_dec = (int) (-0.5 + log(180./(4.0*pTele->fov_radius_deg))/log(2.));
//   quad_depth = MAX(quad_depth_ra,quad_depth_dec); //choose the worst option
// 
// //  //new scheme... we want the quads to be as small as possible whilst having linear size
// //  //just greater than pTele->furthest_radius_deg
// //  //assume the worst case, which is the delta-RA at the equator
// //  furthest_radius_deg = pFieldList->pF[0].furthest_radius_deg; // 
// //  quad_depth = 1;
// //  while (360.0*pow(0.5,quad_depth) >= furthest_radius_deg ) quad_depth++;
// //  if ( quad_depth > 1 ) quad_depth--;
//   fprintf(stdout, "%s QUAD_DEPTH new: quad_depth=%d (furthest_radius_deg=%g, quad_size=%g)\n",
//           DEBUG_PREFIX,  quad_depth, furthest_radius_deg,360.0*pow(0.5,quad_depth));
// 
//   
//   if ( quad_add_children_to_depth ((quad_struct*) pQuad, quad_depth))
//   {
//     fprintf(stderr, "%s Problem when adding %d layers of children to Quadtree structure at file %s line %d\n",
//             ERROR_PREFIX, quad_depth, __FILE__, __LINE__);
//     return 1;
//   }
// 
//   //we do not use this functionality yet, but keep it here ready for later
//   if ( quad_calc_neighbours ((quad_struct*) pQuad))
//   {
//     fprintf(stderr, "%s Problem when calculating quad neighbours at file %s line %d\n",
//             ERROR_PREFIX, __FILE__, __LINE__);
//     return 1;
//   }
// 
// 
//  
//   //now associate each field to a quad - each quad will in general have more than one field
//   //to cope with edge fields, we also see where each bounding corner of a field lands
//   //we will use the rectangular outer corners here as they are simplest to compute
//   //(and we have not yet calculated the PA of each tile).
//   for ( j = 0; j< pFieldList->nFields; j++)
//   {
//     int k;
//     field_struct *pField = (field_struct*) &(pFieldList->pF[j]);
//     //k is just used to iterate around the vertices
//     for ( k=0;k<5; k++)
//     {
//       double x, y;
//       quad_struct *pq = NULL;
//       switch (k)
//       {
//       case 0:  x = pField->ra0;    y = pField->dec0 ;    break; //mid point
//       case 1:  x = pField->ra_min; y = pField->dec_min;  break; //bottom left
//       case 2:  x = pField->ra_min; y = pField->dec_max;  break; //top left
//       case 3:  x = pField->ra_max; y = pField->dec_min;  break; //bottom right
//       case 4:  x = pField->ra_max; y = pField->dec_max;  break; //top right
//       default :
//         return 1;
//         break;
//       }
//       pq = (quad_struct*) quad_get_quad_from_position((quad_struct*)pQuad, x, y);
//       if ( pq  == NULL )
//       {
//         //this is not good - but is probably to do with high latitude points being squiffy
//         fprintf(stderr, "%s Error finding Quadtree structure for field %4d %5s centre= %12.7f %12.7f (test coord=%d,  %12.7f %12.7f)\n", ERROR_PREFIX, j, pField->name, pField->ra0, pField->dec0, k, x, y);
//         
//         //          return 1;
//       }
//       else
//       {
//         if ( quad_add_unique_member_to_quad((quad_struct*)pq, (void*) pField))
//         {
//           //this is bad
//           fprintf(stderr, "%s Error adding field to Quadtree structure for field %4d %5s centre= %12.7f %12.7f : too many members\n",
//                   ERROR_PREFIX, j, pField->name, pField->ra0, pField->dec0);
//           
//           return 1;
//         }
//       }
//     }
//   }
//   return 0;
// }



///////////////////////////////////////////////////////////////////////////////////
//set up the hpx_pixlist_struct - determine required depth from field of view radius
//
int fieldlist_associate_fields_with_hpx (fieldlist_struct *pFieldList, telescope_struct *pTele)
{
  hpx_pixlist_struct *pPixlist = NULL;
  int j;
  long i;
  //  long nside = HPX_NSIDE_FOR_ASSOCIATIONS; 
  clock_t clocks_start = clock();
  if ( pFieldList == NULL ) return 1;
  pPixlist = (hpx_pixlist_struct*) &(pFieldList->Pixlist);
  if ( pFieldList->nFields < 1 ) return 1;

  //we want pixels that are just smaller than the field of view radius
  
  fprintf(stdout, "%s Setting up the hpx pixlist structure associations for each field ...", COMMENT_PREFIX);
  fflush(stdout);
  if ( hpx_pixlist_setup ((hpx_pixlist_struct*) pPixlist, HPX_NSIDE_FOR_ASSOCIATIONS))
  {
    fprintf(stderr, "%s Error setting up  hpx pixlist at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  fprintf(stdout, " ..."); fflush(stdout);
  //now find all the hpx pixels which touch a circle centred on the field centre and with radius=furthest_radius_deg
  //associate this field with each of those pixels
  for ( j = 0; j< pFieldList->nFields; j++)
  {
    field_struct *pField = (field_struct*) &(pFieldList->pF[j]);
    // see e.g. http://healpix.jpl.nasa.gov/html/subroutinesnode72.htm#sub:query_disc
    long plistpix[HPX_MEMBERS_PER_CHUNK+1];  //TODO check that this is appropriate
    long nlist = 0;
    const BOOL nest_flag = (BOOL) TRUE;
    const BOOL inclusive_flag = (BOOL) TRUE;
    //    const BOOL inclusive_flag = (BOOL) FALSE;
    long hpx_pix_temp;
    //get the hpx pixel containing the field centre
    (void) ang2pix_nest(pPixlist->nside,
                        M_PI_2 - pField->dec0*DEG_TO_RAD,
                        pField->ra0*DEG_TO_RAD,
                        (long*) &(hpx_pix_temp));
    pField->hpx_pix = (uint) hpx_pix_temp;
    
    //now get list of all pixels potentially overlapping with field
    //    fprintf(stdout, "\n\n%s HPXTEST Field %6d %10.5f %10.5f\n", COMMENT_PREFIX,
    //            pField->id, (double) pField->ra0, (double) pField->dec0); fflush(stdout);
    if ( hpx_extras_query_disc((long) pPixlist->nside,
                               (double) pField->ra0, (double) pField->dec0, (double) pField->furthest_radius_deg,
                               (long) HPX_MEMBERS_PER_CHUNK, (long*) plistpix, (long*) &nlist ,
                               (BOOL) nest_flag, (BOOL) inclusive_flag)) return 1;
    
    if ( nlist < 1 )
    {
      fprintf(stderr, "%s Field %6d %10.5f %10.5f has %ld hpx pixels\n", WARNING_PREFIX,
              pField->id, (double) pField->ra0, (double) pField->dec0, nlist); 
      
    }
//    //    if ( nlist < 1 )
//    {
//      fprintf(stdout, "%s HPXTEST Field %6d %10.5f %10.5f has %ld hpx pixels\n", COMMENT_PREFIX,
//              pField->id, (double) pField->ra0, (double) pField->dec0, nlist); fflush(stdout);
//    }

    //add the hpx pixels to the list for the field (convert the one-based index scheme to zero-based)
    pField->hpx_npix = nlist;
    for(i=1;i<=nlist;i++) pField->hpx_pixlist[i-1] = (uint) plistpix[i];
    
   
    //now add this field to the pMembers array of each hpx pixel that the field overlaps
    for(i=1;i<=nlist;i++)
    {
      hpx_pix_struct *pPix;
      if ( plistpix[i] < 0 || plistpix[i] >= pPixlist->npix ) return 1;
      pPix = (hpx_pix_struct*) &(pPixlist->pPix[plistpix[i]]);
        
      if ( pPix->nMembers >= pPix->array_length )
      {
        pPix->array_length += HPX_MEMBERS_PER_CHUNK;

        if ( pPix->pMembers == NULL )  // assign first chunk of memory 
        {
          if ((pPix->pMembers = (void**) calloc((size_t) pPix->array_length, sizeof(void*))) == NULL )
          {
            fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
          }
        }
        else  //assign additional memory - want to avoid this as much as possible
        {    
          fprintf(stdout, "%s Too many entries (%d) in member list of pixel %ld .. adding more space\n",
                  WARNING_PREFIX, pPix->nMembers, pPix->index);
          if ((pPix->pMembers = (void**) realloc((void**) pPix->pMembers,
                                                 (size_t) pPix->array_length* sizeof(void*))) == NULL )
          {
            fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
            return 1;
          }
        }
      }
      pPix->pMembers[pPix->nMembers] = (void*) pField;
      pPix->nMembers ++;
    }
  }
  fprintf(stdout, " done (that took %.3fs)\n",
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);  fflush(stdout);

  return 0;
}



///////////////////////////////////////////////////////////////////////////////////
//Work out the proper PA for each pointing that is not already set
//take a the mean of the bearings to each of the neighbouring pointings (modulo 60deg).
//We define PA=0deg to be "point up" ie one of the verticies points northwards
//However, we want to "point" our flat edges in the directions of the neighbouring pointing centres
//so for a "mean bearing (modulo 60deg)" = 0.0deg (ie a neighbouring pointing is directly above), we require a PA of +-30deg
////////////////////////////////////////////////////////////////////////
int fieldlist_calc_PAs (fieldlist_struct *pFieldList)
{
  int i;
  if ( pFieldList == NULL ) return 1;
    
  fprintf(stdout, "%s Calculating field pointing angles from neighbours\n",  COMMENT_PREFIX);
  for (i = 0; i < pFieldList->nFields; i++)
  {
    field_struct *pF1 = (field_struct*) &(pFieldList->pF[i]);
    if (!(pF1->in_survey & SURVEY_ANY) ) continue;  //don't bother calculating PA for these pointings

    if ( !isnan(pF1->pa0) )  continue; // this field's PA has already been set e.g in input file
    
    //only consider cases where have six neighbours, 
    //this should catch every case now we are reading in the whole-sky Hardin patterns
    if (  pF1->num_neighbours == 6 ) 
    {
      int k;
      //find nearest neighbour
      //      double  nearest_dist =  (double) DBL_MAX;
      //      int     nearest_index = -1;
      //find furthest neighbour
      double  furthest_dist =  (double) -DBL_MAX;
      int     furthest_index = -1;
      for(k=0;k<pF1->num_neighbours;k++)
      {
//        if ( pF1->dist_neighbour[k] < nearest_dist)
//        {
//          nearest_dist = pF1->dist_neighbour[k];
//          nearest_index=k;
//        }
        if ( pF1->dist_neighbour[k] > furthest_dist)
        {
          furthest_dist = pF1->dist_neighbour[k];
          furthest_index=k;
        }
      }
      
      /*
      //use bearing of nearest neighbour
      //make sure that we are pointing our face towards it - ie 30 degrees away
      pF1->pa0 = pF1->bearing_neighbour[nearest_index] - (double) 30.;
      */
      
      //use bearing of furthest neighbour
      //make sure that we are pointing our face towards it - ie 30 degrees away
      pF1->pa0 = pF1->bearing_neighbour[furthest_index] - (double) 30.;

      //now normalise angle to lie within range 0->60degrees
      while (pF1->pa0  <  (double) 0.) pF1->pa0 += (double) 60.;
      while (pF1->pa0 >=  (double)60.) pF1->pa0 -=  (double) 60.;
      //      fprintf(stdout, "%s PAcalc Pointing %5s %12.7f %12.7f pa0=%8.2f neighbour=%5s %12.7f %12.7f dist=%8.3fdeg bearing=%.2f bearing=%.2f \n", 
      //DEBUG_PREFIX,pF1->name, pF1->ra0, pF1->dec0, pF1->pa0, pField[pF1->id_neighbour[nearest_index]].name,
      //pField[pF1->id_neighbour[nearest_index]].ra0,pField[pF1->id_neighbour[nearest_index]].dec0,
      //pF1->dist_neighbour[nearest_index], pF1->bearing_neighbour[nearest_index],
      //calc_bearing(pF1->ra0, pF1->dec0, pField[pF1->id_neighbour[nearest_index]].ra0,
      //pField[pF1->id_neighbour[nearest_index]].dec0)); 
    }
    else
    {
      pF1->pa0 = 0.0;
    }
  }

  return 0; 
}
  
////////////////////////////////////////////////////////////////////////
int fieldlist_calc_vertex_coords (fieldlist_struct *pFieldList, focalplane_struct *pFocal)
{
  int i;
  if ( pFieldList == NULL ||
       pFocal      == NULL ) return 1;

  for (i = 0; i < pFieldList->nFields; i++)
  {
    field_struct *pField = (field_struct*) &(pFieldList->pF[i]);
    int dither_index;
    pField-> num_vertex     = pFocal->num_vertex;
    pField-> num_ext_vertex = pFocal->num_ext_vertex;
    if (!(pField->in_survey & SURVEY_ANY) ) continue;
    for (dither_index=0; dither_index<pField->num_dithers; dither_index++)
    {
      int k;
      for (k=0;k<pField->num_vertex;k++)
      {
        geom_project_inverse_gnomonic_from_mm (pField->pra0[dither_index], pField->pdec0[dither_index], pField->pa0,
                                               pFocal->vertex_x[k],  pFocal->vertex_y[k],
                                               pFocal->invplate_scale,
                                               (double*) &(pField->vertex_ra[dither_index][k]),
                                               (double*) &(pField->vertex_dec[dither_index][k]));
      }
      for (k=0;k<pField->num_ext_vertex;k++)
      {
        geom_project_inverse_gnomonic_from_mm (pField->pra0[dither_index], pField->pdec0[dither_index], pField->pa0,
                                               pFocal->ext_vertex_x[k], pFocal->ext_vertex_y[k],
                                               pFocal->invplate_scale,
                                               (double*) &(pField->ext_vertex_ra[dither_index][k]),
                                               (double*) &(pField->ext_vertex_dec[dither_index][k]));
      }
    }
  }
  return 0;
}

  
///
int fieldlist_free_spare_field_pid_arrays (fieldlist_struct *pFieldList)
{
  int i;
  if ( pFieldList == NULL ) return 1;
  //free any spare space in each field struct
  for ( i=0; i < pFieldList->nFields; i++)
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
    if ( pF->in_survey & SURVEY_ANY )
    {
      if ( pF->n_object == 0 )
      {
        if ( pF->pid ) free (pF->pid); pF->pid = NULL;
        if ( pF->pobject_sub_index ) free (pF->pobject_sub_index) ; pF->pobject_sub_index = NULL;
        pF->pid_data_length = 0;
      }
      else
      {
        if ( pF->pid_data_length > pF->n_object)
        {
          pF->pid_data_length = pF->n_object;
          if ((pF->pid               = (uint*) realloc(pF->pid,               sizeof(uint) * pF->pid_data_length)) == NULL ||
              (pF->pobject_sub_index = (int*) realloc(pF->pobject_sub_index, sizeof(int) * pF->pid_data_length)) == NULL )
          {
            fprintf(stderr, "%s Error re-assigning memory (for field %u = %s, n_object=%d)\n",
                    ERROR_PREFIX, pF->id, pF->name, pF->n_object);
            
            return 1;
          }
        }
      }
    }
  }
  return 0;
}
//////////////////////////////////

/////////////////////////////////////////////////////
//now calculate the numbers of requested tiles per field and in total
//first the standard baseline survey - as defined by the num_tiles_per_field keywords
/////////////////////////////////////////////////////
int fieldlist_calc_requested_tiles_per_field(fieldlist_struct *pFieldList, survey_struct *pSurvey)
{
  int i,m;
  for(m=0; m<pSurvey->num_moon_groups; m++) pSurvey->moon[m].num_tiles_req = 0;
  for ( i = 0; i < pFieldList->nFields; i++)
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
    for(m=0; m<pSurvey->num_moon_groups; m++)
    {
      moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
      if ( pF->in_survey & pMoon->mask )
      {
        pF->ntiles_req[pMoon->group_code] = pMoon->num_tiles_per_field;
        pMoon->num_tiles_req += pMoon->num_tiles_per_field;
      }
      else
      {
        pF->ntiles_req[pMoon->group_code] = 0;
      }
    }
  }
  
  pSurvey->num_tiles_req_total = 0;
  for(m=0; m<pSurvey->num_moon_groups; m++) pSurvey->num_tiles_req_total += pSurvey->moon[m].num_tiles_req;
  fprintf(stdout, "%s The baseline survey request was for %8d %-8s tiles \n",
          COMMENT_PREFIX, pSurvey->num_tiles_req_total, "total");
  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    fprintf(stdout, "%s The baseline survey request was for %8d %-8s tiles \n",
            COMMENT_PREFIX, pSurvey->moon[m].num_tiles_req, pSurvey->moon[m].longname);
  }
  
  //now read in the survey tiling modifications file, if necessary
  if (strncmp(pSurvey->str_tiling_description_file, "NONE",  strlen(pSurvey->str_tiling_description_file)) != 0 )
  {
    if (fieldlist_read_tiling_description((survey_struct*) pSurvey,
                                          (fieldlist_struct*) pFieldList,
                                          (const char*) pSurvey->str_tiling_description_file))
    {
      return 1;
    }
    pSurvey->num_tiles_req_total = 0;
    for(m=0; m<pSurvey->num_moon_groups; m++) pSurvey->num_tiles_req_total += pSurvey->moon[m].num_tiles_req;
    fprintf(stdout, "%s The modified survey request was for %8d %-8s tiles \n",
            COMMENT_PREFIX, pSurvey->num_tiles_req_total, "total");
    for (m=0;m<pSurvey->num_moon_groups; m++)
    {
      fprintf(stdout, "%s The modified survey request was for %8d %-8s tiles \n",
              COMMENT_PREFIX, pSurvey->moon[m].num_tiles_req, pSurvey->moon[m].longname);
    }
    
  }
  return 0;
}
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////



/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
//return a subset of potential fields that satisfy the airmass and moon phase/distance criteria
//assume that pSort has enough space
//calling function must manage memory assigned
int fieldlist_select_candidate_fields(fieldlist_struct     *pFieldList,
                                      timeline_struct      *pTime,
                                      survey_struct        *pSurvey,
                                      sortfieldlist_struct *pSortFieldList,
                                      BOOL                  do_sort,
                                      int                   select_on_moon_phase,
                                      float                 effective_airmass_limit,
                                      BOOL                  wind_constraint_flag )
{
  int  i;
  int  moon_phase_code;
  int  moon_group_code;
  moon_struct *pMoon = NULL;
  double cos_dec_zen;
  double sin_dec_zen;
  double cos_dec_moon;
  double sin_dec_moon;
  float moon_frac_dist_thresh;
  double cos_zenith_angle_max;
  double cos_zenith_angle_min;

  if ( pFieldList == NULL ||
       pTime      == NULL ||
       pSurvey    == NULL ||
       pSortFieldList == NULL ) return 1;

  if ( pFieldList->nFields <= 0 ) return 1;
  
  if ( timeline_determine_moon_phase_code((timeline_struct*) pTime,
                                          (float*) NULL,
                                          (int*) &moon_phase_code,
                                          (int*) &moon_group_code,
                                          (survey_struct*) pSurvey,
                                          (moon_struct**) &pMoon))
  {
    fprintf(stderr, "%s Problem calculating moon_phase_code\n", ERROR_PREFIX);
    return 1;
  }
  
  
  
  //do some checks and memory allocating here 
  if ( pSortFieldList->pFields == NULL )
  {
    pSortFieldList->array_length = pFieldList->nFields;
    if ( (pSortFieldList->pFields = (sortfield_struct*) malloc(pSortFieldList->array_length * sizeof(sortfield_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  else if (pSortFieldList->array_length < pFieldList->nFields )
  {
    pSortFieldList->array_length = pFieldList->nFields;
    if ( (pSortFieldList->pFields = (sortfield_struct*) realloc(pSortFieldList->pFields, pSortFieldList->array_length*sizeof(sortfield_struct))) == NULL )
    {
      fprintf(stderr, "%s Error re-assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  //
  
  cos_dec_zen  = (double) cosd(pTime->Rnow.dec_zen);
  sin_dec_zen  = (double) sind(pTime->Rnow.dec_zen);
  cos_dec_moon = (double) cosd(pTime->Rnow.moon.dec);
  sin_dec_moon = (double) sind(pTime->Rnow.moon.dec);
  moon_frac_dist_thresh = (float) pSurvey->moon_dist_frac_min*pTime->Rnow.moon.illum_frac;
  cos_zenith_angle_max = (double) cosd(pSurvey->zenith_angle_max);
  cos_zenith_angle_min = (double) cosd(pSurvey->zenith_angle_min);
  
  pSortFieldList->nFields = 0;
  for(i=0;i<pFieldList->nFields;i++)
  {
    field_struct     *pField     = (field_struct*) &(pFieldList->pF[i]);
    sortfield_struct *pSortField = (sortfield_struct*) &(pSortFieldList->pFields[pSortFieldList->nFields]);

    //check we have at least one field member and that this field has not already been observed fully
    if ( pField->n_object > 0 )
    {
      if ( pField->ntiles_total < MAX_TILES_PER_FIELD )
      {
        //check if field is in the survey footprint
        if ( (select_on_moon_phase == FIELDLIST_SELECT_ON_MOON_PHASE_NONE) ||
             (select_on_moon_phase == FIELDLIST_SELECT_ON_MOON_PHASE_STRICT && (pField->in_survey & pMoon->mask) ) ||
             (select_on_moon_phase == FIELDLIST_SELECT_ON_MOON_PHASE_STRICT_INVERSE && !(pField->in_survey & pMoon->mask) ))
        {
          //calc and select on zenith dist
          //          pSortField->Z = (float) geom_fast_arc_dist (pField->ra0, pTime->Rnow.ra_zen,
          //                                                      pField->sin_dec0, pField->cos_dec0,
          //                                                      sin_dec_zen, cos_dec_zen);

          //faster to avoid calculating the arccos until last moment.
          double cosZ = geom_very_fast_cos_arc_dist (pField->ra0, pTime->Rnow.ra_zen,
                                                     pField->sin_dec0, pField->cos_dec0,
                                                     sin_dec_zen, cos_dec_zen);

          //          if ( pSortField->Z <= pSurvey->zenith_angle_max )//this is the airmass constraint
          if ( cosZ >= cos_zenith_angle_max )//this is the airmass constraint
          {
            //            if ( pSortField->Z >= pSurvey->zenith_angle_min ) //this is the min zenith dist constrain
            if ( cosZ <= cos_zenith_angle_min ) //this is the min zenith dist constrain
            {
              pSortField->moon_dist = (float) geom_fast_arc_dist (pField->ra0, pTime->Rnow.moon.ra,
                                                                  pField->sin_dec0, pField->cos_dec0,
                                                                  sin_dec_moon, cos_dec_moon);
              //calc and select on moon distance 
              if ( pSortField->moon_dist > moon_frac_dist_thresh )
              {
                if ( pSortField->moon_dist > pSurvey->moon_dist_min )
                {
                  pSortField->Z = acosd(cosZ);
                  pSortField->airmass = util_airmass_seczf(pSortField->Z);
                  if ( pSortField->airmass <= effective_airmass_limit )
                  {
                    //check that wind is slow or that field is >90deg from North
                    if ( wind_constraint_flag == FALSE ||
                         pField->dec0 < pTime->Rnow.dec_zen )
                    {
                      //ok, this field seems valid
                      pSortField->pField = (field_struct*) pField;
                      //increment the counter - field is not counted until this point
                      pSortFieldList->nFields ++;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  //no need to do any of the remaining steps in this function
  if ( do_sort == (BOOL) FALSE ) return 0;  

  //  pSurvey->debug_counter ++;
  
  //only need to do the following if we are also sorting the fields 
  if ( pSortFieldList->nFields > 0 ) 
  {
    sortfield_struct *pMean = &(pSortFieldList->Mean);        //sortfield_struct containing mean values of the parameters
    sortfield_struct *pMax  = &(pSortFieldList->Max);         //sortfield_struct containing max values of the parameters
    sortfield_struct *pMin  = &(pSortFieldList->Min);         //sortfield_struct containing min values of the parameters
    //initialiase the Mean sortfield_struct
    pMean->pField = NULL;
    pMean->Z = 0.0;               
    pMean->airmass = 0.0;                   
    pMean->moon_dist = 0.0;                 
    pMean->sky_brightness = 0.0;            
    pMean->visibility = 0.0;             
    pMean->fraction_of_tiles_done = 0.0;    
    pMean->priority_sum_remaining = 0.0; 
    pMean->weight = 0.0;                    

    pMin->pField = NULL;
    pMin->Z = FLT_MAX;               
    pMin->airmass =  FLT_MAX;                   
    pMin->moon_dist =  FLT_MAX;                 
    pMin->sky_brightness =  FLT_MAX;            
    pMin->visibility =  FLT_MAX;             
    pMin->fraction_of_tiles_done =  FLT_MAX;    
    pMin->priority_sum_remaining =  FLT_MAX; 
    pMin->weight =  FLT_MAX;
    
    pMax->pField = NULL;
    pMax->Z = -1.*FLT_MAX;               
    pMax->airmass =  -1.*FLT_MAX;                   
    pMax->moon_dist =  -1.*FLT_MAX;                 
    pMax->sky_brightness =  -1.*FLT_MAX;            
    pMax->visibility =  -1.*FLT_MAX;             
    pMax->fraction_of_tiles_done =  -1.*FLT_MAX;    
    pMax->priority_sum_remaining =  -1.*FLT_MAX; 
    pMax->weight =  -1.*FLT_MAX;
    for(i=0;i<pSortFieldList->nFields;i++)
    {
      int nuseful_tiles_done;
      sortfield_struct *pSortField = (sortfield_struct*) &(pSortFieldList->pFields[i]);
      field_struct     *pField     = (field_struct*) pSortField->pField;

      //this is the part of the Kriscunias&Schaefer formula that is proportional to moon distance
      pSortField->sky_brightness = timeline_calc_relative_sky_brightness ((timeline_struct*) pTime, (double) pSortField->moon_dist);

      nuseful_tiles_done = pField->ntiles_done[MOON_PHASE_DARK] + pField->ntiles_done[MOON_PHASE_GREY];
      if ( moon_group_code == MOON_PHASE_BRIGHT )  nuseful_tiles_done += pField->ntiles_done[MOON_PHASE_BRIGHT];
                                        
      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      //this is a thresholded measure of the fraction of tiles done for this field.
      //take into account dark time tiles done on bright fields but not vice versa
      if ( pField->ntiles_todo[moon_group_code] > 0 )
      {
        pSortField->fraction_of_tiles_done = (float) nuseful_tiles_done / (float) pField->ntiles_todo[moon_group_code]; 
      }
      //apply a larger multiplier to Fields with ntiles_todo = 0, to disuade the algorithm from choosing them
      //ahead of valid Fields that can be observed. Prefer fields with ntiles_req > 0
      //but if we have already started observing a field with todo=0 we should try to complete it
      else if ( pField->ntiles_req[moon_group_code] > 0 )
      {
        if ( nuseful_tiles_done > 0 ) 
        {
          pSortField->fraction_of_tiles_done = (float) nuseful_tiles_done / (float) pField->ntiles_req[moon_group_code];
          if ( nuseful_tiles_done >= pField->ntiles_req[moon_group_code] )
            pSortField->fraction_of_tiles_done *= 2.0;  //disuade further observations on completed Fields
        }
        else
          pSortField->fraction_of_tiles_done = (float) 2.0;
      }
      else
        pSortField->fraction_of_tiles_done = 5.0 + (float) nuseful_tiles_done;
      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////
      
      pSortField->visibility = (float) pField->ntiles_visible_predict[moon_group_code];
      pSortField->priority_sum_remaining = (float) pField->object_priority_remaining;   
      //      pSortField->priority_sum_remaining = (float) pField->object_priority_remaining_weighted; //old implementation   

      pMean->Z              += pSortField->Z;
      pMean->airmass        += pSortField->airmass;
      pMean->moon_dist      += pSortField->moon_dist;
      pMean->sky_brightness += pSortField->sky_brightness;
      pMean->visibility     += pSortField->visibility;
      pMean->fraction_of_tiles_done += pSortField->fraction_of_tiles_done;
      pMean->priority_sum_remaining += pSortField->priority_sum_remaining;

      if ( pMax->Z                      < pSortField->Z)                      pMax->Z                      = pSortField->Z;
      if ( pMax->airmass                < pSortField->airmass)                pMax->airmass                = pSortField->airmass;
      if ( pMax->moon_dist              < pSortField->moon_dist)              pMax->moon_dist              = pSortField->moon_dist;
      if ( pMax->sky_brightness         < pSortField->sky_brightness)         pMax->sky_brightness         = pSortField->sky_brightness;
      if ( pMax->visibility             < pSortField->visibility)             pMax->visibility             = pSortField->visibility;
      if ( pMax->fraction_of_tiles_done < pSortField->fraction_of_tiles_done) pMax->fraction_of_tiles_done = pSortField->fraction_of_tiles_done;
      if ( pMax->priority_sum_remaining < pSortField->priority_sum_remaining) pMax->priority_sum_remaining = pSortField->priority_sum_remaining;
     
      if ( pMin->Z                      > pSortField->Z)                      pMin->Z                      = pSortField->Z;
      if ( pMin->airmass                > pSortField->airmass)                pMin->airmass                = pSortField->airmass;
      if ( pMin->moon_dist              > pSortField->moon_dist)              pMin->moon_dist              = pSortField->moon_dist;
      if ( pMin->sky_brightness         > pSortField->sky_brightness)         pMin->sky_brightness         = pSortField->sky_brightness;
      if ( pMin->visibility             > pSortField->visibility)             pMin->visibility             = pSortField->visibility;
      if ( pMin->fraction_of_tiles_done > pSortField->fraction_of_tiles_done) pMin->fraction_of_tiles_done = pSortField->fraction_of_tiles_done;
      if ( pMin->priority_sum_remaining > pSortField->priority_sum_remaining) pMin->priority_sum_remaining = pSortField->priority_sum_remaining;
    }

    //calc the means
    if ( pSortFieldList->nFields > 0 )
    {
      float inv_n = (1.0 / (float) pSortFieldList->nFields);
      pMean->Z                      *= inv_n;               
      pMean->airmass                *= inv_n;                   
      pMean->moon_dist              *= inv_n;                 
      pMean->sky_brightness         *= inv_n;            
      pMean->visibility             *= inv_n;             
      pMean->fraction_of_tiles_done *= inv_n;    
      pMean->priority_sum_remaining *= inv_n; 
    }
    
    if ( sortfieldlist_calc_field_weights((sortfieldlist_struct*) pSortFieldList,
                                          (survey_struct*)   pSurvey,
                                          (moon_struct*)     pMoon,
                                          (timeline_struct*) pTime))
    {
      return 1;
    }

    //now sort on the weight entries
    //remember that higher weights  mean lower ranking, so do a normal direction sort
    {
      //explict reminder of the correct type for this function
      int(*pfunc)(const void *, const void *) = sortfieldlist_compare_structs_weight; 
      qsort((void*) pSortFieldList->pFields,
            (size_t) pSortFieldList->nFields, (size_t) sizeof(sortfield_struct),
            pfunc );
    }

    
    //write some output to allow visualisation of the field ranking values 
    if ( pSurvey->num_field_ranking_files_written < pSurvey->max_field_ranking_files)
    {
      FILE *pFile = NULL;
      char str_filename[STR_MAX];
      pSurvey->num_field_ranking_files_written ++;
      snprintf(str_filename, STR_MAX, "%s/tile_%s_%06d_ranking.txt",
               FIELD_RANKING_SUBDIRECTORY, pMoon->codename, pMoon->num_tiles_done); //pMoon->total_tiles_observed);

      if ((pFile = fopen(str_filename, "w")) == NULL )
      {
        fprintf(stderr, "%s  There were problems opening the field ranking file : %s\n",
                ERROR_PREFIX, str_filename);
        return 1;
      }
      fprintf(pFile, "#FieldRanking: This is tile=%d at sJD=%10.5f in this moon group (%s group_code=%d, nCandFields=%d)\n",
              pMoon->num_tiles_done, //pMoon->total_tiles_observed,
              (double) (pTime->JD - pSurvey->JD_start),
              pMoon->codename, pMoon->group_code, pSortFieldList->nFields);
      
      if ( sortfieldlist_print_struct ((sortfieldlist_struct*) pSortFieldList, (FILE*) pFile ) )
      {
        fprintf(stderr, "%s  There were problems writing the field ranking file : %s\n",
                ERROR_PREFIX, str_filename);
        if ( pFile ) fclose(pFile); pFile = NULL;
        return 1;
      }
      if ( pFile ) fclose(pFile); pFile = NULL;
    }
  } //if do sort and nFields>0 
  return 0;
}
/////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////
int fieldlist_setup_fields (fieldlist_struct* pFieldList,
                            survey_struct* pSurvey,
                            focalplane_struct* pFocal,
                            telescope_struct *pTele)
{
  if ( pFieldList == NULL ||
       pSurvey == NULL ||
       pFocal == NULL ||
       pTele == NULL ) return 1;

  //////////////////////////////////////////////////
  //First read in the coords of the field centers from a file  
  if ( fieldlist_read_from_file ((fieldlist_struct*)  pFieldList,
                                 (survey_struct*)     pSurvey,
                                 (focalplane_struct*) pFocal,
                                 (const char*)        pFieldList->str_filename))
  {
    fprintf(stderr, "%s I had some problems with the field coordinates file: %s\n",
            ERROR_PREFIX, pFieldList->str_filename);
    return 1;
  }
  //////////////////////////////////////////////////

  //////////////////////////////////////////////////
  //now calculate the (conservatively large) bounds of each pointing,
  if ( fieldlist_calc_bounds ((fieldlist_struct*) pFieldList,
                              (focalplane_struct*) pFocal,
                              (telescope_struct*) pTele,
                              (survey_struct*) pSurvey))
  {
    return 1;
  }
  //And now recalc the bounds of the whole survey
  if ( fieldlist_calc_survey_bounds ((fieldlist_struct*) pFieldList,
                                     (survey_struct*) pSurvey))
  {
    return 1;
  }
  //allocate chunks of memory for the list of objects associated with each field
  if ( fieldlist_setup_pid_arrays ((fieldlist_struct*) pFieldList))
  {
    return 1;
  }
//  //set up the fieldlist Quad struct
//  if ( fieldlist_associate_fields_with_quads((fieldlist_struct*) pFieldList, (telescope_struct*) pTele))
//  {
//    return 1;
//  }

   //set up the fieldlist hpx struct
  if ( fieldlist_associate_fields_with_hpx((fieldlist_struct*) pFieldList,
                                           (telescope_struct*) pTele))
  {
    return 1;
  }
 
  
//  //now output the vertices of the quadtrees to file - debug only
//  if ( pSurvey->debug )
//  {
//    if (quad_write_vertices_file ((quad_struct*) &(pFieldList->Quad), (const char*) "quad_vertices.txt"))
//    {
//      return 1;
//    }
//  }
  ///////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////////////////
  //set up the field neighbours - all field pointing centres that are within
  //two (extended) pointing radii (ie where can get overlap)
  //this takes quite a while, relatively speaking
  if ( fieldlist_setup_field_neighbours ((fieldlist_struct*) pFieldList,
                                         (const double) FIELD_NEIGHBOUR_OVERLAP_RATIO))
  {
    fprintf(stderr, "%s Problem setting up the field neighbours\n",
            ERROR_PREFIX);
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////

//  ///////////////////////////////////////////////////////////////////////////////////
//  //now go through and work out the proper PA for each pointing
//  switch ( pFieldList->tiling_method )
//  {
////  case TILING_METHOD_FILE :
////    fprintf(stdout, "%s Using field pointing angles from file: %s\n",
////            COMMENT_PREFIX, pFieldList->str_filename);
////    break;
//  default :
//    fprintf(stdout, "%s Calculating field pointing angles internally\n", COMMENT_PREFIX);
//    if ( fieldlist_calc_PAs((fieldlist_struct*) pFieldList))
//    {
//      return 1;
//    }
//    break;
//  }

  if ( pFieldList->all_PAs_were_supplied_in_input_file )
  {
    fprintf(stdout, "%s Using field pointing angles from file: %s\n",
            COMMENT_PREFIX, pFieldList->str_filename);
  }
  else
  {
    fprintf(stdout, "%s Calculating some or all field pointing angles internally\n",
            COMMENT_PREFIX);
    if ( fieldlist_calc_PAs((fieldlist_struct*) pFieldList))
    {
      return 1;
    }
  }
  
  //now calculate the dithers for each pointing,
  //  if ( fieldlist_calc_dithers_by_rotations ((fieldlist_struct*) pFieldList, (survey_struct*) pSurvey, (focalplane_struct*) pFocal))
  if ( fieldlist_calc_dithers_by_linear_offsets ((fieldlist_struct*) pFieldList,
                                                 (survey_struct*) pSurvey,
                                                 (focalplane_struct*) pFocal))
  {
    return 1;
  }

  
  //work out the coordinates of the vertices of the fields
  if ( fieldlist_calc_vertex_coords ((fieldlist_struct*) pFieldList,
                                     (focalplane_struct*) pFocal))
  {
    return 1;
  }

  //setup the ESO_OT_hz values for all fields
  {
    int i;
    float hz_norm = ESO_OT_calc_hz(pTele->latitude_deg,
                                   pTele->latitude_deg,
                                   SORTFIELDLIST_ESO_OT_HZ_AIRMASS_NORM);
    if ( FLT_ISZERO(hz_norm) )
    {
      fprintf(stderr, "%s Problem calculating ESO_OT_hz values: norm_hz = %g\n", 
              ERROR_PREFIX, hz_norm);
      return 1;

      return 1;
    }
    for(i=0;i<pFieldList->nFields;i++)
    {
      field_struct *pField  = (field_struct*) &(pFieldList->pF[i]);
      pField->ESO_OT_hz = ESO_OT_calc_hz(pTele->latitude_deg,
                                         pField->dec0,
                                         pSurvey->airmass_max);
      pField->ESO_OT_Pz = pField->ESO_OT_hz / hz_norm;
    }
  }
  
  return 0;
}






//////////////////////////////////////////////////////////////////////////////////////////////////////////
////interpret the contents of the survey tiling description file
//
//  The tiling modifications file (in ASCII) contains one entry per line of
//  format:
//
//  [SHAPE_CODE] [COORD_SYS] [MOON_PHASE] [DELTA_NTILES] [PARAM1] [PARAM2] [PARAM3] .....
//
//  Where [SHAPE_CODE] is one of:
//  CIRCLE
//  BOX
//  BOUNDS
//  
//  Where [COORD_SYS] is one of:
//  J2000
//  GAL
//  
//  Where [MOON_PHASE] is one of:
//  DARKGREY
//  BRIGHT
//  ALL
//  
//  MOON_PHASE gives the moon phase in which the additional tiles for this field should be observed.
//  If DELTA_NTILES is negative, then this number of tiles will be subtracted
//  from the allocation for this Field in this moon phase.
//  
//  Where [DELTA_NTILES] is a signed integer that increments/decrements the
//  requested number of tiles for any field having a centre that falls within the nominated area.
//  Fields which end up having a negative number of requested tiles in a given moon phase will
//  not be observed in that moon phase.
//  
//  The [PARAMi] are all given in degrees, and describe the outline of the
//  shape.
//  
//  SHAPE_CODE = CIRCLE
//  - This describes a circular region having the given centre and radius
//  PARAM1 = Longitude coordinate of circle centre
//  PARAM2 = Latitude coordinate of circle centre
//  PARAM3 = Radius of circle
//
//  SHAPE_CODE = BOX
//  - This describes a rectangular box of the given width and height
//  PARAM1 = Longitude coordinate of box centre
//  PARAM2 = Latitude coordinate of box centre
//  PARAM3 = width of box in longitude direction
//  PARAM4 = height of box in latitude direction
//  
//  SHAPE_CODE = BOUNDS
//  - This describes a region bounded by coordinate limits, in the given coordinate system
//  PARAM1 = Lower longitude limit of region
//  PARAM2 = Upper longitude limit of region
//  PARAM3 = Lower latitude limit of region
//  PARAM4 = Upper latitude limit of region
//
//  The exact behaviour of these regions at the poles of the coordinate system may require some further specification.
//  
//  The OpSim will understand cases where a region wraps around the longitude = 0/360 deg discontinuity.
//  To describe this within the above format just be sure to set the Upper longitude limit of the region
//  to be smaller than the Lower longitude limit of the region.
//
//int OpSim_input_params_read_tiling_description(survey_struct *pSurvey, fieldlist_struct *pFieldList, const char *str_filename)
int   fieldlist_read_tiling_description    (survey_struct *pSurvey, fieldlist_struct *pFieldList, const char *str_filename)
{
  FILE *pDesc_file = NULL;
  char buffer[STR_MAX];
  int old_ntiles[MAX_NUM_MOON_GROUPS];
  int new_ntiles[MAX_NUM_MOON_GROUPS];
  int m;
  int i,j;

  if (pFieldList == NULL ||
      pSurvey    == NULL )
  {
    return 1;
  }

  //count up the number of tiles before modifications
  for(m=0;m<pSurvey->num_moon_groups;m++)
  {
    old_ntiles[pSurvey->moon[m].group_code] = 0;
    for (i=0;i< pFieldList->nFields; i++)
      old_ntiles[pSurvey->moon[m].group_code] += pFieldList->pF[i].ntiles_req[pSurvey->moon[m].group_code];
  }
  fprintf(stdout, "%s I will read in the survey tiling description from file %s\n",
          COMMENT_PREFIX, str_filename);
  
  if ((pDesc_file = fopen(str_filename, "r")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the field coordinates file: %s\n",
            ERROR_PREFIX, str_filename);
    fflush(stderr);
    return 1;
  }

  while (fgets(buffer,STR_MAX,pDesc_file))
  {
    tiling_area_struct Shape;
    char buffer2[STR_MAX];
    BOOL eq_flag;
   //      fprintf(stdout, "%s Interpreting line (length=%d): %s\n", COMMENT_PREFIX, (int) strlen(buffer),buffer);
    //      fflush(stdout);
      
    Shape.shape_code = TILING_AREA_SHAPE_NONE;
    Shape.coord_sys  = TILING_AREA_COORD_SYS_NONE;
    Shape.moon_phase = MOON_GROUP_NULL;
    Shape.tile_increment = 0;
    for(i=0;i<TILING_AREA_MAX_PARAMS;i++){Shape.param[i] = NAN;}

    i = 0;
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] == ' ') i++; //step over leading white space
    if ( buffer[i] == '#' ||  buffer[i] == '\n' ) continue;  //ignore comment lines and empty lines

    //take a copy of input string
    (void) sstrncpy(Shape.str_desc, buffer, STR_MAX);
      
    //work out what shape we are dealing with
    i = 0;
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] == ' ') i++; //step over leading white space
    j = 0;
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] != ' ') {buffer2[j] = toupper(buffer[i]);i++;j++;}; 
    buffer2[j] = '\0';
      
    if      (strncmp(buffer2, "CIRCLE",  strlen(buffer2)) == 0 ) { Shape.shape_code = TILING_AREA_SHAPE_CIRCLE;}
    else if (strncmp(buffer2, "BOX",     strlen(buffer2)) == 0 ) { Shape.shape_code = TILING_AREA_SHAPE_BOX;   }
    else if (strncmp(buffer2, "BOUNDS",  strlen(buffer2)) == 0 ) { Shape.shape_code = TILING_AREA_SHAPE_BOUNDS;}
    else
    {
      fprintf(stderr, "%s I do not understand this shape entry in the tiling modifications file: ->%s<-\n",
              WARNING_PREFIX, Shape.str_desc);
      continue;
    }
    
    //now get the coord system
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] == ' ') i++; //step over any more white space
    j = 0;
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] != ' ') {buffer2[j] = toupper(buffer[i]);i++;j++;}; 
    buffer2[j] = '\0';
    
    if      (strncmp(buffer2, "J2000",  strlen(buffer2)) == 0 ) { Shape.coord_sys = TILING_AREA_COORD_SYS_J2000;}
    else if (strncmp(buffer2, "GAL",    strlen(buffer2)) == 0 ) { Shape.coord_sys = TILING_AREA_COORD_SYS_GAL;  }
    else
    {
      fprintf(stderr, "%s I do not understand this coord sys entry in the tiling modifications file: ->%s<-\n",
              WARNING_PREFIX, Shape.str_desc);
      continue;
    }

    //now get the moon phase
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] == ' ') i++; //step over any more white space
    j = 0;
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] != ' ') {buffer2[j] = toupper(buffer[i]);i++;j++;}; 
    buffer2[j] = '\0';
    
    if      (strncmp(buffer2, "BRIGHT",   strlen(buffer2)) == 0 ) { Shape.moon_phase = MOON_GROUP_BB;}
    else if (strncmp(buffer2, "DARKGREY", strlen(buffer2)) == 0 ) { Shape.moon_phase = MOON_GROUP_DG;}
    else
    {
      fprintf(stderr, "%s I do not understand this moon phase entry in the tiling modifications file: ->%s<-\n",
              WARNING_PREFIX, Shape.str_desc);
      continue;
    }


    

    // read the tile increment/decrement 
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] == ' ') i++; //step over any more white space
    if ( buffer[i] == '=' )
    {
      eq_flag = (BOOL) TRUE;
      if ( i < (int) strlen(buffer) && i < STR_MAX ) i ++;
      else
      {
        fprintf(stderr, "%s I do not understand this moon phase entry in the tiling modifications file: ->%s<-\n",
                WARNING_PREFIX, Shape.str_desc);
        continue;
      }
    }
    else eq_flag = (BOOL) FALSE;
    
    while (i < (int) strlen(buffer) && i < STR_MAX && buffer[i] == ' ') i++; //step over any more white space
    //now get the remaining params
    if ( Shape.shape_code == TILING_AREA_SHAPE_CIRCLE ) // three shape params
    {
      
      int npars = sscanf(&(buffer[i]),
                         "%d %lf %lf %lf",
                         (int*) &(Shape.tile_increment),
                         (double*) &(Shape.param[0]),
                         (double*) &(Shape.param[1]),
                         (double*) &(Shape.param[2]));
      if ( npars != 4 )
      {
        fprintf(stderr, "%s I do not understand the tile increment/params entry in the tiling modifications file: ->%s<-\n",
                WARNING_PREFIX, Shape.str_desc);
        continue;
        
      }
    }
    else if ( Shape.shape_code == TILING_AREA_SHAPE_BOX ||
              Shape.shape_code == TILING_AREA_SHAPE_BOUNDS )  //four shape params
    {
      int npars = sscanf(&(buffer[i]), "%d %lf %lf %lf %lf",
                         (int*) &(Shape.tile_increment),
                         (double*) &(Shape.param[0]),
                         (double*) &(Shape.param[1]),
                         (double*) &(Shape.param[2]),
                         (double*) &(Shape.param[3]));
      if ( npars != 5 )
      {
        fprintf(stderr, "%s I do not understand the tile increment/params entry in the tiling modifications file: ->%s<-\n",
                WARNING_PREFIX, Shape.str_desc);
        continue;
        
      }
    }
    else
    {
      //we shouldn't get to here!
      return 1;
    }
    
    //now we can apply these tiling increments to the field list.
    if (Shape.shape_code != TILING_AREA_SHAPE_NONE &&
        Shape.coord_sys  != TILING_AREA_COORD_SYS_NONE &&
        Shape.moon_phase != MOON_GROUP_NULL )
    {
      //      fprintf(stdout, "%s Interpreting shape= %d sys= %d moon= %d delta_ntiles= %d\n",
      //              COMMENT_PREFIX, Shape.shape_code, Shape.coord_sys, Shape.moon_phase, Shape.tile_increment);
      fprintf(stdout, "%s Applying tile modifications: %s", COMMENT_PREFIX, Shape.str_desc);
      
      if ( Shape.shape_code == TILING_AREA_SHAPE_CIRCLE  )
      {
        double lat0, long0, cos_lat0, sin_lat0;
        double cos_lat   = (double) cosd(Shape.param[1]);
        double sin_lat   = (double) sind(Shape.param[1]);
        for (i=0;i< pFieldList->nFields; i++)
        {
          field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
          //debug section, ignore fields outside the boundary
          if (pF->ra0  < pSurvey->debug_ra_min)  continue ;
          if (pF->ra0  > pSurvey->debug_ra_max)  continue ;
          if (pF->dec0 < pSurvey->debug_dec_min) continue ;
          if (pF->dec0 > pSurvey->debug_dec_max) continue ;

          if ( Shape.coord_sys == TILING_AREA_COORD_SYS_J2000 )
          {
            long0 = (double) pF->ra0;
            lat0  = (double) pF->dec0;
          }
          else if ( Shape.coord_sys == TILING_AREA_COORD_SYS_GAL )
          {
            long0 = (double) pF->l0;
            lat0  = (double) pF->b0;
          }
          else { return 1; } //we can never get to here
          sin_lat0 = (double) sind(lat0);
          cos_lat0 = (double) cosd(lat0);
          
          {
            //              double ad = fast_arc_dist (Shape.param[0], long0, sin_lat, cos_lat, sin_lat0, cos_lat0);
            double ad = geom_fast_arc_dist (Shape.param[0], long0, sin_lat, cos_lat, sin_lat0, cos_lat0);
            if ( ad <= Shape.param[2] )
            {
              //                fprintf(stdout, "%s Debug dist from (sys=%d) (%g,%g,%g,%g) to (%g,%g,%g,%g) = %g deg\n",
              //                        DEBUG_PREFIX, Shape.coord_sys, Shape.param[0], Shape.param[1],sin_lat,cos_lat,
              //                        long0,lat0,sin_lat0,cos_lat0,
              //                        ad); 
              //if we are inside the shape, then increment the number of tiles accordingly
              if ( eq_flag == TRUE )
                pF->ntiles_req[Shape.moon_phase] = Shape.tile_increment;
              else
                pF->ntiles_req[Shape.moon_phase] += Shape.tile_increment;
            }
          }
        }
      }
      else if ( Shape.shape_code == TILING_AREA_SHAPE_BOX )
      {
        //need to make sure that we use the correct width at this latitude
        double lat0, long0, cos_lat0;
        double lat_min, lat_max;
        double long_min, long_max;
        lat_min = MAX(-90., Shape.param[1] - 0.5 * Shape.param[3]);
        lat_max = MIN(+90., Shape.param[1] + 0.5 * Shape.param[3]);
        for (i=0; i<pFieldList->nFields; i++)
        {
          field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
          //debug section ignore fields outside survey region
          if (pF->ra0  < pSurvey->debug_ra_min)  continue ;
          if (pF->ra0  > pSurvey->debug_ra_max)  continue ;
          if (pF->dec0 < pSurvey->debug_dec_min) continue ;
          if (pF->dec0 > pSurvey->debug_dec_max) continue ;
          if ( Shape.coord_sys == TILING_AREA_COORD_SYS_J2000 )
          {
            long0 = pF->ra0;
            lat0  = pF->dec0;
          }
          else if ( Shape.coord_sys == TILING_AREA_COORD_SYS_GAL )
          {
            long0 = pF->l0;
            lat0  = pF->b0;
          }
          else { return 1; } //we can never get to here
          cos_lat0 = cosd(lat0);

          //now do the tests
          if ( cos_lat0 == (double) 0.0 ) continue;
          if ( lat0 >= lat_min && lat0 <= lat_max ) //test if within latitude limits first
          {
            long_min = Shape.param[0] - 0.5*Shape.param[2]/cos_lat0;
            while ( long_min < (double) 0.0 ) long_min += (double) 360.0;
            long_max = Shape.param[0] + 0.5*Shape.param[2]/cos_lat0;
            while ( long_max > (double) 360.0 ) long_max -= (double) 360.0;

            if (long_min < long_max ) //long_min is < long_max - normal inclusion test
            {
              if ( long0 >= long_min && long0 <= long_max ) //test if within longitude limits
              {
                //if we are inside the shape, then increment the number of tiles accordingly
                if ( eq_flag == TRUE )
                  pF->ntiles_req[Shape.moon_phase] = Shape.tile_increment;
                else
                  pF->ntiles_req[Shape.moon_phase] += Shape.tile_increment;
              }
            }
            else //long_min is > long_max - so do an inclusion test - wrapped around longitude=0/360deg
            {
              if ( long0 >= long_min || long0 <= long_max ) //test if within longitude limits 
              {
                //if we are inside the shape, then increment the number of tiles accordingly
                if ( eq_flag == TRUE )
                  pF->ntiles_req[Shape.moon_phase] = Shape.tile_increment;
                else
                  pF->ntiles_req[Shape.moon_phase] += Shape.tile_increment;
              }
            }
          }
        }
      }
      else if ( Shape.shape_code == TILING_AREA_SHAPE_BOUNDS )
      {
        double lat0, long0;
        double long_min,long_max,lat_min,lat_max;
        long_min = Shape.param[0];
        long_max = Shape.param[1];
        lat_min  = Shape.param[2];
        lat_max  = Shape.param[3];
        for (i=0; i<pFieldList->nFields; i++)
        {
          field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
          //debug section
          if (pF->ra0  < pSurvey->debug_ra_min)  continue ;
          if (pF->ra0  > pSurvey->debug_ra_max)  continue ;
          if (pF->dec0 < pSurvey->debug_dec_min) continue ;
          if (pF->dec0 > pSurvey->debug_dec_max) continue ;
          if ( Shape.coord_sys == TILING_AREA_COORD_SYS_J2000 )
          {
            long0 = pF->ra0;
            lat0  = pF->dec0;
          }
          else if ( Shape.coord_sys == TILING_AREA_COORD_SYS_GAL )
          {
            long0 = pF->l0;
            lat0  = pF->b0;
          }
          else { return 1; } //we can never get to here

          //now do the tests
          if ( lat0 >= lat_min &&  lat0 <= lat_max ) //test if within latitude limits first
          {
            if (long_min < long_max ) //long_min is < long_max - normal inclusion test
            {
              if ( long0 >= long_min && long0 <= long_max ) //test if within longitude limits
              {
                //if we are inside the shape, then increment the number of tiles accordingly
                if ( eq_flag == TRUE )
                  pF->ntiles_req[Shape.moon_phase] = Shape.tile_increment;
                else
                  pF->ntiles_req[Shape.moon_phase] += Shape.tile_increment;
              }
            }
            else //long_min is > long_max - so do an inclusion test - wrapped around longitude=0/360deg
            {
              if ( long0 >= long_min || long0 <= long_max ) //test if within longitude limits 
              {
                //if we are inside the shape, then increment the number of tiles accordingly
                if ( eq_flag == TRUE )
                  pF->ntiles_req[Shape.moon_phase] = Shape.tile_increment;
                else
                  pF->ntiles_req[Shape.moon_phase] += Shape.tile_increment;
              }
            }
          }
        }
      }
      else
      {
        //shouldn't get to here
      }
    }
  }//end of loop over the lines in the input file

  //count up the number of tiles after any modifications
  for(m=0;m<pSurvey->num_moon_groups;m++)
  {
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    new_ntiles[pMoon->group_code] = 0;
    for (i=0;i< pFieldList->nFields; i++)
    {
      field_struct* pF = (field_struct*) &(pFieldList->pF[i]);
      //      if ( !(pF->in_survey & SURVEY_ANY) ) continue;
      //threshold the number of tiles per field to zero at a minimum
      //      //also make sure that the in_survey flags are up to date
      if ( pF->ntiles_req[pMoon->group_code] <= 0 )
      {
        pF->ntiles_req[pMoon->group_code] = 0;
        pF->in_survey = (unsigned int) (pF->in_survey & ~(pMoon->mask));  //flag as out of the survey
      }
      else
      {
        pF->in_survey = (unsigned int) (pF->in_survey | pMoon->mask);  //flag as in the survey area
      }
      new_ntiles[pMoon->group_code] += pF->ntiles_req[pMoon->group_code];
    }
  }

  fprintf(stdout, "%s The following modifications were made to the numbers of tiles:\n", COMMENT_PREFIX);
  for(m=0;m<pSurvey->num_moon_groups;m++)
  {
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    fprintf(stdout, "%s Moon phase %-10s -> delta_tiles= %+6d\n",
            COMMENT_PREFIX, pMoon->longname,  new_ntiles[pMoon->group_code] - old_ntiles[pMoon->group_code]);

    //apply these deltas to the survey structure
    //the indexing is different beween the local array and the pSurvey->moon array, so take care
    pMoon->num_tiles_req = new_ntiles[pMoon->group_code];
  }

  if ( pDesc_file ) fclose ( pDesc_file) ;
  pDesc_file = NULL;
  return 0;
}
/////////////////////////////////////////////////////

//return the sub index of pField within pObj->pfield_ids[]
int get_field_sub_index ( object_struct *pObj, field_struct *pField )
{
  int i;
  if ( pObj == NULL || pField == NULL ) return -1;
  for(i=0;i<pObj->num_fields;i++)
  {
    //    if ( (uint) pObj->pfield_ids[i] == (uint) pField->id ) return i;
    if ( (uint) pObj->pObjField[i].field_id == (uint) pField->id ) return i;
  }
  return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//int add_object_to_field (field_struct *pF, object_struct* pObj, float x, float y, utiny flag )
int add_object_to_field (field_struct *pF, object_struct* pObj, survey_struct *pSurvey) //, utiny hexagon_flag )
{
  if (pF == NULL || pObj == NULL || pSurvey == NULL ) return -1;
  //set the per-object params 
//  if ( pObj->num_fields >= MAX_FIELDS_PER_OBJECT )
//  {
//    fprintf(stderr, "%s Too many fields for this object: %u\n", ERROR_PREFIX, pObj->id);
//    return 1;
//  }
//  pObj->pfield_ids[pObj->num_fields] = pF->id;
//  pObj->pperim_flags[pObj->num_fields] = (utiny) hexagon_flag;
    
  
  ///////////////////////////////////////////////////////////////////////////////
  
  //set the per-field params
  //check if need more space
  if (pF->n_object >= pF->pid_data_length)
  {
    pF->pid_data_length += OBJECTS_PER_PID_CHUNK;
    if ((pF->pid               = (uint*) realloc(pF->pid,               sizeof(uint) * pF->pid_data_length)) == NULL ||
        (pF->pobject_sub_index = (int*)  realloc(pF->pobject_sub_index, sizeof(int)  * pF->pid_data_length)) == NULL)
    {
      fprintf(stderr, "%s Error re-assigning memory (for pointing %u = %s, n_object=%d)\n",
              ERROR_PREFIX, pF->id, pF->name, pF->n_object);
        
        return 1;
    }
  }
  
  pF->pid[pF->n_object] = (uint) pObj->id;
  pF->pobject_sub_index[pF->n_object] = pObj->num_fields;
  pF->n_object ++;
  //  if ( pObj->pCat->code != CATALOGUE_CODE_S8 ) //BAO does not contribute to summed priority
  {
    if ( pObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME ) //ignore the totally impossible targets
    {
      //we can expect that priorities have been normalised by this point
      pF->object_priority_total += pObj->priority;  
      pF->object_priority_remaining = pF->object_priority_total;

      //divide an object's priority amonst the fields that can see it (normally this is just 1 field)
      pF->object_priority_total_weighted += pObj->priority / (float) MAX(1,pObj->num_fields);  
      pF->object_priority_remaining_weighted = pF->object_priority_total_weighted;

    }
  }
  //sum the exposure times as well:
  if ( pObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME )
  {
    if      ( pObj->res == RESOLUTION_CODE_LOW  )   pF->object_dark_fhrs_lores += MINS_TO_HOURS * pObj->treq[MOON_PHASE_DARK] / (float) MAX(1,pObj->num_fields);
    else if ( pObj->res == RESOLUTION_CODE_HIGH )   pF->object_dark_fhrs_hires += MINS_TO_HOURS * pObj->treq[MOON_PHASE_DARK] / (float) MAX(1,pObj->num_fields);
  }
  

  pObj->num_fields ++;

  if      ( pObj->res == RESOLUTION_CODE_LOW  )   pF->n_object_lores ++;
  else if ( pObj->res == RESOLUTION_CODE_HIGH )   pF->n_object_hires ++;

  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////

int field_calc_object_priority_totals ( field_struct *pField, objectlist_struct* pObjList, survey_struct *pSurvey )
{
  int i;
  if ( pField == NULL || pObjList == NULL ) return 1;


  //  if ( pField->texp_total_done > 0.0 ) //if observations of the field have been started
  {
    //initialise the counters for fields
    pField->object_priority_total     = 0.0;
    pField->object_priority_completed = 0.0;
    pField->object_priority_remaining = 1.0;
    pField->object_priority_total_weighted      = 0.0;
    pField->object_priority_completed_weighted  = 0.0;
    pField->object_priority_remaining_weighted  = 1.0;
    if ( pField->pid )
    {
      for ( i=0;i< pField->n_object; i++)
      {
        object_struct *pObj = (object_struct*) &(pObjList->pObject[pField->pid[i]]);
        
        //        if ( pObj->pCat->code != CATALOGUE_CODE_S8 ) //BAO targets do not contribute to summed priority in field
        {
          if ( pObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME ) //ignore the totally impossible targets
          {
            float prio_weighted = pObj->priority / (float) MAX(1,pObj->num_fields);  
            pField->object_priority_total          += pObj->priority;
            pField->object_priority_total_weighted += prio_weighted;
            if ( pObj->TexpFracDone >= 1.0 )
            {
              pField->object_priority_completed          += pObj->priority;
              pField->object_priority_completed_weighted += prio_weighted;
            }
            else
            {
              pField->object_priority_remaining          += pObj->priority;
              pField->object_priority_remaining_weighted += prio_weighted;
            }
          }
        }
      }
    }
  }
  return 0;  
}

int fieldlist_calc_object_priority_totals ( fieldlist_struct *pFieldList, objectlist_struct* pObjList, survey_struct *pSurvey  )
{
  int i;
  if ( pFieldList == NULL || pObjList == NULL ) return 1;

  for ( i=0;i< pFieldList->nFields; i++)
  {
    if ( field_calc_object_priority_totals ( (field_struct*) &(pFieldList->pF[i]),
                                             (objectlist_struct*) pObjList,
                                             (survey_struct*) pSurvey  ))
    {
      return 1;
    }
  }
  return 0;  
}





//wrapper for qsort comparison function - normal sort
int sortfieldlist_compare_structs_weight (const void *pSortField1, const void *pSortField2)
{
  if ( pSortField1 == NULL || pSortField2 == NULL ) return 0;
  if      (((sortfield_struct*) pSortField1)->weight < ((sortfield_struct*) pSortField2)->weight) return -1;
  else if (((sortfield_struct*) pSortField1)->weight > ((sortfield_struct*) pSortField2)->weight) return  1;
  else return 0;
}
//wrapper for qsort comparison function - reversed sort
int sortfieldlist_compare_structs_weight_reverse (const void *pSortField1, const void *pSortField2)
{
  if ( pSortField1 == NULL || pSortField2 == NULL ) return 0;
  if      (((sortfield_struct*) pSortField1)->weight > ((sortfield_struct*) pSortField2)->weight) return -1;
  else if (((sortfield_struct*) pSortField1)->weight < ((sortfield_struct*) pSortField2)->weight) return  1;
  else return 0;
}


float sortfieldlist_calc_setting_time(double ra, double st_dusk, float hz )
{
  return 0.0;
}



/////////////////////////////////////////////////////////
// fields with small field weights are prioritised 
//
// Field_weight_i ~=  (AM_i^alpha)
//                  * (10^-0.4*(SB_i - SB_mean))
//                  * (MD_mean / MD_i) 
//                  * (PS_mean / PS_i)
//                  * (Vis_i / Vis_mean )
//                  * (1 - TileFracDone_i)^beta
//                  * LastFieldBonus_i
//
// Then blur with a gaussian, nominal sigma = 0 


// AM_i = airmass of field
// alpha = adjustable exponent, nominal value = 1.25
// SB_i = Sky Brightness at field location
// MD_i = moon distance from field
// PS_i = Summed priority of uncompleted targets in field
// Vis_i = Number of visibility windows for field over full survey
// TileFracDone_i = Fraction of planned tiles that have been executed so far on field
// beta = adjustable exponent, nominal value = 1.0
// LastFieldBonus_i = bonus given to field when it was the last oe observed, nominal value = 0.2

int sortfieldlist_calc_field_weights_standard(sortfieldlist_struct* pSortFieldList, survey_struct* pSurvey, moon_struct *pMoon) 
{
  int i;
  sortfield_struct *pMean = NULL;
  float FD_weight_when_complete;
  if ( pSortFieldList == NULL || pSurvey == NULL || pMoon == NULL) return 1;
  pMean = (sortfield_struct*) &(pSortFieldList->Mean);
  if ( pMean == NULL ) return 1;
  pMean->weight = 0.0;

  FD_weight_when_complete = MAX(pow(0.1, pSurvey->field_weighting_complete_fields_exponent),
                                pow(0.9, pSurvey->field_weighting_complete_fields_exponent));
      
  
  for(i=0;i<pSortFieldList->nFields;i++)
  {
    sortfield_struct *pSortField = (sortfield_struct*) &(pSortFieldList->pFields[i]);
    pSortField->weight =
      pow(pSortField->airmass,FIELD_FOM_PLINDEX_AIRMASS) *                      //fields with small airmasses do better
      pow(10.0, -0.4*(pSortField->sky_brightness - pMean->sky_brightness)) *    //fields with darker skies do better
      (CLIP(pMean->moon_dist,1.0,90.0)/CLIP(pSortField->moon_dist,1.0,90.0)) *  //fields further from moon do better
      (CLIP(pMean->priority_sum_remaining,0.1,FLT_MAX)/CLIP(pSortField->priority_sum_remaining,0.1,FLT_MAX)); //fields with more high priority targets remaining do better
    //only consider visibility for fields that are being observed in the correct moon phase
    if (pSortField->pField->ntiles_req[pMoon->group_code] > 0)
    {
      pSortField->weight *=
        (CLIP(pSortField->visibility,1.0,FLT_MAX)/CLIP(pMean->visibility,1.0,FLT_MAX)); // field that are less visible do better
    }

    //this weighting has more effect when |exponent| is large
    //if exponent > 0 then fields that have already been started do better- ie favour deep coverage
    //if exponent < 0 then fields with a lower completed fraction do better - ie favour wide coverage
    if ( pSortField->fraction_of_tiles_done < 1.0 )
    {
      pSortField->weight *= pow( MAX(1.0-pSortField->fraction_of_tiles_done,0.1), 
                                 pSurvey->field_weighting_complete_fields_exponent); 
    }
    else
    {
      //always give poor weighting to fields that have already been completed
      //depending on the sense of the exponent, set them to have at least twice the
      //weight value that an unstarted/completed field should have 
      pSortField->weight *= 2.0 * FD_weight_when_complete;
      //      if (pSortField->pField->ntiles_todo[pMoon->group_code] > 0 ||
      //          pSortField->pField->ntiles_req[pMoon->group_code] == 0 )
      //      {
      //  //further discourage over-exposure on completed todo fields and unrequested fields (which should have frac>=5)
      pSortField->weight *= pow(pSortField->fraction_of_tiles_done,2.);
        //}
      
    }

    
    if (pSortFieldList->pLastField )
    {
      if ( (field_struct*) pSortFieldList->pLastField ==  (field_struct*) pSortField->pField)
      {
        if ( !isnan(pSurvey->field_weighting_last_field_bonus_factor))
        {
          if ( pSortField->fraction_of_tiles_done < 1.0 )
          {
            
            if ( pSortFieldList->last_field_count < pMoon->num_tiles_per_pass ) //increase priority
              pSortField->weight *= pSurvey->field_weighting_last_field_bonus_factor; 
            else                                                                //decrease priority
              pSortField->weight *= SORTFIELDLIST_STANDARD_PASS_FINISHED_FACTOR;  //
          }
        }
      }
    }

    if ( pSurvey->field_weighting_blur_sigma > 0.0 ) //apply some blurring
    {
      pSortField->weight *= util_rand_gaussianf((float) 1.0, (float) pSurvey->field_weighting_blur_sigma);
    }
    pMean->weight += pSortField->weight;

  }
  if ( pSortFieldList->nFields > 0 )  pMean->weight = (pMean->weight / (float) pSortFieldList->nFields);
  return 0;
}

int sortfieldlist_calc_field_weights_visibility(sortfieldlist_struct* pSortFieldList, survey_struct* pSurvey, moon_struct *pMoon) 
{
  int i;
  sortfield_struct *pMean = NULL;
  if ( pSortFieldList == NULL || pSurvey == NULL  || pMoon == NULL) return 1;
  pMean = (sortfield_struct*) &(pSortFieldList->Mean);
  if ( pMean == NULL ) return 1;

  for(i=0;i<pSortFieldList->nFields;i++)
  {
    sortfield_struct *pSortField = (sortfield_struct*) &(pSortFieldList->pFields[i]);
    //    pSortField->weight = CLIP(pSortField->visibility,1.0,FLT_MAX)/CLIP(pMean->visibility,1.0,FLT_MAX) ;
    pSortField->weight = MAX(pSortField->visibility,1.0)/MAX(pMean->visibility,1.0) ;

    if (pSortFieldList->pLastField )
    {
      if ( (field_struct*) pSortFieldList->pLastField ==  (field_struct*) pSortField->pField)
      {
        if ( !isnan(pSurvey->field_weighting_last_field_bonus_factor))
        {
          if ( pSortField->fraction_of_tiles_done < 1.0 )
          {
            if ( pSortFieldList->last_field_count < pMoon->num_tiles_per_pass ) //increase priority
              pSortField->weight *= pSurvey->field_weighting_last_field_bonus_factor; 
            else                                                                //decrease priority
              pSortField->weight *= 10.0;  //todo - make this an adjustable parameter
          }
        }
      }
    }
    if ( pSurvey->field_weighting_blur_sigma > 0.0 ) //apply some blurring
    {
      pSortField->weight += util_rand_gaussianf((float) 0.0, (float) pSurvey->field_weighting_blur_sigma);
    }
  }

  return 0;
}


int sortfieldlist_calc_field_weights_ESO_OT(sortfieldlist_struct* pSortFieldList, survey_struct* pSurvey, moon_struct *pMoon, timeline_struct *pTime) 
{
  int i;
  double st_dusk; //local sidereal time at dusk
  sortfield_struct *pMean = NULL;
  sortfield_struct *pMax  = NULL;
  sortfield_struct *pMin  = NULL;
  telescope_struct *pTele = NULL;
  if ( pSortFieldList == NULL || pSurvey == NULL || pTime == NULL || pMoon == NULL ) return 1;
  pMean = (sortfield_struct*) &(pSortFieldList->Mean);
  pMean->weight = 0.0;
  pMax = (sortfield_struct*) &(pSortFieldList->Max);
  pMin = (sortfield_struct*) &(pSortFieldList->Min);

  pTele = (telescope_struct*) pTime->pTele;
  if ( pTele == NULL ) return 1;

  
  st_dusk = (DEG_TO_RA_HOURS * pTime->Rnow.ra_zen) - DAYS_TO_HOURS * (pTime->Rnow.JD - pTime->Rnow.JD_twi_eve);
  while ( st_dusk >= 24.0 ) st_dusk -= 24.0;  
  while ( st_dusk <   0.0 ) st_dusk += 24.0;  
  for(i=0;i<pSortFieldList->nFields;i++)
  {
    sortfield_struct *pSortField = (sortfield_struct*) &(pSortFieldList->pFields[i]);
    field_struct *pField = (field_struct*) pSortField->pField;
    float observability = 0.0;
    int   field_tile_priority;
    float field_target_priority;
    int nuseful_tiles_done;
    observability = 10.0
      * ESO_OT_calc_Pseeing((environment_struct*) &(pTele->environ), pField->airmass_min, pSurvey->IQ_max)
      * pField->ESO_OT_Pz
      //ignore sky transparency here
      //* ESO_OT_Psky((environment_struct*) &(pTele->environ), (int) pSurvey->cloud_cover_code_max)
      //ignore moon fraction here
      //* ESO_OT_Pfli()
      * ESO_OT_calc_Psetting((double) pField->ra0, (double) st_dusk, (float) pField->ESO_OT_hz );
    pSortField->weight = 10. * (float) roundf(observability); //round to nearest integer then multiply by 10 

    //apply some thresholds
    if ( pSortField->weight > 100. ) pSortField->weight = 100.;
    if ( pSortField->weight <   0. ) pSortField->weight =   0.;

    //now calc the secondary weight - i.e. relative priority between the fields
    //fields with more runobserved "todo" tiles left do better
    //just use a 1,2,3 ranking (equiv to a,b,c grades)

    nuseful_tiles_done = pField->ntiles_done[MOON_PHASE_DARK] + pField->ntiles_done[MOON_PHASE_GREY];
    if ( pMoon->group_code == MOON_PHASE_BRIGHT )  nuseful_tiles_done += pField->ntiles_done[MOON_PHASE_BRIGHT];

    if ( pField->ntiles_todo[pMoon->group_code] > 0 ) 
    {
      if ( nuseful_tiles_done < pField->ntiles_todo[pMoon->group_code] )
      {
        if ( nuseful_tiles_done > 0 ) field_tile_priority = 1;
        else                          field_tile_priority = 2;
      }
      else field_tile_priority = 3;
    }
    else field_tile_priority = 3;
    pSortField->weight += 0.1 * (float) field_tile_priority;

    //now calc the secondary weight - i.e. relative priority between the fields
    //fields with more high priority targets remaining do better
    if ( pMax->priority_sum_remaining > 0.0 )
    {
      field_target_priority = (pMax->priority_sum_remaining - pSortField->priority_sum_remaining)
        /pMax->priority_sum_remaining; 
      pSortField->weight += 0.01*field_target_priority;
    }

    //keep a record of the average weight
    if ( pSortField->weight > pMax->weight ) pMax->weight = pSortField->weight;
    if ( pSortField->weight < pMin->weight ) pMin->weight = pSortField->weight;
    pMean->weight += pSortField->weight;
  }
  pMean->weight = (pMean->weight / (float) MAX(1,pSortFieldList->nFields));
  return 0;
}




//in general a higher weight gives a lower ranking
//this is oppposite to previous sense.
int sortfieldlist_calc_field_weights(sortfieldlist_struct* pSortFieldList, survey_struct* pSurvey, moon_struct *pMoon, timeline_struct* pTime) 
{
  if ( pSortFieldList == NULL || pSurvey == NULL || pMoon == NULL) return 1;
  //now calculate the weight values
  switch ( pSurvey->field_weighting_method )
  {
  case FIELD_WEIGHTING_METHOD_ESO_OT :
    if ( pTime == NULL ) return 1;
    sortfieldlist_calc_field_weights_ESO_OT((sortfieldlist_struct*) pSortFieldList, (survey_struct*) pSurvey, (moon_struct*) pMoon, (timeline_struct*) pTime);
    break;
  case FIELD_WEIGHTING_METHOD_STANDARD :
    sortfieldlist_calc_field_weights_standard((sortfieldlist_struct*) pSortFieldList, (survey_struct*) pSurvey, (moon_struct*) pMoon);
    break;
  case FIELD_WEIGHTING_METHOD_VISIBILITY :
    sortfieldlist_calc_field_weights_visibility((sortfieldlist_struct*) pSortFieldList, (survey_struct*) pSurvey, (moon_struct*) pMoon);
    break;
  default : return 1;
    break;
  }

  return 0;
}


int sortfieldlist_print_struct (sortfieldlist_struct *pSortFieldList, FILE *pFile )
{
  sortfield_struct *pSF = NULL;
  //  field_struct     *pF = NULL;
  int i;
  if ( pSortFieldList == NULL || pFile == NULL ) return 0;

  
  fprintf(pFile, "#FieldRanking: %4s %10s %10s %9s %9s %6s %6s %6s %6s %8s %6s %10s %8s %8s %5s\n",
          "rank", "Weight", "Fname", "F_ra0", "F_dec0", "Z", "AM", "Mdist", "SkyBr", "Vis", "FrDone", "PriSum", "VisDG", "VisBB", "last?");
  for(i=0;i<pSortFieldList->nFields;i++)
  {
    pSF = (sortfield_struct*)  &(pSortFieldList->pFields[i]);
    field_struct *pF  = (field_struct*) pSF->pField;
    
    fprintf(pFile, "FieldRanking:  %4d %10.4f %10s %9.3f %+9.3f %6.2f %6.3f %6.1f %6.2f %8.1f %6.3f %10.3f %8.1f %8.1f %5s\n",
            i+1, pSF->weight,
            pF->name, pF->ra0, pF->dec0,
            pSF->Z, pSF->airmass, pSF->moon_dist,
            pSF->sky_brightness, pSF->visibility, pSF->fraction_of_tiles_done,
            pSF->priority_sum_remaining,
            pF->ntiles_visible_predict[MOON_GROUP_DG],
            pF->ntiles_visible_predict[MOON_GROUP_BB],
            ((field_struct*) pF == (field_struct*) (pSortFieldList->pLastField) ? "TRUE":"FALSE"));
    
  }

  pSF = (sortfield_struct*) &(pSortFieldList->Mean);
  fprintf(pFile, "FieldRanking:  %4s %10.4f %10s %9s %9s %6.2f %6.3f %6.1f %6.2f %8.1f %6.3f %10.3f %5s\n",
          "MEAN", pSF->weight,
          "-", "-", "-",
          pSF->Z, pSF->airmass, pSF->moon_dist,
          pSF->sky_brightness, pSF->visibility, pSF->fraction_of_tiles_done,
          pSF->priority_sum_remaining,
          "-");

  return 0;
}


