# --------------------------------------------------------------
# This is an example input parameter file for the 4FS OpSim task.
# A full description of the parameters in this file is givven in the
# document "4MOST Facility Simulator Interfaces Format Definition" 
# VLT-SPE-MST-14625-0009.
#
# Comment lines start with a '#' symbol
# Author: T.Dwelly (dwelly@mpe.mpg.de)
# CVS_REVISION "$Revision: 1.1 $"
# CVS_DATE     "$Date: 2013/01/30 10:43:38 $"
# --------------------------------------------------------------

SIM.CODE_NAME                    = "Example Parameter file version"
SURVEY.MAIN_MODE                 = "TIMELINE"
SURVEY.SKYCALC.FILENAME          = "/home/4most/4most/run_skycalc/skycalc_out2_VISTA_proc.txt"

#Telescope and focal plane parameters
TELESCOPE.CODE_NAME                    = "VISTA"
TELESCOPE.FOV_AREA                     = 4.0   # square degrees in FoV 
TELESCOPE.PLATE_SCALE                  = 0.0595  # mm/arcsec
TELESCOPE.EXPOSURE_SCALING             = 1.0     # how to rescale target exposure times

#Positioner geometry and layout parameters
#All positioner measurements are in mm
POSITIONER.LAYOUT_FILENAME             = "NONE"
POSITIONER.LAYOUT_SHAPE                = "HEXAGON"
POSITIONER.SPACING_LOW_RES             = 10.0  
POSITIONER.SPACING_HIGH_LOW_PATTERN    = "ONE_IN_N"
POSITIONER.SPACING_HIGH_LOW_RATIO      = 3

POSITIONER.CODE_NAME                   = "AESOP"
POSITIONER.FAMILY                      = "ECHIDNA"
POSITIONER.PATROL_RADIUS_MAX           =  12.0
POSITIONER.PATROL_RADIUS_MIN           =   0.0
POSITIONER.MINIMUM_SEPARATION          =   0.0
POSITIONER.ECHIDNA.SPINE_DIAMETER      =   0.5
POSITIONER.ECHIDNA.SPINE_LENGTH        = 250.0   

#Environmental overheads and parameters
ENVIRONMENT.CLOUD.FRACTION_PHOTOMETRIC = 0.83
ENVIRONMENT.CLOUD.FRACTION_CLEAR       = 0.00
ENVIRONMENT.CLOUD.FRACTION_THIN        = 0.00
ENVIRONMENT.CLOUD.FRACTION_THICK       = 0.00
ENVIRONMENT.CLOUD.FRACTION_CLOSED      = 0.17      

ENVIRONMENT.SEEING.PROBABILITY         = "0.10 0.10 0.10 0.20 0.40 0.05 0.05"
ENVIRONMENT.SEEING.THRESH_LOW          = "0.40 0.40 0.50 0.60 0.80 1.00 1.20"
ENVIRONMENT.SEEING.THRESH_HIGH         = "0.40 0.50 0.60 0.80 1.00 1.20 2.00"

ENVIRONMENT.WIND.FRACTION_CONSTRAINED  = 0.18  # fraction of time when can only point towards South
ENVIRONMENT.WIND.FRACTION_DOME_CLOSED  = 0.03

#Overhead parameters
OVERHEADS.FAULT_NIGHTS                    = 6.0   # nights closed due to faults per year
OVERHEADS.MAINTENANCE_NIGHTS              = 5.0   # nights colsed due to maintenance per year
OVERHEADS.MAINTENANCE_CADENCE             = 180.0 # nights between shutdowns for maintenance 
OVERHEADS.MAINTENANCE_NIGHTS_PER_SHUTDOWN = 2     # nights per maintenance shutdown

#Parameters affecting observing sequence
OVERHEADS.SLEW_SPEED                  = 2.0   # deg s^-1
OVERHEADS.SLEW_ACCEL                  = 1.0   # deg s^-2
OVERHEADS.SETTLING_PER_SLEW           = 0.2   # minutes per slew
OVERHEADS.GUIDE_STAR_ACQUISITION_TIME = 1.0   # minutes per slew
OVERHEADS.POSITIONING_PER_TILE        = 1.0   # minutes per tile
OVERHEADS.READOUT_PER_TILE            = 1.2   # minutes per tile
OVERHEADS.TIME_PER_DITHER             = 0.1   # minutes per dither offset
OVERHEADS.CALIBRATION_PER_TILE        = 1.0   # minutes per tile

#parameters affecting how sky and PI fibers must be reserved
OVERHEADS.MINIMUM_SKY_FIBERS_LOW_RES  = 100
OVERHEADS.MINIMUM_SKY_FIBERS_HIGH_RES = 50
OVERHEADS.MINIMUM_PI_FIBERS_LOW_RES   = 0
OVERHEADS.MINIMUM_PI_FIBERS_HIGH_RES  = 0
OVERHEADS.MINIMUM_SKY_FIBERS_NUM_SECTORS         = 16
OVERHEADS.MINIMUM_SKY_FIBERS_PER_SECTOR_LOW_RES  = 5
OVERHEADS.MINIMUM_SKY_FIBERS_PER_SECTOR_HIGH_RES = 3

#Parameters controlling the layout of Fields on the sky
#TILING.METHOD     = "HARDIN-SLOANE" # see http://www2.research.att.com/~njas/icosahedral.codes/index.html
TILING.METHOD      = "GEODESIC"      # see http://www.mpe.mpg.de/~tdwelly/geodesic_pattern_list.html
TILING.NUM_POINTS  = 10242
#TILING.METHOD     = "FILE"
#TILING.FILENAME   = ""

TILING.DEC_MIN       = -68.5   # Lower Dec limit for Field centres (J2000, degrees)
TILING.DEC_MAX       = +18.5   # Upper Dec limit for Field centres (J2000, degrees)

#Control over Dithers of Tiles within a Field - offsets in mm
TILING.NUM_DITHER_OFFSETS = 1
TILING.DITHER_OFFSETS_X = "0.0"
TILING.DITHER_OFFSETS_Y = "0.0"
#TILING.NUM_DITHER_OFFSETS = 5
#TILING.DITHER_OFFSETS_X = "0.0 +10.0 +10.0 -10.0 -10.0"
#TILING.DITHER_OFFSETS_Y = "0.0 +10.0 -10.0 +10.0 -10.0"

#Parameters controlling the Survey strategy
SURVEY.DURATION        = 5.0          # years
SURVEY.JD_START        = 2458485.0    # Julian Date, where JD-2458485.0 -> 01/01/2019 
#SURVEY.JD_START        = 2458666.0   # Julian Date, where JD-2458666.0 -> 01/07/2019 
SURVEY.STRATEGY_TYPE   = "DBPDG"      # "Disk in Bright-time, Poles in Dark/Grey-time" 
SURVEY.DBPDG.GAL_LAT_MIN_DARKGREY   = 15.0   # |Galactic latitude| lower limit (degrees)
SURVEY.DBPDG.GAL_LAT_MAX_BRIGHT     = 15.0   # |Galactic latitude| upper limit (degrees)
SURVEY.NUM_TILES_PER_PASS_BRIGHT    = 3
SURVEY.NUM_TILES_PER_PASS_DARKGREY  = 3
SURVEY.NUM_TILES_PER_FIELD_BRIGHT   = 6
SURVEY.NUM_TILES_PER_FIELD_DARKGREY = 6

SURVEY.GOAL = "EXPOSURE_TIME"
SURVEY.EXPOSURE_TIME_BRIGHT     = 20.0   #minutes per tile
SURVEY.EXPOSURE_TIME_GREY       = 20.0
SURVEY.EXPOSURE_TIME_DARK       = 20.0
SURVEY.ZENITH_ANGLE_MIN         = 5.0    #degrees
SURVEY.AIRMASS_MAX              = 1.624
SURVEY.IQ_MAX                   = 3.0    #Maximum allowed delivered seeing, FWHM arcsec in V-band. 
SURVEY.MOON_DIST_MIN            = 10.0   #degrees
SURVEY.MOON_DIST_FRAC_MIN       = 15.0   #product of illumination fraction and moon distance (degrees)

SURVEY.TILING_DESCRIPTION_FILE  = "4FS_OpSim_survey_sky_tiling_description_file_example.txt"
SURVEY.SMC_AIRMASS_MAX          = 1.0    # Observe the SMC when it is higher in sky than this airmass limit 
SURVEY.LMC_AIRMASS_MAX          = 1.0    # Observe the LMC when it is higher in sky than this airmass limit 

#Params controlling how Fields are prioritised and how targets are assigned
SURVEY.TARGET_TO_FIELD_POLICY                   = "ALL"  # Method to assign targets to Fields: ALL, FIRST, CLOSEST, SPARSEST
SURVEY.FIELD_WEIGHTING_METHOD                   = "STANDARD"
#SURVEY.FIELD_WEIGHTING_METHOD                  = "ESO_OT"
SURVEY.FIELD_WEIGHTING_LAST_FIELD_BONUS_FACTOR  = 0.2
SURVEY.DELTA_MAG_FOR_PROBLEM_NEIGHBOUR          = 4.0     # magnitude difference
SURVEY.FIELD_WEIGHTING_BLUR_SIGMA               = 0.0
SURVEY.FIELD_WEIGHTING_COMPLETE_FIELDS_EXPONENT = 1.0     # additional incentive to complete Fields
SURVEY.MAXIMUM_TEXPFRAC                         = 1.0     # targets will not be observed again after this threshold
SURVEY.DISCARD_THRESHOLD_TEXP                   = 299.0   # minutes
SURVEY.FOM_SUCCESS_THRESHOLD                    = 1.0
SURVEY.USE_UP_SPARE_TILES                       = TRUE

#these control log file frequency
SURVEY.PROGRESS_REPORT_CADENCE      = 30   # nights
SURVEY.FOM_REPORT_CADENCE           = 90   # nights

#these control how targets are selected for the bright or Dark/Grey moon parts of the sky survey
#SURVEY.MOON_SPLIT.METHOD           = "MAGNITUDE"
#SURVEY.MOON_SPLIT.MAG_LOW_RES      = 18.0    # AB mag
#SURVEY.MOON_SPLIT.MAG_HIGH_RES     = 16.0    # AB mag
#SURVEY.MOON_SPLIT.METHOD           = "EXPOSURE"
SURVEY.MOON_SPLIT.METHOD            = "SKY_AREA"

#Parameters contriolling the input catalogues
CAT.NUM_CATS    = 8                 # How many catalogues are specified below?
CAT.CATS_TO_USE = "1 2 3 4 5 6 7"   # which of the following catalogues should we use in this simulation?

CAT.CODENAME_01                = "AGN"
CAT.FILENAME_01                = "/home/4most/4most/IoA_catalogues/round6/mock_agn_round6_v2.txt"
CAT.PRIORITY_RESCALE.METHOD_01 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_01 = 1.2
CAT.PRIORITY_RESCALE.PARAM2_01 = 0.0
CAT.EXPOSURE_SCALING_01        = 1.0
CAT.IS_IN_UPDATED_FORMAT_01    = TRUE

CAT.CODENAME_02                = "BAO"
CAT.FILENAME_02                = "/home/4most/4most/IoA_catalogues/round6/mock_bao_round6_v2.txt"
CAT.PRIORITY_RESCALE.METHOD_02 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_02 = 0.5
CAT.PRIORITY_RESCALE.PARAM2_02 = 0.0
CAT.EXPOSURE_SCALING_02        = 1.0
CAT.IS_IN_UPDATED_FORMAT_02    = TRUE

CAT.CODENAME_03                = "GalHaloLR"
CAT.FILENAME_03                = "/home/4most/4most/IoA_catalogues/round6/mock_galhalolr_round6.txt"
CAT.PRIORITY_RESCALE.METHOD_03 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_03 = 1.0
CAT.PRIORITY_RESCALE.PARAM2_03 = 0.0
CAT.EXPOSURE_SCALING_03        = 1.0
CAT.IS_IN_UPDATED_FORMAT_03    = TRUE

CAT.CODENAME_04                = "GalDiskLR"
CAT.FILENAME_04                = "/home/4most/4most/IoA_catalogues/round6/mock_galdisklr_round6_ver2.txt"
CAT.PRIORITY_RESCALE.METHOD_04 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_04 = 1.1
CAT.PRIORITY_RESCALE.PARAM2_04 = 0.0
CAT.EXPOSURE_SCALING_04        = 1.0
CAT.IS_IN_UPDATED_FORMAT_04    = TRUE

CAT.CODENAME_05                = "Clusters"
CAT.FILENAME_05                = "/home/4most/4most/IoA_catalogues/round6/mock_clusters_round6_v2.txt"
CAT.PRIORITY_RESCALE.METHOD_05 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_05 = 2.0
CAT.PRIORITY_RESCALE.PARAM2_05 = 0.0
CAT.EXPOSURE_SCALING_05        = 1.0
CAT.IS_IN_UPDATED_FORMAT_05    = TRUE

CAT.CODENAME_06                = "GalHaloHR"
CAT.FILENAME_06                = "/home/4most/4most/IoA_catalogues/round6/mock_galhalohr_round6.txt"
CAT.PRIORITY_RESCALE.METHOD_06 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_06 = 1.0
CAT.PRIORITY_RESCALE.PARAM2_06 = 0.0
CAT.EXPOSURE_SCALING_06        = 1.0
CAT.IS_IN_UPDATED_FORMAT_06    = TRUE

CAT.CODENAME_07                = "GalDiskHR"
CAT.FILENAME_07                = "/home/4most/4most/IoA_catalogues/round6/mock_galdiskhr_round6_v2.txt"
CAT.PRIORITY_RESCALE.METHOD_07 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_07 = 1.0
CAT.PRIORITY_RESCALE.PARAM2_07 = 0.0
CAT.EXPOSURE_SCALING_07        = 1.0
CAT.IS_IN_UPDATED_FORMAT_07    = TRUE

CAT.CODENAME_08                = "RANDOM"
CAT.FILENAME_08                = "/home/4most/4most/RANDOM_in/RANDOM_cat_current.txt"
CAT.PRIORITY_RESCALE.METHOD_08 = "LINEAR"
CAT.PRIORITY_RESCALE.PARAM1_08 = 1
CAT.PRIORITY_RESCALE.PARAM2_08 = 0.1
CAT.EXPOSURE_SCALING_08        = 1.2
CAT.IS_IN_UPDATED_FORMAT_08    = FALSE

# Miscellaneous simulation control parameters
SIM.SUPPRESS_OUTPUT              = FALSE   # If TRUE then main output catalogues are not written
SIM.DO_STATS                     = TRUE    # If TRUE then compute input and output stats
SIM.DO_PLOTS                     = TRUE    # If TRUE then will produce plots 
SIM.MAX_THREADS                  = 2       # max number of threads to allow 

SIM.WRITE_FIBER_ASSIGNMENTS      = TRUE    # The following control whether (and how many) 
SIM.WRITE_FIBER_POSITIONS        = TRUE    # various output plots and datafiles are produced
SIM.MAX_FIBER_ASSIGNMENT_FILES   = 100 
SIM.MAX_FIBER_POSITION_FILES     = 100
SIM.MAX_FIELD_RANKING_FILES      = 100 

#----------------------------------------------------------
