//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_input_params_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.110 $"
//#define CVS_DATE     "$Date: 2015/11/11 10:53:19 $"
//--    Note: Definitions are in the corresponding header file OpSim_input_params_lib.h
//--    Description:
//--      This module is used to control all input parameter operations
//--

#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <ctype.h>  //for toupper

#include "OpSim_input_params_lib.h"
#include "OpSim_positioner_lib.h"


#define ERROR_PREFIX   "#-OpSim_input_params_lib.c : Error   :"
#define WARNING_PREFIX "#-OpSim_input_params_lib.c : Warning :"
#define COMMENT_PREFIX "#-OpSim_input_params_lib.c : Comment :"
#define DEBUG_PREFIX   "#-OpSim_input_params_lib.c : DEBUG   :"


////////////////////////////////////////////////////////////////////////
//functions
int OpSim_input_params_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

int OpSim_input_params_struct_init (OpSim_input_params_struct **ppIpar)
{
  if ( ppIpar == NULL ) return 1;
  if ( *ppIpar == NULL )
  {
    //make space for the struct
    if ( (*ppIpar = (OpSim_input_params_struct*) malloc(sizeof(OpSim_input_params_struct))) == NULL )
    {
      fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  return 0;
}

int OpSim_input_params_struct_free (OpSim_input_params_struct **ppIpar)
{
  if ( ppIpar == NULL ) return 1;
  if ( *ppIpar )
  {
    //now free the struct itself
    free(*ppIpar);
    *ppIpar = NULL;
  }
  return 0;
}



int OpSim_input_params_print_usage_information(FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile, "\nUsage: \n\
  4FS_OpSim [SWITCHES] [PARAM_FILENAME=filename] [PARAM1=val1 PARAM2=val2 ...]\n\
\n\
[SWITCHES] are taken from the following list:\n\n\
-d,--debug    Turn on debugging mode (expert use only)\n\n\
-h,--help     Print full help listing (and then exit)\n\
-u,--usage    Print this short usage listing\n\
-v,--version  Print full version information (and then exit)\n\
-x,--example  Print an example input parameter file \n\
              (listing all possible input parameters)\n\
\n\
Parameter values are typically supplied in the file indicated by PARAM_FILENAME\n\
Additional input parameters can be supplied on the command line and will override \n\
any values set in the parameter file\n\
For a full description of input parameters consult the interface documentation\n\
The format for input parameters is 'identifier=value', strings and arrays must\n\
be supplied surrounded by quotes, and the token separator for arrays must be one\n\
of the following characters: ',' '|' ';' ' '\n\
\n");

  return 0;
}


//ways to expand macros to strings
#define xstringify_macro(s) stringify_macro(s)
#define stringify_macro(s) #s


//add all the 4FS input params to the ParamList structure
int OpSim_input_params_setup_ParamList ( OpSim_input_params_struct *pIpar, param_list_struct* pParamList, int argc, char** argv) 
{
  param_list_struct *pPL = (param_list_struct*) pParamList;  //just an alias
  if ( pIpar == NULL || pParamList == NULL) return 1;

  //  if(ParLib_setup_param(pPL,"PARAM_FILENAME", PARLIB_ARGTYPE_STRING, 1,pIpar->PARAM_FILENAME, "4FS_input_params.txt",TRUE, "parfile" ,"Input parameters filename - compulsory")) return 1;
  if(ParLib_setup_param(pPL,"PARAM_FILENAME", PARLIB_ARGTYPE_STRING, 1,pIpar->PARAM_FILENAME, "",FALSE, "parfile" ,"Input parameters filename")) return 1;
  //immeadiately try to read the value of this param from the command line:
  {
    param_struct *pParam = NULL;
    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL, (const char*) "PARAM_FILENAME")) == NULL) return 1;
    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;
  }

  //now set up all the other possible input parameters

  //first set up the switches
  if(ParLib_setup_param(pPL,"--help"    ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a", FALSE, "-h","Print help information and exit")) return 1;
  if(ParLib_setup_param(pPL,"--usage"   ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a", FALSE, "-u","Print short usage instructions and exit")) return 1;
  if(ParLib_setup_param(pPL,"--version" ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a", FALSE, "-v","Print version information and exit")) return 1;
  if(ParLib_setup_param(pPL,"--example" ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a", FALSE, "-x","Print example input params file and exit")) return 1;
  if(ParLib_setup_param(pPL,"--debug"   ,PARLIB_ARGTYPE_SWITCH, 0, NULL  , "n/a", FALSE, "-d","Run in debug mode (verbose output etc)")) return 1;


  //main survey mode
  if(ParLib_setup_param(pPL,"SURVEY.MAIN_MODE",  PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_MAIN_MODE,
                        "TIMELINE", FALSE, "SURVEY_MAIN_MODE",
                        "In which mode should I run the survey? (TIMELINE,P2PP)")) return 1;
  
  //TELESCOPE PARAMETERS
  // function   (pParStruct, param_name                               ,argtype      ,dim, pointer_to_variable_location                   ,default_value         ,vital?, alternative_parname                     , comments/notes/help  )
  if(ParLib_setup_param(pPL,"SIM.CODE_NAME"                           ,PARLIB_ARGTYPE_STRING, 1, pIpar->SIM_CODE_NAME                          ,"blank"   ,
                        FALSE,"SIMULATION.CODE_NAME"                     ,"Code name of this simulation run")) return 1;
  if(ParLib_setup_param(pPL,"SIM.COMMENTS"                            ,PARLIB_ARGTYPE_LSTRING,1, pIpar->SIM_COMMENTS                          ,""         ,
                        FALSE,"SIM.COMMENT"                              ,"Human readable comments for this simulation run")) return 1;

  if(ParLib_setup_param(pPL,"TELESCOPE.CODE_NAME"                     ,PARLIB_ARGTYPE_STRING, 1, pIpar->TELESCOPE_CODE_NAME                     ,"VISTA"  ,
                        FALSE,"TELESCOPE_CODE_NAME"                     ,"Code name of this telescope"               )) return 1;
  if(ParLib_setup_param(pPL,"TELESCOPE.FOV_AREA"                      ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->TELESCOPE_FOV_AREA                      ,"4.0"    ,
                        FALSE,"TELESCOPE_FOV_AREA"                      ,"Area of detector FOV (sq degrees)"         )) return 1;
  if(ParLib_setup_param(pPL,"TELESCOPE.PLATE_SCALE"                   ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->TELESCOPE_PLATE_SCALE                   ,"0.0595" ,
                        FALSE,"TELESCOPE_PLATE_SCALE"                   ,"Plate scale on focal plane (mm/arcsec)"    )) return 1;
  if(ParLib_setup_param(pPL,"TELESCOPE.EXPOSURE_SCALING"              ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->TELESCOPE_EXPOSURE_SCALING              ,"1.0"    ,
                        FALSE,"TELESCOPE_EXPOSURE_SCALING"              ,"Ratio of input to real exposure times"     )) return 1;

  //POSITIONER PARAMETERS - all positioner measurements are in mm
  // ParLib_setup_param(pParStruct, param_name                        ,argtype             ,dim, pointer_to_variable_location                   ,default_value   ,
  //                    vital?, alternative_parname                     , comments/notes/help  )
  if(ParLib_setup_param(pPL,"POSITIONER.CODE_NAME"                    ,PARLIB_ARGTYPE_STRING, 1, pIpar->POSITIONER_CODE_NAME                    ,"Echidna1"      ,
                        FALSE,"POSITIONER_CODE_NAME"                    ,"Code Name of this positioner"              )) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.FAMILY"                       ,PARLIB_ARGTYPE_STRING, 1, pIpar->POSITIONER_FAMILY                       ,"ECHIDNA"       ,
                        FALSE,"POSITIONER_FAMILY"                       ,"One of ECHIDNA,LAMOST,POS-B,POTZPOZ,MUPOZ,OLD_MUPOZ")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.LAYOUT_FILENAME"              ,PARLIB_ARGTYPE_STRING, 1, pIpar->POSITIONER_LAYOUT_FILENAME              ,"NONE"          ,
                        FALSE,"POSITIONER_LAYOUT_FILENAME"              ,"NONE or name of a file containing a list of all positioner coords, in mm")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.LAYOUT_SHAPE"                 ,PARLIB_ARGTYPE_STRING, 1, pIpar->POSITIONER_LAYOUT_SHAPE                 ,"HEXAGON"       ,
                        FALSE,"POSITIONER_LAYOUT_SHAPE"                 ,"shape of outer perimeter e.g. HEXAGON,SQUARE,CIRCLE")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.COORDINATE_TWEAK_FACTOR"      ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_COORDINATE_TWEAK_FACTOR      ,"1.0"           ,
                        FALSE,"POSITIONER_COORDINATE_TWEAK_FACTOR"      ,"Factor by which to rescale positioner coordinates when reading layout from file")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.PATROL_RADIUS_MAX"            ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_PATROL_RADIUS_MAX            ,"12.0"          ,
                        FALSE,"POSITIONER_PATROL_RADIUS_MAX"            ,"Maximum Patrol radius of positioners (mm)" )) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.PATROL_RADIUS_MIN"            ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_PATROL_RADIUS_MIN            ,"0.0"           ,
                        FALSE,"POSITIONER_PATROL_RADIUS_MIN"            ,"Minimum Patrol radius of positioners (mm)" )) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.SPACING_HIGH_LOW_PATTERN"     ,PARLIB_ARGTYPE_STRING, 1, pIpar->POSITIONER_SPACING_HIGH_LOW_PATTERN     ,"REGULAR"       ,
                        FALSE,"POSITIONER_SPACING_HIGH_LOW_PATTERN"     ,"Pattern codename for placing high-res positioners")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.SPACING_LOW_RES"              ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_SPACING_LOW_RES              ,"10.0"          ,
                        FALSE,"POSITIONER_SPACING_LOW_RES"              ,"Spacing between positioners, mm")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.SPACING_HIGH_LOW_RATIO"       ,PARLIB_ARGTYPE_INT   , 1,&pIpar->POSITIONER_SPACING_HIGH_LOW_RATIO       ,"3"             ,
                        FALSE,"POSITIONER_SPACING_HIGH_LOW_RATIO"       ,"Relative spacing between high and low res positioners (integer)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.MINIMUM_SEPARATION"           ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_MINIMUM_SEPARATION           ,"0.1"           ,
                        FALSE,"POSITIONER_MINIMUM_SEPARATION"           ,"Minimum separation between positioners(mm)")) return 1;


  //ECHIDNA SPECIFIC PARAMS
  if(ParLib_setup_param(pPL,"POSITIONER.ECHIDNA.SPINE_DIAMETER"       ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_ECHIDNA_SPINE_DIAMETER       ,"0.5"   ,
                        FALSE,"POSITIONER_ECHIDNA_SPINE_DIAMETER"       ,"Diameter of cylinders used to represent ECHIDNA spines, mm")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.ECHIDNA.SPINE_LENGTH"         ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_ECHIDNA_SPINE_LENGTH       ,"250.0"   ,
                        FALSE,"POSITIONER_ECHIDNA_SPINE_LENGTH"        ,"Length of cylinders used to represent ECHIDNA fibers, mm")) return 1;

  //AESOP SPECIFIC PARAMS
  if(ParLib_setup_param(pPL,"POSITIONER.AESOP.SPINE_DIAMETER"       ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_AESOP_SPINE_DIAMETER       ,"0.5"       ,
                        FALSE,"POSITIONER_AESOP_SPINE_DIAMETER"       ,"Diameter of cylinders used to represent AESOP spines, mm")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.AESOP.SPINE_LENGTH"         ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_AESOP_SPINE_LENGTH       ,"250.0"       ,
                        FALSE,"POSITIONER_AESOP_SPINE_LENGTH"        ,"Length of cylinders used to represent AESOP fibers, mm")) return 1;

  //POTZPOZ-SPECIFIC PARAMS
  if(ParLib_setup_param(pPL,"POSITIONER.POTZPOZ.WIDTH"                ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_POTZPOZ_WIDTH                ,"2.0"   ,
                        FALSE,"POSITIONER_POTZPOZ_WIDTH"                ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.POTZPOZ.PIVOT_OFFSET"         ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_POTZPOZ_PIVOT_OFFSET         ,"1.0"   ,
                        FALSE,"POSITIONER_POTZPOZ_PIVOT_OFFSET"         ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.POTZPOZ.RECTANGLE_LENGTH"     ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_POTZPOZ_RECTANGLE_LENGTH     ,"5.0"   ,
                        FALSE,"POSITIONER_POTZPOZ_RECTANGLE_LENGTH"     ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.POTZPOZ.TIP_RADIUS"           ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_POTZPOZ_TIP_RADIUS           ,"0.6"   ,
                        FALSE,"POSITIONER_POTZPOZ_TIP_RADIUS"           ,"see positioner geometry diagram (mm)")) return 1;
  //MUPOZ-SPECIFIC PARAMS
  if(ParLib_setup_param(pPL,"POSITIONER.MUPOZ.TIP_WIDTH"              ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_MUPOZ_TIP_WIDTH              ,"1.4"   ,
                        FALSE,"POSITIONER_MUPOZ_TIP_WIDTH"              ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.MUPOZ.TIP_RADIUS"             ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_MUPOZ_TIP_RADIUS             ,"0.5"   ,
                        FALSE,"POSITIONER_MUPOZ_TIP_RADIUS"             ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.MUPOZ.ARM_WIDTH"              ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_MUPOZ_ARM_WIDTH              ,"5.0"   ,
                        FALSE,"POSITIONER_MUPOZ_ARM_WIDTH"              ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.MUPOZ.ARM_LENGTH"             ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_MUPOZ_ARM_LENGTH             ,"4.0"   ,
                        FALSE,"POSITIONER_MUPOZ_ARM_LENGTH"             ,"see positioner geometry diagram (mm)")) return 1;
  //OLD_MUPOZ-SPECIFIC PARAMS
  if(ParLib_setup_param(pPL,"POSITIONER.OLD_MUPOZ.TIP_WIDTH"          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_OLD_MUPOZ_TIP_WIDTH          ,"5.0"   ,
                        FALSE,"POSITIONER_OLD_MUPOZ_TIP_WIDTH"          ,"see positioner geometry diagram (mm)")) return 1;
  if(ParLib_setup_param(pPL,"POSITIONER.OLD_MUPOZ.TIP_DIST"           ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->POSITIONER_OLD_MUPOZ_TIP_DIST           ,"1.0"   ,
                        FALSE,"POSITIONER_OLD_MUPOZ_TIP_DIST"           ,"see positioner geometry diagram (mm)")) return 1;
                                                                                                                                                         
  //Environmental effects - new default values interpretted from 
  //http://adsabs.harvard.edu//abs/2010SPIE.7737E..19B
  if(ParLib_setup_param(pPL,"ENVIRONMENT.CLOUD.FRACTION_PHOTOMETRIC"  ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_CLOUD_FRACTION_PHOTOMETRIC  ,"0.50"  ,
                        FALSE,"ENVIRONMENT_CLOUD_FRACTION_PHOTOMETRIC"  ,"Fraction of time in photometric conditions")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.CLOUD.FRACTION_CLEAR"        ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_CLOUD_FRACTION_CLEAR        ,"0.30"  ,
                        FALSE,"ENVIRONMENT_CLOUD_FRACTION_CLEAR"         ,"Fraction of time in clear cloud conditions")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.CLOUD.FRACTION_THIN"         ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_CLOUD_FRACTION_THIN         ,"0.10"  ,
                        FALSE,"ENVIRONMENT_CLOUD_FRACTION_THIN"         ,"Fraction of time in thin cloud conditions")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.CLOUD.FRACTION_THICK"        ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_CLOUD_FRACTION_THICK        ,"0.05"  ,
                        FALSE,"ENVIRONMENT_CLOUD_FRACTION_THICK"        ,"Fraction of time in thick cloud conditions")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.CLOUD.FRACTION_CLOSED"       ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_CLOUD_FRACTION_CLOSED       ,"0.05"  ,
                        FALSE,"ENVIRONMENT_CLOUD_FRACTION_CLOSED"       ,"Fraction of time in bad weather conditions")) return 1;

  if(ParLib_setup_param(pPL,"ENVIRONMENT.MOON.FRACTION_DARK"          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_MOON_FRACTION_DARK          ,"0.50"  ,
                        FALSE,"ENVIRONMENT_MOON_FRACTION_DARK"          ,"Fraction of time moon phase is \"Dark\"")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.MOON.FRACTION_GREY"          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_MOON_FRACTION_GREY          ,"0.0"   ,
                        FALSE,"ENVIRONMENT_MOON_FRACTION_GREY"          ,"Fraction of time moon phase is \"Grey\"")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.MOON.FRACTION_BRIGHT"        ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_MOON_FRACTION_BRIGHT        ,"0.50"  ,
                        FALSE,"ENVIRONMENT_MOON_FRACTION_BRIGHT"        ,"Fraction of time moon phase is \"Bright\"")) return 1;

  //wind constraint params
  if(ParLib_setup_param(pPL,"ENVIRONMENT.WIND.FRACTION_CONSTRAINED"   ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_WIND_FRACTION_CONSTRAINED   ,"0.14"  ,
                        FALSE,"ENVIRONMENT_WIND_FRACTION_CONSTRAINED"    ,"Fraction of time high winds cause a pointing restriction")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.WIND.FRACTION_DOME_CLOSED"   ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->ENVIRONMENT_WIND_FRACTION_DOME_CLOSED   ,"0.03"  ,
                        FALSE,"ENVIRONMENT_WIND_FRACTION_DOME_CLOSED"    ,"Fraction of time high winds mean that the dome must be closed")) return 1;



  
  //new seeing method, these are the seeing distribution values taken from the ESO P2PP document
	//Bierwirth, T et al., 	2010, SPIE, 7737E, 19B,  "New observing concepts for ESO survey telescopes", 	
  //http://adsabs.harvard.edu//abs/2010SPIE.7737E..19B
  if(ParLib_setup_param(pPL,"ENVIRONMENT.SEEING.PROBABILITY"  ,PARLIB_ARGTYPE_FLOAT ,ENVIRON_MAX_SEEING_GRADES,&pIpar->ENVIRONMENT_SEEING_PROBABILITY,"0.10 0.10 0.10 0.20 0.40 0.05 0.05",
                        FALSE,"ENVIRONMENT_SEEING_PROBABILITY" ,"Fraction of time spent in each bin of seeing grade")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.SEEING.THRESH_LOW"           ,PARLIB_ARGTYPE_FLOAT ,ENVIRON_MAX_SEEING_GRADES,&pIpar->ENVIRONMENT_SEEING_THRESH_LOW         ,"0.40 0.40 0.50 0.60 0.80 1.00 1.20",
                        FALSE,"ENVIRONMENT_SEEING_THRESH_LO","The seeing value at the lower limit of this seeing grade")) return 1;
  if(ParLib_setup_param(pPL,"ENVIRONMENT.SEEING.THRESH_HIGH"          ,PARLIB_ARGTYPE_FLOAT ,ENVIRON_MAX_SEEING_GRADES,&pIpar->ENVIRONMENT_SEEING_THRESH_HIGH        ,"0.40 0.50 0.60 0.80 1.00 1.20 2.00",
                        FALSE,"ENVIRONMENT_SEEING_THRESH_HI","The seeing value at the uppper limit of this seeing grade")) return 1;

  
  //Overheads parameters
  if(ParLib_setup_param(pPL,"OVERHEADS.FAULT_NIGHTS"                  ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_FAULT_NIGHTS                  ,"5.0"                 ,
                        FALSE,"OVERHEADS_FAULT_NIGHTS"                  ,"Total time lost to faults+unplanned maintenance (nights per year)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MAINTENANCE_NIGHTS"            ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_MAINTENANCE_NIGHTS            ,"5.0"                 ,
                        FALSE,"OVERHEADS_MAINTENANCE_NIGHTS"            ,"Total time lost to planned maintenance (nights per year)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MAINTENANCE_CADENCE"           ,PARLIB_ARGTYPE_INT   , 1,&pIpar->OVERHEADS_MAINTENANCE_CADENCE           ,"180"                 ,
                        FALSE,"OVERHEADS_MAINTENANCE_CADENCE"           ,"Number of nights between scheduled maintenance shutdowns")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MAINTENANCE_NIGHTS_PER_SHUTDOWN",PARLIB_ARGTYPE_INT  , 1,&pIpar->OVERHEADS_MAINTENANCE_NIGHTS_PER_SHUTDOWN,"2"                  ,
                        FALSE,"OVERHEADS_MAINTENANCE_NIGHTS_PER_SHUTDOWN","Number of nights required for each maintenance shutdown")) return 1;

  //the default slew settling time and slew speed are taken from
  //http://casu.ast.cam.ac.uk/surveys-projects/vista/technical/vista-performance-sept-2009/
  //but also see: http://www.vista.ac.uk/Files/docs/6267_7_ProjectStatus.PDF
  //have modified settling time to ~12s
  if(ParLib_setup_param(pPL,"OVERHEADS.SLEW_SPEED"                    ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_SLEW_SPEED                    ,"2.0"                 ,
                        FALSE,"OVERHEADS_SLEW_SPEED"                    ,"Telescope slew speed (deg s^-1)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.SLEW_ACCEL"                    ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_SLEW_ACCEL                    ,"1.0"                 ,
                        FALSE,"OVERHEADS_SLEW_ACCEL"                    ,"Telescope slew acceleration rate (deg s^-2)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.SETTLING_PER_SLEW"             ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_SETTLING_PER_SLEW             ,"0.2"                 ,
                        FALSE,"OVERHEADS_SETTLING_PER_SLEW"             ,"Telescope slew settling time (mins/slew)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.GUIDE_STAR_ACQUISITION_TIME"   ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_GUIDE_STAR_ACQUISITION_TIME   ,"1.0"                 ,
                        FALSE,"OVERHEADS_ACQUISITION_TIME"              ,"Time to acheive guide star lock (mins)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.POSITIONING_PER_TILE"          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_POSITIONING_PER_TILE          ,"1.0"                 ,
                        FALSE,"OVERHEADS_POSITIONING_PER_TILE"          ,"Time taken for fiber positioning (mins/tile)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.READOUT_PER_TILE"              ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_READOUT_PER_TILE              ,"2.0"                 ,
                        FALSE,"OVERHEADS_READOUT_PER_TILE"              ,"Time taken for readout (mins/tile)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.TIME_PER_DITHER"               ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_TIME_PER_DITHER               ,"0.1"                 ,
                        FALSE,"OVERHEADS_TIME_PER_DITHER"               ,"Time taken to dither telescope position (mins/dither)")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.CALIBRATION_PER_TILE"          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->OVERHEADS_CALIBRATION_PER_TILE          ,"1.0"                 ,
                        FALSE,"OVERHEADS_CALIBRATION_PER_TILE"          ,"Time spent carrying out flat field calibration etc (mins/tile)")) return 1;

  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_SKY_FIBERS_LOW_RES"    ,PARLIB_ARGTYPE_INT   , 1,&pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_LOW_RES    ,"150"                 ,
                        FALSE,"OVERHEADS_MINIMUM_SKY_FIBERS_LOW_RES"    ,"Minimum number of low res fibers per tile to be reserved for Sky observations")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_SKY_FIBERS_HIGH_RES"   ,PARLIB_ARGTYPE_INT   , 1,&pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_HIGH_RES   ,"30"                  ,
                        FALSE,"OVERHEADS_MINIMUM_SKY_FIBERS_HIGH_RES"   ,"Minimum number of high res fibers per tile to be reserved for Sky observations")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_PI_FIBERS_LOW_RES"     ,PARLIB_ARGTYPE_INT   , 1,&pIpar->OVERHEADS_MINIMUM_PI_FIBERS_LOW_RES     ,"0"                   ,
                        FALSE,"OVERHEADS_MINIMUM_PI_FIBERS_LOW_RES"     ,"Minimum number of low res fibers per tile to be reserved for PI targets")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_PI_FIBERS_HIGH_RES"    ,PARLIB_ARGTYPE_INT   , 1,&pIpar->OVERHEADS_MINIMUM_PI_FIBERS_HIGH_RES    ,"0"                   ,
                        FALSE,"OVERHEADS_MINIMUM_PI_FIBERS_HIGH_RES"    ,"Minimum number of high res fibers per tile to be reserved for PI targets")) return 1;

  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_SKY_FIBERS_PER_SECTOR_LOW_RES" ,PARLIB_ARGTYPE_INT, 1,&pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_LOW_RES  ,"0"     ,
                        FALSE,"OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_LOW_RES" ,"Minimum number of low res fibers per sector to be reserved for Sky observations")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_SKY_FIBERS_PER_SECTOR_HIGH_RES",PARLIB_ARGTYPE_INT, 1,&pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_HIGH_RES ,"0"     ,
                        FALSE,"OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_HIGH_RES","Minimum number of high res fibers per sector to be reserved for Sky observations")) return 1;
  if(ParLib_setup_param(pPL,"OVERHEADS.MINIMUM_SKY_FIBERS_NUM_SECTORS"        ,PARLIB_ARGTYPE_INT, 1,&pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_NUM_SECTORS         ,"1"     ,
                        FALSE,"OVERHEADS_MINIMUM_SKY_FIBERS_NUM_SECTORS"        ,"Number of sectors we will divide the focal plane into for purposes of sky fiber allocation")) return 1;


  
  //Tiling parameters
  if(ParLib_setup_param(pPL,"TILING.METHOD"                           ,PARLIB_ARGTYPE_STRING, 1, pIpar->TILING_METHOD                           ,"FILE"                ,
                        FALSE,"TILING_METHOD"                           ,"Either DEFAULT, HARDIN-SLOANE, GEODESIC, or FILE")) return 1;
  if(ParLib_setup_param(pPL,"TILING.FILENAME"                         ,PARLIB_ARGTYPE_STRING, 1, pIpar->TILING_FILENAME                         ,"pointings.txt"       ,
                        FALSE,"TILING_FILENAME"                         ,"Name of file containing the input tiling pattern")) return 1;
  if(ParLib_setup_param(pPL,"TILING.NUM_POINTS"                       ,PARLIB_ARGTYPE_INT,    1,&pIpar->TILING_NUM_POINTS                       ,"8762"                ,
                        FALSE,"TILING_NUM_POINTS"                       ,"Number of points over full sky in Geodesic and Hardin+Sloane tiling patterns")) return 1;
  if(ParLib_setup_param(pPL,"TILING.DEC_MIN"                          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->TILING_DEC_MIN                          ,"-70.0"               ,
                        FALSE,"TILING_DEC_MIN"                          ,"Lower Declination limit of the survey tile centres (degrees)")) return 1;
  if(ParLib_setup_param(pPL,"TILING.DEC_MAX"                          ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->TILING_DEC_MAX                          ,"+20.0"               ,
                        FALSE,"TILING_DEC_MAX"                          ,"Upper Declination limit of the survey tile centres (degrees)")) return 1;
  if(ParLib_setup_param(pPL,"TILING.NUM_DITHER_OFFSETS"               ,PARLIB_ARGTYPE_INT,    1,&pIpar->TILING_NUM_DITHERS                      ,"1"                   ,
                        FALSE,"TILING_NUM_DITHERS"                      ,"Number of dither positions per field")) return 1;
  if(ParLib_setup_param(pPL,"TILING.DITHER_OFFSETS_X"                 ,PARLIB_ARGTYPE_DOUBLE, MAX_DITHERS_PER_FIELD,pIpar->TILING_DITHER_OFFSETS_X ,"0.0"              ,
                        FALSE,"TILING_DITHER_OFFSETS_X"                 ,"offsets (in x axis) to apply to tiling pattern for each dither position (mm)")) return 1;
  if(ParLib_setup_param(pPL,"TILING.DITHER_OFFSETS_Y"                 ,PARLIB_ARGTYPE_DOUBLE, MAX_DITHERS_PER_FIELD,pIpar->TILING_DITHER_OFFSETS_Y ,"0.0"              ,
                        FALSE,"TILING_DITHER_OFFSETS_Y"                 ,"offsets (in y axis) to apply to tiling pattern for each dither position (mm)")) return 1;
                                                                                                                                                                                                       
                                                                                                                                                                                                       
  //Survey strategy etc
  //Note that JD=2458120.0 is 12 noon on Jan 1st 2018 UT 
  //          JD=2458485.0 is 12 noon on Jan 1st 2019 UT 
  if(ParLib_setup_param(pPL,"SURVEY.DURATION"                         ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_DURATION                         ,"1.0"                 ,
                        FALSE,"SURVEY_DURATION"                         ,"Duration of 4MOST sky survey (years)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.JD_START"                         ,PARLIB_ARGTYPE_DOUBLE, 1,&pIpar->SURVEY_JD_START                         ,"2459216.0"           ,
                        FALSE,"SURVEY_JD_START"                         ,"The Julian Date of the start of the 4MOST sky survey (days)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.STRATEGY_TYPE"                    ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_STRATEGY_TYPE                    ,"DBPDG"               ,
                        FALSE,"SURVEY_STRATEGY_TYPE"                    ,"Type of survey strategy, e.g. FLAT, DBPDG, MANUAL etc")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.TILING_DESCRIPTION_FILE"          ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_TILING_DESCRIPTION_FILE          ,"NONE"                ,
                        FALSE,"SURVEY_TILING_DESCRIPTION_FILE"          ,"File describing which areas of the sky to target with more/fewer tiles")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.TILE_PLANNING_METHOD_DARKGREY"     ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_TILE_PLANNING_METHOD_DARKGREY     ,"MIN_AIRMASS"      ,
                        FALSE,"TILE_PLANNING_METHOD_DARKGREY"         ,"How to rank dark/grey fields when planning tiling (ALL,DEC,MIN_AIRMASS,GAL_LAT,VISIBILITY,SUM_FIBERHOURS,SUM_PRIORITY...)?")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.TILE_PLANNING_METHOD_BRIGHT"       ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_TILE_PLANNING_METHOD_BRIGHT       ,"GAL_LAT"          ,
                        FALSE,"TILE_PLANNING_METHOD_BRIGHT"           ,"How to rank bright fields when planning tiling (ALL,DEC,MIN_AIRMASS,GAL_LAT,VISIBILITY,SUM_FIBERHOURS,SUM_PRIORITY...)?")) return 1;

  
  if(ParLib_setup_param(pPL,"SURVEY.NUM_TILES_PER_PASS_DARKGREY"      ,PARLIB_ARGTYPE_INT   , 1,&pIpar->SURVEY_NUM_TILES_PER_PASS_DARKGREY      ,"3"                   ,
                        FALSE,"SURVEY_NUM_TILES_PER_PASS_DARKGREY"      ,"Desired number of Tiles for each Field per pass over the sky in Dark/Grey moon phase")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.NUM_TILES_PER_PASS_BRIGHT"        ,PARLIB_ARGTYPE_INT   , 1,&pIpar->SURVEY_NUM_TILES_PER_PASS_BRIGHT        ,"3"                   ,
                        FALSE,"SURVEY_NUM_TILES_PER_PASS_BRIGHT"        ,"Desired number of Tiles for each Field per pass over the sky in Bright moon phase")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.NUM_TILES_PER_FIELD_DARKGREY"     ,PARLIB_ARGTYPE_INT   , 1,&pIpar->SURVEY_NUM_TILES_PER_FIELD_DARKGREY      ,"6"                   ,
                        FALSE,"SURVEY_NUM_TILES_PER_FIELD_DARKGREY"    ,"Number of Tiles for each Field over the sky in Dark/Grey moon phase")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.NUM_TILES_PER_FIELD_BRIGHT"       ,PARLIB_ARGTYPE_INT   , 1,&pIpar->SURVEY_NUM_TILES_PER_FIELD_BRIGHT        ,"6"                   ,
                        FALSE,"SURVEY_NUM_TILES_PER_FIELD_BRIGHT"      ,"Number of Tiles for each Field over the sky in Bright moon phase")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.DBPDG.GAL_LAT_MIN_DARKGREY"       ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_DBPDG_GAL_LAT_MIN_DARKGREY       ,"15.0"                ,
                        FALSE,"SURVEY_DBPDG_GAL_LAT_MIN_DARKGREY"       ,"Minimum Galactic latitude (absolute) to reach in a Dark/Grey time survey")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.DBPDG.GAL_LAT_MAX_BRIGHT"         ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_DBPDG_GAL_LAT_MAX_BRIGHT         ,"15.0"                ,
                        FALSE,"SURVEY_DBPDG_GAL_LAT_MAX_BRIGHT"         ,"Maximum Galactic latitude (absolute) to reach in a Bright time survey")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.EXPOSURE_TIME_DARK"               ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_EXPOSURE_TIME_DARK               ,"20.0"                ,
                        FALSE,"SURVEY_EXPOSURE_TIME_DARK"               ,"Desired exposure time to execute per tile in a Dark time survey (mins)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.EXPOSURE_TIME_GREY"               ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_EXPOSURE_TIME_GREY               ,"20.0"                ,
                        FALSE,"SURVEY_EXPOSURE_TIME_GREY"               ,"Desired exposure time to execute per tile in a Grey time survey (mins)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.EXPOSURE_TIME_BRIGHT"             ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_EXPOSURE_TIME_BRIGHT             ,"20.0"                ,
                        FALSE,"SURVEY_EXPOSURE_TIME_BRIGHT"             ,"Desired exposure time to execute per tile in a Bright time survey (mins)")) return 1;
  //observing restrictions
  if(ParLib_setup_param(pPL,"SURVEY.ZENITH_ANGLE_MIN"                 ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_ZENITH_ANGLE_MIN                 ,"5.0"                 ,
                        FALSE,"SURVEY_ZENITH_ANGLE_MIN"                 ,"Minimum zenith angle of any observed tile" )) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.AIRMASS_MAX"                      ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_AIRMASS_MAX                      ,"1.7"                 ,
                        FALSE,"SURVEY_AIRMASS_MAX"                      ,"Maximum airmass of any observed tile"      )) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.IQ_MAX"                           ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_IQ_MAX                           ,"3.0"                 ,
                        FALSE,"SURVEY_IQ_MAX"                           ,"Maximum delivered IQ of any observed tile (arcsec,FWHM,V-band)" )) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.MOON_DIST_MIN"                    ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_MOON_DIST_MIN                    ,"10.0"                ,
                        FALSE,"SURVEY_MOON_DIST_MIN"                    ,"Minimum distance from the moon of any observation(degrees)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.MOON_DIST_FRAC_MIN"               ,PARLIB_ARGTYPE_FLOAT , 1,&pIpar->SURVEY_MOON_DIST_FRAC_MIN               ,"15.0"                ,
                        FALSE,"SURVEY_MOON_DIST_FRAC_MIN"               ,"Minimum illum_frac*dist from the moon of any observation(deg*frac)")) return 1;


  if(ParLib_setup_param(pPL,"SURVEY.USE_UP_SPARE_TILES"               ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SURVEY_USE_UP_SPARE_TILES               ,"TRUE"                ,
                        FALSE,"USE_UP_SPARE_TILES"                      ,"Should I distribute any spare tiles through the survey?")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.OVERALLOCATION_FACTOR.BRIGHT"     ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_OVERALLOCATION_FACTOR_BRIGHT     ,"1.0"                 ,
                        FALSE,"SURVEY.OVERALLOCATION_FACTOR_BRIGHT"    ,"Factor by which Tiles should be over-allocated (planned) for Fields relative to the time available - bright moon")) return 1; 
  if(ParLib_setup_param(pPL,"SURVEY.OVERALLOCATION_FACTOR.DARKGREY"   ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_OVERALLOCATION_FACTOR_DARKGREY   ,"1.0"                 ,
                        FALSE,"SURVEY.OVERALLOCATION_FACTOR_DARKGREY"  ,"Factor by which Tiles should be over-allocated (planned) for Fields relative to the time available - dark/grey moon")) return 1; 
  if(ParLib_setup_param(pPL,"SURVEY.TARGET_TO_FIELD_POLICY"           ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_TARGET_TO_FIELD_POLICY           ,"ALL"                 ,
                        FALSE,"OBJECT_FIELD_POLICY"                     ,"How should I choose the field(s) for each target? (ALL,FIRST,CLOSEST,SPARSEST)")) return 1;

  
  if(ParLib_setup_param(pPL,"SURVEY.SKYCALC.FILENAME"                 ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_SKYCALC_FILENAME                 ,"skycalc_out2_VISTA_proc.txt",FALSE,"SKYCALC_FILENAME","The filename containing the formatted skycalc output")) return 1;
                         
  //now add any undocumented parameters - ie any params that we expect to be included in the interfaces document, but that have not yet been added
  if(ParLib_setup_param(pPL,"SURVEY.MOON_SPLIT.METHOD"                ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_MOON_SPLIT_METHOD                ,"SKY_AREA"            ,
                        FALSE,"SURVEY_MOON_SPLIT_METHOD"                ,"Method to use when selecting objects for the bright survey? (SKY_AREA,MAGNITUDE,EXPOSURE)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.MOON_SPLIT.MAG_LOW_RES"           ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_MOON_SPLIT_MAG_LOW_RES           ,"17.0"                ,
                        FALSE,"SURVEY_MOON_SPLIT_MAG_LOW_RES"           ,"Faintest magnitude for a low resolution object to apear in the Bright time survey (MOON_SPLIT.METHOD=MAGNITUDE)")) return 1;  //upper magnitude for low-res obect to be used in the bright survey  
  if(ParLib_setup_param(pPL,"SURVEY.MOON_SPLIT.MAG_HIGH_RES"          ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_MOON_SPLIT_MAG_HIGH_RES          ,"16.0"                ,
                        FALSE,"SURVEY_MOON_SPLIT_MAG_HIGH_RES"          ,"Faintest magnitude for a high resolution object to apear in the Bright time survey (MOON_SPLIT.METHOD=MAGNITUDE)")) return 1; //upper magnitude for high-res obect to be used in the bright survey
  if(ParLib_setup_param(pPL,"SURVEY.MOON_SPLIT.EXP_TOLERANCE"         ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_MOON_SPLIT_EXP_TOLERANCE         ,"2.0"                 ,
                        FALSE,"SURVEY_MOON_SPLIT_EXP_TOLERANCE"         ,"Exposure time Multiplier to use when considering objects for the bright survey (MOON_SPLIT.METHOD=EXPOSURE)")) return 1;

  if(ParLib_setup_param(pPL,"SURVEY.MAXIMUM_TEXPFRAC"                 ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_MAXIMUM_TEXPFRAC                 ,"1.00"                ,
                        FALSE,"SURVEY_MAXIMUM_TEXPFRAC"                 ,"Max. ratio of executed to requested exp time for an object to be considered for further observations")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.DISCARD_THRESHOLD_TEXP"           ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_DISCARD_THRESHOLD_TEXP           ,"999.9"               ,
                        FALSE,"DISCARD_THRESHOLD_TEXP"                  ,"Requested exposure time above which we consider a target impossible to observe (mins)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.FOM_SUCCESS_THRESHOLD"            ,PARLIB_ARGTYPE_FLOAT,   1,&pIpar->SURVEY_FOM_SUCCESS_THRESHOLD           ,"1.0"                 ,
                        FALSE,"FOM_SUCCESS_THRESHOLD"                   ,"If FoM reaches this level we stop observing the catalogue")) return 1;

  
  
  if(ParLib_setup_param(pPL,"SURVEY.FIELD_WEIGHTING.METHOD"           ,PARLIB_ARGTYPE_STRING, 1, pIpar->SURVEY_FIELD_WEIGHTING_METHOD           ,"STANDARD"             ,
                        FALSE,"SURVEY.FIELD_WEIGHTING_METHOD"                 ,"How should I choose a field from the list?")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.FIELD_WEIGHTING.PARAMS"           ,PARLIB_ARGTYPE_FLOAT, MAX_FIELD_WEIGHTING_PARAMS, pIpar->SURVEY_FIELD_WEIGHTING_PARAMS  ,"0.0"   ,
                        FALSE,"SURVEY.FIELD_WEIGHTING_PARAMS"                 ,"Parameters used to choose a field from the list, see documentation")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.FIELD_WEIGHTING.LAST_FIELD_BONUS_FACTOR" ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_FIELD_WEIGHTING_LAST_FIELD_BONUS_FACTOR  ,"0.2"   ,
                        FALSE,"SURVEY.FIELD_WEIGHTING_LAST_FIELD_BONUS_FACTOR","Value of weight bonus to improve chance of previous Field being chosen again for next Tile, (smaller factor improves field repeat chance)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.FIELD_WEIGHTING.BLUR_SIGMA"       ,PARLIB_ARGTYPE_FLOAT,  1, &pIpar->SURVEY_FIELD_WEIGHTING_BLUR_SIGMA  ,"0.01" ,
                        FALSE, "SURVEY.FIELD_WEIGHTING_BLUR_SIGMA"                                     ,"Gaussian sigma which to blur the field weighting (<=0 for no blurring)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.FIELD_WEIGHTING.COMPLETE_FIELDS_EXPONENT" ,PARLIB_ARGTYPE_FLOAT,  1,&pIpar->SURVEY_FIELD_WEIGHTING_COMPLETE_FIELDS_EXPONENT,"1.0"   ,
                        FALSE,"SURVEY.FIELD_WEIGHTING_COMPLETE_FIELDS_EXPONENT"   ,"Controls whether to go wide and shallow (<0.0) or to complete fields as fast as possible (>0.0)")) return 1;

  if(ParLib_setup_param(pPL,"SURVEY.DELTA_MAG_FOR_PROBLEM_NEIGHBOUR"  ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_DELTA_MAG_FOR_PROBLEM_NEIGHBOUR ,"4.0" ,
                        FALSE, "DELTA_MAG_FOR_PROBLEM_NEIGHBOUR"                         ,"Minimum delta mag for a bright object to adversly affect a neighbour")) return 1;



  if(ParLib_setup_param(pPL,"SURVEY.OBJECT_WEIGHTING.COMPLETED_EXPONENT"       ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_OBJECT_WEIGHTING_COMPLETED_EXPONENT       ,OBJECT_WEIGHTING_DEFAULT_COMPLETED_EXPONENT   , FALSE,"" ,"How to decrease priority for well completed objects")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.OBJECT_WEIGHTING.STARTED_FACTOR"           ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_OBJECT_WEIGHTING_STARTED_FACTOR           ,OBJECT_WEIGHTING_DEFAULT_STARTED_FACTOR   , FALSE,"" ,"How to increase priority of partially observed targets")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.OBJECT_WEIGHTING.HIRES_BONUS"              ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_OBJECT_WEIGHTING_HIRES_BONUS              ,OBJECT_WEIGHTING_DEFAULT_HIRES_BONUS           , FALSE,"" ,"How to increase priority for high res objects")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.OBJECT_WEIGHTING.HARD_IN_BRIGHT_DECREMENT" ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_OBJECT_WEIGHTING_HARD_IN_BRIGHT_DECREMENT ,OBJECT_WEIGHTING_DEFAULT_HARD_IN_BRIGHT_DECREMENT       , FALSE,"" ,"How to decrease priority for objects that are difficult to complete in bright time")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.OBJECT_WEIGHTING.HARD_IN_BRIGHT_THRESHOLD" ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_OBJECT_WEIGHTING_HARD_IN_BRIGHT_THRESHOLD ,OBJECT_WEIGHTING_DEFAULT_HARD_IN_BRIGHT_THRESHOLD , FALSE,"" ,"Threshold for decreasing priority for objects that are difficult to complete in bright time")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.OBJECT_WEIGHTING.CANNOT_COMPLETE_FACTOR"   ,PARLIB_ARGTYPE_FLOAT, 1, &pIpar->SURVEY_OBJECT_WEIGHTING_CANNOT_COMPLETE_FACTOR   ,OBJECT_WEIGHTING_DEFAULT_CANNOT_COMPLETE_FACTOR , FALSE,"" ,"How to decrease priority for objects that are nominally impossible to complete")) return 1;


  

  //set the type of map projection to be used
  if(ParLib_setup_param(pPL,"MAP.PROJECTION"                          ,PARLIB_ARGTYPE_STRING, 1, pIpar->MAP_PROJECTION                          ,"HAMMER-AITOFF"       ,
                        FALSE,"MAP_PROJECTION"                          ,"Map projection used for output allsky maps (SINUSOIDAL,HAMMER-AITOFF,AITOFF,PARABOLIC)")) return 1;
  if(ParLib_setup_param(pPL,"MAP.LEFT_IS_EAST"                        ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->MAP_LEFT_IS_EAST                        ,"TRUE"                ,
                        FALSE,"LEFT_IS_EAST"                            ,"Set to true for standard map orientation")) return 1;
  if(ParLib_setup_param(pPL,"MAP.NPIX_RA"                             ,PARLIB_ARGTYPE_INT,    1,&pIpar->MAP_NPIX_RA                             ,"360"                 ,
                        FALSE,"MAP_NPIX_RA"                             ,"Number of pixels in RA direction in target density map")) return 1;
  if(ParLib_setup_param(pPL,"MAP.NPIX_DEC"                            ,PARLIB_ARGTYPE_INT,    1,&pIpar->MAP_NPIX_DEC                            ,"180"                 ,
                        FALSE,"MAP_NPIX_DEC"                            ,"Number of pixels in Dec direction in target density map")) return 1;
  if(ParLib_setup_param(pPL,"MAP.HEALPIX_NSIDE"                       ,PARLIB_ARGTYPE_INT,    1,&pIpar->MAP_HEALPIX_NSIDE                       ,"32"                  ,
                        FALSE,"HEALPIX_NSIDE"                           ,"Nside parameter for HEALPIX stylye output maps")) return 1;


  //how many days observing to allow between each progress report plot
  if(ParLib_setup_param(pPL,"SURVEY.PROGRESS_REPORT_CADENCE"          ,PARLIB_ARGTYPE_INT,    1,&pIpar->PROGRESS_REPORT_CADENCE                 ,"-1"                  ,
                        FALSE,"PROGRESS_REPORT_CADENCE"                 ,"What cadence should we use when plotting progress reports? (days)")) return 1;
  if(ParLib_setup_param(pPL,"SURVEY.FOM_REPORT_CADENCE"               ,PARLIB_ARGTYPE_INT,    1,&pIpar->FOM_REPORT_CADENCE                      ,"90"                  ,
                        FALSE,"FOM_REPORT_CADENCE"                      ,"What cadence should we use when working out incremental FoMs? (days)")) return 1;

  ///////////////////////////////////////////////////////
  //special case - accept a variable number of catalogues
  {
    int c;
    param_struct *pParam = NULL;
    char str_cats_to_use[STR_MAX] = {'\0'}; 
    if(ParLib_setup_param(pPL,"CAT.NUM_CATS"                         ,PARLIB_ARGTYPE_INT,    1, &pIpar->CAT_NUM_CATS                         ,"1",
                          FALSE,"NUM_CATS"        ,"Number of input catalogues")) return 1;
    if ((pParam = (param_struct*) ParLib_get_param_by_name((param_list_struct*) pPL, (const char*) "CAT.NUM_CATS")) == NULL) return 1;

    //check to see if the "CAT.NUM_CATS" is supplied in the "PARAM_FILENAME=xx" file
    if ( strlen(pIpar->PARAM_FILENAME) > 0 )
    {
      if(ParLib_read_param_from_file         ((param_struct*)pParam, (char*) pIpar->PARAM_FILENAME ) > 0)
      {
        fprintf(stderr, "%s Problem reading from input params file: >%s<\n", ERROR_PREFIX, pIpar->PARAM_FILENAME);
        return 1;
      }
    }
    //check to see if the "CAT.NUM_CATS" is supplied on teh command line
    if(ParLib_read_param_from_command_line ((param_struct*)pParam, (int) argc, (char**) argv) > 0)  return 1;

    if ( pIpar->CAT_NUM_CATS > MAX_CATALOGUES )
    {
      fprintf(stderr, "%s Too many catalogues: %d (>%d)\n", ERROR_PREFIX, pIpar->CAT_NUM_CATS, MAX_CATALOGUES);
      return 1;
    }
    
    for (c = 0; c<pIpar->CAT_NUM_CATS; c++)
    {
      char str_par_name1[STR_MAX];
      char str_par_name2[STR_MAX];
      //add the standard params for each catalogue
      snprintf(str_cats_to_use, STR_MAX, "%s %d", str_cats_to_use, c+1);

      //catalogue codename
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.CODENAME", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.CODENAME_%02d",  c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_STRING, 1, pIpar->CAT_CODENAME[c] ,"-",
                            TRUE, str_par_name2 ,"Codename for input target catalogue (e.g. GalDiskHalo")) return 1;

      //catalogue displayname
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.DISPLAYNAME", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.DISPLAYNAME_%02d",  c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_STRING, 1, pIpar->CAT_DISPLAYNAME[c] ,"",
                            FALSE, str_par_name2 ,"Display name for input target catalogue")) return 1;

      //catalogue short name
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.SHORTNAME", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.SHORTNAME_%02d",  c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_STRING, 1, pIpar->CAT_SNAME[c] ,"-",
                            TRUE, str_par_name2 ,"Shortname for input target catalogue (e.g. S1,S2..)")) return 1;

      //catalogue filename
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.FILENAME", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.FILENAME_%02d",  c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_STRING, 1, pIpar->CAT_FILENAME[c] ,"filename.txt",
                            TRUE, str_par_name2 ,"Filename for input target catalogue")) return 1;

      // priority rescaling paramms
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.PRIORITY_RESCALE.METHOD", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.PRIORITY_RESCALE.METHOD_%02d", c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_STRING, 1, pIpar->CAT_PRIORITY_RESCALE_METHOD[c]   ,"NONE" ,
                            FALSE, str_par_name2 ,"Method for rescaling target priorities in this catalogue (NONE,LINEAR,etc)")) return 1;

      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.PRIORITY_RESCALE.PARAM1", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.PRIORITY_RESCALE.PARAM1_%02d", c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_FLOAT, 1, &(pIpar->CAT_PRIORITY_RESCALE_PARAM1[c]) ,"1.0"  ,
                            FALSE, str_par_name2 ,"1st parameter used to rescale target priorities for this catalogue")) return 1;

      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.PRIORITY_RESCALE.PARAM2", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.PRIORITY_RESCALE.PARAM2_%02d", c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_FLOAT, 1, &(pIpar->CAT_PRIORITY_RESCALE_PARAM2[c]) ,"0.0"  ,
                            FALSE, str_par_name2 ,"2nd parameter used to rescale target priorities for this catalogue")) return 1;

      // exposure time scaling paramms
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.EXPOSURE_SCALING", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.EXPOSURE_SCALING_%02d", c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_FLOAT, 1, &(pIpar->CAT_EXPOSURE_SCALING[c])        ,"1.0"  ,
                            FALSE, str_par_name2 ,"Multiplicative parameter used to rescale reqested/required exposure times for this catalogue")) return 1;

      // which format is the cataloge in?
      snprintf(str_par_name1, STR_MAX, "CAT.CAT%d.IS_IN_UPDATED_FORMAT", c+1);
      snprintf(str_par_name2, STR_MAX, "CAT.IS_IN_UPDATED_FORMAT_%02d", c+1);
      if(ParLib_setup_param(pPL,str_par_name1,PARLIB_ARGTYPE_BOOL, 1, &(pIpar->CAT_IS_IN_UPDATED_FORMAT[c])     ,"FALSE" ,
                            FALSE, str_par_name2 ,"Set to true if this catalogue includes extra updated texp columns")) return 1;
    
    }

    //
    if(ParLib_setup_param(pPL,"CAT.CATS_TO_USE" ,PARLIB_ARGTYPE_INT,MAX_CATALOGUES,pIpar->CAT_CATS_TO_USE ,str_cats_to_use,FALSE,"CATS_TO_USE" ,"List of catalogues (index numbers) to use in this simulation")) return 1;
  }
  ///////////////////////////////////////////////////////

                                                                                                                                                 
  if(ParLib_setup_param(pPL,"SIM.RANDOM_SEED"                         ,PARLIB_ARGTYPE_LONG,   1,&pIpar->SIM_RANDOM_SEED                             ,"0.0"                 ,
                        FALSE,"RANDOM_SEED"                             ,"What value should I seed the random number generator with? (0 means use /dev/urandom)")) return 1;
  //whether to calculate stats on the input/output catalogues
  if(ParLib_setup_param(pPL,"SIM.DO_STATS"                            ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_DO_STATS                                ,"TRUE"                ,
                        FALSE,"DO_STATS"                                ,"should I calculate statistics on input/output catalogues?")) return 1;
  //whether to make plots from the input/output catalogues
  if(ParLib_setup_param(pPL,"SIM.DO_PLOTS"                            ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_DO_PLOTS                                ,"TRUE"                ,
                        FALSE,"DO_PLOTS"                                ,"should I make plots?")) return 1;

  if(ParLib_setup_param(pPL,"SIM.DO_MAPS"                             ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_DO_MAPS                                 ,"TRUE"                ,
                        FALSE,"DO_MAPS"                                 ,"should I make target density sky maps?")) return 1;

  //set this to true if you want to suppress the actual fiber-object assignment steps
  if(ParLib_setup_param(pPL,"SIM.SUPPRESS_OUTPUT"                     ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_SUPPRESS_OUTPUT                         ,"FALSE"               ,
                        FALSE,"SUPPRESS_OUTPUT"                         ,"If true then I will not carry out fiber assignment steps")) return 1;

  if(ParLib_setup_param(pPL,"SIM.WRITE_FIBER_ASSIGNMENTS"             ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_WRITE_FIBER_ASSIGNMENTS                 ,"TRUE"                ,
                        FALSE,"WRITE_FIBER_ASSIGNMENTS"                 ,"should I write the fiber assignment files?")) return 1;
  if(ParLib_setup_param(pPL,"SIM.WRITE_FIBER_POSITIONS"               ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_WRITE_FIBER_POSITIONS                   ,"FALSE"               ,
                        FALSE,"WRITE_FIBER_POSITIONS"                   ,"should I write the fiber position files (mainly debug use)?")) return 1;
  if(ParLib_setup_param(pPL,"SIM.MAX_FIBER_POSITION_FILES"            ,PARLIB_ARGTYPE_INT,    1,&pIpar->SIM_MAX_FIBER_POSITION_FILES                ,"100"                 ,
                        FALSE,"MAX_FIBER_POSITION_FILES"                ,"What is the max number of fiber position output files to write?")) return 1;
  if(ParLib_setup_param(pPL,"SIM.MAX_FIBER_ASSIGNMENT_FILES"          ,PARLIB_ARGTYPE_INT,    1,&pIpar->SIM_MAX_FIBER_ASSIGNMENT_FILES              ,"100"                 ,
                        FALSE,"MAX_FIBER_ASSIGNMENT_FILES"              ,"What is the max number of fiber assignment output files to write?")) return 1;
  if(ParLib_setup_param(pPL,"SIM.MAX_FIELD_RANKING_FILES"             ,PARLIB_ARGTYPE_INT,    1,&pIpar->SIM_MAX_FIELD_RANKING_FILES                 ,"100"                 ,
                        FALSE,"MAX_FIELD_RANKING_FILES"                 ,"What is the max number of field ranking output files to write?")) return 1;

  if(ParLib_setup_param(pPL,"SIM.GZIP_OUTPUT_CATALOGUES"              ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_GZIP_OUTPUT_CATALOGUES              ,"FALSE"                   ,
                        FALSE,"GZIP_OUTPUT_CATALOGUES"                  ,"Should the output catalogues be gzipped?")) return 1;


  //maximum number of threads that this instance can use
  if(ParLib_setup_param(pPL,"SIM.MAX_THREADS"                         ,PARLIB_ARGTYPE_INT,    1,&pIpar->SIM_MAX_THREADS                         ,"2"                   ,
                        FALSE,"MAX_THREADS"                             ,"How many threads should I use to complete processing?")) return 1;

  
  if(ParLib_setup_param(pPL,"DEBUG.MAX_OBJECTS"                       ,PARLIB_ARGTYPE_INT,    1,&pIpar->DEBUG_MAX_OBJECTS                       , "-1"                 ,
                        FALSE,"MAX_OBJECTS"                             ,"Debug parameter: What is maximum number of objects to read in?")) return 1;
  if(ParLib_setup_param(pPL,"DEBUG.MAX_OBJECTS_PER_CATALOGUE"         ,PARLIB_ARGTYPE_INT,    1,&pIpar->DEBUG_MAX_OBJECTS_PER_CATALOGUE         , "-1"                 ,
                        FALSE,"MAX_OBJECTS_PER_CAT"                     ,"Debug parameter: What is maximum number of objects per catalogue to read in?")) return 1;
  if(ParLib_setup_param(pPL,"DEBUG.MAX_TILES"                         ,PARLIB_ARGTYPE_INT,    1,&pIpar->DEBUG_MAX_TILES                         , "-1"                 ,
                        FALSE,"MAX_TILES"                               ,"Debug parameter: What is maximum number of tiles to observe in?")) return 1;
  if(ParLib_setup_param(pPL,"DEBUG.MAX_NIGHTS"                        ,PARLIB_ARGTYPE_INT,    1,&pIpar->DEBUG_MAX_NIGHTS                        , "-1"                 ,
                        FALSE,"MAX_NIGHTS"                              ,"Debug parameter: What is maximum number of nights to observe for?")) return 1;
 
  if(ParLib_setup_param(pPL,"DEBUG.RA_MIN"                            ,PARLIB_ARGTYPE_DOUBLE, 1,&pIpar->DEBUG_RA_MIN                            ,   "0.0"              ,
                        FALSE,"DEBUG_RA_MIN"                            ,"Debug parameter")) return 1;
  if(ParLib_setup_param(pPL,"DEBUG.RA_MAX"                            ,PARLIB_ARGTYPE_DOUBLE, 1,&pIpar->DEBUG_RA_MAX                            , "360.0"              ,
                        FALSE,"DEBUG_RA_MAX"                            ,"Debug parameter")) return 1;
  if(ParLib_setup_param(pPL,"DEBUG.DEC_MIN"                           ,PARLIB_ARGTYPE_DOUBLE, 1,&pIpar->DEBUG_DEC_MIN                           , "-90.0"              ,
                        FALSE,"DEBUG_DEC_MIN"                           ,"Debug parameter")) return 1;
  if(ParLib_setup_param(pPL,"DEBUG.DEC_MAX"                           ,PARLIB_ARGTYPE_DOUBLE, 1,&pIpar->DEBUG_DEC_MAX                           , "+90.0"              ,
                        FALSE,"DEBUG_DEC_MAX"                           ,"Debug parameter")) return 1;

  if(ParLib_setup_param(pPL,"SIM.CLOBBER"                             ,PARLIB_ARGTYPE_BOOL,   1,&pIpar->SIM_CLOBBER                             , "yes"                ,
                        FALSE,"clobber"                                 ,"Run in clobber mode? (any existing output files will be overwritten)")) return 1;

  return 0;
}


//this does everything related to reading command line and input files
//return < 0 if error
//return = 0 if we should proceed as normal
//return = 1 if need to exit (without error)
int OpSim_input_params_handler ( OpSim_input_params_struct **ppIpar,
                                 param_list_struct **ppParamList,
                                 int argc, char** argv)
{
  //  int status;
  param_struct *pP = NULL; 
  if ( ppIpar == NULL || ppParamList == NULL ) return -1;
  
  //make space for and init the OpSim input params struct 
  if ( OpSim_input_params_struct_init ((OpSim_input_params_struct**) ppIpar)) return -1;
  /////////////////////////////////////////////////////////

  //make space for and init the param_list structure
  if ( ParLib_init_param_list((param_list_struct**) ppParamList))  return -1;

  //now add all the input parameters
  if ( OpSim_input_params_setup_ParamList ( (OpSim_input_params_struct*) *ppIpar,
                                            (param_list_struct*) *ppParamList,
                                            (int) argc, (char**) argv))
  {
    fprintf(stderr, "%s Problem setting up the input parameter list...\n", ERROR_PREFIX);
    fflush(stderr);
    return -1;
  }

  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "PARAM_FILENAME" )) == NULL)
  {
    fprintf(stderr, "%s You do not have PARAM_FILENAME added as a parameter!\n", ERROR_PREFIX);
    fflush(stderr);
    return -1;
  }
  if ( ParLib_read_param_from_command_line ((param_struct*) pP, (int) argc, (char**) argv))
  {
    //    fprintf(stderr, "%s Problem reading the input parameter file\n", ERROR_PREFIX);
  }
  if ( pP->supplied && strlen(pP->pCval))
  {
    //    status = 0;
    if (ParLib_read_params_from_file ((param_list_struct*) *ppParamList, (*ppIpar)->PARAM_FILENAME))
    {
      OpSim_input_params_print_usage_information(stderr);
      return -1;
    }
  }
  //  else status = 1;
  
  if ( ParLib_read_params_from_command_line ((param_list_struct*) *ppParamList, (int) argc, (char**) argv))
  {
    return -1;
  }

  //interpret any supplied command line switches here
  ////////////////////////////////////////////////////////
  //first see if we should print out the version information
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "--VERSION" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( print_full_version_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the help file
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "--help" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( print_full_help_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the useage info
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "--usage" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( OpSim_input_params_print_usage_information((FILE*) stdout)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if we should print out the example input params file
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "--example" )) == NULL) return -1;
  if ( pP->supplied )
  {
    if ( ParLib_print_example_params_file((FILE*) stdout, (param_list_struct*) *ppParamList)) return -1;
    return 1;
  }
  ////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  //now see if the debug flag is set
  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "--debug" )) == NULL)  return -1;
  if ( pP->supplied ) (*ppIpar)->debug = TRUE;
  else                (*ppIpar)->debug = FALSE;
  ////////////////////////////////////////////////////////

//  ////////////////////////////////////////////////////////
//  //now see if the clobber flag is set
//  if ( (pP = (param_struct*) ParLib_get_param_by_name((param_list_struct*) *ppParamList, (const char*) "--clobber" )) == NULL)  return -1;
//  if ( pP->supplied ) (*ppIpar)->SIM_CLOBBER = TRUE;
//  else                (*ppIpar)->SIM_CLOBBER = FALSE;
//  ////////////////////////////////////////////////////////

//  if ( status )
//  {
//    fprintf(stderr, "%s Use PARAM_FILENAME=filename or parfile=filename to set the parameter filename\n", ERROR_PREFIX);
//    OpSim_input_params_print_usage_information((FILE*) stderr);
//    return -1;
//  }
  
  //special check - if nothing is supplied on the command line
  if ( argc == 1 )
  {
    fprintf(stdout, "%s Printing help information because you didn't supply any inpuut parameters:\n", COMMENT_PREFIX);
    if ( OpSim_input_params_print_usage_information((FILE*) stdout)) return -1;
    return -1;
  }
 
  if ( ParLib_check_param_list ((param_list_struct*) *ppParamList))
  {
    //    ParLib_print_param_list (stdout, (param_list_struct*) *ppParamList);
    return -1;
  }

  //  if ( (*ppIpar)->debug )
  ParLib_print_param_list (stdout, (param_list_struct*) *ppParamList);

  //seed the random number generator
  if ( util_srand((*ppIpar)->SIM_RANDOM_SEED) == 0)
  {
    fprintf(stderr, "%s Problem seeding the random number generator\n", ERROR_PREFIX);
    return 1;
  }

  return 0;
}




///////////////////////////////////////////////////////////////////////////
////these funcs interpret the input values in a sensible way

////input parameter functions
int OpSim_input_params_interpret_map_projection ( OpSim_input_params_struct *pIpar, survey_struct *pSurvey ) 
{
  //set up the map projection
  const char *plist_names[]  = {"SINUSOIDAL",
                                "SANSON-FLAMSTEED",
                                "AITOFF",
                                "HAMMER-AITOFF",
                                "PARABOLIC"};
  const int   plist_values[] = {GEOM_MAP_PROJECTION_SINUSOIDAL,
                                GEOM_MAP_PROJECTION_SINUSOIDAL,
                                GEOM_MAP_PROJECTION_AITOFF,
                                GEOM_MAP_PROJECTION_HAMMER_AITOFF,
                                GEOM_MAP_PROJECTION_PARABOLIC};
  int len = (int) (sizeof(plist_values)/sizeof(int));

  if ( pIpar == NULL || pSurvey == NULL) return 1;
  if (util_select_option_from_string(len, plist_names, plist_values, pIpar->MAP_PROJECTION, &(pSurvey->map_projection)))
  {
    fprintf(stderr, "%s  I do not understand this MAP.PROJECTION: >%s<\n",
            ERROR_PREFIX, pIpar->MAP_PROJECTION);fflush(stderr);
    return 1;
  }
  //which way should we orient the map?
  pSurvey->map_left_is_east  = (BOOL) pIpar->MAP_LEFT_IS_EAST;

  //and the density map pixel numbers
  pSurvey->map_npix_ra  = (int) pIpar->MAP_NPIX_RA;
  pSurvey->map_npix_dec = (int) pIpar->MAP_NPIX_DEC;

  //this is for additional fits maps 
  pSurvey->healpix_nside = (long) pIpar->MAP_HEALPIX_NSIDE;

  return 0;
}


int OpSim_input_params_interpret_timeline ( OpSim_input_params_struct *pIpar, survey_struct *pSurvey, timeline_struct *pTime) 
{
  if ( pIpar == NULL || pSurvey == NULL | pTime == NULL ) return 1;
  if ( pSurvey->main_mode == SURVEY_MAIN_MODE_TIMELINE ||
       pSurvey->main_mode == SURVEY_MAIN_MODE_P2PP )
  {
    if ( skycalc_read_records_from_file((skycalc_struct*) (pTime->pSkycalc), (const char*) pIpar->SURVEY_SKYCALC_FILENAME))  return 1;
    if ( timeline_set_time_from_JD((timeline_struct*) pTime, (double) pSurvey->JD_start ))
    {
      fprintf(stderr, "%s Problem setting timeline from JD=%.5f\n", ERROR_PREFIX, pSurvey->JD_start);
      return 1;
    }
  }
  return 0;
}



int OpSim_input_params_interpret_tiling ( OpSim_input_params_struct *pIpar, fieldlist_struct *pFieldList) 
{
  //set up the tiling method
  //now, see if we should read in a positioner file or make one ourselves
  const char *plist_names[]  = {"HARDIN-SLOANE",
                                "GEODESIC",
                                "FILE"};
  const int   plist_values[] = {TILING_METHOD_HARDIN_SLOANE,
                                TILING_METHOD_GEODESIC,
                                TILING_METHOD_FILE};
  int len =  (int) (sizeof(plist_values)/sizeof(int));
  if ( pIpar == NULL || pFieldList == NULL) return 1;

  if (util_select_option_from_string(len, plist_names, plist_values, pIpar->TILING_METHOD, &(pFieldList->tiling_method)))
  {
    fprintf(stderr, "%s  I do not understand this TILING.METHOD: >%s<\n", ERROR_PREFIX, pIpar->TILING_METHOD);fflush(stderr);
    return 1;
  }
  switch (pFieldList->tiling_method)
  {
  case TILING_METHOD_FILE :
    pFieldList->tiling_num_points = -1;
    (void) sstrncpy(pFieldList->str_filename, pIpar->TILING_FILENAME, STR_MAX);
    break; 
  case TILING_METHOD_HARDIN_SLOANE :
    pFieldList->tiling_num_points = pIpar->TILING_NUM_POINTS;
    snprintf(pFieldList->str_filename, STR_MAX, TILING_PATTERNS_STR_FORMAT_HARDIN, pFieldList->tiling_num_points);
    break;
  case TILING_METHOD_GEODESIC :
    pFieldList->tiling_num_points = pIpar->TILING_NUM_POINTS;
    snprintf(pFieldList->str_filename, STR_MAX, TILING_PATTERNS_STR_FORMAT_GEODESIC, pFieldList->tiling_num_points);
    break;
  default:
    fprintf(stderr, "%s  I cannot handle this TILING.METHOD: >%s<\n",
            ERROR_PREFIX, pIpar->TILING_METHOD);fflush(stderr);
    return 1;
    break;
  }
  
  
  return 0;
}




int OpSim_input_params_interpret_survey_strategy ( OpSim_input_params_struct *pIpar, param_list_struct* pParamList, survey_struct *pSurvey ) 
{
  int m;
  if ( pIpar == NULL || pParamList == NULL || pSurvey == NULL) return 1;

  pSurvey->debug   = (BOOL) pIpar->debug; //we need a copy of the debug flag in the survey_struct so can pass it to functions
  pSurvey->clobber = (BOOL) pIpar->SIM_CLOBBER; //we need a copy of the debug flag in the survey_struct so can pass it to functions
  strncpy(pSurvey->sim_code_name, pIpar->SIM_CODE_NAME, STR_MAX);
  strncpy(pSurvey->sim_comments,  pIpar->SIM_COMMENTS,  LSTR_MAX);

  //choose survey mode
  {
    const char *plist_names[]  = {"TIMELINE",
                                  //                                  "POOLED",
                                  "P2PP"};
    const int   plist_values[] = {SURVEY_MAIN_MODE_TIMELINE,
                                  //                                  SURVEY_MAIN_MODE_POOLED,
                                  SURVEY_MAIN_MODE_P2PP};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_MAIN_MODE, &(pSurvey->main_mode)))
    {
      fprintf(stderr, "%s  I do not understand this SURVEY.MAIN_MODE: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_MAIN_MODE);
      return 1;
    }
  }

    
  if ( pSurvey->main_mode == SURVEY_MAIN_MODE_TIMELINE ||
       pSurvey->main_mode == SURVEY_MAIN_MODE_P2PP )
  {
    pSurvey->JD_start = pIpar->SURVEY_JD_START;
    pSurvey->JD_stop  = pSurvey->JD_start + pIpar->SURVEY_DURATION * YEARS_TO_DAYS; 
    pSurvey->progress_report_cadence = pIpar->PROGRESS_REPORT_CADENCE;    
    pSurvey->FoM_report_cadence      = pIpar->FOM_REPORT_CADENCE;

    //
    pSurvey->num_nights = (int) ceil(pIpar->SURVEY_DURATION * YEARS_TO_DAYS);
    if ( pSurvey->num_nights > 0 )
    {
      if ((pSurvey->pSlots_each_night_dg = (int*) calloc((size_t) pSurvey->num_nights, sizeof(int))) == NULL ||
          (pSurvey->pSlots_each_night_bb = (int*) calloc((size_t) pSurvey->num_nights, sizeof(int))) == NULL )
      {
        fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
    }
    else
    {
      fprintf(stderr, "%s  Strange SURVEY.DURATION value: >%g<\n",
              ERROR_PREFIX, pIpar->SURVEY_DURATION);        return 1;
    }
  }
    
 
  //survey strategy
  {
    const char *plist_names[]  = {"FLAT",
                                  "DBPDG",
                                  "MANUAL"};
    const int   plist_values[] = {SURVEY_STRATEGY_FLAT,
                                  SURVEY_STRATEGY_DBPDG,
                                  SURVEY_STRATEGY_MANUAL};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_STRATEGY_TYPE, &(pSurvey->strategy_code)))
    {
      fprintf(stderr, "%s  I do not understand this SURVEY.STRATEGY_TYPE: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_STRATEGY_TYPE);
      return 1;
    }
  }
  
//  //survey goal
//  {
//    const char *plist_names[]  = {"AREA",
//                                  "EXPOSURE_TIME",
//                                  "EXPOSURE"};
//    const int   plist_values[] = {SURVEY_GOAL_AREA,
//                                  SURVEY_GOAL_EXPOSURE,
//                                  SURVEY_GOAL_EXPOSURE};
//    int len = (int) (sizeof(plist_values)/sizeof(int));
//    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_GOAL, &(pSurvey->goal_code)))
//    {
//      fprintf(stderr, "%s  I do not understand this SURVEY.GOAL: >%s<\n",
//              ERROR_PREFIX, pIpar->SURVEY_GOAL);
//      return 1;
//    }
//  }
  
  //moon split method - How to decide which objects get targetted in each part of the survey
  {
    const char *plist_names[]  = {"MAGNITUDE",
                                  "EXPOSURE",
                                  "SKY_AREA"};
    const int   plist_values[] = {SURVEY_MOON_SPLIT_BY_MAGNITUDE,
                                  SURVEY_MOON_SPLIT_BY_EXPOSURE,
                                  SURVEY_MOON_SPLIT_BY_SKY_AREA};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_MOON_SPLIT_METHOD, &(pSurvey->moon_split_code)))
    {
      fprintf(stderr, "%s  I do not understand this SURVEY.MOON_SPLIT.METHOD: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_MOON_SPLIT_METHOD);
      return 1;
    }
  }
  
  pSurvey->duration                       = pIpar->SURVEY_DURATION;
//  if ( pSurvey->main_mode == SURVEY_MAIN_MODE_POOLED )
//    pSurvey->mean_dark_time_hours_per_night = pIpar->SURVEY_MEAN_DARK_TIME;
//  else
//    pSurvey->mean_dark_time_hours_per_night = 0.0;
  pSurvey->iNum_field_in_any_survey       = 0;
  pSurvey->iNum_field_in_bright_survey    = 0;
  pSurvey->iNum_field_in_darkgrey_survey  = 0;
  pSurvey->iNum_field_used_in_any_survey  = 0;
  pSurvey->tiling_dec_min                 = pIpar->TILING_DEC_MIN;
  pSurvey->tiling_dec_max                 = pIpar->TILING_DEC_MAX;
  pSurvey->plot_dec_min                   = CLIP(pSurvey->tiling_dec_min-5.0,-90.0,+90.0);
  pSurvey->plot_dec_max                   = CLIP(pSurvey->tiling_dec_max+5.0,-90.0,+90.0);
  pSurvey->use_up_spare_tiles             = pIpar->SURVEY_USE_UP_SPARE_TILES;
  pSurvey->moon_split_mag_high_res        = pIpar->SURVEY_MOON_SPLIT_MAG_HIGH_RES;
  pSurvey->moon_split_mag_low_res         = pIpar->SURVEY_MOON_SPLIT_MAG_LOW_RES;
  pSurvey->minimum_sky_fibers_low_res     = pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_LOW_RES;
  pSurvey->minimum_sky_fibers_high_res    = pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_HIGH_RES;
  pSurvey->minimum_PI_fibers_low_res      = pIpar->OVERHEADS_MINIMUM_PI_FIBERS_LOW_RES;
  pSurvey->minimum_PI_fibers_high_res     = pIpar->OVERHEADS_MINIMUM_PI_FIBERS_HIGH_RES;    

  strncpy(pSurvey->str_tiling_description_file, pIpar->SURVEY_TILING_DESCRIPTION_FILE, STR_MAX);
  
  pSurvey->overheads_maintenance_nights_per_year = pIpar->OVERHEADS_MAINTENANCE_NIGHTS;
  pSurvey->overheads_fault_nights_per_year       = pIpar->OVERHEADS_FAULT_NIGHTS;
  pSurvey->overheads_slew_speed                  = pIpar->OVERHEADS_SLEW_SPEED;
  pSurvey->overheads_slew_accel                  = pIpar->OVERHEADS_SLEW_ACCEL;
  pSurvey->overheads_settling_mins_per_slew      = pIpar->OVERHEADS_SETTLING_PER_SLEW;
  pSurvey->overheads_guide_star_acquisition_mins = pIpar->OVERHEADS_GUIDE_STAR_ACQUISITION_TIME;
  pSurvey->overheads_positioning_mins_per_tile   = pIpar->OVERHEADS_POSITIONING_PER_TILE;
  pSurvey->overheads_readout_mins_per_tile       = pIpar->OVERHEADS_READOUT_PER_TILE;
  pSurvey->overheads_mins_per_dither             = pIpar->OVERHEADS_TIME_PER_DITHER;
  pSurvey->overheads_calibration_mins_per_tile   = pIpar->OVERHEADS_CALIBRATION_PER_TILE;


  pSurvey->overheads_average_time_per_slew =
    SECS_TO_MINS * util_calc_travel_time(SURVEY_MEAN_SLEW_DISTANCE,
                                         pSurvey->overheads_slew_speed,
                                         pSurvey->overheads_slew_accel)
    + pSurvey->overheads_settling_mins_per_slew
    + pSurvey->overheads_guide_star_acquisition_mins;
  
  pSurvey->overheads_maintenance_cadence              = pIpar->OVERHEADS_MAINTENANCE_CADENCE;
  pSurvey->overheads_maintenance_nights_per_shutdown  = pIpar->OVERHEADS_MAINTENANCE_NIGHTS_PER_SHUTDOWN;

  pSurvey->zenith_angle_min          = pIpar->SURVEY_ZENITH_ANGLE_MIN;
  pSurvey->airmass_max               = pIpar->SURVEY_AIRMASS_MAX;
  pSurvey->zenith_angle_max          = (pSurvey->airmass_max <= 1.0 ? 0.0 : acosd(1.0 / pSurvey->airmass_max));

  pSurvey->moon_dist_min             = pIpar->SURVEY_MOON_DIST_MIN;
  pSurvey->moon_dist_frac_min        = pIpar->SURVEY_MOON_DIST_FRAC_MIN;
  pSurvey->moon_split_exp_tolerance  = pIpar->SURVEY_MOON_SPLIT_EXP_TOLERANCE;
  //  pSurvey->smc_airmass_max           = pIpar->SURVEY_SMC_AIRMASS_MAX;
  //  pSurvey->lmc_airmass_max           = pIpar->SURVEY_LMC_AIRMASS_MAX;
  //  pSurvey->smc_max_tiles             = pIpar->SURVEY_SMC_MAX_TILES;
  //  pSurvey->lmc_max_tiles             = pIpar->SURVEY_LMC_MAX_TILES;
  pSurvey->IQ_max                    = pIpar->SURVEY_IQ_MAX;

  pSurvey->delta_mag_for_problem_neighbour  = pIpar->SURVEY_DELTA_MAG_FOR_PROBLEM_NEIGHBOUR;


  pSurvey->maximum_texpfracdone_to_consider = pIpar->SURVEY_MAXIMUM_TEXPFRAC;
  pSurvey->discard_threshold_texp = pIpar->SURVEY_DISCARD_THRESHOLD_TEXP;
  if ( pIpar->SURVEY_FOM_SUCCESS_THRESHOLD <= 0.0 )
    pSurvey->FoM_success_threshold = FLT_MAX;
  else
    pSurvey->FoM_success_threshold = pIpar->SURVEY_FOM_SUCCESS_THRESHOLD;
    

  pSurvey->do_stats                   = pIpar->SIM_DO_STATS;
  pSurvey->do_plots                   = pIpar->SIM_DO_PLOTS;
  pSurvey->do_maps                    = pIpar->SIM_DO_MAPS;
  pSurvey->write_fiber_assignments    = pIpar->SIM_WRITE_FIBER_ASSIGNMENTS;
  pSurvey->write_fiber_positions      = pIpar->SIM_WRITE_FIBER_POSITIONS;
  pSurvey->max_fiber_position_files   = pIpar->SIM_MAX_FIBER_POSITION_FILES;
  pSurvey->max_fiber_assignment_files = pIpar->SIM_MAX_FIBER_ASSIGNMENT_FILES;
  pSurvey->max_field_ranking_files    = pIpar->SIM_MAX_FIELD_RANKING_FILES;

  pSurvey->suppress_output            = pIpar->SIM_SUPPRESS_OUTPUT;
  pSurvey->max_threads                = pIpar->SIM_MAX_THREADS;
  pSurvey->max_exposure_scaling       = 0.0;
  
//  //survey output catalogue mode
//  {
//    const char *plist_names[]  = {"NONE",
//                                  "MERGED",
//                                  "INDIVIDUAL"};
//    const int   plist_values[] = {SURVEY_OUTPUT_CATALOGUE_MODE_NONE,
//                                  SURVEY_OUTPUT_CATALOGUE_MODE_MERGED,
//                                  SURVEY_OUTPUT_CATALOGUE_MODE_INDIVIDUAL};
//    int len = (int) (sizeof(plist_values)/sizeof(int));
//    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->OUTPUT_CATALOGUE_MODE, &(pSurvey->output_catalogue_mode)))
//    {
//      fprintf(stderr, "%s  I do not understand this SIM.OUTPUT_CATALOGUE_MODE: >%s<\n",
//              ERROR_PREFIX, pIpar->OUTPUT_CATALOGUE_MODE);
//      return 1;
//    }
//  }

  
  
  if ( pIpar->DEBUG_MAX_TILES < 0 ) pSurvey->debug_max_tiles = INT_MAX;
  else                              pSurvey->debug_max_tiles = pIpar->DEBUG_MAX_TILES;
  
  if ( pIpar->DEBUG_MAX_NIGHTS < 0 ) pSurvey->debug_max_nights = INT_MAX;
  else                               pSurvey->debug_max_nights = pIpar->DEBUG_MAX_NIGHTS;

  pSurvey->debug_ra_min          = pIpar->DEBUG_RA_MIN;
  pSurvey->debug_ra_max          = pIpar->DEBUG_RA_MAX;
  pSurvey->debug_dec_min         = pIpar->DEBUG_DEC_MIN;
  pSurvey->debug_dec_max         = pIpar->DEBUG_DEC_MAX;

  //read the dithering params
  {
    int i;
    int num_x, num_y;//, num_z;
    int sup_x, sup_y;//, sup_z;
    pSurvey->tiling_num_dithers = pIpar->TILING_NUM_DITHERS;
    if ( pSurvey->tiling_num_dithers > MAX_DITHERS_PER_FIELD ||
         pSurvey->tiling_num_dithers < 1 )
    {
      fprintf(stderr, "%s  I cannot handle this TILING.NUM_DITHER_OFFSETS: >%d<\n",
              ERROR_PREFIX, pSurvey->tiling_num_dithers);
      return 1;
    }
    sup_x = ParLib_get_supplied_flag((param_list_struct*) pParamList, "TILING.DITHER_OFFSETS_X");
    sup_y = ParLib_get_supplied_flag((param_list_struct*) pParamList, "TILING.DITHER_OFFSETS_Y");
    //    sup_z = ParLib_get_supplied_flag((param_list_struct*) pParamList, "TILING.DITHER_OFFSETS_Z");
    num_x = ParLib_get_dim_supplied ((param_list_struct*) pParamList, "TILING.DITHER_OFFSETS_X");
    num_y = ParLib_get_dim_supplied ((param_list_struct*) pParamList, "TILING.DITHER_OFFSETS_Y");
    //    num_z = ParLib_get_dim_supplied ((param_list_struct*) pParamList, "TILING.DITHER_OFFSETS_Z");

    if (sup_x > 0 && pSurvey->tiling_num_dithers > num_x ||
        sup_y > 0 && pSurvey->tiling_num_dithers > num_y )
        //        sup_z > 0 && pSurvey->tiling_num_dithers > num_z )
    {
      fprintf(stderr, "%s TILING.NUM_DITHER_OFFSETS must be <= length of arrays TILING.DITHER_OFFSETS_X,Y\n", ERROR_PREFIX);
      return 1;
    }
    for(i=0;i<pSurvey->tiling_num_dithers;i++)
    {
      pSurvey->tiling_dither_offsets_x[i] = pIpar->TILING_DITHER_OFFSETS_X[i];// * ARCSEC_TO_DEG;
      pSurvey->tiling_dither_offsets_y[i] = pIpar->TILING_DITHER_OFFSETS_Y[i];// * ARCSEC_TO_DEG;
      //      pSurvey->tiling_dither_offsets_z[i] = pIpar->TILING_DITHER_OFFSETS_Z[i] * ARCSEC_TO_DEG;
    }

    pSurvey->max_dither_radius_mm = 0.0;
    for(i=0;i<pSurvey->tiling_num_dithers;i++)
    {
      double radius = sqrt(SQR(pSurvey->tiling_dither_offsets_x[i]) + SQR(pSurvey->tiling_dither_offsets_y[i]));
      if ( radius > pSurvey->max_dither_radius_mm ) pSurvey->max_dither_radius_mm = radius;
    }
  }

  //read the field weighting method params
  {
    int i;
    const char *plist_names[]  = {"STANDARD",
                                  "VISIBILITY",
                                  "ESO_OT",
                                  "ESO"};
    const int   plist_values[] = {FIELD_WEIGHTING_METHOD_STANDARD,
                                  FIELD_WEIGHTING_METHOD_VISIBILITY,
                                  FIELD_WEIGHTING_METHOD_ESO_OT,
                                  FIELD_WEIGHTING_METHOD_ESO_OT};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_FIELD_WEIGHTING_METHOD, &(pSurvey->field_weighting_method)))
    {
      fprintf(stderr, "%s  I do not understand this SURVEY.FIELD_WEIGHTING_METHOD: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_FIELD_WEIGHTING_METHOD);
      return 1;
    }
    pSurvey->field_weighting_nparams = ParLib_get_dim_supplied((param_list_struct*) pParamList, "SURVEY.FIELD_WEIGHTING_PARAMS");
    if ( pSurvey->field_weighting_nparams > MAX_FIELD_WEIGHTING_PARAMS ) return 1;
    for (i=0;i<pSurvey->field_weighting_nparams;i++)
      pSurvey->field_weighting_params[i] = pIpar->SURVEY_FIELD_WEIGHTING_PARAMS[i]; 

    pSurvey->field_weighting_last_field_bonus_factor  = pIpar->SURVEY_FIELD_WEIGHTING_LAST_FIELD_BONUS_FACTOR;
    pSurvey->field_weighting_blur_sigma               = pIpar->SURVEY_FIELD_WEIGHTING_BLUR_SIGMA;
    pSurvey->field_weighting_complete_fields_exponent = pIpar->SURVEY_FIELD_WEIGHTING_COMPLETE_FIELDS_EXPONENT;

  }


  //copy the object weighting method params
  {
    pSurvey->object_weighting_completed_exponent       = (float) pIpar->SURVEY_OBJECT_WEIGHTING_COMPLETED_EXPONENT      ;
    pSurvey->object_weighting_started_factor           = (float) pIpar->SURVEY_OBJECT_WEIGHTING_STARTED_FACTOR          ;
    pSurvey->object_weighting_hires_bonus              = (float) pIpar->SURVEY_OBJECT_WEIGHTING_HIRES_BONUS             ;
    pSurvey->object_weighting_hard_in_bright_decrement = (float) pIpar->SURVEY_OBJECT_WEIGHTING_HARD_IN_BRIGHT_DECREMENT;
    pSurvey->object_weighting_hard_in_bright_threshold = (float) pIpar->SURVEY_OBJECT_WEIGHTING_HARD_IN_BRIGHT_THRESHOLD;
    pSurvey->object_weighting_cannot_complete_factor   = (float) pIpar->SURVEY_OBJECT_WEIGHTING_CANNOT_COMPLETE_FACTOR  ;
  }

  
  //now set the parameters up according to the specific survey strategy

  // the following are the same for all survey strategy types:
  pSurvey->num_moon_groups = MAX_NUM_MOON_GROUPS;
  pSurvey->pmoon_bb = (moon_struct*) &(pSurvey->moon[MOON_GROUP_BB]);
  pSurvey->pmoon_dg = (moon_struct*) &(pSurvey->moon[MOON_GROUP_DG]);
  pSurvey->pmoon_bb->group_code          = MOON_GROUP_BB;
  pSurvey->pmoon_dg->group_code          = MOON_GROUP_DG;
  pSurvey->pmoon_bb->mask                = (unsigned int) SURVEY_BRIGHT;
  pSurvey->pmoon_dg->mask                = (unsigned int) (SURVEY_DARK | SURVEY_GREY);
  pSurvey->pmoon_bb->in_area_mask        = (unsigned int) SURVEY_IN_AREA_BRIGHT;
  pSurvey->pmoon_dg->in_area_mask        = (unsigned int) (SURVEY_IN_AREA_DARK | SURVEY_IN_AREA_GREY) ;
  pSurvey->pmoon_bb->fraction            = pIpar->ENVIRONMENT_MOON_FRACTION_BRIGHT;
  pSurvey->pmoon_dg->fraction            = pIpar->ENVIRONMENT_MOON_FRACTION_DARK + pIpar->ENVIRONMENT_MOON_FRACTION_GREY;
  pSurvey->pmoon_bb->codename            = MOON_GROUP_CODENAME_BRIGHT;
  pSurvey->pmoon_dg->codename            = MOON_GROUP_CODENAME_DARKGREY;
  pSurvey->pmoon_bb->longname            = MOON_GROUP_LONGNAME_BRIGHT;
  pSurvey->pmoon_dg->longname            = MOON_GROUP_LONGNAME_DARKGREY;
  pSurvey->pmoon_bb->num_tiles_per_pass  = pIpar->SURVEY_NUM_TILES_PER_PASS_BRIGHT;
  pSurvey->pmoon_dg->num_tiles_per_pass  = pIpar->SURVEY_NUM_TILES_PER_PASS_DARKGREY;


  pSurvey->pmoon_bb->num_tiles_per_field = pIpar->SURVEY_NUM_TILES_PER_FIELD_BRIGHT;
  pSurvey->pmoon_dg->num_tiles_per_field = pIpar->SURVEY_NUM_TILES_PER_FIELD_DARKGREY;
    
  pSurvey->pmoon_bb->Minutes_net_per_tile = pIpar->SURVEY_EXPOSURE_TIME_BRIGHT;
  pSurvey->pmoon_dg->Minutes_net_per_tile = MAX(pIpar->SURVEY_EXPOSURE_TIME_GREY,pIpar->SURVEY_EXPOSURE_TIME_DARK);

  pSurvey->pmoon_dg->overallocation_factor = pIpar->SURVEY_OVERALLOCATION_FACTOR_DARKGREY;
  pSurvey->pmoon_bb->overallocation_factor = pIpar->SURVEY_OVERALLOCATION_FACTOR_BRIGHT;

  if ( pIpar->SURVEY_EXPOSURE_TIME_GREY != pIpar->SURVEY_EXPOSURE_TIME_DARK )
  {
    fprintf(stdout, "%s Exposure time in dark time != exposure time in grey time (SURVEY.EXPOSURE_TIME_GREY=%.4gmins SURVEY.EXPOSURE_TIME_DARK=%.4gmins)\n",
            WARNING_PREFIX, pIpar->SURVEY_EXPOSURE_TIME_GREY, pIpar->SURVEY_EXPOSURE_TIME_DARK);
  }
    
  
  switch ( pSurvey->strategy_code )
  {
  case SURVEY_STRATEGY_FLAT :
    // no extra settings 
    break; 
  case SURVEY_STRATEGY_DBPDG :
    pSurvey->dbpdg_gal_lat_max_bright   = pIpar->SURVEY_DBPDG_GAL_LAT_MAX_BRIGHT;
    pSurvey->dbpdg_gal_lat_min_darkgrey = pIpar->SURVEY_DBPDG_GAL_LAT_MIN_DARKGREY;
    break; 
  case SURVEY_STRATEGY_MANUAL :
    // no extra settings 
    break; 
  default:
    return 1;
    break;
  }

  //read the tile planning method params
  {
    const char *plist_names[]  = {"ALL",
                                  "RANDOM",        "-RANDOM",
                                  "DEC",           "-DEC",
                                  "MIN_AIRMASS",   "-MIN_AIRMASS",
                                  "GAL_LAT",       "-GAL_LAT",
                                  "VISIBILITY",    "-VISIBILITY",
                                  "SUM_FIBERHOURS","-SUM_FIBERHOURS",
                                  "SUM_PRIORITY",  "-SUM_PRIORITY"};
    const int   plist_values[] = {TILE_PLANNING_METHOD_ALL,
                                  TILE_PLANNING_METHOD_RANDOM,        -TILE_PLANNING_METHOD_RANDOM,
                                  TILE_PLANNING_METHOD_DEC,           -TILE_PLANNING_METHOD_DEC,
                                  TILE_PLANNING_METHOD_MIN_AIRMASS,   -TILE_PLANNING_METHOD_MIN_AIRMASS,
                                  TILE_PLANNING_METHOD_GAL_LAT,       -TILE_PLANNING_METHOD_GAL_LAT,
                                  TILE_PLANNING_METHOD_VISIBILITY,    -TILE_PLANNING_METHOD_VISIBILITY,
                                  TILE_PLANNING_METHOD_SUM_FIBERHOURS,-TILE_PLANNING_METHOD_SUM_FIBERHOURS,
                                  TILE_PLANNING_METHOD_SUM_PRIORITY,  -TILE_PLANNING_METHOD_SUM_PRIORITY};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    strncpy(pSurvey->pmoon_dg->str_tile_planning_method, pIpar->SURVEY_TILE_PLANNING_METHOD_DARKGREY, STR_MAX);
    strncpy(pSurvey->pmoon_bb->str_tile_planning_method, pIpar->SURVEY_TILE_PLANNING_METHOD_BRIGHT, STR_MAX);
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_TILE_PLANNING_METHOD_DARKGREY, &(pSurvey->pmoon_dg->tile_planning_method)))
    {
      fprintf(stderr, "%s  I do not understand this SURVEY.TILE_PLANNING_METHOD_DARKGREY: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_TILE_PLANNING_METHOD_DARKGREY);
      return 1;
    }
    if ( pSurvey->pmoon_dg->tile_planning_method < 0 )
    {
      pSurvey->pmoon_dg->tile_planning_method *= -1;
      pSurvey->pmoon_dg->tile_planning_sign = -1;
    }
    else pSurvey->pmoon_dg->tile_planning_sign = 1;
      
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_TILE_PLANNING_METHOD_BRIGHT, &(pSurvey->pmoon_bb->tile_planning_method)))
    {
      fprintf(stderr, "%s  I do not understand this SURVEY.TILE_PLANNING_METHOD_BRIGHT: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_TILE_PLANNING_METHOD_BRIGHT);
      return 1;
    }
    if ( pSurvey->pmoon_bb->tile_planning_method < 0 )
    {
      pSurvey->pmoon_bb->tile_planning_method *= -1;
      pSurvey->pmoon_bb->tile_planning_sign = -1;
    }
    else pSurvey->pmoon_bb->tile_planning_sign = 1;
  }

  //initialise the counters and exposure times
  for(m=0; m<pSurvey->num_moon_groups; m++)
  {
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    //    pMoon->total_tiles_observed = 0 ;
    pMoon->total_pseudo_objects = 0 ;
    pMoon->num_tiles_todo = 0;

//    //this is the minimum (gross) time that can be spent observing a tile
//    //currently same in all moon phases, but put hooks in here so that
//    //can adapt it later
//    //assumes that we need to slew for each tile
//    pMoon->minimum_minutes_per_partial_tile = (double) 
//      pSurvey->overheads_settling_mins_per_slew + 
//      MAX(pSurvey->overheads_positioning_mins_per_tile,pSurvey->overheads_readout_mins_per_tile) +
//      MINIMUM_USEFUL_EXPOSURE_MINS;
//    //    if ( pSurvey->overheads_slew_speed >  0.0 )
//    //      pMoon->minimum_minutes_per_partial_tile += (double)
//    //        (SECS_TO_MINS * SURVEY_MEAN_SLEW_DISTANCE / pSurvey->overheads_slew_speed); 
  }
  /////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////
  
  /////////////////////////////////////////////////////////
  //read in the method that should be used to associate objects with fields
  {
    const char *plist_names[]  = {"FIRST",
                                  "CLOSEST",
                                  "ALL",
                                  "SPARSEST"};
    const int   plist_values[] = {TARGET_TO_FIELD_POLICY_FIRST,
                                  TARGET_TO_FIELD_POLICY_CLOSEST,
                                  TARGET_TO_FIELD_POLICY_ALL,
                                  TARGET_TO_FIELD_POLICY_SPARSEST};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->SURVEY_TARGET_TO_FIELD_POLICY, &(pSurvey->target_to_field_policy)))
    {
      fprintf(stderr, "%s I do not understand this TARGET_TO_FIELD_POLICY: >%s<\n",
              ERROR_PREFIX, pIpar->SURVEY_TARGET_TO_FIELD_POLICY); fflush(stderr);
      return 1;
    }
  }

  //init the clusterlist struct to null
  pSurvey->pClusterList = (clusterlist_struct*) NULL;

  pSurvey->gzip_output_catalogues = (BOOL) pIpar->SIM_GZIP_OUTPUT_CATALOGUES;
  
  return 0;
}



int OpSim_input_params_interpret_tele ( OpSim_input_params_struct *pIpar, param_list_struct* pParamList, telescope_struct *pTele ) 
{
  if ( pIpar == NULL || pParamList == NULL || pTele == NULL) return 1;

  pTele->plate_scale       = pIpar->TELESCOPE_PLATE_SCALE;
  if ( pTele->plate_scale <= 0.0 )
  {
    fprintf(stderr, "%s You gave a stupid value for TELESCOPE_PLATE_SCALE = %g\n",  ERROR_PREFIX, pTele->plate_scale);
    return 1;
  }
  pTele->invplate_scale    = (float)1.0/pTele->plate_scale; 
  pTele->fov_area_deg      = pIpar->TELESCOPE_FOV_AREA;
  pTele->fov_radius_deg    = sqrt(pTele->fov_area_deg/(1.5*SQRT_3));            //Area of hexagon = 3*sqrt(3)*a^2 / 2, where a = length of a side or the radius
  
  pTele->fov_radius_mm     = pTele->fov_radius_deg * (float) 3600.0 * pTele->plate_scale;
  (void) sstrncpy(pTele->code_name,pIpar->TELESCOPE_CODE_NAME,STR_MAX);
  
  if ( telescope_set_site ((telescope_struct*) pTele, (const char*) pIpar->TELESCOPE_CODE_NAME ))
  {
    return 1;
  }
  
  //now setup the structure containing the environmental parameters for this telescope site
  pTele->environ.cloud_fraction_photometric = pIpar->ENVIRONMENT_CLOUD_FRACTION_PHOTOMETRIC;
  pTele->environ.cloud_fraction_clear       = pIpar->ENVIRONMENT_CLOUD_FRACTION_CLEAR;
  pTele->environ.cloud_fraction_thin        = pIpar->ENVIRONMENT_CLOUD_FRACTION_THIN;
  pTele->environ.cloud_fraction_thick       = pIpar->ENVIRONMENT_CLOUD_FRACTION_THICK;
  pTele->environ.cloud_fraction_closed      = pIpar->ENVIRONMENT_CLOUD_FRACTION_CLOSED;
  pTele->environ.moon_fraction_dark         = pIpar->ENVIRONMENT_MOON_FRACTION_DARK;
  pTele->environ.moon_fraction_grey         = pIpar->ENVIRONMENT_MOON_FRACTION_GREY;
  pTele->environ.moon_fraction_bright       = pIpar->ENVIRONMENT_MOON_FRACTION_BRIGHT;

  pTele->environ.wind_fraction_constrained  = pIpar->ENVIRONMENT_WIND_FRACTION_CONSTRAINED;
  pTele->environ.wind_fraction_dome_closed  = pIpar->ENVIRONMENT_WIND_FRACTION_DOME_CLOSED;
  pTele->environ.wind_fraction_nominal      = 1.0 - pTele->environ.wind_fraction_constrained - pTele->environ.wind_fraction_dome_closed;

  //new seeing method
  {
    int i;
    pTele->environ.seeing_num_grades = (int) ParLib_get_dim_supplied((param_list_struct*) pParamList, "ENVIRONMENT.SEEING.PROBABILITY");
    
    if ( pTele->environ.seeing_num_grades == 0 )  //ie we are defaulting to the hard coded values, so search for the number of values
    {
      i = 0;
      while ( pIpar->ENVIRONMENT_SEEING_PROBABILITY[i] < 1.0 && i<ENVIRON_MAX_SEEING_GRADES) i ++;
      if  ( i>=ENVIRON_MAX_SEEING_GRADES ) return 1;
      pTele->environ.seeing_num_grades = i+1;
    }
    
    //this is where we have really received the seeing grades on the command line 
    if ( pTele->environ.seeing_num_grades > ENVIRON_MAX_SEEING_GRADES )
    {
      fprintf(stderr, "%s  I cannot handle your list of seeing grades because num_grades supplied=%d(>%d)\n",
              ERROR_PREFIX, pTele->environ.seeing_num_grades, ENVIRON_MAX_SEEING_GRADES);
      return 1;
    }
    for ( i=0;i<pTele->environ.seeing_num_grades;i++)
    {
      pTele->environ.seeing_probability[i] = pIpar->ENVIRONMENT_SEEING_PROBABILITY[i];
      pTele->environ.seeing_thresh_low[i]  = pIpar->ENVIRONMENT_SEEING_THRESH_LOW[i];
      pTele->environ.seeing_thresh_high[i] = pIpar->ENVIRONMENT_SEEING_THRESH_HIGH[i];
      if ( i > 0 ) pTele->environ.seeing_cumulative_fraction[i] = pTele->environ.seeing_cumulative_fraction[i-1] + pTele->environ.seeing_probability[i];
      else         pTele->environ.seeing_cumulative_fraction[i] = pTele->environ.seeing_probability[i];
    }
  }

  
  return 0;
}

int OpSim_input_params_interpret_focalplane ( OpSim_input_params_struct *pIpar, focalplane_struct *pFocal ) 
{
  if ( pIpar == NULL || pFocal == NULL) return 1;

  {
    const char *plist_names[]  = {"ECHIDNA",
                                  "LAMOST",
                                  "POSB",
                                  "POS-B",
                                  "POTZPOZ",
                                  "MUPOZ",
                                  "OLD_MUPOZ",
                                  "STARBUG",
                                  "AESOP"};
    const int   plist_values[] = {POSITIONER_FAMILY_ECHIDNA,
                                  POSITIONER_FAMILY_LAMOST,
                                  POSITIONER_FAMILY_POSB,
                                  POSITIONER_FAMILY_POSB,
                                  POSITIONER_FAMILY_POTZPOZ,
                                  POSITIONER_FAMILY_MUPOZ,
                                  POSITIONER_FAMILY_OLD_MUPOZ,
                                  POSITIONER_FAMILY_STARBUG,
                                  POSITIONER_FAMILY_AESOP};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->POSITIONER_FAMILY,  &(pFocal->positioner_family)))
    {
      fprintf(stderr, "%s I do not understand this POSITIONER.FAMILY: >%s<\n",
              ERROR_PREFIX, pIpar->POSITIONER_FAMILY);fflush(stderr);
      return 1;
    }
  }
  
  switch ( pFocal->positioner_family )
  {
  case POSITIONER_FAMILY_LAMOST :
  case POSITIONER_FAMILY_POSB :
  case POSITIONER_FAMILY_OLD_MUPOZ :
    fprintf(stderr, "%s I cannot currently handle this POSITIONER.FAMILY: >%s<\n",
            ERROR_PREFIX, pIpar->POSITIONER_FAMILY);fflush(stderr);
    return 1;
  default : break;
  }
  
  
  //take a copy of the positioner code name
  (void) sstrncpy(pFocal->code_name, pIpar->POSITIONER_CODE_NAME, STR_MAX);
  //take a copy of the positioner_layout_filename
  (void) sstrncpy(pFocal->positioner_layout_filename, pIpar->POSITIONER_LAYOUT_FILENAME, STR_MAX);

  if ( strlen(pFocal->positioner_layout_filename) &&
       strcmp(pFocal->positioner_layout_filename, "NONE" ) != 0 &&
       strcmp(pFocal->positioner_layout_filename, "None" ) != 0 &&
       strcmp(pFocal->positioner_layout_filename, "none" ) != 0 &&
       strcmp(pFocal->positioner_layout_filename, "NULL" ) != 0 &&
       strcmp(pFocal->positioner_layout_filename, "-"    ) != 0 )
  {
    pFocal->layout_from_file = (BOOL) TRUE;
  }
  else
    pFocal->layout_from_file = (BOOL) FALSE;




  //this is factor used to tweak positions read from the file
  pFocal->coordinate_tweak_factor = (float) pIpar->POSITIONER_COORDINATE_TWEAK_FACTOR;

  if ( pFocal->layout_from_file == FALSE ) 
  {
    const char *plist_names[]  = {"CIRCLE",
                                  "TRIANGLE",
                                  "SQUARE",
                                  "PENTAGON",
                                  "HEXAGON",
                                  "OCTAGON" };
    const int   plist_values[] = {GEOM_SHAPE_CIRCLE,
                                  GEOM_SHAPE_TRIANGLE,
                                  GEOM_SHAPE_SQUARE,
                                  GEOM_SHAPE_PENTAGON,
                                  GEOM_SHAPE_HEXAGON,
                                  GEOM_SHAPE_OCTAGON};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values,
                                       pIpar->POSITIONER_LAYOUT_SHAPE, &(pFocal->layout_shape_code)))
    {
      fprintf(stderr, "%s I do not understand this POSITIONER.LAYOUT_SHAPE: >%s<\n",
              ERROR_PREFIX, pIpar->POSITIONER_LAYOUT_SHAPE);fflush(stderr);
      return 1;
    }
  }
  else pFocal->layout_shape_code = GEOM_SHAPE_HEXAGON;
    
  //take a copy of the layout shape 
  (void) sstrncpy(pFocal->str_layout_shape, pIpar->POSITIONER_LAYOUT_SHAPE, STR_MAX);

  
  pFocal->plate_scale       = pIpar->TELESCOPE_PLATE_SCALE;
  if ( pFocal->plate_scale <= 0.0 )
  {
    fprintf(stderr, "%s You gave a stupid value for TELESCOPE.PLATE_SCALE = %g\n",  ERROR_PREFIX, pFocal->plate_scale);
    return 1;
  }
  pFocal->invplate_scale    = (float)1.0/pFocal->plate_scale; 

  if ( pFocal->layout_from_file == FALSE ) 
    pFocal->positioner_spacing = pIpar->POSITIONER_SPACING_LOW_RES;
  else
    pFocal->positioner_spacing = pIpar->POSITIONER_SPACING_LOW_RES;  //TODO determine from file
   

  if ( pIpar->POSITIONER_PATROL_RADIUS_MAX < 0.0 ) // treat as a multiplier rather than an absolute radius in mm
    pFocal->patrol_radius_max_mm  = fabs(pIpar->POSITIONER_PATROL_RADIUS_MAX) * pFocal->positioner_spacing;  
  else
    pFocal->patrol_radius_max_mm  = pIpar->POSITIONER_PATROL_RADIUS_MAX;
  if ( pIpar->POSITIONER_PATROL_RADIUS_MIN < 0.0 ) // treat as a multiplier rather than an absolute radius in mm
    pFocal->patrol_radius_min_mm  = fabs(pIpar->POSITIONER_PATROL_RADIUS_MIN) * pFocal->positioner_spacing;  
  else
    pFocal->patrol_radius_min_mm  = pIpar->POSITIONER_PATROL_RADIUS_MIN;

  pFocal->patrol_radius_min_mm    = pIpar->POSITIONER_PATROL_RADIUS_MIN;
  pFocal->sq_patrol_radius_max_mm = SQR(pFocal->patrol_radius_max_mm);
  pFocal->sq_patrol_radius_min_mm = SQR(pFocal->patrol_radius_min_mm);
  pFocal->min_sep_mm              = pIpar->POSITIONER_MINIMUM_SEPARATION;
  pFocal->sq_min_sep_mm           = SQR(pFocal->min_sep_mm); 
  pFocal->patrol_radius_max_deg   =  pFocal->patrol_radius_max_mm * pFocal->invplate_scale * ARCSEC_TO_DEG;
  
  //make the plots look good - normalise the density of the patrol radius ellipse to the area patrol radius
  //for low  res set to to 0.13 if the radius is 10.0mm 
  //for high res set to to 0.2  if the radius is 10.0mm 
  pFocal->gp_ellipse_fs_lo = (0.13/SQR(pFocal->patrol_radius_max_mm/10.0));
  pFocal->gp_ellipse_fs_hi = (0.20/SQR(pFocal->patrol_radius_max_mm/10.0));
  
  
  //switch here on the high res positioner pattern
  if ( pFocal->layout_from_file == FALSE )
  {
    const char *plist_names[]  = {"REGULAR",
                                  "ONE_IN_N",
                                  "RANDOM",
                                  "ALL_LORES", "ALL_LOWRES",
                                  "ALL_HIRES", "ALL_HIGHRES"};
    const int   plist_values[] = {HIGH_LOW_PATTERN_REGULAR,
                                  HIGH_LOW_PATTERN_ONE_IN_N,
                                  HIGH_LOW_PATTERN_RANDOM,
                                  HIGH_LOW_PATTERN_ALL_LORES, HIGH_LOW_PATTERN_ALL_LORES,
                                  HIGH_LOW_PATTERN_ALL_HIRES, HIGH_LOW_PATTERN_ALL_HIRES};
    int len = (int) (sizeof(plist_values)/sizeof(int));
    if (util_select_option_from_string(len, plist_names, plist_values, pIpar->POSITIONER_SPACING_HIGH_LOW_PATTERN,  &(pFocal->positioner_hilo_pattern)))
    {
      fprintf(stderr, "%s I do not understand this POSITIONER.SPACING_HIGH_LOW_PATTERN: >%s<\n",
              ERROR_PREFIX,  pIpar->POSITIONER_SPACING_HIGH_LOW_PATTERN);fflush(stderr);
      return 1;
    }
  }
  else pFocal->positioner_hilo_pattern = HIGH_LOW_PATTERN_REGULAR;
    
  strncpy(pFocal->str_positioner_hilo_pattern, pIpar->POSITIONER_SPACING_HIGH_LOW_PATTERN, STR_MAX);
  //this now means different things for different patterns
  pFocal->positioner_hilo_ratio = pIpar->POSITIONER_SPACING_HIGH_LOW_RATIO;
  
  //set up the positioner-specific details
  switch (pFocal->positioner_family)
  {
  case POSITIONER_FAMILY_POTZPOZ :
    pFocal->potzpoz_width            = pIpar->POSITIONER_POTZPOZ_WIDTH;
    pFocal->potzpoz_tip_radius       = pIpar->POSITIONER_POTZPOZ_TIP_RADIUS;
    pFocal->potzpoz_pivot_offset     = pIpar->POSITIONER_POTZPOZ_PIVOT_OFFSET;
    pFocal->potzpoz_rectangle_length = pIpar->POSITIONER_POTZPOZ_RECTANGLE_LENGTH;
    pFocal->sq_min_object_sep_mm     = SQR(pFocal->min_sep_mm + 2.0*pFocal->potzpoz_tip_radius);
    pFocal->potzpoz_half_width       = pFocal->potzpoz_width * (float) 0.5;
    break;
  case POSITIONER_FAMILY_MUPOZ :
    pFocal->mupoz_tip_width          = pIpar->POSITIONER_MUPOZ_TIP_WIDTH;
    pFocal->mupoz_tip_radius         = pIpar->POSITIONER_MUPOZ_TIP_RADIUS;
    pFocal->mupoz_arm_width          = pIpar->POSITIONER_MUPOZ_ARM_WIDTH;
    pFocal->mupoz_arm_length         = pIpar->POSITIONER_MUPOZ_ARM_LENGTH;
    pFocal->mupoz_half_patrol_radius = pFocal->patrol_radius_max_mm * (float) 0.5;
    pFocal->mupoz_half_tip_width     = pFocal->mupoz_tip_width * (float) 0.5;
    pFocal->mupoz_half_arm_width     = pFocal->mupoz_arm_width * (float) 0.5;
    pFocal->mupoz_arm_start          = pFocal->mupoz_half_patrol_radius - pFocal->mupoz_arm_length;
    pFocal->sq_min_object_sep_mm     = SQR(pFocal->min_sep_mm + 2.0*pFocal->mupoz_tip_radius);
    pFocal->mupoz_sq_min_dist_between_axis2 = SQR(pFocal->min_sep_mm + 2.0*(pFocal->mupoz_half_patrol_radius + pFocal->mupoz_tip_radius));
    
    break;
  case POSITIONER_FAMILY_ECHIDNA :
    pFocal->spine_diameter   = pIpar->POSITIONER_ECHIDNA_SPINE_DIAMETER;
    pFocal->spine_length     = pIpar->POSITIONER_ECHIDNA_SPINE_LENGTH;
    pFocal->spine_radius     = 0.5*pFocal->spine_diameter;
    pFocal->sq_min_object_sep_mm = SQR(pFocal->min_sep_mm + pFocal->spine_diameter);
    break;
  case POSITIONER_FAMILY_AESOP :
    pFocal->spine_diameter   = pIpar->POSITIONER_AESOP_SPINE_DIAMETER;
    pFocal->spine_length     = pIpar->POSITIONER_AESOP_SPINE_LENGTH;
    pFocal->spine_radius     = 0.5*pFocal->spine_diameter;
    pFocal->sq_min_object_sep_mm = SQR(pFocal->min_sep_mm + pFocal->spine_diameter);
    break;
    //    case POSITIONER_FAMILY_OLD_MUPOZ :
    //      pFocal->old_mupoz_tip_width      = pIpar->POSITIONER_OLD_MUPOZ_TIP_WIDTH;
    //      pFocal->old_mupoz_tip_dist       = pIpar->POSITIONER_OLD_MUPOZ_TIP_DIST;
    //      break;
    
  case POSITIONER_FAMILY_STARBUG :
    pFocal->starbug_radius = pIpar->POSITIONER_STARBUG_RADIUS;
    pFocal->sq_min_object_sep_mm = SQR(pFocal->min_sep_mm + 2.0*pFocal->starbug_radius);
      //nothing else to add?
    break;

  default:
    fprintf(stderr, "%s I do not understand this POSITIONER.FAMILY: >%s<\n",
            ERROR_PREFIX, pIpar->POSITIONER_FAMILY);
    return 1;
    break;
  }

  //parameters that control the fiber->spectrograph mapping -> TODO make user accessible
  // TODO - read this from teh fibre focal plane file
  pFocal->number_of_spectrographs_hires = FIBER_CCD_MAPPING_NUM_SPECTROS_HI;
  pFocal->number_of_spectrographs_lores = FIBER_CCD_MAPPING_NUM_SPECTROS_LO;
  pFocal->number_of_spectrographs_swres = FIBER_CCD_MAPPING_NUM_SPECTROS_SW;
  pFocal->number_of_detectors_per_spectrograph_hires = FIBER_CCD_MAPPING_NUM_DETECTORS_PER_SPECTRO_HI;
  pFocal->number_of_detectors_per_spectrograph_lores = FIBER_CCD_MAPPING_NUM_DETECTORS_PER_SPECTRO_LO;
  pFocal->number_of_detectors_per_spectrograph_swres = FIBER_CCD_MAPPING_NUM_DETECTORS_PER_SPECTRO_SW;
  pFocal->crossdisp_pixels_per_spectrograph_hires = FIBER_CCD_MAPPING_NUM_XDISP_PIX_HI; //
  pFocal->crossdisp_pixels_per_spectrograph_lores = FIBER_CCD_MAPPING_NUM_XDISP_PIX_LO; //
  pFocal->crossdisp_pixels_per_spectrograph_swres = FIBER_CCD_MAPPING_NUM_XDISP_PIX_SW; //

  pFocal->num_sectors = pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_NUM_SECTORS;
  pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_LOW]  = pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_LOW_RES;
  pFocal->num_fibers_reserved_per_sector[RESOLUTION_CODE_HIGH] = pIpar->OVERHEADS_MINIMUM_SKY_FIBERS_PER_SECTOR_HIGH_RES;
  pFocal->flag_sectors_supplied_in_input_file = FALSE;
  pFocal->flag_edge_flags_supplied_in_input_file = FALSE;

  pFocal->num_fibers = 0;
  pFocal->num_fibers_hi = 0;
  pFocal->num_fibers_lo = 0;
  pFocal->num_fibers_dual = 0;
  pFocal->num_fibers_switchable = 0;
  pFocal->num_fibers_empty = 0;

  
  return 0;
}
///////////////////////////

//new version that allows a variable number of input catalogues
int    OpSim_input_params_interpret_catalogues ( OpSim_input_params_struct *pIpar, param_list_struct* pParamList,
                                                 cataloguelist_struct *pCatList, survey_struct *pSurvey )
{
  int c;
  if ( pIpar == NULL || pParamList == NULL || pCatList == NULL || pSurvey == NULL ) return 1;

  pCatList->num_cats = (int) ParLib_get_dim_supplied((param_list_struct*) pParamList, "CAT.CATS_TO_USE");
  if ( pCatList->num_cats <= 0 ||
       pCatList->num_cats >  MAX_CATALOGUES)
  {
    fprintf(stderr, "%s  I cannot handle your input catalogue code because ncats=%d\n",
            ERROR_PREFIX, pCatList->num_cats);
    return 1;
  }
  //make some space for the catalogues
  if ((pCatList->pCat = (catalogue_struct*) malloc(pCatList->num_cats * sizeof(catalogue_struct))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  for(c=0;c<pCatList->num_cats;c++)
  {
    int i;
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    int ipar_c = pIpar->CAT_CATS_TO_USE[c] -1;
    
    //    if ( pIpar->CATALOGUE_CODE[c] < 1 || pIpar->CATALOGUE_CODE[c] > pIpar->CAT_NUM_CATS)
    if ( ipar_c < 0 || ipar_c >= pIpar->CAT_NUM_CATS)
    {
      fprintf(stderr, "%s  I do not know this input catalogue index: %d (CAT.NUM_CATS=%d)\n",
              ERROR_PREFIX, ipar_c+1, pIpar->CAT_NUM_CATS);
      return 1;
    }
    pCat->index = c;                   //index of this catalogue_struct within the parent container
    pCat->num_lines = 0;              //initialise values
    pCat->index_of_first_object = -1; //
    pCat->index_of_first_object = -1; //
    pCat->FoM = 0.0;
    for ( i=0;i<MAX_SUBFOMS;i++) pCat->subFoM[i] = 0.0;
    pCat->completeness = 0.0;
    pCat->sub_code_min = 0;//range of sub_codes within this catalogue
    pCat->sub_code_max = 0;
    
    strncpy(pCat->filename, (const char*) pIpar->CAT_FILENAME[ipar_c], STR_MAX);
    strncpy(pCat->codename, (const char*) pIpar->CAT_CODENAME[ipar_c], STR_MAX);
    //if the displayname is empty then revert to using the codename
    strncpy(pCat->displayname, (strlen(pIpar->CAT_DISPLAYNAME[ipar_c])>0 ?
                                (const char*) pIpar->CAT_DISPLAYNAME[ipar_c] :
                                (const char*) pIpar->CAT_CODENAME[ipar_c]), STR_MAX);

    
    //switch here on the catalogue codename
    {
      int code;
      const char *plist_names[]  = {CATALOGUE_CODENAME_S1, CATALOGUE_SNAME_S1, CATALOGUE_ALTNAME_S1,       
                                    CATALOGUE_CODENAME_S2, CATALOGUE_SNAME_S2, CATALOGUE_ALTNAME_S2,        
                                    CATALOGUE_CODENAME_S3, CATALOGUE_SNAME_S3, CATALOGUE_ALTNAME_S3, 
                                    CATALOGUE_CODENAME_S4, CATALOGUE_SNAME_S4, CATALOGUE_ALTNAME_S4, 
                                    CATALOGUE_CODENAME_S5, CATALOGUE_SNAME_S5, CATALOGUE_ALTNAME_S5,  
                                    CATALOGUE_CODENAME_S6, CATALOGUE_SNAME_S6, CATALOGUE_ALTNAME_S6, 
                                    CATALOGUE_CODENAME_S7, CATALOGUE_SNAME_S7, CATALOGUE_ALTNAME_S7, 
                                    CATALOGUE_CODENAME_S8, CATALOGUE_SNAME_S8, CATALOGUE_ALTNAME_S8, 
                                    CATALOGUE_CODENAME_S9, CATALOGUE_SNAME_S9, CATALOGUE_ALTNAME_S9,
                                    SUBCATALOGUE_CODENAME_S3_ESN,
                                    SUBCATALOGUE_CODENAME_S3_DYNDISK1,
                                    SUBCATALOGUE_CODENAME_S3_DYNDISK2,
                                    SUBCATALOGUE_CODENAME_S3_CHEMDISK,
                                    SUBCATALOGUE_CODENAME_S3_BULGE,
                                    SUBCATALOGUE_CODENAME_S3_SUBCAT,
                                    SUBCATALOGUE_CODENAME_S4_BULGE,
                                    SUBCATALOGUE_CODENAME_S4_INNERDISK,
                                    SUBCATALOGUE_CODENAME_S4_OUTERDISK,
                                    SUBCATALOGUE_CODENAME_S4_UNBIASED,
                                    SUBCATALOGUE_CODENAME_S4_SUBCAT};    

      const int  plist_values[]  = {CATALOGUE_CODE_S1, CATALOGUE_CODE_S1, CATALOGUE_CODE_S1,       
                                    CATALOGUE_CODE_S2, CATALOGUE_CODE_S2, CATALOGUE_CODE_S2,                 
                                    CATALOGUE_CODE_S3, CATALOGUE_CODE_S3, CATALOGUE_CODE_S3,
                                    CATALOGUE_CODE_S4, CATALOGUE_CODE_S4, CATALOGUE_CODE_S4,
                                    CATALOGUE_CODE_S5, CATALOGUE_CODE_S5, CATALOGUE_CODE_S5, 
                                    CATALOGUE_CODE_S6, CATALOGUE_CODE_S6, CATALOGUE_CODE_S6,
                                    CATALOGUE_CODE_S7, CATALOGUE_CODE_S7, CATALOGUE_CODE_S7,
                                    CATALOGUE_CODE_S8, CATALOGUE_CODE_S8, CATALOGUE_CODE_S8,
                                    CATALOGUE_CODE_S9, CATALOGUE_CODE_S9, CATALOGUE_CODE_S9,
                                    CATALOGUE_CODE_S3_SUBCAT,
                                    CATALOGUE_CODE_S3_SUBCAT,
                                    CATALOGUE_CODE_S3_SUBCAT,
                                    CATALOGUE_CODE_S3_SUBCAT,
                                    CATALOGUE_CODE_S3_SUBCAT,
                                    CATALOGUE_CODE_S3_SUBCAT,
                                    CATALOGUE_CODE_S4_SUBCAT,
                                    CATALOGUE_CODE_S4_SUBCAT,
                                    CATALOGUE_CODE_S4_SUBCAT,
                                    CATALOGUE_CODE_S4_SUBCAT,
                                    CATALOGUE_CODE_S4_SUBCAT};    

      int len = (int) (sizeof(plist_values)/sizeof(int));
      if (util_select_option_from_string(len, plist_names, plist_values, pIpar->CAT_CODENAME[ipar_c], &code))
      {
        code = (utiny) CATALOGUE_CODE_EXTRA;  //not one in the list, so tag is as an "extra"
      }
      pCat->code = (utiny) code;
    }

    pCat->num_subFoMs = 0;

    if ( strlen(pIpar->CAT_SNAME[ipar_c])) strncpy(pCat->sname,pIpar->CAT_SNAME[ipar_c],STR_MAX); 
    else snprintf(pCat->sname, STR_MAX, "S%d", pCat->code);
    // switch (pCat->code)
    // {
    // case CATALOGUE_CODE_S1    : strncpy(pCat->sname,CATALOGUE_SNAME_S1,STR_MAX); break;
    // case CATALOGUE_CODE_S2    : strncpy(pCat->sname,CATALOGUE_SNAME_S2,STR_MAX); break;
    // case CATALOGUE_CODE_S3    : strncpy(pCat->sname,CATALOGUE_SNAME_S3,STR_MAX); break;
    // case CATALOGUE_CODE_S4    : strncpy(pCat->sname,CATALOGUE_SNAME_S4,STR_MAX); break;
    // case CATALOGUE_CODE_S5    : strncpy(pCat->sname,CATALOGUE_SNAME_S5,STR_MAX); break;
    // case CATALOGUE_CODE_S6    : strncpy(pCat->sname,CATALOGUE_SNAME_S6,STR_MAX); break;
    // case CATALOGUE_CODE_S7    : strncpy(pCat->sname,CATALOGUE_SNAME_S7,STR_MAX); break;
    // case CATALOGUE_CODE_S8    : strncpy(pCat->sname,CATALOGUE_SNAME_S8,STR_MAX); break;
    // case CATALOGUE_CODE_S9    : strncpy(pCat->sname,CATALOGUE_SNAME_S9,STR_MAX); break;
    // case CATALOGUE_CODE_EXTRA : strncpy(pCat->sname,CATALOGUE_SNAME_EXTRA,STR_MAX); break;
    // case CATALOGUE_CODE_S3_SUBCAT: strncpy(pCat->sname,CATALOGUE_SNAME_S3,STR_MAX); break;
    // default :
    //   fprintf(stderr, "%s I do not know this catalogue code: %d (%s)\n",
    //           ERROR_PREFIX, pCat->code, pCat->codename);
    //   return 1;
    // }

    if ( pCat->code == CATALOGUE_CODE_S5 ||
         pCat->code == CATALOGUE_CODE_S6 ||
         pCat->code == CATALOGUE_CODE_S7 ||
         pCat->code == CATALOGUE_CODE_S8 )  pCat->is_extragalactic = TRUE;
    else                                    pCat->is_extragalactic = FALSE;         

    //setup the priority&exposure rescaling methods&params
    {
      const char *plist_names[]  = {"NONE",
                                    "LINEAR"};
      const int   plist_values[] = {PRIORITY_RESCALE_TYPE_NONE,
                                    PRIORITY_RESCALE_TYPE_LINEAR};
      int len = (int) (sizeof(plist_values)/sizeof(int));
      if (util_select_option_from_string(len, plist_names, plist_values,
                                         pIpar->CAT_PRIORITY_RESCALE_METHOD[ipar_c],
                                         &(pCat->priority_rescale_method)))
      { 
        fprintf(stderr, "%s  I do not understand this CAT.PRIORITY_RESCALE.METHOD_%02d: >%s<\n",
                ERROR_PREFIX, ipar_c+1, pIpar->CAT_PRIORITY_RESCALE_METHOD[ipar_c]);
        return 1;
      }
      pCat->priority_rescale_param1 = (float) pIpar->CAT_PRIORITY_RESCALE_PARAM1[ipar_c];
      pCat->priority_rescale_param2 = (float) pIpar->CAT_PRIORITY_RESCALE_PARAM2[ipar_c];
      pCat->exposure_scaling        = (float) pIpar->CAT_EXPOSURE_SCALING[ipar_c];
      if (  pCat->exposure_scaling > pSurvey->max_exposure_scaling )
        pSurvey->max_exposure_scaling = pCat->exposure_scaling;
      pCat->is_in_updated_format = (float) pIpar->CAT_IS_IN_UPDATED_FORMAT[ipar_c];

    }
  }
  return 0;
}
















/////////////// very simple
int  OpSim_input_params_interpret_objectlist ( OpSim_input_params_struct *pIpar, objectlist_struct *pObjectList )
{
  if ( pIpar == NULL || pObjectList == NULL ) return 1;
  if ( pIpar->DEBUG_MAX_OBJECTS < 0 )
    pObjectList->max_objects = INT_MAX;
  else
    pObjectList->max_objects = pIpar->DEBUG_MAX_OBJECTS;

  if ( pIpar->DEBUG_MAX_OBJECTS_PER_CATALOGUE < 0 )
    pObjectList->max_objects_per_catalogue = INT_MAX;
  else
    pObjectList->max_objects_per_catalogue = pIpar->DEBUG_MAX_OBJECTS_PER_CATALOGUE;
  return 0;
}


int  OpSim_input_params_interpret_all ( OpSim_input_params_struct *pIpar, timeline_struct* pTime, survey_struct *pSurvey, skycalc_struct* pSkycalc, telescope_struct *pTele, objectlist_struct *pObjectList )
{
  if ( pIpar == NULL ) return 1;


  
  return 0;
}



int    OpSim_input_params_interpret_everything    (OpSim_input_params_struct *pIpar,
                                                   param_list_struct* pParamList,
                                                   survey_struct *pSurvey,
                                                   fieldlist_struct *pFieldList,
                                                   timeline_struct *pTime,
                                                   telescope_struct *pTele,
                                                   focalplane_struct *pFocal,
                                                   cataloguelist_struct *pCatList,
                                                   objectlist_struct *pObjList)
{

  if ( pIpar        == NULL ||
       pParamList   == NULL ||
       pSurvey      == NULL ||
       pFieldList   == NULL ||
       pTime        == NULL ||
       pTele        == NULL ||
       pFocal       == NULL ||
       pCatList     == NULL ||
       pObjList     == NULL ) return 1;
  
  /////////////////////////////////////////////////////
  //////////////////SETUP TILING PARAMS////////////////
  if ( OpSim_input_params_interpret_tiling ( (OpSim_input_params_struct*) pIpar, (fieldlist_struct*) pFieldList)) return 1; 
  /////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //////////////////SETUP SURVEY+TIMELINE PARAMS///////////////
  if ( OpSim_input_params_interpret_survey_strategy ( (OpSim_input_params_struct*) pIpar, (param_list_struct*) pParamList, (survey_struct*) pSurvey ))  return 1; 
  if (pSurvey->main_mode == SURVEY_MAIN_MODE_TIMELINE ||
      pSurvey->main_mode == SURVEY_MAIN_MODE_P2PP )
  {
    if ( OpSim_input_params_interpret_timeline ( (OpSim_input_params_struct*) pIpar, (survey_struct*) pSurvey, (timeline_struct*) pTime)) return 1; 
    fprintf(stdout, "%s Using Timeline mode: Starting JD is %.3f duration is %g years\n",
            COMMENT_PREFIX, pSurvey->JD_start, pSurvey->duration);
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //////////////////SETUP MAP PROJECTION PARAMS///////
  if ( OpSim_input_params_interpret_map_projection ( (OpSim_input_params_struct*) pIpar, (survey_struct*) pSurvey )) return 1;
  ////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //////////////////SETUP TELESCOPE PARAMS////////////
  if ( OpSim_input_params_interpret_tele ( (OpSim_input_params_struct*) pIpar, (param_list_struct*) pParamList, (telescope_struct*) pTele )) return 1; 
  ////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //////////////////SETUP FOCAL PLANE PARAMS/////////////////
  if ( OpSim_input_params_interpret_focalplane ( (OpSim_input_params_struct*) pIpar, (focalplane_struct*) pFocal )) return 1;
  ////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //////////////////SETUP CATALOGUE PARAMS////////////
  if ( OpSim_input_params_interpret_catalogues ( (OpSim_input_params_struct*) pIpar, (param_list_struct*) pParamList, (cataloguelist_struct*) pCatList, (survey_struct*) pSurvey))  return 1;
  ////////////////////////////////////////////////////

  ////////////////////////////////////////////////////
  //////////////////SETUP OBJECTLIST PARAMS////////////
  if ( OpSim_input_params_interpret_objectlist ( (OpSim_input_params_struct*) pIpar, (objectlist_struct*) pObjList )) return 1;
  ////////////////////////////////////////////////////


  return 0;
}
