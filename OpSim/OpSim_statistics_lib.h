//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_statistics_lib.h
//--    Use: This is a header for the OpSim_statistics_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_STATISTICS_H
#define OPSIM_STATISTICS_H


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <time.h>

#include "define.h"
#include "4FS_OpSim.h"
#include "OpSim_positioner_lib.h"
#include "utensils_lib.h"


#define STR_MAX DEF_STRLEN

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.58 $"
//#define CVS_DATE_H     "$Date: 2015/11/10 15:45:49 $"
inline static int OpSim_statistics_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//


#define OPSIM_STATISTICS_HISTO_TYPE_MAG      1
#define OPSIM_STATISTICS_HISTO_TYPE_PRIORITY 2
#define OPSIM_STATISTICS_HISTO_TYPE_RA       3
#define OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT 4
#define OPSIM_STATISTICS_HISTO_TYPE_TREQ     5
#define OPSIM_STATISTICS_HISTO_TYPE_TREQ_D   6
#define OPSIM_STATISTICS_HISTO_TYPE_TREQ_G   7
#define OPSIM_STATISTICS_HISTO_TYPE_TREQ_B   8
#define OPSIM_STATISTICS_HISTO_TYPE_TEXPFRAC 9
#define OPSIM_STATISTICS_HISTO_TYPE_TOBS_ED 10

#define OPSIM_STATISTICS_HISTO_NAME_MAG      "mag"
#define OPSIM_STATISTICS_HISTO_NAME_PRIORITY "priority"
#define OPSIM_STATISTICS_HISTO_NAME_RA       "RA"
#define OPSIM_STATISTICS_HISTO_NAME_REDSHIFT "redshift"
#define OPSIM_STATISTICS_HISTO_NAME_TREQ     "texp_req"
#define OPSIM_STATISTICS_HISTO_NAME_TREQ_D   "texp_req_d"
#define OPSIM_STATISTICS_HISTO_NAME_TREQ_G   "texp_req_g"
#define OPSIM_STATISTICS_HISTO_NAME_TREQ_B   "texp_req_b"
#define OPSIM_STATISTICS_HISTO_NAME_TEXPFRAC "TexpFracDone"
#define OPSIM_STATISTICS_HISTO_NAME_TOBS_ED  "texp_obs_equivD"



#define OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(x) \
((x)== OPSIM_STATISTICS_HISTO_TYPE_MAG       ? OPSIM_STATISTICS_HISTO_NAME_MAG       : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_PRIORITY  ? OPSIM_STATISTICS_HISTO_NAME_PRIORITY  : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_RA        ? OPSIM_STATISTICS_HISTO_NAME_RA        : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT  ? OPSIM_STATISTICS_HISTO_NAME_REDSHIFT  : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_TREQ      ? OPSIM_STATISTICS_HISTO_NAME_TREQ      : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_TREQ_D    ? OPSIM_STATISTICS_HISTO_NAME_TREQ_D    : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_TREQ_G    ? OPSIM_STATISTICS_HISTO_NAME_TREQ_G    : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_TREQ_B    ? OPSIM_STATISTICS_HISTO_NAME_TREQ_B    : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_TEXPFRAC  ? OPSIM_STATISTICS_HISTO_NAME_TEXPFRAC  : \
((x)== OPSIM_STATISTICS_HISTO_TYPE_TOBS_ED   ? OPSIM_STATISTICS_HISTO_NAME_TOBS_ED   : \
 "Unknown"))))))))))


#define OPSIM_STATISTICS_TEXP_HISTO_DELTA  10.0
#define OPSIM_STATISTICS_TEXP_HISTO_MIN    -9.999999
#define OPSIM_STATISTICS_TEXP_HISTO_MAX   360.0

#define OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS 10

/*
#define OPSIM_STATISTICS_PRINT_SIMPLE_FOM(pFile,name,FoM,ncompleted,ntotal,fiber_hours) fprintf((pFile),"Final_FoM %-10s %10.6f %10d %10d %10.7g\n",(name),(FoM),(ncompleted),(ntotal),(fiber_hours))
*/

int OpSim_statistics_print_version         (FILE* pFile);

int calc_tiles_per_field_stats             (FILE* pFile, fieldlist_struct *pFieldList, survey_struct *pSurvey);
int calc_fields_per_object_stats           (objectlist_struct *pObjList, survey_struct *pSurvey);
int calc_num_targets_with_bright_neighbours  (focalplane_struct *pFocal, survey_struct *pSurvey,
                                              int* pnum_lores, int* pnum_hires);



//-----------------------Maps--------------------------------------------//
int create_catalogue_density_map           (survey_struct *pSurvey, objectlist_struct *pObjList,
                                            catalogue_struct *pCat, const char *str_filename,
                                            int (*test_funcA)(object_struct*,multi_type*), multi_type *pMultiA,
                                            int (*test_funcB)(object_struct*,multi_type*), multi_type *pMultiB);
int create_catalogue_density_measure_map   (survey_struct *pSurvey, objectlist_struct *pObjList,
                                            catalogue_struct *pCat, const char *str_filename,
                                            int (*test_funcA)(object_struct*,multi_type*), multi_type *pMultiA,
                                            int (*test_funcB)(object_struct*,multi_type*), multi_type *pMultiB,
                                            float (*value_func)(object_struct*));
int create_catalogue_density_ratio_map     (survey_struct *pSurvey, objectlist_struct *pObjList,
                                            catalogue_struct *pCat, const char *str_filename,
                                            int (*test_func1A)(object_struct*,multi_type*), multi_type *pMulti1A,
                                            int (*test_func1B)(object_struct*,multi_type*), multi_type *pMulti1B,
                                            int (*test_func2A)(object_struct*,multi_type*), multi_type *pMulti2A,
                                            int (*test_func2B)(object_struct*,multi_type*), multi_type *pMulti2B);

int calc_catalogue_density_stats (survey_struct *pSurvey,
                                  FILE   *pFile,
                                  int    *p2dhisto,
                                  float  *parea,
                                  int    *pnra,
                                  int     npix_ra,
                                  int     npix_dec);


int calc_catalogue_density_ratio_stats (survey_struct *pSurvey,
                                        FILE   *pFile,
                                        float  *p2dhisto1,
                                        float  *p2dhisto2,
                                        float  *parea,
                                        int    *pnra,
                                        int     npix_ra,
                                        int     npix_dec);

int calc_catalogue_density_measure_stats   (survey_struct *pSurvey,
                                            FILE   *pFile,
                                            float  *p2dhisto,
                                            float  *parea,
                                            int    *pnra,
                                            int     npix_ra,
                                            int     npix_dec);


//healpix versions of the above
int create_catalogue_density_map_healpix         (survey_struct *pSurvey, objectlist_struct *pObjList,
                                                  catalogue_struct *pCat, const char *str_filename,
                                                  int (*test_funcA)(object_struct*,multi_type*),  multi_type *pMultiA,
                                                  int (*test_funcB)(object_struct*,multi_type*),  multi_type *pMultiB);

int create_catalogue_density_measure_map_healpix (survey_struct *pSurvey, objectlist_struct *pObjList,
                                                  catalogue_struct *pCat, const char *str_filename,
                                                  int (*test_funcA)(object_struct*,multi_type*), multi_type *pMultiA,
                                                  int (*test_funcB)(object_struct*,multi_type*), multi_type *pMultiB,
                                                  float (*value_func)(object_struct*));
int create_catalogue_density_ratio_map_healpix   (survey_struct *pSurvey, objectlist_struct *pObjList,
                                                  catalogue_struct *pCat, const char *str_filename,
                                                  int (*test_func1A)(object_struct*,multi_type*), multi_type *pMulti1A,
                                                  int (*test_func1B)(object_struct*,multi_type*), multi_type *pMulti1B,
                                                  int (*test_func2A)(object_struct*,multi_type*), multi_type *pMulti2A,
                                                  int (*test_func2B)(object_struct*,multi_type*), multi_type *pMulti2B);


int create_catalogue_density_maps        (survey_struct *pSurvey, objectlist_struct *pObjList,
                                          cataloguelist_struct *pCatList, const char *str_prefix,
                                          int (*test_func)(object_struct*,multi_type*), multi_type *pMulti );
int create_catalogue_density_ratio_maps  (survey_struct *pSurvey, objectlist_struct *pObjList,
                                          cataloguelist_struct *pCatList, const char *str_prefix,
                                          int (*test_func1)(object_struct*,multi_type*), multi_type *pMulti1,
                                          int (*test_func2)(object_struct*,multi_type*), multi_type *pMulti2 );

//--------------------------------end Maps--------------------------------------------------//

//--------------------------------1D Stats-------------------------------------------------//

int calc_all_output_stats_and_maps           (survey_struct *pSurvey, objectlist_struct *pObjList,
                                              focalplane_struct *pFocal, cataloguelist_struct *pCatList,
                                              fieldlist_struct *pFieldList);
int calc_all_input_stats_and_maps            (survey_struct *pSurvey, objectlist_struct *pObjList,
                                              focalplane_struct *pFocal, cataloguelist_struct *pCatList);

int calc_output_catalogue_obs_comp_stats     (objectlist_struct *pObjList, catalogue_struct* pCat,
                                              fieldlist_struct *pFieldList);

int calc_input_catalogue_histos               (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                               objectlist_struct *pObjList,
                                               const char* str_dirname,  int histo_type);
int calc_input_catalogue_histos_texp          (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                               objectlist_struct *pObjList,  const char* str_dirname);
int write_input_histo_plotfile                (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                               const char* str_dirname,  int histo_type);

int calc_output_catalogue_histos              (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                               objectlist_struct *pObjList,  const char* str_dirname,
                                               int histo_type);
int write_output_histo_plotfile               (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                               const char* str_dirname,  int histo_type);
//-----------------------------end-1D-Stats------------------------------------------------//


//--------------------------------Misc-stats/plots---------------------------------------------//
int write_and_plot_fiber_efficiency          (survey_struct *pSurvey);
int write_and_plot_fiber_efficiency_maps     (survey_struct *pSurvey, telescope_struct *pTele);
int write_and_plot_fiber_usage               (focalplane_struct *pFocal, survey_struct *pSurvey);

int fiber_offsets_setup_2Dhisto              (focalplane_struct* pFocal, twoDhisto_struct *pHisto);
int fiber_offsets_write_gnuplot_file         (focalplane_struct *pFocal, const char *str_dirname, const char *str_basename);
int fiber_offsets_write_and_plot             (focalplane_struct *pFocal, survey_struct *pSurvey);

int plot_sky_visibility             (survey_struct *pSurvey, telescope_struct *pTele,
                                     focalplane_struct *pFocal, fieldlist_struct *pFieldList, const char* str_stem);
int plot_sky_tiling                 (survey_struct *pSurvey, telescope_struct *pTele,
                                     focalplane_struct *pFocal, fieldlist_struct *pFieldList, const char* str_stem);
int plot_predicted_tile_pressure_vs_RA (survey_struct *pSurvey, const char* str_stem);
int plot_tiles_used_and_wasted_vs_RA (survey_struct *pSurvey,  const char* str_stem);
int write_progress_plotfile         (survey_struct *pSurvey, const char* str_plotdirname, const char* str_plotfilename);
int write_gnuplot_script_for_field_ranking      (const char* str_plotfile, survey_struct *pSurvey);

//----------------------------end-Misc-stats/plots---------------------------------------------//

//----------------------------object filtering/value functions-----------------------------------//
//object filtering functions
int objectfilter_true                      (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_cat_code_eq               (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_resolution_eq             (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_TexpFracDone_geq          (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_TexpFracDone_gt           (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_TexpFracDone_leq          (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_TexpFracDone_lt           (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_num_fields_eq             (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_num_fields_leq            (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_num_fields_geq            (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_treq_possible             (object_struct *pQueryObj, multi_type *pMulti);
int objectfilter_treq_impossible           (object_struct *pQueryObj, multi_type *pMulti);

//object value functions
float objectvalue_count                    (object_struct *pQueryObj);
float objectvalue_treq_dark                (object_struct *pQueryObj);
float objectvalue_treq_grey                (object_struct *pQueryObj);
float objectvalue_treq_bright              (object_struct *pQueryObj);
float objectvalue_texec_dark_equiv         (object_struct *pQueryObj);
//----------------------end-object filtering/value functions-----------------------------------//


#endif
