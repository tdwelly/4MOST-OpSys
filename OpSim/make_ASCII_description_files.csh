#!/bin/csh -f

set SIMULATION_CODE_NAME = "baseline_6x20mins_24thFeb"

#############################################################################################
#Make a input ascii description file for Paris
# also should get a listing of all unique spectra demanded in each input catalogue
set CATALOGUE_FILENAME_LIST  = ( "AGN_v1.0b_templates.txt" \
"mockCatBAO_v1.2b.dat" \
"haloLR_01_mock_v2.txt" \
"diskLR_DB_ESN_02b_mock.txt" \
"galclus_01_mock.txt" \
"haloHR_01b_mock.txt"  \
"diskHR_01b_mock.txt" )
set CATALOGUE_SHORTNAME_LIST = ( "AGN"  "BAO" "GalHaloLR"  "GalDiskLR"  "Clusters"  "GalHaloHR"  "GalDiskHR" )

#run in pwd

set MAIN_BASE = /home/tdwelly/4most/

if ( "TRUE" == "TRUE" ) then

  if ( -e /scheduling_settings.csh ) then
    echo "Sourcing the scheduling params scheduling_settings.csh"
    source scheduling_settings.csh
  else
    echo "Cannot find scheduling parameters file: scheduling_settings.csh"
    set MOON_PHASE_INDEX_ALL      = 1
    set MOON_PHASE_INDEX_DARK     = 2
    set MOON_PHASE_INDEX_GREY     = 3
    set MOON_PHASE_INDEX_BRIGHT   = 4
    set MOON_PHASE_INDEX_DARKGREY = 5
    set MOON_PHASE_NAME      = ( all  dark  grey bright darkgrey )
    set TEXP_PER_TILE = ( $MOON_PHASE_NAME ) 
    set TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK] = 1200.0
    set TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY] = 1200.0
    set TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT] = 1200.0
    set NUM_PASSES_PER_FIELD_BRIGHT   = 2
    set NUM_PASSES_PER_FIELD_DARKGREY = 2
    set NUM_TILES_PER_PASS_BRIGHT   = 3
    set NUM_TILES_PER_PASS_DARKGREY = 3
    @ NUM_TILES_BRIGHT   =  $NUM_PASSES_PER_FIELD_BRIGHT   * $NUM_TILES_PER_PASS_BRIGHT
    @ NUM_TILES_DARKGREY =  $NUM_PASSES_PER_FIELD_DARKGREY * $NUM_TILES_PER_PASS_DARKGREY
  endif

  set I = 1
  while ( $I <=  $#CATALOGUE_FILENAME_LIST )
    set CAT = ${CATALOGUE_SHORTNAME_LIST[$I]}

    #just get the list of all supplied spectral files on the rsync server
    set OUTFILE = ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_${CAT}_spectral_template_description.txt
    echo "Making $OUTFILE from list of spectral files"
    if ( -e $OUTFILE ) rm $OUTFILE

echo "#Author, version and date: OpSim (T.Dwelly), vs 1.0, `date`\
#DRS: ${CAT}\
#Column 1: A30  Template spectrum filename\
#Column 2: A2   Resolution mode(HR or LR)\
#Column 3: I5   Exposure time in seconds\
#Column 4: I1   Extended object flag (0=point source,1=extended source)\
#Column 5: I1   Moon phase code, 1=dark,2=grey,3=bright" > $OUTFILE

#    rsync --password-file=/home/tdwelly/.4mostsync "4M${CAT}@4most.mpe.mpg.de::4M${CAT}-in/*.fits" | gawk -v cat="$CAT" -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]} " 'BEGIN {split(str_texp,texp," ");} //{fn=$5;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else{extend_flag=1};for(moon=1;moon<=3;moon++){printf("%-30s %2s %5d %d %d\n", fn, res, texp[moon], extend_flag, moon)}}' >> $OUTFILE
    rsync --password-file=/home/tdwelly/.4mostsync "4M${CAT}@4most.mpe.mpg.de::4M${CAT}-in/*.fits" | gawk -v cat="$CAT" -v str_max_runs="${NUM_TILES_DARKGREY} ${NUM_TILES_DARKGREY} ${NUM_TILES_BRIGHT}"  -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]} " 'BEGIN {split(str_texp,texp," ");split(str_max_runs,max_runs," ");} //{fn=$5;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else{extend_flag=1};for(moon=1;moon<=3;moon++){for(r=1;r<=max_runs[moon];r++){printf("%-30s %2s %5d %d %d\n", fn, res, r*texp[moon], extend_flag, moon)}}}' >> $OUTFILE
     @ I ++
  end

  echo "To copy these to the rsync area you will want to do the following:"
  echo "########################################################"
  echo "########################################################"
  echo "rsync -v --password-file=/home/tdwelly/.4mostsync ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_*_spectral_template_description.txt 4MMPE@4most.mpe.mpg.de::4MMPE-out/"
  echo "########################################################"
  echo ""
endif
######################











exit




#    if ( "TRUE" == "mmmmTRUE" ) then
#      #find the unique spectral teplates that are used in the input catalogues 
#      set I = 1
#      while ( $I <=  $#CATALOGUE_FILENAME_LIST )
#        set CAT = ${CATALOGUE_SHORTNAME_LIST[$I]}
#        set OUTFILE = ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_${CAT}_spectral_template_description_used.txt
#        echo "Making $OUTFILE from spectral files named in ${MAIN_BASE}/${CAT}_in/${CATALOGUE_FILENAME_LIST[$I]}"
#        if ( -e $OUTFILE ) rm $OUTFILE
#  
#        echo "#Author, version and date: OpSim (T.Dwelly), vs 1.1, `date`\
#  #DRS: ${CAT}\
#  #Column 1: A30  Template spectrum filename\
#  #Column 2: A2   Resolution mode(HR or LR)\
#  #Column 3: I5   Exposure time in seconds\
#  #Column 4: I1   Extended object flag (0=point source,1=extended source)\
#  #Column 5: I1   Moon phase code, 1=dark,2=grey,3=bright" > $OUTFILE
#  
#      #some extra kludges in here to deal with an "A" missing off the fronts of some filenames supplied in the BAO catalogue
#  #    gawk '$1!~/^#/&&NF>=11{print $11}' ${MAIN_BASE}/${CAT}_in/${CATALOGUE_FILENAME_LIST[$I]} | sort -u | gawk -v cat="$CAT" -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]} " 'BEGIN {split(str_texp,texp," ");} //{fn=$1;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else {extend_flag=1};if(cat=="BAO" && fn~/^GN_v1/){fn=sprintf("A%s",fn)};if(fn~/fit$/){fn=sprintf("%ss",fn)};for(moon=1;moon<=3;moon++){printf("%-30s %2s %5d %d %d\n", fn, res, texp[moon], extend_flag, moon)}}' | sort -u >> $OUTFILE
#        #this takes a long time, so comment it out
#        if ( "TRUE" == "nTRUE" ) then
#          gawk '$1!~/^#/&&NF>=11{print $11}' ${MAIN_BASE}/${CAT}_in/${CATALOGUE_FILENAME_LIST[$I]} | sort -u >   ${MAIN_BASE}/${CAT}_in/${CAT}_unique_spectra_used.txt
#        endif
#  
#        gawk -v cat="$CAT" -v str_max_runs="2 2 2" -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]}" 'BEGIN {split(str_texp,texp," ");split(str_max_runs,max_runs," ");} //{fn=$1;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else {extend_flag=1};if(cat=="BAO" && fn~/^GN_v1/){fn=sprintf("A%s",fn)};if(fn~/fit$/){fn=sprintf("%ss",fn)};for(moon=1;moon<=3;moon++){for(r=1;r<=max_runs[moon];r++){printf("%-30s %2s %5d %d %d\n", fn, res, r*texp[moon], extend_flag, moon)}}}' ${MAIN_BASE}/${CAT}_in/${CAT}_unique_spectra_used.txt | sort -u >> $OUTFILE
#  
#  #    if ( "$CAT" == "BAO" ) then
#  #      gawk '//{if($1~/^GN_v1/){printf("A%s\n",$0)}else {print $0}}' $OUTFILE > ${OUTFILE}.tmp
#  #      mv  ${OUTFILE}.tmp $OUTFILE
#  #    endif
#      endif

