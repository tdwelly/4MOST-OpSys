//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_statistics_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.111 $"
//#define CVS_DATE     "$Date: 2015/11/10 15:45:49 $"
//--    Note: Definitions are in the corresponding header file OpSim_statistics_lib.h
//--    Description:
//--      This module is used to calculate various statistics
//--

#include <chealpix.h>   //needed for nice map making
#include <assert.h>   //needed for debug

#include "OpSim_statistics_lib.h"
#include "OpSim_FoMs_lib.h"
#include "geometry_lib.h"


#define ERROR_PREFIX   "#-OpSim_statistics_lib.c:Error  :"
#define WARNING_PREFIX "#-OpSim_statistics_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_statistics_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_statistics_lib.c:DEBUG  :"
#define RESULTS_PREFIX "#-OpSim_statistics_lib.c:RESULTS:"


////////////////////////////////////////////////////////////////////////
//functions



int OpSim_statistics_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}


///////////////////////////////////////////////////////////////////////////////////////
//make some statistics about numbers of Fields that will receive certain number of tiles
int calc_tiles_per_field_stats (FILE *pFile, fieldlist_struct *pFieldList, survey_struct *pSurvey )
{
  int m,j,i;
  const char* str_prefix = "TILES_PER_FIELD_STATS"; 
  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    moon_struct *pMoon = (moon_struct*)  &(pSurvey->moon[m]);
    pMoon->max_num_tiles_per_field_req = 0;
    pMoon->max_num_tiles_per_field_todo = 0;
    pMoon->max_num_tiles_per_field_done = 0;
    for (i=0;i<MAX_TILES_PER_FIELD+1;i++)
    {
      pMoon->histo_tiles_per_field_req[i] = 0;
      pMoon->histo_tiles_per_field_todo[i] = 0;
      pMoon->histo_tiles_per_field_done[i] = 0;
    }
    for ( j = 0; j < pFieldList->nFields; j++)
    {
      field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
      if (pF->ntiles_req[pMoon->group_code] >= 0 &&
          pF->ntiles_req[pMoon->group_code] <  MAX_TILES_PER_FIELD + 1)
      {
        pMoon->histo_tiles_per_field_req[pF->ntiles_req[pMoon->group_code]] ++;
      }
      if ( pF->ntiles_req[pMoon->group_code] > pMoon->max_num_tiles_per_field_req)
        pMoon->max_num_tiles_per_field_req = pF->ntiles_req[pMoon->group_code];
      
      if (pF->ntiles_todo[pMoon->group_code] >= 0 &&
          pF->ntiles_todo[pMoon->group_code] <  MAX_TILES_PER_FIELD + 1)
      {
        pMoon->histo_tiles_per_field_todo[pF->ntiles_todo[pMoon->group_code]] ++;
      }
      if ( pF->ntiles_todo[pMoon->group_code] > pMoon->max_num_tiles_per_field_todo)
        pMoon->max_num_tiles_per_field_todo = pF->ntiles_todo[pMoon->group_code];

      {
        int ndone_in_group;
        if ( pMoon->group_code == MOON_GROUP_BB )
          ndone_in_group = pF->ntiles_done[MOON_PHASE_BRIGHT];
        else
          ndone_in_group = pF->ntiles_done[MOON_PHASE_DARK] + pF->ntiles_done[MOON_PHASE_GREY];
        
        if (ndone_in_group >= 0 &&
            ndone_in_group <  MAX_TILES_PER_FIELD + 1)
        {
          pMoon->histo_tiles_per_field_done[ndone_in_group] ++;
        }
        if ( ndone_in_group > pMoon->max_num_tiles_per_field_done)
          pMoon->max_num_tiles_per_field_done = ndone_in_group;
      }
    }
  }
    //now report the results
  if ( pFile != NULL )
  {
    fprintf(pFile, "%s Here is a summary of the numbers of tiles planned/done per field:\n", COMMENT_PREFIX);
    fprintf(pFile, "%s #%s %8s %8s %8s %8s %8s %8s %8s\n",
            COMMENT_PREFIX, str_prefix, "NTILES", "NREQ_DG","NREQ_BB", "NTODO_DG","NTODO_BB", "NDONE_DG","NDONE_BB");
    for(i=0;i<MAX_TILES_PER_FIELD+1;i++)
    {
      if (pSurvey->pmoon_dg->histo_tiles_per_field_req[i]  > 0 ||
          pSurvey->pmoon_bb->histo_tiles_per_field_req[i]  > 0 ||
          pSurvey->pmoon_dg->histo_tiles_per_field_todo[i] > 0 ||
          pSurvey->pmoon_bb->histo_tiles_per_field_todo[i] > 0 ||
          pSurvey->pmoon_dg->histo_tiles_per_field_done[i] > 0 ||
          pSurvey->pmoon_bb->histo_tiles_per_field_done[i] > 0)
      {
        fprintf(pFile, "%s  %s %8d %8d %8d %8d %8d %8d %8d\n", COMMENT_PREFIX, str_prefix,
                i,
                pSurvey->pmoon_dg->histo_tiles_per_field_req[i],
                pSurvey->pmoon_bb->histo_tiles_per_field_req[i],
                pSurvey->pmoon_dg->histo_tiles_per_field_todo[i],
                pSurvey->pmoon_bb->histo_tiles_per_field_todo[i],
                pSurvey->pmoon_dg->histo_tiles_per_field_done[i],
                pSurvey->pmoon_bb->histo_tiles_per_field_done[i]);
      }
    }
  }
  return 0;
}



////////////////////////////////////////////////////////////////
//calculate some statistics about numbers of objects in fields
int calc_fields_per_object_stats (objectlist_struct *pObjList, survey_struct *pSurvey) 
{
  //  int n_objects_in_n_fields[MAX_FIELDS_PER_OBJECT+1];
  //  int n_objects_in_n_inner_perims[MAX_FIELDS_PER_OBJECT+1];  //measured inside the nominal perimeter
  int *n_objects_in_n_fields = NULL;
  int *n_objects_in_n_inner_perims = NULL;  //measured inside the nominal perimeter
  int n_objects_in_gt0_fields = 0;
  int n_objects_in_gt1_fields = 0;
  int n_objects_in_gt0_inner_perims = 0;
  int n_objects_in_gt1_inner_perims = 0;

  int bounds_n_objects = 0;               
  //  int bounds_n_objects_in_n_fields[MAX_FIELDS_PER_OBJECT+1];
  //  int bounds_n_objects_in_n_inner_perims[MAX_FIELDS_PER_OBJECT+1];  //measured inside stricter bounds 
  int *bounds_n_objects_in_n_fields = NULL;
  int *bounds_n_objects_in_n_inner_perims = NULL;  //measured inside stricter bounds 
  int bounds_n_objects_in_gt0_fields = 0;
  int bounds_n_objects_in_gt1_fields = 0;
  int bounds_n_objects_in_gt0_inner_perims = 0;
  int bounds_n_objects_in_gt1_inner_perims = 0;

  int boundsA_n_objects = 0;               
  //  int boundsA_n_objects_in_n_fields[MAX_FIELDS_PER_OBJECT+1];
  //  int boundsA_n_objects_in_n_inner_perims[MAX_FIELDS_PER_OBJECT+1];  //measured inside stricter bounds 
  int *boundsA_n_objects_in_n_fields = NULL;
  int *boundsA_n_objects_in_n_inner_perims = NULL;  //measured inside stricter bounds 
  int boundsA_n_objects_in_gt0_fields = 0;
  int boundsA_n_objects_in_gt1_fields = 0;
  int boundsA_n_objects_in_gt0_inner_perims = 0;
  int boundsA_n_objects_in_gt1_inner_perims = 0;

  int k,i;

  FILE *pFile = NULL;
  char str_filename[STR_MAX];

  
  if ( pObjList == NULL || pSurvey == NULL ) return 1;

  if ( pObjList->max_num_fields_per_object < 0 ) return 1;
  
  if ((n_objects_in_n_fields               = (int*) calloc((size_t) pObjList->max_num_fields_per_object+1, sizeof(int))) == NULL ||
      (n_objects_in_n_inner_perims         = (int*) calloc((size_t) pObjList->max_num_fields_per_object+1, sizeof(int))) == NULL ||
      (bounds_n_objects_in_n_fields        = (int*) calloc((size_t) pObjList->max_num_fields_per_object+1, sizeof(int))) == NULL ||
      (bounds_n_objects_in_n_inner_perims  = (int*) calloc((size_t) pObjList->max_num_fields_per_object+1, sizeof(int))) == NULL ||
      (boundsA_n_objects_in_n_fields       = (int*) calloc((size_t) pObjList->max_num_fields_per_object+1, sizeof(int))) == NULL ||
      (boundsA_n_objects_in_n_inner_perims = (int*) calloc((size_t) pObjList->max_num_fields_per_object+1, sizeof(int))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  
//  for(k=0;k<MAX_FIELDS_PER_OBJECT+1;k++)
//  {
//    n_objects_in_n_fields[k] = 0;
//    n_objects_in_n_inner_perims[k] = 0;
//    bounds_n_objects_in_n_fields[k] = 0;
//    bounds_n_objects_in_n_inner_perims[k] = 0;
//    boundsA_n_objects_in_n_fields[k] = 0;
//    boundsA_n_objects_in_n_inner_perims[k] = 0;
//  }
  
  for(i=0;i<pObjList->nObjects;i++)
  {
    int flag = 0;
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    n_objects_in_n_fields[pObj->num_fields] ++;
    for(k=0;k<pObj->num_fields;k++)
    {
      //      if ( pObj->pperim_flags[k] == FIELD_FLAG_INSIDE ) flag ++;
      if ( pObj->pObjField[k].perim_flag == FIELD_FLAG_INSIDE ) flag ++;
    }
    n_objects_in_n_inner_perims[flag]++;

    if ( pObj->dec >= pSurvey->bounds_dec_min && pObj->dec <= pSurvey->bounds_dec_max )
    {
      int bounds_flag = 0;
      bounds_n_objects ++;
      bounds_n_objects_in_n_fields[pObj->num_fields] ++;
      for(k=0;k<pObj->num_fields;k++)
      {
        //        if ( pObj->pperim_flags[k] == FIELD_FLAG_INSIDE ) bounds_flag ++;
        if ( pObj->pObjField[k].perim_flag == FIELD_FLAG_INSIDE ) bounds_flag ++;
      }
      bounds_n_objects_in_n_inner_perims[bounds_flag]++;
    }

    if ( pObj->dec >= pSurvey->boundsA_dec_min && pObj->dec <= pSurvey->boundsA_dec_max )
    {
      int boundsA_flag = 0;
      boundsA_n_objects ++;
      boundsA_n_objects_in_n_fields[pObj->num_fields] ++;
      for(k=0;k<pObj->num_fields;k++)
      {
        //        if ( pObj->pperim_flags[k] == FIELD_FLAG_INSIDE ) boundsA_flag ++;
        if ( pObj->pObjField[k].perim_flag == FIELD_FLAG_INSIDE ) boundsA_flag ++;
      }
      boundsA_n_objects_in_n_inner_perims[boundsA_flag]++;
    }
  }

  snprintf(str_filename, STR_MAX, "%s/fields_per_object_stats.txt", PLOTFILES_SUBDIRECTORY);
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the fields per object stats file : %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  fprintf(stdout, "%s Calculating the fields per object stats, writing to file: %s\n", COMMENT_PREFIX, str_filename);

  fprintf(pFile, "%s Statistics of the object->field assignments\n", COMMENT_PREFIX);
  fprintf(pFile, "%s Figures in {} brackets are for the %8d targets in the full       Dec range: %+7.3f<Dec<%+7.3f\n",
          COMMENT_PREFIX, pObjList->nObjects,
          MIN(DEFAULT_SURVEY_DEC_LIMIT_MIN,pSurvey->bounds_dec_min),
          MAX(DEFAULT_SURVEY_DEC_LIMIT_MAX,pSurvey->bounds_dec_max));
  fprintf(pFile, "%s Figures in [] brackets are for the %8d targets in the tiled      Dec range: %+7.3f<Dec<%+7.3f\n",
          COMMENT_PREFIX, bounds_n_objects,
          pSurvey->bounds_dec_min, pSurvey->bounds_dec_max);
  fprintf(pFile, "%s Figures in () brackets are for the %8d targets in the restricted Dec range: %+7.3f<Dec<%+7.3f\n",
          COMMENT_PREFIX, boundsA_n_objects,
          pSurvey->boundsA_dec_min, pSurvey->boundsA_dec_max);
  
  for(k=0;k<pObjList->max_num_fields_per_object+1;k++)
  {
    fprintf(pFile, "%s There were {%8d} [%8d] (%8d) objects that were assigned to %3d field%s\n",
            COMMENT_PREFIX,  n_objects_in_n_fields[k],
            bounds_n_objects_in_n_fields[k], boundsA_n_objects_in_n_fields[k],
            k, (k==1?"":"s"));
    if (k > 0 ) {
      n_objects_in_gt0_fields += n_objects_in_n_fields[k];
      bounds_n_objects_in_gt0_fields  += bounds_n_objects_in_n_fields[k];
      boundsA_n_objects_in_gt0_fields += boundsA_n_objects_in_n_fields[k];
    }
    if (k > 1 )
    {
      n_objects_in_gt1_fields += n_objects_in_n_fields[k];
      bounds_n_objects_in_gt1_fields  += bounds_n_objects_in_n_fields[k];
      boundsA_n_objects_in_gt1_fields += boundsA_n_objects_in_n_fields[k];
    }
  }
  fprintf(pFile, "%s There were {%8d} [%8d] (%8d) objects that were assigned to >=1 Fields\n",
          COMMENT_PREFIX,  n_objects_in_gt0_fields,
          bounds_n_objects_in_gt0_fields, boundsA_n_objects_in_gt0_fields);
  fprintf(pFile, "%s There were {%8d} [%8d] (%8d) objects that were assigned to  >1 Field\n",
          COMMENT_PREFIX,  n_objects_in_gt1_fields,
          bounds_n_objects_in_gt1_fields, boundsA_n_objects_in_gt1_fields);
  fprintf(pFile, "%s i.e. the nominal Field overlap fraction is {%8.5f +- %8.6f} [%8.5f +- %8.6f] (%8.5f +- %8.6f) - assuming the objects are randomly distributed\n",
          COMMENT_PREFIX,
          (float) n_objects_in_gt1_fields / (float) MAX(n_objects_in_gt0_fields,1),
          (float) sqrt(n_objects_in_gt1_fields) / (float) MAX(n_objects_in_gt0_fields,1),
          (float) bounds_n_objects_in_gt1_fields / (float) MAX(bounds_n_objects_in_gt0_fields,1),
          (float) sqrt(bounds_n_objects_in_gt1_fields) / (float) MAX(bounds_n_objects_in_gt0_fields,1),
          (float) boundsA_n_objects_in_gt1_fields / (float) MAX(boundsA_n_objects_in_gt0_fields,1),
          (float) sqrt(boundsA_n_objects_in_gt1_fields) / (float) MAX(boundsA_n_objects_in_gt0_fields,1));

  fprintf(pFile, "%s The fraction of targets not assigned to a Field is {%8.5f +- %8.6f} [%8.5f +- %8.6f] (%8.5f +- %8.6f)\n",
          COMMENT_PREFIX,
          (float) n_objects_in_n_fields[0] / (float) MAX(pObjList->nObjects,1),
          (float) sqrt(n_objects_in_n_fields[0]) / (float) MAX(pObjList->nObjects,1),
          (float) bounds_n_objects_in_n_fields[0] / (float) MAX(bounds_n_objects,1),
          (float) sqrt(bounds_n_objects_in_gt1_fields) / (float) MAX(bounds_n_objects,1),
          (float) boundsA_n_objects_in_n_fields[0] / (float) MAX(boundsA_n_objects,1),
          (float) sqrt(boundsA_n_objects_in_gt1_fields) / (float) MAX(boundsA_n_objects,1));
  
  for(k=0;k<pObjList->max_num_fields_per_object+1;k++)
  {
    fprintf(pFile, "%s There were {%8d} [%8d] (%8d) objects that were in the nominal perimeters of %3d Field%s\n",
            COMMENT_PREFIX,  n_objects_in_n_inner_perims[k],
            bounds_n_objects_in_n_inner_perims[k], boundsA_n_objects_in_n_inner_perims[k],
            k, (k==1?"":"s"));
    if (k > 0 ) {
      n_objects_in_gt0_inner_perims += n_objects_in_n_inner_perims[k];
      bounds_n_objects_in_gt0_inner_perims  += bounds_n_objects_in_n_inner_perims[k];
      boundsA_n_objects_in_gt0_inner_perims += boundsA_n_objects_in_n_inner_perims[k];
    }
    if (k > 1 ) {
      n_objects_in_gt1_inner_perims += n_objects_in_n_inner_perims[k];
      bounds_n_objects_in_gt1_inner_perims  += bounds_n_objects_in_n_inner_perims[k];
      boundsA_n_objects_in_gt1_inner_perims += boundsA_n_objects_in_n_inner_perims[k];
    }
  }
  fprintf(pFile, "%s There were {%8d} [%8d] (%8d) objects that were assigned to >=1 inner perimeters\n",
          COMMENT_PREFIX,  n_objects_in_gt0_inner_perims,
          bounds_n_objects_in_gt0_inner_perims, boundsA_n_objects_in_gt0_inner_perims);
  fprintf(pFile, "%s There were {%8d} [%8d] (%8d) objects that were assigned to  >1 inner perimeters\n",
          COMMENT_PREFIX,  n_objects_in_gt1_inner_perims,
          bounds_n_objects_in_gt1_inner_perims, boundsA_n_objects_in_gt1_inner_perims);
  fprintf(pFile, "%s i.e. the nominal inner perimeter overlap fraction is {%8.5f +- %8.6f} [%8.5f +- %8.6f] (%8.5f +- %8.6f) - assuming the objects are randomly distributed\n",
          COMMENT_PREFIX,
          (float) n_objects_in_gt1_inner_perims / (float) MAX(n_objects_in_gt0_inner_perims,1),
          (float) sqrt(n_objects_in_gt1_inner_perims) / (float) MAX(n_objects_in_gt0_inner_perims,1),
          (float) bounds_n_objects_in_gt1_inner_perims / (float) MAX(bounds_n_objects_in_gt0_inner_perims,1),
          (float) sqrt(bounds_n_objects_in_gt1_inner_perims) / (float) MAX(bounds_n_objects_in_gt0_inner_perims,1),
          (float) boundsA_n_objects_in_gt1_inner_perims / (float) MAX(boundsA_n_objects_in_gt0_inner_perims,1),
          (float) sqrt(boundsA_n_objects_in_gt1_inner_perims) / (float) MAX(boundsA_n_objects_in_gt0_inner_perims,1));
  fprintf(pFile, "%s\n", COMMENT_PREFIX);

  //  if (bounds_n_objects_in_n_fields       ) free (bounds_n_objects_in_n_fields) ; bounds_n_objects_in_n_fields = NULL;
  //  if (bounds_n_objects_in_n_inner_perims ) free (bounds_n_objects_in_n_inner_perims) ; bounds_n_objects_in_n_inner_perims = NULL;
  FREETONULL(n_objects_in_n_fields);
  FREETONULL(n_objects_in_n_inner_perims);
  FREETONULL(bounds_n_objects_in_n_fields);
  FREETONULL(bounds_n_objects_in_n_inner_perims);
  FREETONULL(boundsA_n_objects_in_n_fields);
  FREETONULL(boundsA_n_objects_in_n_inner_perims);

  if ( pFile ) fclose(pFile); pFile = NULL;

  return 0;
}
////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////
int calc_num_targets_with_bright_neighbours ( focalplane_struct *pFocal, survey_struct *pSurvey,
                                              int* pnum_lores, int* pnum_hires) 
//calculate the number of faint objects that have bad neighbours (i.e. very bright objects)
//this method just assumes that all fibers of a given resolution are laid out onto a single CCD
//and are ordered according to their index. 
{
  int i;
  int num_targets_with_problem_neighbours_lores = 0;
  int num_targets_with_problem_neighbours_hires = 0;
  if ( pFocal == NULL || pSurvey == NULL ) return 1;

  for (i=0;i<pFocal->num_fibers;i++)
  {
    fiber_struct *pThisFib = (fiber_struct*) &(pFocal->pFib[i]);
    fiber_struct *pNextFib = NULL;
    fiber_struct *pLastFib = NULL;
    float last_mag = 99.0;
    float next_mag = 99.0;
    int j;

    //find the (up to) two neighbouring fibers of same resolution
    j = i - 1;
    while ( j >= 0 )
    {
      pLastFib = (fiber_struct*) &(pFocal->pFib[j]);
      if ( pLastFib->res == pThisFib->res ) break;
      j--;
    }
    j = i + 1;
    while ( j < pFocal->num_fibers -1 )
    {
      pNextFib = (fiber_struct*) &(pFocal->pFib[j]);
      if ( pNextFib->res == pThisFib->res ) break;
      j++;
    }
    
    
    if ( pLastFib != NULL )
    {
      if ( pLastFib->assign_flag == FIBER_ASSIGN_CODE_TARGET &&
           pLastFib->pObj != NULL ) last_mag = pLastFib->pObj->mag;
    }
    if ( pNextFib != NULL )
    {
      if ( pNextFib->assign_flag == FIBER_ASSIGN_CODE_TARGET &&
           pNextFib->pObj != NULL ) next_mag = pNextFib->pObj->mag;
    }
    
    if ( pThisFib->assign_flag == FIBER_ASSIGN_CODE_TARGET &&
         pThisFib->pObj != NULL )
    {
      if ( pThisFib->pObj->mag - last_mag > (double) pSurvey->delta_mag_for_problem_neighbour ||
           pThisFib->pObj->mag - next_mag > (double) pSurvey->delta_mag_for_problem_neighbour )
      {
        if      ( pThisFib->pObj->res == RESOLUTION_CODE_LOW  ) num_targets_with_problem_neighbours_lores++;
        else if ( pThisFib->pObj->res == RESOLUTION_CODE_HIGH ) num_targets_with_problem_neighbours_hires++;
      }
    }
  }
  ///////////////////////////////////////////////
  if (pnum_lores) *pnum_lores = num_targets_with_problem_neighbours_lores;
  if (pnum_hires) *pnum_hires = num_targets_with_problem_neighbours_hires;

  return 0;
}
  
  


//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//----------------------------------MAPS---------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------//

//  /////////////////////////////////////////////////////////////////
//  //can now build stats from input catalogues
//  //expect this section to increase substantially
//  //write standard named files into the simulation directory
//  //normally will not carry out this section if it is too slow
//  //build histograms over the ra,dec range given by the survey bounds
//  output file in format suitable for plotting with gnuplot pm3d
//  also output some simple stats as comments at bottom of file

int create_catalogue_density_map (survey_struct *pSurvey,
                                  objectlist_struct *pObjList,
                                  catalogue_struct *pCat, 
                                  const char *str_filename,
                                  int (*test_funcA)(object_struct*,multi_type*),
                                  multi_type *pMultiA,
                                  int (*test_funcB)(object_struct*,multi_type*),
                                  multi_type *pMultiB)
{
  FILE   *pFile = NULL;
  int     i,j;
  int    *p2dhisto =  NULL;
  float  *parea    =  NULL;
  double *pdra     =  NULL;
  double *pinv_dra =  NULL;
  int    *pnra     =  NULL;
  int    npix_ra;
  int    npix_dec;
  double ra_min    =   0.0;
  double ra_max    = 360.0;
  double dec_min   = -90.0;
  double dec_max   = +90.0;
  double dra0;   //width at the equator
  double ddec0;
  double inv_ddec0;
  int index_of_first_object;
  int index_of_last_object;

  if (pSurvey == NULL ||
      pObjList == NULL ) return 1;

  npix_ra = pSurvey->map_npix_ra;
  npix_dec = pSurvey->map_npix_dec;
  if ( npix_ra <= 0 || npix_dec <= 0 ) return 1;
  dra0      = (double) ( ra_max -  ra_min) / ((double) npix_ra) ;   //width at the equator
  ddec0     = (double) (dec_max - dec_min) / ((double) npix_dec) ;
  if ( ddec0 > 0.0 ) inv_ddec0 = (double) 1.0 /ddec0;
  else               return 1;

  
  //  fprintf(stdout, "%s Writing catalogue_density_map: %s\n",  COMMENT_PREFIX, str_filename);

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the catalogue_density_map file: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }

  if ((p2dhisto = (int*)    calloc((size_t) npix_ra*npix_dec, sizeof(int))) == NULL ||
      (parea    = (float*)  malloc(sizeof(float)  * npix_dec))     == NULL ||
      (pdra     = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pinv_dra = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pnra     = (int*)    malloc(sizeof(int)    * npix_dec))     == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for(i=0;i<npix_dec;i++)
  {
    double dec0  = dec_min + ddec0*((double) i + 0.5);
    double dec1  = dec0 - ddec0*0.5;
    double dec2  = dec0 + ddec0*0.5;
    double width = (ra_max - ra_min)*cosd(dec0);
    double npix  = floor(width / dra0);
    pnra[i] = (int) npix;
    pdra[i] = (ra_max - ra_min) / npix;
    parea[i] = 0.5*FULL_SKY_AREA_SQDEG * (sind(dec2) - sind(dec1)) / (double) npix; 
    pinv_dra[i] = ( pdra[i] > 0.0 ? (double)  1.0 / pdra[i] : NAN);
  }

  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }

  //  for (i=0;i<pObjList->nObjects; i++)
  for (i=index_of_first_object;i<=index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    //    if ( pObj->cat_code == (utiny) catalogue_code)
    //    if ( pObj->cat_code == (utiny) catalogue_code ||
    //         catalogue_code == CATALOGUE_CODE_ALL )
    {
      if ( test_funcA )
      {
        if ( test_funcA((object_struct*) pObj, (multi_type*) pMultiA) != 0 ) continue;
      }
      if ( test_funcB )
      {
        if ( test_funcB((object_struct*) pObj, (multi_type*) pMultiB) != 0 ) continue;
      }

      {
        int pix_y = (int) ((pObj->dec - dec_min)*inv_ddec0); 
        int pix_x = (int) ((pObj->ra  - ra_min)*pinv_dra[pix_y]);
        if ( pix_y < 0 ||
             pix_y > npix_dec ||
             pix_x < 0 ||
             pix_x > pnra[pix_y] )
        {
          fprintf(stderr, "%s badpix %8d %12.7f %12.7f %6d %6d\n", ERROR_PREFIX, i, pObj->ra, pObj->dec, pix_y, pix_x);
        }
        else
        {
          p2dhisto[pix_x + pix_y*npix_ra] ++;
        }
      }
    }
  }

  
  //now write output to file
  //write so that the density value is associated with the bottom lefthand corner of each pixel
  //write two scans for each dec row (upper and lower dec limits)
  fprintf(pFile, "#%-11s %12s %8s %12s\n", "RA", "DEC", "pix_area", "pix_value");
  for(i=0;i<npix_dec;i++)
  {
    double dec1  = dec_min + ddec0*((double) i);
    double ra;
    int row;
    //suppress rows completely outside dec bounds
    if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
    for(row=0;row<2;row++)
    {
      double dec = dec1 + ddec0 * (double) row;
      fprintf(pFile, "\n");
      for(j=0;j<pnra[i];j++)
      {
        ra = ra_min + ((double) j)*pdra[i];
        fprintf(pFile, "%12.5f %12.5f %8.4f %8d\n", ra, dec, parea[i], p2dhisto[j + i*npix_ra]);
      }
      //print the right hand corner of the last pixel
      ra = ra_min + ((double) j)*pdra[i];
      fprintf(pFile, "%12.5f %12.5f %8.4f %8d\n", ra, dec, parea[i], p2dhisto[(j-1) + i*npix_ra]);
    }
  }


  //  //now calculate density stats
  if ( calc_catalogue_density_stats ((survey_struct*) pSurvey, pFile,
                                     (int*) p2dhisto, (float*) parea,
                                     (int*) pnra, (int) npix_ra, (int) npix_dec)) return 1;


  
  if ( pFile) fclose(pFile);  pFile = NULL;
  
  FREETONULL( pnra )    ;
  FREETONULL( pdra )    ;
  FREETONULL( pinv_dra );
  FREETONULL( parea )   ;
  FREETONULL( p2dhisto) ;
  
  return 0;
}


int calc_catalogue_density_stats (survey_struct *pSurvey,
                                  FILE   *pFile,
                                  int    *p2dhisto,
                                  float  *parea,
                                  int    *pnra,
                                  int     npix_ra,
                                  int     npix_dec)
{
  const double dec_min   = -90.0;
  const double dec_max   = +90.0;
  const float density_levels[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS] = {10.0, 30.0, 50.0, 75.0, 100.0, 200.0, 300.0, 400.0, 500.0, 1000.0 }; 
  int i,j;
  int d;
  float den_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float den2_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  int   npix_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float area_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];  
  float mean_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];  
  float stdev_gt[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float min_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float max_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
    
  float den_all     = 0.0;  //for all pixels in bounds of map
  float den_survey  = 0.0;  //for all pixels strictly within survey dec limits
  float den_gt0     = 0.0;  //for all pixels with n members > 0
  float den2_all    = 0.0;
  float den2_survey = 0.0;
  float den2_gt0    = 0.0;
  int   npix_all    = 0;
  int   npix_survey = 0;
  int   npix_gt0    = 0;
  float area_all     = 0.0;  
  float area_survey  = 0.0;  
  float area_gt0     = 0.0;  
  float mean_all     = 0.0;  
  float mean_survey  = 0.0;  
  float mean_gt0     = 0.0;  
  float stdev_all     = 0.0;  
  float stdev_survey  = 0.0;  
  float stdev_gt0     = 0.0;
  float min_all     =  FLT_MAX;  
  float min_survey  =  FLT_MAX;  
  float min_gt0     =  FLT_MAX;
  float max_all     = -1.0*FLT_MAX;  
  float max_survey  = -1.0*FLT_MAX;  
  float max_gt0     = -1.0*FLT_MAX;

  for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
  {
    den_gt[d]   = 0.0;
    den2_gt[d]  = 0.0;
    npix_gt[d]  =   0;
    area_gt[d]  = 0.0;  
    mean_gt[d]  = 0.0;  
    stdev_gt[d] = 0.0;
    min_gt[d]   = FLT_MAX;
    max_gt[d]   = FLT_MAX*-1.0;
  }
    
  for(i=0;i<npix_dec;i++)
  {
    double ddec0 = (double) (dec_max - dec_min) / ((double) npix_dec) ;
    double dec1  = dec_min + ddec0*((double) i);
    float inv_area = 1.0 / (parea[i] >0.0 ? parea[i] : 1e30);
    if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
    for(j=0;j<pnra[i];j++)
    {
      float density = (float) p2dhisto[j + i*npix_ra] *inv_area;
      den_all += (float) density;
      den2_all += (float) SQR(density);
      npix_all ++;
      area_all += parea[i];
      if ( density > max_all ) max_all = density;
      if ( density < min_all ) min_all = density;
         
      if ( dec1       <= pSurvey->tiling_dec_max &&
           dec1+ddec0 >= pSurvey->tiling_dec_min)
      {
        den_survey += (float) density;
        den2_survey += (float) SQR(density);
        npix_survey ++;
        area_survey += parea[i];
        if ( density > max_survey ) max_survey = density;
        if ( density < min_survey ) min_survey = density;
      }

      if ( p2dhisto[j + i*npix_ra] > 0)
      {
        den_gt0 += (float) density;
        den2_gt0 += (float) SQR(density);
        npix_gt0 ++;
        area_gt0 += parea[i];
        if ( density > max_gt0 ) max_gt0 = density;
        if ( density < min_gt0 ) min_gt0 = density;
      }

      for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
      {
        if ( density > density_levels[d] )
        {
          den_gt[d] += (float) density;
          den2_gt[d] += (float) SQR(density);
          npix_gt[d] ++;
          area_gt[d] += parea[i];
          if ( density > max_gt[d] ) max_gt[d] = density;
          if ( density < min_gt[d] ) min_gt[d] = density;
        }
      }
    }
  }
  if ( npix_all    > 0 ) mean_all = (den_all / (float) npix_all);
  if ( npix_all    > 1 ) stdev_all = sqrt(den2_all*npix_all - SQR(den_all))/(float) (npix_all-1);
  if ( npix_survey > 0 ) mean_survey = (den_survey / (float) npix_survey);
  if ( npix_survey > 1 ) stdev_survey = sqrt(den2_survey*npix_survey - SQR(den_survey))/(float) (npix_survey-1);
  if ( npix_gt0    > 0 ) mean_gt0 = (den_gt0 / (float) npix_gt0);
  if ( npix_gt0    > 1 ) stdev_gt0 = sqrt(den2_gt0*npix_gt0 - SQR(den_gt0))/(float) (npix_gt0-1);

  fprintf(pFile, "\n\n#SkyDensitySummary %6s : %10s %10s %10s %10s %10s %10s %10s\n", "SUBSET", "num_pix", "area", "mean", "stdev", "min", "max", "thresh");
  fprintf(pFile, "#SkyDensitySummary %6s : %10s %10s %10s %10s %10s %10s\n", "-", "-", "(deg2)", "(deg^-2)", "(deg^-2)", "(deg^-2)", "(deg^-2)");
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "ALL",
          npix_all, area_all, mean_all, stdev_all, min_all, max_all);
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "SURVEY",
          npix_survey, area_survey, mean_survey, stdev_survey, min_survey, max_survey);
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "GT0",
          npix_gt0, area_gt0, mean_gt0, stdev_gt0, min_gt0, max_gt0);
  for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
  {
    if ( npix_gt[d] > 0 )
    {
      mean_gt[d] = (den_gt[d] / (float) npix_gt[d]);
      if ( npix_gt[d] > 1 )stdev_gt[d] = sqrt(den2_gt[d]*npix_gt[d] - SQR(den_gt[d]))/(float) (npix_gt[d]-1);
      fprintf(pFile, "#SkyDensitySummary GT%-4.0f : %10d %10.3f %10.5f %10.5f %10.5f %10.5f %10.5g\n",
              density_levels[d], npix_gt[d], area_gt[d], mean_gt[d], stdev_gt[d], min_gt[d], max_gt[d], density_levels[d]);
    }
  }

  fprintf(pFile, "\n\n");
  return 0;
}



//same as above but calculates ratio of cat1/cat2
int create_catalogue_density_ratio_map (survey_struct *pSurvey,
                                        objectlist_struct *pObjList,
                                        catalogue_struct *pCat, 
                                        const char *str_filename,
                                        int (*test_func1A)(object_struct*,multi_type*),
                                        multi_type *pMulti1A,
                                        int (*test_func1B)(object_struct*,multi_type*),
                                        multi_type *pMulti1B,
                                        int (*test_func2A)(object_struct*,multi_type*),
                                        multi_type *pMulti2A,
                                        int (*test_func2B)(object_struct*,multi_type*),
                                        multi_type *pMulti2B)
{
  FILE   *pFile = NULL;
  int     i,j;
  int    *p2dhisto1 =  NULL;
  int    *p2dhisto2 =  NULL;
  float  *parea    =  NULL;
  double *pdra     =  NULL;
  double *pinv_dra     =  NULL;
  int    *pnra     =  NULL;
  int    npix_ra;
  int    npix_dec;
  double ra_min    =   0.0;
  double ra_max    = 360.0;
  double dec_min   = -90.0;
  double dec_max   = +90.0;
  double dra0;   //width at the equator
  double ddec0;
  double inv_ddec0;
  int index_of_first_object;
  int index_of_last_object;

  if (pSurvey == NULL ||
      pObjList == NULL ) return 1;

  npix_ra = pSurvey->map_npix_ra;
  npix_dec = pSurvey->map_npix_dec;
  if ( npix_ra <= 0 || npix_dec <= 0 ) return 1;
  dra0      = (double) ( ra_max -  ra_min) / ((double) npix_ra) ;   //width at the equator
  ddec0     = (double) (dec_max - dec_min) / ((double) npix_dec) ;
  if ( ddec0 > 0.0 ) inv_ddec0 = (double) 1.0 /ddec0;
  else               return 1;
  
  //  fprintf(stdout, "%s Writing catalogue_density_ratio_map: %s\n",  COMMENT_PREFIX, str_filename);

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the catalogue_density_ratio_map file: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }

  if ((p2dhisto1 = (int*)    calloc((size_t) npix_ra*npix_dec, sizeof(int))) == NULL ||
      (p2dhisto2 = (int*)    calloc((size_t) npix_ra*npix_dec, sizeof(int))) == NULL ||
      (parea     = (float*)  malloc(sizeof(float)  * npix_dec))     == NULL ||
      (pdra      = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pinv_dra  = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pnra      = (int*)    malloc(sizeof(int)    * npix_dec))     == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for(i=0;i<npix_dec;i++)
  {
    double dec0  = dec_min + ddec0*((double) i + 0.5);
    double dec1  = dec0 - ddec0*0.5;
    double dec2  = dec0 + ddec0*0.5;
    double width = (ra_max - ra_min)*cosd(dec0);
    double npix  = floor(width / dra0);
    pnra[i] = (int) npix;
    pdra[i] = (ra_max - ra_min) / (double) npix;
    parea[i] = 0.5*FULL_SKY_AREA_SQDEG * (sind(dec2) - sind(dec1)) / (double) npix; 
    pinv_dra[i] = ( pdra[i] > 0.0 ? (double)  1.0 / pdra[i] : NAN);
  }
 
  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }

  for (i=index_of_first_object;i<=index_of_last_object; i++)
  {
    int flag1A = 0; //just in case test_func1B and test_func2B are null
    int flag2A = 0;
    int flag1B = 0; 
    int flag2B = 0;
    object_struct *pObj = &(pObjList->pObject[i]); 
    if ( test_func1A )  flag1A = test_func1A((object_struct*) pObj, (multi_type*) pMulti1A);
    if ( test_func2A )  flag2A = test_func2A((object_struct*) pObj, (multi_type*) pMulti2A);
    if ( test_func1B )  flag1B = test_func1B((object_struct*) pObj, (multi_type*) pMulti1B);
    if ( test_func2B )  flag2B = test_func2B((object_struct*) pObj, (multi_type*) pMulti2B);
    
    if ( (flag1A == 0 && flag1B == 0) || (flag2A == 0 && flag2B == 0))
    {
      int pix_y = (int) ((pObj->dec - dec_min)*inv_ddec0); 
      int pix_x = (int) ((pObj->ra  - ra_min)*pinv_dra[pix_y]);
      if ( pix_y < 0 ||
           pix_y > npix_dec ||
           pix_x < 0 ||
           pix_x > pnra[pix_y] )
      {
        fprintf(stderr, "%s badpix %8d %12.7f %12.7f %6d %6d\n", ERROR_PREFIX, i, pObj->ra, pObj->dec, pix_y, pix_x);
      }
      else
      {
        if ( flag1A == 0 && flag1B == 0 ) p2dhisto1[pix_x + pix_y*npix_ra] ++;
        if ( flag2A == 0 && flag2B == 0 ) p2dhisto2[pix_x + pix_y*npix_ra] ++;
      }
    }
  }

  
  //now write output to file
  //write so that the density value is associated with the bottom lefthand corner of each pixel
  //write two scans for each dec row (upper and lower dec limits)
  fprintf(pFile, "#%-11s %12s %8s %8s %8s %10s\n", "RA", "DEC", "pix_area", "Nobject1", "Nobject2", "pix_value");
  for(i=0;i<npix_dec;i++)
  {
    double dec1  = dec_min + ddec0*((double) i);
    double ra;
    int row;
    //suppress rows completely outside dec bounds
    if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
    for(row=0;row<2;row++)
    {
      double dec = dec1 + ddec0 * (double) row;
      double ratio = NAN;
      fprintf(pFile, "\n");
      for(j=0;j<pnra[i];j++)
      {
        if (p2dhisto2[j + i*npix_ra]>0 ) ratio = (double) p2dhisto1[j + i*npix_ra] / ((double) p2dhisto2[j + i*npix_ra]);
        else                             ratio = (double) NAN;
        ra = ra_min + ((double) j)*pdra[i];
        fprintf(pFile, "%12.5f %12.5f %8.4f %8d %8d %10.6g\n",
                ra, dec, parea[i], p2dhisto1[j + i*npix_ra], p2dhisto2[j + i*npix_ra], ratio);
      }
      //print the right hand corner of the last pixel
      ra = ra_min + ((double) j)*pdra[i];
      if (p2dhisto2[(j-1) + i*npix_ra]>0 ) ratio = (double) p2dhisto1[(j-1) + i*npix_ra] / ((double) p2dhisto2[(j-1) + i*npix_ra]);
      else                                 ratio = NAN;
      fprintf(pFile, "%12.5f %12.5f %8.4f %8d %8d %10.6g\n",
              ra, dec, parea[i], p2dhisto1[(j-1) + i*npix_ra], p2dhisto2[(j-1) + i*npix_ra], ratio);
    }
  }


  //  //now calculate density stats
  if ( calc_catalogue_density_ratio_stats ((survey_struct*) pSurvey, pFile,
                                           (float*) p2dhisto1,(float*) p2dhisto2, (float*) parea,
                                           (int*) pnra, (int) npix_ra, (int) npix_dec)) return 1;

  if ( pFile) fclose(pFile);  pFile = NULL;
  
  if ( pnra )     free(pnra);       pnra     =NULL;
  if ( pdra )     free(pdra);       pdra     =NULL;
  if ( pinv_dra ) free(pinv_dra);   pinv_dra =NULL;
  if ( parea )    free(parea);      parea    =NULL;
  if ( p2dhisto1) free(p2dhisto1);  p2dhisto1=NULL;
  if ( p2dhisto2) free(p2dhisto2);  p2dhisto2=NULL;

  return 0;
}


//now calculate density ratio stats
int calc_catalogue_density_ratio_stats (survey_struct *pSurvey,
                                        FILE   *pFile,
                                        float  *p2dhisto1,
                                        float  *p2dhisto2,
                                        float  *parea,
                                        int    *pnra,
                                        int     npix_ra,
                                        int     npix_dec)
{
  const double dec_min   = -90.0;
  const double dec_max   = +90.0;
  const float density_levels[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS] = {0.1, 0.3, 0.4, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95}; 
  int i,j;
  int d;
  float den_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float den2_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  int   npix_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float area_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];  
  float mean_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];  
  float stdev_gt[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float min_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float max_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];

  float den_all     = 0.0;  //for all pixels in bounds of map
  float den_survey  = 0.0;  //for all pixels strictly within survey dec limits
  float den_gt0     = 0.0;  //for all pixels with n members > 0
  float den2_all    = 0.0;
  float den2_survey = 0.0;
  float den2_gt0    = 0.0;
  int   npix_all    = 0;
  int   npix_survey = 0;
  int   npix_gt0    = 0;
  float area_all     = 0.0;  
  float area_survey  = 0.0;  
  float area_gt0     = 0.0;  
  float mean_all     = 0.0;  
  float mean_survey  = 0.0;  
  float mean_gt0     = 0.0;  
  float stdev_all     = 0.0;  
  float stdev_survey  = 0.0;  
  float stdev_gt0     = 0.0;
  float min_all     =  FLT_MAX;  
  float min_survey  =  FLT_MAX;  
  float min_gt0     =  FLT_MAX;
  float max_all     = -1.0*FLT_MAX;  
  float max_survey  = -1.0*FLT_MAX;  
  float max_gt0     = -1.0*FLT_MAX;
  if ( pFile     == NULL ||
       p2dhisto1 == NULL ||
       p2dhisto2 == NULL ||
       parea     == NULL ||
       pnra      == NULL    ) return 1;
    
  for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
  {
    den_gt[d]   = 0.0;
    den2_gt[d]  = 0.0;
    npix_gt[d]  =   0;
    area_gt[d]  = 0.0;  
    mean_gt[d]  = 0.0;  
    stdev_gt[d] = 0.0;
    min_gt[d]   = FLT_MAX;
    max_gt[d]   = FLT_MAX*-1.0;
  }
    
  for(i=0;i<npix_dec;i++)
  {
    double ddec0 = (double) (dec_max - dec_min) / ((double) npix_dec) ;
    double dec1  = dec_min + ddec0*((double) i);
    if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
    for(j=0;j<pnra[i];j++)
    {
      double density;
      if (p2dhisto2[j + i*npix_ra]>0 ) density = (double) p2dhisto1[j + i*npix_ra] / ((double) p2dhisto2[j + i*npix_ra]);
      else                             density = 0.0;  //to avoid contaminating with nulls
      den_all +=  (float) density;
      den2_all += (float) SQR(density);
      npix_all ++;
      area_all += parea[i];
      if ( density > max_all ) max_all = density;
      if ( density < min_all ) min_all = density;
                             
      if ( dec1 <= pSurvey->tiling_dec_max && dec1+ddec0 >= pSurvey->tiling_dec_min)
      {
        den_survey += (float) density;
        den2_survey += (float) SQR(density);
        npix_survey ++;
        area_survey += parea[i];
        if ( density > max_survey ) max_survey = density;
        if ( density < min_survey ) min_survey = density;
      }

      if ( p2dhisto2[j + i*npix_ra] > 0)
      {
        den_gt0 +=  (float) density;
        den2_gt0 += (float) SQR(density);
        npix_gt0 ++;
        area_gt0 += parea[i];
        if ( density > max_gt0 ) max_gt0 = density;
        if ( density < min_gt0 ) min_gt0 = density;
      }

      for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
      {
        if ( density > density_levels[d] )
        {
          den_gt[d] += (float) density;
          den2_gt[d] += (float) SQR(density);
          npix_gt[d] ++;
          area_gt[d] += parea[i];
          if ( density > max_gt[d] ) max_gt[d] = density;
          if ( density < min_gt[d] ) min_gt[d] = density;
        }
      }

    }
  }
  if ( npix_all    > 0 ) mean_all = (den_all / (float) npix_all);
  if ( npix_all    > 1 ) stdev_all = sqrt(den2_all*npix_all - SQR(den_all))/(float) (npix_all-1);
  if ( npix_survey > 0 ) mean_survey = (den_survey / (float) npix_survey);
  if ( npix_survey > 1 ) stdev_survey = sqrt(den2_survey*npix_survey - SQR(den_survey))/(float) (npix_survey-1);
  if ( npix_gt0    > 0 ) mean_gt0 = (den_gt0 / (float) npix_gt0);
  if ( npix_gt0    > 1 ) stdev_gt0 = sqrt(den2_gt0*npix_gt0 - SQR(den_gt0))/(float) (npix_gt0-1);

  fprintf(pFile, "\n\n#SkyDensitySummary %6s : %10s %10s %10s %10s %10s %10s %10s\n", "SUBSET", "num_pix", "area(deg2)", "mean", "stdev", "min", "max", "thresh" );
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "ALL",    npix_all, area_all, mean_all, stdev_all, min_all, max_all);
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "SURVEY", npix_survey, area_survey, mean_survey, stdev_survey, min_survey, max_survey);
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "GT0",    npix_gt0, area_gt0, mean_gt0, stdev_gt0, min_gt0, max_gt0);
  for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
  {
    if ( npix_gt[d] > 0 )
    {
      mean_gt[d] = (den_gt[d] / (float) npix_gt[d]);
      if ( npix_gt[d] > 1 ) stdev_gt[d] = sqrt(den2_gt[d]*npix_gt[d] - SQR(den_gt[d]))/(float) (npix_gt[d]-1);
      fprintf(pFile, "#SkyDensitySummary GT%-4.2f : %10d %10.3f %10.5f %10.5f %10.5f %10.5f %10.5g\n",
              density_levels[d], npix_gt[d], area_gt[d], mean_gt[d], stdev_gt[d], min_gt[d], max_gt[d], density_levels[d]);
    }
  }


  fprintf(pFile, "\n\n");
  return 0;
}



//  /////////////////////////////////////////////////////////////////
//  //can now build stats from input catalogues
//  //expect this section to increase substantially
//  //write standard named files into the simulation directory
//  //normally will not carry out this section if it is too slow
//  //build histograms over the ra,dec range given by the survey bounds
//  output file in format suitable for plotting with gnuplot pm3d
//  also output some simple stats as comments at bottom of file

int create_catalogue_density_measure_map (survey_struct *pSurvey,
                                          objectlist_struct *pObjList,
                                          catalogue_struct *pCat, 
                                          const char *str_filename,
                                          int (*test_funcA)(object_struct*,multi_type*),
                                          multi_type *pMultiA,
                                          int (*test_funcB)(object_struct*,multi_type*),
                                          multi_type *pMultiB,
                                          float (*value_func)(object_struct*))
{
  FILE   *pFile = NULL;
  int     i,j;
  float  *p2dhisto =  NULL;
  float  *parea    =  NULL;
  double *pdra     =  NULL;
  double *pinv_dra =  NULL;
  int    *pnra     =  NULL;
  int    npix_ra;
  int    npix_dec;
  const double ra_min    =   0.0;
  const double ra_max    = 360.0;
  const double dec_min   = -90.0;
  const double dec_max   = +90.0;
  double dra0;   //width at the equator
  double ddec0;
  double inv_ddec0;
  int index_of_first_object;
  int index_of_last_object;
  //  float min_density = FLT_MAX;
  //  float max_density = -1.0* FLT_MAX;
  
  if (pSurvey == NULL ||
      pObjList == NULL ) return 1;

  npix_ra = pSurvey->map_npix_ra;
  npix_dec = pSurvey->map_npix_dec;
  if ( npix_ra <= 0 || npix_dec <= 0 ) return 1;
  dra0      = (double) ( ra_max -  ra_min) / ((double) npix_ra) ;   //width at the equator
  ddec0     = (double) (dec_max - dec_min) / ((double) npix_dec) ;
  if ( ddec0 > 0.0 ) inv_ddec0 = (double) 1.0 /ddec0;
  else               return 1;

  
  //  fprintf(stdout, "%s Writing catalogue_density_map: %s\n",  COMMENT_PREFIX, str_filename);

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the catalogue_density_map file: %s\n", ERROR_PREFIX, str_filename);
    return 1;
  }

  if ((p2dhisto = (float*)  calloc((size_t) npix_ra*npix_dec, sizeof(float))) == NULL ||
      (parea    = (float*)  malloc(sizeof(float)  * npix_dec))     == NULL ||
      (pdra     = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pinv_dra = (double*) malloc(sizeof(double) * npix_dec))     == NULL ||
      (pnra     = (int*)    malloc(sizeof(int)    * npix_dec))     == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  for(i=0;i<npix_dec;i++)
  {
    double dec0  = dec_min + ddec0*((double) i + 0.5);
    double dec1  = dec0 - ddec0*0.5;
    double dec2  = dec0 + ddec0*0.5;
    double width = (ra_max - ra_min)*cosd(dec0);
    double npix  = floor(width / dra0);
    pnra[i] = (int) npix;
    pdra[i] = (ra_max - ra_min) / npix;
    parea[i] = 0.5*FULL_SKY_AREA_SQDEG * (sind(dec2) - sind(dec1)) / (double) npix; 
    pinv_dra[i] = ( pdra[i] > 0.0 ? (double)  1.0 / pdra[i] : NAN);
  }

  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }

  for (i=index_of_first_object;i<=index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    {
      if ( test_funcA )
      {
        if ( test_funcA((object_struct*) pObj, (multi_type*) pMultiA) != 0 ) continue;
      }
      if ( test_funcB )
      {
        if ( test_funcB((object_struct*) pObj, (multi_type*) pMultiB) != 0 ) continue;
      }

      {
        int pix_y = (int) ((pObj->dec - dec_min)*inv_ddec0); 
        int pix_x = (int) ((pObj->ra  - ra_min)*pinv_dra[pix_y]);
        if ( pix_y < 0 ||
             pix_y > npix_dec ||
             pix_x < 0 ||
             pix_x > pnra[pix_y] )
        {
          fprintf(stderr, "%s badpix %8d %12.7f %12.7f %6d %6d\n", ERROR_PREFIX, i, pObj->ra, pObj->dec, pix_y, pix_x);
        }
        else
        {
          float value = (float) value_func((object_struct*) pObj);
          if ( !isnan(value) ) p2dhisto[pix_x + pix_y*npix_ra] += (float) value;
        }
      }
    }
  }

  fprintf(pFile, "#%-11s %12s %8s %12s\n", "RA", "DEC", "pix_area", "pix_value");

  //now write output to file
  //write so that the density value is associated with the bottom lefthand corner of each pixel
  //write two scans for each dec row (upper and lower dec limits)
  for(i=0;i<npix_dec;i++)
  {
    double dec1  = dec_min + ddec0*((double) i);
    double ra;
    int row;
    //suppress rows completely outside dec bounds
    if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
    for(row=0;row<2;row++)
    {
      double dec = dec1 + ddec0 * (double) row;
      fprintf(pFile, "\n");
      for(j=0;j<pnra[i];j++)
      {
        ra = ra_min + ((double) j)*pdra[i];
        fprintf(pFile, "%12.5f %12.5f %8.4f %12.7g\n", ra, dec, parea[i], p2dhisto[j + i*npix_ra]);
      }
      //print the right hand corner of the last pixel
      ra = ra_min + ((double) j)*pdra[i];
      fprintf(pFile, "%12.5f %12.5f %8.4f %12.7g\n", ra, dec, parea[i], p2dhisto[(j-1) + i*npix_ra]);
    }
  }

  //  //now calculate density stats
  if ( calc_catalogue_density_measure_stats ((survey_struct*) pSurvey, pFile,
                                             (float*) p2dhisto, (float*) parea,
                                             (int*) pnra, (int) npix_ra, (int) npix_dec)) return 1;
  

  if ( pFile) fclose(pFile);  pFile = NULL;
  
  FREETONULL( pnra )    ;
  FREETONULL( pdra )    ;
  FREETONULL( pinv_dra );
  FREETONULL( parea )   ;
  FREETONULL( p2dhisto) ;
  
  return 0;
}

int calc_catalogue_density_measure_stats (survey_struct *pSurvey,
                                          FILE   *pFile,
                                          float  *p2dhisto,
                                          float  *parea,
                                          int    *pnra,
                                          int     npix_ra,
                                          int     npix_dec)
{
  const double dec_min   = -90.0;
  const double dec_max   = +90.0;
  const float density_fractions[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS] =
    {0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 0.95}; 
  const char* density_labels[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS] =
    {"10pc","20pc","30pc","40pc","50pc","60pc","70pc","80pc","90pc","95pc"}; 
  int i,j;
  int d;
  float min_density = FLT_MAX;
  float max_density = -1.0* FLT_MAX;
  float density_range;
  float density_levels[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float den_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float den2_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  int   npix_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float area_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];  
  float mean_gt [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];  
  float stdev_gt[OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float min_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  float max_gt  [OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS];
  
  float den_all     = 0.0;  //for all pixels in bounds of map
  float den_survey  = 0.0;  //for all pixels strictly within survey dec limits
  float den_gt0     = 0.0;  //for all pixels with n members > 0
  float den2_all    = 0.0;
  float den2_survey = 0.0;
  float den2_gt0    = 0.0;
  int   npix_all    = 0;
  int   npix_survey = 0;
  int   npix_gt0    = 0;
  float area_all     = 0.0;  
  float area_survey  = 0.0;  
  float area_gt0     = 0.0;  
  float mean_all     = 0.0;  
  float mean_survey  = 0.0;  
  float mean_gt0     = 0.0;  
  float stdev_all     = 0.0;  
  float stdev_survey  = 0.0;  
  float stdev_gt0     = 0.0;
  float min_all     =  FLT_MAX;  
  float min_survey  =  FLT_MAX;  
  float min_gt0     =  FLT_MAX;
  float max_all     = -1.0*FLT_MAX;  
  float max_survey  = -1.0*FLT_MAX;  
  float max_gt0     = -1.0*FLT_MAX;

  if ( pFile     == NULL ||
       p2dhisto  == NULL ||
       parea     == NULL ||
       pnra      == NULL    ) return 1;

  //it's ok for p2dhisto2 to be null;
  //work out the min-max values of the p2dhisto
  for(i=0;i<npix_dec;i++)
  {
    float inv_area = 1.0 / (parea[i] >0.0 ? parea[i] : 1e30);
    for(j=0;j<pnra[i];j++)
    {
      float density = (float) p2dhisto[j + i*npix_ra] *inv_area;
      if ( density > max_density ) max_density =  density;
      if ( density < min_density ) min_density =  density;
    }
  }
  density_range = (max_density - min_density);
  
  for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
  {
    den_gt[d]   = 0.0;
    den2_gt[d]  = 0.0;
    npix_gt[d]  =   0;
    area_gt[d]  = 0.0;  
    mean_gt[d]  = 0.0;  
    stdev_gt[d] = 0.0;
    min_gt[d]   = FLT_MAX;
    max_gt[d]   = FLT_MAX*-1.0;
    density_levels[d] = min_density+density_fractions[d]*density_range;
    //    fprintf(stdout, "Density levels: density_levels[%d] = %g\n",d,density_levels[d]);
  }

  for(i=0;i<npix_dec;i++)
  {
    double ddec0 = (double) (dec_max - dec_min) / ((double) npix_dec) ;
    double dec1  = dec_min + ddec0*((double) i);
    float inv_area = 1.0 / (parea[i] >0.0 ? parea[i] : 1e30);
    if ( dec1 > pSurvey->bounds_dec_max || dec1+ddec0 < pSurvey->bounds_dec_min) continue;
    for(j=0;j<pnra[i];j++)
    {
      float density = (float) p2dhisto[j + i*npix_ra] *inv_area;
      den_all += (float) density;
      den2_all += (float) SQR(density);
      npix_all ++;
      area_all += parea[i];
      if ( density > max_all ) max_all = density;
      if ( density < min_all ) min_all = density;
      
      if ( dec1       <= pSurvey->tiling_dec_max &&
           dec1+ddec0 >= pSurvey->tiling_dec_min)
      {
        den_survey += (float) density;
        den2_survey += (float) SQR(density);
        npix_survey ++;
        area_survey += parea[i];
        if ( density > max_survey ) max_survey = density;
        if ( density < min_survey ) min_survey = density;
      }
      
      if ( p2dhisto[j + i*npix_ra] *inv_area >  0.0)
      {
        den_gt0 += (float) density;
        den2_gt0 += (float) SQR(density);
        npix_gt0 ++;
        area_gt0 += parea[i];
        if ( density > max_gt0 ) max_gt0 = density;
        if ( density < min_gt0 ) min_gt0 = density;
      }
      
      for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
      {
        if ( density > density_levels[d] )
        {
          den_gt[d] += (float) density;
          den2_gt[d] += (float) SQR(density);
          npix_gt[d] ++;
          area_gt[d] += parea[i];
          if ( density > max_gt[d] ) max_gt[d] = density;
          if ( density < min_gt[d] ) min_gt[d] = density;
        }
      }
    }
  }
  if ( npix_all    > 0 ) mean_all = (den_all / (float) npix_all);
  if ( npix_all    > 1 ) stdev_all = sqrt(den2_all*npix_all - SQR(den_all))/(float) (npix_all-1);
  if ( npix_survey > 0 ) mean_survey = (den_survey / (float) npix_survey);
  if ( npix_survey > 1 ) stdev_survey = sqrt(den2_survey*npix_survey - SQR(den_survey))/(float) (npix_survey-1);
  if ( npix_gt0    > 0 ) mean_gt0 = (den_gt0 / (float) npix_gt0);
  if ( npix_gt0    > 1 ) stdev_gt0 = sqrt(den2_gt0*npix_gt0 - SQR(den_gt0))/(float) (npix_gt0-1);

  fprintf(pFile, "\n\n#SkyDensitySummary %6s : %10s %10s %10s %10s %10s %10s %10s\n", "SUBSET",
          "num_pix", "area", "mean", "stdev", "min", "max", "thresh" );
  fprintf(pFile, "#SkyDensitySummary %6s : %10s %10s %10s %10s %10s %10s\n", "-", "-", "(deg2)",
          "(deg^-2)", "(deg^-2)", "(deg^-2)", "(deg^-2)");
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "ALL",
          npix_all, area_all, mean_all, stdev_all, min_all, max_all);
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "SURVEY",
          npix_survey, area_survey, mean_survey, stdev_survey, min_survey, max_survey);
  fprintf(pFile, "#SkyDensitySummary %-6s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f\n", "GT0",
          npix_gt0, area_gt0, mean_gt0, stdev_gt0, min_gt0, max_gt0);

  for (d=0;d<OPSIM_STATISTICS_NUM_SKY_DENSITY_LEVELS;d++)
  {
    if ( npix_gt[d] > 0 )
    {
      mean_gt[d] = (den_gt[d] / (float) npix_gt[d]);
      if ( npix_gt[d] > 1 ) stdev_gt[d] = sqrt(den2_gt[d]*npix_gt[d] - SQR(den_gt[d]))/(float) (npix_gt[d]-1);
      fprintf(pFile, "#SkyDensitySummary GT%-4s : %10d %10.3f %10.5f %10.5f %10.5f %10.5f %10.5g\n",
              density_labels[d], npix_gt[d], area_gt[d], mean_gt[d], stdev_gt[d], min_gt[d], max_gt[d], density_levels[d]);
    }
  }
  
  fprintf(pFile, "\n\n");
  return 0;
}









//  /////////////////////////////////////////////////////////////////
//  copy of create_catalogue_density_map - but now done using a healpix pixelisation

int create_catalogue_density_map_healpix (survey_struct *pSurvey,
                                          objectlist_struct *pObjList,
                                          catalogue_struct *pCat, 
                                          const char *str_filename,
                                          int (*test_funcA)(object_struct*,multi_type*),
                                          multi_type *pMultiA,
                                          int (*test_funcB)(object_struct*,multi_type*),
                                          multi_type *pMultiB)
{
  long     i;
  float  *phisto =  NULL;
  long    npix;
  int index_of_first_object;
  int index_of_last_object;

  if (pSurvey == NULL ||
      pObjList == NULL ) return 1;

  npix = (long) nside2npix((long) pSurvey->healpix_nside); //work out how many pixels we will need

  if ( npix <= 0 )
  {
    fprintf(stdout, "%s Attempted to form a HEALPIX map with nside=%d npix=%d\n",
            WARNING_PREFIX, (int) pSurvey->healpix_nside,  (int) npix);
    return 0;  //exit quietly
  }
  
  if ((phisto = (float*) calloc((size_t) npix, sizeof(float))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }

  for (i=index_of_first_object;i<=index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    if ( (long) pObj->healpix_pix < (long) npix )
    {
      if ( test_funcA != NULL )
      {
        if ( test_funcA((object_struct*) pObj, (multi_type*) pMultiA) != 0 ) continue;
      }
      if ( test_funcB != NULL )
      {
        if ( test_funcB((object_struct*) pObj, (multi_type*) pMultiB) != 0 ) continue;
      }
      //check the healpix pixel for this object
      phisto[pObj->healpix_pix] += (float) 1.0;
    }
  }

  remove((const char*) str_filename);  //remove the file if it exists
  
  (void) write_healpix_map ((float*) phisto,
                            (long) pSurvey->healpix_nside,
                            (const char*) str_filename,
                            (char) '1',
                            (const char*) "C");
  
  FREETONULL(phisto);

  return 0;
}
///////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////
//  /////////////////////////////////////////////////////////////////
int create_catalogue_density_measure_map_healpix (survey_struct *pSurvey,
                                                  objectlist_struct *pObjList,
                                                  catalogue_struct *pCat, 
                                                  const char *str_filename,
                                                  int (*test_funcA)(object_struct*,multi_type*),
                                                  multi_type *pMultiA,
                                                  int (*test_funcB)(object_struct*,multi_type*),
                                                  multi_type *pMultiB,
                                                  float (*value_func)(object_struct*))
{
  int     i;
  float  *phisto =  NULL;
  long   npix;
  int index_of_first_object;
  int index_of_last_object;
  
  if (pSurvey == NULL ||
      pObjList == NULL ) return 1;

  npix = nside2npix((long) pSurvey->healpix_nside); //work out how many pixels we will need

  if ( npix <= 0 )
  {
    fprintf(stdout, "%s Attempted to form a HEALPIX map with nside=%d npix=%d\n",
            WARNING_PREFIX, (int) pSurvey->healpix_nside,  (int) npix);
    return 0;  //exit quietly
  }

  
  if ((phisto = (float*) calloc((size_t) npix, sizeof(float))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }

  for (i=index_of_first_object;i<=index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    if ( (long) pObj->healpix_pix < (long) npix )
    {
      float value;
      if ( test_funcA )
      {
        if ( test_funcA((object_struct*) pObj, (multi_type*) pMultiA) != 0 ) continue;
      }
      if ( test_funcB )
      {
        if ( test_funcB((object_struct*) pObj, (multi_type*) pMultiB) != 0 ) continue;
      }
      
      value = (float) value_func((object_struct*) pObj);
      if ( !isnan(value) ) phisto[pObj->healpix_pix] += (float) value;
    }
  }

  remove((const char*) str_filename);  //remove the file if it exists
  (void) write_healpix_map ((float*) phisto,
                            (long) pSurvey->healpix_nside,
                            (const char*) str_filename,
                            (char) '1',
                            (const char*) "C");
  
  FREETONULL(phisto);

  return 0;
}




//same as above but calculates ratio of cat1/cat2
int create_catalogue_density_ratio_map_healpix (survey_struct *pSurvey,
                                                objectlist_struct *pObjList,
                                                catalogue_struct *pCat, 
                                                const char *str_filename,
                                                int (*test_func1A)(object_struct*,multi_type*),
                                                multi_type *pMulti1A,
                                                int (*test_func1B)(object_struct*,multi_type*),
                                                multi_type *pMulti1B,
                                                int (*test_func2A)(object_struct*,multi_type*),
                                                multi_type *pMulti2A,
                                                int (*test_func2B)(object_struct*,multi_type*),
                                                multi_type *pMulti2B)
{
  int     i;
  int    *phisto1 =  NULL;
  int    *phisto2 =  NULL;
  float  *phisto =  NULL;
  long   npix;
  int index_of_first_object;
  int index_of_last_object;
  
  if (pSurvey == NULL ||
      pObjList == NULL ) return 1;

  npix = nside2npix((long) pSurvey->healpix_nside); //work out how many pixels we will need
  if ( npix <= 0 )
  {
    fprintf(stdout, "%s Attempted to form a HEALPIX map with nside=%d npix=%d\n",
            WARNING_PREFIX, (int) pSurvey->healpix_nside,  (int) npix);
    return 0;  //exit quietly
  }

  if ((phisto1 = (int*) calloc((size_t) npix, sizeof(int))) == NULL ||
      (phisto2 = (int*) calloc((size_t) npix, sizeof(int))) == NULL ||
      (phisto  = (float*) calloc((size_t) npix, sizeof(float))) == NULL )
  {
    fprintf(stderr, "%s Error assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }


  for (i=index_of_first_object;i<=index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    int flag1A = 0; //just in case test_func1B and test_func2B are null
    int flag2A = 0;
    int flag1B = 0; 
    int flag2B = 0;
    if ( (long) pObj->healpix_pix < (long) npix )
    {
      if ( test_func1A ) flag1A = test_func1A((object_struct*) pObj, (multi_type*) pMulti1A);
      if ( test_func2A ) flag2A = test_func2A((object_struct*) pObj, (multi_type*) pMulti2A);
      if ( test_func1B ) flag1B = test_func1B((object_struct*) pObj, (multi_type*) pMulti1B);
      if ( test_func2B ) flag2B = test_func2B((object_struct*) pObj, (multi_type*) pMulti2B);
      
      if ( flag1A == 0 && flag1B == 0 ) phisto1[pObj->healpix_pix] ++;
      if ( flag2A == 0 && flag2B == 0 ) phisto2[pObj->healpix_pix] ++;
    }
  }

  for (i=0;i<npix; i++)
  {
    if ( phisto2[i] > 0 ) phisto[i] = (float) phisto1[i] / (float) phisto2[i];
    else                  phisto[i] = (float) NAN;
  }
  
  remove((const char*) str_filename);  //remove the file if it exists
  (void) write_healpix_map ((float*) phisto,
                            (long) pSurvey->healpix_nside,
                            (const char*) str_filename,
                            (char) '1',
                            (const char*) "C");
  
  FREETONULL(phisto1);
  FREETONULL(phisto2);
  FREETONULL(phisto);

  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////
int create_catalogue_density_maps (survey_struct *pSurvey,
                                   objectlist_struct *pObjList,
                                   cataloguelist_struct *pCatList,
                                   const char *str_prefix,
                                   int (*test_func)(object_struct*,multi_type*),
                                   multi_type *pMulti )
{
  int c;
  FILE *pPlotfile = NULL;
  char str_Plotfile[STR_MAX];
  float (*value_func)(object_struct*)  = objectvalue_treq_dark;

  if (pSurvey == NULL ||
      pObjList == NULL ||
      pCatList == NULL ) return 1;

  fprintf(stdout, "%s Writing catalogue density map data: %s_*.txt\n",  COMMENT_PREFIX, str_prefix); fflush(stdout);

  //open the gnuplot plotfile
  snprintf(str_Plotfile, STR_MAX, "%s_plotfile.plot", str_prefix);
  if ((pPlotfile = (FILE*) fopen(str_Plotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the plotfile file: %s\n",
            ERROR_PREFIX, str_Plotfile);
    return 1;
  }

  if ( util_write_cd_line_to_plotfile (pPlotfile, "", ""))  return 1;

  //write the s2c(s) line to the plotfile
  {
    fprintf(pPlotfile, "s2c(s) = (s==%d?\"%s\":(s==%d?\"%s\":(s==%d?\"%s\":",
            -1,  CATALOGUE_CODENAME_ALL,
            -2,  CATALOGUE_CODENAME_ALLLORES,
            -3,  CATALOGUE_CODENAME_ALLHIRES);
    for(c=0;c<pCatList->num_cats;c++)  fprintf(pPlotfile, "(s==%d?\"%s\":", c, pCatList->pCat[c].codename);
    fprintf(pPlotfile, "\"Error\")))");
    for(c=0;c<pCatList->num_cats;c++)  fprintf(pPlotfile, ")");
    fprintf(pPlotfile, "\n");
  }

fprintf(pPlotfile, "reset\n\
\n\
cbcut = 0.99\n\
set terminal pngcairo enhanced colour font \"Times,16\" size 2000,1000\n\
\n\
set pm3d corners2color c1\n\
set view map\n\
#set pm3d interpolate 0,0\n");
  
 if ( geom_write_gnuplot_map_projection_functions ((FILE*) pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                   (int) pSurvey->map_projection, (BOOL) pSurvey->map_left_is_east,
                                                   (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 1,
                                                   (const char*) "#909090")) return 1;

 fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_SCREEN);

  fprintf(pPlotfile, "\n\
infile_boundaries = \"< gawk 'BEGIN{for(i=0;i<=360;i++){print i}; exit}'\"\n\
infile_tiling   = \"%s/%s\"\n\
set cblabel \"Target Density (deg^{-2})\" \n\
set colorbox user size 0.02,0.8 origin 0.93,0.1\n\
set origin -0.08,-0.08\n\
set size 1.12,1.12\n\
\n\
do for [survey_index=-3:%d] {\n\
  survey_codename  = s2c(survey_index)\n\
  infile  = sprintf(\"%s_%%s.txt.gz\", survey_codename)\n\
  infile_cmd  = sprintf(\"< zcat -f %%s\", infile)\n\
  str_system= sprintf(\"file %%s | grep ERROR\", infile)\n\
  isexist=system(str_system)\n\
  if(strlen(isexist) == 0 ){\n\
    str_out = sprintf(\"%s_%%s.png\"  , survey_codename)\n\
    str_thumb = sprintf(\"%s_%%s_thumb.png\"  , survey_codename)\n\
    set out str_out\n\
    print \"Plotting sky density map to: \",str_out\n\
    set label 1 survey_codename at screen 0.18,0.12 font \"Times,28\"\n\
\n\
    str_cbmax = sprintf(\"zcat -f %%s | gawk '$1!~/^#/&&NF>=4&&$4>0&&$3>0{print ($4/$3)}' | sort -g | gawk '//{n++;f[n]=$1} END {if(n<=0){print 1}else{print f[int(%%g*n)]}}'\", infile, cbcut) \n\
    cbmax = system(str_cbmax)\n\
    set cbrange [0:cbmax]\n\
\n\
\n\
    splot [][] \\\n\
          infile_cmd              using (x($1,$2)) : (y($1,$2)) : ($4/$3) with pm3d  not,\\\n\
          infile_grid_eq          using (x($3,$4)) : (y($3,$4)) : (1.0) with lines lc rgb \"#cccccc\" lt 1 lw 0.5 not,\\\n\
          infile_grid_gal         using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#888888\" lt 3 lw 0.5 not,\\\n\
          infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#888888\" lt 3 lw 1.5 not,\\\n\
          infile_tiling           using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#BBBBBB\" lt 1 lw 0.3 not,\\\n\
          infile_boundaries       using (x($1,%g)) : (y($1,%g)) : (1.0) with lines lc rgb \"#000000\" lt 3 lw 2   not,\\\n\
          infile_boundaries       using (x($1,%g)) : (y($1,%g)) : (1.0) with lines lc rgb \"#000000\" lt 3 lw 2   not\n \
\n\
    set output\n\
    str_system = sprintf(\"convert -resize x%d %%s %%s\", str_out, str_thumb)\n\
    system(str_system)\n\
  }\n\
\n\
}\n\n",
          PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_FILENAME,
          pCatList->num_cats - 1,
          str_prefix, str_prefix, str_prefix,
          pSurvey->tiling_dec_max,pSurvey->tiling_dec_max, pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
          THUMBNAIL_HEIGHT_PIXELS);

  //now the fiber-hours density map
  fprintf(pPlotfile, "\n\
infile_boundaries = \"< awk 'BEGIN{for(i=0;i<=360;i++){print i}; exit}'\"\n\
infile_tiling   = \"%s/%s\"\n\
set cblabel \"Fiber-hours Density (fiber-hours deg^{-2})\" \n\
set colorbox user size 0.02,0.8 origin 0.93,0.1\n\
set origin -0.08,-0.08\n\
set size 1.12,1.12\n\
\n\
do for [survey_index=-3:%d] {\n\
  survey_codename  = s2c(survey_index)\n\
  infile  = sprintf(\"%s_fhrs_%%s.txt.gz\", survey_codename)\n\
  infile_cmd  = sprintf(\"< zcat -f %%s\", infile)\n\
  str_system= sprintf(\"file %%s | grep ERROR\", infile)\n\
  isexist=system(str_system)\n\
  if(strlen(isexist) == 0 ){\n\
    str_out = sprintf(\"%s_fhrs_%%s.png\"  , survey_codename)\n\
    str_thumb = sprintf(\"%s_fhrs_%%s_thumb.png\"  , survey_codename)\n\
    set out str_out\n\
    print \"Plotting fiber-hours density map to: \",str_out\n\
    set label 1 survey_codename at screen 0.18,0.12 font \",28\"\n\
\n\
    str_cbmax = sprintf(\"zcat -f %%s | gawk '$1!~/^#/&&NF>=4&&$4>0&&$3>0{print ($4/$3)}' | sort -g | gawk '//{n++;f[n]=$1} END {if(n<=0){print 1}else{print f[int(%%g*n)]}}'\", infile, cbcut) \n\
    cbmax = system(str_cbmax)\n\
    set cbrange [0:cbmax]\n\
\n\
    splot [][] \\\n\
          infile_cmd              using (x($1,$2)) : (y($1,$2)) : ($4/$3) with pm3d  not,\\\n\
          infile_grid_eq          using (x($3,$4)) : (y($3,$4)) : (1.0) with lines lc rgb \"#cccccc\" lt 1 lw 0.5 not,\\\n\
          infile_grid_gal         using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#888888\" lt 3 lw 0.5 not,\\\n\
          infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#888888\" lt 3 lw 1.5 not,\\\n\
          infile_tiling           using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#BBBBBB\" lt 1 lw 0.3 not,\\\n\
          infile_boundaries       using (x($1,%g)) : (y($1,%g)) : (1.0) with lines lc rgb \"#000000\" lt 3 lw 2   not,\\\n\
          infile_boundaries       using (x($1,%g)) : (y($1,%g)) : (1.0) with lines lc rgb \"#000000\" lt 3 lw 2   not\n \
\n\
    set output\n\
    str_system = sprintf(\"convert -resize x%d %%s %%s\", str_out, str_thumb)\n\
    system(str_system)\n\
  }\n\
\n\
}\n\n",
          PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_FILENAME,
          pCatList->num_cats -1,
          str_prefix, str_prefix, str_prefix,
          pSurvey->tiling_dec_max,pSurvey->tiling_dec_max, pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
          THUMBNAIL_HEIGHT_PIXELS);
  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char str_filename[STR_MAX];
    char *codename;
    catalogue_struct *pCat;// = (catalogue_struct*) &(pCatList->pCat[c]);
    int (*test_funcB)(object_struct*,multi_type*)  = NULL;
    multi_type MultiB;

    switch ( c )
    {
    case -3 :
      if ( pCatList->num_cats > 1 )
      {
        pCat = NULL;
        codename = (char*) CATALOGUE_CODENAME_ALL;
      }
      else continue;
      break;
    case -2 :
      if ( pSurvey->total_objects_hires > 0 && pSurvey->total_objects_lores > 0 )
      {
        pCat = NULL;
        codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
        MultiB.utval = (utiny) RESOLUTION_CODE_HIGH;
        test_funcB = objectfilter_resolution_eq;
      }
      else continue;
      break;
    case -1 :
      if ( pSurvey->total_objects_hires > 0 && pSurvey->total_objects_lores > 0 )
      {
        pCat = NULL;
        codename = (char*) CATALOGUE_CODENAME_ALLLORES;
        MultiB.utval = (utiny) RESOLUTION_CODE_LOW; 
        test_funcB = objectfilter_resolution_eq;
      }
      else continue;
      break;
    default :
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      break;
    }
    
    //make the number density map
    snprintf(str_filename, STR_MAX, "%s_%s.txt", str_prefix, codename);
    if ( create_catalogue_density_map ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                                       (catalogue_struct*) pCat, (const char*) str_filename,
                                       test_func, (multi_type*) pMulti,
                                       test_funcB, (multi_type*) &MultiB))
    {
      return 1;
    }
    util_gzip_file ( (const char*) str_filename, (BOOL) (pSurvey->max_threads > 1));


    if ( pSurvey->healpix_nside > 0 )
    {
      // make the healpix version
      snprintf(str_filename, STR_MAX, "%s_%s_healpix.fits", str_prefix, codename);
      if ( create_catalogue_density_map_healpix ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                                                 (catalogue_struct*) pCat, (const char*) str_filename,
                                                 test_func, (multi_type*) pMulti,
                                                 test_funcB, (multi_type*) &MultiB))
      {
        return 1;
      }
      util_gzip_file ( (const char*) str_filename, (BOOL)  (pSurvey->max_threads > 1));
    //
    }


    
    //make the required fiber-hours map
    snprintf(str_filename, STR_MAX, "%s_fhrs_%s.txt", str_prefix, codename);
    if ( create_catalogue_density_measure_map ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                                               (catalogue_struct*) pCat, (const char*) str_filename,
                                               test_func, (multi_type*) pMulti,
                                               test_funcB, (multi_type*) &MultiB,
                                               value_func))
    {
      return 1;
    }
    util_gzip_file ( (const char*) str_filename, (BOOL)  (pSurvey->max_threads > 1));

    if ( pSurvey->healpix_nside > 0 )
    {
      // and make the healpix version
      snprintf(str_filename, STR_MAX, "%s_fhrs_%s_healpix.fits", str_prefix, codename);
      if ( create_catalogue_density_measure_map_healpix ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                                                         (catalogue_struct*) pCat, (const char*) str_filename,
                                                         test_func, (multi_type*) pMulti,
                                                         test_funcB, (multi_type*) &MultiB,
                                                         value_func))
      {
        return 1;
      }
      util_gzip_file ( (const char*) str_filename, (BOOL) (pSurvey->max_threads > 1));
    }
    //
  }

  return 0;
}




/////////////////////////////////////////////////////////
int create_catalogue_density_ratio_maps (survey_struct *pSurvey,
                                         objectlist_struct *pObjList,
                                         cataloguelist_struct *pCatList,
                                         const char *str_prefix,
                                         int (*test_func1)(object_struct*,multi_type*),
                                         multi_type *pMulti1,
                                         int (*test_func2)(object_struct*,multi_type*),
                                         multi_type *pMulti2 )
{
  int c;
  FILE *pPlotfile = NULL;
  char str_Plotfile[STR_MAX];
  if (pSurvey == NULL ||
      pObjList == NULL ||
      pCatList == NULL ) return 1;

  fprintf(stdout, "%s Writing catalogue density ratio map data: %s_*.txt\n",  COMMENT_PREFIX, str_prefix); fflush(stdout);

  //make the gnuplot plotfile
  snprintf(str_Plotfile, STR_MAX, "%s_plotfile.plot", str_prefix);
  if ((pPlotfile = (FILE*) fopen(str_Plotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s I had problems opening the plotfile file: %s\n",
            ERROR_PREFIX, str_Plotfile);
    return 1;
  }
  if ( util_write_cd_line_to_plotfile (pPlotfile, "", ""))  return 1;

  
  //write the s2c(s) line to the plotfile
  {
    fprintf(pPlotfile, "s2c(s) = (s==%d?\"%s\":(s==%d?\"%s\":(s==%d?\"%s\":",
            -1,  CATALOGUE_CODENAME_ALL,
            -2,  CATALOGUE_CODENAME_ALLLORES,
            -3,  CATALOGUE_CODENAME_ALLHIRES);
    for(c=0;c<pCatList->num_cats;c++)  fprintf(pPlotfile, "(s==%d?\"%s\":", c, pCatList->pCat[c].codename);
    fprintf(pPlotfile, "\"Error\")))");
    for(c=0;c<pCatList->num_cats;c++)  fprintf(pPlotfile, ")");
    fprintf(pPlotfile, "\n");
  }

  fprintf(pPlotfile, "reset\n\
\n\
set terminal pngcairo enhanced colour font \"Times,16\" size 2000,1000\n\
\n\
set pm3d corners2color c1\n\
set view map\n\
#set pm3d interpolate 0,0\n");

 if ( geom_write_gnuplot_map_projection_functions ((FILE*) pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                   (int) pSurvey->map_projection, (BOOL) pSurvey->map_left_is_east,
                                                   (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 1,
                                                   (const char*) "#909090")) return 1;

  fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_SCREEN);

fprintf(pPlotfile, "\n\
infile_boundaries = \"< gawk 'BEGIN{for(i=0;i<=360;i++){print i}; exit}'\"\n\
infile_tiling   = \"%s/%s\"\n\
set cblabel \"Target Density Ratio\" \n\
set colorbox user size 0.02,0.8 origin 0.93,0.1\n\
set origin -0.08,-0.08\n\
set size 1.12,1.12\n\
\n\
do for [survey_index=-3:%d] {\n\
  survey_codename  = s2c(survey_index)\n\
  infile  = sprintf(\"%s_%%s.txt.gz\", survey_codename)\n\
  infile_cmd  = sprintf(\"< zcat -f %%s\", infile)\n\
  str_system= sprintf(\"file %%s | grep ERROR\", infile)\n\
  isexist=system (str_system)\n\
  if(strlen(isexist) == 0 ){\n\
    str_out = sprintf(\"%s_%%s.png\"  , survey_codename)\n \
    str_thumb = sprintf(\"%s_%%s_thumb.png\"  , survey_codename)\n\
    set out str_out\n\
    print \"Plotting sky density ratio map to: \",str_out\n\
    set label 1 survey_codename at screen 0.04,0.11 left font \"Times,28\" noenhanced\n\
\n\
    splot [][] \\\n\
          infile_cmd        using (x($1,$2)) : (y($1,$2)) : ($6) with pm3d  not,\\\n\
          infile_grid_eq    using (x($3,$4)) : (y($3,$4)) : (1.0) with lines lc rgb \"#cccccc\" lt 1 lw 0.5 not,\\\n\
          infile_grid_gal   using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#888888\" lt 3 lw 0.5 not,\\\n\
          infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#888888\" lt 3 lw 1.5 not,\\\n\
          infile_tiling     using (x($1,$2)) : (y($1,$2)) : (1.0) with lines lc rgb \"#BBBBBB\" lt 1 lw 0.3 not,\\\n\
          infile_boundaries using (x($1,%g)) : (y($1,%g)) : (1.0) with lines lc rgb \"#000000\" lt 3 lw 2 not,\\\n\
          infile_boundaries using (x($1,%g)) : (y($1,%g)) : (1.0) with lines lc rgb \"#000000\" lt 3 lw 2 not\n\
\n\
    set output\n\
    str_system = sprintf(\"convert -resize x%d %%s %%s\", str_out, str_thumb)\n\
    system(str_system)\n\
    }\n\
\n\
}\n\n",
        PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_FILENAME,
        pCatList->num_cats -1,
        str_prefix, str_prefix, str_prefix,
        pSurvey->tiling_dec_max,pSurvey->tiling_dec_max, pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
        THUMBNAIL_HEIGHT_PIXELS);
  
  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char str_filename[STR_MAX];
    char *codename;
    catalogue_struct *pCat;
    int (*test_funcB)(object_struct*,multi_type*)  = NULL;
    multi_type MultiB;

    switch ( c )
    {
    case -3 :
      if ( pCatList->num_cats > 1 )
      {
        pCat = NULL;
        codename = (char*) CATALOGUE_CODENAME_ALL;
      }
      else continue;
      break;
    case -2 :
      if ( pSurvey->total_objects_hires > 0 && pSurvey->total_objects_lores > 0 )
      {
        pCat = NULL;
        codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
        MultiB.utval = (utiny) RESOLUTION_CODE_HIGH;
        test_funcB = objectfilter_resolution_eq;
      }
      else continue;
      break;
    case -1 :
      if ( pSurvey->total_objects_hires > 0 && pSurvey->total_objects_lores > 0 )
      {
        pCat = NULL;
        codename = (char*) CATALOGUE_CODENAME_ALLLORES;
        MultiB.utval = (utiny) RESOLUTION_CODE_LOW; 
        test_funcB = objectfilter_resolution_eq;
      }
      else continue;
      break;
    default :
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      break;
    }

    //make the ratio map data for plotting
    snprintf(str_filename, STR_MAX, "%s_%s.txt", str_prefix, codename);
    if ( create_catalogue_density_ratio_map ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                                             (catalogue_struct*) pCat, (const char*) str_filename,
                                             test_func1, (multi_type*) pMulti1,
                                             test_funcB, (multi_type*) &MultiB,
                                             test_func2, (multi_type*) pMulti2,
                                             test_funcB, (multi_type*) &MultiB))
    {
      return 1;
    }
    util_gzip_file ( (const char*) str_filename, (BOOL)  (pSurvey->max_threads > 1));


    if ( pSurvey->healpix_nside > 0 )
    {
      // make the healpix version
      snprintf(str_filename, STR_MAX, "%s_%s_healpix.fits", str_prefix, codename);
      if ( create_catalogue_density_ratio_map_healpix ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                                                       (catalogue_struct*) pCat, (const char*) str_filename,
                                                       test_func1, (multi_type*) pMulti1,
                                                       test_funcB, (multi_type*) &MultiB,
                                                       test_func2, (multi_type*) pMulti2,
                                                       test_funcB, (multi_type*) &MultiB))
      {
        return 1;
      }
      util_gzip_file ( (const char*) str_filename, (BOOL)  (pSurvey->max_threads > 1));
    }
  }

  return 0;
}





//----------------------------------------------------------------------------------//
int calc_output_catalogue_obs_comp_stats  (objectlist_struct *pObjList, catalogue_struct* pCat, fieldlist_struct *pFieldList)
{
  int i;
  int index_of_first_object;
  int index_of_last_object;
  int nobj      = 0;
  int nobj_obs  = 0;
  int nobj_comp = 0;
  int nobj_infield = 0;   //if within in a survey Field 
  int nobj_inobsfield = 0;    //if within a survey field that was actually observed
  int nobj_inobsfield_bb = 0; //
  int nobj_inobsfield_dg = 0; //

  if ( pObjList == NULL ) return 1;
  if ( pFieldList == NULL ) return 1;
  if ( pCat == NULL )
  {
    index_of_first_object = 0;
    index_of_last_object  = pObjList->nObjects - 1;
  }
  else
  {
    index_of_first_object = pCat->index_of_first_object;
    index_of_last_object =  pCat->index_of_last_object;
  }

  //print a header
  if ( pCat )
  {
    if ( pCat->index_of_first_object == 0 )
    {
      fprintf(stdout, "%s ObsCompStats #%-9s %8s %8s %7s %8s %7s %7s %8s %10s %10s %10s\n",
              COMMENT_PREFIX, "CAT", "Ntot", "Nobs", "Fobs", "Ncomp", "Fcomp", "FoM",
              "NinFld", "NinObFld", "NinObFldDG", "NinObFldBB");
    }
  }
  
  //calc the stats
  for(i=index_of_first_object;i<=index_of_last_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    if ( (unsigned int) pObj->in_survey & (unsigned int) SURVEY_ANY )
    {
      int t;
      int ntiles_dg = 0;
      int ntiles_bb = 0;
      if ( pObj->num_fields > 0 ) nobj_infield++;
      for(t=0;t<pObj->num_fields;t++)
      {
        //        field_struct *pField = (field_struct*) &(pFieldList->pF[pObj->pfield_ids[t]]);
        field_struct *pField = (field_struct*) &(pFieldList->pF[pObj->pObjField[t].field_id]);
        ntiles_bb += pField->ntiles_done[MOON_PHASE_BRIGHT];
        ntiles_dg += pField->ntiles_done[MOON_PHASE_GREY] +
                     pField->ntiles_done[MOON_PHASE_DARK];
      }
      if ( ntiles_bb > 0 ) nobj_inobsfield_bb ++;
      if ( ntiles_dg > 0 ) nobj_inobsfield_dg ++;
      if ( ntiles_dg > 0 || ntiles_bb > 0) nobj_inobsfield ++;

      
      if ( pObj->TexpFracDone > (float) 0.0)
      {
        nobj_obs ++;
        if ( pObj->TexpFracDone >= (float) 1.0)  nobj_comp ++;
      }
      nobj ++;
    }
  }
  //print out the stats 
  fprintf(stdout, "%s ObsCompStats %-10s %8d %8d %7.5f %8d %7.5f %7.5f %8d %10d %10d %10d\n",
          COMMENT_PREFIX,  (pCat?pCat->codename:"All"),
          nobj,
          nobj_obs,  (float) nobj_obs /(float) MAX(nobj,1),
          nobj_comp, (float) nobj_comp/(float) MAX(nobj,1),
          pCat->FoM,
          nobj_infield, nobj_inobsfield, nobj_inobsfield_dg, nobj_inobsfield_bb);
  return 0;
}
//----------------------------------------------------------------------------------//






////////////////////////////////////////////////////////////////////////
int calc_all_input_stats_and_maps (survey_struct *pSurvey, objectlist_struct* pObjList,
                                   focalplane_struct *pFocal, cataloguelist_struct* pCatList)
{
  clock_t clocks_start = clock();
  if ( pSurvey         == NULL ||
       pObjList     == NULL ||
       pFocal           == NULL ||
       pCatList  == NULL ) return 1;

  fprintf(stdout, "%s Calculating statistics from input catalogues, results in directory: %s/ \n",
          COMMENT_PREFIX, INPUT_STATS_SUBDIRECTORY); fflush(stdout);
  if ( util_make_directory ((const char*) INPUT_STATS_SUBDIRECTORY) )
  {
    fprintf(stderr, "%s Error making directory %s at file %s line %d\n",
            ERROR_PREFIX, INPUT_STATS_SUBDIRECTORY, __FILE__, __LINE__);
    return 1;
  }
  
  fprintf(stdout, "%s I will now calculate basic statistics and plotfiles from the input catalogues\n", COMMENT_PREFIX); fflush(stdout);

  //new way of doing things - use generic functions
  {
    const int histo_types[] = {OPSIM_STATISTICS_HISTO_TYPE_MAG,
                               OPSIM_STATISTICS_HISTO_TYPE_PRIORITY,
                               OPSIM_STATISTICS_HISTO_TYPE_RA,
                               OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT,
                               OPSIM_STATISTICS_HISTO_TYPE_TREQ,
                               OPSIM_STATISTICS_HISTO_TYPE_TREQ_D,
                               OPSIM_STATISTICS_HISTO_TYPE_TREQ_G,
                               OPSIM_STATISTICS_HISTO_TYPE_TREQ_B};
    int h;
    for (h=0;h<sizeof(histo_types)/sizeof(int);h++)
    {
      if ( calc_input_catalogue_histos  ((cataloguelist_struct*) pCatList, (survey_struct*) pSurvey,
                                        (objectlist_struct*) pObjList,
                                         (const char*) INPUT_STATS_SUBDIRECTORY, (int) histo_types[h]))
      {
        fprintf(stderr, "%s There was an error when building the %s histogram\n",
                ERROR_PREFIX, OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_types[h])); 
        return 1;
      }

      if ( write_input_histo_plotfile  ((cataloguelist_struct*) pCatList, (survey_struct*) pSurvey,
                                        (const char*) INPUT_STATS_SUBDIRECTORY, (int) histo_types[h]))
      {
        fprintf(stderr, "%s There was an error when writing the %s plotfile\n",
                ERROR_PREFIX, OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_types[h])); 
        return 1;
      }
    }
  }

  
  //make the map of all targets
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    char str_plotfile[STR_MAX];
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_true;
    snprintf(str_prefix,  STR_MAX,"%s/input_sky_density_of_targets", INPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps ((survey_struct*) pSurvey,
                                        (objectlist_struct*) pObjList,
                                        (cataloguelist_struct*) pCatList,
                                        (const char*) str_prefix,
                                        func_addr, NULL))
    {
      fprintf(stderr, "%s There was an error when creating input catalogue density maps \n", ERROR_PREFIX); 
      return 1;
    }

    //set the sky density plots running
    snprintf(str_plotfile, STR_MAX, "%s_plotfile.plot", str_prefix);
    //    if ( util_run_gnuplot_on_plotfile ( (const char*) str_plotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
    if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_plotfile)) return 1;

  }


  //calculate the map of impossible objects (ones which cannnot ever be completed)
  // semi-DEBUG - 
  if ( pSurvey->do_maps ) 
  {
    char str_plotfile[STR_MAX];
    char str_prefix[STR_MAX];
    multi_type Multi;
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_treq_impossible;
    snprintf(str_prefix, STR_MAX, "%s/input_sky_density_of_impossible_targets", INPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps  ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (cataloguelist_struct*) pCatList,
                                         (const char*) str_prefix,
                                         func_addr,
                                         (multi_type*) &Multi))
    {
        fprintf(stderr, "%s There was an error when creating input catalogue density maps \n", ERROR_PREFIX); 
    }
    //set the sky density plots running
    snprintf(str_plotfile, STR_MAX, "%s_plotfile.plot", str_prefix);
    //    if ( util_run_gnuplot_on_plotfile ( (const char*) str_plotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
    if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_plotfile)) return 1;
  }

  //and now calculate the fraction of impossible objects
  if ( pSurvey->do_maps ) 
  {
    char str_plotfile[STR_MAX];
    char str_prefix[STR_MAX];
    multi_type Multi1;
    multi_type Multi2;
    int (*func_addr1)(object_struct*,multi_type*)  = objectfilter_treq_impossible;
    int (*func_addr2)(object_struct*,multi_type*)  = objectfilter_true;
    snprintf(str_prefix, STR_MAX, "%s/input_sky_density_fraction_of_impossible_targets", INPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_ratio_maps  ((survey_struct*) pSurvey,
                                               (objectlist_struct*) pObjList,
                                               (cataloguelist_struct*) pCatList,
                                               (const char*) str_prefix,
                                               func_addr1, (multi_type*) &Multi1,
                                               func_addr2, (multi_type*) &Multi2))
    {
      fprintf(stderr, "%s There was an error when creating input catalogue density maps \n", ERROR_PREFIX); 
    }
    //set the sky density plots running
    snprintf(str_plotfile, STR_MAX, "%s_plotfile.plot", str_prefix);
    //    if ( util_run_gnuplot_on_plotfile ( (const char*) str_plotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
    if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_plotfile)) return 1;
  }
  /////////////////////////////////////////////////////////


  fprintf(stdout, "%s Finished calculating input statistics, that took %.3fs \n",
          COMMENT_PREFIX,  (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
  return 0;
}

////////////////////////////////////////////////////////////////////////
int calc_all_output_stats_and_maps (survey_struct *pSurvey, objectlist_struct* pObjList,
                                    focalplane_struct *pFocal, cataloguelist_struct* pCatList,
                                    fieldlist_struct *pFieldList)
{
  int c;
  clock_t clocks_start = clock();
  if ( pSurvey         == NULL ||
       pObjList     == NULL ||
       pFocal           == NULL ||
       pCatList  == NULL ) return 1;

  fprintf(stdout, "%s Calculating statistics from output catalogues, results in directory: %s/ \n",
          COMMENT_PREFIX, OUTPUT_STATS_SUBDIRECTORY); fflush(stdout);
  //make the directory if it does not exist yet
  if ( util_make_directory ((const char*) OUTPUT_STATS_SUBDIRECTORY) )
  {
    fprintf(stderr, "%s Error making directory %s at file %s line %d\n",
            ERROR_PREFIX, OUTPUT_STATS_SUBDIRECTORY, __FILE__, __LINE__);
    return 1;
  }


  //new way of doing things - use generic functions
  {
    const int histo_types[] = { OPSIM_STATISTICS_HISTO_TYPE_RA,
                                OPSIM_STATISTICS_HISTO_TYPE_TREQ_D,
                                OPSIM_STATISTICS_HISTO_TYPE_TREQ_G,
                                OPSIM_STATISTICS_HISTO_TYPE_TREQ_B,
                                OPSIM_STATISTICS_HISTO_TYPE_TOBS_ED,
                                OPSIM_STATISTICS_HISTO_TYPE_TEXPFRAC,
                                OPSIM_STATISTICS_HISTO_TYPE_MAG,
                                OPSIM_STATISTICS_HISTO_TYPE_PRIORITY,
                                OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT};
    int h;
    for (h=0;h<sizeof(histo_types)/sizeof(const int);h++)
    {
      if ( calc_output_catalogue_histos  ((cataloguelist_struct*) pCatList, (survey_struct*) pSurvey,
                                          (objectlist_struct*) pObjList,
                                          (const char*) OUTPUT_STATS_SUBDIRECTORY, (int) histo_types[h]))
      {
        fprintf(stderr, "%s There was an error when building the histogram\n", ERROR_PREFIX); 
        return 1;
      }

      if ( write_output_histo_plotfile  ((cataloguelist_struct*) pCatList, (survey_struct*) pSurvey,
                                         (const char*) OUTPUT_STATS_SUBDIRECTORY, (int) histo_types[h]))
      {
        fprintf(stderr, "%s There was an error when writing the plotfile\n", ERROR_PREFIX); 
        return 1;
      }
    }
  }

  
  {
    char str_FoM_filename[STR_MAX];
    snprintf(str_FoM_filename,  STR_MAX, "%s/%s", OUTPUT_STATS_SUBDIRECTORY, FOM_RESULTS_FILENAME);
    
    if ( calc_FoMs ((survey_struct*) pSurvey,(objectlist_struct*) pObjList, (cataloguelist_struct*) pCatList, (const char*) str_FoM_filename, (BOOL) TRUE))
    {
      fprintf(stderr, "%s There was a problem calculating output FoM statistics\n", ERROR_PREFIX); 
      return 1;
    }
  }

  
  for (c=0; c<pCatList->num_cats;c++)
  {
    catalogue_struct* pCat = NULL;
    if ( c < pCatList->num_cats) pCat = (catalogue_struct*) &(pCatList->pCat[c]);

    if ( calc_output_catalogue_obs_comp_stats  ((objectlist_struct*) pObjList,
                                                (catalogue_struct*) pCat,
                                                (fieldlist_struct*) pFieldList))
    {
      return 1;
    }
  }
  
  //calculate the map of observed objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi;
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_gt;
    Multi.fval = (float) 0.0; //threshold to use
    snprintf(str_prefix,  STR_MAX,"%s/output_sky_density_of_observed_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps  ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (cataloguelist_struct*) pCatList,
                                         (const char*) str_prefix,
                                         func_addr,
                                         (multi_type*) &Multi))
    {
        fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  
  //calculate the map of un-observed objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi;
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_leq;
    Multi.fval = (float) 0.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_of_unobserved_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps  ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (cataloguelist_struct*) pCatList,
                                         (const char*) str_prefix,
                                         func_addr,
                                         (multi_type*) &Multi))
    {
        fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  
  //calculate the map of fully completed objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi;
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_geq;
    Multi.fval = (float) 1.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_of_completed_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps  ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (cataloguelist_struct*) pCatList,
                                         (const char*) str_prefix,
                                         func_addr,
                                         (multi_type*) &Multi))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
    
  //calculate the map of all incomplete objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi;
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_lt;
    Multi.fval = (float) 1.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_of_incomplete_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps  ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (cataloguelist_struct*) pCatList,
                                         (const char*) str_prefix,
                                         func_addr,
                                         (multi_type*) &Multi))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  //calculate the map of objects which do not land in a field
  // semi-DEBUG - mainly used when we want to get the tiling pattern correct 
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi;
    int (*func_addr)(object_struct*,multi_type*)  = objectfilter_num_fields_eq;
    Multi.utval = (utiny) 0; //value to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_of_unfielded_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_maps  ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (cataloguelist_struct*) pCatList,
                                         (const char*) str_prefix,
                                         func_addr,
                                         (multi_type*) &Multi))
    {
        fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  /////////////////////////////////////////////////////////

  
  //calculate the fraction of observed objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi1;
    multi_type Multi2;
    int (*func_addr1)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_gt;
    int (*func_addr2)(object_struct*,multi_type*)  = objectfilter_true;
    Multi1.fval = (float) 0.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_fraction_of_observed_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_ratio_maps  ((survey_struct*) pSurvey,
                                               (objectlist_struct*) pObjList,
                                               (cataloguelist_struct*) pCatList,
                                               (const char*) str_prefix,
                                               func_addr1, (multi_type*) &Multi1,
                                               func_addr2, (multi_type*) &Multi2))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  
  //calculate the fraction of unobserved objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi1;
    multi_type Multi2;
    int (*func_addr1)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_leq;
    int (*func_addr2)(object_struct*,multi_type*)  = objectfilter_true;
    Multi1.fval = (float) 0.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_fraction_of_unobserved_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_ratio_maps  ((survey_struct*) pSurvey,
                                               (objectlist_struct*) pObjList,
                                               (cataloguelist_struct*) pCatList,
                                               (const char*) str_prefix,
                                               func_addr1, (multi_type*) &Multi1,
                                               func_addr2, (multi_type*) &Multi2))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps\n", ERROR_PREFIX); 
    }
  }
  
  //calculate the fraction of completed objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi1;
    multi_type Multi2;
    int (*func_addr1)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_geq;
    int (*func_addr2)(object_struct*,multi_type*)  = objectfilter_true;
    Multi1.fval = (float) 1.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_fraction_of_completed_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_ratio_maps  ((survey_struct*) pSurvey,
                                               (objectlist_struct*) pObjList,
                                               (cataloguelist_struct*) pCatList,
                                               (const char*) str_prefix,
                                               func_addr1, (multi_type*) &Multi1,
                                               func_addr2, (multi_type*) &Multi2))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  
  //calculate the fraction of incomplete objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi1;
    multi_type Multi2;
    int (*func_addr1)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_lt;
    int (*func_addr2)(object_struct*,multi_type*)  = objectfilter_true;
    Multi1.fval = (float) 1.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_fraction_of_incomplete_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_ratio_maps  ((survey_struct*) pSurvey,
                                               (objectlist_struct*) pObjList,
                                               (cataloguelist_struct*) pCatList,
                                               (const char*) str_prefix,
                                               func_addr1, (multi_type*) &Multi1,
                                               func_addr2, (multi_type*) &Multi2))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  
  //calculate the ratio of completed to observed objects
  if ( pSurvey->do_maps ) 
  {
    char str_prefix[STR_MAX];
    multi_type Multi1;
    multi_type Multi2;
    int (*func_addr1)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_geq;
    int (*func_addr2)(object_struct*,multi_type*)  = objectfilter_TexpFracDone_gt;
    Multi1.fval = (float) 1.0; //threshold to use
    Multi2.fval = (float) 0.0; //threshold to use
    snprintf(str_prefix, STR_MAX, "%s/output_sky_density_ratio_of_completed_to_observed_targets", OUTPUT_STATS_SUBDIRECTORY);
    if ( create_catalogue_density_ratio_maps  ((survey_struct*) pSurvey,
                                               (objectlist_struct*) pObjList,
                                               (cataloguelist_struct*) pCatList,
                                               (const char*) str_prefix,
                                               func_addr1, (multi_type*) &Multi1,
                                               func_addr2, (multi_type*) &Multi2))
    {
      fprintf(stderr, "%s There was an error when creating output catalogue density maps \n", ERROR_PREFIX); 
    }
  }
  fprintf(stdout, "%s Finished calculating output statistics, that took %.3fs \n",
          COMMENT_PREFIX,  (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);

  return 0;
}











//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Generic histogram building+writing routine
// TODO - divide by pObj->sub_code (SUB_CAT_ID) 
int calc_input_catalogue_histos (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                 objectlist_struct *pObjList,  const char* str_dirname,
                                 int histo_type)
{
  int i, j, c;

  double delta = 0.0;
  double min =   0.0;
  double max =   0.0;
  BOOL exgal_only = FALSE;
  char* str_type = NULL;
  if ( pCatList == NULL  || pSurvey == NULL || pObjList == NULL) return 1;

  
  switch (histo_type )
  {
  case OPSIM_STATISTICS_HISTO_TYPE_MAG : 
    min = 10.0;
    max = 25.0;
    delta = 0.5;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_PRIORITY :
    min = 0.0;
    max = 2.0;
    delta = 0.1;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_RA :
    min = 0.0;
    max = 360.0;
    delta = 5.0;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT :
    min = -0.09999;
    max = 10.0;
    delta = 0.1;
    exgal_only = (BOOL) TRUE;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_D :
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_G :
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_B :
    min   = OPSIM_STATISTICS_TEXP_HISTO_MIN;
    max   = pSurvey->discard_threshold_texp;
    delta = OPSIM_STATISTICS_TEXP_HISTO_DELTA;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ :
    //do this one slightly differently (within its own function)
    if ( calc_input_catalogue_histos_texp  ((cataloguelist_struct*) pCatList, (survey_struct*) pSurvey,
                                            (objectlist_struct*) pObjList,
                                            (const char*) INPUT_STATS_SUBDIRECTORY))
    {
      fprintf(stderr, "%s There was an error when building the treq histogram\n", ERROR_PREFIX); 
      return 1;
    }
    return 0;  
    break;
  default :
    fprintf(stderr, "%s  I don't know this input histogram type code: %d (%s)\n",
            ERROR_PREFIX, histo_type, OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type));
    return 1;
    break;
  }
  str_type = (char*) OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type);

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char str_filename[STR_MAX];
    FILE *pFile = NULL;
    char *codename = NULL;
    catalogue_struct *pCat = NULL;

    int    *pnobj = NULL;
    double *pfhrsd = NULL;
    double *pfhrsg = NULL;
    double *pfhrsb = NULL;
    double inv_delta;
    int nbins;
    int ntot = 0;
    int jmax;
    int jmin;
    double tot_fhrsd = 0.0;
    double tot_fhrsg = 0.0;
    double tot_fhrsb = 0.0;
    int index_of_first_object = 0;
    int index_of_last_object  = pObjList->nObjects - 1;

    utiny res_code = (utiny) RESOLUTION_CODE_NULL;

    if ( c == -3 )
    {
      codename = (char*) CATALOGUE_CODENAME_ALL;
      if ( exgal_only && pCatList->num_cats_exgal <= 0 ) continue;
    }
    else if ( c == -2 )
    {
      if ( pSurvey->total_objects_lores <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLLORES;
      res_code = RESOLUTION_CODE_LOW;
      if ( exgal_only && pCatList->num_cats_exgal_lores <= 0 ) continue;
    }
    else if ( c == -1 )
    {
      if ( pSurvey->total_objects_hires <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
      res_code = RESOLUTION_CODE_HIGH;
     if ( exgal_only && pCatList->num_cats_exgal_hires <= 0 ) continue;
    }
    else
    {
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      index_of_first_object = pCat->index_of_first_object;
      index_of_last_object =  pCat->index_of_last_object;
      if ( exgal_only && pCat->is_extragalactic == FALSE ) continue;
    }

    snprintf(str_filename, STR_MAX, "%s/input_%s_histo_%s.txt", str_dirname, str_type, codename);
    //open up the output file
    if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s I had problems opening input %s histogram for %s: %s\n",
              ERROR_PREFIX, str_type, codename, str_filename);
      return 1;
    }

    inv_delta = (double) 1.0 / delta;
    nbins = (int) ceil((max - min)*inv_delta);

    //make some space
    if ( (pnobj  = (int*)    calloc((size_t) nbins+2 , sizeof(int)))    == NULL ||
         (pfhrsd = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsg = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsb = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ) return 1;
    

    //calc the stats
    for(i=index_of_first_object;i<=index_of_last_object;i++)
    {
      object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
      if ( res_code == 0 ||
           res_code == pObj->res )
      {
        switch (histo_type )
        {
        case OPSIM_STATISTICS_HISTO_TYPE_MAG      : j = (int) floor((pObj->mag      - min)*inv_delta); break;
        case OPSIM_STATISTICS_HISTO_TYPE_PRIORITY : j = (int) floor((pObj->priority - min)*inv_delta); break;
        case OPSIM_STATISTICS_HISTO_TYPE_RA       : j = (int) floor((pObj->ra       - min)*inv_delta); break;
        case OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT : j = (int) floor((pObj->redshift - min)*inv_delta); break;
        case OPSIM_STATISTICS_HISTO_TYPE_TREQ_D   : j = (int) floor((pObj->treq[MOON_PHASE_DARK] - min)*inv_delta); break;
        case OPSIM_STATISTICS_HISTO_TYPE_TREQ_G   : j = (int) floor((pObj->treq[MOON_PHASE_GREY] - min)*inv_delta); break;
        case OPSIM_STATISTICS_HISTO_TYPE_TREQ_B   : j = (int) floor((pObj->treq[MOON_PHASE_BRIGHT] - min)*inv_delta); break;
        default: return 1; break;
        }
        if ( j >= nbins ) j = nbins+1;  //deal with cases outside nominal range
        if ( j < 0 )      j = nbins;
        pnobj[j] ++; 
        if (pObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME )
        {
          if ( pObj->treq[MOON_PHASE_DARK]   < MAX_EXPOSURE_TIME ) pfhrsd[j] += (double) (pObj->treq[MOON_PHASE_DARK]);
          if ( pObj->treq[MOON_PHASE_GREY]   < MAX_EXPOSURE_TIME ) pfhrsg[j] += (double) (pObj->treq[MOON_PHASE_GREY]);
          if ( pObj->treq[MOON_PHASE_BRIGHT] < MAX_EXPOSURE_TIME ) pfhrsb[j] += (double) (pObj->treq[MOON_PHASE_BRIGHT]);
        }
      }
    }
    
    for(j=0;j<nbins+2;j++)
    {
      if ( j<nbins) ntot += pnobj[j];
      pfhrsd[j] *= MINS_TO_HOURS;   //treq is in minutes
      pfhrsg[j] *= MINS_TO_HOURS;   //
      pfhrsb[j] *= MINS_TO_HOURS;   //
      tot_fhrsd += pfhrsd[j];
      tot_fhrsg += pfhrsg[j];
      tot_fhrsb += pfhrsb[j];
    }

    //work out the lowest populated bin
    if ( pnobj[nbins] > 0 ) jmin = 0; //print all bins if any are out of lower range
    else
    {
      for(jmin=0;jmin<nbins;jmin++)
      {
        if ( pnobj[jmin] > 0 ) break;
      }
      //increment back by one bin
      jmin=MAX(jmin-1,0);
    }

    //work out the highest populated bin
    if ( pnobj[nbins+1] > 0 ) jmax = nbins; //print all bins if any are out of upper range
    else
    {
      for(jmax=nbins-1;jmax>0;jmax--)
      {
        if ( pnobj[jmax] > 0 ) break;
      }
      //increment by two bins
      jmax=MIN(jmax+2,nbins);
    }

    
    //print out the stats
    fprintf(pFile, "#%-7s %8s %8s %10s %10s %10s %10s %10s %10s %10s %10s\n", "BIN_MIN", "BIN_MAX", "BIN_MID", "N", "FRAC",
            "FHRS_D", "FRAC_D", "FHRS_G", "FRAC_G", "FHRS_B", "FRAC_B" );

    //print objects below the low mag limit
    if ( pnobj[nbins] > 0 ) 
      fprintf(pFile, "%8.3f %8.3f %8.3f %10d %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f\n",
              NAN,  
              min,  
              min - 0.5*delta,
              pnobj[nbins], 
              (float) pnobj[nbins]/(float)MAX(ntot,1),
              pfhrsd[nbins], pfhrsd[nbins]/MAX(tot_fhrsd,1e-3),
              pfhrsg[nbins], pfhrsg[nbins]/MAX(tot_fhrsg,1e-3),
              pfhrsb[nbins], pfhrsb[nbins]/MAX(tot_fhrsb,1e-3));

    //print the normal objects
    for(j=jmin;j<jmax;j++)
    {
      fprintf(pFile, "%8.3f %8.3f %8.3f %10d %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f\n",
              min + j*delta,  
              min + (j+1.)*delta,  
              min + (j+0.5)*delta,
              pnobj[j], 
              (float) pnobj[j]/(float)MAX(ntot,1),
              pfhrsd[j], pfhrsd[j]/MAX(tot_fhrsd,1e-3),
              pfhrsg[j], pfhrsg[j]/MAX(tot_fhrsg,1e-3),
              pfhrsb[j], pfhrsb[j]/MAX(tot_fhrsb,1e-3) );
    }
    //print objects above the high mag limit
    if ( pnobj[nbins+1] > 0 ) 
      fprintf(pFile, "%8.3f %8.3f %8.3f %10d %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f\n",
              max,  
              NAN,  
              max + 0.5*delta,
              pnobj[nbins+1], 
              (float) pnobj[nbins+1]/(float)MAX(ntot,1),
              pfhrsd[nbins+1], pfhrsd[nbins+1]/MAX(tot_fhrsd,1e-3),
              pfhrsg[nbins+1], pfhrsg[nbins+1]/MAX(tot_fhrsg,1e-3),
              pfhrsb[nbins+1], pfhrsb[nbins+1]/MAX(tot_fhrsb,1e-3));

    if ( pFile ) fclose (pFile); pFile = NULL;
    FREETONULL(pnobj);
    FREETONULL(pfhrsd);
    FREETONULL(pfhrsg);
    FREETONULL(pfhrsb);
  }
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//histogram building+writing routine for Texp
int calc_input_catalogue_histos_texp (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                      objectlist_struct *pObjList,  const char* str_dirname)
{
  int i, j, c;
  double delta;
  double min;
  double max;
  char* str_type = NULL;
  if ( pCatList == NULL  || pSurvey == NULL || pObjList == NULL) return 1;

  min   = OPSIM_STATISTICS_TEXP_HISTO_MIN;
  max   = pSurvey->discard_threshold_texp;
  delta = OPSIM_STATISTICS_TEXP_HISTO_DELTA;
  str_type = (char*) OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(OPSIM_STATISTICS_HISTO_TYPE_TREQ);

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char str_filename[STR_MAX];
    FILE *pFile = NULL;
    char *codename = NULL;
    catalogue_struct *pCat = NULL;

    int    *pnobj_d = NULL;  //texp only
    int    *pnobj_g = NULL;  //texp only
    int    *pnobj_b = NULL;  //texp only
    double inv_delta;
    int nbins;
    int ntot = 0;
    int jmax;
    int jmin;
    double total_texp_d = 0.0;
    double total_texp_g = 0.0;
    double total_texp_b = 0.0;
    int index_of_first_object = 0;
    int index_of_last_object  = pObjList->nObjects - 1;

    utiny res_code = (utiny) RESOLUTION_CODE_NULL;

    if ( c == -3 )
    {
      codename = (char*) CATALOGUE_CODENAME_ALL;
    }
    else if ( c == -2 )
    {
      if ( pSurvey->total_objects_lores <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLLORES;
      res_code = RESOLUTION_CODE_LOW;
    }
    else if ( c == -1 )
    {
      if ( pSurvey->total_objects_hires <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
      res_code = RESOLUTION_CODE_HIGH;
    }
    else
    {
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      index_of_first_object = pCat->index_of_first_object;
      index_of_last_object =  pCat->index_of_last_object;
    }

    snprintf(str_filename, STR_MAX, "%s/input_%s_histo_%s.txt", str_dirname, str_type, codename);
    //open up the output file
    if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s I had problems opening input %s histogram for %s: %s\n",
              ERROR_PREFIX, str_type, codename, str_filename);
      return 1;
    }

    inv_delta = (double) 1.0 / delta;
    nbins = (int) ceil((max - min)*inv_delta);

    //make some space
    if ( (pnobj_d  = (int*) calloc((size_t) nbins+2 , sizeof(int)))    == NULL ||
         (pnobj_g  = (int*) calloc((size_t) nbins+2 , sizeof(int)))    == NULL ||
         (pnobj_b  = (int*) calloc((size_t) nbins+2 , sizeof(int)))    == NULL) return 1;
    

    //calc the stats
    for(i=index_of_first_object;i<=index_of_last_object;i++)
    {
      object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
      if ( res_code == 0 ||
           res_code == pObj->res )
      {
        int j_d = (int) floor((pObj->treq[MOON_PHASE_DARK] - min)*inv_delta);
        int j_g = (int) floor((pObj->treq[MOON_PHASE_GREY] - min)*inv_delta);
        int j_b = (int) floor((pObj->treq[MOON_PHASE_BRIGHT] - min)*inv_delta);
        if ( j_d >= nbins ) j_d = nbins+1;  //deal with cases outside nominal range
        if ( j_d < 0 )      j_d = nbins;
        if ( j_g >= nbins ) j_g = nbins+1;  //deal with cases outside nominal range
        if ( j_g < 0 )      j_g = nbins;
        if ( j_b >= nbins ) j_b = nbins+1;  //deal with cases outside nominal range
        if ( j_b < 0 )      j_b = nbins;
        pnobj_d[j_d] ++; 
        pnobj_g[j_g] ++; 
        pnobj_b[j_b] ++; 
        ntot ++;
        if ( pObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME )
        {
          if ( pObj->treq[MOON_PHASE_DARK]   < MAX_EXPOSURE_TIME ) total_texp_d += pObj->treq[MOON_PHASE_DARK];
          if ( pObj->treq[MOON_PHASE_GREY]   < MAX_EXPOSURE_TIME ) total_texp_g += pObj->treq[MOON_PHASE_GREY];
          if ( pObj->treq[MOON_PHASE_BRIGHT] < MAX_EXPOSURE_TIME ) total_texp_b += pObj->treq[MOON_PHASE_BRIGHT];
        }
        
      }
    }


    //work out the lowest populated bin
    if ( (pnobj_d[nbins]+pnobj_g[nbins]+pnobj_b[nbins]) > 0 )
      jmin = 0; //print all bins if any lie out of lower range
    else
    {
      for(jmin=0;jmin<nbins;jmin++)
      {
        if ( (pnobj_d[jmin]+pnobj_g[jmin]+pnobj_b[jmin]) > 0 ) break;
      }
      //increment back by one bin
      jmin=MAX(jmin-1,0);
    }

    //work out the highest populated bin
    if ( (pnobj_d[nbins+1]+pnobj_g[nbins+1]+pnobj_b[nbins+1]) > 0 )
      jmax = nbins; //print all bins if any lie out of upper range
    else
    {
      for(jmax=nbins-1;jmax>0;jmax--)
      {
        if ( (pnobj_d[jmax]+pnobj_g[jmax]+pnobj_b[jmax]) > 0) break;
      }
      //increment by two bins
      jmax=MIN(jmax+2,nbins);
    }

    //print out the stats - first the total requested time in each moon phase
    fprintf(pFile, "#TotalTexpStats Catalogue %-10s tot_Dark= %10.5e tot_Grey= %10.5e tot_Bright= %10.5e fiber-hours\n",
            (char*) codename,
            (double) total_texp_d * MINS_TO_HOURS,
            (double) total_texp_g * MINS_TO_HOURS,
            (double) total_texp_b * MINS_TO_HOURS); 
 
    fprintf(pFile, "#%-7s %8s %8s %10s %10s %10s %10s %10s %10s\n",
            "BIN_MIN", "BIN_MAX", "BIN_MID", "N_DARK", "FRAC_DARK", "N_GREY", "FRAC_GREY", "N_BRIGHT", "FRACBRIGHT" );
    //print objects below the low mag limit
    j = nbins;
    if ( (pnobj_d[j] + pnobj_g[j] + pnobj_b[j]) > 0 ) 
      fprintf(pFile, "%8.5g %8.5g %8.5g %10d %10.8f %10d %10.8f %10d %10.8f\n",
              NAN,
              min,
              min - 0.5*delta,
              pnobj_d[j], (float) pnobj_d[j]/(float) MAX(ntot,1),
              pnobj_g[j], (float) pnobj_g[j]/(float) MAX(ntot,1),
              pnobj_b[j], (float) pnobj_b[j]/(float) MAX(ntot,1));

    //print the normal objects
    for(j=jmin;j<jmax;j++)
    {
      fprintf(pFile, "%8.1f %8.1f %8.1f %10d %10.8f %10d %10.8f %10d %10.8f\n",
              min + j*delta,  
              min + (j+1.)*delta,  
              min + (j+0.5)*delta,
              pnobj_d[j], (float) pnobj_d[j]/(float) MAX(ntot,1),
              pnobj_g[j], (float) pnobj_g[j]/(float) MAX(ntot,1),
              pnobj_b[j], (float) pnobj_b[j]/(float) MAX(ntot,1));
    }
    //print objects above the high mag limit
    j = nbins+1;
    if ( (pnobj_d[j] + pnobj_g[j] + pnobj_b[j]) > 0 ) 
      fprintf(pFile, "%8.5g %8.5g %8.5g %10d %10.8f %10d %10.8f %10d %10.8f\n",
              max,
              NAN,
              max + 0.5*delta,
              pnobj_d[j], (float) pnobj_d[j]/(float) MAX(ntot,1),
              pnobj_g[j], (float) pnobj_g[j]/(float) MAX(ntot,1),
              pnobj_b[j], (float) pnobj_b[j]/(float) MAX(ntot,1));

    
    if ( pFile ) fclose (pFile); pFile = NULL;
    FREETONULL(pnobj_d);
    FREETONULL(pnobj_g);
    FREETONULL(pnobj_b);
  }
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//Generic histogram plotting routine
//This builds one pdf file per catalogue and then merges them at the end
//Writes two panels for each catalogue (nobjects,Nfhrs) on a single page
int write_input_histo_plotfile (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                const char* str_dirname,  int histo_type)
{
  FILE *pPlotfile = NULL;
  int c;
  char str_filename[STR_MAX];
  char *str_title_text = "";
  char *str_xlabel = "";
  char *str_xrange = "";
  char *str_xtics  = "";
  char *str_mxtics = "set mxtics 2";
  char *str_page_size = "29.7cm,21.0cm";
  BOOL exgal_only = FALSE;
  char* str_type = NULL;
  int n_panels = 2;
  
  BOOL do_All      = FALSE;
  BOOL do_AllLoRes = FALSE;
  BOOL do_AllHiRes = FALSE;
  if ( pCatList == NULL  || pSurvey == NULL ) return 1;
  switch (histo_type )
  {
  case OPSIM_STATISTICS_HISTO_TYPE_MAG : 
    str_xlabel = "Magnitude (r_{AB})";
    //    str_xrange = "set xrange [*:*]";
    //    str_xtics  = "set xtics 0,2,30";
    str_title_text = "r-band magnitude";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_PRIORITY :
    str_xlabel = "Adjusted Priority";
    str_xrange = "set xrange [0:2]";
    str_xtics  = "set xtics 0,0.5,2";
    str_title_text = "(adjusted) priority";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_RA :
    str_xlabel = "RA (deg)";
    str_xrange = "set xrange [360:0]";
    str_xtics  = "set xtics 0,30,360";
    str_title_text = "Right Ascension";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT :
    str_xlabel = "redshift";
    str_xrange = "set xrange [0:*]";
    str_xtics  = "";
    str_title_text = "redshift";
    exgal_only = (BOOL) TRUE;
    if ( pCatList->num_cats_exgal <= 0 ) return 0;  //this is not an error 
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ :
    str_xlabel = "T_{exp} requested/required (mins)";
    str_xrange = "set xrange [0:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp}";
    str_page_size = "21.0cm,29.7cm";
    n_panels = 3;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_D :
    str_xlabel = "T_{exp-dark} requested/required (mins)";
    str_xrange = "set xrange [0:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp-dark}";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_G :
    str_xlabel = "T_{exp-grey} requested/required (mins)";
    str_xrange = "set xrange [0:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp-grey}";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_B :
    str_xlabel = "T_{exp-bright} requested/required (mins)";
    str_xrange = "set xrange [0:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp-bright}";
    break;
  default :
    fprintf(stderr, "%s  I don't know this input histogram type code: %d\n", ERROR_PREFIX, histo_type);
    return 1;
    break;
  }
  str_type = (char*) OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type);


  snprintf(str_filename, STR_MAX, "%s/plot_input_%s_histo.plot", str_dirname, str_type);
  //  fprintf(stdout, "%s Making plotfile for input %s distribution: %s\n", COMMENT_PREFIX, str_type, str_filename);
  if ((pPlotfile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) str_dirname, ""))  return 1;

  fprintf(pPlotfile, "\n\
reset\n\
set terminal pdfcairo enhanced colour font \"Times,16\" size %s\n\
set ytics nomirror\n\
set mytics 2\n\
set my2tics 2\n\
set xlabel \"%s\" offset 0,0.5\n\
set label 1 \"\" at graph 0.99,1.03 right font \",16\"\n\
lwidth = 3\n", str_page_size, str_xlabel);

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char *codename = NULL;
    char *displayname = NULL;
    catalogue_struct *pCat = NULL;
    long ntotal;
    double fhrs_total_d; 
    double fhrs_total_g; 
    double fhrs_total_b; 
    double fhrs_total_here;
    char *pstr_moon = "dark";
    if ( c == -3 )
    {
      codename = (char*) "All";
      displayname = (char*) "All targets";
      if ( exgal_only ) ntotal = pSurvey->total_objects_exgal;
      else              ntotal = pSurvey->total_objects_lores + pSurvey->total_objects_hires;
      fhrs_total_d = pCatList->req_fhrs[MOON_PHASE_DARK];
      fhrs_total_g = pCatList->req_fhrs[MOON_PHASE_GREY];
      fhrs_total_b = pCatList->req_fhrs[MOON_PHASE_BRIGHT];
      do_All = TRUE;
    }
    else if ( c == -2 )
    {
      if ( pCatList->num_cats_lores <= 0 ) continue;
      if ( exgal_only && pCatList->num_cats_exgal_lores <= 0 ) continue;
      codename = (char*) "AllLoRes";
      displayname = (char*) "All low-resolution targets";
      if ( exgal_only ) ntotal = pSurvey->total_objects_exgal_lores;
      else              ntotal = pSurvey->total_objects_lores;
      fhrs_total_d = pCatList->req_fhrs_lores[MOON_PHASE_DARK];
      fhrs_total_g = pCatList->req_fhrs_lores[MOON_PHASE_GREY];
      fhrs_total_b = pCatList->req_fhrs_lores[MOON_PHASE_BRIGHT];
     do_AllLoRes = TRUE;
    }
    else if ( c == -1 )
    {
      if ( pCatList->num_cats_hires <= 0 ) continue;
      if ( exgal_only && pCatList->num_cats_exgal_hires <= 0 ) continue;
      codename = (char*) "AllHiRes";
      displayname = (char*) "All high-resolution targets";
      if ( exgal_only ) ntotal = pSurvey->total_objects_exgal_hires;
      else              ntotal = pSurvey->total_objects_hires;
      fhrs_total_d = pCatList->req_fhrs_hires[MOON_PHASE_DARK];
      fhrs_total_g = pCatList->req_fhrs_hires[MOON_PHASE_GREY];
      fhrs_total_b = pCatList->req_fhrs_hires[MOON_PHASE_BRIGHT];
      do_AllHiRes = TRUE;
    }
    else
    {
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      displayname = (char*) pCat->displayname;
      ntotal = pCat->num_objects;
      fhrs_total_d = pCat->req_fhrs[MOON_PHASE_DARK];
      fhrs_total_g = pCat->req_fhrs[MOON_PHASE_GREY];
      fhrs_total_b = pCat->req_fhrs[MOON_PHASE_BRIGHT];
      if ( ntotal <= 0 ) continue;
      if ( exgal_only == TRUE && pCat->is_extragalactic == FALSE ) continue; 
    }

    fhrs_total_here = fhrs_total_d;
    if ( histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_G )
    {
      fhrs_total_here = fhrs_total_g;
      pstr_moon = "grey";
    }
    else if ( histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_B )
    {
      fhrs_total_here = fhrs_total_b;
      pstr_moon = "bright";
    }
    fprintf(pPlotfile, "infile_%-10s = \"< zcat -f input_%s_histo_%s.txt*\"\n", codename, str_type, codename);
    fprintf(pPlotfile, "outfile_%-10s = \"input_%s_histo_%s.pdf\"\n",           codename, str_type, codename);
    fprintf(pPlotfile, "displayname_%-10s = \"%s\"\n",                          codename, displayname);
    fprintf(pPlotfile, "ntotal = %g\n\
str_label1_ntot = \"Total: %d targets\"\n\
discard_thresh = %g\n",
            (double) ntotal,
            (int) ntotal,
            (double) pSurvey->discard_threshold_texp);
    fprintf(pPlotfile, "fhrs_total_here = %g\n\
str_label1_fhrs = \"Total: %.4g Mfhr (%s)\"\n",
            fhrs_total_here,
            fhrs_total_here/1e6,
            (char*) pstr_moon);

    if ( n_panels == 3 ||
         histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_D ||
         histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_G ||
         histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_B )
      fprintf(pPlotfile, "\
set arrow 1 from first discard_thresh,graph 0.0 to first discard_thresh,graph 1.0 nohead lc 1 lt 1 lw 2 \n\
set label 3 \"Cannot be completed\" at first discard_thresh,graph 0.35 tc lt 1 rotate by 90 offset -1,0 font \",14\"\n");

  
    fprintf(pPlotfile, "set out outfile_%s\n\
%s\n\
%s\n\
%s\n\
set format y \"%%3g\"\n\
set yrange [0:*]\n\
infile = infile_%s\n", codename, str_xrange, str_xtics, str_mxtics, codename);

    fprintf(pPlotfile, "%s\n\
set multiplot title \"\\nDistribution of input targets%s w.r.t. %s\\nCatalogue: %s\" font \",18\"\n\
set key at graph 0.75,0.95 samplen 2\n\
set lmargin 4.5\n\
set rmargin 4.5\n",
            OPSIM_BRANDING_PLOTSTRING_SCREEN,
            (n_panels==2?" (and requested fiber-hours)":""),
            str_title_text, displayname);

    if ( n_panels == 2 ) {
      fprintf(pPlotfile, "\
set size 0.8,0.46\n\
origin_x1 = 0.10\n\
origin_y1 = 0.46\n\
origin_y2 = 0.02\n\n");
    }
    else if ( n_panels == 3 ) {
      fprintf(pPlotfile, "\
set label 2 \"Dark\" at graph 0.88,0.90 right font \",20\"\n\
set size 0.8,0.3\n\
origin_x1 = 0.10\n\
origin_y1 = 0.64\n\
origin_y2 = 0.33\n\
origin_y3 = 0.02\n\n");
    }
    
    //----------------------------------------------//
    fprintf(pPlotfile, "\
div_factor = 1e6\n\
f(a) = (a)/(ntotal/div_factor)\n\
g(a) = (a)*(ntotal/div_factor)\n\
set link y2 via f(y) inverse g(y) \n\
set y2tics tc lt 9 offset -0.5,0.0\n\
set ylabel \"Number of targets (x10^6)\" offset 0.5,0\n\
set y2label \"Fraction of total\" offset -2,0 tc lt 9\n\
set label 1 str_label1_ntot\n\
set origin origin_x1,origin_y1\n\
plot \\\n\
     infile using 3 : ($4/div_factor)             with histeps lt 1 lc 3      lw lwidth not,\\\n\
     infile using 3 : (($4>0?$4:1/0)/div_factor)  with points  pt 7 lc 3      ps 0.5 not\n\n");
    //----------------------------------------------//

    fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_UNSET);
    if ( n_panels == 2 )  //i.e. normal plots
    {
      fprintf(pPlotfile, "\
div_factor = 1e6\n\
fhrs_total_here = %g\n\
f(a) = (a)/(fhrs_total_here/div_factor)\n\
g(a) = (a)*(fhrs_total_here/div_factor)\n\
set link y2 via f(y) inverse g(y) \n\
set y2tics tc lt 9 offset -0.5,0.0\n\
set ylabel \"Required %s-time (Mfhr)\" offset 0.5,0\n\
set y2label \"Fraction of total fiber-hours\" offset -2,0 tc lt 9\n\n\
set label 1 str_label1_fhrs\n\
set origin origin_x1,origin_y2\n\
plot \\\n\
     infile using 3 : ($6/div_factor)             with histeps lt 1 lc 3      lw lwidth not,\\\n\
     infile using 3 : (($6>0?$6:1/0)/div_factor)  with points  pt 7 lc 3      ps 0.5 not\n\n",
              (double) fhrs_total_here,
              (char*) pstr_moon);
      //----------------------------------------------//
    }
    else if ( n_panels >= 3 )  //i.e. Texp plots
    {
      //----------------------------------------------//
      fprintf(pPlotfile, "\
set label 2 \"Grey\"\n\
set origin origin_x1,origin_y2\n\
plot \\\n\
     infile using 3 : ($6/div_factor)             with histeps lt 1 lc 3      lw lwidth not,\\\n\
     infile using 3 : (($6>0?$6:1/0)/div_factor)  with points  pt 7 lc 3      ps 0.5 not\n\n");
      //----------------------------------------------//

      //----------------------------------------------//
      fprintf(pPlotfile, "\
set label 2 \"Bright\"\n\
set origin origin_x1,origin_y3\n\
plot \\\n\
     infile using 3 : ($8/div_factor)             with histeps lt 1 lc 3      lw lwidth not,\\\n\
     infile using 3 : (($8>0?$8:1/0)/div_factor)  with points  pt 7 lc 3      ps 0.5 not\n\n");
      //----------------------------------------------//
    }
      
    
    //add a line to make a thumbnail image per catalogue
    fprintf(pPlotfile, "unset multiplot\n\
set out\n\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d 'input_%s_histo_%s.pdf[0]' input_%s_histo_%s_thumb.png\")\n\
system(str_system)\n\n",
            THUMBNAIL_HEIGHT_PIXELS, str_type, codename, str_type, codename);
  }

    //add lines to merge plotfiles and make a thumbnail image
  fprintf(pPlotfile, "str_system = \"pdfunite ");
  if ( do_All      ) fprintf(pPlotfile, "input_%s_histo_%s.pdf ", str_type, "All");
  if ( do_AllLoRes ) fprintf(pPlotfile, "input_%s_histo_%s.pdf ", str_type, "AllLoRes");
  if ( do_AllHiRes ) fprintf(pPlotfile, "input_%s_histo_%s.pdf ", str_type, "AllHiRes");
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    if ( (exgal_only == FALSE) ||
         (exgal_only == TRUE && pCat->is_extragalactic == TRUE) )
      fprintf(pPlotfile, "input_%s_histo_%s.pdf ", str_type, pCat->codename);
  }
  fprintf(pPlotfile, " input_%s_histo.pdf\"\n\
system(str_system)\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d 'input_%s_histo.pdf[0]' input_%s_histo_thumb.png\")\n\
system(str_system)\n\n",
          str_type,
          THUMBNAIL_HEIGHT_PIXELS, str_type, str_type);

  ////////////////////
  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_filename)) return 1;
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Generic output histogram building+writing routine
int calc_output_catalogue_histos (cataloguelist_struct *pCatList,
                                  survey_struct* pSurvey,
                                  objectlist_struct *pObjList,
                                  const char* str_dirname,
                                  int histo_type)
{
  int i, j, c;

  double delta = 0.0;
  double min =   0.0;
  double max =   0.0;
  BOOL exgal_only = FALSE;
  char* str_type = NULL;
  if ( pCatList == NULL  || pSurvey == NULL || pObjList == NULL) return 1;


  switch (histo_type )
  {
  case OPSIM_STATISTICS_HISTO_TYPE_MAG : 
    min = 10.0;
    max = 25.0;
    delta = 0.5;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_PRIORITY :
    min = 0.0;
    max = 2.0;
    delta = 0.1;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_RA :
    min = 0.0;
    max = 360.0;
    delta = 5.0;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT :
    min = -0.09999;
    max = 10.0;
    delta = 0.1;
    exgal_only = (BOOL) TRUE;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_D :
    min   = OPSIM_STATISTICS_TEXP_HISTO_MIN;
    max   = pSurvey->discard_threshold_texp;
    delta = OPSIM_STATISTICS_TEXP_HISTO_DELTA;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_G :
    min   = OPSIM_STATISTICS_TEXP_HISTO_MIN;
    max   = pSurvey->discard_threshold_texp;
    delta = OPSIM_STATISTICS_TEXP_HISTO_DELTA;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_B :
    min   = OPSIM_STATISTICS_TEXP_HISTO_MIN;
    max   = pSurvey->discard_threshold_texp;
    delta = OPSIM_STATISTICS_TEXP_HISTO_DELTA;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TEXPFRAC :
    min = -0.09999;
    max = 2.0;
    delta = 0.1;
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TOBS_ED :
    min   = OPSIM_STATISTICS_TEXP_HISTO_MIN; 
    max   = pSurvey->discard_threshold_texp;
    delta = OPSIM_STATISTICS_TEXP_HISTO_DELTA;
    break;
  default :
    fprintf(stderr, "%s  I don't know this input histogram type code: %d (%s)\n",
            ERROR_PREFIX, histo_type, OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type));
    return 1;
    break;
  }

  str_type = (char*) OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type);

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char str_filename[STR_MAX];
    FILE *pFile = NULL;
    char *codename = NULL;
    catalogue_struct *pCat = NULL;

    int    *pnobj        = NULL;  //num valid objects
    int    *pnobj_obs    = NULL;  //num observed objects
    int    *pnobj_comp   = NULL;  //num completed objects
    double *pfhrsd_req   = NULL;  //fiber hours requested/required
    double *pfhrsg_req   = NULL;
    double *pfhrsb_req   = NULL;
    double *pfhrsd_obs   = NULL;  //fiber hours executed - total
    double *pfhrsg_obs   = NULL;
    double *pfhrsb_obs   = NULL;
    double *pfhrsD_obs   = NULL;  //effective dark fhrs (= TexpFracDone*treq[MOON_PHASE_DARK])
    double *pfhrsd_comp  = NULL;  //fiber hours executed - only completed targets
    double *pfhrsg_comp  = NULL;
    double *pfhrsb_comp  = NULL;
    double *pfhrsD_comp  = NULL;
    double *pfhrsd_unobs = NULL;  //fiber hours requested - unobserved objects
    double *pfhrsg_unobs = NULL;
    double *pfhrsb_unobs = NULL;
    double inv_delta;
    int nbins;
    int ntot = 0;
    int ntot_obs = 0;
    int ntot_comp = 0;
    int jmax;
    int jmin;
    double tot_fhrsd_req  = 0.0; //fiber hours requested/required
    double tot_fhrsg_req  = 0.0;
    double tot_fhrsb_req  = 0.0;
    double tot_fhrsd_obs  = 0.0; //fiber hours executed - total
    double tot_fhrsg_obs  = 0.0;
    double tot_fhrsb_obs  = 0.0;
    double tot_fhrsD_obs  = 0.0;
    double tot_fhrsd_comp = 0.0; //fiber hours executed - only completed targets
    double tot_fhrsg_comp = 0.0;
    double tot_fhrsb_comp = 0.0;
    double tot_fhrsD_comp = 0.0;
    double tot_fhrsd_unobs = 0.0; //fiber hours requested - only unobserved targets
    double tot_fhrsg_unobs = 0.0;
    double tot_fhrsb_unobs = 0.0;
    int index_of_first_object = 0;
    int index_of_last_object  = pObjList->nObjects - 1;

    utiny res_code = (utiny) RESOLUTION_CODE_NULL;

    if ( c == -3 )
    {
      codename = (char*) CATALOGUE_CODENAME_ALL;
      if ( exgal_only && pCatList->num_cats_exgal <= 0 ) continue;
    }
    else if ( c == -2 )
    {
      if ( pSurvey->total_objects_lores <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLLORES;
      res_code = RESOLUTION_CODE_LOW;
      if ( exgal_only && pCatList->num_cats_exgal_lores <= 0 ) continue;
    }
    else if ( c == -1 )
    {
      if ( pSurvey->total_objects_hires <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
      res_code = RESOLUTION_CODE_HIGH;
     if ( exgal_only && pCatList->num_cats_exgal_hires <= 0 ) continue;
    }
    else
    {
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      index_of_first_object = pCat->index_of_first_object;
      index_of_last_object =  pCat->index_of_last_object;
      if ( exgal_only && pCat->is_extragalactic == FALSE ) continue;
    }

    snprintf(str_filename, STR_MAX, "%s/output_%s_histo_%s.txt", str_dirname, str_type, codename);
    //open up the output file
    if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s I had problems opening input %s histogram for %s: %s\n",
              ERROR_PREFIX, str_type, codename, str_filename);
      return 1;
    }

    inv_delta = (double) 1.0 / delta;
    nbins = (int) ceil((max - min)*inv_delta);

    //make some space
    if ( (pnobj        = (int*)    calloc((size_t) nbins+2 , sizeof(int)))    == NULL ||
         (pnobj_obs    = (int*)    calloc((size_t) nbins+2 , sizeof(int)))    == NULL ||
         (pnobj_comp   = (int*)    calloc((size_t) nbins+2 , sizeof(int)))    == NULL ||
         (pfhrsd_req   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsg_req   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsb_req   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsd_obs   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsg_obs   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsb_obs   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsD_obs   = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsd_comp  = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsg_comp  = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsb_comp  = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsD_comp  = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsd_unobs = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsg_unobs = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL ||
         (pfhrsb_unobs = (double*) calloc((size_t) nbins+2 , sizeof(double))) == NULL  ) return 1;
    

    //calc the stats
    for(i=index_of_first_object;i<=index_of_last_object;i++)
    {
      object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
      if ( (unsigned int) pObj->in_survey & (unsigned int) SURVEY_ANY )
      {
        if ( res_code == 0 ||
             res_code == pObj->res )
        {
          switch (histo_type )
          {
          case OPSIM_STATISTICS_HISTO_TYPE_MAG      : j = (int) floor((pObj->mag      - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_PRIORITY : j = (int) floor((pObj->priority - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_RA       : j = (int) floor((pObj->ra       - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT : j = (int) floor((pObj->redshift - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_TREQ_D   : j = (int) floor((pObj->treq[MOON_PHASE_DARK] - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_TREQ_G   : j = (int) floor((pObj->treq[MOON_PHASE_GREY] - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_TREQ_B   : j = (int) floor((pObj->treq[MOON_PHASE_BRIGHT] - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_TEXPFRAC : j = (int) floor((pObj->TexpFracDone - min)*inv_delta); break;
          case OPSIM_STATISTICS_HISTO_TYPE_TOBS_ED  : j = (int) floor((pObj->TexpFracDone*pObj->treq[MOON_PHASE_DARK] - min)*inv_delta); break;
          default: return 1; break;
          }
          if ( j >= nbins ) j = nbins+1;  //deal with cases outside nominal range
          if ( j < 0 )      j = nbins;
          pnobj[j] ++;
          if (pObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME )
          {
            if ( pObj->treq[MOON_PHASE_DARK]   < MAX_EXPOSURE_TIME ) pfhrsd_req[j] += (double) (pObj->treq[MOON_PHASE_DARK]);
            if ( pObj->treq[MOON_PHASE_GREY]   < MAX_EXPOSURE_TIME ) pfhrsg_req[j] += (double) (pObj->treq[MOON_PHASE_GREY]);
            if ( pObj->treq[MOON_PHASE_BRIGHT] < MAX_EXPOSURE_TIME ) pfhrsb_req[j] += (double) (pObj->treq[MOON_PHASE_BRIGHT]);
            if ( pObj->TexpFracDone <= (float) 0.0) //unobs objects 
            {
              if ( pObj->treq[MOON_PHASE_DARK]   < MAX_EXPOSURE_TIME ) pfhrsd_unobs[j] += (double) (pObj->treq[MOON_PHASE_DARK]);
              if ( pObj->treq[MOON_PHASE_GREY]   < MAX_EXPOSURE_TIME ) pfhrsg_unobs[j] += (double) (pObj->treq[MOON_PHASE_GREY]);
              if ( pObj->treq[MOON_PHASE_BRIGHT] < MAX_EXPOSURE_TIME ) pfhrsb_unobs[j] += (double) (pObj->treq[MOON_PHASE_BRIGHT]);
            }

            
          }
          if ( pObj->TexpFracDone > (float) 0.0)
          {
            int k;
            pnobj_obs[j]++;
            double texp_d = 0.0;
            double texp_g = 0.0;
            double texp_b = 0.0;
            for(k=0;k<pObj->num_fields;k++)
            {
              texp_d  += (double) pObj->pObjField[k].ptexp[MOON_PHASE_DARK];
              texp_g  += (double) pObj->pObjField[k].ptexp[MOON_PHASE_GREY];
              texp_b  += (double) pObj->pObjField[k].ptexp[MOON_PHASE_BRIGHT];
            }

            pfhrsd_obs[j] += (double) texp_d;
            pfhrsg_obs[j] += (double) texp_g;
            pfhrsb_obs[j] += (double) texp_b;
            pfhrsD_obs[j] += (double) MIN(1.0,pObj->TexpFracDone) * pObj->treq[MOON_PHASE_DARK];

            if ( pObj->TexpFracDone >= (float) 1.0)
            {
              pnobj_comp[j]++;
              pfhrsd_comp[j] += (double) texp_d;
              pfhrsg_comp[j] += (double) texp_g;
              pfhrsb_comp[j] += (double) texp_b;
              pfhrsD_comp[j] += (double) MIN(1.0,pObj->TexpFracDone) * pObj->treq[MOON_PHASE_DARK];
            }
          }
        }
      }
    }
    
    for(j=0;j<nbins+2;j++)
    {
      if ( j<nbins)
      {
        ntot      += pnobj[j];
        ntot_obs  += pnobj_obs[j];
        ntot_comp += pnobj_comp[j];
      }
      pfhrsd_req[j]   *= MINS_TO_HOURS;   //treq is in minutes
      pfhrsg_req[j]   *= MINS_TO_HOURS;   //
      pfhrsb_req[j]   *= MINS_TO_HOURS;   //
      pfhrsd_obs[j]   *= MINS_TO_HOURS;   //
      pfhrsg_obs[j]   *= MINS_TO_HOURS;   //
      pfhrsb_obs[j]   *= MINS_TO_HOURS;   //
      pfhrsD_obs[j]   *= MINS_TO_HOURS;   //
      pfhrsd_comp[j]  *= MINS_TO_HOURS;   //
      pfhrsg_comp[j]  *= MINS_TO_HOURS;   //
      pfhrsb_comp[j]  *= MINS_TO_HOURS;   //
      pfhrsD_comp[j]  *= MINS_TO_HOURS;   //
      pfhrsd_unobs[j] *= MINS_TO_HOURS;   //
      pfhrsg_unobs[j] *= MINS_TO_HOURS;   //
      pfhrsb_unobs[j] *= MINS_TO_HOURS;   //
      tot_fhrsd_req   += pfhrsd_req[j];
      tot_fhrsg_req   += pfhrsg_req[j];
      tot_fhrsb_req   += pfhrsb_req[j];
      tot_fhrsd_obs   += pfhrsd_obs[j];
      tot_fhrsg_obs   += pfhrsg_obs[j];
      tot_fhrsb_obs   += pfhrsb_obs[j];
      tot_fhrsD_obs   += pfhrsD_obs[j];
      tot_fhrsd_comp  += pfhrsd_comp[j];
      tot_fhrsg_comp  += pfhrsg_comp[j];
      tot_fhrsb_comp  += pfhrsb_comp[j];
      tot_fhrsD_comp  += pfhrsD_comp[j];
      tot_fhrsd_unobs += pfhrsd_unobs[j];
      tot_fhrsg_unobs += pfhrsg_unobs[j];
      tot_fhrsb_unobs += pfhrsb_unobs[j];
    }

    //work out the lowest populated bin
    if ( pnobj[nbins] > 0 ) jmin = 0; //print all bins if any are out of lower range
    else
    {
      for(jmin=0;jmin<nbins;jmin++)
      {
        if ( pnobj[jmin] > 0 ) break;
      }
      //increment back by one bin
      jmin=MAX(jmin-1,0);
    }

    //work out the highest populated bin
    if ( pnobj[nbins+1] > 0 ) jmax = nbins; //print all bins if any are out of upper range
    else
    {
      for(jmax=nbins-1;jmax>0;jmax--)
      {
        if ( pnobj[jmax] > 0 ) break;
      }
      //increment by two bins
      jmax=MIN(jmax+2,nbins);
    }

    //print out the stats - first summaried of total executed time in each moon phase
    fprintf(pFile, "#Histogram for output objects in bins of : %s \n", OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type));
    fprintf(pFile, "#OutputStats Ntot  Catalogue %-10s ntot= %10d ntot_obs= %10d ntot_comp= %10d\n",
            codename, (int) ntot, (int) ntot_obs, (int) ntot_comp); 
    fprintf(pFile, "#OutputStats Treq  Catalogue %-10s tot_Dark= %10.5e tot_Grey= %10.5e tot_Bright= %10.5e fhrs\n",
            codename, tot_fhrsd_req, (double) tot_fhrsg_req, (double) tot_fhrsb_req); 
    fprintf(pFile, "#OutputStats Tobs  Catalogue %-10s tot_Dark= %10.5e tot_Grey= %10.5e tot_Bright= %10.5e tot_EffDark= %10.5e fhrs\n",
            codename, tot_fhrsd_obs, (double) tot_fhrsg_obs, (double) tot_fhrsb_obs, (double) tot_fhrsD_obs); 
    fprintf(pFile, "#OutputStats Tcomp Catalogue %-10s tot_Dark= %10.5e tot_Grey= %10.5e tot_Bright= %10.5e tot_EffDark= %10.5e  fhrs\n",
            codename, (double) tot_fhrsd_comp, (double) tot_fhrsg_comp, (double) tot_fhrsb_comp, (double) tot_fhrsD_comp); 

    fprintf(pFile, "#Description of columns:\n\
#Index  Name          Format    Description\n\
#-----  ------------- --------- ------------------------------------------------------------------------ \n\
#1      BIN_MIN       float     Histogram bin lower limit  \n\
#2      BIN_MAX       float     Histogram bin upper limit  \n\
#3      BIN_MID       float     Histogram bin centre       \n\
#4      N             integer   Number of objects in this bin     \n\
#5      FRAC          float     Fraction of objects in this bin   \n\
#6      N_OBS         integer   Number of observed objects in this bin        \n\
#7      FRAC_OBS      float     Fraction of the observed objects in this bin  \n\
#8      N_COMP        integer   Number of completed objects in this bin       \n\
#9      FRAC_COMP     float     Fraction of the completed objects in this bin \n\
#10     FHRS_REQ_D    float     Sum of fiber-hours required by objects in this bin, dark time \n\
#11     fFHRS_REQ_D   float     Fraction of all fiber-hours required by objects in this bin, dark time \n\
#12     FHRS_REQ_G    float     Sum of fiber-hours required by objects in this bin, grey time \n\
#13     fFHRS_REQ_G   float     Fraction of all fiber-hours required by objects in this bin, grey time \n\
#14     FHRS_REQ_B    float     Sum of fiber-hours required by objects in this bin, bright time \n\
#15     fFHRS_REQ_B   float     Fraction of all fiber-hours required by objects in this bin, bright time \n\
#16     FHRS_OBS_D    float     Sum of fiber-hours executed on objects in this bin, dark time \n\
#17     fFHRS_OBS_D   float     Fraction of all fiber-hours executed on objects in this bin, dark time \n\
#18     FHRS_OBS_G    float     Sum of fiber-hours executed on objects in this bin, grey time \n\
#19     fFHRS_OBS_G   float     Fraction of all fiber-hours executed on objects in this bin, grey time \n\
#20     FHRS_OBS_B    float     Sum of fiber-hours executed on objects in this bin, bright time \n\
#21     fFHRS_OBS_B   float     Fraction of all fiber-hours executed on objects in this bin, bright time \n\
#22     FHRS_OBS_eD   float     Sum of fiber-hours executed on objects in this bin, dark-equivalent time \n\
#23     fFHRS_OBS_eD  float     Fraction of all fiber-hours executed on objects in this bin, dark-equivalent time \n\
#24     FHRS_COMP_D   float     Sum of fiber-hours executed on completed objects in this bin, dark time \n\
#25     fFHRS_COMP_D  float     Fraction of all fiber-hours executed on completed objects in this bin, dark time \n\
#26     FHRS_COMP_G   float     Sum of fiber-hours executed on completed objects in this bin, grey time \n\
#27     fFHRS_COMP_G  float     Fraction of all fiber-hours executed on completed objects in this bin, grey time \n\
#28     FHRS_COMP_B   float     Sum of fiber-hours executed on completed objects in this bin, bright time \n\
#29     fFHRS_COMP_B  float     Fraction of all fiber-hours executed on completed objects in this bin, bright time \n\
#30     FHRS_COMP_eD  float     Sum of fiber-hours executed on completed objects in this bin, dark-equivalent time \n\
#31     fFHRS_COMP_eD float     Fraction of all fiber-hours executed on completed objects in this bin, dark-equivalent time\n\
#32     FHRS_UNOBS_D  float     Sum of fiber-hours required by unobserved objects in this bin, dark time \n\
#33     fFHRS_UNOBS_D float     Fraction of all fiber-hours required by unobserved objects in this bin, dark time \n\
#34     FHRS_UNOBS_G  float     Sum of fiber-hours required by unobserved objects in this bin, grey time \n\
#35     fFHRS_UNOBS_G float     Fraction of all fiber-hours required by unobserved objects in this bin, grey time \n\
#36     FHRS_UNOBS_B  float     Sum of fiber-hours required by unobserved objects in this bin, bright time \n\
#37     fFHRS_UNOBS_B float     Fraction of all fiber-hours required by unobserved objects in this bin, bright time \n");

    fprintf(pFile, "#%-7s %8s %8s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
            "BIN_MIN", "BIN_MAX", "BIN_MID",
            "N",           "FRAC",         "N_OBS",       "FRAC_OBS",     "N_COMP",      "FRAC_COMP",
            "FHRS_REQ_D",  "fFHRS_REQ_D",  "FHRS_REQ_G",  "fFHRS_REQ_G",  "FHRS_REQ_B",  "fFHRS_REQ_B", 
            "FHRS_OBS_D",  "fFHRS_OBS_D",  "FHRS_OBS_G",  "fFHRS_OBS_G",  "FHRS_OBS_B",  "fFHRS_OBS_B", "FHRS_OBS_eD", "fFHRS_OBS_eD",
            "FHRS_COMP_D", "fFHRS_COMP_D", "FHRS_COMP_G", "fFHRS_COMP_G", "FHRS_COMP_B", "fFHRS_COMP_B","FHRS_COMP_eD", "fFHRS_COMP_eD",
            "FHRS_UNOBS_D","fFHRS_UNOBS_D","FHRS_UNOBS_G","fFHRS_UNOBS_G","FHRS_UNOBS_B","fFHRS_UNOBS_B");
    
    //now print out the histogram
    {
      char* str_format = "%8.5g %8.5g %8.5g %10d %10.8f %10d %10.8f %10d %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f %10.4e %10.8f\n";
   

      //print objects below the low mag limit
      if ( pnobj[nbins] > 0 ) 
        fprintf(pFile, str_format,
                NAN,
                min,
                min - 0.5*delta,
                pnobj[nbins], 
                (float) pnobj[nbins]/(float)MAX(ntot,1),
                pnobj_obs[nbins], 
                (float) pnobj_obs[nbins]/(float)MAX(ntot_obs,1),
                pnobj_comp[nbins], 
                (float) pnobj_comp[nbins]/(float)MAX(ntot_comp,1),
                pfhrsd_req[nbins], pfhrsd_req[nbins]/MAX(tot_fhrsd_req,1e-3),
                pfhrsg_req[nbins], pfhrsg_req[nbins]/MAX(tot_fhrsg_req,1e-3),
                pfhrsb_req[nbins], pfhrsb_req[nbins]/MAX(tot_fhrsb_req,1e-3),
                pfhrsd_obs[nbins], pfhrsd_obs[nbins]/MAX(tot_fhrsd_obs,1e-3),
                pfhrsg_obs[nbins], pfhrsg_obs[nbins]/MAX(tot_fhrsg_obs,1e-3),
                pfhrsb_obs[nbins], pfhrsb_obs[nbins]/MAX(tot_fhrsb_obs,1e-3),
                pfhrsD_obs[nbins], pfhrsD_obs[nbins]/MAX(tot_fhrsD_obs,1e-3),
                pfhrsd_comp[nbins], pfhrsd_comp[nbins]/MAX(tot_fhrsd_comp,1e-3),
                pfhrsg_comp[nbins], pfhrsg_comp[nbins]/MAX(tot_fhrsg_comp,1e-3),
                pfhrsb_comp[nbins], pfhrsb_comp[nbins]/MAX(tot_fhrsb_comp,1e-3),
                pfhrsD_comp[nbins], pfhrsD_comp[nbins]/MAX(tot_fhrsD_comp,1e-3),
                pfhrsd_unobs[nbins], pfhrsd_unobs[nbins]/MAX(tot_fhrsd_unobs,1e-3),
                pfhrsg_unobs[nbins], pfhrsg_unobs[nbins]/MAX(tot_fhrsg_unobs,1e-3),
                pfhrsb_unobs[nbins], pfhrsb_unobs[nbins]/MAX(tot_fhrsb_unobs,1e-3));

      //print the normal objects
      for(j=jmin;j<jmax;j++)
      {
        fprintf(pFile, str_format,
                min + j*delta,  
                min + (j+1.)*delta,  
                min + (j+0.5)*delta,
                pnobj[j], 
                (float) pnobj[j]/(float)MAX(ntot,1),
                pnobj_obs[j], 
                (float) pnobj_obs[j]/(float)MAX(ntot_obs,1),
                pnobj_comp[j], 
                (float) pnobj_comp[j]/(float)MAX(ntot_comp,1),
                pfhrsd_req[j], pfhrsd_req[j]/MAX(tot_fhrsd_req,1e-3),
                pfhrsg_req[j], pfhrsg_req[j]/MAX(tot_fhrsg_req,1e-3),
                pfhrsb_req[j], pfhrsb_req[j]/MAX(tot_fhrsb_req,1e-3),
                pfhrsd_obs[j], pfhrsd_obs[j]/MAX(tot_fhrsd_obs,1e-3),
                pfhrsg_obs[j], pfhrsg_obs[j]/MAX(tot_fhrsg_obs,1e-3),
                pfhrsb_obs[j], pfhrsb_obs[j]/MAX(tot_fhrsb_obs,1e-3),
                pfhrsD_obs[j], pfhrsD_obs[j]/MAX(tot_fhrsD_obs,1e-3),
                pfhrsd_comp[j], pfhrsd_comp[j]/MAX(tot_fhrsd_comp,1e-3),
                pfhrsg_comp[j], pfhrsg_comp[j]/MAX(tot_fhrsg_comp,1e-3),
                pfhrsb_comp[j], pfhrsb_comp[j]/MAX(tot_fhrsb_comp,1e-3),
                pfhrsD_comp[j], pfhrsD_comp[j]/MAX(tot_fhrsD_comp,1e-3),
                pfhrsd_unobs[j], pfhrsd_unobs[j]/MAX(tot_fhrsd_unobs,1e-3),
                pfhrsg_unobs[j], pfhrsg_unobs[j]/MAX(tot_fhrsg_unobs,1e-3),
                pfhrsb_unobs[j], pfhrsb_unobs[j]/MAX(tot_fhrsb_unobs,1e-3));
      
      }
      //print objects above the high mag limit
      if ( pnobj[nbins+1] > 0 ) 
        fprintf(pFile,  str_format,
                max,  
                NAN,  
                max + 0.5*delta,
                pnobj[nbins+1], 
                (float) pnobj[nbins+1]/(float)MAX(ntot,1),
                pnobj_obs[nbins+1], 
                (float) pnobj_obs[nbins+1]/(float)MAX(ntot_obs,1),
                pnobj_comp[nbins+1], 
                (float) pnobj_comp[nbins+1]/(float)MAX(ntot_comp,1),
                pfhrsd_req[nbins+1], pfhrsd_req[nbins+1]/MAX(tot_fhrsd_req,1e-3),
                pfhrsg_req[nbins+1], pfhrsg_req[nbins+1]/MAX(tot_fhrsg_req,1e-3),
                pfhrsb_req[nbins+1], pfhrsb_req[nbins+1]/MAX(tot_fhrsb_req,1e-3),
                pfhrsd_obs[nbins+1], pfhrsd_obs[nbins+1]/MAX(tot_fhrsd_obs,1e-3),
                pfhrsg_obs[nbins+1], pfhrsg_obs[nbins+1]/MAX(tot_fhrsg_obs,1e-3),
                pfhrsb_obs[nbins+1], pfhrsb_obs[nbins+1]/MAX(tot_fhrsb_obs,1e-3),
                pfhrsD_obs[nbins+1], pfhrsD_obs[nbins+1]/MAX(tot_fhrsD_obs,1e-3),
                pfhrsd_comp[nbins+1], pfhrsd_comp[nbins+1]/MAX(tot_fhrsd_comp,1e-3),
                pfhrsg_comp[nbins+1], pfhrsg_comp[nbins+1]/MAX(tot_fhrsg_comp,1e-3),
                pfhrsb_comp[nbins+1], pfhrsb_comp[nbins+1]/MAX(tot_fhrsb_comp,1e-3),
                pfhrsD_comp[nbins+1], pfhrsD_comp[nbins+1]/MAX(tot_fhrsD_comp,1e-3),
                pfhrsd_unobs[nbins+1], pfhrsd_unobs[nbins+1]/MAX(tot_fhrsd_unobs,1e-3),
                pfhrsg_unobs[nbins+1], pfhrsg_unobs[nbins+1]/MAX(tot_fhrsg_unobs,1e-3),
                pfhrsb_unobs[nbins+1], pfhrsb_unobs[nbins+1]/MAX(tot_fhrsb_unobs,1e-3));
    }

    if ( pFile ) fclose (pFile); pFile = NULL;
    FREETONULL(pnobj);
    FREETONULL(pnobj_obs);
    FREETONULL(pnobj_comp);
    FREETONULL(pfhrsd_req);
    FREETONULL(pfhrsg_req);
    FREETONULL(pfhrsb_req);
    FREETONULL(pfhrsd_obs);
    FREETONULL(pfhrsg_obs);
    FREETONULL(pfhrsb_obs);
    FREETONULL(pfhrsD_obs);
    FREETONULL(pfhrsd_comp);
    FREETONULL(pfhrsg_comp);
    FREETONULL(pfhrsb_comp);
    FREETONULL(pfhrsD_comp);
    FREETONULL(pfhrsd_unobs);
    FREETONULL(pfhrsg_unobs);
    FREETONULL(pfhrsb_unobs);
  }
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//Generic histogram plotting routine
//This builds one pdf file per catalogue and then merges them at the end
//Writes four panels for each catalogue (nobjects,frac, Nfhrs, frac) on a single page
int write_output_histo_plotfile (cataloguelist_struct *pCatList, survey_struct* pSurvey,
                                 const char* str_dirname,  int histo_type)
{
  FILE *pPlotfile = NULL;
  int c;
  char str_filename[STR_MAX];
  char *str_title_text = "";
  char *str_xlabel = "";
  char *str_xrange = "";
  char *str_xtics  = "";
  char *str_mxtics = "set mxtics 2";
  char *str_page_size = "29.7cm,21.0cm";
  char* str_set_key = "set key at graph 0.99,0.97 right samplen 2";
  BOOL exgal_only = FALSE;
  char* str_type = NULL;
  //  int n_panels = 2;
  double discard_thresh;

  BOOL do_All      = FALSE;
  BOOL do_AllLoRes = FALSE;
  BOOL do_AllHiRes = FALSE;
  if ( pCatList == NULL  || pSurvey == NULL ) return 1;
  switch (histo_type )
  {
  case OPSIM_STATISTICS_HISTO_TYPE_MAG : 
    str_xlabel = "Magnitude (r_{AB})";
    str_title_text = "r-band magnitude";
    str_set_key = "set key at graph 0.02,0.97 left samplen 2";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_PRIORITY :
    str_xlabel = "Adjusted Priority";
    str_xrange = "set xrange [0:2]";
    str_xtics  = "set xtics 0,0.5,2";
    str_title_text = "(adjusted) priority";
    str_set_key = "set key at graph 0.02,0.97 left samplen 2";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_RA :
    str_xlabel = "RA (deg)";
    str_xrange = "set xrange [360:0]";
    str_xtics  = "set xtics 0,30,360";
    str_title_text = "Right Ascension";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_REDSHIFT :
    str_xlabel = "redshift";
    str_xrange = "set xrange [*:*]";
    str_xtics  = "";
    str_title_text = "redshift";
    exgal_only = (BOOL) TRUE;
    if ( pCatList->num_cats_exgal <= 0 ) return 0;  //this is not an error 
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_D :
    str_xlabel = "T_{exp-dark} requested/required (mins)";
    str_xrange = "set xrange [*:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp-dark}";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_G :
    str_xlabel = "T_{exp-grey} requested/required (mins)";
    str_xrange = "set xrange [*:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp-grey}";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TREQ_B :
    str_xlabel = "T_{exp-bright} requested/required (mins)";
    str_xrange = "set xrange [*:(discard_thresh*1.07)]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
    str_title_text = "requested/required T_{exp-bright}";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TOBS_ED :
    str_xlabel = "Equivalent T_{exp-dark} executed (mins)";
    str_xrange = "set xrange [*:*]";
    str_xtics  = "set xtics 0,60,(discard_thresh*1.5)";
    str_title_text = "equivalent T_{exp-dark} executed";
    break;
  case OPSIM_STATISTICS_HISTO_TYPE_TEXPFRAC :
    str_xlabel = "Fraction of required exposure time executed";
    str_xrange = "set xrange [-0.1:2]";
    str_title_text = "TexpFracDone";
    break;
//  case OPSIM_STATISTICS_HISTO_TYPE_TREQ :
//    str_xlabel = "T_{exp} requested/required (mins)";
//    str_xrange = "set xrange [0:(discard_thresh*1.07)]";
//    str_xtics  = "set xtics 0,60,(discard_thresh*1.1)";
//    str_title_text = "requested/required T_{exp}";
//    str_page_size = "21.0cm,29.7cm";
//    n_panels = 3;
//    break;
  default :
    fprintf(stderr, "%s  I don't know this input histogram type code: %d (%s)\n",
            ERROR_PREFIX, histo_type, OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type));
    return 0;
    break;
  }
  str_type = (char*) OPSIM_STATISTICS_HISTO_TYPE_TO_STRING(histo_type);


  snprintf(str_filename, STR_MAX, "%s/plot_output_%s_histo.plot", str_dirname, str_type);
  if ((pPlotfile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  I had problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) str_dirname, ""))  return 1;

  fprintf(pPlotfile, "\n\
reset\n\
set terminal pdfcairo enhanced colour font \"Times,16\" size %s\n\
set ytics nomirror\n\
set mytics 2\n\
set my2tics 2\n\
set xlabel \"%s\" offset 0,0.5\n\
set label 1 \"\" at graph 0.99,1.03 right font \",16\"\n\
lc1 = \"#000000\"\n\
lc2 = \"#bb5500\"\n\
lc3 = \"#0000bb\"\n\
lc4 = \"#00bb00\"\n\
psize = 0.5\n\
ptype = 6\n\
lwidth = 3\n", str_page_size, str_xlabel);

  for(c=-3;c<pCatList->num_cats;c++)
  {
    char *codename = NULL;
    char *displayname = NULL;
    catalogue_struct *pCat = NULL;
    long ntotal;
    double fhrs_total_d; 
    if ( c == -3 )
    {
      codename = (char*) CATALOGUE_CODENAME_ALL;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALL;
      if ( exgal_only ) ntotal = pSurvey->total_objects_exgal;
      else              ntotal = pSurvey->total_objects_lores + pSurvey->total_objects_hires;
      fhrs_total_d = pCatList->req_fhrs[MOON_PHASE_DARK];
      discard_thresh = pSurvey->discard_threshold_texp*pSurvey->max_exposure_scaling;
      do_All = TRUE;
    }
    else if ( c == -2 )
    {
      if ( pCatList->num_cats_lores <= 0 ) continue;
      if ( exgal_only && pCatList->num_cats_exgal_lores <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLLORES;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALLLORES;
      if ( exgal_only ) ntotal = pSurvey->total_objects_exgal_lores;
      else              ntotal = pSurvey->total_objects_lores;
      fhrs_total_d = pCatList->req_fhrs_lores[MOON_PHASE_DARK];
      discard_thresh = pSurvey->discard_threshold_texp*pSurvey->max_exposure_scaling;
      do_AllLoRes = TRUE;
    }
    else if ( c == -1 )
    {
      if ( pCatList->num_cats_hires <= 0 ) continue;
      if ( exgal_only && pCatList->num_cats_exgal_hires <= 0 ) continue;
      codename = (char*) CATALOGUE_CODENAME_ALLHIRES;
      displayname = (char*) CATALOGUE_DISPLAYNAME_ALLHIRES;
      if ( exgal_only ) ntotal = pSurvey->total_objects_exgal_hires;
      else              ntotal = pSurvey->total_objects_hires;
      fhrs_total_d = pCatList->req_fhrs_hires[MOON_PHASE_DARK];
      discard_thresh = pSurvey->discard_threshold_texp*pSurvey->max_exposure_scaling;
      do_AllHiRes = TRUE;
    }
    else
    {
      pCat = (catalogue_struct*) &(pCatList->pCat[c]);
      codename = (char*) pCat->codename;
      displayname = (char*) pCat->displayname;
      ntotal = pCat->num_objects;
      fhrs_total_d = pCat->req_fhrs[MOON_PHASE_DARK];
      discard_thresh = pSurvey->discard_threshold_texp*pCat->exposure_scaling;
      if ( ntotal <= 0 ) continue;
      if ( exgal_only == TRUE && pCat->is_extragalactic == FALSE ) continue; 
    }
    fprintf(pPlotfile, "infile_%-10s = \"< zcat -f output_%s_histo_%s.txt*\"\n", codename, str_type, codename);
    fprintf(pPlotfile, "outfile_%-10s = \"output_%s_histo_%s.pdf\"\n",           codename, str_type, codename);
    fprintf(pPlotfile, "displayname_%-10s = \"%s\"\n",                          codename, displayname);
    fprintf(pPlotfile, "ntotal = %g\n\
discard_thresh = %g\n\
fhrs_total_d = %g\n\
str_label1_ntot = \"Total: %d targets\"\n\
str_label1_fhrs = \"Total required: %.4g Mfhr (dark)\"\n",
            (double) ntotal, (double) discard_thresh, (double) fhrs_total_d,
            (int) ntotal, fhrs_total_d/1e6);

    if ( histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_D ||
         histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_G ||
         histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ_B  )
      fprintf(pPlotfile, "\
set arrow 1 from first discard_thresh,graph 0.0 to first discard_thresh,graph 0.7 nohead lc 1 lt 1 lw 2 \n\
set label 3 \"Cannot be completed\" at first discard_thresh,graph 0.20 tc lt 1 rotate by 90 offset -1,0 font \",14\"\n");
  
    fprintf(pPlotfile, "set out outfile_%s\n\
%s\n\
%s\n\
%s\n\
set format y \"%%3g\"\n\
set yrange [0:*]\n\
min1(x) = (x<1.?1.:x)\n\
infile = infile_%s\n", codename, str_xrange, str_xtics, str_mxtics, codename);

    fprintf(pPlotfile, "%s\n\
set multiplot title \"\\nDistribution of target completeness%s w.r.t. %s\\nCatalogue: %s\" font \",18\"\n\
%s\n\
set lmargin 4.5\n\
set rmargin 4.5\n",
            OPSIM_BRANDING_PLOTSTRING_SCREEN,
            (histo_type == OPSIM_STATISTICS_HISTO_TYPE_TREQ ? "" : " (and required/executed fiber-hours)"),
            str_title_text, displayname, str_set_key);

  
      fprintf(pPlotfile, "\
set size 0.45,0.44\n\
origin_x1 = 0.05\n\
origin_x2 = 0.55\n\
origin_y1 = 0.46\n\
origin_y2 = 0.02\n\n");
    
    
    //----------------------------------------------//
    fprintf(pPlotfile, "\
div_factor = 1e6\n\
f(a) = (a)/(ntotal/div_factor)\n\
g(a) = (a)*(ntotal/div_factor)\n\
set link y2 via f(y) inverse g(y) \n\
set y2tics tc lt 9 offset -0.5,0.0\n\
set ylabel \"Number of targets (x10^6)\" offset 0.5,0\n\
set y2label \"Fraction of total\" offset -2,0 tc lt 9\n\
set label 1 str_label1_ntot\n\
set origin origin_x1,origin_y1\n\
plot \\\n\
     infile using 3 : ($4/div_factor)      with histeps lt 1 lc rgb lc1 lw lwidth t \"Total\",\\\n\
     infile using 3 : (($4-$6)/div_factor) with histeps lt 1 lc rgb lc2 lw lwidth t \"Unobserved\",\\\n\
     infile using 3 : ($6/div_factor)      with histeps lt 1 lc rgb lc3 lw lwidth t \"Observed\",\\\n\
     infile using 3 : ($8/div_factor)      with histeps lt 1 lc rgb lc4 lw lwidth t \"Completed\",\\\n\
     infile using ($1+0.2*($2-$1)) : (($4>0?$4:1/0)/div_factor)       with points  pt ptype lc rgb lc1 ps psize not,\\\n\
     infile using ($1+0.4*($2-$1)) : (($4-$6>0?$4-$6:1/0)/div_factor) with points  pt ptype lc rgb lc2 ps psize not,\\\n\
     infile using ($1+0.6*($2-$1)) : (($6>0?$6:1/0)/div_factor)       with points  pt ptype lc rgb lc3 ps psize not,\\\n\
     infile using ($1+0.8*($2-$1)) : (($8>0?$8:1/0)/div_factor)       with points  pt ptype lc rgb lc4 ps psize not\n\n");
    //----------------------------------------------//

    fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_UNSET);


    //----------------------------------------------//
    //now plot the fractional results - slightly different format
    fprintf(pPlotfile, "set ylabel \"Fraction of targets within each bin\" offset 1.0,0\n\
set bar 0\n\
set yrange [0:1.07]\n\
unset y2tics\n\
unset y2label\n\n");
    
    fprintf(pPlotfile, "set origin origin_x2,origin_y1\n\
plot \\\n\
     (1.0) with lines lt 0 not,\\\n\
     infile using 3 : (($4-$6)/min1($4))  with lines lw 1 lc rgb lc2 not,\\\n\
     infile using 3 : (($6)/min1($4))     with lines lw 1 lc rgb lc3 not,\\\n\
     infile using 3 : (($8)/min1($4))     with lines lw 1 lc rgb lc4 not,\\\n\
     infile using ($1+0.25*($2-$1)) : (($4-$6>0?$4-$6:1/0)/min1($4)) : 1 : 2 with xerrorbars lw lwidth pt ptype lc rgb lc2 ps psize not,\\\n\
     infile using ($1+0.50*($2-$1)) : (($6>0?$6:1/0)/min1($4))       : 1 : 2 with xerrorbars lw lwidth pt ptype lc rgb lc3 ps psize not,\\\n\
     infile using ($1+0.75*($2-$1)) : (($8>0?$8:1/0)/min1($4))       : 1 : 2 with xerrorbars lw lwidth pt ptype lc rgb lc4 ps psize not\n\n");

    //----------------------------------------------//

    
    if ( histo_type != OPSIM_STATISTICS_HISTO_TYPE_TREQ )  //i.e. normal plots
    {
      //----------------------------------------------//
      fprintf(pPlotfile, "\
div_factor = 1e6\n\
f(a) = (a)/(fhrs_total_d/div_factor)\n\
g(a) = (a)*(fhrs_total_d/div_factor)\n\
set link y2 via f(y) inverse g(y) \n\
set y2tics tc lt 9 offset -0.5,0.0\n\
set ylabel \"Required/executed dark-time (Mfhr)\" offset 0.5,0\n\
set y2label \"Fraction of total fiber-hours\" offset -2,0 tc lt 9\n\n\
set label 1 str_label1_fhrs\n\
set origin origin_x1,origin_y2\n\
unset yrange\n\
plot \\\n\
     infile using 3 : ($10/div_factor)                             with histeps lt 1 lc rgb lc1 lw lwidth t \"Required\",\\\n\
     infile using 3 : ($32/div_factor)                             with histeps lt 1 lc rgb lc2 lw lwidth t \"Unobserved\",\\\n\
     infile using 3 : ($22/div_factor)                             with histeps lt 1 lc rgb lc3 lw lwidth t \"Observed\",\\\n\
     infile using 3 : ($30/div_factor)                             with histeps lt 1 lc rgb lc4 lw lwidth t \"Completed\",\\\n\
     infile using ($1+0.2*($2-$1)) : (($10>0?$10:1/0)/div_factor)  with points  pt ptype lc rgb lc1 ps psize not,\\\n\
     infile using ($1+0.4*($2-$1)) : (($32>0?$32:1/0)/div_factor)  with points  pt ptype lc rgb lc2 ps psize not,\\\n\
     infile using ($1+0.6*($2-$1)) : (($22>0?$22:1/0)/div_factor)  with points  pt ptype lc rgb lc3 ps psize not,\\\n\
     infile using ($1+0.8*($2-$1)) : (($30>0?$30:1/0)/div_factor)  with points  pt ptype lc rgb lc4 ps psize not\n\n");
      //----------------------------------------------//

      //----------------------------------------------//
      //now plot the fractional results - slightly different format
      fprintf(pPlotfile, "set ylabel \"Fraction of required fiber-hours in each bin\" offset 1.0,0\n\
set bar 0\n\
set yrange [0:1.07]\n\
unset y2tics\n\
unset y2label\n\n");
    
      fprintf(pPlotfile, "set origin origin_x2,origin_y2\n\
plot \\\n\
     (1.0) with lines lt 0 not,\\\n\
     infile using 3 : (($32)/min1($10))  with lines lw 1 lc rgb lc2 not,\\\n\
     infile using 3 : (($22)/min1($10))  with lines lw 1 lc rgb lc3 not,\\\n\
     infile using 3 : (($30)/min1($10))  with lines lw 1 lc rgb lc4 not,\\\n\
     infile using ($1+0.25*($2-$1)) : (($32>0?$32:1/0)/min1($10)) : 1 : 2 with xerrorbars lw lwidth pt ptype lc rgb lc2 ps psize not,\\\n\
     infile using ($1+0.50*($2-$1)) : (($22>0?$22:1/0)/min1($10)) : 1 : 2 with xerrorbars lw lwidth pt ptype lc rgb lc3 ps psize not,\\\n\
     infile using ($1+0.75*($2-$1)) : (($30>0?$30:1/0)/min1($10)) : 1 : 2 with xerrorbars lw lwidth pt ptype lc rgb lc4 ps psize not\n\n");

    //----------------------------------------------//



    }
    
    
    //add a line to make a thumbnail image per catalogue
    fprintf(pPlotfile, "unset multiplot\n\
set out\n\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d 'output_%s_histo_%s.pdf[0]' output_%s_histo_%s_thumb.png\")\n\
system(str_system)\n\n",
            THUMBNAIL_HEIGHT_PIXELS, str_type, codename, str_type, codename);
  }

    //add lines to merge plotfiles and make a thumbnail image
  fprintf(pPlotfile, "str_system = \"pdfunite ");
  if ( do_All      ) fprintf(pPlotfile, "output_%s_histo_%s.pdf ", str_type, "All");
  if ( do_AllLoRes ) fprintf(pPlotfile, "output_%s_histo_%s.pdf ", str_type, "AllLoRes");
  if ( do_AllHiRes ) fprintf(pPlotfile, "output_%s_histo_%s.pdf ", str_type, "AllHiRes");
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*) &(pCatList->pCat[c]);
    if ( (exgal_only == FALSE) ||
         (exgal_only == TRUE && pCat->is_extragalactic == TRUE) )
      fprintf(pPlotfile, "output_%s_histo_%s.pdf ", str_type, pCat->codename);
  }
  fprintf(pPlotfile, " output_%s_histo.pdf\"\n\
system(str_system)\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d 'output_%s_histo.pdf[0]' output_%s_histo_thumb.png\")\n\
system(str_system)\n\n",
          str_type,
          THUMBNAIL_HEIGHT_PIXELS, str_type, str_type);

  ////////////////////
  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) str_filename)) return 1;
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//--------------------------------Misc-stats/plots---------------------------------------------//

//-------- Maps of fiber efficiency on sky, per res, per moon phase, per year ----------//
int write_and_plot_fiber_efficiency_maps (survey_struct *pSurvey, telescope_struct *pTele)
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];
  double hard_dec_max;
  double hard_dec_min;
  if ( pSurvey == NULL || pTele == NULL) return 1;

  snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, FIBER_EFFICIENCY_MAPS_STEM);
  if ((pPlotfile = (FILE*) fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }
  fprintf(stdout, "%s Writing fiber efficiency maps plotfile: %s\n", COMMENT_PREFIX, strPlotfile); fflush(stdout); 

  hard_dec_max = CLIP(pTele->latitude_deg + pSurvey->zenith_angle_max,-90.0,+90.0); 
  hard_dec_min = CLIP(pTele->latitude_deg - pSurvey->zenith_angle_max,-90.0,+90.0); 

  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY, ""))  return 1;

  fprintf(pPlotfile, "reset\n\
set angles degrees\n\
set size 0.98,0.9\n\
set origin 0.025,0.05\n\
set key bottom left Left reverse samplen 2\n\
\n");


  //now write out plot on standard axes, and with equal area projection.
  //this will give the areas of the sky that will receive tiles (ie based on ntiles_todo values)
  if ( geom_write_gnuplot_map_projection_functions (pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                    (int) pSurvey->map_projection, (BOOL) pSurvey->map_left_is_east,
                                                    (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 11,
                                                    (const char*) "#909090")) return 1;

  fprintf(pPlotfile, "\n\
infile_boundaries = \"< gawk 'BEGIN{for(i=0.0;i<=360.0;i+=5.0){print i}; exit}'\"\n\
\n");

  fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_GRAPH);

  fprintf(pPlotfile, "\n\
set terminal pngcairo enhanced colour font \"Times,16\" size 1200,600\n\
str_title = \"4MOST Fibre Efficiency Map\"\n\
set title str_title font \",16\" offset 0,-0.5\n\
set cblabel \"Fractional fibre usage\"\n\
set cbrange [0:1]\n\
\n\
set cbtics offset -0.5,0\n\
set label 2 \"\" at graph 0.01,0.05 font \",20\"\n\
set label 3 \"\" at graph 0.01,1.10 font \",16\"\n");

  fprintf(pPlotfile, "\n\
tiling_dec_max = %g\n\
tiling_dec_min = %g\n\
hard_dec_max = %g\n\
hard_dec_min = %g\n",
          pSurvey->tiling_dec_max,
          pSurvey->tiling_dec_min,
          hard_dec_max,
          hard_dec_min);

  fprintf(pPlotfile, "num_years = %d\n\
do for [res_index=1:2] {\n\
  if ( res_index == 1 ) { res = \"LO\"; str_res = \"Low\"; }\n\
  if ( res_index == 2 ) { res = \"HI\"; str_res = \"High\"; }\n\
  \n\
  do for [moon_index=1:2] {\n\
    if ( moon_index == 1 ) { moon = \"dg\"; str_moon = \"Dark/Grey\"; }\n\
    if ( moon_index == 2 ) { moon = \"bb\"; str_moon = \"Bright\"; }\n\
    \n\
    do for [year=0:num_years] {\n\
      set label 2 sprintf(\"Year %%d\", year)\n\
      year_start = year - 1;\n\
      year_stop  = year;\n\
      if ( year == 0 ) {year_start=0;year_stop=num_years;set label 2 sprintf(\"Years %%d-%%d\", year_start, year_stop)}\n\
      set label 3 sprintf(\"Resolution: %%s\\nMoon phase: %%s\", str_res, str_moon)\n\
      str_out = sprintf(\"fiber_efficiency_maps_res_%%s_moon_%%s_year_%%d.png\", res, moon, year)\n\
      thumb_out = sprintf(\"fiber_efficiency_maps_res_%%s_moon_%%s_year_%%d_thumb.png\", res, moon, year)\n\
      set out str_out\n\
      # print \"Output at: \",str_out\n\n\
      infile = sprintf(\"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,FIELD_NAME,FIELD_RA,FIELD_DEC,FRAC_EFF(1E)=((NFIB_ASSIGN_%%s+0.0)/(NFIB_AVAIL_%%s+0.0))][ZEN_SKYBR_CODE==\\\"%%s\\\"&&DAYOFSURVEY>%%d&&DAYOFSURVEY<=%%d]' T rownum=no colheader=no | gawk '//{f=$3;ra[f]=$4;dec[f]=$5;n[f]++;sum[f]+=$6;} END {for(f in n){print f,ra[f],dec[f],n[f],sum[f]/n[f]}}'\", res, res, moon, year_start*365, year_stop*365)\n\n\
      plot [][] \\\n\
          infile                  using (x($2,$3)) : (y($2,$3)) : ($5) with points pt 7 ps 0.9 lc palette not,\\\n\
          infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lc -1 lw 0.5 not,\\\n\
          infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 dt 3 not,\\\n\
          infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.5 dt 2 not,\\\n\
          notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
          notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
          notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
          notable_locations_gals  using (x($2,$3))    : (y($2,$3))    : 1 with labels font \",9\" not,\\\n\
          infile_boundaries       using (x($1,tiling_dec_max)) : (y($1,tiling_dec_max)) with lines lc 2 lt 3 lw 3 not,\\\n\
          infile_boundaries       using (x($1,tiling_dec_min)) : (y($1,tiling_dec_min)) with lines lc 2 lt 3 lw 3 not,\\\n\
          infile_boundaries       using (x($1,hard_dec_max))   : (y($1,hard_dec_max))   with lines lc 2 lt 1 lw 1 not,\\\n\
          infile_boundaries       using (x($1,hard_dec_min))   : (y($1,hard_dec_min))   with lines lc 2 lt 1 lw 1 not\n\
       \n\n\
     # make thumbnail images\n\
     set output\n\
     str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d %%s %%s\",str_out,thumb_out)\n\
     system(str_system)\n\
    }\n\
  }\n\
}\n\n\n",
          (int)(ceil(pSurvey->duration)),
          (char*) TILE_LOG_FILENAME_FITS,
          THUMBNAIL_HEIGHT_PIXELS);


  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  // fprintf(stdout, " done\n");  fflush(stdout);

  
  return 0;
}





int write_and_plot_fiber_efficiency (survey_struct *pSurvey)
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];
  if ( pSurvey == NULL ) return 1;

  snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, FIBER_EFFICIENCY_TRENDS_STEM);
  if ((pPlotfile = (FILE*) fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }

  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY, ""))  return 1;

  const char* str_smooth = "gawk -v dt=30 '// {n[$1]++;nfib[$1]+=$3;nass[$1]+=$4;if($1>imax){imax=$1};last_i=$1} END {for(i=0;i<=imax;i++){nbin=0;sum_n=0;sum_nass=0;sum_nfib=0;for(j=i-dt;j<i+dt;j++){if(j>=1&&j<=imax){nbin++;sum_n+=n[j];sum_nfib+=nfib[j];sum_nass+=nass[j]}};print int(i),int(n[i]),int(nfib[i]),int(nass[i]),nbin,sum_n/nbin,sum_nfib/nbin,sum_nass/nbin}}'";
  
  fprintf(pPlotfile, "reset\n\
set terminal pdfcairo enhanced colour font \"Times,16\" size 29.7cm,21.0cm \n\
set out \"%s.pdf\"\n\
%s\n\
\n\
roundto(x,dx) = (dx*(0.5+int(x/dx)))\n\
infile_dg_lo  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_LO,NFIB_ASSIGN_LO]' T mode=q rownum=no colheader=no | grep 'dg'\"\n\
infile_bb_lo  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_LO,NFIB_ASSIGN_LO]' T mode=q rownum=no colheader=no | grep 'bb'\"\n\
infile_all_lo = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_LO,NFIB_ASSIGN_LO]' T mode=q rownum=no colheader=no \"\n\
infile_dg_hi  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_HI,NFIB_ASSIGN_HI]' T mode=q rownum=no colheader=no | grep 'dg'\"\n\
infile_bb_hi  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_HI,NFIB_ASSIGN_HI]' T mode=q rownum=no colheader=no | grep 'bb'\"\n\
infile_all_hi = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_HI,NFIB_ASSIGN_HI]' T mode=q rownum=no colheader=no \"\n\
\n\
infile_dg_lo_sm  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_LO,NFIB_ASSIGN_LO]' T mode=q rownum=no colheader=no | grep 'dg' | %s \"\n\
infile_bb_lo_sm  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_LO,NFIB_ASSIGN_LO]' T mode=q rownum=no colheader=no | grep 'bb' | %s \"\n\
infile_all_lo_sm = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_LO,NFIB_ASSIGN_LO]' T mode=q rownum=no colheader=no | %s \"\n\
infile_dg_hi_sm  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_HI,NFIB_ASSIGN_HI]' T mode=q rownum=no colheader=no | grep 'dg' | %s \"\n\
infile_bb_hi_sm  = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_HI,NFIB_ASSIGN_HI]' T mode=q rownum=no colheader=no | grep 'bb' | %s \"\n\
infile_all_hi_sm = \"< ftlist '../%s[1][col DAYOFSURVEY,ZEN_SKYBR_CODE,NFIB_AVAIL_HI,NFIB_ASSIGN_HI]' T mode=q rownum=no colheader=no | %s \"\n\
\n\
stats [*:*][*:*]  infile_dg_lo  using 1 : ($4/$3) name \"dg_lo_\"  nooutput\n\
stats [*:*][*:*]  infile_bb_lo  using 1 : ($4/$3) name \"bb_lo_\"  nooutput\n\
stats [*:*][*:*]  infile_all_lo using 1 : ($4/$3) name \"all_lo_\" nooutput\n\
stats [*:*][*:*]  infile_dg_hi  using 1 : ($4/$3) name \"dg_hi_\"  nooutput\n\
stats [*:*][*:*]  infile_bb_hi  using 1 : ($4/$3) name \"bb_hi_\"  nooutput\n\
stats [*:*][*:*]  infile_all_hi using 1 : ($4/$3) name \"all_hi_\" nooutput\n\
\n",
          FIBER_EFFICIENCY_TRENDS_STEM,
          OPSIM_BRANDING_PLOTSTRING_SCREEN,
          TILE_LOG_FILENAME_FITS,
          TILE_LOG_FILENAME_FITS,
          TILE_LOG_FILENAME_FITS,
          TILE_LOG_FILENAME_FITS,
          TILE_LOG_FILENAME_FITS,
          TILE_LOG_FILENAME_FITS, 
          TILE_LOG_FILENAME_FITS, str_smooth,
          TILE_LOG_FILENAME_FITS, str_smooth,
          TILE_LOG_FILENAME_FITS, str_smooth,
          TILE_LOG_FILENAME_FITS, str_smooth,
          TILE_LOG_FILENAME_FITS, str_smooth,
          TILE_LOG_FILENAME_FITS, str_smooth );
  
  fprintf(pPlotfile, "set ylabel \"Fractional fiber utilisation\" offset 1,0\n\
\n\
set key bottom right samplen 0.5\n\
set xtics 0,200,3000 nomirror\n\
set mxtics 2\n\
set x2tics 0,365.25,5000\n\
set mx2tics 12\n\
set format x2 \"\"\n\
set x2tics scale 2.0,0.5\n\
set yrange [0.0:1.04]\n\
set xrange [0:*]\n\
\n\
set multiplot title \"Trends in fractional fiber utilisation w.r.t. survey progress\\nFractional fiber utilisation = N_{fibers-on-targets}/(N_{total-fibers} - N_{sky-fibers} - N_{PI-fibers})\" font \",20\"\n\
set size 0.9,0.46\n\
set origin 0.05,0.49\n\
set label 1 \"Low Res Fibers\" at graph 0.99,1.04 right font \",24\" front\n\
set label 2 \"Dark/Grey Time\" at graph 0.01,0.19 left font \",20\" tc lt 1 front\n\
set label 3 \"Bright Time\" at graph 0.01,0.12 left font \",20\" tc lt 3 front\n\
set label 6 \"Any Time\" at graph 0.01,0.05 left font \",20\" tc lt -1 front\n\
set arrow 1 from graph 0.01,0.27 to graph 0.04,0.27 nohead dt 1 lc 1 lw 3\n\
set arrow 2 from graph 0.01,0.32 to graph 0.04,0.32 nohead dt 4 lc 1 lw 3\n\
set arrow 3 from graph 0.05,0.27 to graph 0.08,0.27 nohead dt 1 lc 3 lw 3\n\
set arrow 4 from graph 0.05,0.32 to graph 0.08,0.32 nohead dt 4 lc 3 lw 3\n\
set arrow 5 from graph 0.09,0.27 to graph 0.12,0.27 nohead dt 1 lc -1 lw 3\n\
set arrow 6 from graph 0.09,0.32 to graph 0.12,0.32 nohead dt 4 lc -1 lw 3\n\
set label 4 \"Running Mean\" at graph 0.13,0.27 left font \",16\" tc lt -1 front\n\
set label 5 \"Overall Mean\" at graph 0.13,0.32 left font \",16\" tc lt -1 front\n\
\n");


  fprintf(pPlotfile, "set x2label \"Year of survey\" \n\
plot [][]\\\n\
     infile_dg_lo  using 1 : ($4/$3) with dots lc 1 not,\\\n\
     infile_bb_lo  using 1 : ($4/$3) with dots lc 3 not,\\\n\
     infile_dg_lo_sm  using ($1) : ($8/$7) with lines lc 1  dt 1 lw 3 not,\\\n\
     infile_bb_lo_sm  using ($1) : ($8/$7) with lines lc 3  dt 1 lw 3 not,\\\n\
     infile_all_lo_sm using ($1) : ($8/$7) with lines lc -1 dt 1 lw 3 not,\\\n\
     (all_lo_mean_y) with lines dt 4 lc -1 lw 3 not,\\\n\
     (dg_lo_mean_y)  with lines dt 4 lc 1  lw 3 not,\\\n\
     (bb_lo_mean_y)  with lines dt 4 lc 3  lw 3 not\n\
\n");
  
  fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_UNSET);
  fprintf(pPlotfile, "unset label 2\n\
unset label 3\n\
unset arrow 1\n\
unset arrow 2\n\
unset arrow 3\n\
unset arrow 4\n\
unset arrow 5\n\
unset arrow 6\n\
unset label 4\n\
unset label 5\n\
unset label 6\n\
unset x2label\n\
\n");
  
  fprintf(pPlotfile, "\n\
set label 1 \"High Res Fibers\" \n\
set format x \"%%g\"\n\
set xlabel \"Day of survey\" offset 0,0.5\n\
set origin 0.05,0.02\n\
plot [][]\\\n\
     infile_dg_hi  using 1 : ($4/$3) with dots lc 1 not,\\\n\
     infile_bb_hi  using 1 : ($4/$3) with dots lc 3 not,\\\n\
     infile_dg_hi_sm  using ($1) : ($8/$7) with lines lc 1  dt 1 lw 3 not,\\\n\
     infile_bb_hi_sm  using ($1) : ($8/$7) with lines lc 3  dt 1 lw 3 not,\\\n\
     infile_all_hi_sm using ($1) : ($8/$7) with lines lc -1 dt 1 lw 3 not,\\\n\
     (all_hi_mean_y) with lines dt 4 lc -1 lw 3 not,\\\n\
     (dg_hi_mean_y)  with lines dt 4 lc 1  lw 3 not,\\\n\
     (bb_hi_mean_y)  with lines dt 4 lc 3  lw 3 not\n\
\n\
unset multiplot\n");
    
  //add a line to make thumbnail image
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, FIBER_EFFICIENCY_TRENDS_STEM, FIBER_EFFICIENCY_TRENDS_STEM);

  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;

  //  if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  //  if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) FALSE)) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  return 0;
}


//should improve the normalisation of the colour scale
//still arbitary at the moment
#define MAX_PANEL_COLS 3
#define MAX_PANEL_ROWS 3
int write_and_plot_fiber_usage ( focalplane_struct *pFocal, survey_struct *pSurvey )
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];

  float origin_x[MAX_PANEL_COLS];
  float origin_y[MAX_PANEL_ROWS];
  char  str_code[MAX_PANEL_COLS][STR_MAX];
  float dx, dy;
  int col,row;
  const int nrows = 3;
  int ncols = 0;
  if ( pFocal == NULL ) return 1;

  snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, FIBER_USAGE_STEM);
  if ((pPlotfile = (FILE*) fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }

  //work out the number of panels that we will need and size/origins of panels
  if ( pFocal->num_fibers_really_lo > 0 )
  {
    strcpy(str_code[ncols], "lo");
    ncols ++;
  }
  if ( pFocal->num_fibers_really_hi > 0 )
  {
    strcpy(str_code[ncols], "hi");
    ncols ++;
  }
  if ( pFocal->num_fibers_switchable > 0 )
  {
    strcpy(str_code[ncols], "sw");
    ncols ++;
  }
  if ( ncols == 0 ) return 0; //nothing to plot
  if ( ncols > MAX_PANEL_COLS ) return 1; //too many cols
  dx = 1.0 / (float) (MAX(2,ncols));
  dy = 1.0 / (float) nrows;
  for (col=0;col<ncols;col++) origin_x[col] = 0.02 + 0.96*dx * (float) col;
  for (row=0;row<nrows;row++) origin_y[row] = 0.92 - (0.90*dy * (float) (row+1));
  
  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY, ""))  return 1;

  fprintf(pPlotfile, "reset\n\
set terminal pdfcairo enhanced colour font \"Times,10\" size 21.0cm,29.7cm\n\
set out \"%s.pdf\"\n\
infile_hi = \"< gawk '$3~/^2$|^H$/' ../%s\"\n\
infile_lo = \"< gawk '$3~/^1$|^L$/' ../%s\"\n\
infile_sw = \"< gawk '$3~/^S$|^s$/' ../%s\"\n\
set format x \"\"\n\
set format y \"\"\n\
set lmargin 3\n\
set rmargin 5\n\
set tmargin 4\n\
set bmargin 4\n\
n_lo = %d\n\
n_hi = %d\n\
n_sw = %d\n\
ps_lo = 600./(n_lo+1.)\n\
ps_hi = 600./(n_hi+1.)\n\
ps_sw = 600./(n_sw+1.)\n\
str_label_lo = \"Lo-Res\"\n\
str_label_hi = \"Hi-Res\"\n\
str_label_sw = \"Switchable\"\n\
max_radius = %g\n\
set xrange [-max_radius:max_radius]\n\
set yrange [-max_radius:max_radius]\n\
set label 1 \"\" at graph 0.03,0.95 font \",14\" left\n\
set label 2 \"\" at graph 0.97,0.95 font \",14\" right\n\
set label 10 \"\" at graph 0.03,0.09 font \",12\" left\n\
set label 11 \"\" at graph 0.97,0.13 font \",12\" right\n\
str_title = \"{/*1.2 4MOST Fiber Usage Report}\\nPositioner=%s N_{lo-res}=%d N_{hi-res}=%d N_{switchable}=%d R_{patrol}=%gmm Pitch=%gmm\"\n\n",
          FIBER_USAGE_STEM,
          FIBERS_REPORT_LOG_FILENAME,
          FIBERS_REPORT_LOG_FILENAME,
          FIBERS_REPORT_LOG_FILENAME,
          pFocal->num_fibers_really_lo,
          pFocal->num_fibers_really_hi,
          pFocal->num_fibers_switchable,
          pFocal->fiber_offaxis_max_mm*1.08,
          pFocal->code_name, pFocal->num_fibers_really_lo, pFocal->num_fibers_really_hi, pFocal->num_fibers_switchable,
          pFocal->patrol_radius_max_mm,
          pFocal->positioner_spacing);

  //begin page 1
  fprintf(pPlotfile, "set label 100 str_title at screen 0.5,0.96 center font \",14\"\n\
%s\n\
set multiplot\n\
set size square\n\
set size %g,%g\n",
          OPSIM_BRANDING_PLOTSTRING_SCREEN, dx, dy);

  //begin row 0
  row=0;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Fraction assigned to targets\"\n\
set label 2 \"F_{targets}\"\n");
  for (col=0;col<ncols;col++)
  {
    fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($18/($18+$19+$20+$21)) nooutput\n\
set label 10 sprintf(\"Mean=%%.3f\\nMedian=%%.3f\", STATS_mean_y, STATS_median_y )\n\
set label 11 sprintf(\"Min=%%.3f\\nMax=%%.3f\\nStdDev=%%.3f\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" STATS_mean_y-STATS_stddev_y 1, \"\" STATS_mean_y+STATS_stddev_y 1)\n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($18/($18+$19+$20+$21)) with points pt 7 ps ps_%s lc palette not\n\
unset label 100 \n\
unset label 101 \n\
%s\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col],
            OPSIM_BRANDING_PLOTSTRING_UNSET);
  }

  //begin row 1
  row=1;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Fraction assigned to sky\"\n\
set label 2 \"F_{sky}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($19/($18+$19+$20+$21)) nooutput\n\
set label 10 sprintf(\"Mean=%%.3f\\nMedian=%%.3f\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.3f\\nMax=%%.3f\\nStdDev=%%.3f\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" STATS_mean_y-STATS_stddev_y 1, \"\" STATS_mean_y+STATS_stddev_y 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($19/($18+$19+$20+$21)) with points pt 7 ps ps_%s lc palette not\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col]);
  }


  //begin row 2
  row=2;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Fraction unassigned\"\n\
set label 2 \"F_{spare}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($20/($18+$19+$20+$21)) nooutput\n\
set label 10 sprintf(\"Mean=%%.3f\\nMedian=%%.3f\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.3f\\nMax=%%.3f\\nStdDev=%%.3f\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" STATS_mean_y-STATS_stddev_y 1, \"\" STATS_mean_y+STATS_stddev_y 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($20/($18+$19+$20+$21)) with points pt 7 ps ps_%s lc palette not\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col]);
  }
 fprintf(pPlotfile, "\nunset multiplot\n\n\n");
 //end page 1

 
 //begin page 2
 fprintf(pPlotfile, "set label 100 str_title at screen 0.5,0.96 center font \",14\"\n\
%s\n\
set multiplot\n\
set size square\n\
set size %g,%g\n", OPSIM_BRANDING_PLOTSTRING_SCREEN, dx, dy);

  //begin row 0
  row=0;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Fraction broken\"\n\
set label 2 \"F_{broken}\"\n");
  for (col=0;col<ncols;col++)
  {
    fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($21/($18+$19+$20+$21)) nooutput\n\
set label 10 sprintf(\"Mean=%%.3f\\nMedian=%%.3f\", STATS_mean_y, STATS_median_y )\n\
set label 11 sprintf(\"Min=%%.3f\\nMax=%%.3f\\nStdDev=%%.3f\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" STATS_mean_y-STATS_stddev_y 1, \"\" STATS_mean_y+STATS_stddev_y 1)\n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($21/($18+$19+$20+$21)) with points pt 7 ps ps_%s lc palette not\n\
unset label 100 \n\
unset label 101 \n\
%s\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col],
            OPSIM_BRANDING_PLOTSTRING_UNSET);
  }

  //begin row 1
  row=1;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Number of targets per fiber (x1e3)\"\n\
set label 2 \"N_{target}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($22) nooutput\n\
set label 10 sprintf(\"Mean=%%.1e\\nMedian=%%.1e\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.1e\\nMax=%%.1e\\nStdDev=%%.1e\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y/1e3 1, \"\" (STATS_mean_y-STATS_stddev_y)/1e3 1, \"\" (STATS_mean_y+STATS_stddev_y)/1e3 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($22/1e3) with points pt 7 ps ps_%s lc palette not\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col]);
  }
  //begin row 2
  row=2;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Weighted number of targets per fiber (x1e3)\"\n\
set label 2 \"W_{target}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($23) nooutput\n\
set label 10 sprintf(\"Mean=%%.1e\\nMedian=%%.1e\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.1e\\nMax=%%.1e\\nStdDev=%%.1e\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y/1e3 1, \"\" (STATS_mean_y-STATS_stddev_y)/1e3 1, \"\" (STATS_mean_y+STATS_stddev_y)/1e3 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($23/1e3) with points pt 7 ps ps_%s lc palette not\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col]);
  }
 
  fprintf(pPlotfile, "\nunset multiplot\n\n\n");
  // end page 2
  
  // begin page 3
  fprintf(pPlotfile, "\n\n\
set label 100 str_title at screen 0.5,0.96 center font \",14\"\n\
%s\n\
set multiplot\n\
set size square\n\
set size %g,%g\n", OPSIM_BRANDING_PLOTSTRING_SCREEN, dx, dy);

  //begin row 0
  row=0;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Number of moves (x1e3)\"\n\
set label 2 \"N_{moves}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($14) nooutput\n\
set label 10 sprintf(\"Mean=%%.1e\\nMedian=%%.1e\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.1e\\nMax=%%.1e\\nStdDev=%%.1e\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" STATS_mean_y-STATS_stddev_y 1, \"\" STATS_mean_y+STATS_stddev_y 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($14/1e3) with points pt 7 ps ps_%s lc palette not\n\
unset label 100 \n\
unset label 101 \n\
%s\n",
           str_code[col],
           str_code[col],
           origin_x[col], origin_y[row],
           str_code[col],
           str_code[col],
           OPSIM_BRANDING_PLOTSTRING_UNSET);
  }

  //begin row 1
  row=1;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Distance moved axis1 (m)\"\n\
set label 2 \"D_{moved1}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($15/1e3) nooutput\n\
set label 10 sprintf(\"Mean=%%.1e\\nMedian=%%.1e\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.1e\\nMax=%%.1e\\nStdDev=%%.1e\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" (STATS_mean_y-STATS_stddev_y) 1, \"\" (STATS_mean_y+STATS_stddev_y) 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($15/1e3) with points pt 7 ps ps_%s lc palette not\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col]);
  }

   //begin row 2
  row=2;
  fprintf(pPlotfile, "\n\n\
set cblabel \"Distance moved axis2 (m)\"\n\
set label 2 \"D_{moved2}\"\n");
  for (col=0;col<ncols;col++)
  {
   fprintf(pPlotfile, "set label 1 str_label_%s\n\
stats [*:*][*:*]  infile_%s using 5 : ($16/1e3) nooutput\n\
set label 10 sprintf(\"Mean=%%.1e\\nMedian=%%.1e\", STATS_mean_y, STATS_median_y ) \n\
set label 11 sprintf(\"Min=%%.1e\\nMax=%%.1e\\nStdDev=%%.1e\", STATS_min_y, STATS_max_y, STATS_stddev_y ) \n\
set cbtics autofreq  scale 1.0,3.4 nomirror \n\
set cbtics add (\"\" STATS_mean_y 1, \"\" (STATS_mean_y-STATS_stddev_y) 1, \"\" (STATS_mean_y+STATS_stddev_y) 1)  \n\
set origin %g,%g\n\
plot infile_%s using 5 : 6 : ($16/1e3) with points pt 7 ps ps_%s lc palette not\n",
            str_code[col],
            str_code[col],
            origin_x[col], origin_y[row],
            str_code[col],
            str_code[col]);
  }

  fprintf(pPlotfile, "\nunset multiplot\n\n\n");
 //end page 3


 
  //add a line to make thumbnail image
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, FIBER_USAGE_STEM, FIBER_USAGE_STEM);

  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;

  //  if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  //  if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) FALSE)) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  return 0;
}


//int twoDhisto_struct_init (twoDhisto_struct *pHisto)
//{
//  if ( pHisto == NULL ) return 1;
//  pHisto->xmin = NAN;
//  pHisto->xmax = NAN;
//  pHisto->dx = NAN;
//
//  pHisto->ymin = NAN;
//  pHisto->ymax = NAN;
//  pHisto->dy = NAN;
//
//  pHisto->nx = 0;
//  pHisto->ny = 0;
//  pHisto->npix = 0;
//  pHisto->data_sum = 0;
//  pHisto->pData = NULL;
//  return 0;
//}

//int twoDhisto_struct_setup_for_fiber_offsets (twoDhisto_struct *pHisto, focalplane_struct* pFocal)
int fiber_offsets_setup_2Dhisto (focalplane_struct* pFocal, twoDhisto_struct *pHisto)
{
  if (  pFocal == NULL ||
        pHisto == NULL ) return 1;
  pHisto->xmin = -1.1*pFocal->patrol_radius_max_mm;
  pHisto->xmax =  1.1*pFocal->patrol_radius_max_mm;
  pHisto->ymin = -1.1*pFocal->patrol_radius_max_mm;
  pHisto->ymax =  1.1*pFocal->patrol_radius_max_mm;
  pHisto->nx   = (int) FIBER_OFFSET_HISTO_NPIX;
  pHisto->ny   = (int) FIBER_OFFSET_HISTO_NPIX;
  pHisto->npix = pHisto->nx * pHisto->ny;
  if ( pHisto->nx <= 0 ||
       pHisto->ny <= 0 ) return 1; //bad
  pHisto->dx   = (pHisto->xmax - pHisto->xmin) / (double) pHisto->nx;
  pHisto->dy   = (pHisto->ymax - pHisto->ymin) / (double) pHisto->ny;
  if ( pHisto->dx <= 0. ||
       pHisto->dy <= 0. ) return 1; //bad
  pHisto->inv_dx = 1.0 / pHisto->dx;
  pHisto->inv_dy = 1.0 / pHisto->dy;
  
  if ( (pHisto->pData = (int*) calloc((size_t) pHisto->npix , sizeof(int))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  pHisto->data_sum = 0;
  return 0;
}


//should improve the normalisation of the colour scale
//still arbitary at the moment
int fiber_offsets_write_gnuplot_file ( focalplane_struct *pFocal, const char* str_dirname, const char *str_basename )
{
  FILE *pPlotfile = NULL;
  char str_filename[STR_MAX];
  if ( pFocal == NULL ) return 1;

  snprintf(str_filename, STR_MAX, "%s/plot_%s.plot", str_dirname, str_basename);
  if ((pPlotfile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) str_dirname, ""))  return 1;
  
  
  fprintf(pPlotfile, "\n\
reset\n\
map_lo = \"< awk '$2~/xaxis/ && $9~/step/ {xstep=$10} //{if(NF==0){split(last,a);print a[1],a[2],a[3]+xstep,a[4],a[5],a[6]};print $0;last=$0}' %s_lores.txt\"\n\
map_hi = \"< awk '$2~/xaxis/ && $9~/step/ {xstep=$10} //{if(NF==0){split(last,a);print a[1],a[2],a[3]+xstep,a[4],a[5],a[6]};print $0;last=$0}' %s_hires.txt\"\n\
infile_radial_lo = \"< sort -g -k 7 %s_lores.txt | awk '$1!~/^#/ && NF > 0 {sum+=$6; print $7,sum}'\"\n\
infile_radial_hi = \"< sort -g -k 7 %s_hires.txt | awk '$1!~/^#/ && NF > 0 {sum+=$6; print $7,sum}'\"\n\
infile_angle_lo  = \"< sort -g -k 8 %s_lores.txt | awk '$1!~/^#/ && NF > 0 {sum+=$6; print $8,sum}'\"\n\
infile_angle_hi  = \"< sort -g -k 8 %s_hires.txt | awk '$1!~/^#/ && NF > 0 {sum+=$6; print $8,sum}'\"\n\
infile_cumul_lo = \"< sort -g -r -k 6 %s_lores.txt | awk '$1!~/^#/ && NF > 0 && $6 > 0. {n++; d[n]=$6;} END {for(i=1;i<=n;i++){print d[i],i,(i/n)}}'\"\n\
infile_cumul_hi = \"< sort -g -r -k 6 %s_hires.txt | awk '$1!~/^#/ && NF > 0 && $6 > 0. {n++; d[n]=$6;} END {for(i=1;i<=n;i++){print d[i],i,(i/n)}}'\"\n\
infile_points = \"< awk '$1!~/^#/{r2=$2*$2+$3*$3;print $2,$3,r2}' ../%s/%s.txt | sort -g -k 3 | awk '//{n++;if(n==1){offx=$1;offy=$2;} print ($1-offx),($2-offy);}'\"\n\
out_str = \"%s.pdf\"\n\
set terminal pdfcairo enhanced colour font \"Times,16\" size 21.0cm,29.7cm\n\
set out out_str\n\
patrol_radius_max = %g\n\
patrol_radius_min = %g\n",
          str_basename, str_basename,
          str_basename, str_basename,
          str_basename, str_basename,
          str_basename, str_basename,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          str_basename, 
          pFocal->patrol_radius_max_mm,
          pFocal->patrol_radius_min_mm);

  fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_SCREEN);

 
  fprintf(pPlotfile, "\n\
set xlabel \"Offset (mm)\" offset 0,1.2 \n\
set ylabel \"Offset (mm)\" offset 1.3,0.0 \n\
set tics scale 0.5 \n\
const = 1000.0 \n\
set view map\n\
set format cb \"%%-3g\"\n\
set multiplot  title \"Fiber offset distribution\" font \",24\"\n\
set size 0.45,0.32\n\
set xrange [-1.15*patrol_radius_max:1.15*patrol_radius_max]\n\
set yrange [-1.15*patrol_radius_max:1.15*patrol_radius_max]\n\
set xtics offset 0.0,0.5\n\
set cblabel \"linear density (arbitary scale)\"\n\
set mcbtics 2\n\
set cbtics 0,0.1,10\n\
set label 2 \"Linear Scale\"    at graph 0.98,1.03 right front tc lt -1\n\
set label 1 \"LoRes fibers\"    at graph 0.02,1.03 left front tc lt 1\n\
set origin 0.0,0.68\n\
splot  map_lo using 3 : 4 : ($6*const) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#cccccc\" not\n");
  
  fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_UNSET);
  
  fprintf(pPlotfile, "\n\
set label 1 \"HiRes fibers\"    at graph 0.02,1.03 left front tc lt 3\n\
set origin 0.5,0.68\n\
splot  map_hi using 3 : 4 : ($6*const) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#cccccc\" not\n\
set cblabel \"log(density) (arbitary scale)\"\n\
set cbtics -10,1.0,10\n\
set label 2 \"Log Scale\"       at graph 0.98,1.03 right front tc lt -1\n\
set label 1 \"LoRes fibers\"    at graph 0.02,1.03 left front tc lt 1\n\
set origin 0.0,0.42\n\
splot  map_lo using 3 : 4 : (log10($6*const)) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#999999\" not\n\
set label 1 \"HiRes fibers\"    at graph 0.02,1.03 left front tc lt 3\n\
set origin 0.5,0.42\n\
splot  map_hi using 3 : 4 : (log10($6*const)) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#999999\" not\n\
\n\
set size 0.43,0.22\n\
unset label 2 \n\
set key top left Left reverse samplen 2\n\
set ytics 0,0.2,1.0\n\
set ylabel \"Cumulative fraction\" offset 1.3,0.0 \n\
set xlabel \"Radius (mm)\" offset 0,1.2 \n\
set label 1 \"Cumulative radial distribution\"    at graph 0.02,1.05 left front tc lt -1\n\
set arrow 1 from patrol_radius_min,0.0 to patrol_radius_min,1.0 nohead lt 2 lc -1\n\
set arrow 2 from patrol_radius_max,0.0 to patrol_radius_max,1.0 nohead lt 2 lc -1\n\
set origin 0.01,0.215\n\
plot [0:patrol_radius_max*1.1][0:1] \\\n\
     infile_radial_lo using 1 : 2 with lines lt 1 lc 1 t \"LoRes fibers\",\\\n\
     infile_radial_hi using 1 : 2 with lines lt 1 lc 3 t \"HiRes fibers\"\n\
\n\
unset arrow\n\
set label 1 \"Cumulative angular distribution\"    at graph 0.02,1.05 left front tc lt -1\n\
set xlabel \"Angle (radians)\" offset 0,1.2\n\
set xtics (\"0\" 0., \"{/Symbol p}/2\" 0.5, \"{/Symbol p}\" 1.0, \"3{/Symbol p}/2\" 1.5, \"2{/Symbol p}\" 2.0)\n\
set origin 0.52,0.215\n\
plot [0:2][0:1] \\\n\
     infile_angle_lo using ($1/pi) : 2 with lines lt 1 lc 1 t \"LoRes fibers\",\\\n\
     infile_angle_hi using ($1/pi) : 2 with lines lt 1 lc 3 t \"HiRes fibers\"\n\
\n\
set size 0.7,0.20\n\
set origin 0.15,0.01\n\
set xtics auto\n\
set ytics auto\n\
set label 1 \"Cumulative density distribution\"    at graph 0.02,1.05 left front tc lt -1\n\
set xlabel \"Offset Density (fraction of all offsets, per pixel)\" offset 0.0,0.8\n\
set ylabel \"Frac^n of pixels (>Density)\"\n\
set key bottom left Left samplen 2\n\
set logscale xy\n\
set format x \"10^{%%L}\" \n\
plot [1e-6:*][1e-2:1.1] \\\n\
     infile_cumul_lo using ($1) : 3 with lines lt 1 lc 1 t \"LoRes Fibers\",\\\n\
     infile_cumul_hi using ($1) : 3 with lines lt 1 lc 3 t \"HiRes Fibers\"\n\
\n\
unset multiplot\n\
\n\
set out\n\
print \"Result plot is at \",out_str\n\n");
         
  //add a line to make thumbnail image
  fprintf(pPlotfile, "\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, str_basename, str_basename);

  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;
  return 0;
}

/*
//should improve the normalisation of the colour scale
//still arbitary at the moment
int fiber_offsets_write_gnuplot_file_old ( focalplane_struct *pFocal, const char* str_dirname, const char *str_basename )
{
  FILE *pPlotfile = NULL;
  char str_filename[STR_MAX];
  if ( pFocal == NULL ) return 1;

  snprintf(str_filename, STR_MAX, "%s/plot_%s.plot", str_dirname, str_basename);
  if ((pPlotfile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) str_dirname, ""))  return 1;
  
  
  fprintf(pPlotfile, "\n\
reset\n\
map_lo = \"< gawk '$2~/xaxis/ && $9~/step/ {xstep=$10} //{if(NF==0){split(last,a);print a[1],a[2],a[3]+xstep,a[4],a[5],a[6]};print $0;last=$0}' %s_lores.txt\"\n\
map_hi = \"< gawk '$2~/xaxis/ && $9~/step/ {xstep=$10} //{if(NF==0){split(last,a);print a[1],a[2],a[3]+xstep,a[4],a[5],a[6]};print $0;last=$0}' %s_hires.txt\"\n\
#map_lo  = \"%s_lores.txt\"\n\
#map_hi  = \"%s_hires.txt\"\n\
infile_radial_lo = \"< sort -g -k 7 %s_lores.txt | gawk '$1!~/^#/ && NF > 0 {sum+=$6; print $7,sum}'\"\n\
infile_radial_hi = \"< sort -g -k 7 %s_hires.txt | gawk '$1!~/^#/ && NF > 0 {sum+=$6; print $7,sum}'\"\n\
infile_angle_lo  = \"< sort -g -k 8 %s_lores.txt | gawk '$1!~/^#/ && NF > 0 {sum+=$6; print $8,sum}'\"\n\
infile_angle_hi  = \"< sort -g -k 8 %s_hires.txt | gawk '$1!~/^#/ && NF > 0 {sum+=$6; print $8,sum}'\"\n\
infile_cumul_lo = \"< sort -g -r -k 6 %s_lores.txt | gawk '$1!~/^#/ && NF > 0 && $6 > 0. {n++; d[n]=$6;} END {for(i=1;i<=n;i++){print d[i],i,(i/n)}}'\"\n\
infile_cumul_hi = \"< sort -g -r -k 6 %s_hires.txt | gawk '$1!~/^#/ && NF > 0 && $6 > 0. {n++; d[n]=$6;} END {for(i=1;i<=n;i++){print d[i],i,(i/n)}}'\"\n\
infile_points = \"< gawk '$1!~/^#/{r2=$2*$2+$3*$3;print $2,$3,r2}' ../%s/%s.txt | sort -g -k 3 | gawk '//{n++;if(n==1){offx=$1;offy=$2;} print ($1-offx),($2-offy);}'\"\n\
out_str = \"%s.ps\"\n\
outpdf_str = \"%s.pdf\"\n\
set terminal postscript enhanced portrait colour font \"Times,11\" \n\
set out out_str\n\
patrol_radius_max = %g\n\
patrol_radius_min = %g\n",
          str_basename, str_basename,
          str_basename, str_basename,
          str_basename, str_basename,
          str_basename, str_basename,
          str_basename, str_basename,
          PLOTFILES_SUBDIRECTORY, FIBERS_GEOMETRY_STEM,
          str_basename, str_basename,
          pFocal->patrol_radius_max_mm,
          pFocal->patrol_radius_min_mm);
  
  fprintf(pPlotfile, "\n\
set xlabel \"Offset (mm)\" offset 0,0.3 \n\
set ylabel \"Offset (mm)\" offset 1.3,0.0 \n\
set tics scale 0.5 \n\
const = 1000.0 \n\
set view map\n\
set format cb \"%%-3g\"\n\
set multiplot\n\
set size 0.5,0.35\n\
set xrange [-1.15*patrol_radius_max:1.15*patrol_radius_max]\n\
set yrange [-1.15*patrol_radius_max:1.15*patrol_radius_max]\n\
set cblabel \"linear density (arbitary scale)\"\n\
set mcbtics 2\n\
set cbtics 0,0.1,10\n\
set label 2 \"Linear Scale\"    at graph 0.98,1.05 right front tc lt -1\n\
set label 1 \"LoRes fibers\"    at graph 0.02,1.05 left front tc lt 1\n\
set origin 0.0,0.71\n\
splot  map_lo using 3 : 4 : ($6*const) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#cccccc\" not\n\
set label 1 \"HiRes fibers\"    at graph 0.02,1.05 left front tc lt 3\n\
set origin 0.5,0.71\n\
splot  map_hi using 3 : 4 : ($6*const) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#cccccc\" not\n\
set cblabel \"log(density) (arbitary scale)\"\n\
set cbtics -10,1.0,10\n\
set label 2 \"Log Scale\"    at graph 0.98,1.05 right front tc lt -1\n\
set label 1 \"LoRes fibers\"    at graph 0.02,1.05 left front tc lt 1\n\
set origin 0.0,0.43\n\
splot  map_lo using 3 : 4 : (log10($6*const)) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#999999\" not\n\
set label 1 \"HiRes fibers\"    at graph 0.02,1.05 left front tc lt 3\n\
set origin 0.5,0.43\n\
splot  map_hi using 3 : 4 : (log10($6*const)) with pm3d not,\\\n\
       infile_points using 1 : 2 : (0.0) with points pt 1 ps 1.0 lw 2 lc rgb \"#999999\" not\n\
\n\
set size nosquare \n\
set size 0.45,0.27\n\
unset label 2 \n\
set key bottom right samplen 1\n\
set ytics 0,0.2,1.0\n\
set ylabel \"Cumulative fraction\" offset 1.3,0.0 \n\
set xlabel \"Radius (mm)\" offset 0,0.3 \n\
set label 1 \"Cumulative radial dist^n\"    at graph 0.02,1.05 left front tc lt -1\n\
set arrow 1 from patrol_radius_min,0.0 to patrol_radius_min,1.0 nohead lt 2 lc -1\n\
set arrow 2 from patrol_radius_max,0.0 to patrol_radius_max,1.0 nohead lt 2 lc -1\n\
set origin 0.01,0.18\n\
plot [0:patrol_radius_max*1.1][0:1] \\\n\
     infile_radial_lo using 1 : 2 with lines lt 1 lc 1 t \"LoRes fiber\",\\\n\
     infile_radial_hi using 1 : 2 with lines lt 1 lc 3 t \"HiRes fiber\"\n\
\n\
unset arrow\n\
set label 1 \"Cumulative angular dist^n\"    at graph 0.02,1.05 left front tc lt -1\n\
set xlabel \"Angle (radians)\" offset 0,0.3\n\
set xtics (\"0\" 0., \"{/Symbol p}/2\" 0.5, \"{/Symbol p}\" 1.0, \"3{/Symbol p}/2\" 1.5, \"2{/Symbol p}\" 2.0)\n\
set origin 0.52,0.18\n\
plot [0:2][0:1] \\\n\
     infile_angle_lo using ($1/pi) : 2 with lines lt 1 lc 1 t \"LoRes fiber\",\\\n\
     infile_angle_hi using ($1/pi) : 2 with lines lt 1 lc 3 t \"HiRes fiber\"\n\
\n\
set size 0.6,0.23\n\
set origin 0.20,-0.05\n\
set xtics auto\n\
set ytics auto\n\
set label 1 \"Cumulative density dist^n\"    at graph 0.32,1.10 left front tc lt -1\n\
set xlabel \"Offset Density (fraction of all offsets, per pixel)\"\n\
set ylabel \"Frac^n of pixels (>Density)\"\n\
set key bottom left samplen 2\n\
set logscale xy\n\
set format x \"10^{%%L}\" \n\
plot [1e-6:*][1e-2:1.1] \\\n\
     infile_cumul_lo using ($1) : 3 with lines lt 1 lc 1 t \"LoRes Fibers\",\\\n\
     infile_cumul_hi using ($1) : 3 with lines lt 1 lc 3 t \"HiRes Fibers\"\n\
\n\
unset multiplot\n\
\n\
str_sys = sprintf(\"ps2pdf %%s %%s\", out_str, outpdf_str)\n\
system(str_sys)\n\
str_sys = sprintf(\"rm %%s\", out_str)\n\
system(str_sys)\n\
print \"Result plot is at \",outpdf_str\n\n");
         
  //add a line to make thumbnail image
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, str_basename, str_basename);

  if ( pPlotfile) fclose(pPlotfile); pPlotfile = NULL;
  return 0;
}
*/

int fiber_offsets_write_and_plot (focalplane_struct *pFocal, survey_struct *pSurvey)
{
  char strPlotfile[STR_MAX];
  if ( pFocal == NULL || pSurvey == NULL ) return 1;

  if ( util_make_directory ((const char*) FIBER_OFFSET_SUBDIRECTORY))
  {
    return 1;
  }
  snprintf(strPlotfile, STR_MAX, "%s/%s_lores.txt", FIBER_OFFSET_SUBDIRECTORY, FIBER_OFFSETS_STEM );
  if ( twoDhisto_struct_write_to_file  ((twoDhisto_struct*) &(pFocal->fiberXY_histo_lores),
                                       (const char*) strPlotfile)) return 1;
  snprintf(strPlotfile, STR_MAX, "%s/%s_hires.txt", FIBER_OFFSET_SUBDIRECTORY, FIBER_OFFSETS_STEM );
  if ( twoDhisto_struct_write_to_file  ((twoDhisto_struct*) &(pFocal->fiberXY_histo_hires),
                                       (const char*) strPlotfile)) return 1;
  snprintf(strPlotfile, STR_MAX, "%s/%s_swres.txt", FIBER_OFFSET_SUBDIRECTORY, FIBER_OFFSETS_STEM );
  if ( twoDhisto_struct_write_to_file  ((twoDhisto_struct*) &(pFocal->fiberXY_histo_swres),
                                       (const char*) strPlotfile)) return 1;
  if (fiber_offsets_write_gnuplot_file ((focalplane_struct*) pFocal,
                                        (const char*) FIBER_OFFSET_SUBDIRECTORY,
                                        (const char*) FIBER_OFFSETS_STEM )) return 1;

  snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", FIBER_OFFSET_SUBDIRECTORY,  FIBER_OFFSETS_STEM);
  //  if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  return 0;
}



//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
int plot_sky_visibility (survey_struct *pSurvey,
                         telescope_struct *pTele,
                         focalplane_struct *pFocal,
                         fieldlist_struct *pFieldList,
                         const char* str_stem)
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];
  //  char str_gnuplot_code_name[STR_MAX];
  int a;
  double hard_dec_max;
  double hard_dec_min;

  if ( pTele == NULL || pFocal == NULL ||  pSurvey == NULL) return 1;

 
  (void) snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, str_stem);
  fprintf(stdout, "%s Making plot of tiling on the sky: %s -> %s/%s.pdf ... ",
          COMMENT_PREFIX, strPlotfile, PLOTFILES_SUBDIRECTORY, str_stem);
  fflush(stdout);
  if ((pPlotfile = fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the plotfile file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }
  hard_dec_max = CLIP(pTele->latitude_deg + pSurvey->zenith_angle_max,-90.0,+90.0); 
  hard_dec_min = CLIP(pTele->latitude_deg - pSurvey->zenith_angle_max,-90.0,+90.0); 

  //  hard_dec_max = pTele->latitude_deg + pSurvey->zenith_angle_max; 
  //  hard_dec_min = pTele->latitude_deg - pSurvey->zenith_angle_max; 
  
  if ( util_write_cd_line_to_plotfile (pPlotfile, PLOTFILES_SUBDIRECTORY, ""))  return 1;

//  //remove underscores from the positioner code name
//  (void) sstrncpy(str_gnuplot_code_name,pFocal->code_name , STR_MAX);
//  for(i=0;i<=(int)strlen(str_gnuplot_code_name);i++)
//  {
//    if(str_gnuplot_code_name[i] == '_' ) str_gnuplot_code_name[i] = ' ';
//  }
  
    
  fprintf(pPlotfile, "reset\n\
set angles degrees\n\
set size 1.0,0.6\n\
set origin 0.0,0.15\n\
set key bottom left Left reverse samplen 2\n\
\n");

  //now write out plot on standard axes, and with equal area projection.
  //this will give the areas of the sky that will receive tiles (ie based on ntiles_todo values)
  if ( geom_write_gnuplot_map_projection_functions (pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                    (int) pSurvey->map_projection, (BOOL) pSurvey->map_left_is_east,
                                                    (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 11,
                                                    (const char*) "#909090")) return 1;

  {
    char str_npoints[STR_MAX];
    if ( pFieldList->tiling_method==TILING_METHOD_FILE ) sprintf(str_npoints, "-");
    else  sprintf(str_npoints, "-N_{pnts}%d", pFieldList->tiling_num_points);
    fprintf(pPlotfile, "\n\
infile_boundaries = \"< gawk 'BEGIN{for(i=0;i<=360;i++){print i}; exit}'\"\n\
\n\
set terminal pdfcairo enhanced colour font \"Times,14\" size 29.7cm,21.0cm\n\
set out \"%s.pdf\"\n\
%s\n\
str_title = \"4MOST Relative Sky Visibility\\nTele=%s %s%s, FOV=%gdeg^2, %g year survey\"\n\
set title str_title font \",16\" offset 0,0.5\n\
infile =  \"< zcat -f ../%s\"\n\
set cblabel \"Number of observing slots Field is visible\"\n\
\n",
            str_stem,
            OPSIM_BRANDING_PLOTSTRING_GRAPH,
            pTele->code_name,
            //          str_gnuplot_code_name,
            (pFieldList->tiling_method==TILING_METHOD_HARDIN_SLOANE?"Hardin-Sloane":
             (pFieldList->tiling_method==TILING_METHOD_GEODESIC? "Geodesic":
              (pFieldList->tiling_method==TILING_METHOD_FILE? "User" : "Unknown"))),
            str_npoints, pTele->fov_area_deg, pSurvey->duration,
            PER_FIELD_LOG_FILENAME);
  }
  //the predicted visibility
  fprintf(pPlotfile, "\nset label 1 \"Predicted visibility\" at graph 0.005,0.03 font \",24\"\n");
  fprintf(pPlotfile, "\n\nplot [][] \\\n\
        infile                  using (x($3,$4)) : (y($3,$4)) : ($17+$18) with points pt 7 ps 0.9 lc palette not,\\\n\
        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not\n\n\n\n",
          pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
          pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
          hard_dec_max,hard_dec_max,
          hard_dec_min,hard_dec_min);

  //the realised visibility
  fprintf(pPlotfile, "\nset label 1 \"Realised visibility\" at graph 0.005,0.03 font \",24\"\n");
  fprintf(pPlotfile, "\n\nplot [][] \\\n\
        infile                  using (x($3,$4)) : (y($3,$4)) : ($21+$22) with points pt 7 ps 0.9 lc palette not,\\\n\
        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not\n\n\n\n",
          pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
          pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
          hard_dec_max,hard_dec_max,
          hard_dec_min,hard_dec_min);

  // now make the plots as a function of visibility
  for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
  {
    
    //the predicted visibility
    fprintf(pPlotfile, "\nset label 1 \"Predicted visibility\\nAirmass<=%.1f\" at graph 0.005,0.09 font \",24\"\n",
            1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE);
    fprintf(pPlotfile, "\n\nplot [][] \\\n\
        infile                  using (x($3,$4)) : (y($3,$4)) : ($%d+$%d) with points pt 7 ps 0.9 lc palette not,\\\n\
        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not\n\n\n\n",
            31+a*4, 32+a*4,
            pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
            pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
            hard_dec_max,hard_dec_max,
            hard_dec_min,hard_dec_min);

    //the realised visibility
    fprintf(pPlotfile, "\nset label 1 \"Realised visibility\\nAirmass<=%.1f\" at graph 0.005,0.09 font \",24\"\n",
            1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE);
    fprintf(pPlotfile, "\n\nplot [][] \\\n\
        infile                  using (x($3,$4)) : (y($3,$4)) : ($%d+$%d) with points pt 7 ps 0.9 lc palette not,\\\n\
        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 1 dt 1 lw 2 not\n\n\n\n",
            33+a*4, 34+a*4,
            pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
            pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
            hard_dec_max,hard_dec_max,
            hard_dec_min,hard_dec_min);
  }
  //add a line to make thumbnail images
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, str_stem, str_stem);

  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  //if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  fprintf(stdout, " done\n");  fflush(stdout);
  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
int plot_sky_tiling (survey_struct *pSurvey, telescope_struct *pTele, focalplane_struct *pFocal, fieldlist_struct *pFieldList, const char* str_stem)
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];
  char str_gnuplot_code_name[STR_MAX];
  int i;
  double hard_dec_max;
  double hard_dec_min;

  if ( pTele == NULL || pFocal == NULL ||  pSurvey == NULL) return 1;

  if ( strcmp(str_stem, SKY_TILING_EXECUTED_STEM)  != 0 &&
       strcmp(str_stem, SKY_TILING_PLANNED_STEM)   != 0 &&
       strcmp(str_stem, SKY_TILING_REQUESTED_STEM) != 0 )
  {
    fprintf(stderr, "%s  There were problems understanding this str_stem: %s\n",
            ERROR_PREFIX, str_stem);
    return 1;
  }
  (void) snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, str_stem);
  fprintf(stdout, "%s Making plot of tiling on the sky: %s -> %s/%s.pdf ... ",
          COMMENT_PREFIX, strPlotfile, PLOTFILES_SUBDIRECTORY, str_stem);
  fflush(stdout);
  if ((pPlotfile = fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the plotfile file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }

  //  hard_dec_max = pTele->latitude_deg + pSurvey->zenith_angle_max; 
  //  hard_dec_min = pTele->latitude_deg - pSurvey->zenith_angle_max; 
  hard_dec_max = CLIP(pTele->latitude_deg + pSurvey->zenith_angle_max,-90.0,+90.0); 
  hard_dec_min = CLIP(pTele->latitude_deg - pSurvey->zenith_angle_max,-90.0,+90.0); 


  if ( util_write_cd_line_to_plotfile (pPlotfile, PLOTFILES_SUBDIRECTORY, ""))  return 1;

  //remove underscores from the tile code name
  (void) sstrncpy(str_gnuplot_code_name,pFocal->code_name , STR_MAX);
  for(i=0;i<=(int)strlen(str_gnuplot_code_name);i++)
  {
    if(str_gnuplot_code_name[i] == '_' ) str_gnuplot_code_name[i] = ' ';
  }
  
  fprintf(pPlotfile, "reset\n\
set angles degrees\n\
set size 1.0,0.7\n\
set origin 0.0,0.15\n\
set key bottom left Left reverse samplen 2\n\
rgb_bb = \"#0000bb\"\n\
rgb_dg = \"#dd4000\"\n\
rgb_bb0  = \"#00ffff\"\n\
rgb_dg0  = \"#ffff00\"\n");

  //now write out plot on standard axes, and with equal area projection.
  //this will give the areas of the sky that will receive tiles (ie based on ntiles_todo values)
  if ( geom_write_gnuplot_map_projection_functions (pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                    (int) pSurvey->map_projection,(BOOL) pSurvey->map_left_is_east,
                                                    (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 11,
                                                    (const char*) "#000000")) return 1;

  fprintf(pPlotfile, "\n\
infile_boundaries = \"< gawk 'BEGIN{for(i=0;i<=360;i++){print i}; exit}'\"\n\
\n\
set terminal pdfcairo enhanced colour font \"Times,14\" size 29.7cm,21.0cm\n\
set out \"%s.pdf\"\n\
%s\n\
str_title = \"4MOST Sky Tiling layout\\nTele=%s Positioner='%s' %s-N_{pnts}=%d, FOV=%gdeg^2, %g year survey\"\n\
set title str_title font \",16\" offset 0,2.4\n\
in_area_evertex_file_dg  = \"< gawk 'NF==0{print $0}  // {flag=strtonum($3); if(and(flag,%u)){print $0}}' %s\"\n\
in_area_evertex_file_bb  = \"< gawk 'NF==0{print $0}  // {flag=strtonum($3); if(and(flag,%u)){print $0}}' %s\"\n\n",
          str_stem,
          OPSIM_BRANDING_PLOTSTRING_GRAPH,
          pTele->code_name, str_gnuplot_code_name,
          (pFieldList->tiling_method==TILING_METHOD_HARDIN_SLOANE?"Hardin-Sloane":
           (pFieldList->tiling_method==TILING_METHOD_GEODESIC? "Geodesic":
            (pFieldList->tiling_method==TILING_METHOD_FILE? "User" : "Unknown"))),
          pFieldList->tiling_num_points, pTele->fov_area_deg, pSurvey->duration,
          SURVEY_IN_AREA_DARKGREY,  FIELD_VERTICES_EXTENDED_FILENAME, 
          SURVEY_IN_AREA_BRIGHT,    FIELD_VERTICES_EXTENDED_FILENAME);
   
  ///////////////////////////////////////////////////////////////////////////////////////////////
  //Now write the final sky coverage 
  //now write out plot with colour depth encoding the number of tiles per field that were executed
  {
    //work out the colour density per tile - aim for a max colour of ~80% so that can still see overlaps 
    const int max_steps = 8;
    const float colour_density_floor = 0.05;
    int i_last = -1;
    float ypos_start; 
    float ypos;
    float colour_density_dg;
    float colour_density_bb;
    int max_tiles_dg;
    int max_tiles_bb;
    int *phisto_dg = NULL;
    int *phisto_bb = NULL;
    int col_dg;
    int col_bb;
    int head_col_dg;
    int head_col_bb;
    
    if ( strcmp(str_stem, SKY_TILING_EXECUTED_STEM) == 0 )
    {
      max_tiles_dg = pSurvey->pmoon_dg->max_num_tiles_per_field_done;
      max_tiles_bb = pSurvey->pmoon_bb->max_num_tiles_per_field_done;
      phisto_dg = (int*) pSurvey->pmoon_dg->histo_tiles_per_field_done;
      phisto_bb = (int*) pSurvey->pmoon_bb->histo_tiles_per_field_done;
      col_dg = 11;
      col_bb = 10;
      head_col_dg = 18;
      head_col_bb = 16;
    }
    else if ( strcmp(str_stem, SKY_TILING_PLANNED_STEM) == 0)
    {
      max_tiles_dg = pSurvey->pmoon_dg->max_num_tiles_per_field_todo;
      max_tiles_bb = pSurvey->pmoon_bb->max_num_tiles_per_field_todo;
      phisto_dg = (int*) pSurvey->pmoon_dg->histo_tiles_per_field_todo;
      phisto_bb = (int*) pSurvey->pmoon_bb->histo_tiles_per_field_todo;
      col_dg = 7;
      col_bb = 6;
      head_col_dg = 14;
      head_col_bb = 12;
    }
    else if ( strcmp(str_stem, SKY_TILING_REQUESTED_STEM) == 0)
    {
      max_tiles_dg = pSurvey->pmoon_dg->max_num_tiles_per_field_req;
      max_tiles_bb = pSurvey->pmoon_bb->max_num_tiles_per_field_req;
      phisto_dg = (int*) pSurvey->pmoon_dg->histo_tiles_per_field_req;
      phisto_bb = (int*) pSurvey->pmoon_bb->histo_tiles_per_field_req;
      col_dg = 5;
      col_bb = 4;
      head_col_dg = 30;
      head_col_bb = 28;
    }
    else return 1;

    //apply some absolute upper limits to help with dynamic range in plots
    max_tiles_dg = MIN(max_tiles_dg,16); 
    max_tiles_bb = MIN(max_tiles_bb,16); 

    
    colour_density_dg = 0.90 / (float) MAX(max_tiles_dg,1);
    colour_density_bb = 0.90 / (float) MAX(max_tiles_bb,1);
 
    //threshold so shallow surveys not too dark
    if ( colour_density_dg > 0.15 ) colour_density_dg=0.15;
    if ( colour_density_bb > 0.15 ) colour_density_bb=0.15;
    switch (pSurvey->map_projection )
    {
    case GEOM_MAP_PROJECTION_HAMMER_AITOFF :
    case GEOM_MAP_PROJECTION_AITOFF :
    case GEOM_MAP_PROJECTION_PARABOLIC :
      ypos_start = -57.0;
      break;
    case GEOM_MAP_PROJECTION_SINUSOIDAL :
      ypos_start = -43.0;
      break;
    default :
      return 1;
      break;
    }

    
    fprintf(pPlotfile, "\
evertex_file_dg = \"< gawk 'NF==0 || ($1~/^#/ && $%d > 0) || ($1!~/^#/ && $%d > 0) {print $0}' %s\"\n\
evertex_file_bb = \"< gawk 'NF==0 || ($1~/^#/ && $%d > 0) || ($1!~/^#/ && $%d > 0) {print $0}' %s\"\n\n",
            head_col_dg, col_dg, FIELD_VERTICES_EXTENDED_FILENAME,
            head_col_bb, col_bb, FIELD_VERTICES_EXTENDED_FILENAME);
 
  //plot a key in the bottom left corner - eight steps should be ok
    fprintf(pPlotfile, "set label 1 \"Tiles Per Field\" at 2,%g\n",ypos_start+3.0);
    fprintf(pPlotfile, "set label 2 \"Dark\" at 2,%g\n",ypos_start);
    fprintf(pPlotfile, "set label 3 \"Bright\" at 16,%g\n",ypos_start);
    //first do the dg ones
    ypos = ypos_start; 
    i_last = -1;
    for ( i = 0; i <= max_steps; i ++)
    {
      int i_dg = (int) ceil((double) i*MAX(max_tiles_dg,1)/(double)max_steps);
      if ( i_dg == i_last ) continue;
      i_last = i_dg; 
      ypos -= 3.0;
      fprintf(pPlotfile, "set label  %3d \"%s%d\" at  6.5,%g right font \",12\"\n", i+100, (i==max_steps?"{/Symbol \\263}":""), i_dg, ypos); 
      fprintf(pPlotfile, "set object %3d rectangle at 10.0,%g size 5,2.0 fs transparent solid %5g fc rgb rgb_dg%s lw 0\n",
              i+100, ypos, (i==0?0.3:colour_density_floor+colour_density_dg*((float)i_dg)), (i==0?"0":""));
    }

    //now do the bb ones
    i_last = -1;
    ypos = ypos_start; 
    for ( i = 0; i <= max_steps; i ++)
    {
      int i_bb = (int) ceil((double)i*MAX(max_tiles_bb,1)/(double)max_steps);
      if ( i_bb == i_last ) continue;
      i_last = i_bb; 
      ypos -= 3.0;
      fprintf(pPlotfile, "set label  %3d \"%s%d\" at 22,%g font \",12\"\n", i+200, (i==max_steps?"{/Symbol \\263}":""), i_bb, ypos); 
      fprintf(pPlotfile, "set object %3d rectangle at 18.0,%g size 5,2.0 fs transparent solid %5g fc rgb rgb_bb%s lw 0\n",
              i+200, ypos, (i==0?0.3:0.05+colour_density_bb*((float)i_bb)), (i==0?"0":""));
    }
    
    fprintf(pPlotfile, "\n\nplot [][] \\\n\
        in_area_evertex_file_dg using (x($1,$2)) : ($%d == 0?y($1,$2):1/0) with filledcurves closed fs transparent solid 0.3 noborder lc rgb rgb_dg0 not,\\\n\
        in_area_evertex_file_bb using (x($1,$2)) : ($%d == 0?y($1,$2):1/0) with filledcurves closed fs transparent solid 0.3 noborder lc rgb rgb_bb0 not,\\\n", col_dg, col_bb);

    for ( i=1;i<MAX_TILES_PER_FIELD+1;i++)
    {
      if (phisto_dg[i] > 0 )
        fprintf(pPlotfile,"        evertex_file_dg    using (x($1,$2)) : ($%d == %d?y($1,$2):1/0) with filledcurves closed fs transparent solid %5g noborder lc rgb rgb_dg not,\\\n",
                col_dg, i,colour_density_floor+colour_density_dg*((float)i));
      if (phisto_bb[i] > 0 )
        fprintf(pPlotfile,"        evertex_file_bb    using (x($1,$2)) : ($%d == %d?y($1,$2):1/0) with filledcurves closed fs transparent solid %5g noborder lc rgb rgb_bb not,\\\n",
                col_bb, i,colour_density_floor+colour_density_bb*((float)i));
    }
     
    fprintf(pPlotfile,"\
        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 1 lw 2 not\n\n\n\n",
            pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
            pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
            hard_dec_max,hard_dec_max,
            hard_dec_min,hard_dec_min);

    ////////////////////////////////////////////////////////////////////
    //now plot the bb and dg data separately
    fprintf(pPlotfile, "unset title\n\
set multiplot\n\
set size 0.98,0.49\n\
\n");
    ////dg first
    fprintf(pPlotfile, "unset label\n\
unset object\n\
set label 1000 str_title at screen 0.5,0.985 centre font \",16\"\n\
set label 2 \"Dark/Grey time\" at graph 0.99,0.03 right font \",24\"\n\
%s\n\
set label 1 \"Tiles Per Field\" at 2,-56\n\
set origin 0.01,0.47\n", OPSIM_BRANDING_PLOTSTRING_SCREEN);
    if ( geom_write_long_lat_tics (pPlotfile, (const char*) "#000000")) return 1;
    if ( geom_write_mini_compass (pPlotfile,(BOOL) pSurvey->map_left_is_east)) return 1;

    
    i_last = -1;
    ypos = ypos_start; 
    for ( i = 0; i <= max_steps; i ++)
    {
      int i_dg = (int) ceil((double) i*MAX(max_tiles_dg,1)/(double)max_steps);
      if ( i_dg == i_last ) continue;
      i_last = i_dg; 
      ypos -= 3.0;
      fprintf(pPlotfile, "set label  %3d \"%s%d\" at 6.5,%g right font \",12\"\n", i+100, (i==max_steps?"{/Symbol \\263}":""), i_dg, ypos); 
      fprintf(pPlotfile, "set object %3d rectangle at 10.0,%g size 5,2.0 fs transparent solid %5g fc rgb rgb_dg%s lw 0\n",
              i+100, ypos, (i==0?0.3:colour_density_floor+colour_density_dg*((float)i_dg)), (i==0?"0":""));
    }
    fprintf(pPlotfile, "\n\nplot [][] \\\n\
        in_area_evertex_file_dg using (x($1,$2)) : ($%d == 0?y($1,$2):1/0) with filledcurves closed fs transparent solid 0.3 noborder lc rgb rgb_dg0 not,\\\n", col_dg);
    for ( i=1;i<MAX_TILES_PER_FIELD+1;i++)
    {
      if (phisto_dg[i] > 0 )
        fprintf(pPlotfile,"        evertex_file_dg    using (x($1,$2)) : ($%d == %d?y($1,$2):1/0) with filledcurves closed fs transparent solid %5g noborder lc rgb rgb_dg not,\\\n",
                col_dg, i,colour_density_floor+colour_density_dg*((float)i));
    }
    fprintf(pPlotfile,"\
        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 1 lw 2 not\n\n\n\n",
            pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
            pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
            hard_dec_max,hard_dec_max,
            hard_dec_min,hard_dec_min);

    ////bb next
    fprintf(pPlotfile, "\n\n\
unset label\n\
unset object\n\
set label 1 \"Tiles Per Field\" at 2,-56\n\
set label 2 \"Bright time\" at graph 0.99,0.03 right font \",24\"\n\
set origin 0.01,0.01\n");
    i_last = -1;
    ypos = ypos_start; 
    if ( geom_write_long_lat_tics (pPlotfile, (const char*) "#000000")) return 1;
    if ( geom_write_mini_compass (pPlotfile,(BOOL) pSurvey->map_left_is_east)) return 1;
    for ( i = 0; i <= max_steps; i ++)
    {
      int i_bb = (int) ceil((double) i*MAX(max_tiles_bb,1)/(double)max_steps);
      if ( i_bb == i_last ) continue;
      i_last = i_bb; 
      ypos -= 3.0;
      fprintf(pPlotfile, "set label  %3d \"%s%d\" at  6.5,%g right font \",12\"\n", i+100, (i==max_steps?"{/Symbol \\263}":""), i_bb, ypos); 
      fprintf(pPlotfile, "set object %3d rectangle at 10.0,%g size 5,2.0 fs transparent solid %5g fc rgb rgb_bb%s lw 0\n",
              i+100, ypos, (i==0?0.3:colour_density_floor+colour_density_bb*((float)i_bb)), (i==0?"0":""));
    }
    fprintf(pPlotfile, "\n\nplot [][] \\\n\
        in_area_evertex_file_bb using (x($1,$2)) : ($%d == 0?y($1,$2):1/0) with filledcurves closed fs transparent solid 0.3 noborder lc rgb rgb_bb0 not,\\\n", col_bb);
    for ( i=1;i<MAX_TILES_PER_FIELD+1;i++)
    {
      if (phisto_bb[i] > 0 )
        fprintf(pPlotfile,"        evertex_file_bb    using (x($1,$2)) : ($%d == %d?y($1,$2):1/0) with filledcurves closed fs transparent solid %5g noborder lc rgb rgb_bb not,\\\n",
                col_bb, i,colour_density_floor+colour_density_bb*((float)i));
    }
    fprintf(pPlotfile,
            "        infile_grid_eq          using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 not,\\\n\
        infile_grid_gal         using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 0.5 not,\\\n\
        infile_grid_gal_l0_180  using (x($1,$2)) : (y($1,$2)) with lines lt -1 lc 9 lw 1.0 not,\\\n\
        notable_locations_pnts  using (x($2,$3)) : (y($2,$3)) with points pt 1 ps 1 lt -1 lw 2 not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) with points pt 6 ps 2 lt -1 lw 2 not,\\\n\
        notable_locations_pnts  using (x($2,$3+2.)) : (y($2,$3+2.)) : 1 with labels font \",9\" not,\\\n\
        notable_locations_gals  using (x($2,$3)) : (y($2,$3)) : 1 with labels font \",9\" not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 3 lw 4 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 1 lw 2 not,\\\n\
        infile_boundaries       using (x($1,%g)) : (y($1,%g)) with lines lc -1 dt 1 lw 2 not\n\n\n\n",
            pSurvey->tiling_dec_max,pSurvey->tiling_dec_max,
            pSurvey->tiling_dec_min,pSurvey->tiling_dec_min,
            hard_dec_max,hard_dec_max,
            hard_dec_min,hard_dec_min);
    
    fprintf(pPlotfile, "unset multiplot\n\n\n");

  }


  //add a line to make thumbnail images
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, str_stem, str_stem);

  
  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  //  if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  fprintf(stdout, " done\n");  fflush(stdout);
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
int plot_predicted_tile_pressure_vs_RA (survey_struct *pSurvey, const char* str_stem)
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];

  if (  pSurvey == NULL) return 1;

  (void) snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, str_stem);

  fprintf(stdout, "%s Making plot of predicted tiling pressure vs RA: %s -> %s/%s.pdf ... ",
          COMMENT_PREFIX, strPlotfile, PLOTFILES_SUBDIRECTORY, str_stem);
  fflush(stdout);
  if ((pPlotfile = fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the plotfile file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }


  if ( util_write_cd_line_to_plotfile (pPlotfile, PLOTFILES_SUBDIRECTORY, ""))  return 1;

  fprintf(pPlotfile, "reset\n\
set angles degrees\n\
infile1     = \"%s\"\n\
infile2     = \"%s\"\n\
set terminal pdfcairo enhanced colour font \"Times,16\" size 29.7cm,21.0cm\n\
set out \"%s.pdf\"\n\n",
          TILING_STATS_VS_RA_FILENAME,
          WASTED_TILES_VS_RA_FILENAME,
          str_stem);

    fprintf(pPlotfile, "\n\
set format y \"%%4g\"\n\
set xtics 0,30,360\n\
set mxtics 2\n\
set xlabel \"RA(deg)\"\n\
set ylabel \"Number of %g|%g min tiles (per {/Symbol D}RA=%gdeg bin)\"\n\
set size 0.90,0.45\n\
set label 1 \"\" at graph 0.99,0.94 right font \",30\"\n\
origin_x1 = 0.05\n\
origin_y1 = 0.47\n\
origin_y2 = 0.02\n\
\n",
            pSurvey->pmoon_bb->Minutes_net_per_tile,
            pSurvey->pmoon_dg->Minutes_net_per_tile,
            TILING_RA_BIN_WIDTH);
    
  //-------------------------------------------//
    fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_SCREEN);

    fprintf(pPlotfile, "\
%s\n\
set multiplot title \"\\nTile supply and Tile demand\" font \",24\"\n\
set key bottom right Left reverse samplen 2 \n\
set label 1 \"Tile Supply\" \n\
set origin origin_x1,origin_y1\n\
plot [360:0][0:*]\\\n\
     infile1 using 2 :  5 with histeps lc 3 lw 3 t \"Supply Bright\",\\\n\
     infile1 using 2 :  6 with histeps lc 1 lw 3 t \"Supply Dark/Grey\",\\\n\
     infile1 using 2 :  7 with lines   lc 3 lw 1 dt 2 t \"Supply Bright (smoothed)\",\\\n\
     infile1 using 2 :  8 with lines   lc 1 lw 1 dt 2 t \"Supply Dark/Grey (smoothed)\"\n\
\n\
%s\n\
set label 1 \"Tile Demand\" \n\
set origin origin_x1,origin_y2\n\
plot [360:0][0:*]\\\n\
     infile1 using 2 : 17 with histeps lc 3 lw 3 t \"Demand Bright\",\\\n\
     infile1 using 2 : 18 with histeps lc 1 lw 3 t \"Demand Dark/Grey\",\\\n\
     infile1 using 2 : 19 with lines   lc 3 lw 1 dt 2 t \"Demand Bright (smoothed)\",\\\n\
     infile1 using 2 : 20 with lines   lc 1 lw 1 dt 2 t \"Demand Dark/Grey (smoothed)\"\n\
\n\
unset multiplot\n",
            OPSIM_BRANDING_PLOTSTRING_SCREEN, OPSIM_BRANDING_PLOTSTRING_UNSET);
  //-------------------------------------------//


  //-------------------------------------------//
    fprintf(pPlotfile, "\
%s\n\
set multiplot title \"\\nTile budget and Tiling plan\" font \",24\"\n\
set key bottom right Left reverse samplen 2 \n\
set label 1 \"Tile Budget\" \n\
set origin origin_x1,origin_y1\n\
plot [360:0][0:*]\\\n\
     (0.0) with lines lc -1 dt 2 not,\\\n\
     infile1 using 2 :  7 with lines   lc 3 lw 1 dt 2 t \"Supply Bright (smoothed)\",\\\n\
     infile1 using 2 :  8 with lines   lc 1 lw 1 dt 2 t \"Supply Dark/Grey (smoothed)\",\\\n\
     infile1 using 2 :  9 with histeps lc 3 lw 3 t \"Budget Bright\",\\\n\
     infile1 using 2 : 10 with histeps lc 1 lw 3 t \"Budget Dark/Grey\"\n\
\n\
%s\n\
set label 1 \"Tiling Plan\" \n\
set origin origin_x1,origin_y2\n\
plot [360:0][0:*]\\\n\
     (0.0) with lines lc -1 dt 2 not,\\\n\
     infile1 using 2 :  7        with lines   lc 3 lw 1 dt 2 t \"Supply Bright (smoothed)\",\\\n\
     infile1 using 2 :  8        with lines   lc 1 lw 1 dt 2 t \"Supply Dark/Grey (smoothed)\",\\\n\
     infile1 using 2 :  ($9-$11) with histeps lc 3 lw 3 t \"Planned Bright\",\\\n\
     infile1 using 2 : ($10-$12) with histeps lc 1 lw 3 t \"Planned Dark/Grey\"\n\
\n\
unset multiplot\n",
            OPSIM_BRANDING_PLOTSTRING_SCREEN, OPSIM_BRANDING_PLOTSTRING_UNSET);
  //-------------------------------------------//



  //-------------------------------------------//
    fprintf(pPlotfile, "\
%s\n\
set multiplot title \"\\nTile Surplus/Deficit (Supply - Plan)\" font \",24\"\n\
set key bottom right Left reverse samplen 2 \n\
set label 1 \"Absolute\" \n\
set label 11 \"Surplus\" at graph 0.99, first  20 right font \",16\"\n\
set label 12 \"Deficit\" at graph 0.99, first -20 right font \",16\"\n\
set origin origin_x1,origin_y1\n\
plot [360:0][*:*]\\\n\
     (0.0) with lines lc -1 dt 2 not,\\\n\
     infile1 using 2 :  7           with lines   lc 3 lw 1 dt 2 t \"Supply Bright (smoothed)\",\\\n\
     infile1 using 2 :  8           with lines   lc 1 lw 1 dt 2 t \"Supply Dark/Grey (smoothed)\",\\\n\
     infile1 using 2 : ($7- $9+$11) with histeps lc 3 lw 3 t \"Surplus/Deficit Bright\",\\\n\
     infile1 using 2 : ($8-$10+$12) with histeps lc 1 lw 3 t \"Surplus/Deficit Dark/Grey\"\n\
\n\
%s\n\
unset key\n\
set ytics -10,0.2,1\n\
set label 1 \"Fractional\" \n\
set label 11 \"Surplus\" at graph 0.99, first  0.1 right font \",16\"\n\
set label 12 \"Deficit\" at graph 0.99, first -0.1 right font \",16\"\n\
set ylabel \"Fraction of tiles (bin)^{-1}\"\n\
set origin origin_x1,origin_y2\n\
plot [360:0][*:1]\\\n\
     (0.0) with lines lc -1 dt 2 not,\\\n\
     infile1 using 2 :  7                with lines   lc 3 lw 1 dt 2 t \"Supply Bright (smoothed)\",\\\n\
     infile1 using 2 :  8                with lines   lc 1 lw 1 dt 2 t \"Supply Dark/Grey (smoothed)\",\\\n\
     infile1 using 2 : (($7- $9+$11)/$7) with histeps lc 3 lw 3 t \"Surplus/Deficit Bright\",\\\n\
     infile1 using 2 : (($8-$10+$12)/$8) with histeps lc 1 lw 3 t \"Surplus/Deficit Dark/Grey\"\n\
\n\
unset multiplot\n",
            OPSIM_BRANDING_PLOTSTRING_SCREEN, OPSIM_BRANDING_PLOTSTRING_UNSET);
  //-------------------------------------------//



    
  //add a line to make thumbnail images
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, str_stem, str_stem);

  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  //if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  fprintf(stdout, " done\n");  fflush(stdout);
  return 0;

}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
int plot_tiles_used_and_wasted_vs_RA (survey_struct *pSurvey,  const char* str_stem)
{
  FILE *pPlotfile = NULL;
  char strPlotfile[STR_MAX];

  if ( pSurvey == NULL) return 1;

  (void) snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PLOTFILES_SUBDIRECTORY, str_stem);

  fprintf(stdout, "%s Making plot tiles used and wasted during the survey: %s -> %s/%s.pdf ... ",
          COMMENT_PREFIX, strPlotfile, PLOTFILES_SUBDIRECTORY, str_stem);
  fflush(stdout);
  if ((pPlotfile = fopen(strPlotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the plotfile file: %s\n",
            ERROR_PREFIX, strPlotfile);
    return 1;
  }


  if ( util_write_cd_line_to_plotfile (pPlotfile, PLOTFILES_SUBDIRECTORY, ""))  return 1;

  fprintf(pPlotfile, "reset\n\
set angles degrees\n\
infile1     = \"%s\"\n\
infile2     = \"%s\"\n\
set terminal pdfcairo enhanced colour font \"Times,16\" size 29.7cm,21.0cm\n\
set out \"%s.pdf\"\n\n",
          TILING_STATS_VS_RA_FILENAME,
          WASTED_TILES_VS_RA_FILENAME,
          str_stem);

    fprintf(pPlotfile, "\n\
set format y \"%%4g\"\n\
set xtics 0,30,360\n\
set mxtics 2\n\
set ylabel \"Number of %g|%g min tiles (per {/Symbol D}RA=%gdeg bin)\"\n\
set size 0.90,0.47\n\
set label 1 \"\" at graph 0.99,0.94 right font \",30\"\n\
origin_x1 = 0.05\n\
origin_y1 = 0.47\n\
origin_y2 = 0.02\n\
\n",
            pSurvey->pmoon_bb->Minutes_net_per_tile,
            pSurvey->pmoon_dg->Minutes_net_per_tile,
            TILING_RA_BIN_WIDTH);
    
  //-------------------------------------------//
    fprintf(pPlotfile, "%s\n", OPSIM_BRANDING_PLOTSTRING_SCREEN);



  //-------------------------------------------//
    fprintf(pPlotfile, "\
%s\n\
set multiplot title \"\\nTile Supply/Usage/Wastage\" font \",24\"\n\
set key bottom left Left reverse samplen 2 maxrows 2 \n\
set label 1 \"Tiles Used\" \n\
set ytics auto\n\
f(x) = (x/15.0)\n\
g(fx) = (fx*15.0)\n\
set link x2 via f(x) inverse g(x) \n\
set x2tics 0,3,24 offset 0.0,-0.4\n\
set mx2tics 3\n\
set format x2 \"%%g^{/*0.75 H}\"\n\
set xlabel \"\"\n\
unset label 11 \n\
unset label 12 \n\
set ylabel \"Number of %g min tiles (per {/Symbol D}RA=%gdeg bin)\"\n\
set origin origin_x1,origin_y1\n\
plot [360:0][0:*]\\\n\
     (0.0) with lines lc -1 dt 2 not,\\\n\
     infile1 using 2 :  7           with lines   lc 3 lw 1 dt 2 t \"Supply (smoothed) Bright\"   ,\\\n\
     infile1 using 2 :  8           with lines   lc 1 lw 1 dt 2 t \"Supply (smoothed) Dark/Grey\",\\\n\
     infile2 using 2 :  5           with histeps lc 3 lw 3      t \"Used Bright (zenith RA)\"    ,\\\n\
     infile2 using 2 :  6           with histeps lc 1 lw 3      t \"Used Dark/Grey (zenith RA)\"    \n\
\n\
%s\n\
set xlabel \"R.A. (deg)\"\n\
set key top left Left\n\
set label 1 \"Tiles Wasted\" \n\
set origin origin_x1,origin_y2\n\
plot [360:0][0:*]\\\n\
     infile1 using 2 :  7           with lines   lc 3 lw 1 dt 2 t \"Supply (smoothed) Bright\"   ,\\\n\
     infile1 using 2 :  8           with lines   lc 1 lw 1 dt 2 t \"Supply (smoothed) Dark/Grey\",\\\n\
     infile2 using 2 :  9           with histeps lc 3 lw 3      t \"Wasted Bright (zenith RA)\"  ,\\\n\
     infile2 using 2 : 10           with histeps lc 1 lw 3      t \"Wasted Dark/Grey (zenith RA)\"  \n\
\n\
unset multiplot\n",
            OPSIM_BRANDING_PLOTSTRING_SCREEN,
            pSurvey->pmoon_dg->Minutes_net_per_tile,
            TILING_RA_BIN_WIDTH,
            OPSIM_BRANDING_PLOTSTRING_UNSET);
  //-------------------------------------------//



    
  //add a line to make thumbnail images
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -trim -bordercolor White -border 1x1 -resize x%d '%s.pdf[0]' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, str_stem, str_stem);

  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;

  //if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile, (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE))) return 1;
  if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;

  fprintf(stdout, " done\n");  fflush(stdout);
  return 0;
}



    
//----------------------------------------------------------------------------------------
int write_progress_plotfile (survey_struct *pSurvey, const char* str_plotdirname, const char* str_plotfilename )
{
  int i;
  FILE *pPlotfile = NULL;
  int max_tiles_dg, max_tiles_bb;
  if ( strlen(str_plotfilename) == 0 ) return 1;

  fprintf(stdout, "%s Making plotfile of survey progress: %s\n",
          COMMENT_PREFIX, str_plotfilename);
  fflush(stdout);
  
  if ((pPlotfile = fopen(str_plotfilename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the plotfile file: %s\n",
            ERROR_PREFIX, str_plotfilename);
    return 1;
  }


  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) str_plotdirname, ""))  return 1;

  fprintf(pPlotfile, "print \"gnuplot: Now making progress report images+video\"\n\
reset\n\
set size 1.0,0.7\n\
set origin 0.0,0.15\n\
set key bottom left Left reverse samplen 2\n");
  
  if ( geom_write_gnuplot_map_projection_functions ((FILE*) pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                    (int) pSurvey->map_projection, (BOOL) pSurvey->map_left_is_east,
                                                    (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 11,
                                                    (const char*) "#909090")) return 1;


  max_tiles_dg = CLIP(MIN(2*pSurvey->pmoon_dg->max_num_tiles_per_field_todo,
                            pSurvey->pmoon_dg->max_num_tiles_per_field_done), 12,MAX_TILES_PER_FIELD);
  max_tiles_bb = CLIP(MIN(2*pSurvey->pmoon_bb->max_num_tiles_per_field_todo,
                            pSurvey->pmoon_bb->max_num_tiles_per_field_done), 12,MAX_TILES_PER_FIELD);
    
  fprintf(pPlotfile, "\n\
infile_boundaries = \"< gawk 'BEGIN{for(i=0;i<=360;i++){print i}; exit}'\"\n\
in_area_vertex_file_dg  = \"< gawk 'NF==0{print $0}  // {flag=strtonum($3); if(and(flag,393216)){print $0}}' ../%s/%s\"\n\
in_area_vertex_file_bb  = \"< gawk 'NF==0{print $0}  // {flag=strtonum($3); if(and(flag,65536)){print $0}}' ../%s/%s\"\n\
\n\
set terminal pngcairo enhanced colour font \"Times,14\" size 1024,800\n\
rgb_bb = \"#0000bb\"\n\
rgb_dg = \"#dd4000\"\n\
rgb_bb0  = \"#00ffff\"\n\
rgb_dg0  = \"#ffff00\"\n\
dg_depth = 0.9/%d.0\n\
bb_depth = 0.9/%d.0\n",
          PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_FILENAME,
          PLOTFILES_SUBDIRECTORY, FIELD_VERTICES_FILENAME,
          max_tiles_dg, max_tiles_bb);

  fprintf(pPlotfile, "dg00 = 0.5\n\
bb00 = 0.5\n" ); 
  for(i=1;i<=max_tiles_dg;i++)  fprintf(pPlotfile, "dg%02d = dg_depth*(1. + %d.)\n", i, i); 
  for(i=1;i<=max_tiles_bb;i++)  fprintf(pPlotfile, "bb%02d = bb_depth*(1. + %d.)\n", i, i); 

  fprintf(pPlotfile, "frame = 1\n\
nframes=%d\n\
nights_per_frame=%d\n\
do for [night_num=0:nframes*nights_per_frame:nights_per_frame] {\n\
  str_night_num = sprintf(\"%%04d\", night_num)\n\
  if ( frame >= nframes) {str_night_num = \"final\";}\n\
  str_system= sprintf(\"file progress_report_night_%%s.txt.gz | grep ERROR\", str_night_num)\n\
  isexist=system (str_system);\n\
  if(strlen(isexist) == 0 ){\n\
    vertex_file_dg  = sprintf(\"< zcat -f progress_report_night_%%s.txt.gz | gawk 'NF==0 || ($1~/^#/ && $30 > 0) || ($1!~/^#/ && $11 > 0) {print $0} END {for(i=0;i<=%d;i++){print -1000,-1000,0,0,0,0,i,0,0,0,i;print _}}'\", str_night_num)\n\
    vertex_file_bb  = sprintf(\"< zcat -f progress_report_night_%%s.txt.gz | gawk 'NF==0 || ($1~/^#/ && $28 > 0) || ($1!~/^#/ && $10 > 0) {print $0} END {for(i=0;i<=%d;i++){print -1000,-1000,0,0,0,i,0,0,0,i,0;print _}}'\", str_night_num)\n\
    str_title = sprintf(\"Survey Progress after night number: %%s\", str_night_num)\n\
    set title str_title\n\
    str_out = sprintf(\"progress_report_frame_%%03d.png\",frame)\n\
    set out str_out\n\
    print \"Working on night \",str_night_num,\" output plot at: \",str_out\n\
    plot [][] \\\n",
          (int) ((pSurvey->JD_stop - pSurvey->JD_start)/((float) pSurvey->progress_report_cadence))+1,
          (int) pSurvey->progress_report_cadence,
          max_tiles_dg, max_tiles_bb);

  for(i=0;i<=max_tiles_dg;i++)
  {
    fprintf(pPlotfile, "        vertex_file_dg         using ($11==%2d?x($1,$2):1/0) : (y($1,$2)) with filledcurves closed fs transparent solid dg%02d noborder lc rgb rgb_dg%s not,\\\n", i, i, (i==0?"0":""));
  }
  for(i=0;i<=max_tiles_bb;i++)
  {
    fprintf(pPlotfile, "        vertex_file_bb         using ($10==%2d?x($1,$2):1/0) : (y($1,$2)) with filledcurves closed fs transparent solid bb%02d noborder lc rgb rgb_bb%s not,\\\n", i, i, (i==0?"0":""));
  }
  
  //put outlines around the files that have been completed (wrt the requested number of tiles)
  //consider 
  fprintf(pPlotfile, "        vertex_file_dg         using ($11>=$5?x($1,$2):1/0) : (y($1,$2)) with lines lc -1 lw 0.5 not, \\\n\
        vertex_file_bb         using (($10+$11)>=($4+$5)?x($1,$2):1/0) : (y($1,$2)) with lines lc -1 lw 0.5 not,\\\n");
  
  fprintf(pPlotfile, "        infile_grid_eq         using (x($3,$4)) : (y($3,$4)) with lines lt -1 lw 0.5 lc -1 not,\\\n\
        infile_boundaries      using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 3 lw 2 not,\\\n\
        infile_boundaries      using (x($1,%g)) : (y($1,%g)) with lines lc -1 lt 3 lw 2 not\n\
  }\n\
  frame = frame + 1\n\
}\n\
\n\
#add a few extra copies of the last frame to the list\n\
str_system = sprintf(\"ln -s progress_report_frame_%%03d.png progress_report_frame_final.png\", frame-1)\n\
system(str_system)\n\
do for [frame1=frame:frame+15] {\n\
  str_system = sprintf(\"ln -s progress_report_frame_final.png progress_report_frame_%%03d.png\", frame1)\n\
  system(str_system)\n\
}\n\
#make it into a video!\n\
!ffmpeg -y -r 5 -qscale 3 -i progress_report_frame_%%03d.png  progress_report.mp4\n",
          pSurvey->tiling_dec_max,pSurvey->tiling_dec_max, pSurvey->tiling_dec_min,pSurvey->tiling_dec_min);

    //add a line to make thumbnail images
  fprintf(pPlotfile, "\n\nset output\n\
str_system = sprintf(\"convert -resize x%d '%s.png' %s_thumb.png\")\n\
system(str_system)\n\n",
          THUMBNAIL_HEIGHT_PIXELS, "progress_report_frame_final",  "progress_report_frame_final");

  
  //        infile_grid_gal        using (x($1,$2)) : (y($1,$2)) with lines lt -1 lw 0.5 lc 9  not
  if ( pPlotfile) fclose(pPlotfile);
  pPlotfile=NULL;
  return 0;
}



int write_gnuplot_script_for_field_ranking ( const char* str_plotfile, survey_struct *pSurvey )
{
  FILE *pPlotfile = NULL;
  if ( pSurvey == NULL ) return 1;
  
  if ((pPlotfile = fopen(str_plotfile, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the plotfile file: %s\n",
            ERROR_PREFIX, str_plotfile);
    return 1;
  }
  if ( util_write_cd_line_to_plotfile (pPlotfile, (const char*) FIELD_RANKING_SUBDIRECTORY, ""))  return 1;

  /* fprintf(pPlotfile, "infile_grid_eq  = \"../%s/%s\"\n\
infile_grid_gal = \"../%s/%s\"\n",
          PLOTFILES_SUBDIRECTORY, GEOM_COORDINATE_GRID_FILENAME_EQUATORIAL,
          PLOTFILES_SUBDIRECTORY, GEOM_COORDINATE_GRID_FILENAME_GALACTIC); */

  if (geom_write_gnuplot_map_projection_functions ((FILE*) pPlotfile, (const char*) PLOTFILES_SUBDIRECTORY,
                                                   (int) pSurvey->map_projection, (BOOL) pSurvey->map_left_is_east,
                                                   (double) pSurvey->plot_dec_min, (double) pSurvey->plot_dec_max, 1,
                                                   (const char*) "#000000")) return 1;

  fprintf(pPlotfile, "reset\n\
set terminal wxt size 1200,1100\n\
set angles degrees\n\
#moon_phase = \"dg\"\n\
#tile_num = 1\n\
rand_seed = `date '+%%N'| gawk '{printf(\"%%d\",$0)}'`\n\
seed=rand(rand_seed)\n\
i = 1\n\
while (i<=100) {\n\
  moon_phase_code = (rand(0)<0.5?1:2)\n\
  moon_phase = (moon_phase_code==1?\"dg\":\"bb\")\n\
  ntiles = (moon_phase_code==1?%d:%d)\n\
  tile_num = int(rand(0)*ntiles)\n\
  str_infile = sprintf(\"tile_%%s_%%06d_ranking.txt\", moon_phase, tile_num)\n\
  str_system= sprintf(\"file %%s | grep ERROR\", str_infile)\n\
  isexist = system(str_system)\n\
  i = i + 1\n\
  if(strlen(isexist) == 0 ) {i=1e30;}\n\
}\n\
\n\
print \"plotting-> Tile number: \", tile_num, \" MoonPhase: \", moon_phase\n\
str_title  = sprintf(\"Field Ranking Process - Tile %%06d in moon phase %%s\", tile_num, moon_phase)\n\
#this is all the fields\n\
infile      = sprintf(\"< gawk '$1~/FieldRanking/ && $2!~/MEAN/' %%s\", str_infile)\n\
infile_mean = sprintf(\"< gawk '$1~/FieldRanking/ && $2~/MEAN/' %%s\", str_infile)\n\
#this is just the top-ranked field\n\
infile1     = sprintf(\"< gawk '$1~/FieldRanking/ && $2 == 1' %%s\", str_infile)\n\
#this is the last field\n\
infile_last = sprintf(\"< gawk '$1~/FieldRanking/ && $16~/TRUE/' %%s\", str_infile)\n\
infile_report = sprintf(\"< gawk -v tile=%%d -v moon=%%s 'BEGIN{n=-1} $1!~/^#/ && $11==moon {n++; if (n==tile){print $0;exit}}'  ../tile_reports.txt\", tile_num, moon_phase)\n\
\n\
stats infile using  3 name 'STATS_weight' nooutput\n\
stats infile using  7 name 'STATS_Z'  nooutput\n\
stats infile using  8 name 'STATS_AM' nooutput\n\
stats infile using  9 name 'STATS_MD' nooutput\n\
stats infile using 10 name 'STATS_SB' nooutput\n\
stats infile using 11 name 'STATS_VS' nooutput\n\
stats infile using 12 name 'STATS_FD' nooutput\n\
stats infile using 13 name 'STATS_PS' nooutput\n\
\n\
inf=1e30\n\
clip(x,a,b) = (x<a?a:(x>b?b:x))\n\
min(a,b) = (a<b?a:b)\n\
max(a,b) = (a>b?a:b)\n\
f_Z(x)   =  STATS_weight_mean*((cos(STATS_Z_mean)/cos(x))**1.25) \n\
f_AM(x)  =  STATS_weight_mean*((x/STATS_AM_mean)**1.25)\n\
f_MD(x)  =  STATS_weight_mean*(clip(STATS_MD_mean,1,90)/clip(x,1,90))\n\
f_SB(x)  =  STATS_weight_mean*(10**(-0.4*(x - STATS_SB_mean)))\n\
f_VS(x)  =  STATS_weight_mean*(clip(x,1,inf)/clip(STATS_VS_mean,1,inf)) \n\
FD_exponent = %g\n\
FD_weight_when_complete = max(0.9**FD_exponent,0.1**FD_exponent)\n\
f_FD(x)  =  STATS_weight_mean*(x<1.0 ? (max(0.1,1.0-x)**FD_exponent) : 2.0*FD_weight_when_complete*(x**2))\n\
f_PS(x)  =  STATS_weight_mean*(clip(STATS_PS_mean,0.1,inf)/clip(x,0.1,inf))\n\
\n\
set xlabel offset 0,0.3\n\
factor = 1./%g\n\
set multiplot title str_title\n\
unset logscale\n\
set cbrange [-log10(min(STATS_weight_min,0.1)):-log10(max(STATS_weight_max,10.0))]\n\
unset colorbox\n\
set key bottom right samplen 1\n\
set xlabel \"RA (deg)\" offset 0,0.5\n\
set ylabel \"Dec (deg)\" offset 1,0\n\
set format y \"%%+g\"\n\
set xtics 0,60,360\n\
set size 0.66,0.30\n\
set origin 0.0,0.66\n\
dec_min = %g\n\
dec_max = %g\n",
          MIN(pSurvey->pmoon_dg->num_tiles_todo,pSurvey->max_field_ranking_files),
          MIN(pSurvey->pmoon_bb->num_tiles_todo,pSurvey->max_field_ranking_files),
          pSurvey->field_weighting_complete_fields_exponent,
          pSurvey->field_weighting_last_field_bonus_factor,
          pSurvey->plot_dec_min,
          pSurvey->plot_dec_max);
  
  fprintf(pPlotfile, "plot [0:360][dec_min:dec_max]\\\n\
     infile_grid_eq  using 3 : 4 with lines lt -1 lc -1 lw 0.5 not,\\\n\
     infile_grid_gal using 1 : 2 with lines lt -1 lc 9  lw 0.5 not,\\\n\
     infile_grid_gal_l0_180  using 1 : 2 with lines lt -1 lc 9 lw 1.0 not,\\\n \
     infile      using  5 : 6 : (-log10($3)) with points pt 7 ps 0.50 lc palette not,\\\n\
     infile1     using  5 : 6 with points pt 6 ps 1.7 lw 2  lc 2 t \"Selected Field\",\\\n\
     infile_last using  5 : 6 with points pt 8 ps 2.0 lc 3 t \"Previous Field\",\\\n\
     infile_report using  8 :  9 with points pt 6 ps 3.0 lc -1 not,\\\n\
     infile_report using  8 :  9 : (sprintf(\"Z\")) with labels tc lt -1 not,\\\n\
     infile_report using 13 : 14 with points pt 7 ps 3.0 lc 9 not,\\\n\
     infile_report using 13 : 14 : (sprintf(\"M\")) with labels tc lt -1 not\n\
\n\
set format y \"%%g\"\n\
set xtics autofreq\n\
set logscale y\n\
set yrange [(min(STATS_weight_min,0.1)):(max(STATS_weight_max,10.0))]\n\
set ylabel \"Field Weight\" offset 3,0\n\
set xlabel \"Zenith distance (deg)\"\n\
set size 0.33,0.30\n\
set origin 0.66,0.66\n\
plot [0:*]\\\n\
     infile      using  7 : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using  7 : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using  7 : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using  7 : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using  7 : 3 with points pt 7 ps 1   lc 3 not,\\\n\
     f_Z(x) with lines lt 2 lc 3 not\n\
\n\
set xlabel \"Air Mass\"\n\
set size 0.33,0.33\n\
set origin 0.0,0.33\n\
plot [1:*]\\\n\
     infile      using  8 : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using  8 : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using  8 : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using  8 : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using  8 : 3 with points pt 7 ps 1   lc 3 not,\\\n\
     f_AM(x) with lines lt 1 lc 3 not\n\
\n\
set xlabel \"Moon dist (deg)\"\n\
set origin 0.33,0.33\n\
plot [0:180]\\\n\
     infile      using  9 : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using  9 : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using  9 : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using  9 : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using  9 : 3 with points pt 7 ps 1   lc 3 not,\\\n\
     f_MD(x) with lines lt 1 lc 3 not\n");

  fprintf(pPlotfile, "set xlabel \"relative Sky Brightness (mag)\"\n\
set xrange [(min(STATS_SB_min,0.0)):(max(STATS_SB_max*1.1,1.0))]\n\
set origin 0.66,0.33\n\
plot []\\\n\
     infile      using 10 : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using 10 : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using 10 : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using 10 : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using 10 : 3 with points pt 7 ps 1 lc 3 not,\\\n\
     f_SB(x) with lines lt 1 lc 3 not\n\
\n\
set xlabel \"Number of visibility windows (x10^3)\"\n\
set origin 0.00,0.00\n\
plot [0:*]\\\n\
     infile      using ($11/1000.) : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using ($11/1000.) : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using ($11/1000.) : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using ($11/1000.) : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using ($11/1000.) : 3 with points pt 7 ps 1 lc 3 not,\\\n\
     f_VS(x*1000.) with lines lt 1 lc 3 not\n\
\n\
set xlabel \"Fraction of request Done\"\n\
set xrange [(min(STATS_FD_min,0.0)):(max(STATS_FD_max*1.1,1.0))]\n\
set origin 0.33,0.00\n\
plot []\\\n\
     infile      using 12 : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using 12 : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using 12 : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using 12 : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using 12 : 3 with points pt 7 ps 1   lc 3 not,\\\n\
     f_FD(x) with lines lt 1 lc 3 not,\\\n\
     (x>1.0?STATS_weight_mean*2.0*FD_weight_when_complete:1/0) with lines lt 2 lc 3 not\n\
\n\
set xlabel \"Summed Priority of targets (x10^3)\"\n\
unset xrange\n\
set origin 0.66,0.00\n\
plot [*:*]\\\n\
     infile      using ($13/1000.) : 3 : (-log10($3)) with points pt 7 ps 0.35 lc palette not,\\\n\
     infile1     using ($13/1000.) : 3 with points pt 6 ps 1.7 lw 2  lc 2 not,\\\n\
     infile_last using ($13/1000.) : ($3/factor) : (0.0) : ($3*(1.-1./factor)) with vectors lt 1 lc 3 not,\
     infile_last using ($13/1000.) : 3 with points pt 8 ps 2.0 lc 3 not,\\\n\
     infile_mean using ($13/1000.) : 3 with points pt 7 ps 1   lc 3 not,\\\n\
     f_PS(x*1000.) with lines lt 1 lc 3 not\n\
\n\
unset multiplot\n\n");

 
  if (pPlotfile) fclose(pPlotfile);
  pPlotfile = NULL;
  
  return 0;
}




//----------------------------end-Misc-stats/plots---------------------------------------------//



//----------------------------object filtering/value functions-----------------------------------//
//a set of object tests that can be passed to functions that need to filter the objectlist
//return >0 if condition is met,
//return 0 if condition is not met
//return <0 if error
//the pQueryObj is the real object that is to be tested,
//the pMulti is a union that contains the numerical value of the condition

int objectfilter_true (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL ) return -1;
  return 0;
}
//filter on catalogue code
int objectfilter_cat_code_eq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (utiny) pQueryObj->pCat->code == (utiny) pMulti->utval ) return 0;
  return 1;
}
//filter on resolution
int objectfilter_resolution_eq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (utiny) pQueryObj->res == (utiny) pMulti->utval ) return 0;
  return 1;
}

//filter on TexpFracDone
int objectfilter_TexpFracDone_geq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (float) pQueryObj->TexpFracDone >= (float) pMulti->fval ) return 0;
  return 1;
}
int objectfilter_TexpFracDone_gt (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (float) pQueryObj->TexpFracDone > (float) pMulti->fval ) return 0;
  return 1;
}
int objectfilter_TexpFracDone_lt (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (float) pQueryObj->TexpFracDone < (float) pMulti->fval ) return 0;
  return 1;
}
int objectfilter_TexpFracDone_leq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (float) pQueryObj->TexpFracDone <= (float) pMulti->fval ) return 0;
  return 1;
}

//filter on Num fields
int objectfilter_num_fields_eq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (utiny) pQueryObj->num_fields == (utiny) pMulti->utval ) return 0;
  return 1;
}
int objectfilter_num_fields_leq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (utiny) pQueryObj->num_fields <= (utiny) pMulti->utval ) return 0;
  return 1;
}
int objectfilter_num_fields_geq (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL || pMulti  == NULL ) return -1;
  if ( (utiny) pQueryObj->num_fields >= (utiny) pMulti->utval ) return 0;
  return 1;
}
//filter on whether the required Texp is possible to achieve 
int objectfilter_treq_possible (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL ) return -1;
  if ( (float) pQueryObj->treq[MOON_PHASE_DARK] < (float) MAX_EXPOSURE_TIME  ) return 0;
  return 1;
}
int objectfilter_treq_impossible (object_struct *pQueryObj, multi_type *pMulti )
{
  if ( pQueryObj == NULL ) return -1;
  if ( (float) pQueryObj->treq[MOON_PHASE_DARK] >= (float) MAX_EXPOSURE_TIME  ) return 0;
  return 1;
}


///////////////////////////////////////////////////////////////////////
float objectvalue_count (object_struct *pQueryObj)
{
  if ( pQueryObj == NULL ) return NAN;
  return (float) 1.0;
}

///////////////////////////////////////////////////////////////////////
float objectvalue_treq_dark (object_struct *pQueryObj)
{
  if ( pQueryObj == NULL ) return NAN;
  return (float) (pQueryObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME ? pQueryObj->treq[MOON_PHASE_DARK] * MINS_TO_HOURS : NAN);
}
///////////////////////////////////////////////////////////////////////
float objectvalue_treq_grey (object_struct *pQueryObj)
{
  if ( pQueryObj == NULL ) return NAN;
  return (float) (pQueryObj->treq[MOON_PHASE_GREY] < MAX_EXPOSURE_TIME ? pQueryObj->treq[MOON_PHASE_GREY] * MINS_TO_HOURS : NAN);
}
///////////////////////////////////////////////////////////////////////
float objectvalue_treq_bright (object_struct *pQueryObj)
{
  if ( pQueryObj == NULL ) return NAN;
  return (float) (pQueryObj->treq[MOON_PHASE_BRIGHT] < MAX_EXPOSURE_TIME ? pQueryObj->treq[MOON_PHASE_BRIGHT] * MINS_TO_HOURS : NAN);
}

///////////////////////////////////////////////////////////////////////
//executed exposure time, dark time equivalent, hours
float objectvalue_texec_dark_equiv (object_struct *pQueryObj)
{
  if ( pQueryObj == NULL ) return NAN;
  return (float) (pQueryObj->treq[MOON_PHASE_DARK] < MAX_EXPOSURE_TIME ? pQueryObj->TexpFracDone * pQueryObj->treq[MOON_PHASE_DARK] * MINS_TO_HOURS : NAN);
}

///////////////////////////////////////////////////////////////////////
//----------------------end-object filtering/value functions-----------------------------------//
