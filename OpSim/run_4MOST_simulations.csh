#!/bin/tcsh 
#set echo


#set echo
#some basic checks
if ( "${HOST:r:r:r}" != "ds22" &&  "${HOST:r:r:r}" != "ds21" && "${HOST:r:r:r}" != "ds23") then
  echo "Unknown machine: ${HOST}"
  exit 1
endif

if ( "`uname -m`" != "x86_64" ) then
  echo "Unknown architechure : `uname -m`"
  exit 1
endif

echo '#############################################################################################'
echo "## run_4MOST_simulations.csh script started at `date`"
echo '#############################################################################################'


#prepare for and run a sequence of object assignments
#collate and organise the output
#make logs of progress and suchlike
set HOME_BASE =  ${HOME}/my_4most
set MAIN_BASE =  ${HOME}/4most
set CODE_BASE =  ${HOME}/my_4most/code
set SIM_BASE  = /home/tdwelly/4most/sim
set BASE      = /home/tdwelly/4most/sim
if ( ! -d $SIM_BASE ) mkdir $SIM_BASE
cd $SIM_BASE


#external, non standard programs that are required to support this 
foreach HELPER_PROG ( skycoor wcalc gawk skycalc ) 
  if ( ! -X $HELPER_PROG ) then
    echo "Error, the helper program, $HELPER_PROG, does not exist or is not in the path"
    exit 1
  endif
end





############################################################
#set all the external default parameter values - just in case the supplied parameter file does not have some necessary entries
set PAR_FILE_CSH = ".input_params_file_temp_${HOST}_$$.csh"
if ( -e $PAR_FILE_CSH ) rm $PAR_FILE_CSH
if ( -e ${HOME_BASE}/scripts/default_4FS_input_parameter_file.txt ) then
  gawk 'NF>0 {n=match($0,"#"); if(n==0){print $0}else if (n>1) {print substr($0,1,n-1)}}' ${HOME_BASE}/scripts/default_4FS_input_parameter_file.txt | gawk --field-separator='=' 'NF == 2 {gsub("\\.","_",$1);printf("set %-40s = %s\n", toupper($1),$2)}' > $PAR_FILE_CSH
  source $PAR_FILE_CSH
  rm $PAR_FILE_CSH
else
  exit 1
endif
############################################################


###########################################################
#set all the internal parameters to their default values 
set CLOBBER = "FALSE"
set DEBUG   = "FALSE"
set SKY_AREA_CODE  = A
set CATALOGUE_CODE = 1
set FIELD_INDEX_START = "-"
set FIELD_INDEX_STOP  = "-"
set PROCESS_HILO_SEPARATELY = "FALSE"
set ABSOLUTE_DEC_MIN = -70.0
set ABSOLUTE_DEC_MAX = +20.0 
####
set DO_SECTION_EXTRACT_GAL            = "FALSE"
set DO_SECTION_FIELD_MBR              = "FALSE"
set DO_SECTION_SUMMARISE              = "FALSE"
set DO_SECTION_COPY_TO_OUTDIRS        = "FALSE"
set DO_SECTION_MAKE_SPECTRA_DESC_FILE = "FALSE"

set DO_SECTION_EXTRACT_GAL            = "TRUE"
set DO_SECTION_FIELD_MBR              = "TRUE"
set DO_SECTION_SUMMARISE              = "TRUE"
#set DO_SECTION_COPY_TO_OUTDIRS        = "TRUE"
#set DO_SECTION_MAKE_SPECTRA_DESC_FILE = "TRUE"

#####
##To be added to the parameter file specification eventually
#set SURVEY_DBPDG_DISK_LAT_MIN  = "-20.0" 
#set SURVEY_DBPDG_DISK_LAT_MAX  =  "20.0"
#set SURVEY_DBPDG_NUM_PASSES_BRIGHT   = 2 
#set SURVEY_DBPDG_NUM_PASSES_DARKGREY = 2
#set SURVEY_PRIORITY_TYPE       =  "AREA"
###########################################################

if ( $#argv > 0 ) then
  if ( "$argv[1]" == "-help" || "$argv[1]" == "-h"|| "$argv[1]" == "--h" || "$argv[1]" == "--help" ) then
    echo "Usage: run_4MOST_simulations.csh PARAMETER_FILENAME [PARAM1=value] [PARAM2=value] ... "
    echo "---------------------------------------\
Use one of the following CATALOGUE_CODEs:\
0         Merged\
1         AGN\
2         BAO\
3         GalHaloLR\
4         GalDiskLR\
5         Clusters\
6         GalHaloHR\
7         GalDiskHR\
----------------------------------------\
\
----------------------------------------\
Use one of the following SKY_AREA_CODEs:\
1         - very small patch (~10x10deg)\
2         - 20degree wide stripe\
3         - 40degree wide stripe\
4         - large patch (~50x60deg)\
5         - medium patch (~100x20deg) - runs across Gal plane\
ALL or A  - All of the 4MOST accessible sky (dec limits from par file)\
U or USER - use following limits: SKY_AREA_RA_MIN SKY_AREA_RA_MAX SKY_AREA_DEC_MIN SKY_AREA_DEC_MAX\
----------------------------------------" 
    exit 0
  endif
endif
  

#need to add a way to read in the parameter files
#suggest to run gawk on the raw params file to make into a .csh file, then source it
#then need to be able to adjust params on the command line as well. 
#They should have exactly the same format, and should superceed anything in the params file
#non-standard parameters (mainly for internal testing) should also be set on the command line

#first command line argument must be the parameter file filename
if ( $#argv >= 1 ) then
  set PAR_FILE = "${argv[1]}"
else
  echo "Error, first command line argument must be the input parameter filename (or '-h' for help)"
  exit 1
endif

if ( -e $PAR_FILE ) then
  echo "#These are the parameters read from the input param file ${PAR_FILE}:"  >> $PAR_FILE_CSH
  gawk 'NF>0 {n=match($0,"#"); if(n==0){print $0}else if (n>1) {print substr($0,1,n-1)}}' $PAR_FILE | gawk --field-separator='=' 'NF == 2 {gsub("\\.","_",$1);gsub("-","_",$1);printf("set %-40s = %s\n", toupper($1),$2)}' > $PAR_FILE_CSH
  gawk 'NF>0 {n=match($0,"#"); if(n==0){print $0}else if (n>1) {print substr($0,1,n-1)}}' $PAR_FILE | gawk --field-separator='=' 'NF!=2{printf("Bad parameter entry: %s\n",$0)}'
else
  echo "Error, $PAR_FILE is not a valid input parameter file"
  exit 1
endif

#now read the remaining command line arguments
echo ""   >> $PAR_FILE_CSH
echo "#Now come the command line parameters:"  >> $PAR_FILE_CSH
set I = 2
while ( $I <= $#argv ) 
  set noglob
  echo "${argv[$I]}" | gawk --field-separator='=' 'NF == 2 {gsub("\\.","_",$1);gsub("-","_",$1);if(split($2,a," ")<=1){printf("set %-40s = %s\n", toupper($1),$2)}else {printf("set %-40s = \"%s\"\n", toupper($1),$2)}}' >>  $PAR_FILE_CSH
  echo "${argv[$I]}" | gawk --field-separator='=' 'NF!=2{printf("Bad parameter entry: %s\n",$0)}'
  unset noglob
  @ I ++
end

if ( -e $PAR_FILE_CSH ) then
  echo "Reading the input parameter file $PAR_FILE and command line args (sourcing $PAR_FILE_CSH) ..."
  source $PAR_FILE_CSH

  #set the BOOLEAN parameters up
  if ( "${DEBUG}" == "TRUE" || "${DEBUG}" == "True" || "${DEBUG}" == "true" || "${DEBUG}" == "yes" || "${DEBUG}" == "Yes" || "${DEBUG}" == "YES" ) then
    set DEBUG = "TRUE"
  else
    set DEBUG = "FALSE"
  endif

  if ( "${CLOBBER}" == "TRUE" || "${CLOBBER}" == "True" || "${CLOBBER}" == "true" || "${CLOBBER}" == "yes" || "${CLOBBER}" == "Yes" || "${CLOBBER}" == "YES" ) then
    set CLOBBER = "TRUE"
  else
    set CLOBBER = "FALSE"
  endif


  if ( "${DEBUG}" == "TRUE" ) cat $PAR_FILE_CSH
#  rm $PAR_FILE_CSH 
endif
#################################################################


##following is old description
#sequence of script
#1 - step though positions on sky
#  - foreach position
#2 -   filter input catalogue using modified extract_gal_1.exe - split into hi and lo res
#3 -   foreach res (hi lo)
#4 -     run field_mbr_highres_2.exe or field_mbr_lowres_2.exe
#5 -     rename output files
#6 -   merge hi and low res output files


#set EXTRACT_GAL_EXE = ${CODE_BASE}/extract_gal_tom_v1.exe
#set EXTRACT_GAL_EXE = ${CODE_BASE}/extract_gal_tom_v2.exe
#set EXTRACT_GAL_EXE = ${CODE_BASE}/extract_gal_tom_v3

# look for the installed versions
set EXTRACT_GAL_EXE = extract_gal_tom_v3
set FIELD_MBR_EXE   = field_mbr_tom_v2 

##                              1     2     3      4        5
#set MOON_PHASE_NAME      = ( all  dark  grey bright darkgrey )
#set MOON_PHASE_CODE      = (  aa    dd    gg     bb       dg )
#set MOON_PHASE_FRAC_MEAN = (  0.5  0.0   0.5    1.0     0.33 )
#set MOON_PHASE_FRAC_MIN  = (  0.0  0.0   0.25   0.75    0.00 )
#set MOON_PHASE_FRAC_MAX  = (  1.0  0.25  0.75   1.0     0.75 )
#set MOON_PHASE_INDEX_ALL      = 1
#set MOON_PHASE_INDEX_DARK     = 2
#set MOON_PHASE_INDEX_GREY     = 3
#set MOON_PHASE_INDEX_BRIGHT   = 4
#set MOON_PHASE_INDEX_DARKGREY = 5
#set NUM_TILES     = ( $MOON_PHASE_NAME ) 
#set SKY_HOURS     = ( $MOON_PHASE_NAME ) 
#set NUM_FIELDS    = ( $MOON_PHASE_NAME ) 
#set TEXP_PER_TILE = ( $MOON_PHASE_NAME ) 


#first set up a grid of pointings that will tile the sky between DEC_MIN and DEC_MAX
if ( "$SKY_AREA_CODE" == "1" ) then 
  set RA_MIN = "30.0"
  set RA_MAX = "50.0"
  set DEC_MIN = "-65.0"
  set DEC_MAX = "-55.0"
  set AREA_STRING_FOR_EX_GAL = "debug_ra_min=$RA_MIN debug_ra_max=$RA_MAX debug_dec_min=$DEC_MIN debug_dec_max=$DEC_MAX"
else if ( "$SKY_AREA_CODE" == "2" ) then 
  set RA_MIN = "0.0"
  set RA_MAX = "360.0"
  set DEC_MIN = "-65.0"
  set DEC_MAX = "-45.0"
  set AREA_STRING_FOR_EX_GAL = "debug_ra_min=$RA_MIN debug_ra_max=$RA_MAX debug_dec_min=$DEC_MIN debug_dec_max=$DEC_MAX"
else if ( "$SKY_AREA_CODE" == "3" ) then 
  set RA_MIN = "0.0"
  set RA_MAX = "360.0"
  set DEC_MIN = "-65.0"
  set DEC_MAX = "-25.0"
  set AREA_STRING_FOR_EX_GAL = "debug_ra_min=$RA_MIN debug_ra_max=$RA_MAX debug_dec_min=$DEC_MIN debug_dec_max=$DEC_MAX"
else if ( "$SKY_AREA_CODE" == "4" ) then 
  set RA_MIN = "10.0"
  set RA_MAX = "60.0"
  set DEC_MIN = "-30.0"
  set DEC_MAX = "30.0"
  set AREA_STRING_FOR_EX_GAL = "debug_ra_min=$RA_MIN debug_ra_max=$RA_MAX debug_dec_min=$DEC_MIN debug_dec_max=$DEC_MAX"
else if ( "$SKY_AREA_CODE" == "5" ) then 
  set RA_MIN = "50.0"
  set RA_MAX = "150.0"
  set DEC_MIN = "-10.0"
  set DEC_MAX = "10.0"
  set AREA_STRING_FOR_EX_GAL = "debug_ra_min=$RA_MIN debug_ra_max=$RA_MAX debug_dec_min=$DEC_MIN debug_dec_max=$DEC_MAX"
else if ( "$SKY_AREA_CODE" == "USER" || "$SKY_AREA_CODE" == "U" ) then 
  set RA_MIN = "$SKY_AREA_RA_MIN"
  set RA_MAX = "$SKY_AREA_RA_MAX"
  set DEC_MIN = "$SKY_AREA_DEC_MIN"
  set DEC_MAX = "$SKY_AREA_DEC_MAX"
  set AREA_STRING_FOR_EX_GAL = "debug_ra_min=$RA_MIN debug_ra_max=$RA_MAX debug_dec_min=$DEC_MIN debug_dec_max=$DEC_MAX"
else if ( "$SKY_AREA_CODE" == "ALL" || "$SKY_AREA_CODE" == "A" ) then 
  set RA_MIN = "0.0"
  set RA_MAX = "360.0"
  set DEC_MIN = "$TILING_DEC_MIN"
  set DEC_MAX = "$TILING_DEC_MAX"
  set AREA_STRING_FOR_EX_GAL = ""
else
  echo "I do not know this sky area: $SKY_AREA_CODE"
  exit 1
endif

#interpret params for internal use
set PATROL_RADIUS_ARCSEC    = `echo "$POSITIONER_PATROL_RADIUS_MAX $TELESCOPE_PLATE_SCALE" | gawk '{printf("%.1f", $1/$2)}'`
set TELESCOPE_FOV_RADIUS  = `echo $TELESCOPE_FOV_AREA | gawk '//{printf("%.8g", sqrt($1/(1.5*sqrt(3.))))}'` 
set TELESCOPE_FOV_RADIUS_ARCSEC  = `echo $TELESCOPE_FOV_RADIUS | gawk '//{printf("%.1f", $1*3600.)}'` 


echo "Making hardin file: TILING METHOD = $TILING_METHOD"
if ( "$TILING_METHOD" == "DEFAULT" ) then
  #search the list for the most sensible tiling pattern - just less than inner radius+patrol radius
  set TILING_NUM_POINTS = `gawk -v point_r=$TELESCOPE_FOV_RADIUS -v patrol_r=$PATROL_RADIUS_ARCSEC 'BEGIN {total_r=(point_r*sqrt(3.)/2.)+((patrol_r)/3600.);best=-1} $1!~/^#/{if($3<=total_r){best=$2;exit}} END {print best}' ${HOME_BASE}/pointings/hardin_coverings_list.txt`
  if ( $TILING_NUM_POINTS > 0 ) then
    set HARDIN_FILE = "${HOME_BASE}/pointings/pointings_hardin${TILING_NUM_POINTS}.txt"
    set HARDIN_FORMAT_FILE = "${HARDIN_FILE:r}_format.txt"
    if ( ! -e $HARDIN_FILE ) then
      cd ${HOME_BASE}/pointings/
      ./make_hardin.csh $TILING_NUM_POINTS
      cd $SIM_BASE
    endif
  else
    echo "Error, the Hardin et al. pattern $TILING_NUM_POINTS does not exist"
    exit 1
  endif
else if ( "$TILING_METHOD" == "HARDIN_NUMBER" ) then
  # have been supplied with a Hardin number
  if ( `gawk -v tnp=$TILING_NUM_POINTS 'BEGIN {flag="FALSE"} $2 == tnp {flag="TRUE";exit} END{print flag}' ${HOME_BASE}/pointings/hardin_coverings_list.txt`  == "TRUE" ) then
    set HARDIN_FILE = "${HOME_BASE}/pointings/pointings_hardin${TILING_NUM_POINTS}.txt"
    set HARDIN_FORMAT_FILE = "${HARDIN_FILE:r}_format.txt"
    if ( ! -e $HARDIN_FILE ) then
      cd ${HOME_BASE}/pointings/
      ./make_hardin.csh $TILING_NUM_POINTS
      cd $SIM_BASE
    endif
  else
    echo "Error, the Hardin et al. pattern $TILING_NUM_POINTS does not exist"
    exit 1
  endif
else
  set HARDIN_FORMAT_FILE = "${HARDIN_FILE}"

endif

#test the Hardin file validity
if ( ! -e $HARDIN_FILE ) then
  echo "Error, the Hardin et al. pattern file $HARDIN_FILE does not exist"
  exit 1
else if ( `egrep -c 'header number not followed by blank' $HARDIN_FILE` > 0 ) then
  echo "Error, the Hardin et al. pattern file $HARDIN_FILE is bad"
  exit 1
else if ( `cat $HARDIN_FORMAT_FILE | wc -l` < $TILING_NUM_POINTS ) then
  echo "Error, the Hardin et al. pattern file $HARDIN_FORMAT_FILE is bad"
  exit 1
else
  echo "Using a tiling based on Hardin et al. pattern file $HARDIN_FORMAT_FILE"
endif

#this is not really used currently
if      ( "$TELESCOPE_CODE_NAME" == "VISTA" )   then
  set TELESCOPE_CODE = 1
  set SITE_LONGITUDE = "70:23:51."
  set SITE_LATITUDE  = "-24:36:57."
  set SITE_ALTITUDE  = "2635"
else if ( "$TELESCOPE_CODE_NAME" == "NTT" )   then
  set TELESCOPE_CODE = 2
  set SITE_LONGITUDE = "70:43:54.272"
  set SITE_LATITUDE  = "-29:15:18.440"
  set SITE_ALTITUDE  = "2375"
else
  echo "Error, I do not know this TELESCOPE: $TELESCOPE_CODE_NAME"
  exit 1
endif

#this is for field_mbr only
if      ( "$POSITIONER_FAMILY" == "Echidna" || "$POSITIONER_FAMILY" == "ECHIDNA" ) then
  set POSITIONER_CODE = 1
else if ( "$POSITIONER_FAMILY" == "LAMOST" ) then
  set POSITIONER_CODE = 2
else if ( "$POSITIONER_FAMILY" == "POS-B" ) then
  set POSITIONER_CODE = 3
else if ( "$POSITIONER_FAMILY" == "POTZPOZ" ) then
  set POSITIONER_CODE = 4
else if ( "$POSITIONER_FAMILY" == "MUPOZ" ) then
  set POSITIONER_CODE = 5
else
  echo "Error, I do not know this POSITIONER: $POSITIONER_FAMILY"
  exit 1
endif


##for hexagonal packing set the spacing as diameter*sqrt(3)/2
#set POINTING_SPACING  = `echo $POINTING_DIAMETER | awk '//{print $1*sqrt(3.)/2.}'` 
set NAN_STR = "Null"
set TILE_LIST = ( 1 2 ) 
set TILE_ID_LIST = ( `echo $TILE_LIST | gawk '//{for(i=1;i<=NF;i++){printf("%02d\n",$i)}}'` )
set RES_LIST = ( 1 2 ) 
set RES_NAME_LIST = ( "lo" "hi") 

#Number    Catalogue
#0         Merged
#1         AGN
#2         BAO
#3         GalHaloLR
#4         GalDiskLR
#5         Clusters
#6         GalHaloHR
#7         GalDiskHR
#....
set CATALOGUE_FILENAME_LIST  = ( "AGN_v1.0b_templates.txt" \
"mockCatBAO_v1.2b.dat" \
"haloLR_01_mock_v2.txt" \
"diskLR_DB_ESN_02b_mock.txt" \
"galclus_01_mock.txt" \
"haloHR_01b_mock.txt"  \
"diskHR_01b_mock.txt" )
set CATALOGUE_SHORTNAME_LIST = ( "AGN"  "BAO" "GalHaloLR"  "GalDiskLR"  "Clusters"  "GalHaloHR"  "GalDiskHR" )


#sort the input catalogues, do this only once
if ( $#CATALOGUE_CODE == 1 && "$CATALOGUE_CODE[1]" == "0" ) then
  set LIST = ( 1 2 3 4 5 6 7 ) 
else 
  set LIST = ( $CATALOGUE_CODE ) 
endif
foreach C ( $LIST ) 
  cd ${MAIN_BASE}/${CATALOGUE_SHORTNAME_LIST[$C]}_in/
  if ( ! -e ${CATALOGUE_FILENAME_LIST[$C]:r}_sorted.txt ) then
    echo "Sorting input catalogue to accelerate assignments: ${CATALOGUE_FILENAME_LIST[$C]} > ${CATALOGUE_FILENAME_LIST[$C]:r}_sorted.txt"
    echo "This will take some time.... started at `date`"
    gawk '$1~/^#/ {print $0} $1!~/^#/ {exit}'  ${CATALOGUE_FILENAME_LIST[$C]} > ${CATALOGUE_FILENAME_LIST[$C]:r}_sorted.txt
    gawk -f  ~/.gawkrc -v dec_min=$ABSOLUTE_DEC_MIN -v dec_max=$ABSOLUTE_DEC_MAX -v block_size_deg=2.0 --source '$1!~/^#/ && $4>=dec_min && $4<=dec_max {printf("%4d %4d %s\n", int($4/block_size_deg),int($3*cosd($4)/block_size_deg),$0)}' ${CATALOGUE_FILENAME_LIST[$C]} | sort -n -k 1,2 | gawk '//{print substr($0,11)}' >> ${CATALOGUE_FILENAME_LIST[$C]:r}_sorted.txt
    echo "Finished sorting at `date`"
    echo ""
  endif
  set OLD_CAT_NAME = ${CATALOGUE_FILENAME_LIST[$C]}
  set CATALOGUE_FILENAME_LIST[$C] = ${OLD_CAT_NAME:r}_sorted.txt
end
cd $BASE




switch ( "${CATALOGUE_CODE}" )
  case "0" :
    #Merged catalogue
    set CATALOGUE_NAME = "Merged"
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( 1 2 3 4 5 6 7 )
    set CATALOGUE_FILENAMES = ( ) 
    foreach C ( $CATALOGUE_CODE_FOR_EXTRACT_GAL ) 
      set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAMES $CATALOGUE_FILENAME_LIST[$C] ) 
    end
  breaksw

  case "1" :
    #AGN catalogue
    set CATALOGUE_NAME = "AGN"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  case "2" :
    #BAO catalogue
    set CATALOGUE_NAME = "BAO"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  case "3" :
    #GalHaloLR catalogue
    set CATALOGUE_NAME = "GalHaloLR"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  case "4" :
    #GalDiskLR catalogue
    set CATALOGUE_NAME = "GalDiskLR"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  case "5" :
    #Clusters catalogue
    set CATALOGUE_NAME = "Clusters"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  case "6" :
    #GalHaloHR catalogue
    set CATALOGUE_NAME = "GalHaloHR"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  case "7" :
    #GalDiskHR catalogue
    set CATALOGUE_NAME = "GalDiskHR"
    set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAME_LIST[$CATALOGUE_CODE] ) 
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( $CATALOGUE_CODE )
  breaksw

  default :
    #probably a Merged catalogue
    set CATALOGUE_NAME = "Merged"
    set CATALOGUE_CODE_FOR_EXTRACT_GAL = ( ${CATALOGUE_CODE} )
    set CATALOGUE_CODE = "0"
    set CATALOGUE_FILENAMES = ( ) 
    foreach C ( $CATALOGUE_CODE_FOR_EXTRACT_GAL ) 
      if ( $C >= 1 && $C <=  $#CATALOGUE_FILENAME_LIST ) then
        set CATALOGUE_FILENAMES = ( $CATALOGUE_FILENAMES $CATALOGUE_FILENAME_LIST[$C] ) 
      else
        echo "Error. I do not know this catalogue ID ${CATALOGUE_CODE}, sorry!"
        echo "Use one of the following:\
#CODE      Catalogue\
#0         Merged\
#1         AGN\
#2         BAO\
#3         GalHaloLR\
#4         GalDiskLR\
#5         Clusters\
#6         GalHaloHR\
#7         GalDiskHR\
#'[1] [2] [3] [4] [5] [6] [7]' Use the selected catalogues" 
        exit 1

      endif
    end

  breaksw
endsw

##############################################################################################
#if ( "$SURVEY_STRATEGY_TYPE" == "FLAT" ) then
#  set MOON_PHASE_LIST = ( $MOON_PHASE_INDEX_BRIGHT $MOON_PHASE_INDEX_DARKGREY ) 
#else if ( "$SURVEY_STRATEGY_TYPE" == "DBPDG" ) then
#  set MOON_PHASE_LIST = ( $MOON_PHASE_INDEX_BRIGHT $MOON_PHASE_INDEX_DARKGREY ) 
#else
#  echo "I do not know this type of survey strategy: $SURVEY_STRATEGY_TYPE"
#  exit 1
#endif
##############################################################################################


set SIM_NAME          = "${SIMULATION_CODE_NAME}_${TELESCOPE_CODE_NAME}_${POSITIONER_CODE_NAME}_${CATALOGUE_NAME}"
set SIM_NAME_GNUPLOT  = "${SIMULATION_CODE_NAME} ${TELESCOPE_CODE_NAME} ${POSITIONER_CODE_NAME} ${CATALOGUE_NAME}"

set OUTPUT_BASE = ${BASE}/output_${SIM_NAME}
if ( -d $OUTPUT_BASE ) then
  if ( "${DO_SECTION_EXTRACT_GAL}" == "TRUE") then
    if ( "$CLOBBER" == "TRUE") then
      echo "Tidying up the existing directory ${OUTPUT_BASE}/"
      rm -r ${OUTPUT_BASE}/
      mkdir ${OUTPUT_BASE}/
    else
      echo "Error: OUTPUT_BASE already exists - maybe need to set clobber=true?"
      exit 1
    endif
  endif
else 
  mkdir $OUTPUT_BASE
endif


if (  "${DO_SECTION_EXTRACT_GAL}" == "TRUE" ) then
  set EXTRACT_GAL_PARAMS_FILE =  ${OUTPUT_BASE}/extract_gal_input_params.txt
  if ( -e $EXTRACT_GAL_PARAMS_FILE ) rm $EXTRACT_GAL_PARAMS_FILE
  sed 's/^set //'   ${PAR_FILE_CSH} >> $EXTRACT_GAL_PARAMS_FILE
endif
#take a copy of the formatted input params file
if ( -e  $PAR_FILE_CSH ) mv $PAR_FILE_CSH ${OUTPUT_BASE}/


echo "Working on catalogues $CATALOGUE_NAME (code $CATALOGUE_CODE), output in $OUTPUT_BASE"
cd ${OUTPUT_BASE}/

#this is handled within extract_gal now
#set FIELDS_FILE = "${OUTPUT_BASE}/fields.txt"
set FIELDS_FILE = "${HARDIN_FORMAT_FILE}"
set FIELD_LIST_FILE = "${OUTPUT_BASE}/extract_gal_tile_list.txt"


#############################################################################################
if ( "$DO_SECTION_EXTRACT_GAL" == "TRUE" ) then
  cd ${OUTPUT_BASE}/
  set LOGFILE = ${OUTPUT_BASE}/extract_gal.log
  if ( -e ${LOGFILE} ) rm ${LOGFILE}
  echo "#########Started  the extract_gal section at `date` log at ${LOGFILE}"

  #prepare for the extract_gal code
  foreach CODE ( $CATALOGUE_CODE_FOR_EXTRACT_GAL ) 
    ln -s -f ${MAIN_BASE}/${CATALOGUE_SHORTNAME_LIST[$CODE]}_in/${CATALOGUE_FILENAME_LIST[$CODE]} ${OUTPUT_BASE}/
  end

#  if ( -e  ${HOME_BASE}/fiber_pos/fibers_geometry_new_${TELESCOPE_CODE_NAME}_${POSITIONER_FAMILY}_lohiRes.tab ) then
#    gawk '//{n++;if(n==1){printf("%11.4f %11.4f\n",$1,$2)}else if(n!=2){printf("%4s %11s %11s %d\n",$1,$2,$3,($4=="lo"?1:2))}}' ${HOME_BASE}/fiber_pos/fibers_geometry_new_${TELESCOPE_CODE_NAME}_${POSITIONER_FAMILY}_lohiRes.tab > ${OUTPUT_BASE}/fibers_geometry.tab
#  else
#    echo "Error, cannot find the fiber layout file for ${TELESCOPE_CODE_NAME} ${POSITIONER_FAMILY}"
#    exit 1 
#  endif

  ##########run the extract_gal code
  ${EXTRACT_GAL_EXE} PARAM_FILENAME="$EXTRACT_GAL_PARAMS_FILE" CATALOGUE_CODE="$CATALOGUE_CODE_FOR_EXTRACT_GAL" debug=yes ${AREA_STRING_FOR_EX_GAL}  | tee ${LOGFILE} | egrep '^#-Comment|^#-Warning|^#-Error'

  if ( `grep -c '#-Error' ${LOGFILE}` >= 1 ) then
    echo "There was an error in the ${EXTRACT_GAL_EXE} step, check log ${LOGFILE}"
    exit 1
  endif
  echo "#########Finished the extract_gal section at `date`"
endif
#############################################################################################

#############################################################################################
if ( "$DO_SECTION_FIELD_MBR" == "TRUE" ) then
  echo "#########Started  the field_mbr section at `date`"
  ####main loop over fields is coming up
  cd ${OUTPUT_BASE}/
#  if ( -e ${OUTPUT_BASE}/scheduling_settings.csh ) then
#    echo "Sourcing the scheduling params ${OUTPUT_BASE}/scheduling_settings.csh"
#    source ${OUTPUT_BASE}/scheduling_settings.csh
#  else
#    echo "Cannot find scheduling parameters file: ${OUTPUT_BASE}/scheduling_settings.csh"
#    exit 1
#  endif
#  set NUM_FIELDS = `wc -l ${TILE_LIST_FILE} | gawk '{print $1}'`
  #fudge - only take account of the first tile of each pointing+moonphase
  set NUM_FIELDS = `gawk '$3 == "01" {n++} END {print n}' $FIELD_LIST_FILE`
  set MOON_PHASE_LIST = (`gawk '{print $2}' $FIELD_LIST_FILE | sort -u`)

  if ( "$SURVEY_STRATEGY_TYPE" == "FLAT" ) then
    @ SURVEY_NRUNS_DG =  $SURVEY_FLAT_NUM_PASSES_DARKGREY * $SURVEY_NUM_TILES_PER_PASS_DARKGREY
    @ SURVEY_NRUNS_BB =  $SURVEY_FLAT_NUM_PASSES_BRIGHT   * $SURVEY_NUM_TILES_PER_PASS_BRIGHT
  else if ( "$SURVEY_STRATEGY_TYPE" == "DBPDG" ) then
    @ SURVEY_NRUNS_DG =  $SURVEY_DBPDG_NUM_PASSES_DARKGREY * $SURVEY_NUM_TILES_PER_PASS_DARKGREY
    @ SURVEY_NRUNS_BB =  $SURVEY_DBPDG_NUM_PASSES_BRIGHT   * $SURVEY_NUM_TILES_PER_PASS_BRIGHT
  else
    set SURVEY_NRUNS_DG =  $SURVEY_NUM_TILES_PER_PASS_DARKGREY
    set SURVEY_NRUNS_BB =  $SURVEY_NUM_TILES_PER_PASS_BRIGHT
  endif 

  #############################################################################################
  #set up the output file headers 
  foreach MM ( $MOON_PHASE_LIST ) 
    if ( -e ${OUTPUT_BASE}/${MM}_fiber_assignment.tab ) rm ${OUTPUT_BASE}/${MM}_fiber_assignment.tab
    #Format for output files
    echo "#Column   Format  Name\
#1        I4      Field ID\
#2        F9.5    Field Centre - RA (deg, J2000)\
#3        F9.5    Field Centre - Dec (deg, J2000)\
#4        I2      Tile Number (first tile=1, second tile=2.....)\
#5        A10     Tile Start Date UTC, format YYYY-MM-DD\
#6        A10     Tile Start Time UTC, format hh:mm:ss.s\
#7        F7.1    Tile Exposure time (seconds on sky)\
#8        F4.2    Tile Moon Illuminated fraction - at middle of exposure (1.0=full, 0.0=new)\
#9        F5.1    Tile Moon Distance (deg) - at middle of exposure\
#10       F5.2    Tile Airmass - at middle of exposure\
#11       F5.2    Tile Seeing - mean during exposure (arcsec, FWHM, @ r-band)\
#12       I1      Tile cloud cover code - mean during exposure (1=photometric, 2=thin, 3=thick)\
#13       I4      Fiber ID number\
#14       I1      Fiber resolution (1=low res, 2=high res)\
#15       F9.3    Fiber starting x-coordinate (arcsec)\
#16       F9.3    Fiber starting y-coordinate (arcsec)\
#17       F9.3    Object x-coordinate (arcsec)\
#18       F9.3    Object y-coordinate (arcsec)\
#19       F6.4    Object renormalised priority (0.0=lowest 1.0=highest)\
#20       F9.3    Object x-offset from fiber starting position (arcsec)\
#21       F9.3    Object y-offset from fiber starting position (arcsec)\
#22       I4      Input Catalogue code (1=AGN,2=BAO,3=GalHaloLR,4=GalDiskLR,5=Clusters,6=GalHaloHR,7=GalDiskHR)\
#Next comes a copy of the input catalogue columns....\
#23       A15     Object identification\
#24       I8      Object ID number\
#25       F9.5    Object RA (deg,J2000)\
#26       F9.5    Object Dec(deg,J2000)\
#27       I3      Object original priority\
#28       I4      Requested exposure time dark, minutes\
#29       I4      Requested exposure time grey, minutes\
#30       I4      Requested exposure time bright, minutes\
#31       I1      Fiber resolution (1=low res, 2=high res)\
#32       F5.2    SDSS r-band AB magnitude\
#33       A30     Name of template spectrum\
#34+      ....    Any additional columns supplied in input catalogue\
####" > ${OUTPUT_BASE}/${MM}_fiber_assignment.tab
      
    if ( -e ${OUTPUT_BASE}/${MM}_pseudo_cat.txt )  rm ${OUTPUT_BASE}/${MM}_pseudo_cat.txt
  end

  #############################################################################################


  #############################################################################################
  # main loop to do the field_mbr
  set I = 1
  while ( $I <= $NUM_FIELDS ) 
    set FIELD_INFO = ( `gawk -v i=$I '$3 == "01" {n++;if(n==i){moon_code=($2=="bb"?1:($2=="dd"?3:($2=="dg"?3:"3")));print $0,moon_code,($7/60.);exit}}' $FIELD_LIST_FILE` )
    set FIELD_BASE = ${OUTPUT_BASE}/f${FIELD_INFO[1]}
    set STUB  = "f${FIELD_INFO[1]}_${FIELD_INFO[2]}_01"
    set STUB1 = "f${FIELD_INFO[1]}_${FIELD_INFO[2]}"
    if ( -d ${FIELD_BASE} ) then
      printf 'Running field_mbr on field %5s %2s %02d (%5d/%5d) %9.5f %9.5f ... ' $FIELD_INFO[1] $FIELD_INFO[2] $FIELD_INFO[3] $I $NUM_FIELDS ${FIELD_INFO[4]} ${FIELD_INFO[5]}
      cd ${FIELD_BASE}
    else
      @ I ++
      continue
    endif
    #tidy up any old debris
    rm ${STUB}_*.tab  ${STUB}_*.log >>& /dev/null
  
    ######################
    set NPSEUDO_HILO = `wc -l ${FIELD_BASE}/${STUB}_pseudo_cat.txt | gawk '{print $1}'`
    set NPSEUDO_LO = `gawk 'BEGIN{n=0} $14==1{n++} END{print n}' ${FIELD_BASE}/${STUB}_pseudo_cat.txt`
    set NPSEUDO_HI = `gawk 'BEGIN{n=0} $14==2{n++} END{print n}' ${FIELD_BASE}/${STUB}_pseudo_cat.txt`
    if ( $NPSEUDO_HILO <= 0 ) then
      echo "No pseudo sources or no psuedo catalogue found for ${STUB} (npseudo = $NPSEUDO_HILO)"
      #tidy up
      foreach TILE_ID ( $TILE_ID_LIST ) 
        rm ${STUB1}_${TILE_ID}_conditions.txt >>& /dev/null
      end
      rm ${STUB}_pseudo_cat.txt >>& /dev/null
      cd ${OUTPUT_BASE}/
      @ I ++
      continue 
    endif
  
    #################################################################
    printf ' input sources: %5d total (%5d hi %5d lo) ' $NPSEUDO_HILO $NPSEUDO_HI $NPSEUDO_LO 
    ln -sf ../fibers_geometry.tab ./fibers_geometry.tab

    if      ( "$FIELD_INFO[2]" == "dg" ) then
      set SURVEY_NRUNS = $SURVEY_NRUNS_DG
    else if ( "$FIELD_INFO[2]" == "bb" ) then
      set SURVEY_NRUNS = $SURVEY_NRUNS_BB
    endif

    #set up the runfile
    # Nruns
    # MOON_CODE
    # exposure time, minutes
    # MINIMUM_SKY_FIBERS_LOW_RES
    # MINIMUM_SKY_FIBERS_HIGH_RES
    # POSITIONER_CODE
    # POSITIONER_PATROL_RADIUS_MIN
    # POSITIONER_PATROL_RADIUS_MAX
    # POSITIONER_MINIMUM_SEPARATION
    # - then positioner specific params
    echo "${SURVEY_NRUNS}\
${FIELD_INFO[9]}\
${FIELD_INFO[10]}\
${OVERHEADS_MINIMUM_SKY_FIBERS_LOW_RES}\
${OVERHEADS_MINIMUM_SKY_FIBERS_HIGH_RES}\
${POSITIONER_CODE}\
${POSITIONER_PATROL_RADIUS_MIN}\
${POSITIONER_PATROL_RADIUS_MAX}\
${POSITIONER_MINIMUM_SEPARATION}" >  ${OUTPUT_BASE}/field_mbr.pars

    if ( "$POSITIONER_FAMILY" == "POTZPOZ" ) then
      echo "${POSITIONER_POTZPOZ_WIDTH}\
${POSITIONER_POTZPOZ_TIP_RADIUS}\
${POSITIONER_POTZPOZ_PIVOT_OFFSET}\
${POSITIONER_POTZPOZ_RECTANGLE_LENGTH}" >>  ${OUTPUT_BASE}/field_mbr.pars
    else if ( "$POSITIONER_FAMILY" == "MUPOZ" ) then
      echo "${POSITIONER_MUPOZ_TIP_WIDTH}\
${POSITIONER_MUPOZ_TIP_DIST}" >>  ${OUTPUT_BASE}/field_mbr.pars
    else 
      echo "I cannot deal with $POSITIONER_FAMILY yet"
    exit 1
  endif




    if ( -e pseudo_cat.ascii ) rm pseudo_cat.ascii
    ln -s ${STUB}_pseudo_cat.txt pseudo_cat.ascii
    ##//////////////////////////////////////////////////////////////////////////
    ${FIELD_MBR_EXE} < ${OUTPUT_BASE}/field_mbr.pars >& ${STUB}_field_mbr.log
    ##//////////////////////////////////////////////////////////////////////////
    if ( -e ${STUB}_field_mbr_assignment.tab )       rm ${STUB}_field_mbr_assignment.tab
    if ( -e ${STUB}_field_mbr_summary_orig.tab )     rm ${STUB}_field_mbr_summary_orig.tab
    if ( -e ${STUB}_field_mbr_fibres_catalogue.tab ) rm ${STUB}_field_mbr_fibres_catalogue.tab
    if ( -e assignment.tab )   mv assignment.tab ${STUB}_field_mbr_assignment.tab
    if ( -e summary.tab    )   mv summary.tab    ${STUB}_field_mbr_summary_orig.tab
    if ( -e minimal_result.tab  )   mv minimal_result.tab  ${STUB}_minimal_result.tab
    if ( -e fibres_catalogue.tab ) rm fibres_catalogue.tab
  
  
    #reformat the catalogue columns
    if ( -e ${STUB}_fiber_assignment.tab ) rm ${STUB}_fiber_assignment.tab
    set ASSIGN2_LIST = ()
    set CONDITIONS_LIST = () 
    if ( -e ${STUB}_field_mbr_assignment.tab ) then
      foreach TILE ( $TILE_LIST ) 
        set TILE_ID = `printf '%02d' $TILE`
        gawk -v tile=$TILE  -v nan_str="$NAN_STR" 'FILENAME~/conditions/ {n=split($1,a,"=");if(n==2){par[a[1]]=a[2];}} \
FILENAME~/fibers_geometry/ && NF == 4 {f_res[$1]=$4}\
FILENAME~/assignment/ && $1=="Observation" && $2=="run" {this_tile=$4;if(this_tile>tile){exit}} \
FILENAME~/assignment/ {\
  if(NF>9 && this_tile==tile){\
    printf("%4s %9.5f %+9.5f %2d %10s %10s %7.1f %4.2f %5.1f %5.2f %5.2f %1d %4d %1d %9.3f %9.3f %9.3f %9.3f %6.4f %9.3f %9.3f %5s %s\n", par["FIELD_ID"], par["FIELD_CENTRE_RA"], par["FIELD_CENTRE_DEC"],tile,par["TILE_START_DATE"],par["TILE_START_TIME"],par["TILE_EXPOSURE_TIME"],par["TILE_MOON_PHASE"],par["TILE_MOON_DISTANCE"],par["TILE_AIRMASS"],par["TILE_SEEING"],par["TILE_CLOUD_COVER"], $2, f_res[$1], $3, $4, $7, $8, $9, $10, $11, $12, substr($0,92))}\
  else if(NF==4 && this_tile==tile && $1!="Observation"){\
    printf("%4s %9.5f %+9.5f %2d %10s %10s %7.1f %4.2f %5.1f %5.2f %5.2f %1d %4d %1d %9.3f %9.3f %9.3f %9.3f %6s %9.3f %9.3f %4d %15s %8d %9s %9s %3d %4d %4d %4d %1d %5s %30s\n", par["FIELD_ID"], par["FIELD_CENTRE_RA"], par["FIELD_CENTRE_DEC"],tile,par["TILE_START_DATE"],par["TILE_START_TIME"],par["TILE_EXPOSURE_TIME"],par["TILE_MOON_PHASE"],par["TILE_MOON_DISTANCE"],par["TILE_AIRMASS"],par["TILE_SEEING"],par["TILE_CLOUD_COVER"], $2, f_res[$1], $3, $4, $3, $4, nan_str, 0.0, 0.0, -1, nan_str, "-1", nan_str, nan_str, "-1", "-1", "-1", "-1", f_res[$1], nan_str, "-")}\
}' fibers_geometry.tab ${STUB1}_${TILE_ID}_conditions.txt ${STUB}_field_mbr_assignment.tab >> ${STUB}_fiber_assignment.tab
        set ASSIGN2_LIST    = ( $ASSIGN2_LIST    ${STUB}_fiber_assignment.tab ) 
        set CONDITIONS_LIST = ( $CONDITIONS_LIST ${STUB1}_${TILE_ID}_conditions.txt ) 
          cat ${STUB}_fiber_assignment.tab >> ${OUTPUT_BASE}/${FIELD_INFO[2]}_fiber_assignment.tab
      end
      cat ${STUB}_pseudo_cat.txt >> ${OUTPUT_BASE}/${FIELD_INFO[2]}_pseudo_cat.txt
    endif

    echo " Done field+moon $FIELD_INFO[1] ${FIELD_INFO[2]} at `date`"
    cd $OUTPUT_BASE
    @ I ++
  end
  ###end of main pointing loop
  ####################################################################
  echo "#########Finished the field_mbr section at `date`"
endif



#############################################################################################
if ( "$DO_SECTION_SUMMARISE" == "TRUE" ) then
  echo "#########Started  the summarise section at `date`"
  ####main loop over fields is coming up
  cd ${OUTPUT_BASE}/

  if ( "$SURVEY_STRATEGY_TYPE" == "FLAT" ) then
    set SURVEY_NUM_PASSES_BRIGHT   = $SURVEY_FLAT_NUM_PASSES_BRIGHT
    set SURVEY_NUM_PASSES_DARKGREY = $SURVEY_FLAT_NUM_PASSES_DARKGREY
  else  if ( "$SURVEY_STRATEGY_TYPE" == "DBPDG" ) then
    set SURVEY_NUM_PASSES_BRIGHT   = $SURVEY_DBPDG_NUM_PASSES_BRIGHT
    set SURVEY_NUM_PASSES_DARKGREY = $SURVEY_DBPDG_NUM_PASSES_DARKGREY
  else
    echo "Error"
    exit 1
  endif


  if ( -e ${OUTPUT_BASE}/field_mbr_summary.tab ) rm ${OUTPUT_BASE}/field_mbr_summary.tab
  #Format for output files
  echo "#Column   Format  Name\
#1        I4      Field ID\
#2        F9.5    Field Centre - RA (deg, J2000)\
#3        F9.5    Field Centre - Dec (deg, J2000)\
#4        I3      Number of tiles executed for this field\
#5        I3      Number of tiles in which this target was assigned a fiber\
#6        I2      Number of fibers (of correct resolution) that could reach the target\
#7        F6.4    Renormalised target priority (0.0=lowest 1.0=highest) \
#8        A16     Array of flags that show if the object was observed in a specific tile for this\
#                 field.  A one-character code is given for each tile. ‘0’ denotes\
#                 that object was not observed in this tile, ‘1’ denotes that the object\
#                 was observed in this tile and ‘.’ denotes that this tile was not executed.\
#                 Up to 16 tiles per field can be accommodated with this format.  The\
#                 first (leftmost) character of the array gives information for tile 1,\
#                 the last (rightmost) character gives information for tile 16.  \
#9        F7.1    Total dark time exposure for this object (seconds on sky) \
#10       F7.1    Total grey time exposure for this object (seconds on sky) \
#11       F7.1    Total bright time exposure for this object (seconds on sky)\
#12       I4      Input Catalogue code (1=AGN,2=BAO,3=GalHaloLR,4=GalDiskLR,5=Clusters,6=GalHaloHR,7=GalDiskHR)\
#                 Next is a copy of the input catalogue columns....  \
#13       A15     Object identification\
#14       I8      Object ID number\
#15       F9.5    Object RA (deg,J2000)\
#16       F9.5    Object Dec(deg,J2000)\
#17       I3      Object original priority\
#18       I4      Requested exposure time dark, minutes\
#19       I4      Requested exposure time grey, minutes\
#20       I4      Requested exposure time bright, minutes\
#21       I1      Fiber resolution (1=low res, 2=high res)\
#22       F5.2    SDSS r-band AB magnitude\
#23       A30     Name of template spectrum\
#24+      ....    Any additional columns supplied in input catalogue\
####" > ${OUTPUT_BASE}/field_mbr_summary.tab

  #########################################
  set NUM_FIELDS = `gawk '{print $1}' $FIELD_LIST_FILE | sort -u | wc -l`
  set I = 1
  while ( $I <= $NUM_FIELDS ) 
    set FIELD = `gawk '{print $1}' $FIELD_LIST_FILE | sort -u | gawk '{n++; if(n==i){print $1;exit}}'`
    set FIELD_INFO = ( `gawk -v i=$I '$3 == "01" {n++; if(n==i){print $0;exit}}' $FIELD_LIST_FILE` )
    set FIELD_BASE = ${OUTPUT_BASE}/f${FIELD_INFO[1]}
    set STUB = "${FIELD_BASE}/f${FIELD_INFO[1]}"

    #inputs: list of conditions files, then  *_field_mbr_summary_orig.tab for each moon+tile
    set FILES = ()
    set FILES = ( `sls "${STUB}_bb_01_conditions.txt ${STUB}_dg_01_conditions.txt"` )
    set FILES = ( $FILES `sls "${STUB}_bb_01_field_mbr_summary_orig.tab ${STUB}_dg_01_field_mbr_summary_orig.tab"` )
#    echo "Working on: $FILES"
    if ( $#FILES > 0 ) then
    gawk  -f  ~/.gawkrc -v passes_dg=${SURVEY_NUM_PASSES_DARKGREY} -v passes_bb=${SURVEY_NUM_PASSES_BRIGHT} -v max_flags=16 -v strategy=${SURVEY_STRATEGY_TYPE} --source '\
BEGIN {for(i=1;i<=max_flags;i++){flag[i]=0}; if (strategy=="FLAT"){npasses_total=passes_dg+passes_bb}else if (strategy=="DBPDG"){npasses_total=max(passes_dg,passes_bb)}}\
FILENAME~/conditions/ {l=split(FILENAME,fn,"/");split(fn[l],bb,"_");m=bb[2];t=bb[3]; n=split($1,a,"=");if(n==2){par[m,t,a[1]]=a[2];}}\
FILENAME != last_FILENAME && FILENAME~/summary/ {l=split(FILENAME,fn,"/");split(fn[l],bb,"_");m=bb[2];t=bb[3];}\
// {last_FILENAME=FILENAME;}\
FILENAME~/summary/ && $1~/^newOut/ {name=sprintf("%s,%s",$8,$9);s[name]=$0;split(s[name],b," ");a[name]+=b[3];texp[name,m]+=((b[3]+0.0)*(par[m,t,"TILE_EXPOSURE_TIME"]+0.0));this_bits=str2bits_LE(b[6]);bits[name]=or(bits[name],lshift(this_bits,(m=="bb"?0:2)));}\
END {t="01";for(name in s){split(s[name],b," ");printf("%5s %9.5f %+9.5f %3d %3d %2d %6.4f %16s %7.1f %7.1f %7.1f %4d %s\n",par[m,t,"FIELD_ID"], par[m,t,"FIELD_CENTRE_RA"], par[m,t,"FIELD_CENTRE_DEC"], npasses_total, a[name], b[4],b[5],bits2str_LE(bits[name],max_flags),texp[name,"dg"],texp[name,"gg"],texp[name,"bb"],b[7],substr(s[name],47))}}'  ${FILES} | gawk 'NF > 1'  > ${STUB}_field_mbr_summary.tab


#function print_it(){s="";for(i=1;i<=max_flags;i++){s=s (i<=ntiles_field?flag[i]:".");flag[i]=0}; printf("%5s %9.5f %+9.5f %3d %3d %2d %6.4f %16s %7.1f %7.1f %7.1f %4d %s\n", par[m,t,"FIELD_ID"], par[m,t,"FIELD_CENTRE_RA"], par[m,t,"FIELD_CENTRE_DEC"], ntiles_field, ntiles, nfib, prio, s, texp["d"],texp["g"],texp["b"],cat_code, last); return}

      cat ${STUB}_field_mbr_summary.tab >> ${OUTPUT_BASE}/field_mbr_summary.tab
    endif
    ##################################################

    echo " Done pointing $FIELD_INFO[1] at `date`"
    cd $OUTPUT_BASE
    @ I ++
  end
  ###end of main pointing loop
  ####################################################################
  echo "#########Finished the summarise section at `date`"
endif
#############################################################################################






#############################################################################################
#Make the output for Cambridge
if ( "$DO_SECTION_COPY_TO_OUTDIRS" == "TRUE" ) then
  echo "-------------------------------------------------------------------"
  echo "---------------COPYING TO MPE-out----------------------------------"
  echo "To copy the output to the rsync area you will want to do the following:"
  foreach C ( $CATALOGUE_CODE_FOR_EXTRACT_GAL )
    set CAT = ${CATALOGUE_SHORTNAME_LIST[$C]}
    set OUTFILE = "${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_${CAT}_output_source_catalogue.tab.gz"
#    set INFILE = ${OUTPUT_BASE}/field_mbr_summary.tab
    set INFILE = ${OUTPUT_BASE}/assigned_objects.txt
    if ( -e ${OUTFILE} ) rm ${OUTFILE}
    if ( -e  ${INFILE} ) then
      gawk -v cat_code=$C '$1~/^#/ || $12 == cat_code'   ${INFILE} | gzip - > ${OUTFILE}
      # this is a kludge to get around the problem with too many tiles per field being marked for the DBPDG survey. - also swaps grey time for dark time 
#      gawk -v cat_code=$C '$1~/^#/ {print $0} $1!~/^#/ && $12 == cat_code {printf("%5s %9.5f %+9.5f %3d %3d %2d %6.4f %16s %7.1f %7.1f %7.1f %4d %s\n",$1,$2,$3,($4==4?2:0),$5,$6,$7,$8,$10,$9,$11,$12,substr($0,91))}'   ${OUTPUT_BASE}/field_mbr_summary.tab | gzip - > ${OUTFILE}
      echo "rsync -v --password-file=/home/tdwelly/.4mostsync ${OUTFILE}  4MMPE@4most.mpe.mpg.de::4MMPE-out/"
    endif
  end
  echo "-------------------------------------------------------------------"
endif
#############################################################################################

#############################################################################################
#Make a input ascii description file for Paris
# also should get a listing of all unique spectra demanded in each input catalogue

if ( "${DO_SECTION_MAKE_SPECTRA_DESC_FILE}" == "TRUE" ) then

  cd ${OUTPUT_BASE}/
  if ( -e ${OUTPUT_BASE}/scheduling_settings.csh ) then
    echo "Sourcing the scheduling params ${OUTPUT_BASE}/scheduling_settings.csh"
    source ${OUTPUT_BASE}/scheduling_settings.csh
  else
    echo "Cannot find scheduling parameters file: ${OUTPUT_BASE}/scheduling_settings.csh"
    set MOON_PHASE_INDEX_ALL      = 1
    set MOON_PHASE_INDEX_DARK     = 2
    set MOON_PHASE_INDEX_GREY     = 3
    set MOON_PHASE_INDEX_BRIGHT   = 4
    set MOON_PHASE_INDEX_DARKGREY = 5
    set MOON_PHASE_NAME      = ( all  dark  grey bright darkgrey )
    set TEXP_PER_TILE = ( $MOON_PHASE_NAME ) 
    set TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK] = 3600.0
    set TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY] = 3600.0
    set TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT] = 3600.0
  endif

  #find the unique spectral teplates that are used in the input catalogues 
  set I = 1
  while ( $I <=  $#CATALOGUE_FILENAME_LIST )
    set CAT = ${CATALOGUE_SHORTNAME_LIST[$I]}
    set OUTFILE = ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_${CAT}_spectral_template_description_used.txt
    echo "Making $OUTFILE from spectral files named in ${MAIN_BASE}/${CAT}_in/${CATALOGUE_FILENAME_LIST[$I]}"
    if ( -e $OUTFILE ) rm $OUTFILE

echo "#Author, version and date: OpSim (T.Dwelly), vs 1.1, `date`\
#DRS: ${CAT}\
#Column 1: A30  Template spectrum filename\
#Column 2: A2   Resolution mode(HR or LR)\
#Column 3: I5   Exposure time in seconds\
#Column 4: I1   Extended object flag (0=point source,1=extended source)\
#Column 5: I1   Moon phase code, 1=dark,2=grey,3=bright" > $OUTFILE

    #some extra kludges in here to deal with an "A" missing off the fronts of some filenames supplied in the BAO catalogue
#    gawk '$1!~/^#/&&NF>=11{print $11}' ${MAIN_BASE}/${CAT}_in/${CATALOGUE_FILENAME_LIST[$I]} | sort -u | gawk -v cat="$CAT" -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]} " 'BEGIN {split(str_texp,texp," ");} //{fn=$1;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else {extend_flag=1};if(cat=="BAO" && fn~/^GN_v1/){fn=sprintf("A%s",fn)};if(fn~/fit$/){fn=sprintf("%ss",fn)};for(moon=1;moon<=3;moon++){printf("%-30s %2s %5d %d %d\n", fn, res, texp[moon], extend_flag, moon)}}' | sort -u >> $OUTFILE
    #this takes a long time, so comment it out
    if ( "TRUE" == "nTRUE" ) then
      gawk '$1!~/^#/&&NF>=11{print $11}' ${MAIN_BASE}/${CAT}_in/${CATALOGUE_FILENAME_LIST[$I]} | sort -u >   ${MAIN_BASE}/${CAT}_in/${CAT}_unique_spectra_used.txt
    endif

    gawk -v cat="$CAT" -v str_max_runs="2 2 2" -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]}" 'BEGIN {split(str_texp,texp," ");split(str_max_runs,max_runs," ");} //{fn=$1;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else {extend_flag=1};if(cat=="BAO" && fn~/^GN_v1/){fn=sprintf("A%s",fn)};if(fn~/fit$/){fn=sprintf("%ss",fn)};for(moon=1;moon<=3;moon++){for(r=1;r<=max_runs[moon];r++){printf("%-30s %2s %5d %d %d\n", fn, res, r*texp[moon], extend_flag, moon)}}}' ${MAIN_BASE}/${CAT}_in/${CAT}_unique_spectra_used.txt | sort -u >> $OUTFILE

#    if ( "$CAT" == "BAO" ) then
#      gawk '//{if($1~/^GN_v1/){printf("A%s\n",$0)}else {print $0}}' $OUTFILE > ${OUTFILE}.tmp
#      mv  ${OUTFILE}.tmp $OUTFILE
#    endif
    #get the list of all supplied spectral files on the rsync server
    set OUTFILE = ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_${CAT}_spectral_template_description.txt
    echo "Making $OUTFILE from list of spectral files"
    if ( -e $OUTFILE ) rm $OUTFILE

echo "#Author, version and date: OpSim (T.Dwelly), vs 1.0, `date`\
#DRS: ${CAT}\
#Column 1: A30  Template spectrum filename\
#Column 2: A2   Resolution mode(HR or LR)\
#Column 3: I5   Exposure time in seconds\
#Column 4: I1   Extended object flag (0=point source,1=extended source)\
#Column 5: I1   Moon phase code, 1=dark,2=grey,3=bright" > $OUTFILE

#    rsync --password-file=/home/tdwelly/.4mostsync "4M${CAT}@4most.mpe.mpg.de::4M${CAT}-in/*.fits" | gawk -v cat="$CAT" -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]} " 'BEGIN {split(str_texp,texp," ");} //{fn=$5;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else{extend_flag=1};for(moon=1;moon<=3;moon++){printf("%-30s %2s %5d %d %d\n", fn, res, texp[moon], extend_flag, moon)}}' >> $OUTFILE
    rsync --password-file=/home/tdwelly/.4mostsync "4M${CAT}@4most.mpe.mpg.de::4M${CAT}-in/*.fits" | gawk -v cat="$CAT" -v str_max_runs="2 2 2"  -v str_texp="${TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]} ${TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]} " 'BEGIN {split(str_texp,texp," ");split(str_max_runs,max_runs," ");} //{fn=$5;if(cat~/HR/){res="HR"}else{res="LR"}; if(cat~/^Gal/ || (cat=="AGN" && (fn~/_type1/||fn~/_type2/))){extend_flag=0}else{extend_flag=1};for(moon=1;moon<=3;moon++){for(r=1;r<=max_runs[moon];r++){printf("%-30s %2s %5d %d %d\n", fn, res, r*texp[moon], extend_flag, moon)}}}' >> $OUTFILE
     @ I ++
  end

  echo "To copy these to the rsync area you will want to do ONE of the following options:"
  echo "########################################################"
  echo "#Don't do the following!"
  echo "#rsync -v --password-file=/home/tdwelly/.4mostsync ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_*_spectral_template_description.txt      4MMPE@4most.mpe.mpg.de::4MMPE-out/"
  echo "########################################################"
  echo ""
  echo "########################################################"
  echo "rsync -v --password-file=/home/tdwelly/.4mostsync ${MAIN_BASE}/MPE_out/${SIMULATION_CODE_NAME}_*_spectral_template_description_used.txt 4MMPE@4most.mpe.mpg.de::4MMPE-out/"
  echo "########################################################"
  echo ""
endif
#############################################################################################

echo '#############################################################################################'
echo "## run_4MOST_simulations.csh script finished at `date` (I was working in ${OUTPUT_BASE} )"
echo '#############################################################################################'


exit 

#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################
#############################################################################################










#
##############################################################################################
###make a series of diagnostic plots to show the efficiency of the system
#if ( "$DO_SECTION_PLOTS" == "nnnnnnnnnTRUE" ) then
#
#  #first do "tiles vs completeness"
#  set PLOTFILE =  ${OUTPUT_BASE}/diagnostic_plots.plot
#  if ( -e $PLOTFILE ) rm $PLOTFILE
#
#  echo 'set terminal postscript enhanced colour dashed landscape font "Times-Roman,14"\
#set out "'${PLOTFILE:r}.ps'"' > $PLOTFILE
#
#  foreach M ( $MOON_PHASE_LIST ) 
#    set MM = ${MOON_PHASE_CODE[$M]}
#    echo -n 'reset\
#stats_lo = "< gawk -v slash='"'"'/'"'"' -v u='"'"'_'"'"' '"'"'/^#TOTAL/{split(FILENAME,a,slash);split(a[5],b,u);s=b[3];n=($5<=0?1:$5);nok=($5-$6<=0?1:$5-$6);print s,1,$10,($9/n),($9/nok);print s,2,$14,($13/n),($13/nok); print s,3,$18,($17/n),($17/nok); print s,4,$22,($21/n),($21/nok); print _}'"'"' '${OUTPUT_BASE}/${MM}_stats_loRes.tab'" \
#stats_hi = "< gawk -v slash='"'"'/'"'"' -v u='"'"'_'"'"' '"'"'/^#TOTAL/{split(FILENAME,a,slash);split(a[5],b,u);s=b[3];n=($5<=0?1:$5);nok=($5-$6<=0?1:$5-$6);print s,1,$10,($9/n),($9/nok);print s,2,$14,($13/n),($13/nok); print s,3,$18,($17/n),($17/nok); print s,4,$22,($21/n),($21/nok); print _}'"'"' '${OUTPUT_BASE}/${MM}_stats_hiRes.tab'" \
#reset\
#set title "Fractional completeness vs number of Tiles per Field - '${SIM_NAME_GNUPLOT}' - '${MM}'"\
#set xlabel "Number of Tiles executed per Field"\
#set ylabel "Cumulative Fractional Completeness"\
#set key bottom right\
#set xtics 1,1,4\
#plot [0.5:4.5][0:1.05]\\
#     (1.0)          with lines lt 0 t""' >>  $PLOTFILE
#    if ( `gawk '$1~/^#TOTAL/{print $5}' ${OUTPUT_BASE}/${MM}_stats_loRes.tab`  > 0 ) then
#      echo -n ',\\\n     stats_lo       using ($2) : ($4) with linespoints lt 1 lc 1 lw 1 pt 1 ps 1.5 t "low  res - weighted mean",\\
#       stats_lo       using ($2) : ($5) with linespoints lt 4 lc 1 lw 2 pt 6 ps 1.5 t "low  res - filtered mean"' >>  $PLOTFILE
#    endif
#    if ( `gawk '$1~/^#TOTAL/{print $5}' ${OUTPUT_BASE}/${MM}_stats_hiRes.tab`  > 0 ) then
#    echo -n ',\\\n     stats_hi       using ($2) : ($4) with linespoints lt 1 lc 3 lw 1 pt 1 ps 1.5 t "high res - weighted mean",\\
#       stats_hi       using ($2) : ($5) with linespoints lt 4 lc 3 lw 2 pt 6 ps 1.5 t "high res - filtered mean"' >> $PLOTFILE
#    endif
#    echo "" >> $PLOTFILE
#
#    if ( "$CATALOGUE_NAME" == "Merged" ) then
#      foreach C ( $CATALOGUE_CODE_FOR_EXTRACT_GAL )
#        echo -n 'reset\
#stats_lo = "< gawk -v slash='"'"'/'"'"' -v u='"'"'_'"'"' '"'"'/^#TOTAL/{split(FILENAME,a,slash);split(a[5],b,u);s=b[3];n=($5<=0?1:$5);nok=($5-$6<=0?1:$5-$6);print s,1,$10,($9/n),($9/nok);print s,2,$14,($13/n),($13/nok); print s,3,$18,($17/n),($17/nok); print s,4,$22,($21/n),($21/nok); print _}'"'"' '${OUTPUT_BASE}/${MM}_stats_${CATALOGUE_SHORTNAME_LIST[$C]}_loRes.tab'" \
#stats_hi = "< gawk -v slash='"'"'/'"'"' -v u='"'"'_'"'"' '"'"'/^#TOTAL/{split(FILENAME,a,slash);split(a[5],b,u);s=b[3];n=($5<=0?1:$5);nok=($5-$6<=0?1:$5-$6);print s,1,$10,($9/n),($9/nok);print s,2,$14,($13/n),($13/nok); print s,3,$18,($17/n),($17/nok); print s,4,$22,($21/n),($21/nok); print _}'"'"' '${OUTPUT_BASE}/${MM}_stats_${CATALOGUE_SHORTNAME_LIST[$C]}_hiRes.tab'" \
#reset\
#set title "Fractional completeness vs number of Tiles per field - '${SIM_NAME_GNUPLOT}' - '${MM}' - '${CATALOGUE_SHORTNAME_LIST[$C]}'"\
#set xlabel "Number of Tiles executed per Field"\
#set ylabel "Cumulative Fractional Completeness"\
#set key bottom right\
#set xtics 1,1,4\
#plot [0.5:4.5][0:1.05]\\
#     (1.0)          with lines lt 0 t""' >>  $PLOTFILE
#        if ( `gawk '$1~/^#TOTAL/{print $5}' ${OUTPUT_BASE}/${MM}_stats_${CATALOGUE_SHORTNAME_LIST[$C]}_loRes.tab`  > 0 ) then
#          echo -n ',\\\n     stats_lo       using ($2) : ($4) with linespoints lt 1 lc 1 lw 1 pt 1 ps 1.5 t "low  res - weighted mean",\\
#       stats_lo       using ($2) : ($5) with linespoints lt 4 lc 1 lw 2 pt 6 ps 1.5 t "low  res - filtered mean"' >>  $PLOTFILE
#        endif
#        if ( `gawk '$1~/^#TOTAL/{print $5}' ${OUTPUT_BASE}/${MM}_stats_${CATALOGUE_SHORTNAME_LIST[$C]}_hiRes.tab`  > 0 ) then
#          echo -n ',\\\n     stats_hi       using ($2) : ($4) with linespoints lt 1 lc 3 lw 1 pt 1 ps 1.5 t "high res - weighted mean",\\
#       stats_hi       using ($2) : ($5) with linespoints lt 4 lc 3 lw 2 pt 6 ps 1.5 t "high res - filtered mean"' >> $PLOTFILE
#        endif
#        echo "" >> $PLOTFILE
#      end
#    endif
#  end
#
#  gnuplot $PLOTFILE
#  echo "Output plotfile is at ${PLOTFILE:r}.ps"
#endif
##############################################################################################


#exit












##do some extras
#sort -u -k 5 ${OUTPUT_BASE}/pointing?????_pseudo_cat_loRes.txt > ${OUTPUT_BASE}/pseudo_cat_loRes_unique.txt
#foreach TILE_ID ( 01 02 ) 
#  sort -u -k 23 ${OUTPUT_BASE}/fiber_assignment_tile${TILE_ID}.tab | egrep -v '(^#|Null)' > ${OUTPUT_BASE}/fiber_assignment_tile${TILE_ID}_unique.tab
#end
#sort -u -k 11 ${OUTPUT_BASE}/field_mbr_summary.tab  > ${OUTPUT_BASE}/field_mbr_summary_unique.tab




###to merge the catalogues do this:
#if ( "TRUE" == "neverTRUE" ) then
#  if ( -e /home/tdwelly/4most/Merged_in/Merged_v1.0_mock.txt ) rm /home/tdwelly/4most/Merged_in/Merged_v1.0_mock.txt
#  echo '# Column  1: A15 object identification\
## Column  2: I8 object ID\
## Column  3: F9.5 RA, in degrees\
## Column  4: F9.5 Dec, in degrees\
## Column  5: I3 priority, ranging from 0 to 100\
## Column  6: I4 exposure time dark, in minutes\
## Column  7: I4 exposure time grey, in minutes\
## Column  8: I4 exposure time bright, in minutes\
## Column  9: I1 fiber resolution for the object, 1: low res; 2: high res\
## Column 10: F4.1 SDSS rband AB magnitude within 1.5 arcsec fiber\
## Column 11: A30 name of template spectrum\
## Column 12: I4 Catalogue identification Code (1=AGN,2=BAO etc) \
## Column 13+: Additional columns' >  /home/tdwelly/4most/Merged_in/Merged_v1.0_mock.txt
#  set CATALOGUE_LIST = ( AGN BAO Clusters GalHaloLR GalDiskLR GalHaloHR GalDiskHR ) 
#  set I = 1
#  while ( $I <= $#CATALOGUE_LIST ) 
#    set CAT = "${CATALOGUE_LIST[$I]}"
#    gawk -v cat_code=$I '$1!~/^#/ && NF > 1 {printf("%s %4d %s\n",substr($0,1,85),cat_code,substr($0,87))}' /home/tdwelly/4most/${CAT}_in/* >> /home/tdwelly/4most/Merged_in/Merged_v1.0_mock.txt
#    @ I ++
#  end
#endif



















  ##### need to change things from here to incorporate the new hi+lo simultaneous code
  #if ( "$PROCESS_HILO_SEPARATELY" == "TRUE" ) then
  #
  #    if ( -e pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt ) rm pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt
  #    if ( -e pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt ) rm pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt
  #    gawk '$13==2{print $0}' pointing${POINTING_INFO[1]}_pseudo_cat.txt > pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt
  #    gawk '$13==1{print $0}' pointing${POINTING_INFO[1]}_pseudo_cat.txt > pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt
  #    if ( -e pseudo_cat.ascii ) rm pseudo_cat.ascii
  #    printf ' input sources: %5d total (' $NPSEUDO_HILO
  #    #############################################################################################
  #  
  #  
  #    #############################################################################################
  #    # Now do hi resolution first
  #    ln -sf ../fibers_geometry_hiRes.tab ./fibers_geometry.tab
  #  
  #    echo "$TELESCOPE_CODE\
  #$POSITIONER_CODE" >  field_mbr.pars
  #    set NPSEUDO_HI = `wc -l pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt | gawk '{print $1}'`
  #    printf '%5d hi ' $NPSEUDO_HI
  #    if ( $NPSEUDO_HI > 0 ) then
  #      if ( -e pseudo_cat.ascii ) rm pseudo_cat.ascii
  #      ln -s pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt pseudo_cat.ascii
  #      ##########run the field_mbr_highres code
  #      ${CODE_BASE}/field_mbr_tom_v1.exe < field_mbr.pars >& pointing${POINTING_INFO[1]}_field_mbr_highres.log
  #      ######################################
  #      if (`grep -c 'Segmentation fault' pointing${POINTING_INFO[1]}_field_mbr_highres.log` > 0 ) then
  #        echo -n "Error: Segmentation Fault in hiRes" 
  #        # try to shuffle the sources a little and rerun - kludge!    
  #      endif
  #      if ( -e pointing${POINTING_INFO[1]}_field_mbr_assignment_highres.tab       ) rm pointing${POINTING_INFO[1]}_field_mbr_assignment_highres.tab
  #      if ( -e pointing${POINTING_INFO[1]}_field_mbr_summary_highres.tab          ) rm pointing${POINTING_INFO[1]}_field_mbr_summary_highres.tab
  #      if ( -e pointing${POINTING_INFO[1]}_field_mbr_fibres_catalogue_highres.tab ) rm pointing${POINTING_INFO[1]}_field_mbr_fibres_catalogue_highres.tab
  #      if ( -e zuordnung.tab ) mv zuordnung.tab pointing${POINTING_INFO[1]}_field_mbr_assignment_highres.tab
  #      if ( -e summary.tab   ) mv summary.tab   pointing${POINTING_INFO[1]}_field_mbr_summary_highres.tab
  #  #    if ( -e fibres_catalogue.tab  ) mv fibres_catalogue.tab  pointing${POINTING_INFO[1]}_field_mbr_fibres_catalogue_highres.tab
  #      if ( -e pseudo_cat.ascii ) rm pseudo_cat.ascii
  #      if ( -e fibres_catalogue.tab ) rm fibres_catalogue.tab
  #    else if ( $NPSEUDO_HI == 0 )  then
  #      rm pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt
  #    endif
  #  
  #    #now do the low resolution tile
  #    ln -sf ../fibers_geometry_loRes.tab ./fibers_geometry.tab
  #    set NPSEUDO_LO = `wc -l pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt | gawk '{print $1}'`
  #    printf '%5d lo) ' $NPSEUDO_LO
  #    if ( $NPSEUDO_LO > 0 ) then
  #      if ( -e pseudo_cat.ascii ) rm pseudo_cat.ascii
  #      ln -s pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt pseudo_cat.ascii
  #      ##########run the field_mbr_lowres code
  #      ${CODE_BASE}/field_mbr_tom_v1.exe < field_mbr.pars >& pointing${POINTING_INFO[1]}_field_mbr_lowres.log
  #      if ( -e pointing${POINTING_INFO[1]}_field_mbr_assignment_lowres.tab       ) rm pointing${POINTING_INFO[1]}_field_mbr_assignment_lowres.tab
  #      if ( -e pointing${POINTING_INFO[1]}_field_mbr_summary_lowres.tab          ) rm pointing${POINTING_INFO[1]}_field_mbr_summary_lowres.tab
  #      if ( -e pointing${POINTING_INFO[1]}_field_mbr_fibres_catalogue_lowres.tab ) rm pointing${POINTING_INFO[1]}_field_mbr_fibres_catalogue_lowres.tab
  #      if ( -e zuordnung.tab ) mv zuordnung.tab pointing${POINTING_INFO[1]}_field_mbr_assignment_lowres.tab
  #      if ( -e summary.tab   ) mv summary.tab   pointing${POINTING_INFO[1]}_field_mbr_summary_lowres.tab
  #  #    if ( -e fibres_catalogue.tab ) mv fibres_catalogue.tab pointing${POINTING_INFO[1]}_field_mbr_fibres_catalogue_lowres.tab
  #      if ( -e pseudo_cat.ascii ) rm pseudo_cat.ascii
  #      if ( -e fibres_catalogue.tab ) rm fibres_catalogue.tab
  #    else if ( $NPSEUDO_LO == 0 )  then
  #      rm pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt
  #    endif
  #    if ( -e field_mbr.pars ) rm field_mbr.pars
  #  
  #    #reformat the catalogue columns, and combine high and low resolution catalogues
  #    if ( -e pointing${POINTING_INFO[1]}_fiber_assignment.tab ) rm pointing${POINTING_INFO[1]}_fiber_assignment.tab
  #    set ASSIGN_LIST = ()
  #    set ASSIGN2_LIST = ()
  #    if ( -e pointing${POINTING_INFO[1]}_field_mbr_assignment_lowres.tab  ) set ASSIGN_LIST = ( $ASSIGN_LIST pointing${POINTING_INFO[1]}_field_mbr_assignment_lowres.tab ) 
  #    if ( -e pointing${POINTING_INFO[1]}_field_mbr_assignment_highres.tab ) set ASSIGN_LIST = ( $ASSIGN_LIST pointing${POINTING_INFO[1]}_field_mbr_assignment_highres.tab )
  #  
  #    if ( $#ASSIGN_LIST > 0 ) then 
  #      foreach TILE ( $TILE_LIST ) 
  #        set TILE_ID = `printf '%02d' $TILE`
  #        gawk -v tile=$TILE -v cat_code="$CATALOGUE_CODE" -v nan_str="$NAN_STR" 'FILENAME~/conditions/ {n=split($1,a,"=");if(n==2){par[a[1]]=a[2];}} \
  #FILENAME~/assignment_lowres/ {res=1} \
  #FILENAME~/assignment_highres/ {res=2} \
  #FILENAME~/assignment/ && $1=="Observation" && $2=="tile" {this_tile=$4;if(this_tile>tile){exit}} \
  #FILENAME~/assignment/ {\
  #  if(NF>9 && this_tile==tile){\
  #    printf("%4s %9.5f %+9.5f %2d %10s %10s %7.1f %4.2f %5.1f %5.2f %5.2f %1d %4d %1d %9.3f %9.3f %9.3f %9.3f %5.3f %9.3f %9.3f %4s %s\n", par["POINTING_ID"], par["POINTING_CENTRE_RA"], par["POINTING_CENTRE_DEC"],tile,par["TILE_START_DATE"],par["TILE_START_TIME"],par["TILE_EXPOSURE_TIME"],par["TILE_MOON_PHASE"],par["TILE_MOON_DISTANCE"],par["TILE_AIRMASS"],par["TILE_SEEING"],par["TILE_CLOUD_COVER"], $2, res, $3, $4, $7, $8, $9, $10, $11, cat_code, substr($0,89))}\
  #  else if(NF==4 && this_tile==tile && $1!="Observation"){\
  #    printf("%4s %9.5f %+9.5f %2d %10s %10s %7.1f %4.2f %5.1f %5.2f %5.2f %1d %4d %1d %9.3f %9.3f %9.3f %9.3f %5s %9.3f %9.3f %4d %15s %8d %9s %9s %3d %4d %4d %4d %1d %4s %30s\n", par["POINTING_ID"], par["POINTING_CENTRE_RA"], par["POINTING_CENTRE_DEC"],tile,par["TILE_START_DATE"],par["TILE_START_TIME"],par["TILE_EXPOSURE_TIME"],par["TILE_MOON_PHASE"],par["TILE_MOON_DISTANCE"],par["TILE_AIRMASS"],par["TILE_SEEING"],par["TILE_CLOUD_COVER"], $2, res, $3, $4, $3, $4, nan_str, 0.0, 0.0, -1, nan_str, "-1", nan_str, nan_str, "-1", "-1", "-1", "-1", res, nan_str, "-")}\
  #}' pointing${POINTING_INFO[1]}_conditions_tile${TILE_ID}.txt ${ASSIGN_LIST}  > pointing${POINTING_INFO[1]}_fiber_assignment_tile${TILE_ID}.tab
  #        set ASSIGN2_LIST = ( $ASSIGN2_LIST  pointing${POINTING_INFO[1]}_fiber_assignment_tile${TILE_ID}.tab ) 
  #        cat pointing${POINTING_INFO[1]}_fiber_assignment_tile${TILE_ID}.tab >> ${OUTPUT_BASE}/fiber_assignment_tile${TILE_ID}.tab
  #        if ( -e pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt ) cat pointing${POINTING_INFO[1]}_pseudo_cat_loRes.txt >> ${OUTPUT_BASE}/pseudo_cat_loRes.txt
  #        if ( -e pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt ) cat pointing${POINTING_INFO[1]}_pseudo_cat_hiRes.txt >> ${OUTPUT_BASE}/pseudo_cat_hiRes.txt
  #      end
  #    endif
  #    #now summarise the summary files
  #    set SUMMARY_LIST = ()
  #    if ( -e pointing${POINTING_INFO[1]}_field_mbr_summary_lowres.tab )  set SUMMARY_LIST = ( $SUMMARY_LIST pointing${POINTING_INFO[1]}_field_mbr_summary_lowres.tab ) 
  #    if ( -e pointing${POINTING_INFO[1]}_field_mbr_summary_highres.tab ) set SUMMARY_LIST = ( $SUMMARY_LIST pointing${POINTING_INFO[1]}_field_mbr_summary_highres.tab ) 
  #    if ( $#SUMMARY_LIST > 0 ) then
  #  #    gawk  '$1~/^source/{src_num=$2;prio=$5;ntiles=$8;nfib=$14;flag[1]=0;flag[2]=0;flag[3]=0;flag[4]=0} NF == 1 {flag[int($1)]=1} $1!~/^source/ && NF > 1 {printf("%4d %5.3f %3d %2d %1d %1d %1d %1d %s\n", src_num, prio, ntiles, nfib, flag[1], flag[2], flag[3], flag[4], $0)} ' ${OUTPUT_BASE}/pointing${POINTING_INFO[1]}_field_mbr_summary_lowres.tab 
  #      gawk -v cat_code="$CATALOGUE_CODE" 'BEGIN {last=""} \
  #FILENAME~/conditions/ {n=split($1,a,"=");if(n==2){par[a[1]]=a[2];}} \
  #FILENAME~/summary_lowres/ {res=1} \
  #FILENAME~/summary_highres/ {res=2} \
  #FILENAME~/summary/ && $1~/^source/{if(last!=""){printf("%4s %9.5f %+9.5f %1d %5.3f %3d %2d %1d %1d %1d %1d %4s %s\n", par["POINTING_ID"], par["POINTING_CENTRE_RA"], par["POINTING_CENTRE_DEC"], res, prio, ntiles, nfib, flag[1], flag[2], flag[3], flag[4], cat_code, last)};last="";src_num=$2;prio=$5;ntiles=$8;nfib=$14;flag[1]=0;flag[2]=0;flag[3]=0;flag[4]=0;} \
  #FILENAME~/summary/ && NF == 1 {flag[int($1)]=1} FILENAME~/summary/ && $1!~/^source/ && NF > 1 {last=$0} \
  #END {if(last!=""){printf("%4s %9.5f %+9.5f %1d %5.3f %3d %2d %1d %1d %1d %1d %4s %s\n", par["POINTING_ID"], par["POINTING_CENTRE_RA"], par["POINTING_CENTRE_DEC"], res, prio, ntiles, nfib, flag[1], flag[2], flag[3], flag[4], cat_code, last)}}' pointing${POINTING_INFO[1]}_conditions_tile01.txt ${SUMMARY_LIST} > pointing${POINTING_INFO[1]}_field_mbr_summary.tab
  #      cat pointing${POINTING_INFO[1]}_field_mbr_summary.tab >> ${OUTPUT_BASE}/field_mbr_summary.tab
  #    endif
  #
  #
  #  ##############################################################################################
  #  #this is where we do the assignment for the hi + lo resolution fibers in one pass 
  #endif






#
##############################################################################################
#if ( "$DO_SECTION_FIELDS" == "TRUE" ) then
#  #define positions on the sky
#  #need to cover entire southern sky from 0h < RA < 24h, -75deg < dec < +20deg
#  # use the methods of http://www2.research.att.com/~njas/icosahedral.codes/
#
#  #If you use any of these arrangements, please acknowledge this source. For example, you might say something like the following.
#  #This arrangement of points is taken from Reference [1], where Reference [1] is:
#  #R. H. Hardin, N. J. A. Sloane and W. D. Smith, Tables of spherical codes with icosahedral symmetry, published electronically at http://www.research.att.com/~njas/icosahedral.codes/.     # results are x,y,z coords of on teh unit sphere
#  # one coord per line
#  # inner radius (radius of largest circle that fits inside hexagon) is given by r_in = r*cos(pi/6) = r * sqrt(3)/2 
#  # r_in = 1.2990 deg for r=1.5deg , and r_in = 0.866 deg for r=1.0deg and r_in = 1.09 deg for r=1.26deg and  r_in = 1.0825 deg for r=1.250deg
#  # ie we need the Hardin 'coverage' that gives just under that as a minimum separation
#  # from the blanton site:
#  # Icosahedral best coverings
#  # dim numpts  deepest hole (or covering radius, in degrees)
#  #   3   4092   1.988228662936 15,8
#  #   3   6912   1.528963079847 19,11
#  #   3   7002   1.519288003357 20,10
#  #   3   7682   1.449774552065 16,16
#  #   3   8192   1.403881943807 18,15
#  #   3   8192   1.405300351294 27,3
#  #   3   8672   1.364413944909 17,17
#  #   3   9722   1.288545767384 18,18
#  # ....  
#  #   3  10832   1.220670083820 19,19
#  #   3  12002   1.159587155975 20,20
#  #   3  13232   1.104325930573 21,21
#  #   3  14522   1.054091991326 22,22
#  # ...
#  #   3  21872   0.858770306968 27,27
#
#####test      gawk -f ~/.gawkrc -v ra_min=$RA_MIN -v dec_min=$DEC_MIN -v ra_max=$RA_MAX -v dec_max=$DEC_MAX  --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],sqrt((x[i]*x[i])+(y[i]*y[i])+(z[i]*z[i])));ra=(phi*180./pi)+180.; dec=(theta*180./pi)-90.;if(ra>=ra_min&&dec>=dec_min&&ra<=ra_max&&dec<=dec_max) {printf("%05d %12.6f %12.6f %12.5f %12.5f %12.5f \n", i, ra,dec, x[i], y[i], z[i])}}} ' ${FILE} > ${FILE:r}_format.txt
#  #just process the input Hardin file to ake it readable (one set of coords per line)  
##  gawk -f ~/.gawkrc -v ra_min=$RA_MIN -v dec_min=$DEC_MIN -v ra_max=$RA_MAX -v dec_max=$DEC_MAX  --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],sqrt((x[i]*x[i])+(y[i]*y[i])+(z[i]*z[i])));ra=(phi*180./pi)+180.; dec=(theta*180./pi)-90.;if(ra>=ra_min&&dec>=dec_min&&ra<=ra_max&&dec<=dec_max) {printf("%05d %12.6f %12.6f\n", i, ra, dec)}}}' ${HARDIN_FILE} > ${FIELDS_FILE:r}_raw.txt
#  gawk -f ~/.gawkrc --source 'BEGIN {i=0;j=0} NF==1{i++;if(i%3==1){j++;x[j]=$1} else if (i%3==2) {y[j]=$1} else if (i%3==0) {z[j]=$1}} END {for(i=1;i<=j;i++){phi=atan2(y[i],x[i]);theta=acos2(z[i],sqrt((x[i]*x[i])+(y[i]*y[i])+(z[i]*z[i])));ra=(phi*180./pi)+180.; dec=(theta*180./pi)-90.;printf("%05d %12.6f %12.6f\n", i, ra, dec)}}' ${HARDIN_FILE} > ${FIELDS_FILE:r}_raw.txt
#
#  ##############################################
#  #add the Galactic coords
#  set TEMP1 = temp1_${HOST}_$$.txt
#  set TEMP2 = temp2_${HOST}_$$.txt
#  if ( -e $TEMP1 ) rm $TEMP1
#  if ( -e $TEMP2 ) rm $TEMP2
#  gawk '//{printf("%12s %12s J2000\n", $2, $3)}' ${FIELDS_FILE:r}_raw.txt > $TEMP1
#  skycoor -g @${TEMP1} | gawk '//{printf("%10s %10s\n", $1, $2)}'  > $TEMP2
#  paste -d ' ' "${FIELDS_FILE:r}_raw.txt" "${TEMP2}" > ${TEMP1} 
#  mv ${TEMP1} ${FIELDS_FILE:r}_raw.txt
#  if ( -e $TEMP2 ) rm $TEMP2
#  ##############################################
#
#  ##############################################
#  # Add the minumum airmass column => use the sec(zenith_angle) approx, and then sort
#  gawk -f ~/.gawkrc  -v lat_dms="$SITE_LATITUDE"  --source 'BEGIN {split(lat_dms,a,":");s=(a[1]>0.?+1.:-1.);lat_deg=a[1]+(s*a[2]/60.)+(s*a[3]/3600.)} {printf("%05d %12s %12s %10s %10s %8.3f\n", $1, $2, $3, $4, $5, secd($3-lat_deg))}' ${FIELDS_FILE:r}_raw.txt  | sort -g -k 5 > ${TEMP1}
#  mv ${TEMP1} ${FIELDS_FILE:r}_raw.txt
#  ##############################################
#  gawk '//{printf("%05d %12.6f %12.6f %8s\n",$1,$2,$3,"nan")}' ${FIELDS_FILE:r}_raw.txt | sort -n -k 3 > ${FIELDS_FILE:r}_all.txt
#
#
#  #############################################################################################
#  #this needs to be beefed up to include properly the scheduling parameters.
#  # aim to produce a filtered list of fields/tiles that can be observed in the given timescale
#
#  #Here we aim to cover the whole sky area with the exposure times per tile that that implies
#  echo "Using Survey Priority type: $SURVEY_PRIORITY_TYPE"
#  if ( "$SURVEY_PRIORITY_TYPE" == "AREA" ) then
#    #this should be fairly universal
#    set SKY_HOURS[$MOON_PHASE_INDEX_ALL] = `gawk -v sd=$SURVEY_DURATION -v smdt=$SURVEY_MEAN_DARK_TIME -v ofn=$OVERHEADS_FAULT_NIGHTS -v omn=$OVERHEADS_MAINTENANCE_NIGHTS -v esfu=$ENVIRONMENT_SEEING_FRACTION_USEABLE -v ecfp=$ENVIRONMENT_CLOUD_FRACTION_PHOTOMETRIC -v ecft=$ENVIRONMENT_CLOUD_FRACTION_THIN -v ooc=$OVERHEADS_OVERHEAD_CALIBRATION 'BEGIN {hrs=(sd*(365.-ofn-omn)*esfu*(ecfp+ecft)*(smdt-(ooc/60.))); printf("%.1f\n",hrs)}'`
#
#    set SKY_HOURS[$MOON_PHASE_INDEX_BRIGHT]   = `gawk -v hrs=$SKY_HOURS[$MOON_PHASE_INDEX_ALL] -v emf=$ENVIRONMENT_MOON_FRACTION_BRIGHT 'BEGIN { printf("%.1f\n",hrs*emf)}'`
#    set SKY_HOURS[$MOON_PHASE_INDEX_GREY]     = `gawk -v hrs=$SKY_HOURS[$MOON_PHASE_INDEX_ALL] -v emf=$ENVIRONMENT_MOON_FRACTION_GREY   'BEGIN { printf("%.1f\n",hrs*emf)}'`
#    set SKY_HOURS[$MOON_PHASE_INDEX_DARK]     = `gawk -v hrs=$SKY_HOURS[$MOON_PHASE_INDEX_ALL] -v emf=$ENVIRONMENT_MOON_FRACTION_DARK   'BEGIN { printf("%.1f\n",hrs*emf)}'`
#    set SKY_HOURS[$MOON_PHASE_INDEX_DARKGREY] = `gawk -v hrs=$SKY_HOURS[$MOON_PHASE_INDEX_ALL] -v emfg=$ENVIRONMENT_MOON_FRACTION_GREY -v emfd=$ENVIRONMENT_MOON_FRACTION_DARK 'BEGIN {printf("%.1f\n",hrs*(emfg+emfd))}'`
#
#
#    echo "Using Survey strategy type: $SURVEY_STRATEGY_TYPE"
#    echo "Total     on-sky hours: $SKY_HOURS[$MOON_PHASE_INDEX_ALL]"
#    echo "Bright    on-sky hours: $SKY_HOURS[$MOON_PHASE_INDEX_BRIGHT]"
#    echo "Grey      on-sky hours: $SKY_HOURS[$MOON_PHASE_INDEX_GREY]"
#    echo "Dark      on-sky hours: $SKY_HOURS[$MOON_PHASE_INDEX_DARK]"
#    echo "Dark/Grey on-sky hours: $SKY_HOURS[$MOON_PHASE_INDEX_DARKGREY]"
#    if ( "$SURVEY_STRATEGY_TYPE" == "INTERNAL" ) then
#      gawk '//{printf("%05d %12.6f %12.6f %8s\n",$1,$2,$3,"nan")}' ${FIELDS_FILE:r}_raw.txt | sort -n -k 3 > ${FIELDS_FILE:r}_all.txt
#      set NUM_FIELDS[$MOON_PHASE_INDEX_ALL] = `wc -l ${FIELDS_FILE:r}_all.txt | gawk '{print $1}'`
#
#    #########################################################################
#    else if ( "$SURVEY_STRATEGY_TYPE" == "FLAT" ) then
#      # this is a survey that covers the available sky uniformly in bright time and grey/dark separately 
#      set NUM_FIELDS[$MOON_PHASE_INDEX_ALL] = `wc -l ${FIELDS_FILE:r}_raw.txt | gawk '{print $1}'`
#      foreach M ( $MOON_PHASE_LIST )  
#        gawk '//{printf("%05d %12.6f %12.6f\n",$1,$2,$3)}' ${FIELDS_FILE:r}_raw.txt | sort -n -k 3 > ${FIELDS_FILE:r}_${MOON_PHASE_CODE[$M]}.txt
#        set NUM_FIELDS[$M]   = `wc -l ${FIELDS_FILE:r}_${MOON_PHASE_CODE[$M]}.txt | gawk '{print $1}'`
#      end
#      set NUM_FIELDS[${MOON_PHASE_INDEX_DARK}]     = `gawk -v nfdg=$NUM_FIELDS[${MOON_PHASE_INDEX_DARKGREY}] -v emfd=$ENVIRONMENT_MOON_FRACTION_DARK -v emfg=$ENVIRONMENT_MOON_FRACTION_GREY 'BEGIN {printf("%.0f",nfdg*emfd/(emfd+emfg))}'` 
#      set NUM_FIELDS[${MOON_PHASE_INDEX_GREY}]     = `gawk -v nfdg=$NUM_FIELDS[${MOON_PHASE_INDEX_DARKGREY}] -v emfd=$ENVIRONMENT_MOON_FRACTION_DARK -v emfg=$ENVIRONMENT_MOON_FRACTION_GREY 'BEGIN {printf("%.0f",nfdg*emfg/(emfd+emfg))}'` 
#
#      set NUM_TILES[${MOON_PHASE_INDEX_DARK}]      = ${SURVEY_FLAT_NUM_PASSES_DARKGREY}
#      set NUM_TILES[${MOON_PHASE_INDEX_GREY}]      = ${SURVEY_FLAT_NUM_PASSES_DARKGREY}
#      set NUM_TILES[${MOON_PHASE_INDEX_BRIGHT}]    = ${SURVEY_FLAT_NUM_PASSES_BRIGHT}
#      set NUM_TILES[${MOON_PHASE_INDEX_DARKGREY}]  = ${SURVEY_FLAT_NUM_PASSES_DARKGREY}
#    #########################################################################
#    else if ( "$SURVEY_STRATEGY_TYPE" == "DBPDG" ) then
#      #DBPDG = Disk Bright Poles Dark Grey
#      # this is a survey that covers the non-disk sky uniformly in grey/dark time and covers the galactic disk in bright time 
#      set NUM_FIELDS[$MOON_PHASE_INDEX_ALL] = `wc -l ${FIELDS_FILE:r}_raw.txt | gawk '{print $1}'`
#      foreach M ( $MOON_PHASE_LIST )  
#        gawk -v moon_phase="$MOON_PHASE_NAME[$M]" -v lat_min=$SURVEY_DBPDG_DISK_LAT_MIN -v lat_max=$SURVEY_DBPDG_DISK_LAT_MAX  ' {if(moon_phase=="bright" && $5>=lat_min && $5<=lat_max){printf("%05d %12.6f %12.6f\n",$1,$2,$3)}; if(moon_phase=="darkgrey" && ($5<lat_min || $5>lat_max)){printf("%05d %12.6f %12.6f\n",$1,$2,$3)}}' ${FIELDS_FILE:r}_raw.txt  | sort -n -k 3 > ${FIELDS_FILE:r}_${MOON_PHASE_CODE[$M]}.txt
##        gawk -v moon_phase="$MOON_PHASE_NAME[$M]" -v lat_min=$SURVEY_DBPDG_DISK_LAT_MIN -v lat_max=$SURVEY_DBPDG_DISK_LAT_MAX  ' {if(moon_phase=="bright" && $5>=lat_min && $5<=lat_max){printf("%05d %12.6f %12.6f\n",$1,$2,$3)}; if(moon_phase=="darkgrey" && ($5<lat_min || $5>lat_max)){printf("%05d %12.6f %12.6f %8s\n",$1,$2,$3,"nan")}}' ${FIELDS_FILE:r}_raw.txt  | sort -n -k 3 > ${FIELDS_FILE:r}_${MOON_PHASE_CODE[$M]}.txt
#        set NUM_FIELDS[$M]   = `wc -l ${FIELDS_FILE:r}_${MOON_PHASE_CODE[$M]}.txt | gawk '{print $1}'`
#      end
#      set NUM_FIELDS[${MOON_PHASE_INDEX_DARK}]     = `gawk -v nfdg=$NUM_FIELDS[${MOON_PHASE_INDEX_DARKGREY}] -v emfd=$ENVIRONMENT_MOON_FRACTION_DARK -v emfg=$ENVIRONMENT_MOON_FRACTION_GREY 'BEGIN {printf("%.0f",nfdg*emfd/(emfd+emfg))}'` 
#      set NUM_FIELDS[${MOON_PHASE_INDEX_GREY}]     = `gawk -v nfdg=$NUM_FIELDS[${MOON_PHASE_INDEX_DARKGREY}] -v emfd=$ENVIRONMENT_MOON_FRACTION_DARK -v emfg=$ENVIRONMENT_MOON_FRACTION_GREY 'BEGIN {printf("%.0f",nfdg*emfg/(emfd+emfg))}'` 
#
#      set NUM_TILES[${MOON_PHASE_INDEX_BRIGHT}]    = ${SURVEY_DBPDG_NUM_PASSES_BRIGHT}
#      set NUM_TILES[${MOON_PHASE_INDEX_DARKGREY}]  = ${SURVEY_DBPDG_NUM_PASSES_DARKGREY}
#      set NUM_TILES[${MOON_PHASE_INDEX_DARK}]      = ${SURVEY_DBPDG_NUM_PASSES_DARKGREY}
#      set NUM_TILES[${MOON_PHASE_INDEX_GREY}]      = ${SURVEY_DBPDG_NUM_PASSES_DARKGREY}
#
#    #########################################################################
#    else
#      echo "I do not know this type of survey strategy: $SURVEY_STRATEGY_TYPE"
#      exit 1
#    endif
#    #########################################################################
#
#    set TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]   = `gawk -v hrs=${SKY_HOURS[$MOON_PHASE_INDEX_BRIGHT]}   -v nfields=${NUM_FIELDS[$MOON_PHASE_INDEX_BRIGHT]}   -v sfnp=${SURVEY_FLAT_NUM_PASSES_BRIGHT}   -v ootpt=${OVERHEADS_OVERHEAD_TOTAL_PER_TILE} 'BEGIN {ntiles=nfields*sfnp; printf("%.0f",(3600.*hrs - 60.*ntiles*ootpt)/ntiles)}'`
#    set TEXP_PER_TILE[$MOON_PHASE_INDEX_DARKGREY] = `gawk -v hrs=${SKY_HOURS[$MOON_PHASE_INDEX_DARKGREY]} -v nfields=${NUM_FIELDS[$MOON_PHASE_INDEX_DARKGREY]} -v sfnp=${SURVEY_FLAT_NUM_PASSES_DARKGREY} -v ootpt=${OVERHEADS_OVERHEAD_TOTAL_PER_TILE} 'BEGIN {ntiles=nfields*sfnp; printf("%.0f",(3600.*hrs - 60.*ntiles*ootpt)/ntiles)}'`
#    set TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]     = $TEXP_PER_TILE[$MOON_PHASE_INDEX_DARKGREY]
#    set TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]     = $TEXP_PER_TILE[$MOON_PHASE_INDEX_DARKGREY]
#
#    echo "Number of AllSky   fields: $NUM_FIELDS[$MOON_PHASE_INDEX_ALL]"
#    echo "Number of Bright   fields: $NUM_FIELDS[$MOON_PHASE_INDEX_BRIGHT]"
#    echo "Number of GreyDark fields: $NUM_FIELDS[$MOON_PHASE_INDEX_DARKGREY]"
#    echo "Number of Grey     fields: $NUM_FIELDS[$MOON_PHASE_INDEX_GREY]"
#    echo "Number of Dark     fields: $NUM_FIELDS[$MOON_PHASE_INDEX_DARK]"
#    echo "Texp(s) per Bright   tile: $TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]"
#    echo "Texp(s) per Grey     tile: $TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]"
#    echo "Texp(s) per Dark     tile: $TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]"
#
#    #write the params to a file so we can use them later    
#    if ( -e ${OUTPUT_BASE}/scheduling_settings.csh ) rm ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]   = '"$TEXP_PER_TILE[$MOON_PHASE_INDEX_BRIGHT]"    >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set TEXP_PER_TILE[$MOON_PHASE_INDEX_DARKGREY] = '"$TEXP_PER_TILE[$MOON_PHASE_INDEX_DARKGREY]"  >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]     = '"$TEXP_PER_TILE[$MOON_PHASE_INDEX_GREY]"      >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]     = '"$TEXP_PER_TILE[$MOON_PHASE_INDEX_DARK]"      >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_TILES[$MOON_PHASE_INDEX_BRIGHT]       = '"$NUM_TILES[$MOON_PHASE_INDEX_BRIGHT]"        >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_TILES[$MOON_PHASE_INDEX_DARKGREY]     = '"$NUM_TILES[$MOON_PHASE_INDEX_DARKGREY]"      >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_TILES[$MOON_PHASE_INDEX_GREY]         = '"$NUM_TILES[$MOON_PHASE_INDEX_GREY]"          >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_TILES[$MOON_PHASE_INDEX_DARK]         = '"$NUM_TILES[$MOON_PHASE_INDEX_DARK]"          >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_FIELDS[$MOON_PHASE_INDEX_BRIGHT]      = '"$NUM_FIELDS[$MOON_PHASE_INDEX_BRIGHT]"       >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_FIELDS[$MOON_PHASE_INDEX_DARKGREY]    = '"$NUM_FIELDS[$MOON_PHASE_INDEX_DARKGREY]"     >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_FIELDS[$MOON_PHASE_INDEX_GREY]        = '"$NUM_FIELDS[$MOON_PHASE_INDEX_GREY]"         >> ${OUTPUT_BASE}/scheduling_settings.csh
#    echo 'set NUM_FIELDS[$MOON_PHASE_INDEX_DARK]        = '"$NUM_FIELDS[$MOON_PHASE_INDEX_DARK]"         >> ${OUTPUT_BASE}/scheduling_settings.csh
#
#  #############################################################################################
#  else
#    echo "I do not know this type of survey prioritisation: $SURVEY_PRIORITY_TYPE"
#    exit 1
#  endif
#endif
##############################################################################################

##############################################################################################
#if ( "$DO_SECTION_COPY_TO_OUTDIRS" == "NEVER_TRUE" ) then
#  set OUT_PREFIX = "${CATALOGUE_NAME}_${TELESCOPE_CODE_NAME}_${POSITIONER_CODE_NAME}_"
#  set IN_BASE = ${BASE}/output_${SIM_NAME}
#  set OUT_BASE = /home/tdwelly/4most/${CATALOGUE_NAME}_out/
#  
#  if ( -d $IN_BASE && -d $OUT_BASE ) then
#    echo "Copying results over to the standard ouput data areas: $IN_BASE -> $OUT_BASE"
#    echo "This is a brief description of the files in this directory\
#The '*' symbol represents a string specifying the {CATALOGUE_NAME}_{TELESCOPE}_{POSITIONER}\
#Where {CATALOGUE}  is one of: AGN BAO Clusters GalHaloLR GalDiskLR GalHaloHR GalDiskHR \
#where {TELESCOPE}  is one of: VISTA NTT\
#where {POSITIONER} will be something like: Echidna LAMOST EchidnaNew1 etc \
#- '*_fiber_assignment_alltiles.tab.gz' \
#  This file contains the results of the fiber positioning algorithm\
#  Each row gives the details for a single fiber in a single 'tile' in a single 'field'\
#  If a fiber has been assigned to a target then the input details (taken from the mock catalogue) are included\
#  Fibers that were not assigned to a science target are included for completeness\
#\
#- '*_field_mbr_summary.tab.gz'\
#  This file includes one row for each mock target that was considered for inclusion in the observations of any tile\
#  Targets that were not observed are also included for completeness\
#  The input details (taken from the mock catalogue) are included\
#\
#- '*_stats_loRes.tab'\
#  This file includes a statistical summary of the completeness of the assignment process for the low resolution fibers \
#  one entry is given for each tile and then a summary line is given at the end for the survey as a whole\
#\
#- '*_stats_hiRes.tab'\
#  This file includes a statistical summary of the completeness of the assignment process for the high resolution fibers \
#  one entry is given for each tile and then a summary line is given at the end for the survey as a whole\
#" > ${OUT_BASE}/README
#    gzip -c ${IN_BASE}/fiber_assignment_alltiles.tab > ${OUT_BASE}/${OUT_PREFIX}fiber_assignment_alltiles.tab.gz
#    gzip -c ${IN_BASE}/field_mbr_summary.tab        > ${OUT_BASE}/${OUT_PREFIX}field_mbr_summary.tab.gz
#    cp -p ${IN_BASE}/stats_loRes.tab ${OUT_BASE}/${OUT_PREFIX}stats_loRes.tab
#    cp -p ${IN_BASE}/stats_hiRes.tab ${OUT_BASE}/${OUT_PREFIX}stats_hiRes.tab
#
#  endif
#endif
##############################################################################################


#foreach CAT ( "AGN"  "BAO" "GalHaloLR"  "GalDiskLR"  "Clusters"  "GalHaloHR"  "GalDiskHR" ) 
#  gawk '$1!~/^#/{print $1}' /home/tdwelly/4most/MPE_out/test_${CAT}_spectral_template_description_used.txt | sort -u >  /home/tdwelly/4most/${CAT}_in/${CAT}_unique_spectra_used.txt
#end
