//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_OB_lib.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//#define CVS_REVISION "$Revision: 1.1 $"
//#define CVS_DATE     "$Date: 2015/09/09 10:10:30 $"
//--    Note: Definitions are in the corresponding header file OpSim_OB_lib.h
//--    Description:
//--      This module is used to determine colisions between OBs etc
//--

//#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>
//#include <string.h>
//#include <time.h>

#include "OpSim_OB_lib.h"
#include <assert.h>   //needed for debugging


#define ERROR_PREFIX   "#-OpSim_OB_lib.c:Error:  "
#define WARNING_PREFIX "#-OpSim_OB_lib.c:Warning:"
#define COMMENT_PREFIX "#-OpSim_OB_lib.c:Comment:"
#define DEBUG_PREFIX   "#-OpSim_OB_lib.c:DEBUG:  "


////////////////////////////////////////////////////////////////////////
//functions
int OpSim_OB_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}




////////////////////////
/////////// OB_struct functions /////////////
int OB_struct_init (OB_struct *pOB, int tiles_per_OB, int assigns_per_tile)
{
  if ( pOB == NULL ) return 1;

  //set the contents to their initial values 
  pOB->array_length = (int) 0;
  pOB->pTile = (tile_struct*) NULL;           //assume that this doesn't point to anything already

  pOB->UID  = (int) -1;
  pOB->time_gross = (float) NAN;
  pOB->status = (int) OB_STATUS_NULL;
  pOB->JD_observed = (double) NAN;

  pOB->nTiles = (int) 0;  //we will fill this up later

  //now, if needed, then must make space for the array of tile_struct structures
  if (  tiles_per_OB > 0 )
  {
    int i;
    if ( (pOB->pTile = (tile_struct*) malloc(sizeof(tile_struct) * tiles_per_OB)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
    pOB->array_length = (int) tiles_per_OB ;
    
    //now initialise the array of tiles
    for (i=0;i<pOB->array_length;i++)
    {
      tile_struct *pTile = (tile_struct*) &(pOB->pTile[i]);
      if ( tile_struct_init ((tile_struct*) pTile, (int) assigns_per_tile))
      {
        fprintf(stderr, "%s Problem initialising tile_struct\n", ERROR_PREFIX);
        return 1;
      }
    }
  }
  return 0; 
}

////////////////////////
int OB_struct_free (OB_struct **ppOB)
{
  if ( ppOB == NULL ) return 1;
  if ( *ppOB )
  {
    //free the array of tile structures
    if ( (*ppOB)->pTile )
    {
      int i;
      for ( i=0;i<(*ppOB)->nTiles;i++ )
      {
        tile_struct *pTile = (tile_struct*) &((*ppOB)->pTile[i]);
        if (tile_struct_free ((tile_struct**) &pTile)) return 1;
      }
    }
    //now free the OB struct itself
    free(*ppOB);
    *ppOB = NULL;
  }
  return 0; 
}

//write the info from an OB struct to a file
int OB_struct_write (OB_struct *pOB, FILE *pFile, const char *str_prefix)
{
  int i;
  char *str_status = NULL;
  if ( pOB == NULL || pFile == NULL ) return 1;

  switch (pOB->status )
  {
  case OB_STATUS_NULL      : str_status = (char*) OB_STATUS_STR_NULL;     break;
  case OB_STATUS_READY     : str_status = (char*) OB_STATUS_STR_READY;    break;
  case OB_STATUS_OBSERVED  : str_status = (char*) OB_STATUS_STR_OBSERVED; break;
  case OB_STATUS_FAILED    : str_status = (char*) OB_STATUS_STR_FAILED;   break;

  default : return 1; break;
  }

  fprintf(pFile, "%s%8s %8d %4d %12.6f %8.1f\n",
          (const char*) (str_prefix ? str_prefix : ""),
          (char*)  str_status,
          (int)    pOB->UID,
          (int)    pOB->nTiles,
          (double) pOB->JD_observed,
          (float)  pOB->time_gross);

  //now write a line for each tile
  for(i=0;i<pOB->nTiles;i++)
  {
    char str_prefix2[STR_MAX];
    snprintf(str_prefix2, STR_MAX, "%sTileSummary %2d ", (str_prefix?str_prefix:""), i);
    if ( tile_struct_write ((tile_struct*) &(pOB->pTile[i]), (FILE*) pFile, (const char*) str_prefix2)) return 1;
  }
  fflush(pFile);
  return 0;
}

int OB_struct_write_file (OB_struct *pOB, const char *str_filename)
{
  FILE *pFile = NULL;
  char str_prefix[STR_MAX] = {'\0'};
  if ( pOB == NULL ) return 1;

  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s There were problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  //  snprintf(str_prefix, STR_MAX, "OBSummary %8d ", pOB->UID );
  //  snprintf(str_prefix, STR_MAX, "");

  if ( OB_struct_write ((OB_struct*) pOB, (FILE*) pFile, (const char*) str_prefix))
  {
    fprintf(stderr, "%s There were problems writing to the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  if ( pFile) fclose(pFile); pFile = NULL;
  return 0;
}

/////////// OB_struct functions /////////////

////////////////////////////////////////////////////////////////////////////////////////////////
/////////// OBlist_struct functions /////////////
int OBlist_struct_init (OBlist_struct **ppOBlist, int total_num_OBs, int tiles_per_OB, int assigns_per_tile)
{
  if ( ppOBlist == NULL ) return 1;

  if ( *ppOBlist != NULL ) return 1;
  
  //make space for the struct
  if ( (*ppOBlist = (OBlist_struct*) malloc(sizeof(OBlist_struct))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;    
  }
  //set the contents to their initial values 
  (*ppOBlist)->array_length = (int) 0;
  (*ppOBlist)->pOB = (OB_struct*) NULL;           //assume that this doesn't already point to an array
  (*ppOBlist)->nOBs = (int) 0;

  //now, if needed, then must make space for the array of OB structures
  if ( total_num_OBs > 0 )
  {
    int i;
    if ( ((*ppOBlist)->pOB = (OB_struct*) malloc(sizeof(OB_struct) * total_num_OBs)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
    (*ppOBlist)->array_length = (int) total_num_OBs ;
    (*ppOBlist)->nOBs = (int) 0;

    
    //now initialise the array of OBs
    for ( i=0;i<(*ppOBlist)->array_length;i++)
    {
      OB_struct *pOB =  (OB_struct*) &((*ppOBlist)->pOB[i]);
      if ( OB_struct_init ((OB_struct*) pOB, (int) tiles_per_OB, (int) assigns_per_tile))
      {
        fprintf(stderr, "%s Problem initialising OB_struct\n", ERROR_PREFIX);
        return 1;
      }   
    }
  }
  return 0;
}

int OBlist_struct_free (OBlist_struct **ppOBlist)
{
  if ( ppOBlist == NULL ) return 1;
  if ( *ppOBlist )
  {
    //free the array of OB structures
    if ( (*ppOBlist)->pOB )
    {
      int i;
      for ( i=0;i<(*ppOBlist)->nOBs;i++ )
      {
        OB_struct *pOB = (OB_struct*) &((*ppOBlist)->pOB[i]);
        
        if (OB_struct_free ((OB_struct**) &pOB)) return 1;
      }
    }
    //now free the OBlist struct itself
    free(*ppOBlist);
    *ppOBlist = NULL;
  }
  return 0; 
}
/////////// OBlist_struct functions /////////////
////////////////////////////////////////////////////////////////////////////////////////////////

//choose the best OB to observe - filter, calc weights, and then sort
int OBlist_struct_choose_OB   (OBlist_struct *pOBlist,
                               survey_struct *pSurvey,
                               timeline_struct *pTime )
{
  if ( pOBlist == NULL ||
       pSurvey == NULL ||
       pTime == NULL ) return 1;
  
  return 0;

}




//model the standard ESO Observer Tool OB weighting scheme as closely as possible
//follow the description in the ESO P2PPv3 documentation Bierwirth et al.
//assume that the initial filtering parameters are set to
//Start Date: now
//Duration: Next hour? Next Tile only?
//Step interval: One Tile exposure time?
//Current seeing: Set to current zenith IQ value
//Max seeing: set to some large number
//Current sky transparency: use current cloud conditions
//Current wind direction: ignored for now?

//filter only fields that satisfy the following over the time interval:
//airmass
//moon distance
//fractional moon illum
//zenith avoidance
//visible over time interval

//ranking algorithm components:
//
//seeing probability - Pseeing
// - calculate seeing at the minimum airmass the Field can reach for some nominal seeing value
// - use lookup table to calc prob of achieving the efffective seeing at the minimum airmass of the field 
//
//airmass probability - Pz
// - we need to calc this - essentially a function of dec_Field and z_requested (max airmass) 
// - h_z(z_requested,dec_Field) = (24/360)*acos[{sin(alt)-sin(dec_zen)*sin(dec_field)}/{cos(dec_zen)*cos(dec_Field)}]
// - where alt = asin(1/z_requested)
// - then Pz = hz(z_requested,dec_Field) / hz(1.7,dec_zen)   //normalised
//
//sky transparency prob - Psky
// - has no effect if all OBs require same transparency
// - then Psky = prob of requested transparency (from lookup table) 
//
//fractional lunar illum - Pfli
//- - then Pfli = prob of requested moon phase (from lookup table) 
//
//Setting target probability - Psetting
// - calculate hour angle of field at dusk: hdusk = RA_Field - STdusk
//    hdusk < -12 ? hdusk = hdusk + 24h
//    hdusk > +12 ? hdusk = hdusk - 24h
// where STdusk is local sidereal time at dusk
// then tracking time: t = hz + hdusk (if hz > hdusk)   //note that we do not actually use the tracking time
//                     t = 2*hz       (if hz <= hdusk) 
// Then,        Psetting = (hz + hdusk)/2*hz  (if hz > hdusk) 
//                       = 1.0                (if hz <= hdusk) 
//
// Time Rank - Rtime
// - ignore for now - set Rtime = 1.0
//
// Observability Class - obs_class
// obs_class = 10*round_to_integer(10*Pseeing*Pz*Psky*Pfli*Psetting*Rtime)
// ie 11 steps in observability grade between 000,010,020 ... 090,100

float ESO_OT_calc_Pseeing(environment_struct *pEnviron, float airmass_min, float seeing_requested )
{
  float seeing_zen, result;
  int i;
  if ( pEnviron == NULL || airmass_min < 1.0 ) return NAN;
  seeing_zen = seeing_requested * pow(airmass_min, -0.4);
  //now extract the probability from the seeing stats array
  for ( i=0; i< pEnviron->seeing_num_grades; i++)
    if (seeing_zen < pEnviron->seeing_thresh_high[i]) break;

  if ( i==0 )
  {
    result = pEnviron->seeing_cumulative_fraction[i];
  }
  else if ( i==pEnviron->seeing_num_grades )
  {
    result = 1.0;
  }
  else
  {
    //interpolate linearly
    result = pEnviron->seeing_cumulative_fraction[i-1] +
      (seeing_zen < pEnviron->seeing_thresh_low[i])*
      (pEnviron->seeing_cumulative_fraction[i] - pEnviron->seeing_cumulative_fraction[i-1])
      /(pEnviron->seeing_thresh_high[i] - pEnviron->seeing_thresh_low[i]) ;
  }
  return result;
}

float ESO_OT_calc_hz(double latitude, double dec_field, float airmass_max )
{
  float hz;
  if ( airmass_max < 1.0 ) return 0.0;
  hz = (float)
    DEG_TO_RA_HOURS 
    * acosd((1./airmass_max) - sind(latitude)*sind(dec_field))
    / (cosd(latitude)*cosd(dec_field));
  if ( hz > 24.0 ) hz =  (float)24.0;
  if ( hz <  0.0 ) hz =  (float) 0.0;
  return (float) hz;
}

float ESO_OT_Pz_calc(double latitude, float hz)
{
  float norm = ESO_OT_calc_hz(latitude, latitude, SORTFIELDLIST_ESO_OT_PZ_EXPONENT);
  return (float) hz / norm;
}

float ESO_OT_calc_Psky(environment_struct *pEnviron, int cloud_cover_code )
{
  float result;
  if ( pEnviron == NULL ) return NAN;
  switch (cloud_cover_code )
  {
  case CLOUD_COVER_CODE_PHOTOMETRIC : result = pEnviron->cloud_fraction_photometric; break;
  case CLOUD_COVER_CODE_CLEAR       : result = pEnviron->cloud_fraction_photometric
                                             + pEnviron->cloud_fraction_clear      ; break;
  case CLOUD_COVER_CODE_THIN        : result = pEnviron->cloud_fraction_photometric
                                             + pEnviron->cloud_fraction_clear      
                                             + pEnviron->cloud_fraction_thin       ; break;
  case CLOUD_COVER_CODE_THICK       : result = pEnviron->cloud_fraction_photometric
                                             + pEnviron->cloud_fraction_clear      
                                             + pEnviron->cloud_fraction_thin      
                                             + pEnviron->cloud_fraction_thick      ; break;
  case CLOUD_COVER_CODE_BAD         : result = 1.0; break;
  default :
    fprintf(stderr, "%s  I do not understand this cloud_cover_code: >%d<\n",
            ERROR_PREFIX,cloud_cover_code );
    return NAN;
    break;
  }
  return result;
}


//Setting target probability - Psetting
// - calculate hour angle of field at dusk: hdusk = RA_Field - STdusk
//    hdusk < -12 ? hdusk = hdusk + 24h
//    hdusk > +12 ? hdusk = hdusk - 24h
// where STdusk is local sidereal time at dusk
// then tracking time: t = hz + hdusk (if hz > hdusk)   //note that we do not actually use the tracking time
//                     t = 2*hz       (if hz <= hdusk) 
// Then,        Psetting = (hz + hdusk)/2*hz  (if hz > hdusk) 
//                       = 1.0                (if hz <= hdusk) 
//supply ra_field in deg, st_dusk in decimal hours
float ESO_OT_calc_Psetting( double ra_field, double st_dusk, float hz )
{
  double hdusk = ra_field*DEG_TO_RA_HOURS - st_dusk;
  float result;
  if ( hz < 0. || hz > 24. ) return NAN;
  if ( hdusk < -12. )  hdusk += 24.;
  if ( hdusk > +12. )  hdusk -= 24.;
  if (hz > hdusk) result = (float) (hz + hdusk)/(2.0*hz); 
  else            result = (float) 1.0; 
  return (float) result;
}

