# This is the makefile for OpSim_timeline_lib.c
include ../arch.make

COMMON_DIR = ../common

IDIR = -I./ -I$(COMMON_DIR)/ -I$(CFITSIO_PATH)/include

OUTSTEM = OpSim_timeline_lib

OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))

DEP_LIBS = $(COMMON_DIR)/utensils_lib $(COMMON_DIR)/geometry_lib $(COMMON_DIR)/fits_helper_lib 

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) $(COMMON_DIR)/define.h


#No main() so only make object library

$(OFILE)	:	$(DEP_HFILES) $(CFILE)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE)

clean   : 
	rm -f $(OFILE)


#end
