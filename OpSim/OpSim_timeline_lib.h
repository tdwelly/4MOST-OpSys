//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_timeline_lib.h
//--    Use: This is a header for the OpSim_timeline_lib.c C program
//--
//--    Notes:
//--

#ifndef OPSIM_TIMELINE_H
#define OPSIM_TIMELINE_H

#define CVS_REVISION_OPSIM_TIMELINE_LIB_H "$Revision: 1.4 $"
#define CVS_DATE_OPSIM_TIMELINE_LIB_H     "$Date: 2015/08/18 11:44:16 $"

#include <stdio.h>

#include "define.h"
#include "OpSim_telescope_site_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.4 $"
//#define CVS_DATE_H     "$Date: 2015/08/18 11:44:16 $"
inline static int OpSim_timeline_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

#define STR_MAX DEF_STRLEN

#define MAX_SKYCALC_RECORDS 1000000 //one million is far too many already


//----------------------------------------------------------------------------------------------
//-- public structure definitions
//#ifndef telescope_struct
//typedef struct telescope_struct telescope_struct;
//#endif

//
//sun details structure
typedef struct {
  double JD;  //the JD at which these details are valid
  double ra;  //deg, J2000
  double dec; //deg, J2000
  double Z;   //zenith distance, deg
  double HA;  //deg, hour angle
} sundeets_struct;

//
//moon details structure
typedef struct {
  double JD;  //the JD at which these details are valid
  double ra;  //deg, J2000
  double dec; //deg, J2000
  double Z;   // zenith distance, deg
  double HA;  //deg, hour angle
  float  phase_angle; //deg, angle between Earth and Sun as seen by moon
  float  illum_frac;  //fraction, between 0 and 1
} moondeets_struct;


//this contains one entry read from the formatted skycalc output file
typedef struct {
  double JD;
  double JD_midnight;
  double JD_twi_eve;     //JD of end of evening twilight (Julian days)
  double JD_twi_morn;    //JD of start of morning twilight (Julian days)
  double ra_zen;   //coords of zenith at this time, degrees, J2000
  double dec_zen;
  moondeets_struct moon;
  sundeets_struct sun;
  BOOL   nighttime_flag;
  float  moon_illum_midnight;  //moon illum fraction at midnight, between 0 and 1
} skycalcrecord_struct;

//this contains a list of skycalcrecord structs.
typedef struct {
  int num_records;
  int array_length;
  skycalcrecord_struct *pRecord;
  double JD_min;
  double JD_max;  
} skycalc_struct;


// This is a structure that we use to keep track of the
// passage of time
// 
typedef struct {
  double JD;             //full representation of JD
  int   iJD;             //integer part of JD    = (int) JD
  float fJD;             //fractional part of JD = (JD-iJD)

  skycalcrecord_struct Rnow;   //an interpolated record for the current JD 
  skycalcrecord_struct *pRlo;  //pointers to records that span the current JD 
  skycalcrecord_struct *pRhi;  //

  //these next two do not change after being set once
  //  struct telescope_struct *pTele; //pointer to the telescope site struct
  //  void *pTele; //pointer to the telescope site struct
  telescope_struct *pTele; //pointer to the telescope site struct
  skycalc_struct *pSkycalc; //pointer to skycalc results list structure
  
} timeline_struct;

//----------------------------------------------------------------------------------------------
//-- public function declarations

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int    OpSim_timeline_print_version           (FILE* pFile);
int    moondeets_init                         (moondeets_struct *pMoonDeets);
int    sundeets_init                          (sundeets_struct *pSunDeets);
       
int    skycalc_struct_init                    (skycalc_struct **ppSkycalc);
int    skycalc_struct_free                    (skycalc_struct **ppSkycalc);
int    skycalc_read_record_from_string        (skycalc_struct *pSkycalc, const char *str_line);
int    skycalc_read_records_from_file         (skycalc_struct *pSkycalc, const char *str_filename);
int    skycalc_read_records_from_file_ascii   (skycalc_struct *pSkycalc, const char *str_filename);
int    skycalc_read_records_from_file_fits    (skycalc_struct *pSkycalc, const char *str_filename);
int    skycalcrecord_init                     (skycalcrecord_struct *pRecord);
int    skyrecord_print_short                  (FILE* pFile, skycalcrecord_struct *pRecord, BOOL print_header);
int    skycalcrecord_interpolate              (const skycalcrecord_struct *pRlo, const skycalcrecord_struct *pRhi, skycalcrecord_struct *pRmid);
int    skycalc_interp_moon_illum              (skycalc_struct *pSkycalc);
       
//int    timeline_struct_init                   (timeline_struct **ppTime, struct telescope_struct *pTele, skycalc_struct *pSkycalc);
//int    timeline_struct_init                   (timeline_struct **ppTime, void *pTele, skycalc_struct *pSkycalc);
int    timeline_struct_init                   (timeline_struct **ppTime, telescope_struct *pTele, skycalc_struct *pSkycalc);
int    timeline_struct_free                   (timeline_struct **ppTime);
int    timeline_print_short                   (FILE* pFile, timeline_struct *pTime, BOOL print_header);
int    timeline_update_from_skycalc           (timeline_struct *pTime);
int    timeline_update                        (timeline_struct *pTime);
int    timeline_set_time_from_JD              (timeline_struct *pTime, double JD);
int    timeline_increment_time_by_secs        (timeline_struct *pTime, double delta_t);
int    timeline_increment_time_by_mins        (timeline_struct *pTime, double delta_t);
int    timeline_increment_time_by_hours       (timeline_struct *pTime, double delta_t);
int    timeline_increment_time_by_days        (timeline_struct *pTime, double delta_t);
int    timeline_increment_time_to_next_night  (timeline_struct *pTime);
//int    timeline_determine_moon_phase_code     (timeline_struct *pTime, float *pSkyBrZen, int* pcode);

float  timeline_calc_sky_brightness            (timeline_struct *pTime, double ra, double dec);
float  timeline_calc_relative_sky_brightness   (timeline_struct *pTime, double moon_dist);


#endif
