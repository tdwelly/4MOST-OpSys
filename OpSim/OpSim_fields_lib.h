//----------------------------------------------------------------------------------
//--
//--    Filename: OpSim_fields_lib.h
//--    Use: This is a header for the OpSim_fields_lib.c C code file
//--
//--    Notes:
//--

#ifndef OPSIM_FIELDS_H
#define OPSIM_FIELDS_H


#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <string.h>
#include <time.h>

#include "define.h"
#include "4FS_OpSim.h"
#include "utensils_lib.h"


#define STR_MAX DEF_STRLEN

//----------------------------------
/// these are all used by the tiling area reading code 
#define TILING_AREA_MAX_PARAMS 8

#define TILING_AREA_SHAPE_NONE  0
#define TILING_AREA_SHAPE_CIRCLE 1
#define TILING_AREA_SHAPE_BOX    2
#define TILING_AREA_SHAPE_BOUNDS 3

#define TILING_AREA_COORD_SYS_NONE  0
#define TILING_AREA_COORD_SYS_J2000 1
#define TILING_AREA_COORD_SYS_GAL   2
//----------------------------------

#define MINIMUM_USEFUL_EXPOSURE_MINS 10.0

typedef struct {
  char str_desc[STR_MAX]; //original description string
  int  shape_code;
  int  coord_sys;
  int  moon_phase;
  int  tile_increment;
  double param[TILING_AREA_MAX_PARAMS];
} tiling_area_struct;

//-----------------Info for printing code revision-----------------//
inline static int OpSim_fields_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//-----------------Info for printing code revision-----------------//


int field_test_if_object_is_in_bounds        (field_struct *pField, object_struct *pObj );
int field_test_if_point_is_in_bounds         (field_struct *pField, double ra0, double dec0 );
int field_test_if_point_is_in_bounds2        (field_struct *pField, double ra0, double dec0 );

int fieldlist_struct_init                  (fieldlist_struct     **ppFieldList);
int fieldlist_struct_free                  (fieldlist_struct     **ppFieldList);

int fieldlist_read_from_file                 (fieldlist_struct *pFieldList,
                                              survey_struct *pSurvey,
                                              focalplane_struct *pFocal,
                                              const char* strField_filename);
int fieldlist_setup_field_neighbours         (fieldlist_struct *pFieldList, const double tolerance);
int fieldlist_write_vertex_file              (fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream);
int fieldlist_write_dithered_vertex_file     (fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream);
int fieldlist_write_vertex_files             (fieldlist_struct *pFieldList);
int fieldlist_write_extended_vertex_file     (fieldlist_struct *pFieldList, const char *str_filename, FILE* pStream);
int fieldlist_write_dithered_extended_vertex_file ( fieldlist_struct *pFieldList,
                                                    const char *str_filename,
                                                    FILE* pStream );
int fieldlist_calc_survey_bounds             (fieldlist_struct *pFieldList, survey_struct *pSurvey );
int fieldlist_calc_dithers_by_linear_offsets (fieldlist_struct* pFieldList,
                                              survey_struct *pSurvey,
                                              focalplane_struct* pFocal);
int fieldlist_calc_bounds                    (fieldlist_struct *pFieldList,
                                              focalplane_struct *pFocal,
                                              telescope_struct *pTele,
                                              survey_struct *pSurvey);
int fieldlist_calc_vertex_coords             (fieldlist_struct *pFieldList, focalplane_struct *pFocal);
int fieldlist_calc_PAs                       (fieldlist_struct *pFieldList);
int fieldlist_calc_requested_tiles_per_field (fieldlist_struct *pFieldList, survey_struct *pSurvey);
int fieldlist_setup_pid_arrays               (fieldlist_struct *pFieldList);
int fieldlist_free_spare_field_pid_arrays    (fieldlist_struct *pFieldList);
//int fieldlist_associate_fields_with_quads    (fieldlist_struct *pFieldList, telescope_struct *pTele);
int fieldlist_select_candidate_fields        (fieldlist_struct *pFieldList,
                                              timeline_struct *pTime,
                                              survey_struct *pSurvey,
                                              sortfieldlist_struct *pSortFieldList,
                                              BOOL do_sort,
                                              int select_on_moon_phase,
                                              float effective_airmass_limit,
                                              BOOL wind_constraint_flag);
int fieldlist_setup_fields                   (fieldlist_struct* pFieldList,
                                              survey_struct* pSurvey,
                                              focalplane_struct* pFocal,
                                              telescope_struct *pTele);

//int    OpSim_input_params_read_tiling_description    (survey_struct *pSurvey, fieldlist_struct *pFieldList, const char *str_filename);
int   fieldlist_read_tiling_description    (survey_struct *pSurvey,
                                            fieldlist_struct *pFieldList,
                                            const char *str_filename);

int fieldlist_associate_fields_with_hpx (fieldlist_struct *pFieldList, telescope_struct *pTele);

int get_field_sub_index                    (object_struct *pObj, field_struct *pField );
int add_object_to_field                    (field_struct *pF, object_struct *pObj, survey_struct *pSurvey); //, utiny hexagon_flag );

int field_calc_object_priority_totals     ( field_struct *pField, objectlist_struct* pObjList , survey_struct *pSurvey);
int fieldlist_calc_object_priority_totals ( fieldlist_struct *pFieldList, objectlist_struct* pObjList, survey_struct *pSurvey );




///////////////////
// sortfieldlist stuff
int sortfieldlist_print_struct                  (sortfieldlist_struct *pSortFieldList, FILE *pFile );
int sortfieldlist_compare_structs_weight        (const void *pSortField1, const void *pSortField2);
int sortfieldlist_compare_structs_weight_reverse(const void *pSortField1, const void *pSortField2);
int sortfieldlist_calc_field_weights_standard   (sortfieldlist_struct *pSortFieldList, survey_struct *pSurvey, moon_struct *pMoon);
int sortfieldlist_calc_field_weights_visibility (sortfieldlist_struct *pSortFieldList, survey_struct *pSurvey, moon_struct *pMoon); 
int sortfieldlist_calc_field_weights_ESO_OT     (sortfieldlist_struct* pSortFieldList, survey_struct* pSurvey, moon_struct *pMoon, timeline_struct *pTime);
int sortfieldlist_calc_field_weights            (sortfieldlist_struct *pSortFieldList, survey_struct *pSurvey, moon_struct *pMoon, timeline_struct *pTime);
///////////////////




#endif
