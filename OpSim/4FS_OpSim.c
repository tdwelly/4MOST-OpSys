//----------------------------------------------------------------------------------
//--
//--    Filename: 4FS_OpSim.c
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//--    Use: This is the main source for the 4FS Operations Simulator code
//--
//--    Notes:
//--

//#define _GNU_SOURCE   // to enable sincos

//standard header files
#include <stdlib.h>  // for  malloc,calloc,system functions etc
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <limits.h>
#include <float.h>
#include <time.h>
//#include <sys/syscall.h>
//#include <sys/stat.h>
//#include <sys/types.h>
//#include <sys/utsname.h>
//#include <pthread.h>
#include <unistd.h>   //needed for fork(), getcwd etc
#include <signal.h>   //needed for process handling

#include <chealpix.h>   //needed for nice map making
#include <fitsio.h>    //needed for fits catalogues etc
#include <assert.h>    //for debugging

//my header files come next
#include "4FS_OpSim.h"


//now add the included libraries that depend on definitions/data-structures in 4FS_OpSim.h
#include "OpSim_input_params_lib.h"
#include "OpSim_statistics_lib.h"
#include "OpSim_FoMs_lib.h"
#include "OpSim_positioner_lib.h"
#include "OpSim_html_lib.h"
#include "OpSim_OB_lib.h"   //for ESO_OT mode survey (not yet operational)
#include "OpSim_fields_lib.h"


//#define CVS_REVISION "$Revision: 1.260 $"
//#define CVS_DATE     "$Date: 2015/11/11 10:53:19 $"

//apart from a few defintions that we do not want to escape beyond the scope of this file
#define COMMENT_PREFIX "#-4FS_OpSim.c:Comment: "
#define WARNING_PREFIX "#-4FS_OpSim.c:Warning: "
#define ERROR_PREFIX   "#-4FS_OpSim.c:Error:   "
#define DEBUG_PREFIX   "#-4FS_OpSim.c:Debug:   "
#define DATA_PREFIX    ""


//the following controls whether to emit very verbose debug information, use with care
//#define ALLOCATOR_DEBUG_OUTPUT

// Example of how to make comments appear in Doxygen ouput
// Put the following just before a function:
//
// /// A brief description.
// /** A detailed description.
// */
//
// Put the following to comment a strcuture variable declaration:
//
// int i; //!< Brief description.
//

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////MAIN STARTS HERE////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------------------//
/// Now we start the main program.
/** The main entry point to the OpSim.
*/
int main (int argc, char* argv[])
{
  /////////////////////////////////////
  // internal record of start of execution time
  clock_t clocks_main_start = clock();  
  
  ////////////////////////////////////////////////////////////
  //////////////     Main data structures    /////////////////
  ////////////////////////////////////////////////////////////

  // Keeps track of the current time line.
  timeline_struct *pTime = NULL;  

  // this keeps track of the info read in from skycalc.
  skycalc_struct *pSkycalc = NULL;
  
  // this is a wrapper structure that contains the list of all input objects.
  objectlist_struct *pObjectList = NULL;
  
  // this is the primary list of fields that we use for the first pass.
  fieldlist_struct *pFieldList = NULL;
   
  // this holds the info about the positioners in the current tile.
  focalplane_struct *pFocal = NULL;

  // this holds the info about the telescope and site.
  telescope_struct *pTele = NULL;

  // this holds details of the survey within each moon phase.
  survey_struct *pSurvey = NULL;

  // this holds the names of the input catalogues.
  cataloguelist_struct *pCatalogueList = NULL;

  ////////////////////////////////////////////////////////////
  //////////    END of Main data structures    ///////////////
  ////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////
  /////////////////Section dealing with the input parameters///////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////
  //this structure deals with the reading in of input parameters
  param_list_struct *pParamList = NULL;

  //this structure holds the input parameters themselves
  OpSim_input_params_struct *pIpar = NULL;

  /////////////////////////
  // Prevent zombie child processes by setting a behaviour for signal handling 
  struct sigaction sa;
  sa.sa_handler = SIG_IGN;
  sa.sa_flags   = SA_NOCLDWAIT;
  sigemptyset(&sa.sa_mask);
  if ( sigaction(SIGCHLD, &sa, NULL) == -1 )
  {
    perror("sigaction");
    return 1;
  }
  /////////////////////////

  /////////////////////////////////////////////////////////
  //emit the command line and program details
  if ( print_full_version_information_to_file ((const char*) VERSION_INFO_FILENAME)) return 1;
  if ( print_version_and_inputs(stdout, (int) argc, (char **) argv)) return 1;
  /////////////////////////////////////////////////////////

  //make space for and init the object list structure
  if ( objectlist_struct_init ((objectlist_struct**) &pObjectList)) return 1;

  //make space for and init the field list structure
  if ( fieldlist_struct_init ((fieldlist_struct**) &pFieldList)) return 1;

  //make space for and init the tile structure
  if ( focalplane_struct_init ((focalplane_struct**) &pFocal)) return 1;

  //make space for and init the tele structure
  if ( telescope_struct_init ((telescope_struct**) &pTele)) return 1;

  //make space for and init the survey structure
  if ( survey_struct_init ((survey_struct**) &pSurvey)) return 1;

  //make space for and init the catalogue list structure
  if ( cataloguelist_struct_init ((cataloguelist_struct**) &pCatalogueList)) return 1;

  //make space for and init the skycalc structure
  if ( skycalc_struct_init((skycalc_struct**) &pSkycalc)) return 1;

  //make space for and init the skycalc structure
  if ( timeline_struct_init((timeline_struct**) &pTime,
                            (telescope_struct*) pTele,
                            (skycalc_struct*) pSkycalc)) return 1;

  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //set up and interpret the input params
  {
    int status; 
    if ( (status = OpSim_input_params_handler ( (OpSim_input_params_struct**) &pIpar,
                                                (param_list_struct**) &pParamList,
                                                (int) argc, (char**) argv)) != 0 )
    {
      if ( status < 0 ) return 1; //this is due to a real error
      else              exit(0);  //not a real error, but we should exit
    }
  }
  if ( ParLib_print_param_list_to_file ((const char*) INTERPRETED_PARAMETER_FILENAME,
                                        (param_list_struct*)pParamList)) return 1;


  //now interpret all the input parameters
  if ( OpSim_input_params_interpret_everything ((OpSim_input_params_struct*) pIpar,
                                                (param_list_struct*) pParamList,
                                                (survey_struct*) pSurvey,
                                                (fieldlist_struct*) pFieldList,
                                                (timeline_struct*) pTime,
                                                (telescope_struct*) pTele,
                                                (focalplane_struct*) pFocal,
                                                (cataloguelist_struct*) pCatalogueList,
                                                (objectlist_struct*) pObjectList))  return 1; 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//  {
//    long i;
//    double result_d;
//    float  result_f;
//    double val_d;
//    long n = 100000000;
//    clock_t clocks_start;
//    clocks_start = clock();
//    for(i=0;i<n;i++)
//    {
//      val_d = drand48()*1.0;
//      result_d = acos(val_d);
//    }
//    fprintf(stdout, "%s Finished timing acos() (that took %.8fs)\n",
//            COMMENT_PREFIX, (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
//    fflush(stdout);
//
//    for(i=0;i<n;i++)
//    {
//      val_d = drand48()*TWOPI;
//      result_d = cos(val_d);
//    }
//    fprintf(stdout, "%s Finished timing cos() (that took %.8fs)\n",
//            COMMENT_PREFIX, (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
//    fflush(stdout);
//
//    for(i=0;i<n;i++)
//    {
//      val_d = drand48()*360.0;
//      result_d = cosd(val_d);
//    }
//    fprintf(stdout, "%s Finished timing cosd() (that took %.8fs)\n",
//            COMMENT_PREFIX, (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
//    fflush(stdout);
//  }

  
    ///print out the grids in equatorial and galactic coordinates
  if ( geom_write_coordinate_grid_files ((const char*) PLOTFILES_SUBDIRECTORY ))  return 1;
  ////////////////////////////////////////////////////
 

  //////////////////////////////////////////////////////////////////////
  //////////////////SETUP FIBERS////////////////////////////////////////
  //carry out operations based on the tile/positioner variables
  if ( OpSim_positioner_setup_fibers ((focalplane_struct*) pFocal, (telescope_struct*) pTele,
                                      (survey_struct*) pSurvey, (BOOL) pSurvey->do_plots))  return 1;
  //////////////////////////////////////////////////////////////////////

  
  //////////////////////////////////////////////////////////////////////
  //////////////////SETUP FIELDS ///////////////////////////////////////
  if ( fieldlist_setup_fields ((fieldlist_struct*) pFieldList, (survey_struct*) pSurvey,
                               (focalplane_struct*) pFocal, (telescope_struct*) pTele)) return 1;
  //////////////////////////////////////////////////////////////////////

  
  ///now need to work out the fraction of time available in each moon phase 
  if ( pSurvey->main_mode == SURVEY_MAIN_MODE_TIMELINE ||
       pSurvey->main_mode == SURVEY_MAIN_MODE_P2PP  )
  {
    if ( survey_calc_moon_fractions ((survey_struct*) pSurvey,
                                     (telescope_struct*) pTele,
                                     (timeline_struct*) pTime))
    {
      fprintf(stderr, "%s There was a problem calculating moon phase fractions\n", ERROR_PREFIX); 
      return 1;
    }
  }


  /////////////////////////////////////////////////////
  //now calculate the numbers of requested tiles per field and in total
  //first the standard baseline survey - as defined by the num_tiles_per_field keywords
  //and then modified by the tiling_description_file if necessary
  /////////////////////////////////////////////////////
  if ( fieldlist_calc_requested_tiles_per_field((fieldlist_struct*) pFieldList, (survey_struct*) pSurvey))
  {
    return 1;
  }
  /////////////////////////////////////////////////////
  /////////////////////////////////////////////////////

  //  ////-------------cut here ----------------------------------------///
  //  if ( survey_determine_sky_tiling ((survey_struct*) pSurvey,
  //                                    (fieldlist_struct*) pFieldList,
  //                                    (telescope_struct*) pTele,
  //                                    (focalplane_struct*) pFocal))
  //  {
  //    return 1;
  //  }
  //  ////-------------cut here ----------------------------------------///
    

  ///////////////////////////////////////////////////////////////////////////////////////
  //this actually reads in the objects, and does some processing
  if ( read_and_process_input_catalogues((objectlist_struct*)     pObjectList,
                                         (survey_struct*)         pSurvey,
                                         (cataloguelist_struct*)  pCatalogueList))
  {
    return 1; 
  }
  ///////////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  //now we need to assign objects to pointings
  //the hpx indexing makes this very computationally efficient
  
  if ( associate_objects_with_fields ((objectlist_struct*) pObjectList,
                                      (fieldlist_struct*)  pFieldList,
                                      (telescope_struct*)  pTele,
                                      (focalplane_struct*) pFocal,
                                      (survey_struct*)     pSurvey,
                                      (const int)          pSurvey->target_to_field_policy))
  {
    fprintf(stderr, "%s There was an error when associating objects with fields on the sky!\n", ERROR_PREFIX); 
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////
  
  ///////////////////////////////////////////////////////////////////////////////////
  //free any spare space in each field struct
  if ( fieldlist_free_spare_field_pid_arrays ((fieldlist_struct*) pFieldList))
  {
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////

  
  ///////////////////////////////////////////////////////////////////////////////////
  //calculate some statistics about numbers of objects in fields
  if ( calc_fields_per_object_stats ((objectlist_struct*) pObjectList, (survey_struct*) pSurvey))
  {
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////////////////
  //calculate some statistics about input catalogues
  //do this here because we want to know which objects are inside at lease one field
  if ( pSurvey->do_stats )
  {
    if ( calc_all_input_stats_and_maps ((survey_struct*) pSurvey,
                                        (objectlist_struct*) pObjectList,
                                        (focalplane_struct*) pFocal,
                                        (cataloguelist_struct*) pCatalogueList))
    {
      fprintf(stderr, "%s Problem when calculating input catalogue statistics and maps\n", ERROR_PREFIX);
    }
  }
  else
  {
    fprintf(stdout, "%s Calculation + writing of input statistics files is suppressed\n", WARNING_PREFIX);
    fflush(stdout);
  }
  ///////////////////////////////////////////////////////////////////////////////////

  fprintf(stdout, "%s End of preparation phase (that took %.2fs)\n", COMMENT_PREFIX,
          (double) (clock() - clocks_main_start)/((double) CLOCKS_PER_SEC)); 
  fprintf(stdout, "%s --------------------------------------------------\n", COMMENT_PREFIX);
  fflush(stdout);
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  //carry out the main survey

  switch ( pSurvey->main_mode )
  {
  case SURVEY_MAIN_MODE_TIMELINE :
  //we will carry out the survey in "real time"
    if ( timeline_survey_wrapper ((survey_struct*)        pSurvey,
                                  (objectlist_struct*)    pObjectList,
                                  (fieldlist_struct*)     pFieldList,
                                  (focalplane_struct*)    pFocal,
                                  (telescope_struct*)     pTele,
                                  (timeline_struct*)      pTime,
                                  (cataloguelist_struct*) pCatalogueList))
    {
      return 1;
    }
    break;
    //  case SURVEY_MAIN_MODE_POOLED :
    //     fprintf(stderr, "%s SURVEY_MAIN_MODE=POOLED is no longer supported (deprecated)\n", ERROR_PREFIX);
    //    return 1;
//    //we will just do things monolithically and not consider the timeline at all.
//    //call field_mbr from inside extract_gal - only way to do things now
//    //we will write out all the necessary files ready to run field mbr externallly though.
//    if ( carry_out_non_timeline_survey ((survey_struct*)     pSurvey,
//                                        (objectlist_struct*) pObjectList,
//                                        (fieldlist_struct*)  pFieldList,
//                                        (focalplane_struct*) pFocal,
//                                        (telescope_struct*)  pTele))
//    {
//      return 1;
//    }
    break;
  case SURVEY_MAIN_MODE_P2PP :
     fprintf(stderr, "%s SURVEY_MAIN_MODE=P2PP is not yet supported\n", ERROR_PREFIX);
    return 1;
    break;
  default :
    fprintf(stderr, "%s Unknown main mode (=%d)\n", ERROR_PREFIX, pSurvey->main_mode);
    return 1;
    break;
  }
  ///////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////
  fprintf(stdout, "%s --------------------------------------------------\n", COMMENT_PREFIX);
  fflush(stdout);
  
  //////////////////////////////////////////////////////////////////////////////////
  if ( pSurvey->suppress_output)
  {
    fprintf(stdout, "%s Writing of output catalogues is suppressed\n", WARNING_PREFIX);
  }
  else
  {
    clock_t object_summary_file_clocks_start = clock();

    if ( write_object_summary_files ((objectlist_struct*) pObjectList,
                                     (survey_struct*) pSurvey,
                                     (cataloguelist_struct*) pCatalogueList,
                                     (const char*) "object_summary_file"))
    {
      return 1;
    }
    ///////////////////////////////////////////////////////////////////////////////////
    fprintf(stdout, "%s Writing output catalogues took %.3fs\n",
            COMMENT_PREFIX, (double)(clock()-object_summary_file_clocks_start)/(double)CLOCKS_PER_SEC);
    fflush(stdout);
  }
  (void) fflush(stdout);
  ///////////////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////
  ///we can plot some nice diagrams of the final sky tiling
  if ( pSurvey->do_plots )
  {
    FILE *pFile = NULL;
    char str_filename[STR_MAX];
    snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, TILES_PER_FIELD_STATS_FILENAME);
    if ((pFile = fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s  There were problems opening file : %s\n",
              ERROR_PREFIX, str_filename);
      return 1;
    }
    if ( calc_tiles_per_field_stats ((FILE*) pFile, (fieldlist_struct*) pFieldList, (survey_struct*) pSurvey ))
    {
      fprintf(stderr, "%s There was a problem calculating tiles per field statistics\n", ERROR_PREFIX); 
      return 1;
    }
    if ( pFile ) fclose(pFile); pFile = NULL;


    if ( plot_sky_tiling ((survey_struct*) pSurvey,
                          (telescope_struct*) pTele,
                          (focalplane_struct*) pFocal,
                          (fieldlist_struct*) pFieldList,
                          (const char*) SKY_TILING_EXECUTED_STEM))
    {
      //not a big problem if this fails
      fprintf(stdout, "%s There was a problem making final sky tiling plots\n", WARNING_PREFIX); fflush(stdout);
    }


    //plot the survey progress file (for animation)
    if (TRUE) //( pSurvey->main_mode != SURVEY_MAIN_MODE_POOLED)
    {
      char str_filename[STR_MAX];
      snprintf(str_filename, STR_MAX, "%s/plot_progress_report.plot", PROGRESS_REPORT_SUBDIRECTORY);
      if ( write_progress_plotfile ((survey_struct*) pSurvey, (const char*) PROGRESS_REPORT_SUBDIRECTORY,
                                    (const char*) str_filename ))
      {
        fprintf(stderr, "%s Problem writing survey progress plotfile: %s\n", ERROR_PREFIX,str_filename);
        //oh well, not to worry
      }
    }
  }
  else
  {
    fprintf(stdout, "%s The generation of output plots is supressed\n", WARNING_PREFIX);
  }
  ///////////////////////////////////////////////////////////////////////

  if ( write_per_field_logfile_fits ((fieldlist_struct*) pFieldList,
                                     (const char*) PER_FIELD_LOG_FILENAME_FITS, (BOOL) pSurvey->clobber))
  {
    return 1;
  }
  if ( write_per_field_logfile_ascii ((fieldlist_struct*) pFieldList, (const char*) PER_FIELD_LOG_FILENAME))
  {
    return 1;
  }

  ///////////////////////////////////////////////////////////////////////////////////
  //calculate some more statistics from the results
  if ( pSurvey->do_stats )
  {

    if ( OpSim_positioner_write_fibers_report ((focalplane_struct*) pFocal, (const char*) FIBERS_REPORT_LOG_FILENAME))
    {
      return 1;
    }
    //write and plot the fiber useage plot
    if (write_and_plot_fiber_usage ((focalplane_struct*)pFocal, (survey_struct*) pSurvey )) return 1;


    if ( plot_sky_visibility ((survey_struct*) pSurvey,
                              (telescope_struct*) pTele,
                              (focalplane_struct*) pFocal,
                              (fieldlist_struct*) pFieldList,
                              (const char*) SKY_VISIBILITY_STEM))
    {
      //not a big problem if this fails
      fprintf(stdout, "%s There was a problem making final sky visibility plots\n", WARNING_PREFIX); fflush(stdout);
    }

    if ( plot_predicted_tile_pressure_vs_RA ((survey_struct*) pSurvey,
                                             (const char*) RA_PRESSURE_STEM))
    {
      //not a big problem if this fails
      fprintf(stdout, "%s There was a problem making plot of predicted RA pressure\n", WARNING_PREFIX); fflush(stdout);
    }
    
    if ( plot_tiles_used_and_wasted_vs_RA   ((survey_struct*) pSurvey,
                                             (const char*) TILES_USED_WASTED_STEM))
    {
      //not a big problem if this fails
      fprintf(stdout, "%s There was a problem making plot of used and wasted tiles\n", WARNING_PREFIX); fflush(stdout);
    }
    
    if ( calc_all_output_stats_and_maps ((survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjectList,
                                         (focalplane_struct*) pFocal,
                                         (cataloguelist_struct*) pCatalogueList,
                                         (fieldlist_struct*) pFieldList))
    {
      fprintf(stderr, "%s Problem when calculating output catalogue statistics and maps\n", ERROR_PREFIX);
    }

    ////////////////////////////////////////////////////////////////
    //calculate the extra Clusters stats and bonus output catalogue 
    if ( pSurvey->pClusterList )
    {
      int c;
      catalogue_struct *pCat = NULL;
      for(c=0;c<pCatalogueList->num_cats;c++)
      {
        if ( pCatalogueList->pCat[c].code == CATALOGUE_CODE_S5 )
        {
          pCat = (catalogue_struct*) &(pCatalogueList->pCat[c]);
        }
      }
      if ( pCat != NULL)
      {
        if ( clusterlist_struct_calc_summary ((clusterlist_struct*) pSurvey->pClusterList,
                                              (objectlist_struct*) pObjectList,
                                              (catalogue_struct*) pCat))
        {
          fprintf(stderr, "%s Problem when calculating Cluster catalogue stats\n", ERROR_PREFIX);
        }
        if ( clusterlist_struct_write_summary ((clusterlist_struct*) pSurvey->pClusterList,
                                               (objectlist_struct*) pObjectList,
                                               (const char*) CLUSTERS_SUMMARY_FILENAME))
        {
          fprintf(stderr, "%s Problem when writing Cluster catalogue stats\n", ERROR_PREFIX);
        }
      }
    }
    ////////////////////////////////////////////////////////////////

  }
  else
  {
    fprintf(stdout, "%s I have been asked to suppress calculation + writing of output statistics files\n", WARNING_PREFIX);
  }
  ///////////////////////////////////////////////////////////////////////////////////

  if ( OpSim_html_write_summary_page ((survey_struct*) pSurvey, (focalplane_struct*) pFocal,
                                      (param_list_struct*) pParamList, (cataloguelist_struct*) pCatalogueList,
                                      (const char*) OPSIM_OUTPUT_HTML_SUMMARY_PAGE) )
  {
    fprintf(stderr, "%s Problem writing html summary page\n", ERROR_PREFIX);
    return 1;
  }


  //do some tidying up
  //  if ( pParamList ) free(pParamList) ; pParamList = NULL;

  if (ParLib_free_param_list        ((param_list_struct**)         &pParamList    ) ||
      fieldlist_struct_free         ((fieldlist_struct**)          &pFieldList    ) || 
      objectlist_struct_free        ((objectlist_struct**)         &pObjectList   ) ||
      focalplane_struct_free        ((focalplane_struct**)         &pFocal        ) ||
      timeline_struct_free          ((timeline_struct**)           &pTime         ) ||
      skycalc_struct_free           ((skycalc_struct**)            &pSkycalc      ) ||
      cataloguelist_struct_free     ((cataloguelist_struct**)      &pCatalogueList) ||
      OpSim_input_params_struct_free((OpSim_input_params_struct**) &pIpar         ) ||
      survey_struct_free            ((survey_struct**)             &pSurvey       ))
  {
    fprintf(stderr, "%s Error freeing memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
  }
  
  fflush(stdout);
  fprintf(stdout, "%s End of processing, that took %.2f seconds\n", COMMENT_PREFIX,
          (double) (clock() - clocks_main_start)/((double) CLOCKS_PER_SEC)); 
  fprintf(stdout, "%s Please wait a few seconds for background processes to finish\n", COMMENT_PREFIX);
  fflush(stdout);
  exit(0);
}//end main end_main
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// END OF MAIN /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////






/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// LOCAL FUNCTIONS //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
int FourFS_OpSim_print_version (FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION, CVS_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__); 
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}

int print_version_and_inputs( FILE* pstream, int argc, char **argv)
{
  int i;
  //print out the command line and program details
  //some of these values are set by CVS
  if ( pstream == NULL ) return 1;
  //  if ( _4FS_OpSim_print_version ((FILE*) pstream)) return 1;

  fprintf(stdout, "%s The command line (%d args) was: ", COMMENT_PREFIX, argc );
  for ( i=0;i<argc;i++) 
  {
    fprintf(stdout, "%s ", argv[i]);
  }
  fprintf(stdout, "\n"); fflush(stdout);
  return 0;
}


//////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
//associate all the objects in pObjList with fields in pFieldList
//this version deals with the cases where object lies in more than one field
///expect pointers to arrays of objects and fields
int associate_objects_with_fields(objectlist_struct *pObjList,
                                  fieldlist_struct  *pFieldList,
                                  telescope_struct  *pTele,
                                  focalplane_struct *pFocal,
                                  survey_struct     *pSurvey,
                                  const int target_to_field_policy )
{
  double *pcos_radius_list = NULL;
  uint    *pField_ids = NULL;
  int i;
  clock_t clocks_start = clock();
  hpx_pixlist_struct *pPixlist;

  //  ulong sum_nf = 0; //count up how many fields are searched per object
  //#define NUM_NF_HISTO_BINS 30 
  //  int nf_histo[NUM_NF_HISTO_BINS+1];
  //  for(i=0;i<=NUM_NF_HISTO_BINS;i++) nf_histo[i] = 0;
  if (pObjList   == NULL ||
      pFieldList == NULL ||
      pTele      == NULL ||
      pSurvey    == NULL ||
      pFocal     == NULL )
  {
    return 1;
  }
  
  pPixlist = (hpx_pixlist_struct*) &(pFieldList->Pixlist);
  if (pPixlist == NULL)  return 1;

  fprintf(stdout, "%s I will now associate objects with fields on the sky (may take some time) ... ",
          COMMENT_PREFIX);
  fflush(stdout);


  if ( (pcos_radius_list = (double*) calloc((size_t) pFieldList->nFields, sizeof(double))) == NULL ||
       (pField_ids       = (uint*)   calloc((size_t) pFieldList->nFields, sizeof(uint)))   == NULL)
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  for (i = 0; i < pObjList->nObjects; i++)
  {
    //shorten the name of the object pointer
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    //    quad_struct *pq = NULL;
    hpx_pix_struct *pPix;
    long j;
    int num_fields;
    long hpx_pix_temp;

    //check if object is in the survey footprint
    if ( !(pObj->in_survey & SURVEY_ANY) ) continue;
    
    //initialise the object's parameters
    pObj->num_fields = 0;
    //    //find the Quadtree that this object lies within
    //    pq = (quad_struct*) quad_get_quad_from_position((quad_struct*) &(pFieldList->Quad), pObj->ra, pObj->dec);

    //get the hpx pixel containing the field centre
    (void) ang2pix_nest(pPixlist->nside,
                        M_PI_2 - pObj->dec*DEG_TO_RAD,
                        pObj->ra*DEG_TO_RAD,
                        (long*) &hpx_pix_temp);
    
    if ( hpx_pix_temp < 0 || hpx_pix_temp >= pPixlist->npix )
    {
      //this must mean that object is outside survey area
      //do nothing
      continue;
    }
    pObj->hpx_pix = (uint) hpx_pix_temp;

    pPix = (hpx_pix_struct*) &(pPixlist->pPix[pObj->hpx_pix]);
    if ( pPix->nMembers <= 0)
    {
      // If we got here then this means that the hpx_pix struct has no fields associated with it.
      // this probably means that object is outside survey area
      // Therefore, do nothing
      continue;
    }
    
    // Now see if the oject lies in any of the potential fields associated with this pixel
    //build a list of the fields which pass some basic bounds checking
    num_fields = 0;
    for (j = 0; j <  pPix->nMembers; j++)
    {
      field_struct *pF = (field_struct*) pPix->pMembers[j];
      if (((uint) pF->in_survey) & ((uint) SURVEY_ANY))
      {
        if ( ((uint) pF->in_survey) & ((uint) pObj->in_survey) )
        {
          if ( field_test_if_object_is_in_bounds((field_struct*) pF, (object_struct*) pObj))
          {
            pField_ids[num_fields++] = (uint) pF->id;
            if ( num_fields >= pFieldList->nFields )  //this should never happen
            {
              fprintf(stderr, "%s Bad number of potential fields for object\n", ERROR_PREFIX );
              return 1;
            }
          }
        }
      }
    }

    
//    sum_nf += num_fields;
//    if ( num_fields >= 0 && num_fields < NUM_NF_HISTO_BINS ) nf_histo[num_fields] ++;
//    else                                                     nf_histo[NUM_NF_HISTO_BINS] ++;

    if ( num_fields > 0 )
    {
      field_struct *pF_last = NULL;
      int k;
      int num_object_fields_temp = 0;
      object_field_struct pObjFieldTemp[MAX_FIELDS_PER_OBJECT_TEMP]; //used as a temporary storage location

      // sorting only needed for certain TARGET_TO_FIELD policies
      if ( target_to_field_policy == TARGET_TO_FIELD_POLICY_CLOSEST )
      {
        
        for (j = 0; j < num_fields ; j++)
        {
          field_struct *pF = (field_struct*) &(pFieldList->pF[pField_ids[j]]);
          double sin_obj_dec = sind(pObj->dec);
          double cos_obj_dec = cosd(pObj->dec);
          //          (void) sincos((double) DEG_TO_RAD*pObj->dec, (double*) &sin_obj_dec, (double*) &cos_obj_dec);
          pcos_radius_list[j] = geom_very_fast_cos_arc_dist(pF->ra0, pObj->ra,
                                                            pF->sin_dec0, pF->cos_dec0,
                                                            sin_obj_dec, cos_obj_dec);
        }
        //sort this list of fields in order of cos_radius
        util_dusort((unsigned long) num_fields, (double*) pcos_radius_list, (uint*) pField_ids);
      }


      //and now want to descend through that list - ie in increasing radial distance
      //if policy is closest, then will stop as soon as find a pointing that actually contains the source.
      //if policy is all, then will allow up to MAX_FIELDS_PER_OBJECT fields
      for (k=num_fields-1;k>=0;k--)
      {
        int dither_index;
        utiny hex_flag0 = 0, perimeter_flag0 = 0;
        utiny hex_flag_any = 0, perimeter_flag_any = 0;
        double x;
        double y;
        field_struct *pF = (field_struct*) &(pFieldList->pF[pField_ids[k]]);
        //        if (!(pF->in_survey & SURVEY_ANY)) continue;
          
        if ( (field_struct*) pF == (field_struct*) pF_last ) continue;  //this is just a repeat of the last field
        pF_last = (field_struct*) pF;
          
        //project the ra,dec onto the focal plane
        //want to know if object lies inside ANY dither position, and if object
        //lies inside main hexagon at zeroth dither position

        for (dither_index=0; dither_index<pF->num_dithers; dither_index++)
        {
          utiny hex_flag = 0;
          utiny perimeter_flag = 0;
          if (geom_project_gnomonic_to_mm (pF->pra0[dither_index], pF->pdec0[dither_index], pF->pa0,
                                           pObj->ra, pObj->dec, pFocal->plate_scale,
                                           (double*) &x, (double*) &y ))
          {
            return 1;
          }
          //            hex_flag = (int) geom_test_if_in_hexagon((double) pTele->fov_radius_mm, x, y);
          hex_flag = (int) OpSim_positioner_test_if_inside_tile_inner_perimeter ((focalplane_struct*) pFocal,
                                                                                 (double) x, (double) y );
          if ( dither_index == 0 )
          {
            hex_flag0 = hex_flag;
            perimeter_flag0 = perimeter_flag;
          }
          if ( hex_flag )
          {
            hex_flag_any++;
          }
          else
          {
            //              perimeter_flag = geom_test_if_near_hexagon_perimeter(pTele->fov_radius_mm, x, y, pFocal->patrol_radius_max_mm);
            perimeter_flag = OpSim_positioner_test_if_near_tile_inner_perimeter ((focalplane_struct*) pFocal,
                                                                                 (double) x, (double) y );
            if ( perimeter_flag ) perimeter_flag_any++;
          }
            
          //if we have it in at least one hexagon or perimeter
          //so this is a surefire success! 
          if ( hex_flag || perimeter_flag ) break;
        }
          
        if ( hex_flag_any > 0 || perimeter_flag_any > 0 ) //success!
        {
          
          //add this field to the pObjFieldTemp struct
          if ( num_object_fields_temp >= MAX_FIELDS_PER_OBJECT_TEMP )
          {
            fprintf(stderr, "%s Too many fields (>=%d) for this object: %u\n",
                    ERROR_PREFIX, (int) num_object_fields_temp, (uint) pObj->id);
            return 1;
          }
          
          pObjFieldTemp[num_object_fields_temp].field_id   = (uint) pF->id;
          pObjFieldTemp[num_object_fields_temp].perim_flag = (utiny) (hex_flag0 > 0 ? FIELD_FLAG_INSIDE :
                                                               (perimeter_flag0 > 0 ? FIELD_FLAG_BORDER : FIELD_FLAG_OUTSIDE));
          num_object_fields_temp ++;
          ///////////////////////////////////////////////////////////////////////////////
          //Now add the object to the field struct
          if ( add_object_to_field((field_struct*) pF,
                                   (object_struct*) pObj,
                                   (survey_struct*) pSurvey))//,
            //                                   (utiny) (hex_flag0 > 0 ? FIELD_FLAG_INSIDE :
            //                                            ( perimeter_flag0 > 0 ? FIELD_FLAG_BORDER : FIELD_FLAG_OUTSIDE))))
          {
            return 1;
          }

          
          
          
          if (target_to_field_policy == TARGET_TO_FIELD_POLICY_CLOSEST ||
              target_to_field_policy == TARGET_TO_FIELD_POLICY_FIRST   )
          {
            break; //exit the for loop over all possible fields because we have found a suitable field
          }
          else if (target_to_field_policy == TARGET_TO_FIELD_POLICY_ALL )
          {
            //            if (pObj->num_fields >= MAX_FIELDS_PER_OBJECT )
            if (num_object_fields_temp >= MAX_FIELDS_PER_OBJECT )
            {
              break; //we have enough fields now, so continue quietly to next object
            }
          }
          else
          {
            //we do not know any other policies
            return 1;
          }
        }
      }//end of loop over candidate fields

      //now allocate space and copy  pObjFieldTemp struct info to the object struct
      pObj->num_fields = num_object_fields_temp;
      if ( pObj->num_fields > 0 )
      {
        if ((pObj->pObjField = (object_field_struct*) calloc((size_t) pObj->num_fields,
                                                             (size_t) sizeof(object_field_struct))) == NULL )
        {
          fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
          return 1;
        }
        for(k=0;k< pObj->num_fields ; k++)
        {
          pObj->pObjField[k].field_id   = pObjFieldTemp[k].field_id;
          pObj->pObjField[k].perim_flag = pObjFieldTemp[k].perim_flag;
        }
        if (pObj->num_fields > pObjList->max_num_fields_per_object )
          pObjList->max_num_fields_per_object = pObj->num_fields;    //record the max number of fields per object
      }

    }
  }//end of for loop over all objects


  //free some memory
  if ( pcos_radius_list ) free (pcos_radius_list); pcos_radius_list = NULL;
  if ( pField_ids )       free (pField_ids);       pField_ids = NULL;
  fprintf(stdout, " done (that took %.3fs)\n",
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
  fflush(stdout);

//  //print some debug info
//  fprintf(stdout, "%s NF_MEAN %ld/%d = %.2f\n", DEBUG_PREFIX,
//          sum_nf,  pObjList->nObjects, sum_nf/((float)MAX(1,pObjList->nObjects)));
//  for(i=0;i<=NUM_NF_HISTO_BINS;i++)
//    fprintf(stdout, "%s NF_HISTO %3d %8d %8.5f\n", DEBUG_PREFIX,
//            i, nf_histo[i],  nf_histo[i]/((float)MAX(1,pObjList->nObjects)));

  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////




/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////


//if return value = 0 : this means successfullly added object to fiber
//if return value > 0 : this means that this object was not added to the fiber for a good reason
//if return value < 0 : this means that there was an error 
//int add_object_to_fiber ( focalplane_struct *pFocal, fiber_struct *pFib, object_struct *pObj, int field_sub_index )
int add_object_to_fiber ( focalplane_struct *pFocal, fiber_struct *pFib, object_struct *pObj, double x, double y)
{
  float sq_dist;
  if ( pFocal == NULL ||
       pFib == NULL ||
       pObj == NULL ) return -1;

  if ( pFib->is_empty ) return 1;
  if ( (utiny) pFib->res == (utiny) pObj->res ||
       (utiny) pFib->res == (utiny) RESOLUTION_CODE_DUAL )
  {
    //    sq_dist = SQR(pObj->px[field_sub_index] - pFib->Geom.x0) + SQR(pObj->py[field_sub_index] - pFib->Geom.y0);
    sq_dist = SQR(x - pFib->Geom.x0) + SQR(y - pFib->Geom.y0);
    if ( sq_dist <= pFocal->sq_patrol_radius_max_mm )
    {
      if ( sq_dist >= pFocal->sq_patrol_radius_min_mm )
      {
        //We can make improvements here for PotzPoz
        //Can add some further cuts for the dead zones beyond neighbouring positioners,
        //and dead zones inside the exclusion zones around neighbour fiber bases.
        //We can work out the excluded angle ranges for each fiber once at the beginning of the
        //survey, then calc the angle of the target wrt x0,y0 here, and test against the precalculated ranges
        
        //all good so far, so this is a possible positioner for this object
        if(pFib->nobjects >= MAX_OBJECTS_PER_FIBER)
        {
            //but this is a problem
          fprintf(stderr, "%s A Bad thing happened: pFib->nobjects=%d\n",
                  ERROR_PREFIX, pFib->nobjects); 
          return -1;
        }
        pFib->object_list[pFib->nobjects]    = (uint) pObj->id;
        pFib->object_sq_dist[pFib->nobjects] = (float) sq_dist;
        pFib->nobjects ++;
        pFib->nobjects_weighted += (1.0/(float) MAX(pObj->num_fields, 1));
        return 0;
      }
    }
  }
  return 1;
}

//if return value = 0 : this means successfullly added fiber to objectfiber struct
//if return value !=0 : this means that there was an error 
int add_fiber_to_objectfiber_struct ( objectfiber_struct *pObjFib, fiber_struct *pFib, int objfib_index )
{
  if ( pFib    == NULL ||
       pObjFib == NULL ||
       objfib_index < 0 ) return -1;

  if ( pObjFib->pnumfibs[objfib_index] >= MAX_FIBERS_PER_OBJECT )
  {
    fprintf(stderr, "%s Too many objects (n=%d) added to fiber id=%d\n",
            ERROR_PREFIX, pObjFib->pnumfibs[objfib_index], pFib->id);
    return 1;
  }

  pObjFib->pfib_ids[objfib_index*MAX_FIBERS_PER_OBJECT + pObjFib->pnumfibs[objfib_index]] = (int) pFib->index;
  pObjFib->psqdist [objfib_index*MAX_FIBERS_PER_OBJECT + pObjFib->pnumfibs[objfib_index]] = (float) pFib->object_sq_dist[pFib->nobjects-1];
  pObjFib->pnumfibs[objfib_index]++;

  return 0;
}



//assumes that quad struct has already been set up for the tile
int associate_object_with_fibers ( object_struct *pObj,
                                   field_struct *pField,
                                   focalplane_struct *pFocal,
                                   objectfiber_struct* pObjFib,
                                   int objfib_index,
                                   int dither_index)
{
  int j,k;
  quad_struct  *pq = NULL; //this will point to the struct for the central quad
  double x, y;
  if ( pObj     == NULL ||
       pField   == NULL ||
       pFocal    == NULL ||
       pObjFib  == NULL ) return 1;

  

  //get the x,y coords of this object in this field
  //project the ra,dec onto the focal plane
  if (geom_project_gnomonic_to_mm ((double) pField->pra0[dither_index], (double) pField->pdec0[dither_index],
                                   (double) pField->pa0,
                                   (double) pObj->ra, (double) pObj->dec, pFocal->plate_scale,
                                   (double*) &x, (double*) &y ))
  {
    return 1;
  }
          
  
  ////////////////////////////////////////////////
  //             Layout of quad neighbours
  //      -----------------------------------
  //      |          |           |          |
  //      |    NW    |     NN    |    NE    |
  //      |          |           |          |
  //      |          |           |          |
  //      -----------------------------------
  //      |          |      +obj |          |
  //      |    WW    |           |    EE    |
  //      |          |    pq     |          |
  //      |          |           |          |
  //      -----------------------------------
  //      |          |           |          |
  //      |    SW    |    SS     |    SE    |
  //      |          |           |          |
  //      |          |           |          |
  //      -----------------------------------
  
  //this is the new and faster quadtree method
  //get the quad at the location of the object
  if ((pq = (quad_struct*) quad_get_quad_from_position  ((quad_struct*) &(pFocal->Quad),
                                                         (double) x, (double) y)) == NULL)
      //                                                         (double) pObj->px[field_sub_index],
      //                                                         (double) pObj->py[field_sub_index])) == NULL)
  {
    // this object does not fall into a quad struct - very odd
    //this is not expected, so emit warning and continue onto next object
    fprintf(stderr, "%s A Bad thing happened: no quad struct found for Object id %u in field %5s at position %g,%g\n",
            WARNING_PREFIX, pObj->id, pField->name, x, y); 
    return 0;
  }
  
  /////////////////////////////////////////////////////////////////////////////////
  //consider each fiber that lies in the central quad enclosing the object and any of the neighbours surrounding it
  for(k=-1;k<QUADTREE_NNEIGHBOURS;k++)
  {
    quad_struct *pThisQuad = NULL;
    if ( k < 0 ) pThisQuad = (quad_struct*) pq;
    else         pThisQuad = (quad_struct*) pq->pNeighbours[k];
    
    if ( pThisQuad != NULL )
    {
      for(j=0;j<pThisQuad->nMembers;j++)
      {
        int flag;
        fiber_struct *pFib = (fiber_struct*) ((void*) (pThisQuad->pMembers[j]));

        if ( pFib != NULL )
        {
          if ( pFib->is_empty == FALSE)
          {
            if ((flag = add_object_to_fiber ( (focalplane_struct*) pFocal,
                                              (fiber_struct*) pFib,
                                              (object_struct*) pObj,
                                              (double) x, (double) y )) < 0 )
            {
              fprintf(stderr, "%s Problem when adding object id=%u to fiber id=%d\n", ERROR_PREFIX, pObj->id, pFib->id);
              return 1;
            }
            ////////////////////
            if ( flag == 0 ) //if the object was actually added to fiber, then we need to add the fiber to the ObjFib struct
            {
              //add the fiber to the objectfiber_struct - use the same index for this Object as used in this Field's objectlist
              if ( add_fiber_to_objectfiber_struct ( (objectfiber_struct*) pObjFib, (fiber_struct*) pFib, (int) objfib_index))
              {
                return 1;
              }
            }
          }
          ////////////////////
        }
        else
        {
          fprintf(stderr, "%s Null fiber_struct pFib = 0x%08lx\n", ERROR_PREFIX, (unsigned long) pFib); //debug
        }
      }
    }
  }
  /////////////////////////////////////////////////////////////////////////////////
  return 0;
}



//go through all the objects in a field struct, and associate them with fibers within a tile
//effort is required to make this as efficient as possible
//because it will be run once for every Field in the survey.
//this is not actually doing the fiber allocation, just working out which objects can be seen by which fibers
int associate_objects_with_fibers ( objectlist_struct  *pObjList,
                                    field_struct       *pField,
                                    focalplane_struct  *pFocal,
                                    objectfiber_struct *pObjFib,
                                    int                 dither_index)
{
  int i,j;

  int n_flagged[NRESOLUTION_CODES];//debug
  int n_no_fibs[NRESOLUTION_CODES];//debug
  //  float sq_dist, max_sq_dist, min_sq_dist;
  if ( pObjList == NULL ||
       pField   == NULL ||
       pFocal    == NULL ||
       pObjFib  == NULL ) return 1;

  for(i=0;i<NRESOLUTION_CODES;i++)
  {
    n_flagged[i] = 0;//debug
    n_no_fibs[i] = 0;//debug
  }
  
  //init the tile struct by initialising all the fiber structs
  for(j=0;j<pFocal->num_fibers;j++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[j]);
    pFib->nobjects = 0;
    pFib->nobjects_weighted = 0.0;
    pFib->assign_flag = (utiny) FIBER_ASSIGN_CODE_NONE;
    pFib->pObj = NULL;
    pFib->fiber_ranking = -1;
  }

  //i is an index that identifies the object within the pid list of the field
  //it is also the index of this object within the objectfiber_struct
  for(i=0;i<pField->n_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[pField->pid[i]]);

    if ( pObj->to_observe ) //if this object is to be considered here
    {
      n_flagged[pObj->res] ++ ; //debug
      if ( associate_object_with_fibers ( (object_struct*) pObj,
                                          (field_struct*) pField,
                                          (focalplane_struct*) pFocal,
                                          (objectfiber_struct*) pObjFib,
                                          (int) i,
                                          (int) dither_index))
      {
        return 1;
      }
    }
    if ( pObjFib->pnumfibs[i] <= 0 ) n_no_fibs[pObj->res] ++; //debug


    {
      int   field_sub_index;  //the index of this field within the object's array of fields 
      if ( (field_sub_index = get_field_sub_index ((object_struct*) pObj, (field_struct*) pField )) < 0 )
      {
        fprintf(stderr, "%s Error finding field_sub_index at file %s line %d\n",
                ERROR_PREFIX, __FILE__, __LINE__);
        return -1;
      }
      //record the number of fibers available for this object in this field
      //remember the maximum number of fibers that can see the target in any of the used dither positions
      //      pObj->pnfibs[field_sub_index] = (uint) MAX(pObjFib->pnumfibs[i],pObj->pnfibs[field_sub_index]);
      pObj->pObjField[field_sub_index].nfibs = (uint) MAX(pObjFib->pnumfibs[i],pObj->pObjField[field_sub_index].nfibs);

    }


  }//for each object
  //  fprintf(stdout, "%s associate_objects_with_fibers(): Field %s N_object= %8d N_flagged[hi,lo]= %8d %8d N_no_fibs[hi,lo]= %8d %8d\n", COMMENT_PREFIX, pField->name,  pField->n_object, n_flagged[RESOLUTION_CODE_HIGH], n_flagged[RESOLUTION_CODE_LOW], n_no_fibs[RESOLUTION_CODE_HIGH], n_no_fibs[RESOLUTION_CODE_LOW]); 
  return 0;
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////
int read_and_process_input_catalogues( objectlist_struct *pObjList,
                                       survey_struct *pSurvey,
                                       cataloguelist_struct* pCatList)
{
  int c;
  clock_t clocks_start = clock();
  long total_est_lines = 0;
  const long num_sample_lines = 500;
  const float safety_factor = 1.1;
  fprintf(stdout, "%s I will now read in and process %d input catalogue%s ... \n",
          COMMENT_PREFIX, pCatList->num_cats, (pCatList->num_cats==1?"":"s"));
  fflush(stdout);



  //first need to determine the format of each catalogue
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);
    int result;
    result = fhelp_test_if_file_is_fits((const char*) pCat->filename);
    if ( result == (int) FHELP_FILE_FORMAT_CODE_FITS )
    {
      pCat->format_code = CATALOGUE_FORMAT_FITS;
    }
    else if ( result == (int) FHELP_FILE_FORMAT_CODE_NOT_FITS )
    {
      pCat->format_code = CATALOGUE_FORMAT_ASCII;
    }
    else
    {
      fprintf(stderr, "%s There were problems opening the catalogue file: %s (result=%d)\n",
              ERROR_PREFIX, pCat->filename, result);
      return 1;
    }
    fprintf(stdout, "%s   Catalogue %d/%d: \"%s\" is in format: %s \n",
            COMMENT_PREFIX, c+1, pCatList->num_cats,
            pCat->filename, (pCat->format_code==CATALOGUE_FORMAT_FITS?"FITS":"ASCII"));
  }

  pObjList->array_length = 0;
  //estimate the total number of lines in the catalogue files 
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);
    long est_lines = 0;
    if ( pCat->format_code == CATALOGUE_FORMAT_FITS )
    {
      fitsfile *fptr;     // FITS file pointer
      int status = 0;
      if (fits_open_table((fitsfile**) &fptr, pCat->filename, READONLY, &status))
      {
        fprintf(stderr, "%s There were problems determining the number of targets in the catalogue file: %s\n",
                ERROR_PREFIX, pCat->filename);
        return 1;
      }
      fits_get_num_rows(fptr, &est_lines, &status);
      fits_close_file(fptr, &status);

      //curtail the catalogue if requested
      if ( pObjList->max_objects_per_catalogue > 0 &&
           pObjList->max_objects_per_catalogue < LONG_MAX )
      {
        est_lines = (long) MIN(pObjList->max_objects_per_catalogue,est_lines);
      }

      
      pObjList->array_length += (long) est_lines;
    }
    else
    {
      //debug - for now
      if ( (est_lines = util_est_num_lines_in_named_file ((const char*) pCat->filename, (const long) num_sample_lines)) < 0 )
      {
        fprintf(stderr, "%s There were problems estimating the number of targets in the catalogue file: %s\n",
                ERROR_PREFIX, pCat->filename);
        return 1;
      }
      pObjList->array_length += (long) (est_lines*safety_factor);
    }
    total_est_lines += est_lines;
  }

  if ( pObjList->array_length > total_est_lines ) {
    fprintf(stdout, "%s   I estimate that there are %ld total targets - so will allocate space for %ld targets\n",
            COMMENT_PREFIX, total_est_lines, (long) pObjList->array_length);
    fflush(stdout);
  }
  else
  {
    fprintf(stdout, "%s   I determined that there are %ld total targets - so will allocate space for %ld targets\n",
            COMMENT_PREFIX, total_est_lines, (long) pObjList->array_length);
    fflush(stdout);
  }

  if ( pObjList->array_length > 0 )
  {
    //    if ((pObjList->pObject = (object_struct*) malloc(sizeof(object_struct)*pObjList->array_length)) == NULL )
    if ((pObjList->pObject = (object_struct*) calloc((size_t) pObjList->array_length,
                                                     (size_t) sizeof(object_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  else
  {
    fprintf(stderr, "%s   Estimated number of input targets in the catalogue files = %d\n",
              ERROR_PREFIX, pObjList->array_length);
    return 1;
  }
  /////////////////////////////////////////////////////////////////  

  
  /////////////////////////////////////////////////////////////////  
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);
    if ( strlen(pCat->filename)    > pCatList->longest_cat_filename )
      pCatList->longest_cat_filename = strlen(pCat->filename);
    if ( strlen(pCat->codename)    > pCatList->longest_cat_codename )
      pCatList->longest_cat_codename = strlen(pCat->codename);
    if ( strlen(pCat->displayname) > pCatList->longest_cat_displayname )
      pCatList->longest_cat_displayname = strlen(pCat->displayname);
  }
  /////////////////////////////////////////////////////////////////  

  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);

    if ( pCat->format_code == CATALOGUE_FORMAT_FITS )
    {
      if ( read_input_catalogue_fits((objectlist_struct*) pObjList,
                                     (survey_struct*)     pSurvey,
                                     (catalogue_struct*)  pCat,
                                     (cataloguelist_struct*) pCatList))
        return 1;
    }
    else
    {
      fprintf(stderr, "%s Catalogues in ASCII format are no longer supported\n",
              ERROR_PREFIX);
      //      if ( read_input_catalogue_ascii((objectlist_struct*) pObjList,
      //                                      (survey_struct*)     pSurvey,
      //                                      (catalogue_struct*)  pCat))
        return 1;
    }

  }
  fprintf(stdout, "%s ... done (that took %.3fs)\n", COMMENT_PREFIX,
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
  fflush(stdout);

  /////////////////////////////////////////////////////////////
  //tidy up the memory allocation if we have a surplus of space
  if ( pObjList->nObjects <= 0 )
  {
    //strange but not fatal?
  }
  else if ( pObjList->array_length > pObjList->nObjects )
  {
    if ((pObjList->pObject = (object_struct*) realloc(pObjList->pObject, sizeof(object_struct)*pObjList->nObjects)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
    pObjList->array_length = pObjList->nObjects;
  }
  /////////////////////////////////////////////////////////////////  


  
  fprintf(stdout, "%s Current object_struct array data size is %8.1f MBytes (%d bytes per object_struct)\n",
          COMMENT_PREFIX,
          (float) sizeof(object_struct)*pObjList->nObjects/((float)(1024.0*1024.0)),
          (int) sizeof(object_struct)); fflush(stdout); 
  
    //apply some basic processing to the input catalogues
  if ( objectlist_process_objects ((objectlist_struct*) pObjList, (survey_struct*) pSurvey))
  {
    return 1;
  }


  //now do some stats
  for(c=0;c<pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);
    long i;
    int m;
    pCat->num_objects_hires       = 0;    //number of objects of the given type
    pCat->num_objects_lores       = 0;
    pCat->num_objects_exgal       = 0;
    pCat->num_objects_exgal_lores = 0;
    pCat->num_objects_exgal_hires = 0;
    pCat->num_objects_gal         = 0;
    pCat->num_objects_gal_lores   = 0;
    pCat->num_objects_gal_hires   = 0;
    pCat->num_objects_possible    = 0;
    pCat->num_objects_impossible  = 0;

    for(m=0;m<MAX_NUM_MOON_PHASES;m++)
    {
      pCat->req_fhrs[m]       = (double) 0.0;
      pCat->req_fhrs_lores[m] = (double) 0.0;
      pCat->req_fhrs_hires[m] = (double) 0.0;
    }
      
    for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object;i++)
    {
      object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
      if ( isnan(pObj->redshift))
      {
        pCat->num_objects_gal ++;
        if ( pObj->res == RESOLUTION_CODE_LOW  ) { pCat->num_objects_lores ++;  pCat->num_objects_gal_lores ++;} 
        else                                     { pCat->num_objects_hires ++;  pCat->num_objects_gal_hires ++;} 
      }
      else
      {
        pCat->num_objects_exgal ++;
        if ( pObj->res == RESOLUTION_CODE_LOW  ) { pCat->num_objects_lores ++;  pCat->num_objects_exgal_lores ++;} 
        else                                     { pCat->num_objects_hires ++;  pCat->num_objects_exgal_hires ++;} 
          
      }
        
      if (pObj->treq[MOON_PHASE_DARK] < (float) MAX_EXPOSURE_TIME )
      {
        pCat->num_objects_possible ++;
        for(m=0;m<MAX_NUM_MOON_PHASES;m++)
        {
          if (pObj->treq[m] < (float) MAX_EXPOSURE_TIME )
          {
            pCat->req_fhrs[m] += pObj->treq[m];
            if ( pObj->res == RESOLUTION_CODE_LOW ) pCat->req_fhrs_lores[m] += pObj->treq[m];
            else                                    pCat->req_fhrs_hires[m] += pObj->treq[m];
          }
        }
      }
      else pCat->num_objects_impossible ++;
    }

    
    //increment the counters and totals for the catlist
    if ( pCat->num_objects_hires       > 0) pCatList->num_cats_hires      ++;
    if ( pCat->num_objects_lores       > 0) pCatList->num_cats_lores      ++;
    if ( pCat->num_objects_exgal       > 0) pCatList->num_cats_exgal      ++;
    if ( pCat->num_objects_exgal_lores > 0) pCatList->num_cats_exgal_lores++;
    if ( pCat->num_objects_exgal_hires > 0) pCatList->num_cats_exgal_hires++;
    if ( pCat->num_objects_gal         > 0) pCatList->num_cats_gal        ++;
    if ( pCat->num_objects_gal_lores   > 0) pCatList->num_cats_gal_lores  ++;
    if ( pCat->num_objects_gal_hires   > 0) pCatList->num_cats_gal_hires  ++;
      
    for(m=0;m<MAX_NUM_MOON_PHASES;m++)
    {
      pCat->req_fhrs[m]       *= MINS_TO_HOURS;
      pCat->req_fhrs_lores[m] *= MINS_TO_HOURS;
      pCat->req_fhrs_hires[m] *= MINS_TO_HOURS;
      pCatList->req_fhrs[m]       += pCat->req_fhrs[m];
      pCatList->req_fhrs_lores[m] += pCat->req_fhrs_lores[m];
      pCatList->req_fhrs_hires[m] += pCat->req_fhrs_hires[m];
    }
  }

  
  if ( pSurvey->debug )
    fprintf(stdout, "%s Finished reading and processing input catalogues (that took %.3fs)\n",
            COMMENT_PREFIX, (double) (clock()-clocks_start)/(double)CLOCKS_PER_SEC);  fflush(stdout);

  
  return 0;
}
////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////
///read in a catalogue of objects from a fits file
//assume that sufficient space has already been allocated for the objectlist array
int read_input_catalogue_fits( objectlist_struct *pObjList,
                               survey_struct *pSurvey,
                               catalogue_struct* pCat,
                               cataloguelist_struct* pCatList)
{
  fitsfile *fptr;     // FITS file pointer
  int status = 0;    // CFITSIO status value
  long nrows = 0;
  long nrows_total = 0;
  long nrows_per_chunk = 0;
  uint i, j;
  
  int  iNum_object_this_cat = 0;
  int  iNum_total_object_this_cat = 0;

  //temporary arrays for holding input data -
  //  char**  pptemp_object_id     = NULL;
  //  char*   ptemp_object_id      = NULL;
  //  long*   ptemp_idnum     = NULL;
  double* ptemp_ra        = NULL;
  double* ptemp_dec       = NULL;
  int*    ptemp_priority  = NULL;
  float*  ptemp_texp_d    = NULL;
  float*  ptemp_texp_g    = NULL;
  float*  ptemp_texp_b    = NULL;
  utiny*  ptemp_res       = NULL;
  float*  ptemp_mag       = NULL;
  char**  pptemp_template = NULL;
  char*   ptemp_template  = NULL;
  float*  ptemp_redshift  = NULL;
  int*    ptemp_sub_cat_id= NULL;

  //  int colwidth_object_id     = 15+1;  //todo - read this from the file;
  //  int colwidth_template = 30+1;  //todo - read this from the file;
  //  long colwidth_object_id     = 0;  //read this from the file;
  long colwidth_template = 0;  //read this from the file;

  //  int colnum_object_id = 0;
  //  int colnum_idnum = 0;
  int colnum_ra = 0;
  int colnum_dec = 0;
  int colnum_priority = 0;
  int colnum_texp_d = 0;
  int colnum_texp_g = 0;
  int colnum_texp_b = 0;
  int colnum_res = 0;
  int colnum_mag = 0;
  int colnum_template = 0;      
  int colnum_redshift = 0;
  int colnum_sub_cat_id = 0;

  //  int chunk;
  //  char last_cluster_name[STR_MAX];
  //  float last_cluster_redshift = NAN;
  //  int cluster_ID = -1;
  int last_cluster_ID = -1;
  char str_format[STR_MAX];
  
  if ( pObjList == NULL ||
       pSurvey  == NULL ||
       pCat     == NULL ||
       pCatList == NULL) return 1;

  snprintf(str_format, STR_MAX, "%%s   Reading catalogue %%-%ds (FITS,filename %%-%ds) ... ",
           pCatList->longest_cat_codename, pCatList->longest_cat_filename);
  //  fprintf(stdout, "%s   Reading catalogue %-10s (FITS,filename %-80s) ... ",
  fprintf(stdout, str_format, COMMENT_PREFIX, pCat->codename, pCat->filename);
  fflush(stdout);


  pCat->num_objects = 0;
  pCat->num_valid_objects = 0;
  for(i=0;i<MAX_SUBCATS;i++)
  {
    pCat->num_valid_objects_subcat[i] = 0;
  }

  //special treatment for clusters
  if ( pCat->code == CATALOGUE_CODE_S5 )
  {
    //    strcpy(last_cluster_name, ""); //init the last_cluster_name
    if ( pSurvey->pClusterList == NULL )
    {
      if ( clusterlist_struct_init ((clusterlist_struct**) &(pSurvey->pClusterList), (int) CLUSTERS_PER_CHUNK ))
      {
        fprintf(stderr, "%s There were problems setting up the Clusters array\n", ERROR_PREFIX);
        return 1;
      }
    }
    else
    {
      fprintf(stderr, "%s The clusterlist is already initialised - maybe you supplied multiple clusters catalogues!\n",
              ERROR_PREFIX);
      return 1;
    }
  }
  //////////////////////////////////////////////////

  pCat->index_of_first_object = pObjList->nObjects; //this can be used later to skip ahead to just
                                                    //the objects for this catalogue 

  
  if (fits_open_table((fitsfile**) &fptr, (char*) pCat->filename, READONLY, (int*) &status) == 0 )
  {
    int ncols = 0;
    if (fits_get_num_rows(fptr, &nrows_total, &status))
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }

    if ( pObjList->max_objects_per_catalogue > 0 &&
         pObjList->max_objects_per_catalogue < LONG_MAX &&
         nrows_total > pObjList->max_objects_per_catalogue )
    {
      nrows = (long) pObjList->max_objects_per_catalogue;
    }
    else nrows = nrows_total;


    if( fits_get_num_cols(fptr, &ncols, &status))
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }
    
    if (fits_get_rowsize(fptr, &nrows_per_chunk, &status))
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }
//    fprintf(stdout, "\n%s Catalogue \"%s\" ncols=%d nrows=%ld nrows_per_chunk=%ld\n",
//            COMMENT_PREFIX, pCat->filename,
//            ncols,nrows,nrows_per_chunk);
//    fflush(stdout);
    
    
    //get the column numbers
    //    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "OBJECT_ID",   &colnum_object_id, &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "RA",          &colnum_ra       , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "DEC",         &colnum_dec      , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "PRIORITY",    &colnum_priority , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "TEXP_D",      &colnum_texp_d   , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "TEXP_G",      &colnum_texp_g   , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "TEXP_B",      &colnum_texp_b   , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "RESOLUTION",  &colnum_res      , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "R_MAG",       &colnum_mag      , &status);
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "TEMPLATE",    &colnum_template , &status);

    //    if ( colnum_object_id<= 0 || colnum_object_id> ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "OBJECT_ID"   );return 1;};
    if ( colnum_ra       <= 0 || colnum_ra       > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "RA"          );return 1;};
    if ( colnum_dec      <= 0 || colnum_dec      > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "DEC"         );return 1;};
    if ( colnum_priority <= 0 || colnum_priority > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "PRIORITY"    );return 1;};
    if ( colnum_texp_d   <= 0 || colnum_texp_d   > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "TEXP_D"      );return 1;};
    if ( colnum_texp_g   <= 0 || colnum_texp_g   > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "TEXP_G"      );return 1;};
    if ( colnum_texp_b   <= 0 || colnum_texp_b   > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "TEXP_B"      );return 1;};
    if ( colnum_res      <= 0 || colnum_res      > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "RESOLUTION"  );return 1;};
    if ( colnum_mag      <= 0 || colnum_mag      > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "R_MAG"       );return 1;};
    if ( colnum_template <= 0 || colnum_template > ncols ) {fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "TEMPLATE"    );return 1;};

    if (status)
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }

    //see if there is a REDSHIFT column
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "REDSHIFT",    &colnum_redshift, &status);
    if (status)
    {
      if (pCat->is_extragalactic )
      {
        fprintf(stderr,"%s Problem reading %s column from file\n", ERROR_PREFIX, "REDSHIFT" );
        fits_report_error(stderr, status); // print any error messages
        return 1;
      }
      else
      {
        colnum_redshift  = -1;
        status = 0;
      }
    }

    //see if there is a SUB_CAT_ID column
    fits_get_colnum ((fitsfile*) fptr, CASEINSEN, "SUB_CAT_ID",  &colnum_sub_cat_id, &status);
    if (status == COL_NOT_FOUND )
    {
      colnum_sub_cat_id  = -1;
      status = 0;
    }

    
    if (status)
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }
    //    fprintf(stdout, "%s Placeholder at file %s line %d\n", DEBUG_PREFIX, __FILE__, __LINE__);fflush(stdout);


    //read the width of the TEMPLATE column
    {
      int typecode;
      long repeat;
      long width;

//      ///read the width of the OBJECT_ID column
//      if ( fits_get_coltype((fitsfile*) fptr, (int) colnum_object_id, (int*) &typecode, (long*) &repeat, (long*) &width, (int*) &status))
//      {
//        fits_report_error(stderr, status); // print any error messages
//        fprintf(stderr,"%s Problem reading coltype of %s column\n", ERROR_PREFIX, "OBJECT_ID");
//        return 1;
//      }
//      if ( width > 0 )  colwidth_object_id = width+1;
//      else
//      {
//        fprintf(stderr,"%s Illegal column width for %s column (width=%ld)\n", ERROR_PREFIX, "OBJECT_ID", width);
//        return 1;
//      }

      //read the width of the TEMPLATE column
      if ( fits_get_coltype((fitsfile*) fptr, (int) colnum_template, (int*) &typecode, (long*) &repeat, (long*) &width, (int*) &status))
      {
        fits_report_error(stderr, status); // print any error messages
        fprintf(stderr,"%s Problem reading coltype of %s column\n", ERROR_PREFIX, "TEMPLATE");
        return 1;
      }
      if ( width > 0 )  colwidth_template = width+1;
      else
      {
        fprintf(stderr,"%s Illegal column width for %s column (width=%ld)\n", ERROR_PREFIX, "TEMPLATE", width);
        return 1;
      }
    }



    
    //make some space for the temporary arrays 
    if ( nrows_per_chunk > 0 )
    {
      //      if ((pptemp_object_id= (char**)  malloc(sizeof(char*) *nrows_per_chunk)) == NULL ||
      //          (ptemp_object_id = (char*)   malloc(sizeof(char)  *nrows_per_chunk*colwidth_object_id)) == NULL || 
      if ((ptemp_ra        = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
          (ptemp_dec       = (double*) malloc(sizeof(double)*nrows_per_chunk)) == NULL ||  
          (ptemp_priority  = (int*)    malloc(sizeof(int)   *nrows_per_chunk)) == NULL ||  
          (ptemp_texp_d    = (float*)  malloc(sizeof(float) *nrows_per_chunk)) == NULL ||  
          (ptemp_texp_g    = (float*)  malloc(sizeof(float) *nrows_per_chunk)) == NULL ||  
          (ptemp_texp_b    = (float*)  malloc(sizeof(float) *nrows_per_chunk)) == NULL ||  
          (ptemp_res       = (utiny*)  malloc(sizeof(utiny) *nrows_per_chunk)) == NULL ||  
          (ptemp_mag       = (float*)  malloc(sizeof(float) *nrows_per_chunk)) == NULL ||
          (pptemp_template = (char**)  malloc(sizeof(char*) *nrows_per_chunk)) == NULL ||
          (ptemp_template  = (char*)   malloc(sizeof(char)  *nrows_per_chunk*colwidth_template)) == NULL ||
          (ptemp_sub_cat_id= (int*)    malloc(sizeof(int)   *nrows_per_chunk)) == NULL  )
      {
        fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
                ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      if ( colnum_redshift > 0 )
      {
        if ((ptemp_redshift = (float*)  malloc(sizeof(float) *nrows_per_chunk)) == NULL )
        {
          fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
                  ERROR_PREFIX, __FILE__, __LINE__);
          return 1;
        }
      }
      //make sure that the pointers for the string arrays point to the correct locations 
      //      for(i=0;i<nrows_per_chunk;i++)  pptemp_object_id[i] = (char*) &(ptemp_object_id[i*colwidth_object_id]);
      for(i=0;i<nrows_per_chunk;i++)  pptemp_template[i]  = (char*) &(ptemp_template[i*colwidth_template]);
    }
    else
    {
      //nrows_per_chunk is zero so exit with error...
      return 1;
    }

    //now read through the file a "nrows_per_chunk" chunk at a time - to maximise efficiency
    //    chunk = 0;
    for ( i=0; i<nrows; i+=nrows_per_chunk )
    {
      long nrows_this_chunk = MIN(nrows_per_chunk, nrows-i);
      //      fprintf(stdout, "%s Reading chunk %8d nrows_this_chunk=%8ld (rows %8ld -> %8ld) ... ",
      //              COMMENT_PREFIX, chunk, nrows_this_chunk, (long)i+1, (long) i+nrows_this_chunk);fflush(stdout);
      
      //      fits_read_col(fptr, TSTRING, colnum_object_id,i+1, 1, nrows_this_chunk, NULL, pptemp_object_id,NULL, &status);
      fits_read_col(fptr, TDOUBLE, colnum_ra,       i+1, 1, nrows_this_chunk, NULL, ptemp_ra,        NULL, &status);
      fits_read_col(fptr, TDOUBLE, colnum_dec,      i+1, 1, nrows_this_chunk, NULL, ptemp_dec,       NULL, &status);
      fits_read_col(fptr, TINT,    colnum_priority, i+1, 1, nrows_this_chunk, NULL, ptemp_priority,  NULL, &status);
      fits_read_col(fptr, TFLOAT,  colnum_texp_d,   i+1, 1, nrows_this_chunk, NULL, ptemp_texp_d,    NULL, &status);
      fits_read_col(fptr, TFLOAT,  colnum_texp_g,   i+1, 1, nrows_this_chunk, NULL, ptemp_texp_g,    NULL, &status);
      fits_read_col(fptr, TFLOAT,  colnum_texp_b,   i+1, 1, nrows_this_chunk, NULL, ptemp_texp_b,    NULL, &status);
      fits_read_col(fptr, TBYTE,   colnum_res,      i+1, 1, nrows_this_chunk, NULL, ptemp_res,       NULL, &status);
      fits_read_col(fptr, TFLOAT,  colnum_mag,      i+1, 1, nrows_this_chunk, NULL, ptemp_mag,       NULL, &status);
      fits_read_col(fptr, TSTRING, colnum_template, i+1, 1, nrows_this_chunk, NULL, pptemp_template, NULL, &status);

      if ( colnum_redshift > 0 )
        fits_read_col(fptr, TFLOAT,  colnum_redshift,i+1, 1, nrows_this_chunk, NULL, ptemp_redshift,   NULL, &status);

      if ( colnum_sub_cat_id > 0 )
        fits_read_col(fptr, TINT,  colnum_sub_cat_id,i+1, 1, nrows_this_chunk, NULL, ptemp_sub_cat_id, NULL, &status);

      if (status)
      {
        fits_report_error(stderr, status); // print any error messages
        return 1;
      }
      //      fprintf(stdout, "done1 ...");fflush(stdout);
      // copy the values from the temp arrays into the pObj structures
      for ( j=0; j<nrows_this_chunk; j++)
      {
        object_struct *pObj = (object_struct*) &(pObjList->pObject[pObjList->nObjects]);
        //make sure that the exposure times are sensible
        if ( isnan(ptemp_texp_d[j]) || ptemp_texp_d[j] <= 0.0 ) ptemp_texp_d[j] = (float) (MAX_EXPOSURE_TIME + 0.1);
        if ( isnan(ptemp_texp_g[j]) || ptemp_texp_g[j] <= 0.0 ) ptemp_texp_g[j] = (float) (MAX_EXPOSURE_TIME + 0.1);
        if ( isnan(ptemp_texp_b[j]) || ptemp_texp_b[j] <= 0.0 ) ptemp_texp_b[j] = (float) (MAX_EXPOSURE_TIME + 0.1);
        pObj->ra                      = (double) ptemp_ra[j];
        pObj->dec                     = (double) ptemp_dec[j];
        pObj->priority                = (float)  ptemp_priority[j];
        pObj->treq[MOON_PHASE_DARK]   = (float)  ptemp_texp_d[j];
        pObj->treq[MOON_PHASE_GREY]   = (float)  ptemp_texp_g[j];
        pObj->treq[MOON_PHASE_BRIGHT] = (float)  ptemp_texp_b[j];
        pObj->mag                     = (float)  ptemp_mag[j];
        pObj->res                     = (utiny)  ptemp_res[j];
        
        //        pObj->cat_code    = (utiny) pCat->code;
        pObj->pCat        = (catalogue_struct*) pCat;
        pObj->id          = (uint) pObjList->nObjects;
        pObj->line_number = (uint) j;

        //this counter counts all potential objects, not just the valid ones:
        iNum_total_object_this_cat ++;

        //skipping some input sources screws up writing out the catalogue - so just read everything
//        //We always check against the bounds of the survey - to speed things up later
//        //use separate checks to reduce number of comparisions
//        if ( geom_test_if_in_long_lat_range (pObj->ra, pObj->dec,
//                                             pSurvey->bounds_ra_min, pSurvey->bounds_ra_max,
//                                             MIN(DEFAULT_SURVEY_DEC_LIMIT_MIN,pSurvey->bounds_dec_min),
//                                             MAX(DEFAULT_SURVEY_DEC_LIMIT_MAX,pSurvey->bounds_dec_max)) == 0 ) continue;

        pObj->redshift = (float) NAN; //initialise
        if ( colnum_redshift > 0   ) pObj->redshift = (float) ptemp_redshift[j]; //just copy the redshift

        pObj->sub_code = (int) 0;
        if ( colnum_sub_cat_id > 0 )  pObj->sub_code = (int) ptemp_sub_cat_id[j];

        //read some extra information
        switch ((int) pObj->pCat->code)
        {
        case CATALOGUE_CODE_S1 :
          // sub_cat_id is the HPX pixel index for nside=OPSIM_STATISTICS_FOM_S1_HPX_NSIDE
          {          
            long healpix_pix_temp;
            (void) ang2pix_nest(OPSIM_FOMS_FOM_S1_HPX_NSIDE,
                                ((double) 90.0 - pObj->dec)*(double) DEG_TO_RAD, pObj->ra*(double) DEG_TO_RAD,
                                (long*) &healpix_pix_temp);
            pObj->sub_code = (int) healpix_pix_temp;
          }
          break;
        case CATALOGUE_CODE_S2 :
          break;
        case CATALOGUE_CODE_S3 :
          if ( pObj->sub_code != SUBCATALOGUE_CODE_S3_ESN       &&
               pObj->sub_code != SUBCATALOGUE_CODE_S3_DYNDISK1  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S3_DYNDISK2  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S3_CHEMDISK  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S3_BULGE     )
          {
            pObj->sub_code = (int) SUBCATALOGUE_CODE_S3_UNKNOWN; //emit an error
            fprintf(stderr, "%s Failed to assign sub-catalogue code to target on line %d\n",
                    ERROR_PREFIX, pObj->line_number);
          }
          break;
        case CATALOGUE_CODE_S4 :
          //          if ( pObj->sub_code != SUBCATALOGUE_CODE_S4_OUTERDISK &&
          //               pObj->sub_code != SUBCATALOGUE_CODE_S4_INNERDISK &&
          //               pObj->sub_code != SUBCATALOGUE_CODE_S4_BULGE     )
          //          {
          if ( pObj->sub_code != SUBCATALOGUE_CODE_S4_BULGE &&
               pObj->sub_code != SUBCATALOGUE_CODE_S4_INNERDISK &&
               pObj->sub_code != SUBCATALOGUE_CODE_S4_OUTERDISK &&
               pObj->sub_code != SUBCATALOGUE_CODE_S4_UNBIASED )
          {
            pObj->sub_code = (int) SUBCATALOGUE_CODE_S4_UNKNOWN; //emit an error
            fprintf(stderr, "%s Failed to assign sub-catalogue code to target on line %d\n",
                    ERROR_PREFIX, pObj->line_number);
          }
          break;
        case CATALOGUE_CODE_S5 :
          //we can assume that the Clusters are sorted, so that all members
          //of a single cluster appear consecutively.
          // pObj->sub_code = pObj->sub_code-1;   // make the list zero-indexed
          assert(pObj->sub_code >= 0); 
          if ( pObj->sub_code != last_cluster_ID )
          {
            cluster_struct tempCluster;
            if ( cluster_struct_init((cluster_struct*) &tempCluster)) return 1;

            snprintf(tempCluster.name, CLUSTERS_NAME_MAX_LENGTH, "Cluster%06d", pObj->sub_code);
            tempCluster.id = (int) pObj->sub_code;
            tempCluster.redshift = pObj->redshift;
            tempCluster.objid_BCG = (int) pObj->id; //assume that the BCG is the first cluster in the list
            if (clusterlist_struct_add_cluster ((clusterlist_struct*) pSurvey->pClusterList, (cluster_struct*) &tempCluster))
            {
              fprintf(stderr, "%s Problem adding new cluster (%d) to cluster list\n", ERROR_PREFIX, pObj->sub_code );
              return 1;
            }

//            fprintf(stdout, "DEBUG added CLUSTER ID=%6d NAME=%s (%d)\n",
//                    pSurvey->pClusterList->nClusters,
//                    pSurvey->pClusterList->pCluster[pSurvey->pClusterList->nClusters-1].name,
//                    pObj->sub_code);
//            fflush(stdout);
            last_cluster_ID = pObj->sub_code;
          }

          break;
        case CATALOGUE_CODE_S6 :
          pObj->sub_code = SUBCATALOGUE_CODE_S6_SUBSET1;
          break;
        case CATALOGUE_CODE_S7 :
          if ( pObj->sub_code != SUBCATALOGUE_CODE_S7_DEEP &&
               pObj->sub_code != SUBCATALOGUE_CODE_S7_WIDE )
            //               pObj->sub_code != SUBCATALOGUE_CODE_S7_ULTRADEEP )
          {
            pObj->sub_code = (int) SUBCATALOGUE_CODE_S7_UNKNOWN; //should emit an error
            fprintf(stderr, "%s Failed to assign sub-catalogue code to target on line %d\n",
                    ERROR_PREFIX, pObj->line_number);
          }
          break;
        case CATALOGUE_CODE_S8 :
          // sub_cat_id is the HPX pixel index for nside=OPSIM_STATISTICS_FOM_S8_HPX_NSIDE
          {          
            long healpix_pix_temp;
            (void) ang2pix_nest(OPSIM_FOMS_FOM_S8_HPX_NSIDE,
                                ((double) 90.0 - pObj->dec)*(double) DEG_TO_RAD, pObj->ra*(double) DEG_TO_RAD,
                                (long*) &healpix_pix_temp);
            pObj->sub_code = (int) healpix_pix_temp;
          }
          break;
          //        case CATALOGUE_CODE_S8 :
          //          pObj->sub_code = SUBCATALOGUE_CODE_S8_SUBSET1;
          //          break;
        case CATALOGUE_CODE_S9 :
          if ( pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET1  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET2  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET3  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET4  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET5  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET6  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET7  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET8  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET9  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_HR_SUBSET10 &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET1  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET2  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET3  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET4  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET5  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET6  &&
               pObj->sub_code != SUBCATALOGUE_CODE_S9_LR_SUBSET7 )
          {
            pObj->sub_code = (int) SUBCATALOGUE_CODE_S9_UNKNOWN; //should emit an error
            fprintf(stderr, "%s Failed to assign sub-catalogue code to target on line %d\n",
                    ERROR_PREFIX, pObj->line_number);
          }
          break;
          
        default :
          //do nothing special for other catalogues
          //          pObj->sub_code = (int) 0;
          break;  
        }
        
        //these counters record only the number of _valid_ objects
        pObjList->nObjects++;
        iNum_object_this_cat ++;
        if (pObj->sub_code >=0 && pObj->sub_code < MAX_SUBCATS)
        {
          pCat->num_valid_objects_subcat[pObj->sub_code] ++;
        }
        //keep track of the range of sub_codes within this catalogue
        if ( pObj->sub_code > pCat->sub_code_max ) pCat->sub_code_max = pObj->sub_code;
        if ( pObj->sub_code < pCat->sub_code_min ) pCat->sub_code_min = pObj->sub_code;
      }
      //      chunk ++;
      //      fprintf(stdout, "done2\n");fflush(stdout);
    }
    //    fprintf(stdout, "%s Placeholder at file %s line %d\n", DEBUG_PREFIX, __FILE__, __LINE__);fflush(stdout);

    status = 0;
    fits_close_file(fptr, &status);
    fptr = NULL;
    //tidy up temp arrays
    //    FREETONULL(pptemp_object_id);
    //    FREETONULL(ptemp_object_id );
    FREETONULL(ptemp_ra        );
    FREETONULL(ptemp_dec       );
    FREETONULL(ptemp_priority  );
    FREETONULL(ptemp_texp_d    );
    FREETONULL(ptemp_texp_g    );
    FREETONULL(ptemp_texp_b    );
    FREETONULL(ptemp_res       );
    FREETONULL(ptemp_mag       );
    FREETONULL(pptemp_template );
    FREETONULL(ptemp_template  );
    FREETONULL(ptemp_redshift  );
  }


  pCat->index_of_last_object = (int) pObjList->nObjects - 1;
  pCat->num_objects          = (int) iNum_total_object_this_cat;
  pCat->num_valid_objects    = (int) iNum_object_this_cat;
  pCat->num_lines            = (int) nrows;
  fflush(stdout);

  fprintf(stdout, " read %9d valid objects (there were %9ld total objects)",
          (int)   pCat->num_valid_objects,
          (long)  nrows_total);
  
  fflush(stdout);

  if ( (int) pCat->code == (int) CATALOGUE_CODE_S5 )
  {
    //report/record the number of unique clusters
    //    pSurvey->num_unique_clusters = cluster_ID + 1;
    pSurvey->num_unique_clusters = pSurvey->pClusterList->nClusters;
    fprintf(stdout, " - there were %s%d unique clusters",
            (pObjList->max_objects_per_catalogue > 0 && pObjList->max_objects_per_catalogue < INT_MAX ? ">=" : ""),
            (int) pSurvey->num_unique_clusters);
    fflush(stdout);
    if ( clusterlist_struct_tidy ((clusterlist_struct*) pSurvey->pClusterList))
    {
      fprintf(stderr, "%s Problem tidying the Clusterlist\n", ERROR_PREFIX);
      return 1;
    }
  }
  //  fprintf(stdout, "...all done\n");
  fprintf(stdout, "\n");
  fflush(stdout);
  
  if (status) fits_report_error(stderr, status); // print any error messages
  return 0;
}
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////



////
int objectlist_process_objects (objectlist_struct *pObjList, survey_struct *pSurvey)
{
  //    we will now do some initial processing for each object
  int i,m;

//  const double ra_min    = (double)   0.0;
//  const double ra_max    = (double) 360.0;
//  const double dec_min   = (double) -90.0;
//  const double dec_max   = (double) +90.0;
//  double dra0;   //width at the equator
//  double ddec0;
//  double inv_ddec0;
//  double *pinv_dra =  NULL;
//  double *pdra     =  NULL;
//  int    *pnra     =  NULL;

  
  fprintf(stdout, "%s Processing and assigning valid objects to dark/grey/bright surveys - Segregating targets %s\n",
          COMMENT_PREFIX,
          (pSurvey->moon_split_code == SURVEY_MOON_SPLIT_BY_EXPOSURE ? "by required exposure time" :
           (pSurvey->moon_split_code == SURVEY_MOON_SPLIT_BY_MAGNITUDE ? "by target magnitude" :
            (pSurvey->moon_split_code == SURVEY_MOON_SPLIT_BY_SKY_AREA   ? "by target sky position" :
             "by an unknown method!"))));
  for (m=0;m<pSurvey->num_moon_groups;m++) pSurvey->moon[m].num_objects = (int) 0;
  pSurvey->total_objects_lores       = 0;
  pSurvey->total_objects_hires       = 0;
  pSurvey->total_objects_exgal       = 0;
  pSurvey->total_objects_exgal_lores = 0;
  pSurvey->total_objects_exgal_hires = 0;
  pSurvey->total_objects_gal         = 0;
  pSurvey->total_objects_gal_lores   = 0;
  pSurvey->total_objects_gal_hires   = 0;
  pSurvey->total_objects_impossible  = 0;
  pSurvey->total_objects_impossible_lores = 0;
  pSurvey->total_objects_impossible_hires = 0;

//  //prepare the output map pixels
//  if ( pSurvey->map_npix_ra <= 0 || pSurvey->map_npix_dec <= 0 ) return 1;
//  dra0   = (double) ( ra_max -  ra_min) / ((double) pSurvey->map_npix_ra) ;   //width at the equator
//  ddec0  = (double) (dec_max - dec_min) / ((double) pSurvey->map_npix_dec) ;
//  if ( ddec0 > 0.0 ) inv_ddec0 = (double) 1.0 /ddec0;
//  else               return 1;
//
//  if ((pdra     = (double*) malloc(sizeof(double) * pSurvey->map_npix_dec))     == NULL ||
//      (pinv_dra = (double*) malloc(sizeof(double) * pSurvey->map_npix_dec))     == NULL ||
//      (pnra     = (int*)    malloc(sizeof(int)    * pSurvey->map_npix_dec))     == NULL )
//  {
//    fprintf(stderr, "%s Error assigning memory at file %s line %d\n",
//            ERROR_PREFIX, __FILE__, __LINE__);
//    return 1;
//  }
//
//  for(i=0;i<pSurvey->map_npix_dec;i++)
//  {
//    double dec0  = dec_min + ddec0*((double) i + 0.5);
//    double dec1  = dec0 - ddec0*0.5;
//    double dec2  = dec0 + ddec0*0.5;
//    double width = (ra_max - ra_min)*cosd(dec0);
//    double npix  = floor(width / dra0);
//    pnra[i] = (int) npix;
//    pdra[i] = (ra_max - ra_min) / (double) npix;
//    pinv_dra[i] = ( pdra[i] > 0.0 ? (double)  1.0 / pdra[i] : NAN);
//  }




  //LOOP THROUGH ALL THE OBJECTS
  for(i=0; i < pObjList->nObjects; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    catalogue_struct *pCat = (catalogue_struct*) (pObj->pCat);

    //get the healpix pixel within which this object lies
    if ( pSurvey->healpix_nside > 0 )
    {
      long healpix_pix_temp;
      (void) ang2pix_nest(pSurvey->healpix_nside,
                          ((double) 90.0 - pObj->dec)*(double) DEG_TO_RAD, pObj->ra*(double) DEG_TO_RAD,
                          (long*) &healpix_pix_temp);
      pObj->healpix_pix = (uint) healpix_pix_temp;
    }
//    //now calc the pixel in the output density maps 
//    pObj->map_pix_y = (int) ((pObj->dec - dec_min)*inv_ddec0); 
//    pObj->map_pix_x = (int) ((pObj->ra  - ra_min)*pinv_dra[ pObj->map_pix_y]);

    
    pObj->to_observe = (BOOL) FALSE;
    pObj->num_fields = (utiny) 0;  //number of fields that this object lies within
    pObj->pObjField = NULL;

//    for (j=0;j<MAX_FIELDS_PER_OBJECT;j++)
//    {
//      pObj->pfield_ids[j] = (uint) 0;   // list of ids of fields that this object lies within;
//      pObj->pperim_flags[j] = (utiny) FIELD_FLAG_OUTSIDE;
//      pObj->pnfibs[j]     = (uint) 0;         //number of fibers that could reach this object in each field that can see it
//      pObj->pntiles[j]    = (utiny) 0;
//
//      //      pObj->pwas_observed[j] = (unsigned long) 0; //this receives the result of the run_field_mbr function
//      pObj->ptexp[MOON_PHASE_DARK][j] = (float) 0.0; //delivered exposure time in each field (minutes) 
//      pObj->ptexp[MOON_PHASE_GREY][j] = (float) 0.0;
//      pObj->ptexp[MOON_PHASE_BRIGHT][j] = (float) 0.0; 
//    }
    pObj->was_observed_last_tile = (ulong) 0; //

    //sort out priorities - apply rescaling parameters, and then normalise to 0.0-1.0 range
    switch ( pCat->priority_rescale_method )
    {
    case PRIORITY_RESCALE_TYPE_NONE :
      pObj->priority =  (float) 0.01 * (pObj->priority);
      break;
    case PRIORITY_RESCALE_TYPE_LINEAR :
      pObj->priority =  (float) 0.01 * (pObj->priority * pCat->priority_rescale_param1
                                        + pCat->priority_rescale_param2);
      break;
    default :
      //TBD
      break;
    }


    for(m=MOON_PHASE_BRIGHT;m<=MOON_PHASE_DARK;m++)
    {
      //sort out exposure times to reflect the fudge factors in the param file
      pObj->treq[m]  *= pCat->exposure_scaling;
      //flag any targets that are impossible in this moon phase
      if ( pObj->treq[m] >= (float) pSurvey->discard_threshold_texp )
        pObj->treq[m] = (float) MAX_EXPOSURE_TIME;  

//      //sort out exposure times to reflect the fudge factors in the param file
//      if ( pObj->treq[m] >= (float) pSurvey->discard_threshold_texp )
//        pObj->treq[m]  = (float) MAX_EXPOSURE_TIME;   //flag these targets as impossible in this moon phase
//      else
//        pObj->treq[m]  = MIN(pCat->exposure_scaling*pObj->treq[m], (float) MAX_EXPOSURE_TIME); 
    }

    //count the objects that cannot be done under any circumstances
    if ( pObj->treq[MOON_PHASE_DARK] >= MAX_EXPOSURE_TIME )
    {
      pSurvey->total_objects_impossible++;
      if ( pObj->res == RESOLUTION_CODE_HIGH ) pSurvey->total_objects_impossible_hires++;
      else                                     pSurvey->total_objects_impossible_lores++;
    }

    
    //this will keep a track of how much exposure time has been executed for this object
    pObj->TexpFracDone = (float) 0.0;
   
    
    //put the object in the correct moon phase part of the survey
    pObj->in_survey = (uint) SURVEY_NONE;
    switch ( pSurvey->moon_split_code ) 
    {
    case SURVEY_MOON_SPLIT_BY_EXPOSURE : //split using the supplied exposure times
      switch (pSurvey->strategy_code )
      {
      case SURVEY_STRATEGY_FLAT :
        if ( pObj->treq[MOON_PHASE_BRIGHT] <= pSurvey->moon_split_by_exposure_texp_bright_threshold )
        {
          pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_BRIGHT ) ;
        }
        if ( pObj->treq[MOON_PHASE_BRIGHT]  >= pSurvey->pmoon_bb->Minutes_net_per_field )
        {
          //not all objects go into the dark survey
          pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_DARKGREY ) ;
        }
        break;
      case SURVEY_STRATEGY_DBPDG :
        if ( pObj->treq[MOON_PHASE_BRIGHT] <= pSurvey->moon_split_by_exposure_texp_bright_threshold )
        {
          pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_BRIGHT ) ;
        }
        //all objects can go into the dark survey
        pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_DARKGREY ) ;
        break;
      default: return 1;
        break;
      }
      break;      
    case SURVEY_MOON_SPLIT_BY_MAGNITUDE :    //split using the supplied mag limits
      if ( pObj->res == RESOLUTION_CODE_HIGH ) //high res
      {
        if ( pObj->mag <= pSurvey->moon_split_mag_high_res) pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_BRIGHT ) ;
        else                                                pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_DARKGREY ) ;
      }
      else   //low res
      {
        if ( pObj->mag <= pSurvey->moon_split_mag_low_res)  pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_BRIGHT ) ;
        else                                                pObj->in_survey = (uint) ( pObj->in_survey |  SURVEY_DARKGREY ) ;
      }
      break;

    case SURVEY_MOON_SPLIT_BY_SKY_AREA :
      //split using just the sky areas - so we cannot actually do any useful flagging yet
      pObj->in_survey = (uint) SURVEY_ANY ;
      break;

    default:
      fprintf(stderr, "%s  I do not understand this SURVEY.MOON_SPLIT.METHOD: >%d<\n",
              ERROR_PREFIX, pSurvey->moon_split_code);
      return 1;
      break;
    }
    
    //count up the number of objects in each moon phase
    for ( m=0;m<pSurvey->num_moon_groups;m++) if ( pObj->in_survey & pSurvey->moon[m].mask ) pSurvey->moon[m].num_objects++;
    //and of each resolution type 
    if ( pObj->res == RESOLUTION_CODE_HIGH ) pSurvey->total_objects_hires++;
    if ( pObj->res == RESOLUTION_CODE_LOW  ) pSurvey->total_objects_lores++;
    if ( pCat->is_extragalactic )
    {
      pSurvey->total_objects_exgal++;
      if ( pObj->res == RESOLUTION_CODE_HIGH ) pSurvey->total_objects_exgal_hires++;
      if ( pObj->res == RESOLUTION_CODE_LOW  ) pSurvey->total_objects_exgal_lores++;
    }
    else
    {
      pSurvey->total_objects_gal++;
      if ( pObj->res == RESOLUTION_CODE_HIGH ) pSurvey->total_objects_gal_hires++;
      if ( pObj->res == RESOLUTION_CODE_LOW  ) pSurvey->total_objects_gal_lores++;
    }
  }
  //report the resulting numbers - if appropriate
  if (   pSurvey->moon_split_code != SURVEY_MOON_SPLIT_BY_SKY_AREA  )
  {
    for ( m=0;m<pSurvey->num_moon_groups;m++)
    {
      fprintf(stdout, "%s I read in %9d objects for the %8s survey)\n",
              COMMENT_PREFIX, pSurvey->moon[m].num_objects, pSurvey->moon[m].longname);
    }
  }

  return 0;
}





/////////////////////////////////////////

///////////////////////////////////////////
int survey_calc_times_and_tiles (survey_struct *pSurvey, fieldlist_struct* pFieldList, telescope_struct *pTele)
{
  int m;
  if ( pSurvey    == NULL ||
       pTele      == NULL ||
       pFieldList == NULL ) return 1;

  ///////////////////////////////////////////////
  /////we now compute the time available per tile/field etc
  //need to calc the on-sky time available over the full survey
  //this is the same calculation for all survey strategies
//these are calculated earlier  if ( survey_calc_total_time_available((survey_struct*) pSurvey, (telescope_struct*) pTele))
//these are calculated earlier  {
//these are calculated earlier    return 1;
//these are calculated earlier  }
  
//   //now calc the exposure times depending on the survey strategy
//   switch ( pSurvey->goal_code ) 
//   {
//     /////////////////////////////////////////////////////////////////////////////////
//   case SURVEY_GOAL_AREA :
//     //we must complete the designated number of tiles, so we can now calculate the implied exposure time
// //    if (survey_calc_times_with_goal_area((survey_struct*) pSurvey))
// //    {
// //      return 1;
// //    }
//     fprintf(stderr, "%s  I no longer understand this SURVEY.GOAL code: >%d<\n", ERROR_PREFIX, pSurvey->goal_code);
//     break;
//     /////////////////////////////////////////////////////////////////////////////////
//   case SURVEY_GOAL_EXPOSURE :
//     //we must complete the designated exposure time per tile
//     //we will complete as much as possible of the survey in the time permitted
//     if (survey_calc_times_with_goal_exposure((survey_struct*) pSurvey))
//     {
//       return 1;
//     }
// 
//    
//     //we should now go through the list of fields and choose which ones will be used
//     if (survey_choose_tiles_with_goal_exposure ((survey_struct*) pSurvey, (fieldlist_struct*) pFieldList, (telescope_struct*) pTele))
//     {
//       return 1;
//     }
//     
//     break;
//     /////////////////////////////////////////////////////////////////////////////////
// 
//   default:
//     fprintf(stderr, "%s  I do not understand this SURVEY.GOAL code: >%d<\n", ERROR_PREFIX, pSurvey->goal_code);
//     return 1;
//     break;
//     /////////////////////////////////////////////////////////////////////////////////
//   }

//these are calculated earlier  //Now only support the SURVEY_GOAL_EXPOSURE method
//these are calculated earlier  //we must complete the designated exposure time per tile
//these are calculated earlier  //we will complete as much as possible of the survey in the time permitted
//these are calculated earlier  if (survey_calc_times_with_goal_exposure((survey_struct*) pSurvey))
//these are calculated earlier  {
//these are calculated earlier    return 1;
//these are calculated earlier  }


  //we should now go through the list of fields and choose which ones will be used
  if (survey_choose_tiles_with_goal_exposure ((survey_struct*) pSurvey, 
                                              (fieldlist_struct*) pFieldList,
                                              (telescope_struct*) pTele))
  {
    return 1;
  }

//  //we should now go through the list of fields and choose which ones will be used
//  if (survey_choose_tiles_using_visibility ((survey_struct*) pSurvey, 
//                                            (fieldlist_struct*) pFieldList,
//                                            (telescope_struct*) pTele))
//  {
//    return 1;
//  }

  
  
  pSurvey->moon_split_by_exposure_texp_bright_threshold =
    pSurvey->pmoon_bb->Minutes_net_per_field * pSurvey->moon_split_exp_tolerance;
  //    pSurvey->moon_split_by_exposure_texp_bright_threshold = pSurvey->pmoon_bb->Minutes_net_per_field *TOLERANCE_TEXP_FACTOR;

  pSurvey->num_tiles_todo_total = 0;
  for(m=0; m<pSurvey->num_moon_groups; m++) pSurvey->num_tiles_todo_total += pSurvey->moon[m].num_tiles_todo;
  fprintf(stdout, "%s The final survey plan is for %8d %-8s tiles (including overallocation factors)\n",
          COMMENT_PREFIX, pSurvey->num_tiles_todo_total, "total");
  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    fprintf(stdout, "%s The final survey plan is for %8d %-8s tiles (including %g overallocation factor)\n",
            COMMENT_PREFIX, pSurvey->moon[m].num_tiles_todo, pSurvey->moon[m].longname, pSurvey->moon[m].overallocation_factor);
  }


  return 0;
}
///////////////////////////////////////////

///////////////////////////////////////////
int survey_calc_total_time_available(survey_struct *pSurvey, telescope_struct *pTele)
{
  int m;
  if ( pSurvey == NULL ||
       pTele   == NULL ) return 1;
  pSurvey->Hours_gross_total =
    pSurvey->duration                                                                      //this is in years
    * (YEARS_TO_DAYS - pSurvey->overheads_fault_nights_per_year
       - pSurvey->overheads_maintenance_nights_per_year)                                   //this is in nights per year
    * (1.0         - pTele->environ.cloud_fraction_closed)                                 //this is fractional
    * pSurvey->mean_dark_time_hours_per_night;
  
  fprintf(stdout, "%s Total estimated gross time available is (%-8s): %9.2f hours\n",
          COMMENT_PREFIX, "Total", pSurvey->Hours_gross_total);
  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    pSurvey->moon[m].Hours_gross = pSurvey->Hours_gross_total * pSurvey->moon[m].fraction;
    fprintf(stdout, "%s Total estimated gross time available is (%-8s): %9.2f hours\n",
            COMMENT_PREFIX, pSurvey->moon[m].longname, pSurvey->moon[m].Hours_gross);
  }
  return 0;
}

////we must complete the designated number of tiles, so we can now calculate the implied exposure time
//int survey_calc_times_with_goal_area (survey_struct *pSurvey)
//{
//  int m;
//  if ( pSurvey == NULL ) return 1;
//  for (m=0;m<pSurvey->num_moon_groups; m++)
//  {
//    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
//    pMoon->num_tiles_poss  = (int) (pMoon->num_tiles_req);
//    if ( pMoon->num_tiles_poss <= 0 ) //this means that there are no tiles for this moon phase
//    {
//      pMoon->Minutes_gross_per_tile = (float) -1.0;  
//    }
//    else
//    {
//      pMoon->Minutes_gross_per_tile = pMoon->Hours_gross *HOURS_TO_MINS /((float) pMoon->num_tiles_poss);
//
//      //we count one slew for each pass - recoup some time later
//      //we assume that re-positioning and readout can run concurrently
//      if (pMoon->num_tiles_per_field > 0 &&
//          pMoon->num_tiles_per_pass  > 0 )
//      {
//        float average_num_slews_per_tile;
//        if ( pSurvey->tiling_num_dithers > 1 )//we will need to move+settle between every tile 
//          average_num_slews_per_tile = 1.0; 
//        else                         //we will need to move+settle between every pass 
//          average_num_slews_per_tile = ceil((float) pMoon->num_tiles_per_field / (float) pMoon->num_tiles_per_pass) / (float) pMoon->num_tiles_per_field;
//
//        pMoon->Minutes_net_per_tile =
//          pMoon->Minutes_gross_per_tile
//          - pSurvey->overheads_calibration_mins_per_tile
//          - MAX(pSurvey->overheads_positioning_mins_per_tile,pSurvey->overheads_readout_mins_per_tile)
//          - ((pSurvey->overheads_settling_mins_per_slew +
//              pSurvey->overheads_guide_star_acquisition_mins)* average_num_slews_per_tile);
//      }
//      else
//      {
//        //this should not happen
//        return 1;
//      }
//    }
//    //the following is only valid for the baseline survey...
//    //But I think it is only used for excluding objects from the bright survey....
//    pMoon->Minutes_net_per_field = pMoon->Minutes_net_per_tile * pMoon->num_tiles_per_field;
//  } 
//  return 0;
//}

int survey_calc_tile_overheads (survey_struct *pSurvey)
{
  int m;
  
  if ( pSurvey == NULL ) return 1;
  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    int i;
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    float overhead_per_field = 0.0;
    int effective_tiles_per_field = MAX(1,MAX(pMoon->num_tiles_per_field,pMoon->num_tiles_per_pass));
    pSurvey->overhead_per_readout_reposition_calib_mins =
      MAX(pSurvey->overheads_readout_mins_per_tile,
          pSurvey->overheads_positioning_mins_per_tile)
      + pSurvey->overheads_calibration_mins_per_tile;
    
    //calc overheads for first tile in OB (ie 1st tile in each pass) 
    pMoon->average_overhead_per_first_tile_mins =
      MAX(pSurvey->overhead_per_readout_reposition_calib_mins,
          pSurvey->overheads_average_time_per_slew);
    
    //calc overheads for second, third ...tiles in OB  
    pMoon->average_overhead_per_subsequent_tile_mins =
      MAX(pSurvey->overhead_per_readout_reposition_calib_mins,
          (pSurvey->tiling_num_dithers > 1 ? pSurvey->overheads_mins_per_dither : 0.0));
    
    //    for(i=0;i<pMoon->num_tiles_per_field;i++)
    for(i=0;i<effective_tiles_per_field;i++)
    {
      if ( i % pMoon->num_tiles_per_pass == 0 ) overhead_per_field += pMoon->average_overhead_per_first_tile_mins;
      else                                      overhead_per_field += pMoon->average_overhead_per_subsequent_tile_mins;
    }
    pMoon->average_overhead_per_tile = overhead_per_field/((float) effective_tiles_per_field);
    //    pMoon->average_overhead_per_tile = overhead_per_field/((float) MAX(pMoon->num_tiles_per_pass,pMoon->num_tiles_per_field));
    
    //    pMoon->Minutes_net_per_field = pMoon->Minutes_net_per_tile * pMoon->num_tiles_per_field;
    pMoon->Minutes_net_per_field = pMoon->Minutes_net_per_tile * effective_tiles_per_field;
    pMoon->Minutes_gross_per_tile =
      pMoon->Minutes_net_per_tile + pMoon->average_overhead_per_tile;

    pMoon->minimum_gross_mins_per_partial_tile =  pMoon->average_overhead_per_first_tile_mins + MINIMUM_USEFUL_EXPOSURE_MINS;

    fprintf(stdout, "%s Gross time per tile for moon phase %8s is: %8.3f mins\n",
            COMMENT_PREFIX, pMoon->longname, pMoon->Minutes_gross_per_tile);
  }

  return 0;
}


int survey_calc_tiles_available (survey_struct *pSurvey)
{
  int m;
  if ( pSurvey == NULL ) return 1;

  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    if ( pMoon->Minutes_gross_per_tile > 0.0 )
      pMoon->num_tiles_poss = (int) floor(pMoon->Hours_gross*HOURS_TO_MINS / pMoon->Minutes_gross_per_tile) ;  
    else
      pMoon->num_tiles_poss = 0;

    if ( pMoon->num_tiles_poss < pMoon->num_tiles_req )
    {
      fprintf(stdout, "%s For moon phase %8s number of requested tiles %6d > number of possible tiles %6d (deficit= %6d )\n",
              WARNING_PREFIX,
              pMoon->longname,
              pMoon->num_tiles_req,
              pMoon->num_tiles_poss,
              pMoon->num_tiles_poss - pMoon->num_tiles_req );
    }
    if ( pMoon->num_tiles_poss > pMoon->num_tiles_req )
    {
      fprintf(stdout, "%s For moon phase %8s number of requested tiles %6d < number of possible tiles %6d (surplus= %6d )\n",
              WARNING_PREFIX,
              pMoon->longname,
              pMoon->num_tiles_req,
              pMoon->num_tiles_poss,
              pMoon->num_tiles_poss - pMoon->num_tiles_req );
    }
  }


  pSurvey->num_tiles_poss_total = 0;
  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    pSurvey->num_tiles_poss_total += pMoon->num_tiles_poss;
    fprintf(stdout, "%s On-sky time is available for up to %8d %8s tiles with (net exposure): %9.2f mins per tile\n",
            COMMENT_PREFIX,  pMoon->num_tiles_poss,  pMoon->longname,
            pMoon->Minutes_net_per_tile);
  }
  fprintf(stdout,   "%s We have time to carry out up to    %8d total    tiles\n",
          COMMENT_PREFIX, pSurvey->num_tiles_poss_total);
  //  fprintf(stdout,   "%s We will carry out observations in  %8d unique   fields\n",
  //          COMMENT_PREFIX, pSurvey->iNum_field_used_in_any_survey);
  ///////////////////////////////////////////////
  
  
  return 0;
}

/////////////////////////////////////////////////////////////
int survey_choose_tiles_using_visibility (survey_struct *pSurvey, fieldlist_struct *pFieldList, telescope_struct *pTele)
{
  int m,j;
  float *pSortdata = NULL;
  int   *pSortindex = NULL;

  if ( pSurvey    == NULL ||
       pFieldList == NULL ||
       pTele      == NULL) return 1;
  fprintf(stdout, "%s We will now calculate which fields should be observed, and with how many tiles\n", COMMENT_PREFIX);
  fprintf(stdout, "%s Selecting fields/tiles on basis of visibility\n",  COMMENT_PREFIX);


  //allocate some space to place the sorting arrays.
  if (pFieldList->nFields > 0 )
  {
    if ((pSortindex = (int*)   malloc(sizeof(int)   * pFieldList->nFields)) == NULL ||
        (pSortdata  = (float*) malloc(sizeof(float) * pFieldList->nFields)) == NULL  )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  for(m=0; m<pSurvey->num_moon_groups; m++)
  {
    int num_valid = 0;
    int num_tiles_remaining;
    //initialise the ntiles_todo fields
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    pMoon->num_tiles_todo = 0;
    for ( j = 0; j < pFieldList->nFields; j++)
    {
      field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
      pF->ntiles_todo[pMoon->group_code] = 0;
    }

    for ( j = 0; j < pFieldList->nFields; j++)
    {
      field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
      //      if ( pF->in_survey & pMoon->mask &&    pF->ntiles_req[pMoon->group_code] > 0 )
      if ( pF->in_survey & SURVEY_ANY )   // include all fields at this point
      {
        pSortindex[num_valid] = j;
        pSortdata[num_valid] = -1.0 * pF->ntiles_visible_predict_weighted[pMoon->group_code];
        num_valid++;  //this is a valid field
      }
    }

    //sort the selected fields
    util_fisort((ulong) num_valid,  (float*) pSortdata, (int*) pSortindex);

    //we should over-allocate by the specified amount
      //this is a local counter of how many tiles we have remaining and available to hand out to fields
    num_tiles_remaining = (int) (pMoon->num_tiles_poss * pMoon->overallocation_factor);

    //now assign the available tiles to the valid fields in order
    for ( j = 0; j < num_valid; j++)
    {
      field_struct *pF = (field_struct*) &(pFieldList->pF[pSortindex[j]]);
      //only consider fields that are in this moon phase and have >0 requested tiles
      if ( pF->in_survey & pMoon->mask       &&         
           pF->ntiles_req[pMoon->group_code] > 0 )
      {
        if (num_tiles_remaining> 0 &&
            pF->ntiles_todo[pMoon->group_code] < pF->ntiles_req[pMoon->group_code] )
        {
          int n;
          //use up as many tiles as possible to the requested number 
          n = MIN(num_tiles_remaining,pF->ntiles_req[pMoon->group_code] - pF->ntiles_todo[pMoon->group_code]);
          n = MIN(n, MAX_TILES_PER_FIELD);  //keep things under control
          n = MAX(0,n);                     //
          pF->ntiles_todo[pMoon->group_code] += n;
          pMoon->num_tiles_todo += n;
          num_tiles_remaining -= n;
        }
      }
    }

    if ( num_tiles_remaining > 0 )  
    {
      if ( pSurvey->use_up_spare_tiles == TRUE )
      {
        //TODO -- assign spare tikles to fields ... see below

      }
    }
  }

  //free the arrays used in this function
  if ( pSortindex) free (pSortindex);  pSortindex = NULL;
  if ( pSortdata)  free (pSortdata);   pSortdata  = NULL;

  ///////////////////////////All this is untested!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //make total todo-exposure times for each field
  //and flag fields which are out of the survey area because we do not have enough survey time for them
  pSurvey->iNum_field_used_in_any_survey = 0;
  for ( j = 0; j < pFieldList->nFields; j++)
  {
    int m;
    field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
    BOOL flag = FALSE; 
    pF->texp_total_todo = 0.0;
    for(m=0; m<pSurvey->num_moon_groups; m++)
    {
      moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
      if ( pF->in_survey & pMoon->mask        &&
           pF->ntiles_todo[pMoon->group_code] > 0 )
      {
        flag = TRUE; 
        pF->texp_total_todo += (float) pF->ntiles_todo[pMoon->group_code] * pMoon->Minutes_net_per_tile;
      }
      //      else
      //      {
      //        pF->ntiles_todo[pMoon->group_code] = 0;
      //        pF->in_survey = (unsigned int) (pF->in_survey & ~(pMoon->mask));
      //      }
    }
    if ( flag ) pSurvey->iNum_field_used_in_any_survey ++;
  }
  ///////////////////////////All this is untested!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  //now reassess the ra_dec bounds of the survey to be executed
  //TOD check if this is necessary
  if ( fieldlist_calc_survey_bounds ((fieldlist_struct*) pFieldList, (survey_struct*) pSurvey))
  {
    return 1; //bad news
  }

  return 0;
}
/////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
int survey_choose_tiles_with_goal_exposure (survey_struct *pSurvey, fieldlist_struct *pFieldList, telescope_struct *pTele)
{
  int j,m;
  int ra_bin;
  //  int ra_bin_temp;  //DEBUG
  float *pSortdata = NULL;
  int   *pSortindex = NULL;

  FILE *pFile = NULL;
  char str_filename[STR_MAX];

  float num_fields_per_RA_bin[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
  float num_fields_per_RA_bin_smooth[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
  float ntiles_req_per_RA_bin[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
  float ntiles_req_per_RA_bin_smooth[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];

  
  if ( pSurvey    == NULL ||
       pFieldList == NULL ||
       pTele      == NULL) return 1;
  //we should now go through the list of fields and choose which ones will be used
  //need to decide on what order to prioritise fields
  //baseline is different for DarkGrey and Bright surveys
  //  - start at telescope latitude and work North+South in DarkGrey time,  
  //  - start on the plane and work outwards in Bright time  
  fprintf(stdout, "%s We will now calculate which fields should be observed, and with how many tiles\n", COMMENT_PREFIX);

  //  fprintf(stdout, "%s Selecting %8s fields on basis of |Dec-Lat|\n",  COMMENT_PREFIX, "DarkGrey");
  //  fprintf(stdout, "%s Selecting %8s fields on basis of |b|\n",        COMMENT_PREFIX, "Bright");

  for(m=0; m<pSurvey->num_moon_groups; m++)
  {
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    fprintf(stdout, "%s Selecting %8s fields on basis of %s (%s)\n",
            COMMENT_PREFIX, pMoon->longname,
            TILE_PLANNING_METHO_STRING(pMoon->tile_planning_method),
            pMoon->tile_planning_sign>1?"ascending":"descending");
  }
  

  
  snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, TILING_STATS_VS_RA_FILENAME);
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening file : %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  fprintf(stdout, "%s Writing tiling stats to file: %s\n", COMMENT_PREFIX, str_filename);

  //write a full header
  {
    time_t rawtime;
    struct tm *timeinfo;
    
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, "#This is the tiling stats vs RA logfile\n\
#Calculated as part of the tiling planning process whereby tiles are allocated to fields,\n\
#and does not necessarily indicate the actual tiles observed on each field in the simulated survey\n\
#The smoothing boxcar has full width %g deg\n\
#This log was first written at: %s\n\
#Original filename: %s\n\
#This logfile contains one entry per bin in RA\n\
#Description of columns:\n\
#Index  Name        Format    Description\n\
#-----  ----------- --------- ------------------------------------------------------------------------ \n\
#1      RABIN       integer   Index of this RA bin\n\
#2      RA_MID      float     RA at the middle of this bin (deg,J2000)\n\
#3      RA_MIN      float     RA at the lower limit of this bin (deg,J2000)\n\
#4      RA_MAX      float     RA at the upper limit of this bin (deg,J2000)\n\
#5      NavailBB    float     Estimated number of tiles available in this bin (Bright moon)\n\
#6      NavailDG    float     Estimated number of tiles available in this bin (Dark/Grey moon)\n\
#7      smNavailBB  float     Estimated number of tiles available in this bin - boxcar smoothed (Bright moon)\n\
#8      smNavailDG  float     Estimated number of tiles available in this bin - boxcar smoothed (Dark/Grey moon)\n\
#9      NbudgetBB   integer   Number of tiles that were budgeted for allocation in this bin (Bright moon)\n\
#10     NbudgetDG   integer   Number of tiles that were budgeted for allocation in this bin (Dark/Grey moon)\n\
#11     NremainBB   integer   Number of surplus tiles that were not allocated in this bin (Bright moon)\n\
#12     NremainDG   integer   Number of surplus tiles that were not allocated in this bin (Dark/Grey moon)\n\
#13     NfieldBB    float     Number of fields that lie in this bin (in Bright moon mask)\n\
#14     NfieldDG    float     Number of fields that lie in this bin (in Dark/Grey moon mask)\n\
#15     smNfieldBB  float     Number of fields that lie in this bin - boxcar smoothed (in Bright moon mask)\n\
#16     smNfieldDG  float     Number of fields that lie in this bin - boxcar smoothed (in Dark/Grey moon mask)\n\
#17     NreqBB      float     Number of tiles requested in this bin (Bright moon)\n\
#18     NreqDG      float     Number of tiles requested in this bin (Dark/Grey moon)\n\
#19     smNreqBB    float     Number of tiles requested in this bin - boxcar smoothed (Bright moon)\n\
#20     smNreqDG    float     Number of tiles requested in this bin - boxcar smoothed (Dark/Grey moon)\n",
            (float) TILING_RA_BIN_WIDTH*TILING_RA_BIN_SMOOTH_FACTOR,
            asctime(timeinfo),
            str_filename);
  }
  
  //write a one line column name key
  fprintf(pFile, "#%-5s %8s %8s %8s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
          "RABIN", "RA_MID", "RA_MIN1", "RA_MAX1", 
          "NavailBB",   "NavailDG",
          "smNavailBB", "smNavailDG",
          "NbudgetBB",  "NbudgetDG",
          "NremainBB",  "NremainDG",
          "NfieldBB",   "NfieldDG",
          "smNfieldBB", "smNfieldDG",
          "NreqBB",     "NreqDG",
          "smNreqBB",   "smNreqDG");

  //allocate some space to place the sorting arrays.
  if (pFieldList->nFields > 0)
  {
    if ((pSortindex = (int*)   malloc(sizeof(int)   * pFieldList->nFields)) == NULL ||
        (pSortdata  = (float*) malloc(sizeof(float) * pFieldList->nFields)) == NULL  )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  //initialise the ntiles_todo fields
  {
    int m;
    for(m=0; m<pSurvey->num_moon_groups; m++)
    {
      moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
      pMoon->num_tiles_todo = 0;
      for ( j = 0; j < pFieldList->nFields; j++)
      {
        field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
        pF->ntiles_todo[pMoon->group_code] = 0;
      }
    }
  }

  //count the number of fields in each ra bin
  for(m=0; m<pSurvey->num_moon_groups; m++)
  {
    int ra_bin1, ra_bin2;
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    for(ra_bin=0; ra_bin<TILING_NRA_BINS; ra_bin++)
    {
      num_fields_per_RA_bin[ra_bin][pMoon->group_code]        = (float) 0.0;
      num_fields_per_RA_bin_smooth[ra_bin][pMoon->group_code] = (float) 0.0;
      ntiles_req_per_RA_bin[ra_bin][pMoon->group_code]        = (float) 0.0;
      ntiles_req_per_RA_bin_smooth[ra_bin][pMoon->group_code] = (float) 0.0;
    }

    for ( j = 0; j < pFieldList->nFields; j++)
    {
      field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
      int ra_bin =  (int) floor(pF->ra0 / TILING_RA_BIN_WIDTH);
      if ( ra_bin >= 0 && ra_bin < TILING_NRA_BINS )
      {
        if ( pF->in_survey & pMoon->mask  &&           //if the field lies in this part of the survey
             pF->ntiles_req[pMoon->group_code] > 0 &&  //if some tiles have been requested for this field
             pF->n_object > 0  )                       //if the field contains at least some targets
        {
          num_fields_per_RA_bin[ra_bin][pMoon->group_code] += (float) 1.0;
          ntiles_req_per_RA_bin[ra_bin][pMoon->group_code] += (float) pF->ntiles_req[pMoon->group_code];
        }
      }
      else return 1;  //something is going badly wrong if this turns up
    }
    //now smooth the result
    for ( ra_bin1=0; ra_bin1 < TILING_NRA_BINS; ra_bin1 ++ )
    {
      for ( ra_bin2=ra_bin1-TILING_RA_BIN_SMOOTH_FACTOR/2; ra_bin2 <= ra_bin1+TILING_RA_BIN_SMOOTH_FACTOR/2; ra_bin2 ++ )
      {
        int ra_bin2a = (ra_bin2 < 0 ? ra_bin2 + TILING_NRA_BINS : (ra_bin2 >= TILING_NRA_BINS ?  ra_bin2 - TILING_NRA_BINS : ra_bin2));
        num_fields_per_RA_bin_smooth[ra_bin1][pMoon->group_code] += (float)
          num_fields_per_RA_bin[ra_bin2a][pMoon->group_code] / (float) TILING_RA_BIN_SMOOTH_FACTOR;
        ntiles_req_per_RA_bin_smooth[ra_bin1][pMoon->group_code] += (float)
          ntiles_req_per_RA_bin[ra_bin2a][pMoon->group_code] / (float) TILING_RA_BIN_SMOOTH_FACTOR;
      }
    }
  }


  //loop over the RA bins
  for(ra_bin=0; ra_bin<TILING_NRA_BINS; ra_bin++)
  {
    int m;
    int num_tiles_initially[MAX_NUM_MOON_GROUPS];
    int num_tiles_remaining[MAX_NUM_MOON_GROUPS];
    double ra_mid  = (double) (ra_bin + 0.5) * (double) TILING_RA_BIN_WIDTH;
    double ra_min1 = (double) (ra_bin    ) * (double) TILING_RA_BIN_WIDTH;
    double ra_max1 = (double) (ra_bin + 1) * (double) TILING_RA_BIN_WIDTH;

    //now loop around the moon phases
    for(m=0; m<pSurvey->num_moon_groups; m++)
    {
      int j,num_valid = 0;
      moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
      float tiling_sign = (float) pMoon->tile_planning_sign;  
      //we should over-allocate by the specified amount
      //and rescale by the relative number of fields/requested tiles in this ra_bin compared to local bins
      num_tiles_initially[pMoon->group_code] =
        (int) ceil(pSurvey->num_tiles_available_per_RA_bin_smooth[ra_bin][pMoon->group_code] 
                   * pMoon->overallocation_factor
                   //                   * num_fields_per_RA_bin[ra_bin][pMoon->group_code]
                   //                   / MAX(1.0,num_fields_per_RA_bin_smooth[ra_bin][pMoon->group_code]));
                   * ntiles_req_per_RA_bin[ra_bin][pMoon->group_code]
                   / MAX(1.0,ntiles_req_per_RA_bin_smooth[ra_bin][pMoon->group_code]));

      //this is a local counter of how many tiles we have remaining and available to hand out to fields
      num_tiles_remaining[pMoon->group_code] = num_tiles_initially[pMoon->group_code];
      //      num_tiles_requested[pMoon->group_code] = 0;
      //      num_fields[pMoon->group_code] = 0;

      for ( j = 0; j < pFieldList->nFields; j++)
      {
        field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
        pSortindex[num_valid] = j;
        if ( (pF->in_survey & SURVEY_ANY) &&
             pF->n_object > 0 )
        {
          int ra_bin_test =  (int) floor(pF->ra0 / TILING_RA_BIN_WIDTH);
          if ( ra_bin_test == ra_bin ) 
          {

            switch (pMoon->tile_planning_method)
            {
            case TILE_PLANNING_METHOD_ALL : 
              pSortdata[num_valid] = (float) 1.0;   //no need to sort
              break;
            case TILE_PLANNING_METHOD_DEC : 
              pSortdata[num_valid] = (float) tiling_sign * pF->dec0;
              break;
            case TILE_PLANNING_METHOD_GAL_LAT : 
              pSortdata[num_valid] = (float) tiling_sign * fabs(pF->b0);
              break;
            case TILE_PLANNING_METHOD_MIN_AIRMASS : 
              pSortdata[num_valid] = (float) tiling_sign * pF->airmass_min;
              break;
            case TILE_PLANNING_METHOD_VISIBILITY : 
              pSortdata[num_valid] = (float) tiling_sign * pF->ntiles_visible_predict[pMoon->group_code];
              break;
            case TILE_PLANNING_METHOD_SUM_FIBERHOURS : 
              pSortdata[num_valid] = (float) tiling_sign * (pF->object_dark_fhrs_lores + pF->object_dark_fhrs_hires);
              break;
            case TILE_PLANNING_METHOD_SUM_PRIORITY : 
              pSortdata[num_valid] = (float) tiling_sign * (pF->object_priority_total);
              break;
            case TILE_PLANNING_METHOD_SUM_PRIORITY_WEIGHTED : 
              pSortdata[num_valid] = (float) tiling_sign * (pF->object_priority_total_weighted);
              break;
            case TILE_PLANNING_METHOD_RANDOM : 
              pSortdata[num_valid] = (float) drand48();
              break;
            case TILE_PLANNING_METHOD_UNKNOWN : 
              return 1;  //
              break;
            default:
              return 1;
              break;
            }
          
            num_valid++;  //this is a valid field
          }
        }
      }
      //sort the selected fields
      util_fisort((ulong) num_valid,  (float*) pSortdata, (int*) pSortindex);


      //now assign the available tiles to the valid fields in order
      for ( j = 0; j < num_valid; j++)
      {
        field_struct *pF = (field_struct*) &(pFieldList->pF[pSortindex[j]]);
        if ( (pF->in_survey & pMoon->mask)         &&
             pF->ntiles_req[pMoon->group_code] > 0 &&
             pF->n_object > 0 )
        {

          if ((num_tiles_remaining[pMoon->group_code] > 0 &&
               pF->ntiles_todo[pMoon->group_code] < pF->ntiles_req[pMoon->group_code]) ||
              pMoon->tile_planning_method == TILE_PLANNING_METHOD_ALL )
          {
            int n;
            //use up as many tiles as possible to the requested number 
            if ( pMoon->tile_planning_method == TILE_PLANNING_METHOD_ALL )
            {
              n = pF->ntiles_req[pMoon->group_code] - pF->ntiles_todo[pMoon->group_code];
            }
            else
            {
              n = MIN(num_tiles_remaining[pMoon->group_code],
                      pF->ntiles_req[pMoon->group_code] - pF->ntiles_todo[pMoon->group_code]);
            }
            n = CLIP(n,0,MAX_TILES_PER_FIELD);  //keep things within limits
            pF->ntiles_todo[pMoon->group_code] += n;
            pMoon->num_tiles_todo += n;
            num_tiles_remaining[pMoon->group_code] -= n;
          }
        }
      }

      
      //great, we still have some spare tiles in this airmass bin! So we will hand these out to the existing fields
      //we will do this in proportion to the number of tiles that they already have received
      if ( num_tiles_remaining[pMoon->group_code] > 0 &&
           pMoon->tile_planning_method != TILE_PLANNING_METHOD_ALL)  
      {
        if ( pSurvey->use_up_spare_tiles == TRUE )
        {
          float frac_remaining = (float) num_tiles_remaining[pMoon->group_code] / (float) MAX(pMoon->num_tiles_todo,1);
          //          fprintf(stdout, "%s We have (%6d/%6d) spare tiles in %8s time in range [%3g<=RA<%3g deg] - assigning them to Fields\n",
          //                  COMMENT_PREFIX, num_tiles_remaining[m], num_tiles_initially[m], pMoon->longname,  ra_min, ra_max);
          for ( j = 0; j < num_valid; j++)
            //          for ( j = 0; j < pFieldList->nFields; j++)
          {
            field_struct* pF = (field_struct*) &(pFieldList->pF[pSortindex[j]]);
            if ( (pF->in_survey & pMoon->mask) &&
                 pF->ntiles_todo[pMoon->group_code] > 0 &&
                 pF->n_object > 0 )
            {
              if (num_tiles_remaining[pMoon->group_code] > 0 )
              {
                int n;
                n = (int) ceil(frac_remaining * pF->ntiles_todo[pMoon->group_code]);
                n = MIN(num_tiles_remaining[m],n);
                n = MIN(n, MAX_TILES_PER_FIELD - pF->ntiles_todo[pMoon->group_code]);
                pF->ntiles_todo[pMoon->group_code] += n;
                pMoon->num_tiles_todo += n;
                num_tiles_remaining[pMoon->group_code] -= n;
              }
              else
              {
                //We have finally run out of tiles!
                break;
              }
            }
          }
        }
        if ( num_tiles_remaining[pMoon->group_code] > 0 )  
        {
          //          fprintf(stdout, "%s We STILL have (%6d/%6d) spare tiles in %8s time in range [%3g<=RA<%3g deg]- these will be wasted\n",
          //                  WARNING_PREFIX, num_tiles_remaining[m], num_tiles_initially[m], pMoon->longname, ra_min, ra_max );
        }
      }
    }

  
    //report the assignment results to the log file
    fprintf(pFile, "%6d %8g %8g %8g %10.1f %10.1f %10.1f %10.1f %10d %10d %10d %10d %10.1f %10.1f %10.1f %10.1f %10.1f %10.1f %10.1f %10.1f\n",
            (int)   ra_bin,
            (float) ra_mid,
            (float) ra_min1,
            (float) ra_max1, //ra_min2, ra_max2,
            (float) pSurvey->num_tiles_available_per_RA_bin[ra_bin][MOON_GROUP_BB],
            (float) pSurvey->num_tiles_available_per_RA_bin[ra_bin][MOON_GROUP_DG],
            (float) pSurvey->num_tiles_available_per_RA_bin_smooth[ra_bin][MOON_GROUP_BB],
            (float) pSurvey->num_tiles_available_per_RA_bin_smooth[ra_bin][MOON_GROUP_DG],
            (int)   num_tiles_initially[MOON_GROUP_BB],
            (int)   num_tiles_initially[MOON_GROUP_DG],
            (int)   num_tiles_remaining[MOON_GROUP_BB],
            (int)   num_tiles_remaining[MOON_GROUP_DG],
            (float) num_fields_per_RA_bin[ra_bin][MOON_GROUP_BB],
            (float) num_fields_per_RA_bin[ra_bin][MOON_GROUP_DG],
            (float) num_fields_per_RA_bin_smooth[ra_bin][MOON_GROUP_BB],
            (float) num_fields_per_RA_bin_smooth[ra_bin][MOON_GROUP_DG],
            (float) ntiles_req_per_RA_bin[ra_bin][MOON_GROUP_BB],
            (float) ntiles_req_per_RA_bin[ra_bin][MOON_GROUP_DG],
            (float) ntiles_req_per_RA_bin_smooth[ra_bin][MOON_GROUP_BB],
            (float) ntiles_req_per_RA_bin_smooth[ra_bin][MOON_GROUP_DG]);

  }
  if ( pFile ) fclose(pFile); pFile = NULL;

  //free the arrays used in this function
  if ( pSortindex) free (pSortindex);  pSortindex = NULL;
  if ( pSortdata)  free (pSortdata);   pSortdata  = NULL;
  
  ///////////////////////////All this is untested!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //make total todo-exposure times for each field
  //and flag fields which are out of the survey area because we do not have enough survey time for them
  pSurvey->iNum_field_used_in_any_survey = 0;
  for ( j = 0; j < pFieldList->nFields; j++)
  {
    int m;
    field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
    BOOL flag = FALSE; 
    pF->texp_total_todo = 0.0;
    for(m=0; m<pSurvey->num_moon_groups; m++)
    {
      moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
      if ( pF->in_survey & pMoon->mask &&
           pF->ntiles_todo[pMoon->group_code] > 0 )
      {
        flag = TRUE; 
        pF->texp_total_todo +=  (float) pF->ntiles_todo[pMoon->group_code] * pMoon->Minutes_net_per_tile;
      }
//      else
//      {
//        pF->ntiles_todo[pMoon->group_code] = 0;
//        pF->in_survey = (unsigned int) (pF->in_survey & ~(pMoon->mask));
//      }
    }
    if ( flag ) pSurvey->iNum_field_used_in_any_survey ++;
  }
  ///////////////////////////All this is untested!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

//  ///////////////////////////All this is untested!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//  //now flag fields which are out of the survey area because we do not have enough survey time for them
//  for(m=0; m<pSurvey->num_moon_groups; m++)
//  {
//    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
//    for ( j = 0; j < pFieldList->nFields; j++)
//    {
//      field_struct *pF = (field_struct*) &(pFieldList->pF[j]);
//      if ( (pF->in_survey & pMoon->mask) &&  pF->ntiles_todo[pMoon->group_code] > 0 )
//      {
//        pSurvey->iNum_field_used_in_any_survey ++; //this will count twice for fields in darkgrey and bright regions
//      }
//      else
//      {
//        //So flag this field as living outside of the doable survey area
////        if ( pSurvey->main_mode == SURVEY_MAIN_MODE_POOLED )
////        {
////          pF->in_survey = (unsigned int) (pF->in_survey & ~(pMoon->mask));
////        }
//        pF->ntiles_todo[pMoon->group_code] = 0;
//        pF->in_survey = (unsigned int) (pF->in_survey & ~(pMoon->mask));
//      }
//      
////      if (!(pF->in_survey & SURVEY_ANY) )
////      {
////        //this field will no longer receive objects, so free up the pid array if was already allocated
////        if ( pF->pid ) free (pF->pid) ; pF->pid = NULL;
////        if ( pF->pobject_sub_index ) free (pF->pobject_sub_index) ; pF->pobject_sub_index = NULL;
////        pF->pid_data_length = 0;
////      }
//    }
//  }

//  //make total todo-exposure times for each field
//  {
//    int j;
//    for ( j = 0; j < pFieldList->nFields; j++)
//    {
//      field_struct* pF = (field_struct*) &(pFieldList->pF[j]);
//      pF->texp_total_todo = 0.0;
//      if ( pF->in_survey )
//      {
//        for(m=0; m<pSurvey->num_moon_groups; m++)
//        {
//          moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
//          pF->texp_total_todo += pF->ntiles_todo[pMoon->group_code] * pMoon->Minutes_net_per_tile;
//        }
//      }
//    }
//  }
    
  //now reassess the ra_dec bounds of the survey to be executed
  //update the survey boundaries
  if ( fieldlist_calc_survey_bounds ((fieldlist_struct*) pFieldList, (survey_struct*) pSurvey))
  {
    return 1; //bad news
  }
  return 0;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


int survey_determine_sky_tiling (survey_struct *pSurvey,
                                 fieldlist_struct* pFieldList,
                                 telescope_struct *pTele,
                                 focalplane_struct *pFocal)
{

  if ( pSurvey == NULL ||
       pFieldList == NULL ||
       pTele == NULL ||
       pFocal == NULL ) return 1;
  
  ///////////////////////////////////////////////
  /////we now compute the time available per tile/field etc
  if ( survey_calc_times_and_tiles ((survey_struct*) pSurvey, (fieldlist_struct*) pFieldList, (telescope_struct*) pTele))
  {
    //this probably indicates something very bad has happened
    fprintf(stderr, "%s There was a problem calculating survey times and tiles\n", ERROR_PREFIX); 
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////
  //make some statistics about numbers of Fields that will receive certain number of tiles
  if ( pSurvey->do_stats)
  {    
    FILE *pFile = NULL;
    char str_filename[STR_MAX];
    snprintf(str_filename, STR_MAX, "%s/tiles_per_field_stats_planned.txt", PLOTFILES_SUBDIRECTORY);
    if ((pFile = fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s  There were problems opening file : %s\n",
              ERROR_PREFIX, str_filename);
      return 1;
    }
    if ( calc_tiles_per_field_stats ((FILE*) pFile, (fieldlist_struct*) pFieldList, (survey_struct*) pSurvey ))
    {
      fprintf(stderr, "%s There was a problem calculating tiles per field statistics\n", ERROR_PREFIX); 
      return 1;
    }
    if ( pFile ) fclose(pFile); pFile = NULL;
  }
  ///////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////
  //we can now make plots of the tiling on the sky 
  //- first output the vertices of the fields to file
  if ( fieldlist_write_vertex_files ((fieldlist_struct*) pFieldList ))
  {
    //not a massive problem if this fails
    fprintf(stdout, "%s There was a problem witing field vertex files\n", WARNING_PREFIX); fflush(stdout);
  }
  ///////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////
  ///we can plot some nice diagrams of each of the sky tiling here
  //all outputs wil be in pdf format
  if ( pSurvey->do_plots )
  {
    if ( plot_sky_tiling ((survey_struct*) pSurvey,
                          (telescope_struct*) pTele,
                          (focalplane_struct*) pFocal,
                          (fieldlist_struct*) pFieldList,
                          (const char*) SKY_TILING_REQUESTED_STEM ) ||
         plot_sky_tiling ((survey_struct*) pSurvey,
                          (telescope_struct*) pTele,
                          (focalplane_struct*) pFocal,
                          (fieldlist_struct*) pFieldList,
                          (const char*) SKY_TILING_PLANNED_STEM ))
    {
      //not a big problem if this fails
      fprintf(stdout, "%s There was a problem making sky tiling plots\n", WARNING_PREFIX); fflush(stdout);
      
    }
  }
  ///////////////////////////////////////////////////////////////////////

  return 0;
}


////////////////////////////////////////////////////////////////

//print out unassigned objects - outside the footprint of the pointings that were read in,
//but only write those sources that fall inside the RA and DEC limits
int write_unassigned_objects_file (objectlist_struct *pObjList, cataloguelist_struct* pCatList, const char *str_filename )
{
  object_struct* pObj;
  int n_unassigned = 0;
  FILE* pUnassigned_file = NULL;
  int i;
  //these are the variables that are used when re-reading input files
  FILE* pInputfile = NULL;
  uint  inputfile_line_number = 0;
  utiny inputfile_cat_code = (utiny) CATALOGUE_CODE_NULL;

  if ( pObjList == NULL ||
       pCatList == NULL ) return 1;


  fprintf(stdout, "%s Writing out the unassigned_objects file ... \n", COMMENT_PREFIX);
  if ((pUnassigned_file = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the unassigned object file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  for(i=0;i<pObjList->nObjects;i++)
  {
    char info_buffer[STR_MAX];
    int c;
    pObj = (object_struct*) &(pObjList->pObject[i]);

    //need to find the correct line in the correct input catalogue file
    if (pObj->pCat->code != inputfile_cat_code)
    {
      if ( pInputfile ) fclose (pInputfile) ;
      pInputfile = NULL;

      //choose the correct catalogue from the list
      c = 0;
      while (pCatList->pCat[c].code != pObj->pCat->code &&
             c < pCatList->num_cats - 1) c++;

      if ( pCatList->pCat[c].code != pObj->pCat->code ) return 1;
      inputfile_cat_code = pCatList->pCat[c].code;
      
      //open the file
      if ((pInputfile = fopen(pCatList->pCat[c].filename, "r")) == NULL )
      {
        fprintf(stderr, "\n%s  There were problems opening the catalogue file: %s\n",
                ERROR_PREFIX, pCatList->pCat[c].filename);
        return 1;
      }
      fprintf(stdout, "%s   Re-reading target info from input catalogue file: %s\n",
              COMMENT_PREFIX, pCatList->pCat[c].filename);    fflush(stdout);

      inputfile_line_number = 0;
    }
    //advance to the correct line in the input file
    while (fgets(info_buffer,STR_MAX,pInputfile) &&
           ++inputfile_line_number < (uint) pObj->line_number );


    //      if ( pObj->flag == 0 )
    if ( pObj->num_fields == 0     )
    {
//      fprintf(pUnassigned_file, "%8d %3d %8d %9.5f %+9.5f %6.4f %7.1f %7.1f %7.1f %4d %5.2f\n",
//                (int)    pObj->id,
//                (int)    pObj->pCat->code,
//                (int)    pObj->line_number,
//                (double) pObj->ra,
//                (double) pObj->dec,
//                (float)  pObj->priority,
//                (float)  pObj->treq[MOON_PHASE_DARK],
//                (float)  pObj->treq[MOON_PHASE_GREY],
//                (float)  pObj->treq[MOON_PHASE_BRIGHT],
//                (int)    pObj->res,
//                (float)  pObj->mag);
      fprintf(pUnassigned_file, "%s", info_buffer);
      n_unassigned ++;
    }
  }
  if ( pInputfile ) fclose (pInputfile) ;
  pInputfile = NULL;

  if (pUnassigned_file) fclose(pUnassigned_file);
  pUnassigned_file = NULL;
  fprintf(stdout, "%s I wrote out %9d objects to unnassigned object file %s\n",
          COMMENT_PREFIX, n_unassigned, str_filename);
  return 0;
}

//write one line for an object
int write_object_summary_header (FILE* pFile, const char *str_filename, catalogue_struct *pCat ) // const char *str_catname)
{
  time_t rawtime;
  struct tm *timeinfo;
  if ( pFile == NULL ) return 1;
  //write the header
  //full description first
  if (time((time_t*) &rawtime) < 0 ) return 1;
  timeinfo = localtime(&rawtime);

  fprintf(pFile, "#This is the main output target catalogue from the 4FS OpSim\n\
#This is the output catalogue for: %s\n\
#This file was written at: %s\
#Original filename: %s\n\
#This catalogue contains one entry per valid input target\n\
#Targets are in same order as original input catalogue\n\
#Description of columns:\n\
#Index  Name                     Format Description \n\
#-----  ------------------------ ------ ---------------------------------------------------------------- \n\
#  1    Cat                      int    Catalogue code number\n\
#  2    LineNum                  int    Line number of this target in original catalogue file\n\
#  3    IntPri                   float  Internal (adjusted) priority of this target\n\
#  4    Nfie                     int    Number of 4MOST Fields in which this target lies\n\
#  5    Ntil                     int    Number of 4MOST Tiles in which this target was observed\n\
#  6    Nfib                     int    Number of fibers of correct resolution which could 'see' this target\n\
#  7    Texp_D                   float  Net exposure time delivered in Dark moon conditions (mins)\n\
#  8    Texp_G                   float  Net exposure time delivered in Grey moon conditions (mins)\n\
#  9    Texp_B                   float  Net exposure time delivered in Bright moon conditions (mins)\n\
# 10    FrDone                   float  Fraction of the requested/required exposure time that was delivered\n\
#  -    -                        -      The following are copies of the input parameters for each target\n\
# 11    InputObjectName          string User-supplied name\n\
# 12    ObjectID                 int    User-supplied ID number\n\
# 13    ObjectRA                 double RA(deg,J2000)\n\
# 14    ObjectDec                double Dec(deg,J2000)\n\
# 15    Pri                      int    Original Priority (1..100)\n",
          pCat->codename,
          asctime(timeinfo),
          str_filename);

  if ( pCat->is_in_updated_format )
  {
    fprintf(pFile, "# 16    oxpD                     int    Original Exposure time requested/required in Dark moon conditions (mins)\n\
# 17    oxpG                     int    Original Exposure time requested/required in Grey moon conditions (mins)\n\
# 18    oxpB                     int    Original Exposure time requested/required in Bright moon conditions (mins)\n\
# 19    expD                     int    Updated Exposure time requested/required in Dark moon conditions (mins)\n\
# 20    expG                     int    Updated Exposure time requested/required in Grey moon conditions (mins)\n\
# 21    expB                     int    Updated Exposure time requested/required in Bright moon conditions (mins)\n\
# 22    magAB                    float  Optical magnitude of target (r-band,AB)\n\
# 23    TemplateSpectrumFilename string Filename of the reference spectrum for this target\n\
# 24    ExtraInfo                -      Any additional user-suppplied information for this target\n\
#-----  ------------------------ ------ --------------------------------------------------------------- \n");

    //now the simple header string
    fprintf(pFile, "#%-3s %8s %6s %4s %4s %4s %6s %6s %6s %6s %15s %8s %9s %9s %3s %4s %4s %4s %4s %4s %4s %1s %5s %-30s %s\n",
            "Cat",
            "LineNum",
            "IntPri",
            "Nfie",
            "Ntil",
            "Nfib",
            "Texp_D",
            "Texp_G",
            "Texp_B",
            "FrDone",
            "InputObjectName",
            "ObjectID",
            "ObjectRA",
            "ObjectDec",
            "Pri",
            "oxpD",
            "oxpG",
            "oxpB",
            "expD",
            "expG",
            "expB",
            "r",
            "magAB",
            "TemplateSpectrumFilename",
            "ExtraInfo");
  }
  else
  {
    fprintf(pFile, "# 16    expD                     int    Exposure time requested/required in Dark moon conditions (mins)\n\
# 17    expG                     int    Exposure time requested/required in Grey moon conditions (mins)\n\
# 18    expB                     int    Exposure time requested/required in Bright moon conditions (mins)\n\
# 19    magAB                    float  Optical magnitude of target (r-band,AB)\n\
# 20    TemplateSpectrumFilename string Filename of the reference spectrum for this target\n\
# 21    ExtraInfo                -      Any additional user-suppplied information for this target\n\
#-----  ------------------------ ------ --------------------------------------------------------------- \n");

    //now the simple header string
    fprintf(pFile, "#%-3s %8s %6s %4s %4s %4s %6s %6s %6s %6s %15s %8s %9s %9s %3s %4s %4s %4s %1s %5s %-30s %s\n",
            "Cat",
            "LineNum",
            "IntPri",
            "Nfie",
            "Ntil",
            "Nfib",
            "Texp_D",
            "Texp_G",
            "Texp_B",
            "FrDone",
            "InputObjectName",
            "ObjectID",
            "ObjectRA",
            "ObjectDec",
            "Pri",
            "expD",
            "expG",
            "expB",
            "r",
            "magAB",
            "TemplateSpectrumFilename",
            "ExtraInfo");
  }

          


  fflush(pFile);
  return 0;
}

int write_object_summary_line (FILE* pFile, object_struct *pObj, int survey_main_mode, const char* str_suffix)
{
  int t;
  float texp_d = 0.0;
  float texp_g = 0.0;
  float texp_b = 0.0;
  float texp_dg = 0.0;
  int ntiles = 0;
  uint nfibers = 0;
  if ( pFile == NULL || pObj == NULL ) return 1;
  
  for(t=0;t<pObj->num_fields;t++)  //do the summary over all fields that this object lies within
  {
    texp_d  += pObj->pObjField[t].ptexp[MOON_PHASE_DARK];
    texp_g  += pObj->pObjField[t].ptexp[MOON_PHASE_GREY];
    texp_b  += pObj->pObjField[t].ptexp[MOON_PHASE_BRIGHT];
    texp_dg += pObj->pObjField[t].ptexp[MOON_PHASE_DARK] + pObj->pObjField[t].ptexp[MOON_PHASE_GREY];
    ntiles  += pObj->pObjField[t].ntiles;
    nfibers += pObj->pObjField[t].nfibs;

//    texp_d += pObj->ptexp[MOON_PHASE_DARK][t];
//    texp_g += pObj->ptexp[MOON_PHASE_GREY][t];
//    texp_b += pObj->ptexp[MOON_PHASE_BRIGHT][t];
//    texp_dg += pObj->ptexp[MOON_PHASE_DARK][t] + pObj->ptexp[MOON_PHASE_GREY][t];
//    ntiles += pObj->pntiles[t];
//    nfibers +=  pObj->pnfibs[t];
  }
//  if ( survey_main_mode == SURVEY_MAIN_MODE_POOLED )
//  {
//    texp_d = texp_dg;
//    texp_g = 0.0;
//  }

  fprintf(pFile, "%-3d %9d %6.4f %4d %4d %4u %6.1f %6.1f %6.1f %6.4f %s",
          (int)   pObj->pCat->code,
          (int)   pObj->line_number,
          (float) pObj->priority,
          (int)   pObj->num_fields,
          (int)   ntiles,
          (uint)  nfibers,
          (float) texp_d,
          (float) texp_g,
          (float) texp_b,
          (float) pObj->TexpFracDone,
          str_suffix);
  return 0;
}

//print out a summary of what happened to each objects with one line per object
//
//int write_object_summary_file (objectlist_struct *pObjList, survey_struct *pSurvey, cataloguelist_struct* pCatList, const char *str_filename )
//{
//  int n_object = 0;
//  FILE* pObject_file = NULL;
//  int i;
//  //these are the variables that are used when re-reading input files
//  FILE* pInputfile = NULL;
//  uint  inputfile_line_number = 0;
//  utiny inputfile_cat_code = (utiny) CATALOGUE_CODE_NULL;
//  if ( pObjList == NULL ||
//       pCatList == NULL ||
//       pSurvey == NULL ) return 1;
//
//
//  if ((pObject_file = fopen(str_filename, "w")) == NULL )
//  {
//    fprintf(stderr, "%s  There were problems opening the object summary file: %s\n",
//            ERROR_PREFIX, str_filename);
//    return 1;
//  }
//  fprintf(stdout, "%s Now I will write out the object summary file: %-40s \n", COMMENT_PREFIX, str_filename);
//  fflush(stdout);
//  
//  //  if ( write_object_summary_header ((FILE*) pObject_file, (const char*) str_filename, (const char*) "All" ))
//  if ( write_object_summary_header ((FILE*) pObject_file, (const char*) str_filename, (catalogue_struct*) NULL ))
//  {
//    fprintf(stderr, "%s  There were problems writing to the object summary file: %s\n",
//            ERROR_PREFIX, str_filename);
//    return 1;
//  }
//  
//  for(i=0;i<pObjList->nObjects;i++)
//  {
//    char info_buffer[STR_MAX];
//    int c;
//    //    int t;
//    //    float texp_d = 0.0;
//    //    float texp_g = 0.0;
//    //    float texp_b = 0.0;
//    //    float texp_dg = 0.0;
//    //    int ntiles = 0;
//    //    uint nfibers = 0;
//    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
//    //    char *psurvey_type = NULL;
//    
//    //need to find the correct line in the correct input catalogue file
//    if (pObj->pCat->code != inputfile_cat_code)
//    {
//      if ( pInputfile ) fclose (pInputfile) ;   pInputfile = NULL;
//      //choose the correct catalogue from the list
//      c = 0;
//      while (pCatList->pCat[c].code != pObj->pCat->code &&
//             c < pCatList->num_cats - 1) c++;
//
//      if ( pCatList->pCat[c].code != pObj->pCat->code ) return 1;
//      inputfile_cat_code = pCatList->pCat[c].code;
//      
//      //open the file
//      if ((pInputfile = fopen(pCatList->pCat[c].filename, "r")) == NULL )
//      {
//        fprintf(stderr, "%s  There were problems opening the catalogue file: %s\n",
//                ERROR_PREFIX, pCatList->pCat[c].filename);
//        return 1;
//      }
//      fprintf(stdout, "%s   Re-reading target info from input catalogue file: %s\n",
//              COMMENT_PREFIX, pCatList->pCat[c].filename);    fflush(stdout);
//      inputfile_line_number = 0;
//    }
//    //advance to the correct line in the input file
//    while (fgets(info_buffer,STR_MAX,pInputfile) &&
//           ++inputfile_line_number < (uint) pObj->line_number );
//
//    if ( write_object_summary_line ((FILE*) pObject_file, (object_struct*) pObj, (int) pSurvey->main_mode, (const char*) info_buffer))
//    {
//      fprintf(stderr, "\n%s  There were problems writing object line to file: %s\n",
//              ERROR_PREFIX, str_filename);
//      return 1;
//    }
//    n_object ++;
//  }
//  if ( pInputfile ) fclose (pInputfile) ;
//  pInputfile = NULL;
//
//  if (pObject_file) fclose(pObject_file);
//  pObject_file = NULL;
//  fprintf(stdout, "%s I wrote out %9d objects to object summary file %s\n",
//          COMMENT_PREFIX, n_object, str_filename);
//  fflush(stdout);
//  return 0;
//}

int write_object_summary_files (objectlist_struct *pObjList,
                                survey_struct *pSurvey,
                                cataloguelist_struct* pCatList, \
                                const char *str_prefix )
{
  int c;
  if ( pObjList == NULL ||
       pCatList == NULL ||
       pSurvey == NULL ) return 1;
  for(c=0;c< pCatList->num_cats;c++)
  {
    catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);
    if ( pCat->format_code == CATALOGUE_FORMAT_FITS )
    {
      if ( write_object_summary_file_fits ((objectlist_struct*) pObjList,
                                           (survey_struct*) pSurvey,
                                           (catalogue_struct*) pCat, 
                                           (const char*) str_prefix )) return 1;

    }
    else
    {
      if ( write_object_summary_file_ascii((objectlist_struct*) pObjList,
                                           (survey_struct*) pSurvey,
                                           (catalogue_struct*) pCat, 
                                           (const char*) str_prefix )) return 1;
    }
  }
  return 0;
}
      
// //print out a summary of what happened to each objects with one line per object, one file per catalogue
// // old version
// int write_object_summary_files (objectlist_struct *pObjList,
//                                 survey_struct *pSurvey,
//                                 cataloguelist_struct* pCatList, 
//                                 const char *str_prefix )
// {
//   int c;
//   if ( pObjList == NULL ||
//        pCatList == NULL ||
//        pSurvey == NULL ) return 1;
//   
//   for(c=0;c< pCatList->num_cats;c++)
//   {
//     int i;
//     int n_object = 0;
//     FILE* pObject_file = NULL;
//     //these are the variables that are used when re-reading input files
//     FILE* pInputfile = NULL;
//     uint  inputfile_line_number = 0;
//     catalogue_struct *pCat = (catalogue_struct*)  &(pCatList->pCat[c]);
//     char str_filename[STR_MAX];
// 
// 
//     if ( pCat->format_code == CATALOGUE_FORMAT_FITS )
//     {
//       snprintf(str_filename,STR_MAX,"%s_%s.fits.gz", str_prefix, pCat->codename);
//       //      fprintf(stdout, "%s Skipping object summary file (FITS not yet supported): %s\n",
//       //              WARNING_PREFIX, str_filename);
//       //      continue;
//     }
//     else
//     {
//       snprintf(str_filename,STR_MAX,"%s_%s.txt", str_prefix, pCat->codename);
//     }
// 
//     if ((pObject_file = fopen(str_filename, "w")) == NULL )
//     {
//       fprintf(stderr, "%s  There were problems opening the object summary file: %s\n",
//               ERROR_PREFIX, str_filename);
//       return 1;
//     }
//     fprintf(stdout, "%s Now I will write out the object summary file: %-40s ...", COMMENT_PREFIX, str_filename);
//     fflush(stdout);
//     
//     if ( write_object_summary_header ((FILE*) pObject_file,
//                                       (const char*) str_filename,
//                                       (catalogue_struct*) pCat ))
//     {
//       fprintf(stderr, "%s  There were problems writing to the object summary file: %s\n",
//               ERROR_PREFIX, str_filename);
//       return 1;
//     }
//   
//     //open the input catalogue file
//     if ((pInputfile = fopen(pCat->filename, "r")) == NULL )
//     {
//       fprintf(stderr, "\n%s  There were problems opening the catalogue file: %s\n",
//               ERROR_PREFIX, pCat->filename);
//       return 1;
//     }
//    
//     for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object ;i++)
//     {
//       char info_buffer[STR_MAX];
//       object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
// 
//       //advance to the correct line in the input file
//       while (fgets(info_buffer,STR_MAX,pInputfile) &&
//              ++inputfile_line_number < (uint) pObj->line_number );
// 
//       if ( write_object_summary_line ((FILE*) pObject_file,
//                                       (object_struct*) pObj,
//                                       (int) pSurvey->main_mode,
//                                       (const char*) info_buffer))
//       {
//         fprintf(stderr, "\n%s  There were problems writing object line to file: %s\n",
//                 ERROR_PREFIX, pCat->filename);
//         return 1;
//       }
// 
//       n_object ++;
//     }
//     if ( pInputfile ) fclose (pInputfile) ;  pInputfile = NULL;
//     if (pObject_file) fclose(pObject_file);  pObject_file = NULL;
//     fprintf(stdout, " wrote out %9d objects\n", n_object);
//     fflush(stdout);
// 
//     util_gzip_file ( (const char*) str_filename, (BOOL) (pSurvey->max_threads>1));
// 
//   }
//   return 0;
// }

//print out a summary of what happened to each objects with one line per object, one file per catalogue
// old version
int write_object_summary_file_ascii (objectlist_struct *pObjList,
                                     survey_struct *pSurvey,
                                     catalogue_struct* pCat, 
                                     const char *str_prefix )
{
  int i;
  int n_object = 0;
  FILE* pObject_file = NULL;
  //these are the variables that are used when re-reading input files
  FILE* pInputfile = NULL;
  uint  inputfile_line_number = 0;
  char str_filename[STR_MAX];
  if ( pObjList == NULL ||
       pCat     == NULL ||
       pSurvey == NULL ) return 1;

  snprintf(str_filename,STR_MAX,"%s_%s.txt", str_prefix, pCat->codename);

  if ((pObject_file = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the object summary file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  fprintf(stdout, "%s Writing the object summary file (ascii format): %-40s ...", COMMENT_PREFIX, str_filename);
  fflush(stdout);
  
  if ( write_object_summary_header ((FILE*) pObject_file,
                                    (const char*) str_filename,
                                    (catalogue_struct*) pCat ))
  {
    fprintf(stderr, "%s  There were problems writing to the object summary file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  
  //open the input catalogue file
  if ((pInputfile = fopen(pCat->filename, "r")) == NULL )
  {
    fprintf(stderr, "\n%s  There were problems opening the catalogue file: %s\n",
            ERROR_PREFIX, pCat->filename);
    return 1;
  }
  
  for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object ;i++)
  {
    char info_buffer[STR_MAX];
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
    
    //advance to the correct line in the input file
    while (fgets(info_buffer,STR_MAX,pInputfile) &&
           ++inputfile_line_number < (uint) pObj->line_number );
    
    if ( write_object_summary_line ((FILE*) pObject_file,
                                    (object_struct*) pObj,
                                    (int) pSurvey->main_mode,
                                    (const char*) info_buffer))
    {
      fprintf(stderr, "\n%s  There were problems writing object line to file: %s\n",
              ERROR_PREFIX, pCat->filename);
      return 1;
    }
    
    n_object ++;
  }
  if ( pInputfile ) fclose (pInputfile) ;  pInputfile = NULL;
  if (pObject_file) fclose(pObject_file);  pObject_file = NULL;
  fprintf(stdout, " wrote out %9d objects\n", n_object);
  fflush(stdout);
  
  if ( pSurvey->gzip_output_catalogues)
  {
    util_gzip_file ( (const char*) str_filename, (BOOL)  (pSurvey->max_threads>1));
  }
  return 0;
}

//print out a summary of what happened to each objects with one line per object, one file per catalogue
//
int write_object_summary_file_fits (objectlist_struct *pObjList,
                                    survey_struct *pSurvey,
                                    catalogue_struct* pCat, 
                                    const char *str_prefix )
{
  int n_object = 0;
  fitsfile *pInfile  = NULL;
  fitsfile *pOutfile = NULL;
  fitsfile *pMemfile = NULL;
  char str_infilename[LSTR_MAX];
  char str_memfilename[LSTR_MAX];
  char str_outfilename[LSTR_MAX];
  char *pstr_outfilename_forgzip;
  const int previous = 1,  current =1, following =1; //i.e. copy all HDUs
  int status = 0;
  if ( pObjList == NULL ||
       pCat     == NULL ||
       pSurvey  == NULL ) return 1;

  snprintf(str_infilename,LSTR_MAX,"%s", pCat->filename);
  //  snprintf(str_infilename,LSTR_MAX,"%s[1][col TESTCOL(1J)=#row]", pCat->filename);
  snprintf(str_memfilename,LSTR_MAX,"mem://%s_%s.fits", str_prefix, pCat->codename);

  // built in catalogue gzipping was causing performance issues - so do externally
  //  snprintf(str_outfilename,LSTR_MAX,"!%s_%s.fits%s", str_prefix, pCat->codename,
  //           (pSurvey->gzip_output_catalogues? ".gz" : ""));
  snprintf(str_outfilename,LSTR_MAX,"%s%s_%s.fits",
           (pSurvey->clobber?"!":""), str_prefix, pCat->codename);
  pstr_outfilename_forgzip = (char*) str_outfilename + (pSurvey->clobber?1:0); //skip the leading "!"
  
  if (fits_open_file ((fitsfile**) &pInfile, str_infilename, READONLY, &status))
  {
    fits_report_error(stderr, status); // print any error messages
    fprintf(stderr, "\n%s  There were problems opening the input catalogue file: %s\n",
            ERROR_PREFIX, str_infilename);
    return 1;
  }
   
  if (fits_create_file ((fitsfile**) &pMemfile, str_memfilename, &status))
  {
    fits_report_error(stderr, status); // print any error messages
    fprintf(stderr, "%s  There were problems opening the memory file: %s\n",
            ERROR_PREFIX, str_memfilename);
    return 1;
  }
  
  if (fits_create_file ((fitsfile**) &pOutfile, str_outfilename, &status))
  {
    fits_report_error(stderr, status); // print any error messages
    fprintf(stderr, "%s  There were problems opening the output object summary file: %s\n",
            ERROR_PREFIX, str_outfilename);
    return 1;
  }

  {
    char str_buffer_temp[LSTR_MAX];
    snprintf(str_buffer_temp, LSTR_MAX, "%s%s", pstr_outfilename_forgzip, (pSurvey->gzip_output_catalogues?".gz":""));
    fprintf(stdout, "%s Writing the object summary file (fits format): %-40s ...", COMMENT_PREFIX, str_buffer_temp);
    fflush(stdout);
  }
  
  //make a copy in memory of the input catalogue file
  if ( fits_copy_file ((fitsfile*) pInfile, (fitsfile*) pMemfile,
                       (int) previous, (int) current, (int) following, (int*) &status))
  {
    fits_report_error(stderr, status); // print any error messages
    return 1;
  }
  //  fprintf(stdout, " .1."); fflush(stdout);
  //now edit and write the new columns
  {
    int num_cols_before;
    long nrows_per_chunk;
    const char *ttype[] = {"EFF_PRIORITY", "NFIELDS", "NTILES", "NFIBERS", "TEXP_DONE_D", "TEXP_DONE_G", "TEXP_DONE_B", "FRAC_DONE"};
    const char *tform[] = {"1E"          , "1I"     , "1I"    , "1I"     , "1E"         , "1E"         , "1E"         , "1E"       };
    int        colnum[] = {1             , 2        , 3       , 4        , 5            , 6            , 7            , 8          };
    const int  colfmt[] = {TFLOAT        , TINT     , TINT    , TINT     , TFLOAT       , TFLOAT       , TFLOAT       , TFLOAT     };
    const int num_new_cols = (int) (sizeof(tform)/sizeof(char*));
    float *pbuffer_in_priority = NULL;
    int   *pbuffer_nfields = NULL;
    int   *pbuffer_ntiles  = NULL;
    int   *pbuffer_nfibers = NULL;
    float *ptexp_d         = NULL;
    float *ptexp_g         = NULL;
    float *ptexp_b         = NULL;
    float *pfrac_done      = NULL;
    long i,j,k;

    if ( fits_get_num_cols((fitsfile*) pMemfile, &num_cols_before, &status))
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }

    //Note The following operation is horrendously inefficient when the
    //code is built with the -fsanitize=address
    //this might be because the insert cols operation requires lots of memory shuffling
    if ( fits_insert_cols ((fitsfile*) pMemfile, num_cols_before+1, num_new_cols,
                           (char**) ttype, (char**) tform, (int*) &status))
    {
      fits_report_error(stderr, status); // print any error messages
      return 1;
    }
    //    fprintf(stdout, " .2."); fflush(stdout);
    for(i=0;i<num_new_cols;i++) colnum[i] = num_cols_before + i + 1; //column numbers in output file
      
    
    if ( fits_get_rowsize((fitsfile*) pMemfile, &nrows_per_chunk, &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }

    
    //allocate space for the buffers
    if ((pbuffer_in_priority = (float*) malloc(sizeof(float) * nrows_per_chunk)) == NULL ||
        (pbuffer_nfields     = (int*)   malloc(sizeof(int)   * nrows_per_chunk)) == NULL ||
        (pbuffer_ntiles      = (int*)   malloc(sizeof(int)   * nrows_per_chunk)) == NULL ||
        (pbuffer_nfibers     = (int*)   malloc(sizeof(int)   * nrows_per_chunk)) == NULL ||
        (ptexp_d             = (float*) malloc(sizeof(float) * nrows_per_chunk)) == NULL ||
        (ptexp_g             = (float*) malloc(sizeof(float) * nrows_per_chunk)) == NULL ||
        (ptexp_b             = (float*) malloc(sizeof(float) * nrows_per_chunk)) == NULL ||
        (pfrac_done          = (float*) malloc(sizeof(float) * nrows_per_chunk)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }

    j = 0;
    k = 1;
    for(i=pCat->index_of_first_object;i<=pCat->index_of_last_object ;i++)
    {
      object_struct *pObj = (object_struct*) &(pObjList->pObject[i]);
      int t;
      float texp_d = 0.0;
      float texp_g = 0.0;
      float texp_b = 0.0;
      float texp_dg = 0.0;
      int ntiles = 0;
      int nfibers = 0;
      for(t=0;t<pObj->num_fields;t++)  //do the summary over all fields that this object lies within
      {
        texp_d  += pObj->pObjField[t].ptexp[MOON_PHASE_DARK];
        texp_g  += pObj->pObjField[t].ptexp[MOON_PHASE_GREY];
        texp_b  += pObj->pObjField[t].ptexp[MOON_PHASE_BRIGHT];
        texp_dg += pObj->pObjField[t].ptexp[MOON_PHASE_DARK] + pObj->pObjField[t].ptexp[MOON_PHASE_GREY];
        ntiles  += pObj->pObjField[t].ntiles;
        nfibers += pObj->pObjField[t].nfibs;

//        texp_d += pObj->ptexp[MOON_PHASE_DARK][t];
//        texp_g += pObj->ptexp[MOON_PHASE_GREY][t];
//        texp_b += pObj->ptexp[MOON_PHASE_BRIGHT][t];
//        texp_dg += pObj->ptexp[MOON_PHASE_DARK][t] + pObj->ptexp[MOON_PHASE_GREY][t];
//        ntiles += pObj->pntiles[t];
//        nfibers +=  pObj->pnfibs[t];
      }
//      if ( pSurvey->main_mode == SURVEY_MAIN_MODE_POOLED )
//      {
//        texp_d = texp_dg;
//        texp_g = 0.0;
//      }

      //copy the data into the buffers from the pObj arrays
      pbuffer_in_priority[j] = (float) pObj->priority;
      pbuffer_nfields[j]     = (int) pObj->num_fields;
      pbuffer_ntiles[j]      = (int) ntiles;
      pbuffer_nfibers[j]     = (int) nfibers;
      ptexp_d[j]             = (float) texp_d;
      ptexp_g[j]             = (float) texp_g;
      ptexp_b[j]             = (float) texp_b;
      pfrac_done[j]          = (float) pObj->TexpFracDone;
     
      n_object ++;
      j++;
      if ( j >=nrows_per_chunk || i == pCat->index_of_last_object)
      {
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[0], (int) colnum[0], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, pbuffer_in_priority, (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[1], (int) colnum[1], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, pbuffer_nfields,     (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[2], (int) colnum[2], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, pbuffer_ntiles,      (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[3], (int) colnum[3], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, pbuffer_nfibers,     (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[4], (int) colnum[4], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, ptexp_d,             (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[5], (int) colnum[5], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, ptexp_g,             (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[6], (int) colnum[6], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, ptexp_b,             (int*) &status);
        fits_write_col ((fitsfile*) pMemfile, (int) colfmt[7], (int) colnum[7], (LONGLONG) k,
                        (LONGLONG) 1, (LONGLONG) j, pfrac_done,          (int*) &status);
        if ( status ) 
        {
          //          fprintf(stdout, "DEBUG %8ld %3ld %12.7f %12.7f %8.3f %6d %6d %6d %7.2f %7.2f %7.2f %7.4f\n",
          //                  i, j, pObj->ra, pObj->dec, pbuffer_in_priority[0], pbuffer_nfields[0], pbuffer_ntiles[0], pbuffer_nfibers[0],
          //                  ptexp_d[0], ptexp_g[0], ptexp_b[0], pfrac_done[0]); fflush(stdout);
          fits_report_error(stderr, status);
          return 1;
        }
        k += j; //keeps track of the current row number in the output file  
        j = 0;  //start filling a new buffer
      }
    }

    //free the buffers
    if ( pbuffer_in_priority ) free (pbuffer_in_priority); pbuffer_in_priority = NULL;
    if ( pbuffer_nfields     ) free (pbuffer_nfields    ); pbuffer_nfields     = NULL;
    if ( pbuffer_ntiles      ) free (pbuffer_ntiles     ); pbuffer_ntiles      = NULL;
    if ( pbuffer_nfibers     ) free (pbuffer_nfibers    ); pbuffer_nfibers     = NULL;
    if ( ptexp_d             ) free (ptexp_d            ); ptexp_d             = NULL;
    if ( ptexp_g             ) free (ptexp_g            ); ptexp_g             = NULL;
    if ( ptexp_b             ) free (ptexp_b            ); ptexp_b             = NULL;
    if ( pfrac_done          ) free (pfrac_done         ); pfrac_done          = NULL;

  }
  //  fprintf(stdout, " .3."); fflush(stdout);

  
  //now copy the outfile from the memory file

  //make a copy in memory of the input catalogue file
  if ( fits_copy_file ((fitsfile*) pMemfile, (fitsfile*) pOutfile, 
                       (int) previous, (int) current, (int) following, (int*) &status))
  {
    fits_report_error(stderr, status); // print any error messages
    return 1;
  }
  //  fprintf(stdout, " .4."); fflush(stdout);

  fits_close_file(pInfile, &status);
  fits_close_file(pOutfile, &status);
  fits_close_file(pMemfile, &status);
  
  
  fprintf(stdout, " wrote out %9d objects\n", n_object);
  fflush(stdout);

  if ( pSurvey->gzip_output_catalogues )
  {
    if ( util_gzip_file ( (const char*) pstr_outfilename_forgzip, (BOOL)  (pSurvey->max_threads>1)))
    {
      fprintf(stderr, "%s Problem gzipping %s\n", WARNING_PREFIX, pstr_outfilename_forgzip);
    }
  }
  return 0;
}





/////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
void debug_print_type_sizes(FILE* pstream)
{
  if ( pstream == NULL ) return;
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "char",                 (long) sizeof(char));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "short",                (long) sizeof(short));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "int",                  (long) sizeof(int));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "long",                 (long) sizeof(long));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "float",                (long) sizeof(float));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "double",               (long) sizeof(double));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "BOOL",                 (long) sizeof(BOOL));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "utiny",                (long) sizeof(utiny));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "tiny",                 (long) sizeof(tiny));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "uint",                 (long) sizeof(uint));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "field_struct",         (long) sizeof(field_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "fieldlist_struct",     (long) sizeof(fieldlist_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "object_struct",        (long) sizeof(object_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "objectlist_struct",    (long) sizeof(objectlist_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "objectfiber_struct",   (long) sizeof(objectfiber_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "sortfield_struct",     (long) sizeof(sortfield_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "sortfieldlist_struct", (long) sizeof(sortfieldlist_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "fibergeom_struct",     (long) sizeof(fibergeom_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "fiber_struct",         (long) sizeof(fiber_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "focalplane_struct",          (long) sizeof(focalplane_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "sector_struct",        (long) sizeof(sector_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "catalogue_struct",     (long) sizeof(catalogue_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "cataloguelist_struct", (long) sizeof(cataloguelist_struct));  

  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "environment_struct",   (long) sizeof(environment_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "telescope_struct",     (long) sizeof(telescope_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "moon_struct",          (long) sizeof(moon_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "survey_struct",        (long) sizeof(survey_struct));  
  //  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "tiling_area_struct",   (long) sizeof(tiling_area_struct));  
  //  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "master_struct",    (long)     sizeof(master_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "sundeets_struct",      (long) sizeof(sundeets_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "moondeets_struct",     (long) sizeof(moondeets_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "skycalcrecord_struct", (long) sizeof(skycalcrecord_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "skycalc_struct",       (long) sizeof(skycalc_struct));  
  fprintf(pstream, "%s The size of %-32s is %8ld bytes\n", DEBUG_PREFIX, "timeline_struct",      (long) sizeof(timeline_struct));  
  //fprintf(pstream, "%s The size of %16s is %8d bytes\n", DEBUG_PREFIX, "", sizeof());  
}



//get things all set up for a timeline survey
//call this just before starting the main loop fo the timeline survey
int timeline_survey_prep_tasks ( survey_struct *pSurvey,
                                 objectlist_struct *pObjList,
                                 fieldlist_struct *pFieldList,
                                 focalplane_struct *pFocal,
                                 telescope_struct *pTele,
                                 timeline_struct *pTime,
                                 cataloguelist_struct *pCatList,
                                 sortfieldlist_struct *pSortFieldList )
{
  int m;
  const char *str_field_ranking_plotfile  = "plot_field_ranking.plot";
  if ( pSurvey        == NULL ||
       pObjList       == NULL ||
       pFieldList     == NULL ||
       pFocal         == NULL ||
       pTime          == NULL ||
       pCatList       == NULL ||
       pSortFieldList == NULL ) return 1;
  
  pSurvey->night_counter = 0;
  pSurvey->nights_since_last_maintenance_shutdown = 0;
  pSurvey->nights_spent_in_maintenance_shutdown = 0;
  pSurvey->total_num_nights = (int) ceil(pSurvey->JD_stop-pSurvey->JD_start);
  pSurvey->debug_num_tiles_observed = 0;


  for (m=0;m<pSurvey->num_moon_groups; m++)
  {
    int ra_bin;
    moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
    pMoon->num_tiles_wasted = 0;
    pMoon->num_tiles_done = 0;
    for ( ra_bin=0; ra_bin < TILING_NRA_BINS; ra_bin ++ )
    {
      pSurvey->num_tiles_wasted_per_RAzen_bin[ra_bin][m] = (int) 0;
      pSurvey->num_tiles_done_per_RA_bin[ra_bin][m]   = (int) 0;
      pSurvey->num_tiles_done_per_RAzen_bin[ra_bin][m]   = (int) 0;
    }
  }
  fprintf(stdout, "%s Now I will carry out the timeline survey (JD_start,stop= %8.1f %8.1f, total nights= %d) \n",
          COMMENT_PREFIX, pSurvey->JD_start, pSurvey->JD_stop,  pSurvey->total_num_nights);    

  pSortFieldList->pFields = NULL;
  pSortFieldList->nFields = 0;
  pSortFieldList->array_length = 0;
  pSortFieldList->pLastField = NULL;
  pSortFieldList->last_field_count = 0;

  //make directories and gnuplot fles for fiber assignment/position plotting
  if ( pSurvey->write_fiber_assignments || pSurvey->write_fiber_positions)
  {
    if ( util_make_directory ((const char*) FIBER_ASSIGNMENT_SUBDIRECTORY))
    {
      return 1;
    }
  }
  if ( pSurvey->write_fiber_positions )
  {
    char str_filename[STR_MAX];
    snprintf(str_filename, STR_MAX, "%s/plot_fiber_positions.plot", FIBER_ASSIGNMENT_SUBDIRECTORY);
    if ( OpSim_positioner_write_instantaneous_positioners_geometry_plotfile ((const char*) str_filename, (focalplane_struct*) pFocal, (survey_struct*) pSurvey))
    {
      fprintf(stderr, "%s could not write to fiber position plotfile: %s\n", WARNING_PREFIX, str_filename);
    }
  }
   
  //////////////////////////////Prep work////////////////////////////////////////////
  //open up/init the report files
  if (init_tile_reports_logfile((survey_struct*) pSurvey, (const char*) TILE_REPORTS_LOG_FILENAME, (FILE**) &(pSurvey->pTileReports)))
  {
    fprintf(stderr, "%s  There were problems initialising the logfile : %s\n",
            ERROR_PREFIX, TILE_REPORTS_LOG_FILENAME);
    return 1;
  }

  if (init_night_reports_logfile((survey_struct*) pSurvey, (const char*) NIGHT_REPORTS_LOG_FILENAME, (FILE**) &(pSurvey->pNightReports)))
  {
    fprintf(stderr, "%s  There were problems initialising the logfile : %s\n",
            ERROR_PREFIX, NIGHT_REPORTS_LOG_FILENAME);
    return 1;
  }

  if (init_timeline_reports_logfile((survey_struct*) pSurvey, (const char*) TIMELINE_REPORTS_LOG_FILENAME, (FILE**) &(pSurvey->pTimelineReports)))
  {
    fprintf(stderr, "%s  There were problems initialising the logfile : %s\n",
            ERROR_PREFIX, TIMELINE_REPORTS_LOG_FILENAME);
    return 1;
  }

  if (init_per_tile_log((survey_struct*) pSurvey,  (const char*) PER_TILE_LOG_FILENAME, (FILE**) &(pSurvey->pPerTileLog)))
  {
    fprintf(stderr, "%s  There were problems initialising the logfile : %s\n",
            ERROR_PREFIX, PER_TILE_LOG_FILENAME);
    return 1;
  }

  if (init_tile_log_fits((survey_struct*) pSurvey,  (const char*) TILE_LOG_FILENAME_FITS))
  {
    fprintf(stderr, "%s  There were problems initialising the tile logfile : %s\n",
            ERROR_PREFIX, TILE_LOG_FILENAME_FITS);
    return 1;
  }

  //open the directory for the progress and FoM  reports
  if ( util_make_directory ((const char*) PROGRESS_REPORT_SUBDIRECTORY) )
  {
    fprintf(stderr, "%s Error making directory at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //open the directory for the field_ranking plots
  if ( util_make_directory ((const char*) FIELD_RANKING_SUBDIRECTORY) )
  {
    fprintf(stderr, "%s Error making directory at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  {
    char str_filename[STR_MAX];
    snprintf(str_filename, STR_MAX, "%s/%s", FIELD_RANKING_SUBDIRECTORY, str_field_ranking_plotfile);
    if ( write_gnuplot_script_for_field_ranking ( (const char*) str_filename, (survey_struct*) pSurvey))
    {
      fprintf(stderr, "%s  There were problems writing the field ranking plotfile : %s\n",
              ERROR_PREFIX, str_filename);
      return 1;
    }
  }


  //prepare the 2D histo that will record the positions of the fibers over the duration of the surveys
  if ( fiber_offsets_setup_2Dhisto ((focalplane_struct*) pFocal, ((twoDhisto_struct*) &(pFocal->fiberXY_histo_hires))) ||
       fiber_offsets_setup_2Dhisto ((focalplane_struct*) pFocal, ((twoDhisto_struct*) &(pFocal->fiberXY_histo_lores))) ||
       fiber_offsets_setup_2Dhisto ((focalplane_struct*) pFocal, ((twoDhisto_struct*) &(pFocal->fiberXY_histo_swres))) )
  {
    fprintf(stderr, "%s Error setting up 2d histo at file %s line %d\n",
            ERROR_PREFIX, __FILE__, __LINE__);
    
    return 1;
  }

  if ( survey_calc_total_time_available((survey_struct*) pSurvey, (telescope_struct*) pTele)) return 1;
  if ( survey_calc_tile_overheads ((survey_struct*) pSurvey)) return 1;
  if ( survey_calc_tiles_available ((survey_struct*) pSurvey)) return 1;

  return 0;
}

//do all tasks that are required at the beginning of a survey night
int timeline_survey_begin_night ( survey_struct *pSurvey,
                                  objectlist_struct *pObjList,
                                  fieldlist_struct *pFieldList,
                                  focalplane_struct *pFocal,
                                  telescope_struct *pTele,
                                  timeline_struct *pTime,
                                  cataloguelist_struct *pCatList,
                                  sortfieldlist_struct *pSortFieldList)
{
  if ( pSurvey        == NULL ||
       pObjList       == NULL ||
       pFieldList     == NULL ||
       pFocal         == NULL ||
       pTime          == NULL ||
       pCatList       == NULL ||
       pSortFieldList == NULL) return 1;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////START OF STUFF THAT GETS DONE ONCE PER NIGHT////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    if ( timeline_increment_time_to_next_night((timeline_struct*) pTime)) return 1;
    pSurvey->last_ra  = pTime->Rnow.ra_zen;      //start with telescope pointing to zenith
    pSurvey->last_dec = pTime->Rnow.dec_zen;     //
    pSortFieldList->pLastField = NULL;    //reset the last field observed to null at beginning of night
    pSortFieldList->last_field_count = 0; //

    pSurvey->night_counter ++;                //increment the night counter
    pSurvey->tile_counter = 0;             
    pSurvey->str_cloud_code = "INDEF";
    pSurvey->cloud_code = -1;
    pSurvey->print_header_flag = (BOOL) TRUE;
    pSurvey->observe_tonight = (BOOL) TRUE;

    //print out the progress report file 
    if ( pSurvey->progress_report_cadence > 0 )
    {
      if ( pSurvey->night_counter % pSurvey->progress_report_cadence == 1 )
      {
        char str_buffer[STR_MAX];
        snprintf(str_buffer, STR_MAX, "%s/progress_report_night_%04d.txt", PROGRESS_REPORT_SUBDIRECTORY, pSurvey->night_counter-1);
        if ( fieldlist_write_vertex_file ( (fieldlist_struct*) pFieldList, str_buffer , (FILE*) NULL))
        {
          fprintf(stderr, "%s Problem writing progress report vertex file to %s\n", ERROR_PREFIX, str_buffer);
        }
        if ( util_gzip_file ( (const char*) str_buffer, (BOOL)  (pSurvey->max_threads>1)))
        {
          //ignore this fault and continue
        }
      }
    }

    //print out the FoM report file if necessary
    if ( pSurvey->FoM_report_cadence > 0 )
    {
      if ( pSurvey->night_counter % pSurvey->FoM_report_cadence == 1 )
      {
        char str_buffer[STR_MAX];
        snprintf(str_buffer, STR_MAX, "%s/FoM_report_night_%04d.txt", PROGRESS_REPORT_SUBDIRECTORY, pSurvey->night_counter-1);
        if ( calc_FoMs ((survey_struct*) pSurvey, (objectlist_struct*) pObjList,
                        (cataloguelist_struct*) pCatList, (const char*) str_buffer, (BOOL) FALSE))
        {
          fprintf(stderr, "%s Problem writing FoM report file to %s\n", ERROR_PREFIX, str_buffer);
        }
        
        //also update the priority total for all Fields - this takes up a lot of processing time
        if ( fieldlist_calc_object_priority_totals ( (fieldlist_struct*) pFieldList,
                                                     (objectlist_struct*) pObjList,
                                                     (survey_struct*) pSurvey )) return 1;
      }
    }

    ///print some info to the terminal and logfiles
    {
      fprintf(pSurvey->pNightReports, "Night %4d/%-4d JD_midnight= %.5f JD_twi_eve= %.5f JD_twi_morn= %.5f JD_start= %.5f : ",
            pSurvey->night_counter,pSurvey->total_num_nights,
            pTime->Rnow.JD_midnight, pTime->Rnow.JD_twi_eve, pTime->Rnow.JD_twi_morn,
            pSurvey->JD_start);
      //      fflush(pSurvey->pNightReports);
      
      //new backspace method for printing minimal info to screen
      if ( pSurvey->night_counter == 1 )
      {
        fprintf(stdout, "%s Main survey: Working on Night %4d/%-4d",
                COMMENT_PREFIX, pSurvey->night_counter, pSurvey->total_num_nights);
        fflush(stdout);
      }
      else
      {
        fprintf(stdout, "\b\b\b\b\b\b\b\b\b%4d/%-4d", pSurvey->night_counter, pSurvey->total_num_nights);
        fflush(stdout);
      }
      if ( pSurvey->night_counter == pSurvey->total_num_nights )
      {
        fprintf(stdout, "\n");
        fflush(stdout);
      }
      
    }

    //write a header line to the per tile logfile - one header line per day
    if ( print_per_tile_log_header_line ((FILE*) pSurvey->pPerTileLog)) return 1;
    
    //let's see if we have any maintenance going on tonight
    //do this on a regular schedule
    if ( pSurvey->overheads_maintenance_nights_per_shutdown > 0 )
    {
      pSurvey->nights_since_last_maintenance_shutdown ++;
      if ( pSurvey->nights_since_last_maintenance_shutdown > pSurvey->overheads_maintenance_cadence )
      {
        fprintf(pSurvey->pNightReports, "We are going to carry out planned maintenance all night, sorry!\n");
        pSurvey->nights_spent_in_maintenance_shutdown ++;
        //check if we have done enough maintenance now
        if ( pSurvey->nights_spent_in_maintenance_shutdown >= pSurvey->overheads_maintenance_nights_per_shutdown )
        {
          pSurvey->nights_spent_in_maintenance_shutdown = 0;  //reset so that next night is ok
          pSurvey->nights_since_last_maintenance_shutdown = 0;
          //assume that we were able to fix all the the fibers
          if ( OpSim_positioner_repair_faulty_fibers((focalplane_struct*) pFocal, (double) pTime->JD ))
            return 1;
        }
        pSurvey->observe_tonight = (BOOL) FALSE;
        return 0;
      }
    }

    //let's see if we have any technical problems tonight
    if ( drand48() <= (double) pSurvey->overheads_fault_nights_per_year*DAYS_TO_YEARS )
    {
      fprintf(pSurvey->pNightReports, "We had a technical problem, closing dome all night, sorry!\n");
      pSurvey->observe_tonight = (BOOL) FALSE;
      return 0; //continue to next night
    }

    //let's see what the clouds are doing tonight
    if ( weather_random_cloud ((environment_struct*) &(pTele->environ),
                               (int*) &(pSurvey->cloud_code), (const char**) &(pSurvey->str_cloud_code)))  return 1;

    if ( pSurvey->cloud_code == CLOUD_COVER_CODE_BAD)
    {
      fprintf(pSurvey->pNightReports, "The cloud cover is too thick, closing dome all night, sorry!\n");
      pSurvey->observe_tonight = (BOOL) FALSE;
      return 0; //continue to next night
    }

    //check for bad (dome closed) wind conditions
    {
      float rand = (float) drand48();
      if ( rand < pTele->environ.wind_fraction_dome_closed )
      {
        fprintf(pSurvey->pNightReports, "The wind speed is too high, closing dome all night, sorry!\n");
        pSurvey->observe_tonight = (BOOL) FALSE;
        return 0; //continue to next night
      }
      else if ( rand < pTele->environ.wind_fraction_dome_closed + pTele->environ.wind_fraction_constrained )
      {
        pSurvey->wind_constraint_flag = TRUE; //i.e. we must point >90deg away from the wind
      }
      else pSurvey->wind_constraint_flag = FALSE;
        
    }

    fprintf(pSurvey->pNightReports, "\n");

    //wite the tile reports header line;
    if ( print_tile_reports_header_line ((FILE*) pSurvey->pTileReports)) return 1;


    //see if we need to switch the resolution mode of the switchable fibers
    if ( pFocal->num_fibers_switchable > 0)
    {
      utiny resolution_code;
//      int moon_group_code;
//      if ( timeline_determine_moon_phase_code((timeline_struct*) pTime,
//                                              (float*) NULL,
//                                              (int*) NULL,
//                                              (int*) &moon_group_code,
//                                              (survey_struct*) pSurvey,
//                                              (moon_struct**) NULL))
//      {
//        fprintf(stderr, "%s Problem calculating moon_phase_code\n", ERROR_PREFIX);
//        return 1;
//      }
//      
//      //if tonight will be mostly dark so turn on the low res fibers
//      //else turn on the high res fibers
//      if ( moon_group_code == MOON_GROUP_DG )  resolution_code = RESOLUTION_CODE_LOW;
//      else                                     resolution_code = RESOLUTION_CODE_HIGH;

      if ( pSurvey->night_counter > pSurvey->num_nights || pSurvey->night_counter < 1 ) return 1;

      if ( pSurvey->pSlots_each_night_bb[pSurvey->night_counter-1] >=
           pSurvey->pSlots_each_night_dg[pSurvey->night_counter-1] ) resolution_code = RESOLUTION_CODE_HIGH;
      else                                                           resolution_code = RESOLUTION_CODE_LOW;
      
      if ( OpSim_positioner_set_switchable_fibers_to_res ((focalplane_struct*) pFocal, (utiny) resolution_code ))
      {
        fprintf(stderr, "%s Problem switching fiber resolution\n", ERROR_PREFIX);
        return 1;
      }
        
    }
    
    //set positioners to base positions, and reset their attributes
    if ( OpSim_positioner_reset_fibers ((focalplane_struct*) pFocal)) return 1;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////END OF STUFF THAT GETS DONE ONCE PER NIGHT/////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////


  return 0;
}


//run obserrvations of a full night
int timeline_survey_observe_night ( survey_struct *pSurvey,
                                  objectlist_struct *pObjList,
                                  fieldlist_struct *pFieldList,
                                  focalplane_struct *pFocal,
                                  telescope_struct *pTele,
                                  timeline_struct *pTime,
                                  cataloguelist_struct *pCatList,
                                  sortfieldlist_struct *pSortFieldList)
{
  if ( pSurvey        == NULL ||
       pObjList       == NULL ||
       pFieldList     == NULL ||
       pFocal         == NULL ||
       pTime          == NULL ||
       pCatList       == NULL ||
       pSortFieldList == NULL  ) return 1;

  //////////////////////////////////////////////////////
  //this loop steps through the night
  /////////////////////////////////////////////////////
  while ( pTime->Rnow.nighttime_flag )
  {
    const int num_tiles_to_observe = 1;   //hardcoded here, but could be made into a variable later
    double minutes_remaining_in_night = (pTime->Rnow.JD_twi_morn - pTime->JD)*DAYS_TO_MINS;
    double minutes_used_up;
    double minutes_already_advanced;
    int moon_phase_code;
    int moon_group_code;
    moon_struct *pMoon = NULL;
    double moon_Az;
    float brightness_zen;
    float seeing_zen;
    int tile_status_flag = TILE_STATUS_FULL_EXPOSURE; 
    float Texp_mins = 0.0;
    int pass = 0;
    double tile_stop_sJD = 0.0;
    double tile_start_sJD = (double) (pTime->JD - pSurvey->JD_start);
    float effective_airmass_limit;
    int max_passes = (pSurvey->use_up_spare_tiles?2:1);
    
    //find out what class of sky brightness we have at zenith
    if ( timeline_determine_moon_phase_code((timeline_struct*) pTime,
                                            (float*) &brightness_zen,
                                            (int*) &moon_phase_code,
                                            (int*) &moon_group_code,
                                            (survey_struct*) pSurvey,
                                            (moon_struct**) &pMoon))
    {
      fprintf(stderr, "%s Problem calculating moon phase\n", ERROR_PREFIX);
      return 1;
    }
    //this includes an estimate of the slew overhead
    //      pMoon->minimum_minutes_per_complete_tile = (double) pMoon->Minutes_gross_per_tile;
      
    //work out the zenith seeing
      
    if ( weather_random_zenith_IQ ((environment_struct*) &(pTele->environ), (float *) &seeing_zen))
    {
      fprintf(stderr, "%s Problem calculating seeing at zenith, result was %8g\n", ERROR_PREFIX, seeing_zen);
    }

    // now calculate the effective maximum airmass -
    // ie the airmass which gives a delivered seeing of pSurvey->IQ_max
    effective_airmass_limit = (float) calc_max_airmass ((double) seeing_zen,
                                                        (double) pSurvey->IQ_max,
                                                        (double) pSurvey->airmass_max);


      
    //In main survey we have three cases - either:
    //  a) There is time for a complete tile inc overheads
    //  b) There is time for a partial tile inc overheads
    //  c) There is not enough time for anything useful
    if ( minutes_remaining_in_night >= pMoon->Minutes_gross_per_tile )
    {
      //carry out a full tile
      //        minutes_used_up =  pMoon->Minutes_gross_per_tile;
      minutes_already_advanced =  pMoon->Minutes_gross_per_tile*0.5; //want to move to middle of tile time
      tile_status_flag = TILE_STATUS_FULL_EXPOSURE;
      Texp_mins = pMoon->Minutes_net_per_tile;
      tile_stop_sJD = tile_start_sJD + pMoon->Minutes_gross_per_tile*MINS_TO_DAYS;
      minutes_used_up =  pMoon->Minutes_gross_per_tile;
    }
    else if ( minutes_remaining_in_night >= pMoon->minimum_gross_mins_per_partial_tile &&
              pSurvey->use_up_spare_tiles )
    {
      //        double overhead_minutes =  pMoon->Minutes_gross_per_tile - pMoon->Minutes_net_per_tile;
      //not enough time for full exposure, but we can use up the remaining time for a partial exposure
      //        minutes_used_up = minutes_remaining_in_night + 60.0; //the +60 ensures that we definitely go past twilight 
      minutes_already_advanced =  pMoon->minimum_gross_mins_per_partial_tile * 0.5; //want to move to middle of tile time
      tile_status_flag = TILE_STATUS_PARTIAL_EXPOSURE;
      Texp_mins = MINIMUM_USEFUL_EXPOSURE_MINS; //just use a single reduced exposure time
      tile_stop_sJD = tile_start_sJD + pMoon->minimum_gross_mins_per_partial_tile*MINS_TO_DAYS;
      minutes_used_up = pMoon->minimum_gross_mins_per_partial_tile; //the +60 ensures that we definitely go past twilight 
    }
    else
    {
      //not enough time for anything, so move beyond end of night
      tile_status_flag = TILE_STATUS_NO_EXPOSURE;
      Texp_mins = 0.0;
      //the +60 ensures that we definitely go past twilight
      if ( timeline_increment_time_by_mins((timeline_struct*) pTime, (double) minutes_remaining_in_night + 60.0 ))
      {
        return 1;
      }
      continue; //continue onto the next night
    }

      
    // Advance the time to the middle of the forthcoming tile
    if ( timeline_increment_time_by_mins((timeline_struct*) pTime, (double) minutes_already_advanced ))
    {
      return 1;
    }

    if ( timeline_print_short((FILE*) pSurvey->pTimelineReports,(timeline_struct*) pTime, (BOOL) pSurvey->print_header_flag))
    {
      //deliberately ignore any errors
    }  

    pSurvey->print_header_flag = FALSE;
    moon_Az = geom_calc_bearing(pTime->Rnow.ra_zen,pTime->Rnow.dec_zen,pTime->Rnow.moon.ra, pTime->Rnow.moon.dec);

    //print a record of the observing conditions
    //format NightNum,TileNum,surveyDay,RA_zen,Dec_Zen,SkyMagZen,MoonClass,MoonIllum,MoonRA,MoonDec,MoonZ,MoonAz
    pSurvey->tile_counter++;

    //      fflush(pSurvey->pTileReports);
    fprintf(pSurvey->pTileReports,
            "%-12s %5d %4d %10.5f %10.5f %6.0f %4s %8.4f %+8.3f %5.2f %2s %5.3f %7.3f %+7.3f %8.3f %8.3f %5.3f %5s : ",
            "TileReport", pSurvey->night_counter, pSurvey->tile_counter,
            tile_start_sJD, tile_stop_sJD,
            Texp_mins*MINS_TO_SECS, (tile_status_flag==TILE_STATUS_FULL_EXPOSURE?"Full":"Part"),
            pTime->Rnow.ra_zen, pTime->Rnow.dec_zen, brightness_zen,
            pMoon->codename, pTime->Rnow.moon.illum_frac,
            pTime->Rnow.moon.ra, pTime->Rnow.moon.dec,pTime->Rnow.moon.Z, moon_Az,
            seeing_zen, (char*) pSurvey->str_cloud_code);
    //      fflush(pSurvey->pTileReports);


    
    //write the same info to the fits tile log file, without incrementing row
    {
      fits_table_helper_struct *pT = (fits_table_helper_struct*) &(pSurvey->TileLog); //short alias
      int j;
      double tile_start_JD   = (double) (pSurvey->JD_start + tile_start_sJD);
      double tile_stop_JD    = (double) (pSurvey->JD_start + tile_stop_sJD);
      double tile_mid_JD     = (double) 0.5*(tile_start_JD+tile_stop_JD);
      float  texp_net_secs   = (float) Texp_mins*MINS_TO_SECS;
      float  texp_gross_secs = (float) (tile_stop_JD-tile_start_JD)*DAYS_TO_SECS;
      char   *pzen_sky[1]    = {(char*) pMoon->codename};
      char   *pcloud_code[1] = {(char*) pSurvey->str_cloud_code};
      for(j=0;j<pT->ncols;j++) pT->pvoid[j] = NULL;
      j= 0; pT->pvoid[j] = (void*) (int*)    &(pSurvey->night_counter);
      j= 1; pT->pvoid[j] = (void*) (int*)    &(pSurvey->tile_counter);
      j= 2; pT->pvoid[j] = (void*) (double*) &(tile_start_sJD);
      j= 3; pT->pvoid[j] = (void*) (double*) &(tile_stop_sJD);
      j= 4; pT->pvoid[j] = (void*) (double*) &(tile_start_JD);
      j= 5; pT->pvoid[j] = (void*) (double*) &(tile_stop_JD);
      j= 6; pT->pvoid[j] = (void*) (double*) &(tile_mid_JD);
      j= 7; pT->pvoid[j] = (void*) (float*)  &(texp_net_secs);
      j= 8; pT->pvoid[j] = (void*) (float*)  &(texp_gross_secs);
      j= 9; pT->pvoid[j] = (void*) (int*)    &(tile_status_flag);
      j=10; pT->pvoid[j] = (void*) (double*) &(pTime->Rnow.ra_zen);
      j=11; pT->pvoid[j] = (void*) (double*) &(pTime->Rnow.dec_zen);
      j=12; pT->pvoid[j] = (void*) (float*)  &(seeing_zen);
      j=13; pT->pvoid[j] = (void*) (float*)  &(brightness_zen);
      j=14; pT->pvoid[j] = (void*) (char**)  pzen_sky;
      j=15; pT->pvoid[j] = (void*) (float*)  &(pTime->Rnow.moon.illum_frac);
      j=16; pT->pvoid[j] = (void*) (double*) &(pTime->Rnow.moon.ra);
      j=17; pT->pvoid[j] = (void*) (double*) &(pTime->Rnow.moon.dec);
      j=18; pT->pvoid[j] = (void*) (double*) &(pTime->Rnow.moon.Z);
      j=19; pT->pvoid[j] = (void*) (double*) &(moon_Az);
      j=20; pT->pvoid[j] = (void*) (char*)    pcloud_code;
      //
      if ( fths_write_row ((fits_table_helper_struct*) pT,  (BOOL) FALSE)) return 1;
    }
    

    
//    //check to see if we should observe the SMC or LMC
//    //we should do this only in dark/grey time
//    magellanic_cloud_flag = MAGELLANIC_CLOUD_FLAG_NONE;
//    //      if ( pMoon->group_code == MOON_GROUP_DG )
//    if ( pSurvey->lmc_airmass_max > 1.0 )
//    {
//      if (  pSurvey->num_tiles_on_lmc[MOON_GROUP_BB] + pSurvey->num_tiles_on_lmc[MOON_GROUP_DG] < pSurvey->lmc_max_tiles )
//      {
//        double AM_lmc = (double) util_airmass_secz(geom_arc_dist(GEOM_COORD_LMC_RA0,GEOM_COORD_LMC_DEC0, pTime->Rnow.ra_zen, pTime->Rnow.dec_zen));
//        if ( AM_lmc <= pSurvey->lmc_airmass_max )
//        {
//          magellanic_cloud_flag |= MAGELLANIC_CLOUD_FLAG_LMC;
//        }
//      }
//    }
//    if ( pSurvey->smc_airmass_max > 1.0 )
//    {
//      if ( pSurvey->num_tiles_on_smc[MOON_GROUP_BB] + pSurvey->num_tiles_on_smc[MOON_GROUP_DG] < pSurvey->smc_max_tiles )
//      {
//        double AM_smc = (double) util_airmass_secz(geom_arc_dist(GEOM_COORD_SMC_RA0,GEOM_COORD_SMC_DEC0, pTime->Rnow.ra_zen, pTime->Rnow.dec_zen));
//        if ( AM_smc <= pSurvey->smc_airmass_max )
//        {
//          magellanic_cloud_flag |= MAGELLANIC_CLOUD_FLAG_SMC;
//        }
//      }
//    }

//    if ( magellanic_cloud_flag == MAGELLANIC_CLOUD_FLAG_NONE )
    {
      //pass1 chooses correct moon phase fields first
      //pass2 chooses fields that do not satisfy the moon phase criteria - only if using up spare tiles
      //if pass1 is successful we never get to pass2
      pass = 0;
      while (++pass<=max_passes)
      {
        //now choose a shortlist of Fields to observe
        if ( fieldlist_select_candidate_fields((fieldlist_struct*)     pFieldList,
                                               (timeline_struct*)      pTime,
                                               (survey_struct*)        pSurvey,
                                               (sortfieldlist_struct*) pSortFieldList,
                                               (BOOL)                  TRUE,        //do sorting = true
                                               (int) (pass==1 ? FIELDLIST_SELECT_ON_MOON_PHASE_STRICT :
                                                      FIELDLIST_SELECT_ON_MOON_PHASE_NONE),
                                               (float) effective_airmass_limit,
                                               (BOOL) pSurvey->wind_constraint_flag)) 
        {
          fprintf(stderr, "%s Problem selecting candidate fields (%d%s pass)\n",
                  ERROR_PREFIX, pass, (pass==1?"st":(pass==2?"nd":"th")));
          return 1;
        }
          
        //make a record in the field structure that each of the fields was visible - first pass only
        if ( pass == 1 )
        {
          int j;
          for(j=0;j<pSortFieldList->nFields;j++) {
            pSortFieldList->pFields[j].pField->ntiles_visible[moon_group_code] ++;
            int a;
            for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
            {
              if ( pSortFieldList->pFields[j].airmass <= 1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE)
                pSortFieldList->pFields[j].pField->ntiles_visible_am[moon_group_code][a] ++;
            }
          }
        }
          
        if ( pSortFieldList->nFields > 0 )
        {
          field_struct *pField = NULL;
          int j = -1;
          int npseudo = 0;
          //select the first field that has at least 1 object in it
          while ( npseudo == 0 && ++j<pSortFieldList->nFields )
          {
            int nuseful_tiles_done;
            pField = (field_struct*) (pSortFieldList->pFields[j].pField);

            //tiles observed in dark+grey are always useful
            nuseful_tiles_done = pField->ntiles_done[MOON_PHASE_DARK] + pField->ntiles_done[MOON_PHASE_GREY];
            if ( moon_group_code == MOON_PHASE_BRIGHT )  nuseful_tiles_done += pField->ntiles_done[MOON_PHASE_BRIGHT];

            if ( pField->ntiles_total < MAX_TILES_PER_FIELD &&
                 (pass > 1 ||  (nuseful_tiles_done < pField->ntiles_todo[moon_group_code])))
            {
              if ( pass == 1 )
              {
                //count the number of objects that can be observed
                if ( prepare_objects_for_observation ((survey_struct*) pSurvey,
                                                      (field_struct*) pField,
                                                      (objectlist_struct*) pObjList,
                                                      (moon_struct*) pMoon,
                                                      (int*) &npseudo ))
                {
                  fprintf(stderr, "%s Error preparing objects_for observation at file %s line %d\n",
                          ERROR_PREFIX, __FILE__, __LINE__);
                  return 1;
                }
              }
              else
              {
                //count the number of objects that can be observed
                if ( prepare_objects_for_observation_ignore_moon ((survey_struct*) pSurvey,
                                                                  (field_struct*) pField,
                                                                  (objectlist_struct*) pObjList,
                                                                  (int*) &npseudo ))
                {
                  fprintf(stderr, "%s Error preparing objects_for observation_ignore_moon at file %s line %d\n",
                          ERROR_PREFIX, __FILE__, __LINE__);
                  return 1;
                }
              }
            }
          }
            
          if ( npseudo <= 0 ) 
          {
            if ( pass >= max_passes ) 
            {
              //get to here if no suitable objects have been found in any field and it is
              //the 2nd pass or if we are only doing one pass
              fprintf(pSurvey->pTileReports,
                      "Warning : No suitable Objects in Field (%d/%d npseudo=%d nobjects=%d pass=%d) %5s %8.4f %+8.3f\n",
                      j, pSortFieldList->nFields, npseudo, pField->n_object, pass, pField->name, pField->ra0, pField->dec0);
              //                fflush(pSurvey->pTileReports);
              pMoon->num_tiles_wasted += num_tiles_to_observe;
              {
                int razen_bin = (int) (pTime->Rnow.ra_zen / TILING_RA_BIN_WIDTH );
                pSurvey->num_tiles_wasted_per_RAzen_bin[razen_bin][pMoon->group_code] += num_tiles_to_observe;
              }
              //write nulls to floating point columns in pSurvey->TileLog
              if ( write_nulls_to_tilelog ((fits_table_helper_struct*) &(pSurvey->TileLog),
                                           (survey_struct*) pSurvey, (focalplane_struct*) pFocal))
              {
                return 1;
              }
                            
            }
          }
          else    // we have at least 1 candidate target so we can observe this field
          {
            double Z             = (double) geom_arc_dist(pField->ra0, pField->dec0, pTime->Rnow.ra_zen, pTime->Rnow.dec_zen);
            double airmass       = (double) util_airmass_secz(Z);
            float  brightness    = (float) timeline_calc_sky_brightness ((timeline_struct*) pTime,
                                                                         (double) pField->ra0, (double) pField->dec0);
            float  Az            = (float) geom_calc_bearing(pTime->Rnow.ra_zen,pTime->Rnow.dec_zen,pField->ra0,pField->dec0);
            float  IQ            = (float) weather_seeing_at_airmass(seeing_zen,airmass);
            float  slew_distance = (float) geom_arc_dist(pField->ra0, pField->dec0, pSurvey->last_ra, pSurvey->last_dec);
            float  moon_dist     = (float) geom_arc_dist(pField->ra0, pField->dec0,pTime->Rnow.moon.ra, pTime->Rnow.moon.dec);
            float  actual_overhead_mins;
            int    last_dither_index = pField->last_dither_index;

            // use the formula of Fillipenko (1982), PASP, 94, 715F
            // http://adsabs.harvard.edu/abs/1982PASP...94..715F
            // sin(eta) = sin(pi/2 - dec_zen) * sin(HA) / sin(dist_zen)
            // dec_zen = observaer latitude
            // HA = object's Hour Angle (+ve if west of meridian)
            // dist_zen = distance of object from zenith
            float parallactic_angle = (float) asind(sind(pTime->Rnow.ra_zen - pField->ra0) *
                                                    sind(90.0 - pTime->Rnow.dec_zen) / sind(Z));
            
            if (isnan(slew_distance)) //DEBUG catch oddities 
            {
              fprintf(stderr, "%s Cannot proceed because slew distance (from %g,%g to %g,%g) =%g is NaN (at file %s line %d)\n",
                      ERROR_PREFIX, pSurvey->last_ra, pSurvey->last_dec, pField->ra0, pField->dec0, slew_distance, __FILE__, __LINE__);
              return 1;
            }
              
              
            //this is where the actual field allocation/observation takes place
            if ( observe_tiles_on_a_field ((timeline_struct*) pTime,
                                           (moon_struct*) NULL,
                                           (focalplane_struct*) pFocal,
                                           (fieldlist_struct*) pFieldList,
                                           (field_struct*) pField,
                                           (objectlist_struct*) pObjList,
                                           (survey_struct*) pSurvey,
                                           (int) num_tiles_to_observe,
                                           (float) Texp_mins))
            {
              fprintf(stderr, "%s Error in  observe_tiles_on_a_field() called from file %s line %d\n",
                      ERROR_PREFIX, __FILE__, __LINE__);
              return 1;
            }
            pMoon->num_tiles_done += num_tiles_to_observe;
            {
              int razen_bin = (int) (pTime->Rnow.ra_zen / TILING_RA_BIN_WIDTH );
              int ra_bin = (int) (pField->ra0 / TILING_RA_BIN_WIDTH );
              pSurvey->num_tiles_done_per_RA_bin[ra_bin][pMoon->group_code] += num_tiles_to_observe;
              pSurvey->num_tiles_done_per_RAzen_bin[razen_bin][pMoon->group_code] += num_tiles_to_observe;
            }

            
            if (pSortFieldList->pLastField == (field_struct*) pField) pSortFieldList->last_field_count += num_tiles_to_observe;
            else                                                      pSortFieldList->last_field_count  = num_tiles_to_observe;
              
            pSortFieldList->pLastField = (field_struct*) pField; //improve the chances of this field being observed next time
              

            //see if the time actually used up differs from the estimated time
            //if this is not the first tile on this field.
            if ((field_struct*) pField == (field_struct*) pSortFieldList->pLastField )
            {
              if ( (int) pField->last_dither_index != (int) last_dither_index &&
                   pSurvey->tiling_num_dithers > 0 )
                actual_overhead_mins = MAX(pSurvey->overhead_per_readout_reposition_calib_mins,
                                           pSurvey->overheads_mins_per_dither);
              else
                actual_overhead_mins = pSurvey->overhead_per_readout_reposition_calib_mins;
            }
            else
            {
              float slew_mins = (float) SECS_TO_MINS *
                util_calc_travel_time(slew_distance,
                                      pSurvey->overheads_slew_speed,
                                      pSurvey->overheads_slew_accel);
              actual_overhead_mins = MAX(pSurvey->overhead_per_readout_reposition_calib_mins, slew_mins);
            }
            minutes_used_up = pMoon->Minutes_net_per_tile + actual_overhead_mins;
                            
            pSurvey->debug_num_tiles_observed += num_tiles_to_observe;
              
            pSurvey->last_ra = pField->ra0;
            pSurvey->last_dec = pField->dec0;
              
            //print the field info to the tile report logfile
            fprintf(pSurvey->pTileReports,
                    "%-9s %8.4lf %+8.3lf %8.3f %8.3f %5.3f %5.2f %5.3f %5.1f %3d %3d %3d %3d %3d %3d %6.1f\n",
                    (char*) pField->name,
                    (double) pField->ra0,
                    (double) pField->dec0,
                    (float) Z,
                    (float) Az,
                    (float) airmass,
                    (float) brightness,
                    (float) IQ,
                    (float) moon_dist,
                    (int) pField->ntiles_req[MOON_GROUP_BB],
                    (int) pField->ntiles_req[MOON_GROUP_DG],
                    (int) pField->ntiles_todo[MOON_GROUP_BB],
                    (int) pField->ntiles_todo[MOON_GROUP_DG],
                    (int) pField->ntiles_done[MOON_PHASE_BRIGHT],
                    (int) (pField->ntiles_done[MOON_PHASE_GREY] + pField->ntiles_done[MOON_PHASE_DARK]),
                    (float) slew_distance);

            //write the same info to the fits tile log file, and increment row
            {
              fits_table_helper_struct *pT = (fits_table_helper_struct*) &(pSurvey->TileLog); //short alias
              int j;
              float airmass_f = (float) airmass;
              float Z_f = (float) Z;
              char *pfn[1] = {(char*) pField->name};
              int ntiles_done_dg = (int) (pField->ntiles_done[MOON_PHASE_GREY] +
                                          pField->ntiles_done[MOON_PHASE_DARK]);
              for(j=0;j<pT->ncols;j++) pT->pvoid[j] = NULL;
              j=21; pT->pvoid[j] = (void*) (char**)  (pfn);
              j=22; pT->pvoid[j] = (void*) (double*) &(pField->ra0);
              j=23; pT->pvoid[j] = (void*) (double*) &(pField->dec0);
              j=24; pT->pvoid[j] = (void*) (double*) &(pField->pa0);
              j=25; pT->pvoid[j] = (void*) (double*) &(pField->l0);
              j=26; pT->pvoid[j] = (void*) (double*) &(pField->b0);
              j=27; pT->pvoid[j] = (void*) (float*)  &(Z_f);
              j=28; pT->pvoid[j] = (void*) (float*)  &(Az);
              j=29; pT->pvoid[j] = (void*) (float*)  &(parallactic_angle);
              j=30; pT->pvoid[j] = (void*) (float*)  &(airmass_f);
              j=31; pT->pvoid[j] = (void*) (float*)  &(brightness);
              j=32; pT->pvoid[j] = (void*) (float*)  &(IQ);
              j=33; pT->pvoid[j] = (void*) (float*)  &(moon_dist);
              j=34; pT->pvoid[j] = (void*) (float*)  &(slew_distance);
              j=35; pT->pvoid[j] = (void*) (int*)    &(pField->ntiles_req[MOON_GROUP_BB]);
              j=36; pT->pvoid[j] = (void*) (int*)    &(pField->ntiles_req[MOON_GROUP_DG]);
              j=37; pT->pvoid[j] = (void*) (int*)    &(pField->ntiles_todo[MOON_GROUP_BB]);
              j=38; pT->pvoid[j] = (void*) (int*)    &(pField->ntiles_todo[MOON_GROUP_DG]);
              j=39; pT->pvoid[j] = (void*) (int*)    &(pField->ntiles_done[MOON_PHASE_BRIGHT]);
              j=40; pT->pvoid[j] = (void*) (int*)    &(ntiles_done_dg);
              j=41; pT->pvoid[j] = (void*) (float*)  &(pField->ntiles_visible_predict[MOON_GROUP_BB]);   
              j=42; pT->pvoid[j] = (void*) (float*)  &(pField->ntiles_visible_predict[MOON_GROUP_DG]);   
              
              //now write the relevant columns and increment the row counter
              if ( fths_write_row ((fits_table_helper_struct*) pT,  (BOOL) TRUE))    return 1;
            }

            
            pass ++; // make sure we don't go round the pass loop again 
            if ( pSurvey->debug_num_tiles_observed >= pSurvey->debug_max_tiles ) break; //DEBUG break
          }//if npseudo > 0
        }
        else
        {
          if ( pass >= max_passes ) 
          {
            //get to here if still no suitable fields have been found after 2nd pass
            fprintf(pSurvey->pTileReports, "Warning : There were no suitable Fields to observe\n"); 
            //              fflush(pSurvey->pTileReports);
            pMoon->num_tiles_wasted += num_tiles_to_observe;
            {
              int razen_bin = (int) (pTime->Rnow.ra_zen / TILING_RA_BIN_WIDTH );
              pSurvey->num_tiles_wasted_per_RAzen_bin[razen_bin][pMoon->group_code] += num_tiles_to_observe;
            }

            //write nulls to floating point columns in pSurvey->TileLog and increment the row counter
            if ( write_nulls_to_tilelog ((fits_table_helper_struct*) &(pSurvey->TileLog),
                                         (survey_struct*) pSurvey, (focalplane_struct*) pFocal))
            {
              return 1;
            }

          }
        }
        if ( pSurvey->debug_num_tiles_observed >= pSurvey->debug_max_tiles ) break; //DEBUG break
      } //end of pass loop
    }


    {
      double time_to_increment = minutes_used_up - minutes_already_advanced;
      //if at end of night - guarrantee a move into twilight (by advancing time by 60mins)
      if ( tile_status_flag == TILE_STATUS_PARTIAL_EXPOSURE ) time_to_increment += 60.0; 
      //now increment the night by the amount of time that was used up
      if ( timeline_increment_time_by_mins((timeline_struct*) pTime, (double) time_to_increment))
      {
        return 1;
      }
    }
        
    if ( pSurvey->debug_num_tiles_observed >= pSurvey->debug_max_tiles ) break; //DEBUG break
  } // end of while loop stepping through a night

  return 0;
}

int timeline_survey_end_tasks ( survey_struct *pSurvey,
                              objectlist_struct *pObjList,
                              fieldlist_struct *pFieldList,
                              focalplane_struct *pFocal,
                              telescope_struct *pTele,
                              timeline_struct *pTime,
                              cataloguelist_struct *pCatList,
                              sortfieldlist_struct *pSortFieldList )
{
  if ( pSurvey        == NULL ||
       pObjList       == NULL ||
       pFieldList     == NULL ||
       pFocal         == NULL ||
       pTime          == NULL ||
       pCatList       == NULL ||
       pSortFieldList == NULL ) return 1;


  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////
  //write a final progress report file 
  {
    char str_buffer[STR_MAX];
    snprintf(str_buffer, STR_MAX, "%s/progress_report_night_final.txt",PROGRESS_REPORT_SUBDIRECTORY);
    if ( fieldlist_write_vertex_file ( (fieldlist_struct*) pFieldList, str_buffer, (FILE*) stdout ))
    {
      fprintf(stderr, "%s Problem writing progress report vertex file to %s\n", ERROR_PREFIX, str_buffer);
    }
    if ( util_gzip_file ( (const char*) str_buffer, (BOOL)  (pSurvey->max_threads>1)))
    {
      //ignore this fault and continue
    }
  }

  //write a final FoM report file 
  {
    char str_buffer[STR_MAX];
    snprintf(str_buffer, STR_MAX, "%s/FoM_report_night_%04d.txt", PROGRESS_REPORT_SUBDIRECTORY, pSurvey->night_counter);
    if ( calc_FoMs ((survey_struct*) pSurvey, (objectlist_struct*) pObjList, (cataloguelist_struct*) pCatList, (const char*) str_buffer, (BOOL) TRUE))
    {
      fprintf(stderr, "%s Problem writing FoM report file to %s\n", ERROR_PREFIX, str_buffer);
    }
  }

  //write some totals of the tiles observed and wasted
  {
    int m;
    for(m=0; m<pSurvey->num_moon_groups; m++)
    {
      moon_struct *pMoon = (moon_struct*) &(pSurvey->moon[m]);
      //      fprintf(stdout, "%s Total tiles observed in moon phase %-8s = %6d\n",
      //              COMMENT_PREFIX, pMoon->longname, pMoon->total_tiles_observed);
      fprintf(stdout, "%s Num   tiles observed in moon phase %-8s = %6d\n",
              COMMENT_PREFIX, pMoon->longname, pMoon->num_tiles_done);
      fprintf(stdout, "%s Num   tiles wasted   in moon phase %-8s = %6d\n",
              COMMENT_PREFIX, pMoon->longname, pMoon->num_tiles_wasted);
    }

    //now write the detailed stats to file
    {
      int ra_bin;
      char str_filename[STR_MAX];
      FILE *pFile = NULL;
      time_t rawtime;
      struct tm *timeinfo;

      snprintf(str_filename, STR_MAX, "%s/%s", PLOTFILES_SUBDIRECTORY, WASTED_TILES_VS_RA_FILENAME);
      if ((pFile = fopen(str_filename, "w")) == NULL )
      {
        fprintf(stderr, "%s  There were problems opening file : %s\n",
                ERROR_PREFIX, str_filename);
        return 1;
      }
      
      fprintf(stdout, "%s Writing done/wasted tiling stats to file: %s\n", COMMENT_PREFIX, str_filename);

      //write a full header
      if (time((time_t*) &rawtime) < 0 ) return 1;
      timeinfo = localtime(&rawtime);
      fprintf(pFile, "# This is the tile done/wasted stats vs RA logfile\n\
# Calculated after execution of the survey\n\
# This log was first written at: %s\
# Original filename: %s\n\
# Total number of tiles observed usefully in Bright    moon phase = %d \n\
# Total number of tiles observed usefully in Dark/Grey moon phase = %d \n\
# Total number of tiles wasted            in Bright    moon phase = %d \n\
# Total number of tiles wasted            in Dark/Grey moon phase = %d \n\
# This logfile contains one entry per bin in RA\n\
# Description of columns:\n\
# Index  Name        Format    Description\n\
# -----  ----------- --------- ------------------------------------------------------------------------ \n\
# 1      RABIN       integer   Index of this RA bin\n\
# 2      RA_MID      float     RA at the middle of this bin (deg,J2000)\n\
# 3      RA_MIN      float     RA at the lower limit of this bin (deg,J2000)\n\
# 4      RA_MAX      float     RA at the upper limit of this bin (deg,J2000)\n\
# 5      NdoneRAzBB   integer  Number of tiles executed usefully in this RA(zen) bin (Bright moon)\n\
# 6      NdoneRAzDG   integer  Number of tiles executed usefully in this RA(zen) bin (Dark/Grey moon)\n\
# 7      NdoneRABB   integer   Number of tiles executed usefully in this RA bin (Bright moon)\n\
# 8      NdoneRADG   integer   Number of tiles executed usefully in this RA bin (Dark/Grey moon)\n\
# 9      NwastedBB   integer   Number of tiles wasted in this RA(zen) bin (Bright moon)\n\
# 10     NwastedDG   integer   Number of tiles wasted in this RA(zen) bin (Dark/Grey moon)\n", 
              asctime(timeinfo),
              str_filename,
              pSurvey->moon[MOON_GROUP_BB].num_tiles_done,
              pSurvey->moon[MOON_GROUP_DG].num_tiles_done,
              pSurvey->moon[MOON_GROUP_BB].num_tiles_wasted,
              pSurvey->moon[MOON_GROUP_DG].num_tiles_wasted);
  
      //write a one line column name key
      fprintf(pFile, "#%-5s %8s %8s %8s %10s %10s %10s %10s %10s %10s\n",
              "RABIN", "RA_MID", "RA_MIN1", "RA_MAX1", 
              "NdoneRAzBB", "NdoneRAzDG",
              "NdoneRABB", "NdoneRADG",
              "NwastedBB", "NwastedDG");
      //now write one line per RA bin
      for(ra_bin=0; ra_bin<TILING_NRA_BINS; ra_bin++)
      {
        double ra_mid  = (double) (ra_bin + 0.5) * (double) TILING_RA_BIN_WIDTH;
        double ra_min1 = (double) (ra_bin    ) * (double) TILING_RA_BIN_WIDTH;
        double ra_max1 = (double) (ra_bin + 1) * (double) TILING_RA_BIN_WIDTH;
        fprintf(pFile, "%6d %8g %8g %8g %10d %10d %10d %10d %10d %10d\n",
                (int)   ra_bin,
                (float) ra_mid,
                (float) ra_min1,
                (float) ra_max1, 
                (int) pSurvey->num_tiles_done_per_RAzen_bin[ra_bin][MOON_GROUP_BB],
                (int) pSurvey->num_tiles_done_per_RAzen_bin[ra_bin][MOON_GROUP_DG],
                (int) pSurvey->num_tiles_done_per_RA_bin[ra_bin][MOON_GROUP_BB],
                (int) pSurvey->num_tiles_done_per_RA_bin[ra_bin][MOON_GROUP_DG],
                (int) pSurvey->num_tiles_wasted_per_RAzen_bin[ra_bin][MOON_GROUP_BB],
                (int) pSurvey->num_tiles_wasted_per_RAzen_bin[ra_bin][MOON_GROUP_DG]);
      }
      
      if ( pFile ) fclose(pFile); pFile = NULL;
    }
  }
          
  

  //tidy up
  
  FREETONULL(pSortFieldList->pFields);
  // close the files
  if(pSurvey->pTileReports)     {fclose(pSurvey->pTileReports) ;     pSurvey->pTileReports = NULL;}
  if(pSurvey->pNightReports)    {fclose(pSurvey->pNightReports);     pSurvey->pNightReports = NULL;}
  if(pSurvey->pTimelineReports) {fclose(pSurvey->pTimelineReports);  pSurvey->pTimelineReports = NULL;}
  if(pSurvey->pPerTileLog)      {fclose(pSurvey->pPerTileLog);       pSurvey->pPerTileLog = NULL; }

  //close the fits TileLog file
  if ( fths_close((fits_table_helper_struct*) &(pSurvey->TileLog))) return 1;

  //re-write the field_vertex_files (so that they contain the tiles done etc
  if ( fieldlist_write_vertex_files ( (fieldlist_struct*) pFieldList ))
  {
    return 1;
  }

  //write the fiber efficiency plotfile
  if ( write_and_plot_fiber_efficiency ((survey_struct*) pSurvey)) return 1;
  
  //write the fiber efficiency maps
  if ( write_and_plot_fiber_efficiency_maps ((survey_struct*) pSurvey, (telescope_struct*) pTele)) return 1;
  
  //write the fiber offset files
  if ( fiber_offsets_write_and_plot ((focalplane_struct*) pFocal, (survey_struct*) pSurvey)) return 1;
  //tidy up the fiber offsets 2d histo
  FREETONULL(pFocal->fiberXY_histo_lores.pData);
  FREETONULL(pFocal->fiberXY_histo_hires.pData);
  FREETONULL(pFocal->fiberXY_histo_swres.pData);


  if ( plot_FoM_progress ((cataloguelist_struct*) pCatList, (survey_struct*) pSurvey, (const char*) PROGRESS_REPORT_SUBDIRECTORY))
  {
    fprintf(stderr, "%s  There were problems plotting FoM progress in : %s\n",
            ERROR_PREFIX, PROGRESS_REPORT_SUBDIRECTORY);
    return 1;
  }
  //  //plot the FoM progress file
  //  {
  //    char strPlotfile[STR_MAX];
  //    snprintf(strPlotfile, STR_MAX, "%s/plot_%s.plot", PROGRESS_REPORT_SUBDIRECTORY, FOM_PROGRESS_PLOT_STEM);
  //    //    if (util_run_gnuplot_on_plotfile( (const char*) strPlotfile, (BOOL) TRUE)) return 1;
  //    if ( survey_run_gnuplot_on_plotfile ( (survey_struct*) pSurvey, (const char*) strPlotfile)) return 1;
  //  }

  return 0;
}

  


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//This carries out a full day-by-day simulation of the 4MOST sky survey
int timeline_survey_wrapper    ( survey_struct *pSurvey,
                                objectlist_struct *pObjList,
                                fieldlist_struct *pFieldList,
                                focalplane_struct *pFocal,
                                telescope_struct *pTele,
                                timeline_struct *pTime,
                                cataloguelist_struct *pCatList)
{
  clock_t clocks_start;
  sortfieldlist_struct SortFieldList;
  
  if ( pSurvey    == NULL ||
       pObjList   == NULL ||
       pFieldList == NULL ||
       pFocal     == NULL ||
       pTime      == NULL ||
       pCatList   == NULL ) return 1;

  ///////////////////////////////////////////////////////////////////////////////////////////////////
  if ( timeline_survey_prep_tasks ( (survey_struct*) pSurvey,
                                    (objectlist_struct*) pObjList,
                                    (fieldlist_struct*) pFieldList,
                                    (focalplane_struct*) pFocal,
                                    (telescope_struct*) pTele,
                                    (timeline_struct*) pTime,
                                    (cataloguelist_struct*) pCatList,
                                    (sortfieldlist_struct*) &SortFieldList))
  {
    fprintf(stderr, "%s Problem carrying out preparatory tasks for timeline survey\n", ERROR_PREFIX);
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////


  
  //////////////////////////////calculate the observability of each Field////////////////////////////
  //do a dry run through the survey to work out how many times each
  //field can be observed
  if ( survey_calc_observability ( (survey_struct*)    pSurvey,
                                   (fieldlist_struct*) pFieldList,
                                   (telescope_struct*) pTele,
                                   (timeline_struct*)  pTime,
                                   (sortfieldlist_struct*) &SortFieldList))
  {
    fprintf(stderr, "%s Problem calculating the observability of each Field\n", ERROR_PREFIX);
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////


  ////-------------cut here ----------------------------------------///
  if ( survey_determine_sky_tiling ((survey_struct*) pSurvey,
                                    (fieldlist_struct*) pFieldList,
                                    (telescope_struct*) pTele,
                                    (focalplane_struct*) pFocal))
  {
    fprintf(stderr, "%s Problem planning the sky tiling\n", ERROR_PREFIX);
    return 1;
  }
  ////-------------cut here ----------------------------------------///

  


  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  clocks_start = clock();


  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  //This is the main survey loop...
  //We go around this loop once per night
  while ( pTime->JD < pSurvey->JD_stop)
  {
    if ( timeline_survey_begin_night ( (survey_struct*) pSurvey,
                                       (objectlist_struct*) pObjList,
                                       (fieldlist_struct*) pFieldList,
                                       (focalplane_struct*) pFocal,
                                       (telescope_struct*) pTele,
                                       (timeline_struct*) pTime,
                                       (cataloguelist_struct*) pCatList,
                                       (sortfieldlist_struct*) &SortFieldList)) return 1;


    if ( pSurvey->observe_tonight == FALSE ) continue;   //bad weather, tech faults etc
    
    if ( timeline_survey_observe_night ( (survey_struct*) pSurvey,
                                         (objectlist_struct*) pObjList,
                                         (fieldlist_struct*) pFieldList,
                                         (focalplane_struct*) pFocal,
                                         (telescope_struct*) pTele,
                                         (timeline_struct*) pTime,
                                         (cataloguelist_struct*) pCatList,
                                         (sortfieldlist_struct*) &SortFieldList)) return 1;
    
    
    if ( pSurvey->debug_num_tiles_observed >= pSurvey->debug_max_tiles )
    {
      fprintf(stdout, "\n%s I'm quitting because num_tiles_observed >= debug_max_tiles (%d>=%d)\n",
              WARNING_PREFIX,
              pSurvey->debug_num_tiles_observed,
              pSurvey->debug_max_tiles); 
      break; 
    }

    if ( pSurvey->night_counter >= pSurvey->debug_max_nights )
    {
      fprintf(stdout, "\n%s I'm quitting because number of nights >= debug_max_nights (%d>=%d)\n",
              WARNING_PREFIX,pSurvey->night_counter,pSurvey->debug_max_nights); 
      break; //DEBUG
    }
 
  }//end of main while loop through nights of survey 
  ////////////////////////////////////////////////////////////////////////////////////////////////////////

  fprintf(stdout, "%s Finished simulating the main timeline survey (that took %.2fs)\n",
          COMMENT_PREFIX, (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);
  fflush(stdout);

  ///////////////////////////////////////////////////////////////////////////////////////////////////
  if ( timeline_survey_end_tasks ( (survey_struct*) pSurvey,
                                   (objectlist_struct*) pObjList,
                                   (fieldlist_struct*) pFieldList,
                                   (focalplane_struct*) pFocal,
                                   (telescope_struct*) pTele,
                                   (timeline_struct*) pTime,
                                   (cataloguelist_struct*) pCatList,
                                   (sortfieldlist_struct*) &SortFieldList))
  {
    fprintf(stderr, "%s Problem carrying out end tasks for timeline survey\n", ERROR_PREFIX);
    return 1;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////////////



  return 0;
}
///////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////




//int timeline_determine_moon_phase_code( timeline_struct* pTime, float *pSkyBrZen, int* pcode)
//{
//  float brightness;
//  if ( pTime  == NULL || pcode == NULL ) return 1;
//  
//  brightness = weather_calc_sky_brightness ((timeline_struct*) pTime,
//                                            (double) pTime->Rnow.ra_zen, (double) pTime->Rnow.dec_zen);
//  
//  if      ( brightness < SKY_BRIGHTNESS_THRESH_BRIGHT ) *pcode = (int) MOON_PHASE_BRIGHT;
//  else if ( brightness < SKY_BRIGHTNESS_THRESH_GREY   ) *pcode = (int) MOON_PHASE_GREY;
//  else                                                  *pcode = (int) MOON_PHASE_DARK;
//  if ( pSkyBrZen != NULL ) *pSkyBrZen = (float) brightness;
//  
//  return 0;
//}



int timeline_determine_moon_phase_code (timeline_struct* pTime, float *pSkyBrZen,
                                        int *pPhase_code, int *pGroup_code,
                                        survey_struct *pSurvey, moon_struct **ppMoon)
{
  float brightness;
  if ( pTime  == NULL ) return 1;
  
  brightness = timeline_calc_sky_brightness ((timeline_struct*) pTime,
                                            (double) pTime->Rnow.ra_zen, (double) pTime->Rnow.dec_zen);
  if ( pSkyBrZen != NULL ) *pSkyBrZen = (float) brightness;
  
  if ( pPhase_code != NULL ||
       pGroup_code != NULL )
  {
    if      ( brightness < SKY_BRIGHTNESS_THRESH_BRIGHTGREY )
    {
      if ( pPhase_code != NULL ) *pPhase_code = (int) MOON_PHASE_BRIGHT;
      if ( pGroup_code != NULL ) *pGroup_code = (int) MOON_GROUP_BB;
      if ( pSurvey != NULL && ppMoon != NULL) *ppMoon = (moon_struct*) pSurvey->pmoon_bb;
    }
    else if ( brightness < SKY_BRIGHTNESS_THRESH_GREYDARK   )
    {
      if ( pPhase_code != NULL ) *pPhase_code = (int) MOON_PHASE_GREY;
      if ( pGroup_code != NULL ) *pGroup_code = (int) MOON_GROUP_DG;
      if ( pSurvey != NULL && ppMoon != NULL) *ppMoon = (moon_struct*) pSurvey->pmoon_dg;
    }
    else
    {
      if ( pPhase_code != NULL ) *pPhase_code = (int) MOON_PHASE_DARK;
      if ( pGroup_code != NULL ) *pGroup_code = (int) MOON_GROUP_DG;
      if ( pSurvey != NULL && ppMoon != NULL) *ppMoon = (moon_struct*) pSurvey->pmoon_dg;
    }
  }

  return 0;
}














///////////////////////////////////////////////////////////////
//set some flags etc in preparation for observing objects in afield
int prepare_objects_for_observation (survey_struct* pSurvey,
                                     field_struct *pField,
                                     objectlist_struct *pObjList,
                                     moon_struct *pMoon, int *pnok )
{
  int i;
  int nok = 0;
  if ( pSurvey  == NULL ||
       pField   == NULL ||
       pObjList == NULL ||
       pMoon    == NULL) return 1;
  
  for ( i=0;i<pField->n_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[pField->pid[i]]);
    //    object_struct *pObj = NULL;
    //    uint id = (uint) pField->pid[i];
    //    if ( id >= (uint) pObjList->nObjects) //this should never ever happen
    //    {
    //      fprintf(stderr, "%s Bad Bad Bad: id=%u Ok range: [%d:%d]\n",
    //              ERROR_PREFIX, id, 0, pObjList->nObjects-1); 
    //      return 1;
    //    }
    //    pObj = (object_struct*) &(pObjList->pObject[id]);
    if (  (uint) pObj->in_survey &  (uint) pMoon->mask  &&
          pObj->TexpFracDone < (float) pSurvey->maximum_texpfracdone_to_consider )
    {
      pObj->to_observe = (BOOL) TRUE;
      nok ++;
    }
    else  pObj->to_observe = (BOOL) FALSE;
  }
  if ( pnok != NULL ) *pnok = (int) nok;
  return 0; 
}

///////////////////////////////////////////////////////////////
//set some flags etc in preparation for observing objects in afield
int prepare_objects_for_observation_ignore_moon (survey_struct* pSurvey,
                                                 field_struct *pField,
                                                 objectlist_struct *pObjList, int *pnok )
{
  int i;
  int nok = 0;
  if ( pSurvey  == NULL ||
       pField   == NULL ||
       pObjList == NULL ) return 1;
  
  for ( i=0;i<pField->n_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[pField->pid[i]]);
    if ( (uint) pObj->in_survey & (uint) SURVEY_ANY  &&
         pObj->TexpFracDone < (float) pSurvey->maximum_texpfracdone_to_consider ) 
    {
      pObj->to_observe = (BOOL) TRUE;
      nok ++;
    }
    else  pObj->to_observe = (BOOL) FALSE;
  }
  if ( pnok != NULL ) *pnok = (int) nok;
  return 0; 
}


////////////////////////////////////////////////////////////////////////
//calculate what fraction of the tiles will be in
//dark, grey and bright time
//use a nominal time resolution
//also calculate the mean duration of a night over the survey interval
int survey_calc_moon_fractions ( survey_struct    *pSurvey,
                                 telescope_struct *pTele,
                                 timeline_struct  *pTime)
{
  
  double original_JD;
  clock_t clocks_start = clock();
  int num_tiles[MAX_NUM_MOON_PHASES];
  float frac_tiles[MAX_NUM_MOON_PHASES];
  int i;
  int num_times_total;
  const double time_resolution_mins = 10.0;

  ///SkyBrightnessHisto:
  //these bits are for calculating histogram of sky brightness
  const float delta = (float) 0.2;
  const float min   = (float) 15.0;
  const float max   = (float) 22.0;
  int *pnobj = NULL;
  float inv_delta = (float) 1.0 / delta;
  int nbins = (int) ceil((max - min)*inv_delta);
  int nlo = 0, nhi = 0, ntot = 0;

  float mean_night_duration_hours = (float) 0.0;
  int number_of_nights = 0;
  int last_iJD = -INT_MAX;

  
  if ( pSurvey    == NULL ||
       pTele      == NULL ||
       pTime      == NULL ) return 1;

  ///SkyBrightnessHisto:
  if ( (pnobj = (int*) calloc((size_t) nbins , sizeof(int))) == NULL )
  {
    return 1;
  }
  //
  
  fprintf(stdout, "%s Now I will work out the moon fractions ... ", COMMENT_PREFIX);
  (void) fflush(stdout);

  //init
  for(i=0;i<MAX_NUM_MOON_PHASES;i++) num_tiles[i] = 0;
  
  original_JD = (double) pTime->JD; //we will want to reset back to this at end;

  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  //go around this loop once per night
  while ( pTime->JD < pSurvey->JD_stop)
  {
    if ( timeline_increment_time_to_next_night((timeline_struct*) pTime))
    {
      return 1;
    }
    if ( pTime->iJD > last_iJD ) 
    {
      //use this to calc the mean night duration
      if ( pTime->Rnow.JD_twi_morn - pTime->Rnow.JD_twi_eve < 1.0 )
      {
        mean_night_duration_hours += (float)(DAYS_TO_HOURS*(pTime->Rnow.JD_twi_morn - pTime->Rnow.JD_twi_eve));
        number_of_nights ++;
        last_iJD = pTime->iJD;
        //      fprintf(stdout, "TIME_DEBUG %6d %12.4f %12.4f %10.4f %6d %10.4f\n",
        //              pTime->iJD, pTime->Rnow.JD_twi_eve, pTime->Rnow.JD_twi_morn,
        //              DAYS_TO_HOURS*(pTime->Rnow.JD_twi_morn - pTime->Rnow.JD_twi_eve),
        //              number_of_nights, mean_night_duration_hours);
      }
    }
    //////////////////////////////////////////////////////
    while ( pTime->Rnow.nighttime_flag )
    {
      double minutes_remaining_in_night = (pTime->Rnow.JD_twi_morn - pTime->JD)*DAYS_TO_MINS;
      int moon_phase_code;
      int  moon_group_code;
      moon_struct *pMoon = NULL;
      float brightness_zen = 0.0;
  
      if ( timeline_determine_moon_phase_code((timeline_struct*) pTime,
                                              (float*) &brightness_zen,
                                              (int*) &moon_phase_code,
                                              (int*) &moon_group_code,
                                              (survey_struct*) pSurvey,
                                              (moon_struct**) &pMoon))
      {
        fprintf(stderr, "%s Problem calculating moon_phase_code\n", ERROR_PREFIX);
        return 1;
      }
     
      //if there is time left
      if ( minutes_remaining_in_night >= time_resolution_mins )
      {
        num_tiles[moon_phase_code] ++; //count the tiles

        ///SkyBrightnessHisto:
        ntot ++;
        i = (int) floor((brightness_zen - min)*inv_delta);
        if      ( i < 0 )      nlo ++;
        else if ( i >= nbins ) nhi ++;
        else                   pnobj[i] ++; 
      }

      //now increment the night by the amount of time that was used up
      if ( timeline_increment_time_by_mins((timeline_struct*) pTime, (double) time_resolution_mins ))
      {
        return 1;
      }
    }
  }
  //set the time back to the start
  if ( timeline_set_time_from_JD((timeline_struct*) pTime, (double) original_JD)) return 1;
  fprintf(stdout, "done (that took %.3fs)\n",
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC);  fflush(stdout);

  pSurvey->mean_dark_time_hours_per_night = (float) mean_night_duration_hours / ((float) MAX(1,number_of_nights));
  fprintf(stdout, "%s Mean night length (twilight-twiglight) during survey (%d nights): %8.3f hours\n",
          COMMENT_PREFIX, number_of_nights, pSurvey->mean_dark_time_hours_per_night);
  
  num_times_total = num_tiles[MOON_PHASE_BRIGHT] +
    num_tiles[MOON_PHASE_GREY] +
    num_tiles[MOON_PHASE_DARK];
  frac_tiles[MOON_PHASE_BRIGHT] = (float) num_tiles[MOON_PHASE_BRIGHT]/(float)  MAX(1,num_times_total);
  frac_tiles[MOON_PHASE_GREY]   = (float) num_tiles[MOON_PHASE_GREY]  /(float)  MAX(1,num_times_total);
  frac_tiles[MOON_PHASE_DARK]   = (float) num_tiles[MOON_PHASE_DARK]  /(float)  MAX(1,num_times_total);
  fprintf(stdout, "%s Fraction of tiles in each moon phase: Dark= %8.5f Grey= %8.5f Bright= %8.5f\n",
          COMMENT_PREFIX,  frac_tiles[MOON_PHASE_DARK], frac_tiles[MOON_PHASE_GREY], frac_tiles[MOON_PHASE_BRIGHT]);

  pSurvey->pmoon_bb->fraction = frac_tiles[MOON_PHASE_BRIGHT];
  pSurvey->pmoon_dg->fraction = frac_tiles[MOON_PHASE_DARK] + frac_tiles[MOON_PHASE_GREY];

  //print out the mag histo stats
  ///SkyBrightnessHisto:
  {
    FILE* pFile = NULL;
    const char* str_filename = SKY_BRIGHTNESS_HISTO_FILENAME;
    //open up the output file
    if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
    {
      fprintf(stderr, "%s There were problems opening the sky brightness histo output file: %s\n", ERROR_PREFIX, str_filename);
      return 1;
    }

    fprintf(pFile, "#Predicted sky brightness distribution over the 4MOST survey.\n\
#The table below gives the fraction of tiles than we can expect to observe in each of\n\
#a number of bins of zenithal sky brightness (AB mag/arcsec2, V-band).\n\
#Calculated using a time resolution (step) of %g mins\n", time_resolution_mins);
    fprintf(pFile, "#%-7s %8s %8s %10s %10s\n", "BIN_MIN", "BIN_MAX", "BIN_MID", "NSTEPS", "FRACTION");
    //print bins below the low mag limit
    fprintf(pFile, "%8s %8.3f %8.3f %10d %10.8f\n",
            "NaN",  
            min,  
            min - 0.5*delta,
            nlo, 
            (float) nlo/(float)MAX(ntot,1));
    for(i=0;i<nbins;i++)
    {
      fprintf(pFile, "%8.3f %8.3f %8.3f %10d %10.8f\n",
              min + i*delta,  
              min + (i+1.)*delta,  
              min + (i+0.5)*delta,
              pnobj[i], 
              (float) pnobj[i]/(float)MAX(ntot,1));
    }
    //print objects above the high mag limit
    fprintf(pFile, "%8.3f %8s %8.3f %10d %10.8f\n",
            max,  
            "NaN",  
            max + 0.5*delta,
            nhi, 
            (float) nhi/(float)MAX(ntot,1));
    if ( pFile ) fclose (pFile); pFile = NULL;
  }
  if ( pnobj ) free   (pnobj); pnobj = NULL;

  return 0;
}
////////////////////////////////////////////////////////////////////////



// ////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////
// int survey_calc_observability_hpx ( survey_struct    *pSurvey,
//                                     fieldlist_struct *pFieldList,
//                                     telescope_struct *pTele,
//                                     timeline_struct  *pTime,
//                                     sortfieldlist_struct *pSortFieldList )
// {
//   return 0;
// }
// ////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//This carries out a full run through of the 4MOST sky survey to
//calculate which fields will be most easily observeable
//record results in the 
int survey_calc_observability ( survey_struct    *pSurvey,
                                fieldlist_struct *pFieldList,
                                telescope_struct *pTele,
                                timeline_struct  *pTime,
                                sortfieldlist_struct *pSortFieldList )
{
  int m,a;
  double minimum_minutes_per_complete_tile;
  double original_JD;
  clock_t clocks_start = clock();
  int night_of_survey = 0;
  //  double derived_JD_stop;
  //  float weight1,weight2;
  //  int night_to_switch_weight;
  float *pairmass_prob = NULL;
  const float airmass_prob_step = 0.01;
  const float airmass_prob_invstep = 1.0 / airmass_prob_step;
  int airmass_prob_nsteps;
  float good_weather_fraction;

  if ( pSurvey    == NULL ||
       pFieldList == NULL ||
       pTele      == NULL ||
       pTime      == NULL ||
       pSortFieldList == NULL ) return 1;

  
  fprintf(stdout, "%s Now I will work out the observability of each Field ... ", COMMENT_PREFIX);
  fflush(stdout);

  original_JD = (double) pTime->JD; //we will want to reset back to this at end;

  for(m=0;m<MAX_NUM_MOON_GROUPS;m++)
  {
    int ra_bin;
    for ( ra_bin=0; ra_bin < TILING_NRA_BINS; ra_bin ++ )
    {
      pSurvey->num_tiles_available_per_RA_bin[ra_bin][m] = 0.0;
      pSurvey->num_tiles_available_per_RA_bin_smooth[ra_bin][m] = 0.0;
    }
  }


  //set up the Airmass probability array
  //this array gives the probability that a particular airmass will be visible given the
  //seeing distribution of the site and the maximum IQ of the survey
  //make this a fast procedure by mapping the airmass into an integer to access the array
  airmass_prob_nsteps = (int) ceil((pSurvey->airmass_max - 1.0) * airmass_prob_invstep);
  if ( airmass_prob_nsteps <= 0 ) return 1;
  if ((pairmass_prob = (float*) malloc(sizeof(float)*airmass_prob_nsteps)) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  for(a=0;a<airmass_prob_nsteps;a++)
  {
    //airmass at middle of this bin
    float am             = 1.0 + ((float)a+0.5)*airmass_prob_step;         
    //max zeeing at zenith for this am to still meet IQ_max
    pairmass_prob[a] = weather_IQ_prob ((environment_struct*) &(pTele->environ), (float) am, (float) pSurvey->IQ_max);   
  }
  

  //modify the weight values by the fraction of time affected by bad weather and faults 
  good_weather_fraction = (float) (1.0 - DAYS_TO_YEARS*(float)pSurvey->overheads_fault_nights_per_year) *
    (1.0 - pTele->environ.cloud_fraction_closed);


//old //can be clever and only go round the loop for a maximum of 1 year, scale the visibility up from this
//old // need to cope with fractional years - easy. if duration = i.f years, then go round loop from 0->1 years
//old // However, this assumed that the moon location doesn't change from year to year which is obviously
//old // an oversimplification. So have removed the speed up tio improve accuracy.
//old //  derived_JD_stop = pTime->JD + (double) YEARS_TO_DAYS * MIN(1.0,pSurvey->duration);
//old //  weight1 = (float) ceil(pSurvey->duration) * good_weather_fraction;
//old //  weight2 = (float)floor(pSurvey->duration) * good_weather_fraction;
//old //  night_to_switch_weight = (int) (YEARS_TO_DAYS * MIN(1.0,pSurvey->duration));
  ////////////////////////////////////////////////////////////////////////////////////////////////////////

  //go around this loop once per night of the survey 
  //  while ( pTime->JD < derived_JD_stop)
  while ( pTime->JD < pSurvey->JD_stop)
  {
    float weight = good_weather_fraction;

    night_of_survey ++;
    if ( timeline_increment_time_to_next_night((timeline_struct*) pTime))
    {
      return 1;
    }

//    //weight nights according to the number of times they occur in the survey
//    if ( night_of_survey >= night_to_switch_weight ) weight = weight2;
//    else                                             weight = weight1;

    //////////////////////////////////////////////////////
    //go round this loop once per tile
    while ( pTime->Rnow.nighttime_flag )
    {
      double minutes_remaining_in_night = (pTime->Rnow.JD_twi_morn - pTime->JD)*DAYS_TO_MINS;
      double minutes_used_up;
      int moon_phase_code;
      int  moon_group_code;
      int j;
      moon_struct *pMoon = NULL;
      float brightness_zen = 0.0;
      //      int magellanic_cloud_flag;
      //      float seeing_zen, effective_airmass_limit;
      if ( timeline_determine_moon_phase_code((timeline_struct*) pTime,
                                              (float*) &brightness_zen,
                                              (int*) &moon_phase_code,
                                              (int*) &moon_group_code,
                                              (survey_struct*) pSurvey,
                                              (moon_struct**) &pMoon))
      {
        fprintf(stderr, "%s Problem calculating moon_phase_code\n", ERROR_PREFIX);
        return 1;
      }

      //save the info about the number of bright/dark/grey slots
      if ( night_of_survey <= pSurvey->num_nights)
      {
        if ( moon_group_code == MOON_GROUP_DG ) pSurvey->pSlots_each_night_dg[night_of_survey - 1] ++;
        else                                    pSurvey->pSlots_each_night_bb[night_of_survey - 1] ++;
      }

      minimum_minutes_per_complete_tile = (double) pMoon->Minutes_gross_per_tile;
      
      //In main survey we have three cases - either:
      //  a) There is time for a complete tile inc overheads
      //  b) There is time for a partial tile inc overheads
      //  c) There is not enough time for anything useful
      // However, here we will only count full tiles
     
      if ( minutes_remaining_in_night >= minimum_minutes_per_complete_tile )
      {
        //carry out a full tile
        minutes_used_up = minimum_minutes_per_complete_tile;
      }
      else
      {
        //not enough time for anything, so move just beyond end of night
        minutes_used_up = minutes_remaining_in_night + 1.0;
        //increment the night by the amount of time that was used up
        if ( timeline_increment_time_by_mins((timeline_struct*) pTime, (double) minutes_used_up ))
        {
          return 1;
        }
        continue;
      }

      
      {
        int   ntiles_requested_total = 0;
        float inv_ntiles_requested_total;
        if ( fieldlist_select_candidate_fields((fieldlist_struct*) pFieldList,
                                               (timeline_struct*)  pTime,
                                               (survey_struct*)    pSurvey,
                                               (sortfieldlist_struct*) pSortFieldList,
                                               (BOOL)            FALSE, //do not sort
                                               (int)             FIELDLIST_SELECT_ON_MOON_PHASE_STRICT, //take account of moon phase
                                               (float)           pSurvey->airmass_max,
                                               (BOOL)            FALSE ))   //no wind constraint
        {
          fprintf(stderr, "%s Problem selecting candidate fields\n", ERROR_PREFIX);
          return 1;
        }

        //sum the total tiles requested for all of the visible fields
        for(j=0;j<pSortFieldList->nFields;j++)
        {
          field_struct *pF = (field_struct*) pSortFieldList->pFields[j].pField;
          ntiles_requested_total += pF->ntiles_req[moon_group_code];
        }
        inv_ntiles_requested_total = 1.0 / ((float) MAX(1,ntiles_requested_total)); 
        
        //record in the field structure that we expect the field to be visible
        for(j=0;j<pSortFieldList->nFields;j++)
        {
          field_struct *pF = (field_struct*) pSortFieldList->pFields[j].pField;        
          float ntiles_fractional = weight;
          int a;
          int am_int;
          //          pSortFieldList->pFields[j].pField->ntiles_visible_predict[moon_group_code] += 1.0;

          //take account of the probability of the observation to be wind constrained
          if ( pF->dec0 > pTime->Rnow.dec_zen )  ntiles_fractional *= (1.0 - pTele->environ.wind_fraction_constrained);
          

          //take account of the probability of this field being observable
          //from its airmass combined with the seeing stats and max IQ limit
          am_int = (int) floor((pSortFieldList->pFields[j].airmass - 1.0)*airmass_prob_invstep);
          if ( am_int >= 0 && am_int < airmass_prob_nsteps) ntiles_fractional *= pairmass_prob[am_int];
          else                                              ntiles_fractional = 0.0;
          
          pF->ntiles_visible_predict[moon_group_code] += ntiles_fractional;
          pF->ntiles_visible_predict_weighted[moon_group_code] += ntiles_fractional * inv_ntiles_requested_total;

          for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
          {
            if ( pSortFieldList->pFields[j].airmass <= 1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE)
              pF->ntiles_visible_am_predict[moon_group_code][a] += ntiles_fractional;
          }
        }

        //record the number of tiles available as a function of RA
        {
          int ra_bin =  (int) floor(pTime->Rnow.ra_zen / TILING_RA_BIN_WIDTH);
          assert( ra_bin >= 0);
          assert(ra_bin < TILING_NRA_BINS );  //something is going badly wrong if this turns up
          pSurvey->num_tiles_available_per_RA_bin[ra_bin][moon_group_code] += weight;
        }

      }
      
      //now increment the night by the amount of time that was used up
      if ( timeline_increment_time_by_mins((timeline_struct*) pTime, (double) minutes_used_up ))
      {
        return 1;
      }
    }
  }
    
  //set the time back to the start
  if ( timeline_set_time_from_JD((timeline_struct*) pTime, (double) original_JD)) return 1;

  fprintf(stdout, "done (that took %.3fs)\n",
          (double)(clock()-clocks_start)/(double)CLOCKS_PER_SEC); 
  fflush(stdout);


  //boxcar smooth the tiles available per ra bin
  for(m=MOON_GROUP_BB;m<=MOON_GROUP_DG;m++)
  {
    int ra_bin1, ra_bin2;
    for ( ra_bin1=0; ra_bin1 < TILING_NRA_BINS; ra_bin1 ++ )
    {
      pSurvey->num_tiles_available_per_RA_bin_smooth[ra_bin1][m] = 0.0;
      for ( ra_bin2=ra_bin1-TILING_RA_BIN_SMOOTH_FACTOR/2; ra_bin2 <= ra_bin1+TILING_RA_BIN_SMOOTH_FACTOR/2; ra_bin2 ++ )
      {
        int ra_bin2a = (ra_bin2 < 0 ? ra_bin2 + TILING_NRA_BINS :
                        (ra_bin2 >= TILING_NRA_BINS ?  ra_bin2 - TILING_NRA_BINS :
                         ra_bin2));
        pSurvey->num_tiles_available_per_RA_bin_smooth[ra_bin1][m] +=
          pSurvey->num_tiles_available_per_RA_bin[ra_bin2a][m] / (float) TILING_RA_BIN_SMOOTH_FACTOR;
      }
    }
  }

  FREETONULL(pairmass_prob);
  return 0;
  }






///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
////////////////////////New functions to observe a field with n tiles ////////////
/////////////////////////////////////////////////////////////////////////////////
//work out which unassigned & nominally operating fibers are flagged to be able to see this object
//
int get_fibers_for_object (object_struct *pObj,
                           focalplane_struct *pFocal,
                           uint *pid_list,
                           float *pdist_list,
                           int *pnumber_of_fibers )  
{
  int   nfibs = 0;
  uint j,k;
  if ( pObj == NULL ||
       pFocal == NULL ||
       pid_list == NULL ||
       pdist_list == NULL ||
       pnumber_of_fibers == NULL ) return 1;
  
  for(j=0;j<(uint)pFocal->num_fibers;j++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[j]);
    if ( pFib->assign_flag == (utiny) FIBER_ASSIGN_CODE_NONE )
    {
      if ( pFib->status == (uint) FIBER_STATUS_FLAG_NOMINAL )
      {
        for(k=0;k<(uint)pFib->nobjects;k++)
        {
          if ( (uint) pFib->object_list[k] == (uint) pObj->id )
          {
            if ( nfibs >= (MAX_FIBERS_PER_OBJECT - 1)  )
            {
              return 1;
            }
            pid_list[nfibs] = (uint) j;
            pdist_list[nfibs] = pFib->object_sq_dist[k];
            nfibs ++;
            break; //out of k loop
          }
        }
      }
    }
  }
  *pnumber_of_fibers = (int) nfibs;
  return 0;
}

//return <0 if there is a problem
//return  0 if object is to be considered for this tile
//return >1 if object is not to be considered
//if sky_brightness is to be ignored then just set to a very large value (e.g 99.9 )
int calc_object_weight (survey_struct *pSurvey, object_struct *pObj, field_struct *pField, int moon_phase_code, float *presult )
{
  float result = 0.0;
  if ( pSurvey == NULL ||
       pObj    == NULL ||
       pField  == NULL ) return -1;

  
  //Do some basic chaecks of this object's validity
  //return as soon as any condition is not met
  if ( pObj->to_observe            == (BOOL) FALSE ) return 1;  //this is set by prepare_objects_for_observation
  if ( pObj->TexpFracDone          >= (float) pSurvey->maximum_texpfracdone_to_consider ) return 1;
  if ( pObj->treq[MOON_PHASE_DARK] >= MAX_EXPOSURE_TIME ) return 1;
  if ( pObj->treq[moon_phase_code] >= MAX_EXPOSURE_TIME ) return 1;
  if ( pObj->pCat->FoM             >= pSurvey->FoM_success_threshold ) return 1; 

  //{
  //    return 1;  //this object should not be considered for observation 
  //  }

  //special case: stop the S3,S4,S9 targets being considered when their sub-catalogue FoM threshold has been met
  if ( pObj->pCat->code == CATALOGUE_CODE_S3 ||
       pObj->pCat->code == CATALOGUE_CODE_S4 )
  {
    assert(pObj->sub_code >= 0);
    assert(pObj->sub_code < MAX_SUBFOMS);
    if ( pObj->pCat->subFoM[pObj->sub_code] >= pSurvey->FoM_success_threshold )
    {
      *presult = (float) 0.0;
      return 1;  //this object should not be considered for observation 
    }
  }

  //TODO - fix this properly
  if ( pObj->pCat->code == CATALOGUE_CODE_S9  )
  {
    int sub_cat_id = pObj->sub_code + (pObj->res == RESOLUTION_CODE_LOW ? 10 : 0);
    assert(sub_cat_id >= 0);
    assert(sub_cat_id < MAX_SUBFOMS);
    if ( pObj->pCat->subFoM[sub_cat_id] >= pSurvey->FoM_success_threshold )
    {
      *presult = (float) 0.0;
      return 1;  //this object should not be considered for observation 
    }
  }
  
  //if we got to here then the target is valid
  result = pObj->priority;
  
  //decrease priority for well completed objects
  if ( pObj->TexpFracDone >= 1.0 )
  {
    //    result *= pow(pObj->TexpFracDone, OBJECT_WEIGHT_COMPLETED_EXPONENT); 
    result *= pow(pObj->TexpFracDone, pSurvey->object_weighting_completed_exponent); 
  }
  else if ( pObj->TexpFracDone > 0. )  
  {
    //increase priority for partially completed targets
    //    result *= OBJECT_WEIGHT_STARTED_FACTOR; 
    result *= pSurvey->object_weighting_started_factor; 
  }
  else
  {
    //decrease priority of as yet unobserved objects that cannot possibly be completed in time allowed
    if ( pObj->treq[MOON_PHASE_DARK] > (pField->texp_total_todo - pField->texp_total_done) )
    {
      //      result *= OBJECT_WEIGHT_CANNOT_COMPLETE_FACTOR; 
      result *= pSurvey->object_weighting_cannot_complete_factor;
    }
  }

  //increase priority for high res objects
  if ( pObj->res == RESOLUTION_CODE_HIGH )
  {
    //    result += OBJECT_WEIGHT_HIRES_BONUS; 
    result += pSurvey->object_weighting_hires_bonus; 
  }

  
  //decrease priority for objects that require a much longer exposure in bright time than in dark time 
  if ( moon_phase_code == MOON_PHASE_BRIGHT )
  {
    //    if ( pObj->treq[MOON_PHASE_BRIGHT]/MAX(1.0,pObj->treq[MOON_PHASE_DARK]) > OBJECT_WEIGHT_HARD_IN_BRIGHT_THRESHOLD)
    if ( pObj->treq[MOON_PHASE_BRIGHT]/MAX(1.0,pObj->treq[MOON_PHASE_DARK]) > pSurvey->object_weighting_hard_in_bright_threshold)
    {
      //      result += OBJECT_WEIGHT_HARD_IN_BRIGHT_DECREMENT; 
      result += pSurvey->object_weighting_hard_in_bright_decrement;
    }
  }
   
  *presult = (float) result;

  return 0;
}

/////////////////////////////////////////////////////////////////////////
//return <0 if there is a problem
//return  0 if a fiber was successfully assigned to the object 
//return >1 if no fiber could be assigned to the object (this is unfortunate but not an error) 
//assign_fiber_to_object
int assign_a_fiber_to_object (focalplane_struct  *pFocal,
                              field_struct       *pField,
                              object_struct      *pObj,
                              objectfiber_struct *pObjFib,
                              int                 objectfiber_index,
                              fiber_struct      **ppAssignedFib,
                              int                 dither_index,
                              int                *pnfibers_remain_per_sector)
{
  int j;
  int   nfibs = 0;
  int   *pfibs_id = NULL;         //shortcuts
  float *pfibs_sqdist = NULL;    //
  BOOL  object_targetted_flag = FALSE;
  double x, y; //object coords
#ifdef ALLOCATOR_DEBUG_OUTPUT
  int   field_sub_index;  //the index of this field within the object's array of fields 
#endif

  if ( pFocal == NULL ||
       pField == NULL ||
       pObj == NULL  ||
       pObjFib == NULL ||
       ppAssignedFib == NULL ) return -1;
  
    
  //make shorter named aliases into the arrays for easy sorting purposes
  nfibs        = (int)      pObjFib->pnumfibs[objectfiber_index];
  pfibs_sqdist = (float*) &(pObjFib->psqdist[objectfiber_index*MAX_FIBERS_PER_OBJECT]);
  pfibs_id     = (int*)   &(pObjFib->pfib_ids[objectfiber_index*MAX_FIBERS_PER_OBJECT]);

  if ( nfibs >= MAX_FIBERS_PER_OBJECT ||
       nfibs < 0 ) //this should never happen
  {
    fprintf(stderr, "%s Too many/few fibers for this object: nfibs= %d\n", ERROR_PREFIX, nfibs);
    return -1;
  }

  //now sort potential fibers in ascending order of distance
  //this could instead be done by taking account of the number of targets that each fiber can see
  if ( nfibs > 0 )
  {
    if ( util_fisort((ulong) nfibs, (float*) pfibs_sqdist, (int*) pfibs_id))
    {
      fprintf(stderr, "%s Error sorting data at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return -1;
    }
  }

  if (geom_project_gnomonic_to_mm ((double) pField->pra0[dither_index],
                                   (double) pField->pdec0[dither_index],
                                   (double) pField->pa0,
                                   (double) pObj->ra, (double) pObj->dec, pFocal->plate_scale,
                                   (double*) &x, (double*) &y ))
  {
    return 1;
  }

  
#ifdef ALLOCATOR_DEBUG_OUTPUT //print out the candidate fiber list
  if ( (field_sub_index = get_field_sub_index ((object_struct*) pObj, (field_struct*) pField )) < 0 )
  {
    fprintf(stderr, "%s Error finding field_sub_index at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return -1;
  }

  for(j=0;j<nfibs;j++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[pfibs_id[j]]);
    fprintf(stdout, "%s Field= %s object_id= %8d x,y= %7.3f %7.3f -?- fiber_id= %5d x0,y0= %10.4f %10.4f rank= %2d/%2u dx,dy= %7.3f %7.3f dist= %8.4f pfibs_id=%6d\n",
            DEBUG_PREFIX, pField->name, pObj->id, (double) x, (double) y,
            pFib->id,
            pFib->Geom.x0, pFib->Geom.y0,
            j+1,
            (uint) pObj->pnfibs[field_sub_index],
            pFib->Geom.x0 - x, pFib->Geom.y0 - y,
            sqrt(pfibs_sqdist[j]), pfibs_id[j]); fflush(stdout);
  }
#endif    

  //now attempt to assign the fibers
  for(j=0;j<nfibs;j++)
  {
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[pfibs_id[j]]);
    int collision_code ;
    if ( pFib->is_empty )
    {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
      fprintf(stdout, "%s Field= %s object_id= %8d X-- fiber_id= %5d (this fiber position is empty)\n", DEBUG_PREFIX, pField->name,pObj->id, pFib->id); fflush(stdout);
#endif        
      continue;
    }
    if ( pnfibers_remain_per_sector[           pObj->res*MAX_SECTORS_PER_TILE + pFib->sector] + \
         pnfibers_remain_per_sector[RESOLUTION_CODE_DUAL*MAX_SECTORS_PER_TILE + pFib->sector]
         <= pFocal->num_fibers_reserved_per_sector[pObj->res] )
    {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
      fprintf(stdout, "%s Field= %s object_id= %8d --- fiber_id= %5d (sector->id %d is fully assigned)\n", DEBUG_PREFIX, pField->name, pObj->id, pFib->id, pFocal->Sector[pFib->sector].id); fflush(stdout);
#endif    
      continue;
    }

    if ( pFib->assign_flag != FIBER_ASSIGN_CODE_NONE )
    {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
      fprintf(stdout, "%s Field= %s object_id= %8d --- fiber_id= %5d (fiber already assigned)\n", DEBUG_PREFIX, pField->name, pObj->id, pFib->id); fflush(stdout);
#endif    
      continue;
    }

    
    collision_code = OpSim_positioner_test_collisions((focalplane_struct*) pFocal, (fiber_struct*) pFib,
                                                      (float) x,
                                                      (float) y);
    if ( collision_code < 0 )
    {
      fprintf(stderr, "%s Error testing collisions (coll_code %2s) at file %s line %d\n", ERROR_PREFIX, COLLISION_CODE_TO_STR(collision_code), __FILE__, __LINE__);
      return -1;
    }
    else if ( collision_code > 0 )
    {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
      fprintf(stdout, "%s Field= %s object_id= %8d X-- fiber_id= %5d (fiber collision: coll_code= %2s)\n", DEBUG_PREFIX, pField->name,pObj->id, pFib->id, COLLISION_CODE_TO_STR(collision_code)); fflush(stdout);
#endif        
      continue;
    }
    else //no collisions
    {
      // assignment is possible, so enact it!
      if ( OpSim_positioner_set_fiber_to_position ((focalplane_struct*) pFocal, (fiber_struct*) pFib,
                                                   (float) x,
                                                   (float) y))
      {
        fprintf(stderr, "%s Error in set_fiber_to_position() at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
        return -1;
      }

      //if we got here then we have successfully assigned a fiber to the object

      //set the info that cannot be deduced by calling function
      pFib->fiber_ranking = j+1;

      *ppAssignedFib = (fiber_struct*) pFib;  //pass this pointer back for the calling function to use
      object_targetted_flag = TRUE;

      
#ifdef ALLOCATOR_DEBUG_OUTPUT 
      fprintf(stdout, "%s Field= %s object_id= %8d <-- fiber_id= %5d (I tried %2d/%2u fibs)\n",
              DEBUG_PREFIX, pField->name, pObj->id,pFib->id, j+1, (uint) pObj->pnfibs[field_sub_index]);
      fflush(stdout);
#endif        

      //we have successfully assigned a fiber to the object, so break out of loop
      break;
    }
  }
  
  if (object_targetted_flag ) return 0;
  else
  {
#ifdef ALLOCATOR_DEBUG_OUTPUT 
    fprintf(stdout, "%s Field= %s object_id= %8d XXX fiber_id= ..... (I tried %4u fibs - and no assignment)\n",
            DEBUG_PREFIX, pField->name, pObj->id, (uint) pObj->pnfibs[field_sub_index]);
    fflush(stdout);
#endif
    
    return 1;
  }
}




///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
int observe_tile_on_a_field (timeline_struct    *pTime,
                             moon_struct        *pMoon,
                             focalplane_struct  *pFocal,
                             fieldlist_struct   *pFieldList,
                             field_struct       *pField,
                             objectlist_struct  *pObjList,
                             survey_struct      *pSurvey,
                             objectfiber_struct *pObjFib,
                             float              texp_per_tile_mins,
                             int                dither_index)
{
  int i;
  int ncand;
  int moon_phase_code;
  int moon_group_code;
  float *pSortdata = NULL;
  int   *pSortindex = NULL;
  int   *pObjFiberindex = NULL;
  uint tile_mask ;
  moon_struct *pMoonLocal = NULL;
  int nfibers_remain[NRESOLUTION_CODES];
  int nfibers_reserved[NRESOLUTION_CODES];
  int ncandidates[NRESOLUTION_CODES];
  int nassigned[NRESOLUTION_CODES];
  float tile_throughput = 1.0; //TODO - add variable tile throughput  

  int nfibers_remain_per_sector[NRESOLUTION_CODES*MAX_SECTORS_PER_TILE];

  if ( pFocal     == NULL  ||
       pField    == NULL  ||
       pFieldList == NULL  ||
       pObjList  == NULL  ||
       pSurvey   == NULL  ||
       pObjFib   == NULL  ) return 1;

  if ( pTime == NULL  && pMoon == NULL ) return 1; //one of thse can be NULL, but not both

  for(i=0;i<NRESOLUTION_CODES;i++)
  {
    ncandidates[i] = 0;
    nassigned[i] = 0;
  }


  //steps
  // 1) allocate sorting arrays
  // 2) reset fibers
  // 3) increment counters
  // 4) make shortlist of input targets 
  // 5) prioritise input targets and order them
  // 6) calc which fibers can see which targets
  // 7) assign fibers to targets taking account of collisions
  // 8) write results to target arrays
  // 9) check for fiber failures

  //allocate some space to place the sorting arrays.
  if (pField->n_object > 0 )
  {
    if ((pSortindex     = (int*)   malloc(sizeof(int)   * pField->n_object)) == NULL ||
        (pObjFiberindex = (int*)   malloc(sizeof(int)   * pField->n_object)) == NULL ||
        (pSortdata      = (float*) malloc(sizeof(float) * pField->n_object)) == NULL  )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }
  else
  {
    fprintf(stdout, "%s There were no objects to observe in field: %s\n", WARNING_PREFIX, pField->name);
    fflush(stdout);
    return 0; //there were no objects to observe
  }

  //reset the positioner attributes, and move them back to their base positions
  if ( OpSim_positioner_reset_fibers((focalplane_struct*) pFocal )) return 1;

  //determine moon phase - depends on how the moon phase/timeline was supplied
  if ( pMoon != NULL ) //this is for a non-timeline survey or where the moon phase is supplied
  {
    pMoonLocal = (moon_struct*) pMoon;
//    moon_phase_code = pMoonLocal->phase;
//    switch ( moon_phase_code )
//    {
//    case MOON_PHASE_BRIGHT : moon_group_code = MOON_GROUP_BB; break;
//    case MOON_PHASE_DARK   :
//    case MOON_PHASE_GREY   : moon_group_code = MOON_GROUP_DG; break;
//    default : return 1; break;
//    }
    moon_group_code = pMoonLocal->group_code;
    switch ( moon_group_code )
    {
    case MOON_GROUP_BB : moon_phase_code = MOON_PHASE_BRIGHT; break;
    case MOON_GROUP_DG : moon_phase_code = MOON_PHASE_DARK; break;
    default : return 1; break;
    }
   
  }
  else //this is for a timeline survey where pMoon is not supplied
  {
    if ( timeline_determine_moon_phase_code((timeline_struct*) pTime,
                                            (float*) NULL,
                                            (int*) &moon_phase_code,
                                            (int*) &moon_group_code,
                                            (survey_struct*) pSurvey,
                                            (moon_struct**) &pMoonLocal))
    {
      fprintf(stderr, "%s Problem calculating moon_phase_code\n", ERROR_PREFIX);
      return 1;
    }
  }

  //this counts as an observation with a tile, so increment counter
  tile_mask = (uint) (((uint)1) << pField->ntiles_total ); 
  pField->ntiles_total ++;
  pField->ntiles_done[moon_phase_code] ++;
  pField->ntiles_per_dither[dither_index] ++;
  //  if ( moon_group_code == MOON_PHASE_DARKGREY &&
  //       moon_phase_code != MOON_PHASE_DARKGREY )
  //    pField->ntiles_done[MOON_PHASE_DARKGREY] ++;
  //
  pField->texp_total_done += texp_per_tile_mins;

  //select a shortlist of candidate targets, and calc their relative weightings
  ncand = 0;
  for(i=0;i<pField->n_object;i++)
  {
    int flag;
    float weight = 0.0;
    object_struct *pObj = (object_struct*) &(pObjList->pObject[pField->pid[i]]);
    if ( (flag = calc_object_weight ((survey_struct*) pSurvey, (object_struct*) pObj, (field_struct*) pField,
                                     (int) moon_phase_code, (float*) &weight )) < 0 )
    {
      return 1;
    }
    if ( flag == 0 ) //if this is a valid object
    {
      pSortindex[ncand] = (int) pObj->id;
      pSortdata[ncand] = weight;
      pObjFiberindex[ncand] = i;
      ncand++;
      ncandidates[pObj->res] ++;
    }
  }

  //sort the arrays - note this is is in ascending order,
  //whereas we want to use objects with the highest priority first
  if ( util_fiisort((ulong) ncand,
                    (float*) pSortdata,
                    (int*) pSortindex,
                    (int*) pObjFiberindex))
  {
    fprintf(stderr, "%s Error sorting data at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  if ( util_shuffle_matches_2arrays ((unsigned long) ncand,
                                     (size_t) sizeof(float),
                                     (void*) pSortdata,
                                     (size_t) sizeof(int),
                                     (void*) pSortindex,
                                     (size_t) sizeof(int),
                                     (void*) pObjFiberindex))
  {
    fprintf(stderr, "%s Error shuffling data at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }

  //initialise the numbers of hi and lo res fibers that remain
  nfibers_remain[RESOLUTION_CODE_LOW]  = pFocal->num_fibers_lo ; 
  nfibers_remain[RESOLUTION_CODE_HIGH] = pFocal->num_fibers_hi ; 
  nfibers_remain[RESOLUTION_CODE_DUAL] = pFocal->num_fibers_dual; 
  
  //and that are reserved for sky etc
  nfibers_reserved[RESOLUTION_CODE_LOW]  = pSurvey->minimum_sky_fibers_low_res + pSurvey->minimum_PI_fibers_low_res; 
  nfibers_reserved[RESOLUTION_CODE_HIGH] = pSurvey->minimum_sky_fibers_high_res + pSurvey->minimum_PI_fibers_high_res; 

  //do same on a sector-by-sector basis
  for ( i=0;i<pFocal->num_sectors;i++)
  {
    nfibers_remain_per_sector[RESOLUTION_CODE_LOW *MAX_SECTORS_PER_TILE + i]  = pFocal->Sector[i].num_fibers_per_res[RESOLUTION_CODE_LOW]; 
    nfibers_remain_per_sector[RESOLUTION_CODE_HIGH*MAX_SECTORS_PER_TILE + i]  = pFocal->Sector[i].num_fibers_per_res[RESOLUTION_CODE_HIGH]; 
    nfibers_remain_per_sector[RESOLUTION_CODE_DUAL*MAX_SECTORS_PER_TILE + i]  = pFocal->Sector[i].num_fibers_per_res[RESOLUTION_CODE_DUAL]; 
  }

  
  //now carry out the assignment
  //step through the list of objects one by one,
  //starting with the highest priority object
  for(i=ncand-1;i>0;i--)
  {
    int flag = 0;
    object_struct *pObj = (object_struct*) &(pObjList->pObject[pSortindex[i]]);
    fiber_struct *pAssignedFib = NULL;
    if ( nfibers_remain[pObj->res] + nfibers_remain[RESOLUTION_CODE_DUAL] <= nfibers_reserved[pObj->res] ) continue;

    if ( (flag = assign_a_fiber_to_object ((focalplane_struct*) pFocal,
                                           (field_struct*) pField,
                                           (object_struct*) pObj,
                                           (objectfiber_struct*) pObjFib,
                                           (int) pObjFiberindex[i],
                                           (fiber_struct**) &pAssignedFib,
                                           (int) dither_index,
                                           (int*) nfibers_remain_per_sector)) < 0 )
    {
      fprintf(stderr, "%s Error assigning a fiber to object at file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1; //this is an error condition
    }

    pObj->was_observed_last_tile = (ulong) 0;   //reset for any targets that were not observed 
    if ( flag == (int) 0 )  //we have successfully assigned a fiber to the object,
    {
      float old_TexpFracDone = (float) pObj->TexpFracDone;
      float fiber_throughput;
      int field_sub_index;
      if ( (field_sub_index = get_field_sub_index ((object_struct*) pObj, (field_struct*) pField )) < 0 )
      {
        fprintf(stderr, "%s Error finding field_sub_index at file %s line %d\n",
                ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }
      /////////////////////////////////////////////////////////////////////////////////
      /////////these variables get set when a fiber is assigned to a target ///////////
      /////////////////////////////////////////////////////////////////////////////////
      //local counters:
      nassigned[pObj->res] ++;
      nfibers_remain[pAssignedFib->res] --; //take away one fiber from the tile's pool
      nfibers_remain_per_sector[pAssignedFib->res*MAX_SECTORS_PER_TILE + pAssignedFib->sector] --; //take away one fiber from the sector's pool

      /////////////////////////////////
      //variables that are set for the fiber:
      pAssignedFib->assign_flag = FIBER_ASSIGN_CODE_TARGET;
      pAssignedFib->pObj = (object_struct*) pObj;
      pAssignedFib->assignment_order =
        nassigned[RESOLUTION_CODE_LOW] +
        nassigned[RESOLUTION_CODE_HIGH] +
        nassigned[RESOLUTION_CODE_DUAL] ;
      /////////////////////////////////
      //variables that are set for the object:
      //      pObj->pntiles[field_sub_index] ++; 
      pObj->pObjField[field_sub_index].ntiles ++; 
      //      pObj->pwas_observed[field_sub_index] = ((ulong) pObj->pwas_observed[field_sub_index] | (ulong) tile_mask) ;
      pObj->was_observed_last_tile = (ulong) tile_mask;
   
      //      pObj->ptexp[moon_phase_code][field_sub_index] += (float) texp_per_tile_mins; 
      pObj->pObjField[field_sub_index].ptexp[moon_phase_code] += (float) texp_per_tile_mins; 

      //work out the throughput of the fiber x tile
      fiber_throughput = tile_throughput * OpSim_positioner_calc_relative_throughput_fiber ((focalplane_struct*) pFocal,
                                                                                            (fiber_struct*) pAssignedFib );
      if ( fiber_throughput > 0.0 )
      {
        //work out the contribution to the requested exposure time
        if ( pObj->treq[moon_phase_code] < MAX_EXPOSURE_TIME )
        {
          if ( pObj->treq[moon_phase_code] > 0.0 )
          {
            pObj->TexpFracDone += (float) (texp_per_tile_mins * fiber_throughput / pObj->treq[moon_phase_code]);
          }
          else pObj->TexpFracDone = pSurvey->maximum_texpfracdone_to_consider;  //this object is now considered complete
        }
        ////////////////////////////////////////////////////////
        //if the object has become completed in this tile then decrement the object's weight from the field total
        if ( pObj->TexpFracDone >= 1.0 && old_TexpFracDone < 1.0 )
        {
          //          if ( pObj->pCat->code != CATALOGUE_CODE_COSMOLOGY ) //BAO does not contribute to summed priority
          {
            int f;
            float priority_share_per_field = pObj->priority/(float)MAX(1,pObj->num_fields);
            for(f=0;f<pObj->num_fields;f++)
            {
              //              field_struct *pF = (field_struct*) &(pFieldList->pF[pObj->pfield_ids[f]]);
              field_struct *pF = (field_struct*) &(pFieldList->pF[pObj->pObjField[f].field_id]);
              pF->object_priority_remaining          -= pObj->priority;
              pF->object_priority_remaining_weighted -= priority_share_per_field;
            }
          }
        }
      }
      ///////////////////////////////////////////////////////
      
      //check if we have totally run out of fibers in this Tile - if true then stop making assignments
      if ( nfibers_remain[RESOLUTION_CODE_LOW] + nfibers_remain[RESOLUTION_CODE_DUAL]
           <= nfibers_reserved[RESOLUTION_CODE_LOW] &&
         nfibers_remain[RESOLUTION_CODE_HIGH] + nfibers_remain[RESOLUTION_CODE_DUAL]
           <= nfibers_reserved[RESOLUTION_CODE_HIGH]  )
      {
        break;
      }
    } //if we assigned a fiber to the object
  } //loop around all candidates


  //assign some fibers to be sky fibers
  //we want the sky fibers to be allocated throughout the fov
  //require a minimum number per sector and a minimum number in total
  if ( OpSim_positioner_assign_fibers_to_be_sky_fibers ((focalplane_struct*) pFocal, (survey_struct*) pSurvey ))
  {
    fprintf(stderr, "%s Problem assigning sky fibers\n", ERROR_PREFIX);
    return 1;
  }

  //make a note of the latest ditehr position that was used
  pField->last_dither_index = dither_index;

  //record some stats for the fibers
  if ( OpSim_positioner_record_fiber_assignment_stats ((focalplane_struct*) pFocal )) return 1;
  
  //report a full listing of the fiber assignment
  if ( pSurvey->write_fiber_assignments &&
       pSurvey->num_fiber_assignment_files_written < pSurvey->max_fiber_assignment_files)
  {
    char str_filename[STR_MAX];
    pSurvey->num_fiber_assignment_files_written ++;
    snprintf(str_filename, STR_MAX, "%s/%s_f%s_t%02d.txt",
             FIBER_ASSIGNMENT_SUBDIRECTORY, FIBER_ASSIGNMENT_STEM, pField->name,  pField->ntiles_total);
    if (write_fiber_assignment_for_tile((const char*) str_filename, (field_struct*) pField, (focalplane_struct*) pFocal,
                                        (objectlist_struct*) pObjList, pField->ntiles_total-1,(int) dither_index))
    {
      return 1;
    }

    snprintf(str_filename, STR_MAX, "%s/%s_f%s_t%02d.txt",
             FIBER_ASSIGNMENT_SUBDIRECTORY, TARGET_DETAILS_STEM, pField->name,  pField->ntiles_total);
    if (write_object_catalogue_for_tile((const char*) str_filename, (field_struct*) pField, (focalplane_struct*) pFocal,
                                        (objectlist_struct*) pObjList, pField->ntiles_total-1,(int) dither_index))
    {
      return 1;
    }
    
  }

  //add the fiber positions to the fiber offset histogram structure
  if ( OpSim_positioner_add_fibers_to_offset_histos ( (focalplane_struct*) pFocal ))  return 1;

  //report one line summary of the results to the logfile
  //bad_neighbours, problem_neighbours
  //  if ( pSurvey->write_fiber_assignments && pSurvey->pPerTileLog )
  if ( pSurvey->pPerTileLog )
  {
    int num_targets_with_problem_neighbours_low_res;
    int num_targets_with_problem_neighbours_high_res;

    ////////////////////////////////////////////////////////////////////////
    //calculate the number of faint objects that have bad neighbours (i.e. very bright objects)
    if ( calc_num_targets_with_bright_neighbours ( (focalplane_struct*) pFocal ,
                                                   (survey_struct*) pSurvey,
                                                   (int*) &num_targets_with_problem_neighbours_low_res,
                                                   (int*) &num_targets_with_problem_neighbours_high_res))
    {
      return 1;
    }
    ///////////////////////////////////////////////

    
    //more compact format, easier to plot, needs a title line tho (add one per night) 
    fprintf(pSurvey->pPerTileLog,
            "%-10.5f %6s %8.4f %+8.4f %5d %5d %2s %7.2f %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d %6d\n",
            (pTime ? pTime->JD - pSurvey->JD_start : NAN),
            pField->name,
            pField->pra0[dither_index],pField->pdec0[dither_index], pField->ntiles_total,
            pField->ntiles_todo[MOON_GROUP_BB]+pField->ntiles_todo[MOON_GROUP_DG],
            (char*) pMoonLocal->codename, texp_per_tile_mins,
            ncandidates[RESOLUTION_CODE_HIGH],
            ncandidates[RESOLUTION_CODE_LOW],
            MAX(0,pFocal->num_fibers_hi - nfibers_reserved[RESOLUTION_CODE_HIGH]), 
            MAX(0,pFocal->num_fibers_lo - nfibers_reserved[RESOLUTION_CODE_LOW]), 
            nassigned[RESOLUTION_CODE_HIGH],
            nassigned[RESOLUTION_CODE_LOW],
            num_targets_with_problem_neighbours_high_res,
            num_targets_with_problem_neighbours_low_res,
            pSurvey->minimum_PI_fibers_high_res,
            pSurvey->minimum_PI_fibers_low_res,
            pSurvey->minimum_sky_fibers_high_res,
            pSurvey->minimum_sky_fibers_low_res);

    //write the same info to the fits tile log file, without incrementing row
    {
      fits_table_helper_struct *pT = (fits_table_helper_struct*) &(pSurvey->TileLog); //short alias
      int j;
      int nfib_avail_high_res  = (int) MAX(0,pFocal->num_fibers_hi - nfibers_reserved[RESOLUTION_CODE_HIGH]);
      int nfib_avail_low_res   = (int) MAX(0,pFocal->num_fibers_lo - nfibers_reserved[RESOLUTION_CODE_LOW]);
      int nunassigned_high_res = (int) nfib_avail_high_res - nassigned[RESOLUTION_CODE_HIGH];
      int nunassigned_low_res  = (int) nfib_avail_low_res  - nassigned[RESOLUTION_CODE_LOW];
      for(j=0;j<pT->ncols;j++) pT->pvoid[j] = NULL;
      
      j=43; pT->pvoid[j] = (void*) (int*) &(ncandidates[RESOLUTION_CODE_HIGH]);
      j=44; pT->pvoid[j] = (void*) (int*) &(ncandidates[RESOLUTION_CODE_LOW]);
      j=45; pT->pvoid[j] = (void*) (int*) &(nfib_avail_high_res);
      j=46; pT->pvoid[j] = (void*) (int*) &(nfib_avail_low_res);
      j=47; pT->pvoid[j] = (void*) (int*) &(pSurvey->minimum_sky_fibers_high_res);
      j=48; pT->pvoid[j] = (void*) (int*) &(pSurvey->minimum_sky_fibers_low_res);
      j=49; pT->pvoid[j] = (void*) (int*) &(pSurvey->minimum_PI_fibers_high_res);
      j=50; pT->pvoid[j] = (void*) (int*) &(pSurvey->minimum_PI_fibers_low_res);
      j=51; pT->pvoid[j] = (void*) (int*) &(nassigned[RESOLUTION_CODE_HIGH]);
      j=52; pT->pvoid[j] = (void*) (int*) &(nassigned[RESOLUTION_CODE_LOW]);
      j=53; pT->pvoid[j] = (void*) (int*) &(nunassigned_high_res);
      j=54; pT->pvoid[j] = (void*) (int*) &(nunassigned_low_res);
      j=55; pT->pvoid[j] = (void*) (int*) &(num_targets_with_problem_neighbours_high_res);
      j=56; pT->pvoid[j] = (void*) (int*) &(num_targets_with_problem_neighbours_low_res);
      //write row but do not increment row counter
      if ( fths_write_row ((fits_table_helper_struct*) pT,  (BOOL) FALSE))  return 1;
      
    }
    
  }

  //write a fiber position file if required
  if ( pSurvey->write_fiber_positions &&
       pSurvey->num_fiber_position_files_written < pSurvey->max_fiber_position_files )
  {
    char str_filename[STR_MAX];
    pSurvey->num_fiber_position_files_written ++;
    snprintf(str_filename, STR_MAX, "%s/%s_f%s_t%02d.txt",
             FIBER_ASSIGNMENT_SUBDIRECTORY, FIBER_POSITIONS_STEM, pField->name, pField->ntiles_total);
    if ( OpSim_positioner_write_instantaneous_positioners_geometry((const char*) str_filename, (focalplane_struct*) pFocal))
    {
      fprintf(stderr, "%s Error in write_instantaneous_positioners_geometry() called from file %s line %d\n",
              ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }
  }

  //find out which fibers have failed etc
  if ( OpSim_positioner_check_for_fiber_failures ((focalplane_struct*) pFocal ))
  {
    return 1;
  }
  
  //free the arrays used in this function
  if ( pSortindex)     free(pSortindex);     pSortindex = NULL;
  if ( pSortdata)      free(pSortdata);      pSortdata  = NULL;
  if ( pObjFiberindex) free(pObjFiberindex); pObjFiberindex  = NULL;
  return 0;
}
///////////////////////////////////////////////////////////////////////////////



//write nulls to floating point columns in pSurvey->TileLog and increment row
int write_nulls_to_tilelog ( fits_table_helper_struct *pT, survey_struct* pSurvey, focalplane_struct* pFocal )
{
  int j;
  const char buffer[STR_MAX] = {'-', '\0'};  
  const char  *pcnull[1] = {(const char*) buffer};
  //  const char  *pcnull[1] = {(const char*) "-"};
  //  const char  *pcnull[1] = {(const char*) "-               "};
  const float  fnull     = (float) NAN; 
  const double dnull     = (double) NAN;
  const int    jzero     = (int) 0;
  int nfib_avail_high_res  = (int) MAX(0,pFocal->num_fibers_hi - pSurvey->minimum_sky_fibers_high_res - pSurvey->minimum_PI_fibers_high_res);
  int nfib_avail_low_res   = (int) MAX(0,pFocal->num_fibers_lo - pSurvey->minimum_sky_fibers_low_res  - pSurvey->minimum_PI_fibers_low_res);
  if ( pT == NULL || pSurvey == NULL || pFocal == NULL) return 1;

  for(j=0;j<pT->ncols;j++) pT->pvoid[j] = (void*) NULL;
  j=21; pT->pvoid[j] = (void*) (char**) (pcnull);  //DEBUG
  j=22; pT->pvoid[j] = (void*) (double*) &(dnull);
  j=23; pT->pvoid[j] = (void*) (double*) &(dnull);
  j=24; pT->pvoid[j] = (void*) (double*) &(dnull);
  j=25; pT->pvoid[j] = (void*) (double*) &(dnull);
  j=26; pT->pvoid[j] = (void*) (double*) &(dnull);
  j=27; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=28; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=29; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=30; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=31; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=32; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=33; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=34; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=35; pT->pvoid[j] = (void*) (int*)    &(jzero);
  j=36; pT->pvoid[j] = (void*) (int*)    &(jzero);
  j=37; pT->pvoid[j] = (void*) (int*)    &(jzero);
  j=38; pT->pvoid[j] = (void*) (int*)    &(jzero);
  j=39; pT->pvoid[j] = (void*) (int*)    &(jzero);
  j=40; pT->pvoid[j] = (void*) (int*)    &(jzero);
  j=41; pT->pvoid[j] = (void*) (float*)  &(fnull);
  j=42; pT->pvoid[j] = (void*) (float*)  &(fnull);

  j=45; pT->pvoid[j] = (void*) (int*) &(nfib_avail_high_res);
  j=46; pT->pvoid[j] = (void*) (int*) &(nfib_avail_low_res);

  //now write the relevant columns and increment the row counter
  if ( fths_write_row ((fits_table_helper_struct*) pT,  (BOOL) TRUE)) return 1;

  return 0;
}








///////////////////////////////////////////////////////////////////////////////
//this will perform observations of ntiles on one field
//all tiles are executed at the same dither position
int observe_tiles_on_a_field (timeline_struct   *pTime,
                              moon_struct       *pMoon,
                              focalplane_struct *pFocal,
                              fieldlist_struct  *pFieldList,
                              field_struct      *pField,
                              objectlist_struct *pObjList,
                              survey_struct     *pSurvey,
                              int                ntiles,
                              float              texp_per_tile_mins)
{
  int t;
  objectfiber_struct ObjFib;
  int dither_index;
 
  if ( pFocal     == NULL  ||
       pFieldList == NULL  ||
       pField    == NULL  ||
       pObjList  == NULL  ||
       pSurvey   == NULL   ) return 1;
  if ( pTime == NULL && pMoon == NULL ) return 1; //need one of these to be valid
  if ( ntiles < 1 ) return 1;

  //initialise and make some space for the objectfiber_struct members - allocate more than we will actually need.
  ObjFib.nobjects = (int) pField->n_object;
  //  ObjFib.pFibers = NULL;
  ObjFib.pfib_ids = NULL;
  ObjFib.pnumfibs  = NULL;
  ObjFib.psqdist = NULL;
  //  if ((ObjFib.pFibers = (fiber_struct**) calloc(ObjFib.nobjects*MAX_FIBERS_PER_OBJECT, sizeof(fiber_struct*))) == NULL ||
  if ((ObjFib.pfib_ids = (int*)   calloc((size_t) ObjFib.nobjects*MAX_FIBERS_PER_OBJECT, sizeof(int)))  == NULL ||
      (ObjFib.pnumfibs = (int*)   calloc((size_t) ObjFib.nobjects,                       sizeof(int)))  == NULL ||
      (ObjFib.psqdist  = (float*) calloc((size_t) ObjFib.nobjects*MAX_FIBERS_PER_OBJECT, sizeof(float))) == NULL)
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  
  //  //choose a dither position to use.
  //  //can just do it randomly
  //  dither_index = (int) floor(pField->num_dithers * drand48());
  //  dither_index = MIN(dither_index,pField->num_dithers-1); //double check that we will not go past end of buffer

  //observe a dither position that has the smallest number of previous dithers
  //there may be more than one of these so we want to choose one randomly
  {
    int i;
    int dither_index_list[MAX_DITHERS_PER_FIELD];
    int min_tiles = INT_MAX;
    int nwith_min = 0;
    for(i=0;i<pField->num_dithers;i++)
      if (pField->ntiles_per_dither[i] < min_tiles ) min_tiles = pField->ntiles_per_dither[i];

    for(i=0;i<pField->num_dithers;i++)
    {
      if ( pField->ntiles_per_dither[i] == min_tiles )
      {
        dither_index_list[nwith_min] = i;
        nwith_min++;
      }
    }
    //randomly choose one of these dither positions to use.
    i = (int) (random() % nwith_min);
    dither_index = dither_index_list[i];
  }
  
  //work out which objects can be seen by which fibers
  //this needs to be done once per N tiles on a field
  //makes up a significant portion of the total work load
  if ( associate_objects_with_fibers ( (objectlist_struct*) pObjList,
                                       (field_struct*) pField,
                                       (focalplane_struct*) pFocal,
                                       (objectfiber_struct*) &ObjFib,
                                       (int) dither_index))
  {
    return 1;
  }

  
  
  for(t=0;t<ntiles;t++)
  {
    if ( observe_tile_on_a_field ((timeline_struct*)    pTime,
                                  (moon_struct*)        pMoon,
                                  (focalplane_struct*)  pFocal,
                                  (fieldlist_struct*)   pFieldList,
                                  (field_struct*)       pField,
                                  (objectlist_struct*)  pObjList,
                                  (survey_struct*)      pSurvey,
                                  (objectfiber_struct*) &ObjFib,
                                  (float)               texp_per_tile_mins,
                                  (int)                 dither_index))
    {
      return 1;
    }
  }
  //  if ( ObjFib.pFibers) free(ObjFib.pFibers); ObjFib.pFibers = NULL;
  if ( ObjFib.pfib_ids) free(ObjFib.pfib_ids); ObjFib.pfib_ids = NULL;
  if ( ObjFib.pnumfibs) free(ObjFib.pnumfibs); ObjFib.pnumfibs  = NULL;
  if ( ObjFib.psqdist) free(ObjFib.psqdist); ObjFib.psqdist = NULL;
  return 0;
}
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
int objectlist_struct_init (objectlist_struct **ppObjList)
{
  if ( ppObjList == NULL ) return 1; //paranoid debug check
  if ( *ppObjList == NULL )
  {
    //make space for the struct
    if ( (*ppObjList = (objectlist_struct*) malloc(sizeof(objectlist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }

  (*ppObjList)->nObjects = 0;
  (*ppObjList)->pObject = NULL;
  (*ppObjList)->array_length = 0;
  (*ppObjList)->max_num_fields_per_object = 0;
  return 0;
}

int objectlist_struct_free (objectlist_struct **ppObjList)
{
  if ( ppObjList == NULL) return 1;
  if ( *ppObjList )
  {
    //free the contents of the object list
    if ((*ppObjList)->pObject)
    {
      long i;
      for(i=0;i<(*ppObjList)->nObjects;i++)  //free up the memory allocated for each object
      {
        if ( (*ppObjList)->pObject[i].pObjField ) free ((*ppObjList)->pObject[i].pObjField);
        (*ppObjList)->pObject[i].pObjField = NULL;
      }

      free((*ppObjList)->pObject);
      (*ppObjList)->pObject = NULL;
    }
    //free the objectlist structure itself
    free(*ppObjList);
      *ppObjList = NULL;
  }
  return 0;
}

int focalplane_struct_init (focalplane_struct **ppFocal)
{
  int i;
  focalplane_struct *pFocal = NULL;
  if ( ppFocal == NULL ) return 1;
  if ( *ppFocal == NULL )
  {
    //make space for the struct
    if ( (*ppFocal = (focalplane_struct*) malloc(sizeof(focalplane_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  
  pFocal = (focalplane_struct*) *ppFocal; //use a local pointer

  //init the data values
  


  ////init everything
  pFocal->positioner_family = POSITIONER_FAMILY_UNKNOWN;
  strncpy(pFocal->code_name, "", STR_MAX);
  strncpy(pFocal->positioner_layout_filename, "", STR_MAX);
  pFocal->plate_scale = NAN;   
  pFocal->invplate_scale = NAN;

  strncpy(pFocal->str_layout_shape, "", STR_MAX);
  pFocal->layout_shape_code = GEOM_SHAPE_NONE;  
  
  pFocal->coordinate_tweak_factor = NAN; 
  pFocal->positioner_spacing = NAN;
  pFocal->positioner_hilo_ratio = 0;
  pFocal->positioner_hilo_pattern = 0; 
  strncpy(pFocal->str_positioner_hilo_pattern, "", STR_MAX);

  pFocal->num_fibers = 0;
  pFocal->num_fibers_lo = 0;
  pFocal->num_fibers_hi = 0;
  pFocal->num_fibers_really_lo = 0;
  pFocal->num_fibers_really_hi = 0;
  pFocal->num_fibers_dual = 0;
  pFocal->num_fibers_switchable = 0;
  pFocal->num_fibers_empty = 0;

  pFocal->pFib = NULL;

  pFocal->min_sep_mm = NAN;            
  pFocal->sq_min_sep_mm = NAN;         
  pFocal->patrol_radius_max_mm = NAN;  
  pFocal->patrol_radius_min_mm = NAN;  
  pFocal->sq_patrol_radius_max_mm = NAN;
  pFocal->sq_patrol_radius_min_mm = NAN;

  pFocal->patrol_radius_max_deg = NAN;  
  pFocal->fiber_offaxis_max_mm = NAN;   
  pFocal->fiber_offaxis_max_deg = NAN;  
  pFocal->sq_fiber_offaxis_max_mm = NAN;
  
  pFocal->num_vertex = 0; 
  pFocal->num_ext_vertex = 0; 
  for(i=0;i<MAX_VERTICES_PER_TILE;i++)
  {
    pFocal->vertex_x[i] = NAN;
    pFocal->vertex_y[i] = NAN;
    pFocal->ext_vertex_x[i] = NAN;
    pFocal->ext_vertex_y[i] = NAN;
  }

  pFocal->gp_ellipse_fs_lo = NAN;
  pFocal->gp_ellipse_fs_hi = NAN;
  pFocal->sq_min_object_sep_mm = NAN;
  
  pFocal->potzpoz_width = NAN;
  pFocal->potzpoz_tip_radius = NAN;
  pFocal->potzpoz_pivot_offset = NAN;
  pFocal->potzpoz_rectangle_length = NAN;
  pFocal->potzpoz_half_width = NAN;
  pFocal->mupoz_tip_width = NAN;
  pFocal->mupoz_tip_radius = NAN;
  pFocal->mupoz_arm_width = NAN;
  pFocal->mupoz_arm_length = NAN;
  pFocal->mupoz_half_patrol_radius = NAN; 
  pFocal->mupoz_half_tip_width = NAN;     
  pFocal->mupoz_half_arm_width = NAN;    
  pFocal->mupoz_arm_start = NAN;         
  pFocal->mupoz_sq_min_dist_between_axis2 = NAN; 
  pFocal->spine_diameter = NAN;
  pFocal->spine_length = NAN;
  pFocal->spine_radius = NAN; 
  pFocal->old_mupoz_tip_width = NAN;
  pFocal->old_mupoz_tip_dist = NAN;
  pFocal->starbug_radius = NAN;

  //  pFocal->Quad;  //init?

  //basic init of the the 2D histo struct(s)
  if ( twoDhisto_struct_init ((twoDhisto_struct*) &(pFocal->fiberXY_histo_lores)))  return 1;
  if ( twoDhisto_struct_init ((twoDhisto_struct*) &(pFocal->fiberXY_histo_hires)))  return 1;
  if ( twoDhisto_struct_init ((twoDhisto_struct*) &(pFocal->fiberXY_histo_swres)))  return 1;

  pFocal->number_of_spectrographs_hires = 0;   
  pFocal->number_of_spectrographs_lores = 0;
  pFocal->number_of_spectrographs_swres = 0;
  pFocal->number_of_detectors_per_spectrograph_hires = 0;
  pFocal->number_of_detectors_per_spectrograph_lores = 0;
  pFocal->number_of_detectors_per_spectrograph_swres = 0;
  pFocal->crossdisp_pixels_per_spectrograph_hires = 0;
  pFocal->crossdisp_pixels_per_spectrograph_lores = 0;
  pFocal->crossdisp_pixels_per_spectrograph_swres = 0;
  pFocal->crossdisp_pixels_per_fiber_hires = NAN;  
  pFocal->crossdisp_pixels_per_fiber_lores = NAN;
  pFocal->crossdisp_pixels_per_fiber_swres = NAN;

  pFocal->num_sectors = 0;
  for(i=0;i<NRESOLUTION_CODES;i++) pFocal->num_fibers_reserved_per_sector[i] = 0;
  //for(i=0;i<MAX_SECTORS_PER_TILE;i++) pFocal->Sector[MAX_SECTORS_PER_TILE];  //init?

  pFocal->flag_sectors_supplied_in_input_file = FALSE;
  pFocal->flag_edge_flags_supplied_in_input_file = FALSE;

  return 0;
}

int focalplane_struct_free (focalplane_struct **ppFocal)
{
  int i;
  if ( ppFocal == NULL ) return 1;
  if ( *ppFocal )
  {
    //free the fibers array
    if ( (*ppFocal)->pFib ) free((*ppFocal)->pFib);
    (*ppFocal)->pFib = NULL;

    //free up the quadtree structures
    quad_free_quad((quad_struct*) &((*ppFocal)->Quad));

    //free the sectors
    for ( i=0; i<(*ppFocal)->num_sectors; i++)
    {
      if ( (*ppFocal)->Sector[i].ppFib )
      {
        free ((*ppFocal)->Sector[i].ppFib);
        (*ppFocal)->Sector[i].ppFib = NULL;
      }
    }
    
    
    //now free the focal plane struct itself
    free(*ppFocal);
    *ppFocal = NULL;
  }
  return 0;
}

/////////////////////////////////////////
int survey_struct_init (survey_struct **ppSurvey)
{
  if ( ppSurvey == NULL ) return 1;
  if ( *ppSurvey == NULL )
  {
    //make space for the struct
    //    if ( (*ppSurvey = (survey_struct*) malloc(sizeof(survey_struct))) == NULL )
    if ( (*ppSurvey = (survey_struct*) calloc((size_t) 1, (size_t) sizeof(survey_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
//  pSurvey = (survey_struct*) *ppSurvey;
//  
//  //init the data values
//  pSurvey->pmoon_bb = NULL;
//  pSurvey->pmoon_dg = NULL;
//
//  pSurvey->num_fiber_position_files_written = 0;
//  pSurvey->num_fiber_assignment_files_written = 0;
//  pSurvey->num_field_ranking_files_written = 0;
//
//  pSurvey->pClusterList       = NULL;
//
//  pSurvey->pSlots_each_night_dg = NULL;
//  pSurvey->pSlots_each_night_bb = NULL;
//
//  pSurvey->pTileReports       = NULL;
//  pSurvey->pNightReports      = NULL;
//  pSurvey->pTimelineReports   = NULL;
//  pSurvey->pPerTileLog        = NULL;
  if ( fths_init ((fits_table_helper_struct*) &((*ppSurvey)->TileLog))) return 1;

  //  pSurvey->debug   = FALSE;
  //  pSurvey->clobber = FALSE;

  //  pSurvey->healpix_nside = (long) 64; //DEBUG - make this user definable

  return 0;
}

int survey_struct_free (survey_struct **ppSurvey)
{
  if ( ppSurvey == NULL ) return 1;
  if ( *ppSurvey )
  {
    survey_struct *pSurvey = (survey_struct*) *ppSurvey;
    if ( pSurvey->pSlots_each_night_dg ) free ( pSurvey->pSlots_each_night_dg) ; pSurvey->pSlots_each_night_dg = NULL;
    if ( pSurvey->pSlots_each_night_bb ) free ( pSurvey->pSlots_each_night_bb) ; pSurvey->pSlots_each_night_bb = NULL;
    clusterlist_struct_free ((clusterlist_struct**) &(pSurvey->pClusterList));

    //now free the survey struct itself
    free(*ppSurvey);
    *ppSurvey = NULL;
  }
  return 0;
}

int cataloguelist_struct_init (cataloguelist_struct **ppCatalogueList)
{
  if ( ppCatalogueList == NULL ) return 1;
  if ( *ppCatalogueList == NULL )
  {
    //make space for the struct
    if ( (*ppCatalogueList = (cataloguelist_struct*) malloc(sizeof(cataloguelist_struct))) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
    //init the catalogue struct
  (*ppCatalogueList)->pCat = NULL;
  (*ppCatalogueList)->num_cats             = 0;
  (*ppCatalogueList)->num_cats_hires       = 0;
  (*ppCatalogueList)->num_cats_lores       = 0;
  (*ppCatalogueList)->num_cats_exgal       = 0;
  (*ppCatalogueList)->num_cats_exgal_lores = 0;
  (*ppCatalogueList)->num_cats_exgal_hires = 0;
  (*ppCatalogueList)->num_cats_gal         = 0;
  (*ppCatalogueList)->num_cats_gal_lores   = 0;
  (*ppCatalogueList)->num_cats_gal_hires   = 0;

  {
    int m;
    for(m=0;m<MAX_NUM_MOON_PHASES;m++)
    {
      (*ppCatalogueList)->req_fhrs[m]       = (double) 0.0;
      (*ppCatalogueList)->req_fhrs_lores[m] = (double) 0.0;
      (*ppCatalogueList)->req_fhrs_hires[m] = (double) 0.0;
    }
  }

  (*ppCatalogueList)->longest_cat_filename = 0;
  (*ppCatalogueList)->longest_cat_codename = 0;
  (*ppCatalogueList)->longest_cat_displayname = 0;
  
  return 0;
}

int cataloguelist_struct_free (cataloguelist_struct **ppCatalogueList)
{
  if ( ppCatalogueList == NULL ) return 1;
  if ( *ppCatalogueList )
  {
    //free the array of catalogues
    if ((*ppCatalogueList)->pCat)
    {
      free( (*ppCatalogueList)->pCat);
      (*ppCatalogueList)->pCat = NULL;
    }
    //now free the CatalogueList struct itself
    free(*ppCatalogueList);
    *ppCatalogueList = NULL;
  }
  return 0;
 
}



int write_fiber_assignment_for_tile (const char* str_filename, field_struct *pField, focalplane_struct *pFocal, objectlist_struct *pObjList, int tile_index, int dither_index)
{
  FILE * pFile = NULL;
  int j;
  if ( str_filename == NULL ||
       pField       == NULL ||
       pFocal        == NULL ||
       pObjList     == NULL ) return 1;

  //open up the output file
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s There were problems opening the output fiber assignment file for Field %s Tile %d filename: %s\n",
            ERROR_PREFIX, pField->name, tile_index+1, str_filename);
    return 1;
  }
  fprintf(pFile,
          "#%-5s %4s %5s %5s %5s %8s %8s %10s %10s %7s %7s %6s %3s %8s %8s %4s %5s %4s %8s %8s %10s %10s %6s %5s %4s %4s %4s\n",
          "Findx", "Fres", "Fnriv", "Fnobj", "F_ord",
          "F_x0", "F_y0", "F_ra0", "F_dec0", "F_dx", "F_dy",
          "Status",
          "cat", "Olinenum", "O_index", "Ores", "Onfib", "Frnk",
          "O_x", "O_y", "O_ra", "O_dec",
          "O_prio", "O_mag", "O_td","O_tg","O_tb" );

  //now go through each fiber and report the details of the assignment
  for (j=0;j<pFocal->num_fibers;j++)
  {
    int field_sub_index;
    fiber_struct *pFib = (fiber_struct*) &(pFocal->pFib[j]);
    object_struct *pObj = NULL;
    double F_ra,F_dec;
    double x,y;

    if ( geom_project_inverse_gnomonic_from_mm ((double) pField->pra0[dither_index],
                                                (double) pField->pdec0[dither_index],
                                                (double) pField->pa0,
                                                pFib->Geom.x, pFib->Geom.y, pFocal->invplate_scale,
                                                (double*) &F_ra, (double*) &F_dec))
    {
      return 1;
    }
    
    switch ( pFib->assign_flag )
    {
    case FIBER_ASSIGN_CODE_TARGET :
      pObj = (object_struct*) pFib->pObj;
      if ( pObj == NULL ) return 1;  //this should never happpen if the FIBER_ASSIGN_CODE_TARGET is set
      //format roughly follows previous fiber assignment files
      if ( (field_sub_index = get_field_sub_index ((object_struct*) pObj, (field_struct*) pField )) < 0 )
      {
        fprintf(stderr, "%s Error finding field_sub_index at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
        return 1;
      }

      if (geom_project_gnomonic_to_mm ((double) pField->pra0[dither_index],
                                       (double) pField->pdec0[dither_index],
                                       (double) pField->pa0,
                                       (double) pObj->ra, (double) pObj->dec, pFocal->plate_scale,
                                       (double*) &x, (double*) &y ))
      {
        return 1;
      }


      fprintf(pFile, "%6d %4s %5d %5d %5d %8.3f %8.3f %10.6f %+10.6f %7.3f %7.3f %6s %3d %8u %8u %4d %5u %4d %8.3f %8.3f %10.6f %+10.6f %6.3f %5.2f %4.0f %4.0f %4.0f\n",
              pFib->id, OpSim_positioner_fiber_status_string((fiber_struct*)pFib),
              pFib->nrivals, pFib->nobjects, pFib->assignment_order,
              pFib->Geom.x0, pFib->Geom.y0, F_ra, F_dec,
              pFib->Geom.x-pFib->Geom.x0, pFib->Geom.y-pFib->Geom.y0,
              "TARGET",
              pObj->pCat->code, pObj->line_number, pObj->id, pObj->res,
              //              (uint) pObj->pnfibs[field_sub_index],
              (uint) pObj->pObjField[field_sub_index].nfibs,
              pFib->fiber_ranking,
              x,y,
              pObj->ra, pObj->dec, pObj->priority, pObj->mag,
              pObj->treq[MOON_PHASE_DARK], pObj->treq[MOON_PHASE_GREY], pObj->treq[MOON_PHASE_BRIGHT] );
      break;
    case FIBER_ASSIGN_CODE_SKY :
      fprintf(pFile, "%6d %4s %5d %5d %5d %8.3f %8.3f %10.6f %+10.6f %7.3f %7.3f %6s\n",
              pFib->id,  OpSim_positioner_fiber_status_string((fiber_struct*)pFib),
              pFib->nrivals, pFib->nobjects, pFib->assignment_order,
              pFib->Geom.x0, pFib->Geom.y0, F_ra, F_dec,
              pFib->Geom.x-pFib->Geom.x0, pFib->Geom.y-pFib->Geom.y0,
              (pFib->status==FIBER_STATUS_FLAG_NOMINAL?"SKY":"SKYSTK"));
      break;
    case FIBER_ASSIGN_CODE_NONE :
      fprintf(pFile, "%6d %4s %5d %5d %5d %8.3f %8.3f %10.6f %+10.6f %7.3f %7.3f %6s\n",
              pFib->id,  OpSim_positioner_fiber_status_string((fiber_struct*)pFib),
              pFib->nrivals, pFib->nobjects, pFib->assignment_order,
              pFib->Geom.x0, pFib->Geom.y0, F_ra, F_dec,
              pFib->Geom.x-pFib->Geom.x0, pFib->Geom.y-pFib->Geom.y0,
              (pFib->status==FIBER_STATUS_FLAG_NOMINAL?"SPARE":"BROKEN"));
      break;
    default :
      fprintf(stderr, "%s Bad assign_flag at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
      break;
    }
    
  }

  if ( fclose(pFile) ) return 1;
  return 0;
}

//write all the candidate targets for a tile to a file - mainly for debug purposes
int write_object_catalogue_for_tile (const char* str_filename, field_struct *pField, focalplane_struct *pFocal, objectlist_struct *pObjList, int tile_index, int dither_index)
{
  FILE * pFile = NULL;
  int i;
  ulong tile_flag = (ulong) 1 << tile_index;

  if ( str_filename == NULL ||
       pField       == NULL ||
       pFocal        == NULL ||
       pObjList     == NULL ) return 1;

  
  //open up the output file
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s There were problems opening the output fiber assignment file for Field %s Tile %d filename: %s\n",
            ERROR_PREFIX, pField->name, tile_index+1, str_filename);
    return 1;
  }

  //write a header here
  fprintf(pFile, "#%-4s %3s %8s %8s %4s %4s %5s %5s %8s %8s %10s %10s %6s %5s %4s %4s %4s %4s %4s %4s %7s\n",
          "Obs?",
          "cat", "Olinenum", "O_index", "Ores",
          "OnFld", "Onfib", "Ontls",
          "O_x", "O_y", "O_ra", "O_dec",
          "O_prio", "O_mag",
          "O_td","O_tg","O_tb",
          "O_xd","O_xg","O_xb", "OexpFrc");

  for (i=0;i<pField->n_object;i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[pField->pid[i]]);
    double x,y;
    int field_sub_index;
    if ( (field_sub_index = get_field_sub_index ((object_struct*) pObj, (field_struct*) pField )) < 0 )
    {
      fprintf(stderr, "%s Error finding field_sub_index at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;
    }

    if (geom_project_gnomonic_to_mm ((double) pField->pra0[dither_index],
                                     (double) pField->pdec0[dither_index],
                                     (double) pField->pa0,
                                     (double) pObj->ra, (double) pObj->dec, pFocal->plate_scale,
                                     (double*) &x, (double*) &y ))
    {
      return 1;
    }

    fprintf(pFile, "%-5s %3d %8u %8u %4d %4d %5u %5d %8.3f %8.3f %10.6f %+10.6f %6.3f %5.2f %4.0f %4.0f %4.0f %4.0f %4.0f %4.0f %7.5g\n",
            //            (pObj->pwas_observed[field_sub_index] & tile_flag) ? "YES" : "NO",
            (pObj->was_observed_last_tile & tile_flag) ? "YES" : "NO",
            pObj->pCat->code,
            pObj->line_number,
            pObj->id,
            pObj->res,
            pObj->num_fields,
            (uint) pObj->pObjField[field_sub_index].nfibs,
            (uint) pObj->pObjField[field_sub_index].ntiles,
            //            (uint) pObj->pnfibs[field_sub_index],
            //            (uint) pObj->pntiles[field_sub_index],
            x,y,
            pObj->ra, pObj->dec, pObj->priority, pObj->mag,
            pObj->treq[MOON_PHASE_DARK],
            pObj->treq[MOON_PHASE_GREY],
            pObj->treq[MOON_PHASE_BRIGHT],
            pObj->pObjField[field_sub_index].ptexp[MOON_PHASE_DARK],
            pObj->pObjField[field_sub_index].ptexp[MOON_PHASE_GREY],
            pObj->pObjField[field_sub_index].ptexp[MOON_PHASE_BRIGHT],
            //            pObj->ptexp[MOON_PHASE_DARK][field_sub_index],
            //            pObj->ptexp[MOON_PHASE_GREY][field_sub_index],
            //            pObj->ptexp[MOON_PHASE_BRIGHT][field_sub_index],
            pObj->TexpFracDone);


  }
  if ( fclose(pFile) ) return 1;
  return 0;
}






int print_full_version_information_to_file(const char* str_filename)
{
  FILE *pFile = NULL;

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  if ( print_full_version_information((FILE*) pFile))
  {
    fprintf(stderr, "%s  There were problems writing to the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
 
  fflush(pFile);
  if ( pFile ) fclose(pFile); pFile = NULL;
  return 0;
}


int print_full_version_information(FILE *pFile)
{

  if ( pFile == NULL ) return 1;
  fprintf(pFile, "%s Full software version information:\n", COMMENT_PREFIX);
  if ( FourFS_OpSim_print_version                ((FILE*) pFile)) return 1;
  if ( FourFS_OpSim_print_version_header         ((FILE*) pFile)) return 1;
  if ( OpSim_input_params_print_version          ((FILE*) pFile)) return 1;
  if ( OpSim_input_params_print_version_header   ((FILE*) pFile)) return 1;
  if ( OpSim_statistics_print_version            ((FILE*) pFile)) return 1;
  if ( OpSim_statistics_print_version_header     ((FILE*) pFile)) return 1;
  if ( OpSim_positioner_print_version            ((FILE*) pFile)) return 1;
  if ( OpSim_positioner_print_version_header     ((FILE*) pFile)) return 1;
  if ( OpSim_telescope_site_print_version        ((FILE*) pFile)) return 1;
  if ( OpSim_telescope_site_print_version_header ((FILE*) pFile)) return 1;
  if ( OpSim_html_print_version                  ((FILE*) pFile)) return 1;
  if ( OpSim_html_print_version_header           ((FILE*) pFile)) return 1;
  if ( OpSim_timeline_print_version              ((FILE*) pFile)) return 1;
  if ( OpSim_timeline_print_version_header       ((FILE*) pFile)) return 1;
  if ( define_print_version_header               ((FILE*) pFile)) return 1;
  if ( ParLib_print_version                      ((FILE*) pFile)) return 1;
  if ( ParLib_print_version_header               ((FILE*) pFile)) return 1;
  if ( util_print_version                        ((FILE*) pFile)) return 1;
  if ( util_print_version_header                 ((FILE*) pFile)) return 1;
  if ( quad_print_version                        ((FILE*) pFile)) return 1;
  if ( quad_print_version_header                 ((FILE*) pFile)) return 1;
  if ( geom_print_version                        ((FILE*) pFile)) return 1;
  if ( geom_print_version_header                 ((FILE*) pFile)) return 1;
  if ( hpx_extras_print_version                  ((FILE*) pFile)) return 1;
  if ( hpx_extras_print_version_header           ((FILE*) pFile)) return 1;
  if ( fhelp_print_version                       ((FILE*) pFile)) return 1;
  if ( fhelp_print_version_header                ((FILE*) pFile)) return 1;

  return 0;
}


int print_full_help_information(FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile, "%s Printing full help information as requested:\n", COMMENT_PREFIX);
  fflush(pFile);
  if ( OpSim_input_params_print_usage_information(pFile)) return 1;
  
  return 0;
}



//write a log with one entry per field, in FITS format
int write_per_field_logfile_fits (fieldlist_struct *pFieldList, const char* str_filename, BOOL clobber)
{
  //  const int max_cols = 64;   //remember to update this when adding new columns 
  enum {max_cols = 64};   //avoids compiler warning on clang.
                          //Remember to update this when adding new columns 
  int i;
  int status = 0;
  fitsfile *pFile = NULL;
  char *ttype[max_cols];
  char *tform[max_cols]; 
  char *tunit[max_cols];
  char *tcomm[max_cols];
  int  colfmt[max_cols];
  int  colnum[max_cols];
  int  colwidth[max_cols];
  int ncols; //this is the actual number of columns
  int j = 0;
  long k;
  long nFields_in_survey = 0;
  char str_filename_new[LSTR_MAX];

  char tf_PR_VIS_AM[STR_MAX];
  char tf_VIS_AM[STR_MAX];
  char tf_VERTEX[STR_MAX];
  if ( pFieldList == NULL ) return 1;
  if ( pFieldList->nFields <= 0 ) return 1;  // something odd going on if this is the case
 
  //update the lengths of the AM steps columns
  snprintf(tf_PR_VIS_AM, STR_MAX, "%dE", FIELD_VISIBILITY_AM_NUM_STEPS); 
  snprintf(tf_VIS_AM,    STR_MAX, "%dJ", FIELD_VISIBILITY_AM_NUM_STEPS); 
  //and the vertex vectors 
  //assume that all fields have the same number of vertices as the first field
  snprintf(tf_VERTEX,    STR_MAX, "%dD",  pFieldList->pF[0].num_vertex); 
  
  j= 0;ttype[j]="NAME"            ;tform[j]="12A"       ;tunit[j]=""         ;colfmt[j]=TSTRING;colwidth[j]=1;
  j= 1;ttype[j]="INDEX"           ;tform[j]="1J"        ;tunit[j]=""         ;colfmt[j]=TULONG ;colwidth[j]=1;
  j= 2;ttype[j]="RA"              ;tform[j]="1D"        ;tunit[j]="deg,J2000";colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 3;ttype[j]="DEC"             ;tform[j]="1D"        ;tunit[j]="deg,J2000";colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 4;ttype[j]="PA"              ;tform[j]="1E"        ;tunit[j]="deg,EofN" ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 5;ttype[j]="GAL_L"           ;tform[j]="1D"        ;tunit[j]="deg"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 6;ttype[j]="GAL_B"           ;tform[j]="1D"        ;tunit[j]="deg"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 7;ttype[j]="SURVEYCODE"      ;tform[j]="1K"        ;tunit[j]=""         ;colfmt[j]=TULONG ;colwidth[j]=1;
  j= 8;ttype[j]="NUM_DITHER"      ;tform[j]="1J"        ;tunit[j]=""         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j= 9;ttype[j]="NUM_NEIGH"       ;tform[j]="1J"        ;tunit[j]=""         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=10;ttype[j]="NEAREST_NEIGH"   ;tform[j]="12A"       ;tunit[j]=""         ;colfmt[j]=TSTRING;colwidth[j]=1;
  j=11;ttype[j]="AM_MIN"          ;tform[j]="1E"        ;tunit[j]="sec(Zmin)";colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=12;ttype[j]="NTARG_LO"        ;tform[j]="1J"        ;tunit[j]=""         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=13;ttype[j]="NTARG_HI"        ;tform[j]="1J"        ;tunit[j]=""         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=14;ttype[j]="PRIORITY_TOT"    ;tform[j]="1E"        ;tunit[j]=""         ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=15;ttype[j]="PRIORITY_REMAIN" ;tform[j]="1E"        ;tunit[j]=""         ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=16;ttype[j]="WPRIORITY_TOT"   ;tform[j]="1E"        ;tunit[j]=""         ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=17;ttype[j]="WPRIORITY_REMAIN";tform[j]="1E"        ;tunit[j]=""         ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=18;ttype[j]="TILES_PR_VIS_BB" ;tform[j]="1E"        ;tunit[j]="tiles"    ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=19;ttype[j]="TILES_PR_VIS_DG" ;tform[j]="1E"        ;tunit[j]="tiles"    ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=20;ttype[j]="TILES_WPR_VIS_BB";tform[j]="1E"        ;tunit[j]="tiles"    ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=21;ttype[j]="TILES_WPR_VIS_DG";tform[j]="1E"        ;tunit[j]="tiles"    ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=22;ttype[j]="TILES_VIS_BB"    ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=23;ttype[j]="TILES_VIS_DG"    ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=24;ttype[j]="TILES_REQ_BB"    ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=25;ttype[j]="TILES_REQ_DG"    ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=26;ttype[j]="TILES_PLAN_BB"   ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=27;ttype[j]="TILES_PLAN_DG"   ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=28;ttype[j]="TILES_DONE_BB"   ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=29;ttype[j]="TILES_DONE_DG"   ;tform[j]="1J"        ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=30;ttype[j]="TILES_FHRS_LO"   ;tform[j]="1E"        ;tunit[j]="fib-hrs"  ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=31;ttype[j]="TILES_FHRS_HI"   ;tform[j]="1E"        ;tunit[j]="fib-hrs"  ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=32;ttype[j]="PR_VIS_BB_AM"    ;tform[j]=tf_PR_VIS_AM;tunit[j]="tiles"    ;colfmt[j]=TFLOAT ;colwidth[j]=FIELD_VISIBILITY_AM_NUM_STEPS;
  j=33;ttype[j]="PR_VIS_DG_AM"    ;tform[j]=tf_PR_VIS_AM;tunit[j]="tiles"    ;colfmt[j]=TFLOAT ;colwidth[j]=FIELD_VISIBILITY_AM_NUM_STEPS;
  j=34;ttype[j]="VIS_BB_AM"       ;tform[j]=tf_VIS_AM   ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=FIELD_VISIBILITY_AM_NUM_STEPS;
  j=35;ttype[j]="VIS_DG_AM"       ;tform[j]=tf_VIS_AM   ;tunit[j]="tiles"    ;colfmt[j]=TLONG  ;colwidth[j]=FIELD_VISIBILITY_AM_NUM_STEPS;
  j=36;ttype[j]="VERTEX_RA"       ;tform[j]=tf_VERTEX   ;tunit[j]="deg,J2000";colfmt[j]=TDOUBLE;colwidth[j]=pFieldList->pF[0].num_vertex;
  j=37;ttype[j]="VERTEX_DEC"      ;tform[j]=tf_VERTEX   ;tunit[j]="deg,J2000";colfmt[j]=TDOUBLE;colwidth[j]=pFieldList->pF[0].num_vertex;
  j=38;ttype[j]="HPX_PIX"         ;tform[j]="1J"        ;tunit[j]=""         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=39;ttype[j]="TEXP_TODO"       ;tform[j]="1E"        ;tunit[j]="mins"     ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=40;ttype[j]="TEXP_DONE"       ;tform[j]="1E"        ;tunit[j]="mins"     ;colfmt[j]=TFLOAT ;colwidth[j]=1;

  ncols = j+1;
  
  //j= ; ttype[j]= ; tform[j]= ; tunit[j]= ; colfmt[j]= ;  colnum[j]= j+1;
  //j= ; ttype[j]= ; tform[j]= ; tunit[j]= ; colfmt[j]= ;  colnum[j]= j+1;

  for(j=0;j<ncols;j++) colnum[j] = j+1;
  for(j=0;j<ncols;j++) tcomm[j] = "";

  //set up the comments for each table column
  j= 0; tcomm[j] = "code name";
  j= 1; tcomm[j] = "running index";
  j= 2; tcomm[j] = "The RA coordinate at centre";
  j= 3; tcomm[j] = "The Dec coordinate at centre";
  j= 4; tcomm[j] = "The position angle (deg E of N)";
  j= 5; tcomm[j] = "The Galactic longitude at centre";
  j= 6; tcomm[j] = "The Galactic latitude at centre";
  j= 7; tcomm[j] = "mask giving sub-survey areas";
  j= 8; tcomm[j] = "No. possible dither positions";
  j= 9; tcomm[j] = "No. neighbour fields";
  j=10; tcomm[j] = "Code name for nearest neighbour";
  j=11; tcomm[j] = "Minimum airmass achievable";
  j=12; tcomm[j] = "No. candidate lo-res targets";
  j=13; tcomm[j] = "No. candidate hi-res targets";
  j=14; tcomm[j] = "Sum priority of targets";
  j=15; tcomm[j] = "Sum priority of noncompleted targets";
  j=16; tcomm[j] = "Sum weighted priority of targets";
  j=17; tcomm[j] = "Sum weighted priority of noncompleted targets";
  j=18; tcomm[j] = "Predicted visibility in bright time";
  j=19; tcomm[j] = "Predicted visibility in dark/grey time";
  j=20; tcomm[j] = "Weighted predicted visibility bright time";
  j=21; tcomm[j] = "Weighted predicted visibility dark/grey time";
  j=22; tcomm[j] = "Actual visibility bright time";
  j=23; tcomm[j] = "Actual visibility dark/grey time";
  j=24; tcomm[j] = "Requested tiles bright time";
  j=25; tcomm[j] = "Requested tiles dark/grey time";
  j=26; tcomm[j] = "Planned tiles bright time";
  j=27; tcomm[j] = "Planned tiles dark/grey time";
  j=28; tcomm[j] = "Executed tiles bright time";
  j=29; tcomm[j] = "Executed tiles dark/grey time";
  j=30; tcomm[j] = "Summed required lo-res fiber-hours";
  j=31; tcomm[j] = "Summed required hi-res fiber-hours";
  j=32; tcomm[j] = "Predicted visibility bright time, per AM";
  j=33; tcomm[j] = "Predicted visibility dark/grey time, per AM";
  j=34; tcomm[j] = "Actual visibility bright time, per AM";
  j=35; tcomm[j] = "Actual visibility dark/grey time, per AM";
  j=36; tcomm[j] = "RA coords of vertices";
  j=37; tcomm[j] = "Dec coords of vertices";
  j=38; tcomm[j] = "HEALPIX pixel index, Equatorial system";
  j=39; tcomm[j] = "Sum expected exposure time";
  j=40; tcomm[j] = "Sum executed exposure time";
  
  
  //count the number of fields actually in a survey
  for ( i=0; i< pFieldList->nFields; i++)
  {
    if ( pFieldList->pF[i].in_survey & (SURVEY_ANY | SURVEY_IN_AREA_ANY) ) nFields_in_survey ++;
  }

  //add a "!" prefix if we are going to clobber the output
  snprintf(str_filename_new, LSTR_MAX, "%s%s", CLOBBER2S(clobber), str_filename);
  
  if (fits_create_file ((fitsfile**) &pFile, str_filename_new, &status))
  {
    fits_report_error(stderr, status); // print any error messages
    fprintf(stderr, "%s  There was a problem creating the field logfile (FITS): %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  if (  fits_create_tbl ((fitsfile*) pFile,
                         (int) BINARY_TBL,
                         (LONGLONG) nFields_in_survey,
                         (int) ncols,
                         (char**) ttype,
                         (char**) tform,
                         (char**) tunit,
                         (char*) "FIELDLOG",
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  for(j=0;j<ncols;j++)
  {
    //now add some comments to the column name keywords
    char str_keyname[STR_MAX];
    snprintf(str_keyname, STR_MAX, "TTYPE%d", j+1);
    if (fits_modify_comment ((fitsfile*)pFile, (char*) str_keyname, (char*) tcomm[j], (int*) &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }


  

  //have to write file one row at a time because data is stored in array of structures  
  k = 0;
  for ( i=0; i< pFieldList->nFields; i++)
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
    if ( pF->in_survey & (SURVEY_ANY | SURVEY_IN_AREA_ANY) )
    {
      int j = 0;
      char *pstr_name[1] = {(char*) pF->name};
      char *pstr_nn_name[1] = {(char*) (pF->id_nearest_neighbour >=0 && pF->id_nearest_neighbour < pFieldList->nFields ?
                                        pFieldList->pF[pF->id_nearest_neighbour].name : "-")};
      int ntiles_done_dg = pF->ntiles_done[MOON_PHASE_GREY]+pF->ntiles_done[MOON_PHASE_DARK];
      int hpx_pix = (int) pF->hpx_pix;
      void* pvoid[ncols];
      for(j=0;j<ncols;j++) pvoid[j] = NULL;
      j= 0; pvoid[j] = (void*) &(pstr_name);
      j= 1; pvoid[j] = (void*) &(pF->id);
      j= 2; pvoid[j] = (void*) &(pF->ra0);
      j= 3; pvoid[j] = (void*) &(pF->dec0);
      j= 4; pvoid[j] = (void*) &(pF->pa0);
      j= 5; pvoid[j] = (void*) &(pF->l0);
      j= 6; pvoid[j] = (void*) &(pF->b0);
      j= 7; pvoid[j] = (void*) &(pF->in_survey);
      j= 8; pvoid[j] = (void*) &(pF->num_dithers);
      j= 9; pvoid[j] = (void*) &(pF->num_neighbours);
      j=10; pvoid[j] = (void*) &(pstr_nn_name);
      j=11; pvoid[j] = (void*) &(pF->airmass_min);
      j=12; pvoid[j] = (void*) &(pF->n_object_lores);
      j=13; pvoid[j] = (void*) &(pF->n_object_hires);
      j=14; pvoid[j] = (void*) &(pF->object_priority_total);
      j=15; pvoid[j] = (void*) &(pF->object_priority_remaining);
      j=16; pvoid[j] = (void*) &(pF->object_priority_total_weighted);
      j=17; pvoid[j] = (void*) &(pF->object_priority_remaining_weighted);
      j=18; pvoid[j] = (void*) &(pF->ntiles_visible_predict[MOON_GROUP_BB]);
      j=19; pvoid[j] = (void*) &(pF->ntiles_visible_predict[MOON_GROUP_DG]);
      j=20; pvoid[j] = (void*) &(pF->ntiles_visible_predict_weighted[MOON_GROUP_BB]);
      j=21; pvoid[j] = (void*) &(pF->ntiles_visible_predict_weighted[MOON_GROUP_DG]);
      j=22; pvoid[j] = (void*) &(pF->ntiles_visible[MOON_GROUP_BB]);
      j=23; pvoid[j] = (void*) &(pF->ntiles_visible[MOON_GROUP_DG]);
      j=24; pvoid[j] = (void*) &(pF->ntiles_req[MOON_GROUP_BB]);
      j=25; pvoid[j] = (void*) &(pF->ntiles_req[MOON_GROUP_DG]);
      j=26; pvoid[j] = (void*) &(pF->ntiles_todo[MOON_GROUP_BB]);
      j=27; pvoid[j] = (void*) &(pF->ntiles_todo[MOON_GROUP_DG]);
      j=28; pvoid[j] = (void*) &(pF->ntiles_done[MOON_PHASE_BRIGHT]);
      j=29; pvoid[j] = (void*) &(ntiles_done_dg);
      j=30; pvoid[j] = (void*) &(pF->object_dark_fhrs_lores);
      j=31; pvoid[j] = (void*) &(pF->object_dark_fhrs_hires);
      j=32; pvoid[j] = (void*) &(pF->ntiles_visible_am_predict[MOON_GROUP_BB][0]);
      j=33; pvoid[j] = (void*) &(pF->ntiles_visible_am_predict[MOON_GROUP_DG][0]);
      j=34; pvoid[j] = (void*) &(pF->ntiles_visible_am[MOON_GROUP_BB][0]);
      j=35; pvoid[j] = (void*) &(pF->ntiles_visible_am[MOON_GROUP_DG][0]);
      j=36; pvoid[j] = (void*) &(pF->vertex_ra[0][0]); //just report the first dither position
      j=37; pvoid[j] = (void*) &(pF->vertex_dec[0][0]);
      j=38; pvoid[j] = (void*) &(hpx_pix);
      j=39; pvoid[j] = (void*) &(pF->texp_total_todo);
      j=40; pvoid[j] = (void*) &(pF->texp_total_done);

      for(j=0;j<ncols;j++)
      {
        fits_write_col ((fitsfile*) pFile,
                        (int) colfmt[j],
                        (int) colnum[j],
                        (LONGLONG) k+1,
                        (LONGLONG) 1,
                        (int) colwidth[j],
                        (void*) pvoid[j],
                        (int*) &status);
        if ( status ) 
        {
          fprintf(stderr, "%s Error writing file=%s row=%ld column[%d]=%s\n", ERROR_PREFIX, str_filename, k+1, j, ttype[j]);
          fits_report_error(stderr, status);  
          return 1;
        }
      }
      k++;
    }
  }
  
  if ( fits_close_file((fitsfile*) pFile, (int*) &status))
  {
    fits_report_error(stderr, status);
    return 1;
  }
  return 0;
}



int write_per_field_logfile_ascii (fieldlist_struct *pFieldList, const char* str_filename)
{
  int i,a;
  FILE *pFile = NULL;
  if ( pFieldList == NULL ) return 1;

  if ((pFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  //write a full header
  {
    time_t rawtime;
    struct tm *timeinfo;
    
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, "#This is the per Field logfile\n\
#This log was first written at: %s\
#Original filename: %s\n\
#This logfile contains one entry per Field\n\
#The file gives summary info about the results for each Field at the end of the survey\n\
#Description of columns:\n\
#Index  Name        Format    Description\n\
#-----  ----------- --------- ------------------------------------------------------------------------ \n\
#1      Name        string    Code name of this field\n\
#2      Index       int       running index of this field\n\
#3      RA          double    The RA coordinate of the Field centre (deg,J2000)\n\
#4      Dec         double    The Dec coordinate of the Field centre (deg,J2000)\n\
#5      PA          float     The position angle of the Field (deg E of N)\n\
#6      Gal_l       double    The Galactic longitude of the Field centre (deg)\n\
#7      Gal_b       double    The Galactic latitude of the Field centre (deg)\n\
#8      SurveyCode  hex       A hex code that identifies which sub survey this Field lies within\n\
#9      Dth         int       Number of possible dither positions for this Field\n\
#10     Nei         int       Number of neighbour Fields for this Field\n\
#11     nNeig       string    Code name of the nearest neighbour Field\n\
#12     AMmin       float     Minimum airmass achievable for this field\n\
#13     NobjLo      int       Number of candidate low resolution targets in this Field\n\
#14     NobjHi      int       Number of candidate high resolution targets in this Field\n\
#15     TotalPrio   float     Summed (adjusted) priority of all targets in this field\n\
#16     RemainPrio  float     Summed (adjusted) priority of all uncompleted targets in this field \n\
#17     PrVsBB      float     Predicted visibility of this Field (bright time, number of tile windows)\n\
#18     PrVsDG      float     Predicted visibility of this Field (dark/grey time, number of tile windows)\n\
#19     wPrVBB      float     Weighted predicted visibility of this Field (bright time, weighted number of tile windows)\n\
#20     wPrVDG      float     Weighted predicted visibility of this Field (dark/grey time, weighted number of tile windows)\n\
#21     Vis_BB      int       Actual visibility of this Field    (bright time, number of tile windows) \n\
#22     Vis_DG      int       Actual visibility of this Field    (dark/grey time, number of tile windows) \n\
#23     Req_BB      int       Requested number of bright time Tiles in this Field\n\
#24     Req_DG      int       Requested number of dark/grey time Tiles in this Field\n\
#25     PlanBB      int       Planned number of bright time Tiles in this Field (at beginning of survey)\n\
#26     PlanDG      int       Planned  number of dark/grey time Tiles in this Field (at beginning of survey)\n\
#27     DoneBB      int       Executed number of bright time Tiles in this Field (at end of survey)\n\
#28     DoneDG      int       Executed number of dark/grey time Tiles in this Field (at end of survey)\n\
#29     FhrsLo      float     Summed required fiber-hours for low resolution targets in this Field\n\
#30     FhrsHi      float     Summed required fiber-hours for high resolution targets in this Field\n",                           
          asctime(timeinfo),
          str_filename);

    for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
    {
      fprintf(pFile, "#%2d     %5s%d      float     Predicted visibility of this Field (bright    time, number of tile windows, AM<=%.1f)\n\
#%2d     %5s%d      float     Predicted visibility of this Field (dark/grey time, number of tile windows, AM<=%.1f)\n\
#%2d     %5s%d      int       Actual visibility of this Field    (bright    time, number of tile windows, AM<=%.1f)\n\
#%2d     %5s%d      int       Actual visibility of this Field    (dark/grey time, number of tile windows, AM<=%.1f)\n",
              31+a*4, "pVBBa", a+1, 1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE,
              32+a*4, "pVDGa", a+1, 1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE,
              33+a*4, " VBBa", a+1, 1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE,
              34+a*4, " VDGa", a+1, 1.0 + (a+1) * FIELD_VISIBILITY_AM_STEP_SIZE);
    }
    fprintf(pFile, "#-----  ----------- --------- ------------------------------------------------------------------------ \n");

  }


  //write a one line column name key
  fprintf(pFile, "#%-9s %6s %10s %10s %7s %10s %10s \
%10s %3s %3s %5s %5s \
%6s %6s \
%10s %10s \
%6s %6s \
%6s %6s \
%6s %6s \
%6s %6s \
%6s %6s \
%6s %6s \
%8s %8s",
          "Name", "Index", "RA(deg)", "Dec(deg)", "PA(deg)", "Gal_l(deg)", "Gal_b(deg)", 
          "SurveyCode", "Dth", "Nei", "nNeig", "AMmin",
          "NobjLo", "NobjHi",
          "TotalPrio", "RemainPrio",
          "PrVsBB", "PrVsDG",
          "wPrVBB", "wPrVDG",
          "Vis_BB", "Vis_DG",
          "Req_BB", "Req_DG",
          "PlanBB", "PlanDG",
          "DoneBB", "DoneDG",
          "FhrsLo", "FhrsHi");
  for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
  {
    fprintf(pFile, " %5s%d %5s%d %5s%d %5s%d", "pVBBa", a+1, "pVDGa", a+1, " VBBa", a+1, " VDGa", a+1);
  }
  fprintf(pFile, "\n");

  for ( i=0; i< pFieldList->nFields; i++)
  {
    field_struct *pF = (field_struct*) &(pFieldList->pF[i]);
    if ( pF->in_survey & (SURVEY_ANY | SURVEY_IN_AREA_ANY) )
    {
      char *str_nn_name = (char*) (pF->id_nearest_neighbour >=0 && pF->id_nearest_neighbour < pFieldList->nFields ?
                                   pFieldList->pF[pF->id_nearest_neighbour].name : "-");
      fprintf(pFile, "%10s %6d %10.5f %+10.5f %7.2f %10.5f %+10.5f \
0x%08x %3d %3d %5s %5.3f \
%6d %6d \
%10.3f %10.3f \
%6.1f %6.1f \
%6.1f %6.1f \
%6d %6d \
%6d %6d \
%6d %6d \
%6d %6d \
%8.2f %8.2f",
              pF->name, pF->id, pF->ra0, pF->dec0, pF->pa0, pF->l0, pF->b0, 
              pF->in_survey, pF->num_dithers, pF->num_neighbours, (char*) str_nn_name, pF->airmass_min,
              pF->n_object_lores,                                pF->n_object_hires, 
              pF->object_priority_total_weighted,                pF->object_priority_remaining_weighted, 
              pF->ntiles_visible_predict[MOON_GROUP_BB],         pF->ntiles_visible_predict[MOON_GROUP_DG], 
              pF->ntiles_visible_predict_weighted[MOON_GROUP_BB],pF->ntiles_visible_predict_weighted[MOON_GROUP_DG], 
              pF->ntiles_visible[MOON_GROUP_BB],                 pF->ntiles_visible[MOON_GROUP_DG], 
              pF->ntiles_req[MOON_GROUP_BB],                     pF->ntiles_req[MOON_GROUP_DG],
              pF->ntiles_todo[MOON_GROUP_BB],                    pF->ntiles_todo[MOON_GROUP_DG],
              pF->ntiles_done[MOON_PHASE_BRIGHT],                pF->ntiles_done[MOON_PHASE_GREY]+pF->ntiles_done[MOON_PHASE_DARK],
              pF->object_dark_fhrs_lores,                        pF->object_dark_fhrs_hires);
      for(a=0;a<FIELD_VISIBILITY_AM_NUM_STEPS;a++)
      {
        fprintf(pFile, " %6.1f %6.1f %6d %6d",
                pF->ntiles_visible_am_predict[MOON_GROUP_BB][a], pF->ntiles_visible_am_predict[MOON_GROUP_DG][a], 
                pF->ntiles_visible_am[MOON_GROUP_BB][a],         pF->ntiles_visible_am[MOON_GROUP_DG][a]); 
      }
      fprintf(pFile, "\n");

    }
  }
  if ( pFile ) fclose(pFile); pFile = NULL;
  
  return 0;
}





//write a header line to the per tile logfile - one header line per day
int print_per_tile_log_header_line (FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile, "#%-9s %6s %8s %8s %5s %5s %2s %7s %6s %6s %6s %6s %6s %6s %6s %6s %6s %6s %6s %6s\n",
          "DayOfSurv","Field","F_ra0", "F_dec0", "nTile", "nTodo", "Mn", "Texp(m)",
          "hiCand", "loCand", "hiNfib", "loNfib", "hiNass", "loNass", "hiBNbr", "loBNbr",
          "hiN_PI", "loN_PI", "hiNsky", "loNsky"  );
  fflush(pFile);

  return 0;
}

/// Create tile logfile and write fits header keywords.
/** Detailed description.
*/
int init_tile_log_fits (survey_struct *pSurvey, const char *str_filename)
{
  fits_table_helper_struct *pT = NULL;  //alias
  if ( pSurvey == NULL ) return 1;
  pT = (fits_table_helper_struct*) &(pSurvey->TileLog);//alias

  if ( fths_init((fits_table_helper_struct*)pT)) return 1; //now done in survey_struct_init()

  if(fths_addcol(pT,"DAYOFSURVEY"    ,"1J" ,TLONG  ,1,"day"            ,"Integer night number within survey"   )) return 1;// 0
  if(fths_addcol(pT,"TILE_INDEX"     ,"1J" ,TLONG  ,1,""               ,"Tile window index within this night"  )) return 1;// 1
  if(fths_addcol(pT,"TIME_START"     ,"1D" ,TDOUBLE,1,"dayofsurvey"    ,"Tile start time"                      )) return 1;// 2
  if(fths_addcol(pT,"TIME_STOP"      ,"1D" ,TDOUBLE,1,"dayofsurvey"    ,"Tile stop  time"                      )) return 1;// 3
  if(fths_addcol(pT,"JD_START"       ,"1D" ,TDOUBLE,1,"JulianDay"      ,"Tile start time"                      )) return 1;// 4
  if(fths_addcol(pT,"JD_STOP"        ,"1D" ,TDOUBLE,1,"JulianDay"      ,"Tile stop  time"                      )) return 1;// 5
  if(fths_addcol(pT,"JD_MID"         ,"1D" ,TDOUBLE,1,"JulianDay"      ,"Tile mid   time"                      )) return 1;// 6
  if(fths_addcol(pT,"TEXP_NET"       ,"1E" ,TFLOAT ,1,"s"              ,"Tile on-sky exposure (net)"           )) return 1;// 7
  if(fths_addcol(pT,"TEXP_GROSS"     ,"1E" ,TFLOAT ,1,"s"              ,"Tile duration (gross)"                )) return 1;// 8
  if(fths_addcol(pT,"FULL_FLAG"      ,"1J" ,TLONG  ,1,"0=full,1=part"  ,"Is this is a full tile?"              )) return 1;// 9
  if(fths_addcol(pT,"ZEN_RA"         ,"1D" ,TDOUBLE,1,"deg,J2000"      ,"RA of zenith (at mid-time)"           )) return 1;//10
  if(fths_addcol(pT,"ZEN_DEC"        ,"1D" ,TDOUBLE,1,"deg,J2000"      ,"Dec of zenith"                        )) return 1;//11
  if(fths_addcol(pT,"ZEN_SEEING"     ,"1E" ,TFLOAT ,1,"FWHM,arcsec,V"  ,"Seeing at zenith"                     )) return 1;//12
  if(fths_addcol(pT,"ZEN_SKYBR"      ,"1E" ,TFLOAT ,1,"ABmag/arcsec2"  ,"Sky brightness at zenith"             )) return 1;//13
  if(fths_addcol(pT,"ZEN_SKYBR_CODE" ,"2A" ,TSTRING,1,"dd|gg|bb|dg|bg" ,"Sky brightness at zenith code"        )) return 1;//14
  if(fths_addcol(pT,"MOON_ILLUM"     ,"1E" ,TFLOAT ,1,"fraction"       ,"Moon illumination fraction"           )) return 1;//15
  if(fths_addcol(pT,"MOON_RA"        ,"1D" ,TDOUBLE,1,"deg,J2000"      ,"RA of Moon (at mid-time)"             )) return 1;//16
  if(fths_addcol(pT,"MOON_DEC"       ,"1D" ,TDOUBLE,1,"deg,J2000"      ,"Dec of Moon (at mid-time)"            )) return 1;//17
  if(fths_addcol(pT,"MOON_Z"         ,"1D" ,TDOUBLE,1,"deg"            ,"Zenith distance of moon"              )) return 1;//18
  if(fths_addcol(pT,"MOON_AZ"        ,"1D" ,TDOUBLE,1,"deg"            ,"Azimuth angle of moon"                )) return 1;//19
  if(fths_addcol(pT,"CLOUD_CODE"     ,"5A" ,TSTRING,1,"pho|thn|thk|bad","Cloud cover code"                     )) return 1;//20

  if(fths_addcol(pT,"FIELD_NAME"     ,"12A",TSTRING,1,""               ,"Code name of field"                   )) return 1;//21
  if(fths_addcol(pT,"FIELD_RA"       ,"1D" ,TDOUBLE,1,"deg,J2000"      ,"RA of field centre"                   )) return 1;//22
  if(fths_addcol(pT,"FIELD_DEC"      ,"1D" ,TDOUBLE,1,"deg,J2000"      ,"Dec of field centre"                  )) return 1;//23
  if(fths_addcol(pT,"FIELD_PA"       ,"1D" ,TDOUBLE,1,"deg,EofN"       ,"PA of field"                          )) return 1;//24
  if(fths_addcol(pT,"FIELD_GAL_L"    ,"1D" ,TDOUBLE,1,"deg"            ,"Galactic long of field centre"        )) return 1;//25
  if(fths_addcol(pT,"FIELD_GAL_B"    ,"1D" ,TDOUBLE,1,"deg"            ,"Galactic lat of field centre"         )) return 1;//26
  if(fths_addcol(pT,"FIELD_Z"        ,"1E" ,TFLOAT ,1,"deg"            ,"Zenith dist of field centre(mid-time)")) return 1;//27
  if(fths_addcol(pT,"FIELD_AZ"       ,"1E" ,TFLOAT ,1,"deg"            ,"Azimuth angle of field centre"        )) return 1;//28
  if(fths_addcol(pT,"FIELD_PARA_ANG" ,"1E" ,TFLOAT ,1,"deg"            ,"Parallactic angle of field centre"    )) return 1;//29
  if(fths_addcol(pT,"FIELD_AM"       ,"1E" ,TFLOAT ,1,"sec(Z)"         ,"Airmass of field centre(mid-time)"    )) return 1;//30
  if(fths_addcol(pT,"FIELD_SKYBR"    ,"1E" ,TFLOAT ,1,"ABmag/arcsec2"  ,"Sky brightness at field centre"       )) return 1;//31
  if(fths_addcol(pT,"FIELD_IQ"       ,"1E" ,TFLOAT ,1,"FWHM,arcsec,V"  ,"IQ at field centre"                   )) return 1;//32
  if(fths_addcol(pT,"FIELD_MOONDIST" ,"1E" ,TFLOAT ,1,"deg"            ,"Distance from moon to field centre"   )) return 1;//33
  if(fths_addcol(pT,"SLEW_DIST"      ,"1E" ,TFLOAT ,1,"deg"            ,"Distance slewed from previous tile"   )) return 1;//34
  if(fths_addcol(pT,"NTILES_REQ_BB"  ,"1J" ,TLONG  ,1,"tiles"          ,"No. tiles requested, bright sky"      )) return 1;//35
  if(fths_addcol(pT,"NTILES_REQ_DG"  ,"1J" ,TLONG  ,1,"tiles"          ,"No. tiles requested, dark/grey sky"   )) return 1;//36
  if(fths_addcol(pT,"NTILES_PLAN_BB" ,"1J" ,TLONG  ,1,"tiles"          ,"No. tiles planned, bright sky"        )) return 1;//37
  if(fths_addcol(pT,"NTILES_PLAN_DG" ,"1J" ,TLONG  ,1,"tiles"          ,"No. tiles planned, dark/grey sky"     )) return 1;//38
  if(fths_addcol(pT,"NTILES_DONE_BB" ,"1J" ,TLONG  ,1,"tiles"          ,"No. tiles done, bright sky"           )) return 1;//39
  if(fths_addcol(pT,"NTILES_DONE_DG" ,"1J" ,TLONG  ,1,"tiles"          ,"No. tiles done, dark/grey sky"        )) return 1;//40
  if(fths_addcol(pT,"NTILES_PRVIS_BB","1E" ,TFLOAT ,1,"tiles"          ,"Predicted visibility, bright sky"     )) return 1;//41
  if(fths_addcol(pT,"NTILES_PRVIS_DG","1E" ,TFLOAT ,1,"tiles"          ,"Predicted visibility, dark/grey sky"  )) return 1;//42

  if(fths_addcol(pT,"NCAND_HI"       ,"1J" ,TLONG  ,1,"targets"        ,"No. candidate hi-res targets"         )) return 1;//43
  if(fths_addcol(pT,"NCAND_LO"       ,"1J" ,TLONG  ,1,"targets"        ,"No. candidate lo-res targets"         )) return 1;//44
  if(fths_addcol(pT,"NFIB_AVAIL_HI"  ,"1J" ,TLONG  ,1,"fibers"         ,"No. hi-res fibers available"          )) return 1;//45
  if(fths_addcol(pT,"NFIB_AVAIL_LO"  ,"1J" ,TLONG  ,1,"fibers"         ,"No. lo-res fibers available"          )) return 1;//46
  if(fths_addcol(pT,"NFIB_SKY_HI"    ,"1J" ,TLONG  ,1,"fibers"         ,"No. hi-res fibers for sky"            )) return 1;//47
  if(fths_addcol(pT,"NFIB_SKY_LO"    ,"1J" ,TLONG  ,1,"fibers"         ,"No. lo-res fibers for sky"            )) return 1;//48
  if(fths_addcol(pT,"NFIB_PI_HI"     ,"1J" ,TLONG  ,1,"fibers"         ,"No. hi-res fibers for PI targets"     )) return 1;//49
  if(fths_addcol(pT,"NFIB_PI_LO"     ,"1J" ,TLONG  ,1,"fibers"         ,"No. lo-res fibers for PI targets"     )) return 1;//50
  if(fths_addcol(pT,"NFIB_ASSIGN_HI" ,"1J" ,TLONG  ,1,"fibers"         ,"No. hi-res fibers assigned to targets")) return 1;//51
  if(fths_addcol(pT,"NFIB_ASSIGN_LO" ,"1J" ,TLONG  ,1,"fibers"         ,"No. lo-res fibers assigned to targets")) return 1;//52
  if(fths_addcol(pT,"NFIB_SPARE_HI"  ,"1J" ,TLONG  ,1,"fibers"         ,"No. hi-res fibers unassigned"         )) return 1;//53
  if(fths_addcol(pT,"NFIB_SPARE_LO"  ,"1J" ,TLONG  ,1,"fibers"         ,"No. lo-res fibers unassigned"         )) return 1;//54
  if(fths_addcol(pT,"NPROB_NEIGH_HI" ,"1J" ,TLONG  ,1,"targets"        ,"No. targets with problem neighbours"  )) return 1;//55
  if(fths_addcol(pT,"NPROB_NEIGH_LO" ,"1J" ,TLONG  ,1,"targets"        ,"No. targets with problem neighbours"  )) return 1;//56
                                                                                                                            

  if ( fths_create ((fits_table_helper_struct*) pT,
                    (char*) str_filename,
                    (char*) "TILELOG",
                    (long)  0,
                    (BOOL) pSurvey->clobber))
  {
    fprintf(stderr, "%s  There was a problem creating the tile logfile: \"%s\"\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }
  return 0;
}

/*
//create tile logfile and write fits header keywords
int init_tile_logfile_fits (survey_struct *pSurvey, const char *str_filename)
{
  char str_filename_new[LSTR_MAX];
  int status = 0;
  const int max_cols = 64;   //remember to update this when adding new columns 
  char *ttype[max_cols];
  char *tform[max_cols]; 
  char *tunit[max_cols];
  char *tcomm[max_cols];
  int  colfmt[max_cols];
  int  colnum[max_cols];
  int  colwidth[max_cols];
  int j = 0;
  int ncols;

  if ( pSurvey == NULL ) return 1;
 
  //add a "!" prefix if we are going to clobber the output
  snprintf(str_filename_new, LSTR_MAX, "%s%s", CLOBBER2S(pSurvey->clobber), str_filename);
  
  if (fits_create_file ((fitsfile**) &(pSurvey->pTileLog), (char*) str_filename_new, (int*) &status))
  {
    fits_report_error(stderr, status); // print any error messages
    fprintf(stderr, "%s  There was a problem creating the tile logfile (FITS): %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  //now prepare the column names etc
  j= 0;ttype[j]="DAYOFSURVEY"     ;tform[j]="1J"        ;tunit[j]="day"            ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j= 1;ttype[j]="SEQUENCE"        ;tform[j]="1J"        ;tunit[j]=""               ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j= 2;ttype[j]="TIME_START"      ;tform[j]="1D"        ;tunit[j]="dayofsurvey"    ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 3;ttype[j]="TIME_STOP"       ;tform[j]="1D"        ;tunit[j]="dayofsurvey"    ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 4;ttype[j]="JD_START"        ;tform[j]="1D"        ;tunit[j]="JulianDay"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 5;ttype[j]="JD_STOP"         ;tform[j]="1D"        ;tunit[j]="JulianDay"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 6;ttype[j]="JD_MID"          ;tform[j]="1D"        ;tunit[j]="JulianDay"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j= 7;ttype[j]="TEXP_NET"        ;tform[j]="1E"        ;tunit[j]="s"              ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j= 8;ttype[j]="TEXP_GROSS"      ;tform[j]="1E"        ;tunit[j]="s"              ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j= 9;ttype[j]="FULL_FLAG"       ;tform[j]="1I"        ;tunit[j]="1=full,0=part"  ;colfmt[j]=TINT   ;colwidth[j]=1;
  j=10;ttype[j]="ZEN_RA"          ;tform[j]="1D"        ;tunit[j]="deg,J2000"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=11;ttype[j]="ZEN_DEC"         ;tform[j]="1D"        ;tunit[j]="deg,J2000"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=12;ttype[j]="ZEN_SEEING"      ;tform[j]="1E"        ;tunit[j]="FWHM,arcsec,V"  ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=13;ttype[j]="ZEN_SKYBR"       ;tform[j]="1E"        ;tunit[j]="ABmag/arcsec2"  ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=14;ttype[j]="ZEN_SKYBR_CODE"  ;tform[j]="2A"        ;tunit[j]="dd|gg|bb|dg|bg" ;colfmt[j]=TSTRING;colwidth[j]=1;
  j=15;ttype[j]="MOON_ILLUM"      ;tform[j]="1E"        ;tunit[j]="fraction"       ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=16;ttype[j]="MOON_RA"         ;tform[j]="1D"        ;tunit[j]="deg,J2000"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=17;ttype[j]="MOON_DEC"        ;tform[j]="1D"        ;tunit[j]="deg,J2000"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=18;ttype[j]="MOON_Z"          ;tform[j]="1E"        ;tunit[j]="deg"            ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=19;ttype[j]="MOON_AZ"         ;tform[j]="1E"        ;tunit[j]="deg"            ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=20;ttype[j]="CLOUD_CODE"      ;tform[j]="3A"        ;tunit[j]="pho|thn|thk|bad";colfmt[j]=TSTRING;colwidth[j]=1;
  j=21;ttype[j]="FIELD_NAME"      ;tform[j]="12A"       ;tunit[j]=""               ;colfmt[j]=TSTRING;colwidth[j]=1;
  j=22;ttype[j]="FIELD_RA"        ;tform[j]="1D"        ;tunit[j]="deg,J2000"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=23;ttype[j]="FIELD_DEC"       ;tform[j]="1D"        ;tunit[j]="deg,J2000"      ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=24;ttype[j]="FIELD_PA"        ;tform[j]="1E"        ;tunit[j]="deg,EofN"       ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=25;ttype[j]="FIELD_GAL_L"     ;tform[j]="1D"        ;tunit[j]="deg"            ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=26;ttype[j]="FIELD_GAL_B"     ;tform[j]="1D"        ;tunit[j]="deg"            ;colfmt[j]=TDOUBLE;colwidth[j]=1;
  j=27;ttype[j]="FIELD_Z"         ;tform[j]="1E"        ;tunit[j]="deg"            ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=28;ttype[j]="FIELD_AZ"        ;tform[j]="1E"        ;tunit[j]="deg"            ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=29;ttype[j]="FIELD_AM"        ;tform[j]="1E"        ;tunit[j]="sec(Z)"         ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=30;ttype[j]="FIELD_SKYBR"     ;tform[j]="1E"        ;tunit[j]="ABmag/arcsec2"  ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=31;ttype[j]="FIELD_IQ"        ;tform[j]="1E"        ;tunit[j]="FWHM,arcsec,V"  ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=32;ttype[j]="FIELD_MOONDIST"  ;tform[j]="1E"        ;tunit[j]="deg"            ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=33;ttype[j]="SLEW_DIST"       ;tform[j]="1E"        ;tunit[j]="deg"            ;colfmt[j]=TFLOAT ;colwidth[j]=1;
  j=34;ttype[j]="NTILES_REQ_BB"   ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=35;ttype[j]="NTILES_REQ_DG"   ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=36;ttype[j]="NTILES_PLAN_BB"  ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=37;ttype[j]="NTILES_PLAN_DG"  ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=38;ttype[j]="NTILES_DONE_BB"  ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=39;ttype[j]="NTILES_DONE_DG"  ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=40;ttype[j]="NTILES_TODO_BB"  ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=41;ttype[j]="NTILES_TODO_DG"  ;tform[j]="1J"        ;tunit[j]="tiles"          ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=42;ttype[j]="NCAND_HI"        ;tform[j]="1J"        ;tunit[j]="targets"        ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=43;ttype[j]="NCAND_LO"        ;tform[j]="1J"        ;tunit[j]="targets"        ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=44;ttype[j]="NFIB_AVAIL_HI"   ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=45;ttype[j]="NFIB_AVAIL_LO"   ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=46;ttype[j]="NFIB_SKY_HI"     ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=47;ttype[j]="NFIB_SKY_LO"     ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=48;ttype[j]="NFIB_PI_HI"      ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=49;ttype[j]="NFIB_PI_LO"      ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=50;ttype[j]="NFIB_ASSIGN_HI"  ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=51;ttype[j]="NFIB_ASSIGN_LO"  ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=52;ttype[j]="NFIB_SPARE_HI"   ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
  j=53;ttype[j]="NFIB_SPARE_LO"   ;tform[j]="1J"        ;tunit[j]="fibers"         ;colfmt[j]=TLONG  ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
//  j=  ;ttype[j]=""                ;tform[j]=""          ;tunit[j]=""               ;colfmt[j]=       ;colwidth[j]=1;
  ncols = j+1;
  for(j=0;j<ncols;j++) colnum[j] = j+1;
  for(j=0;j<ncols;j++) tcomm[j] = "";


  //set up the comments for each table column
  j= 0;tcomm[j]="Integer night number within survey";
  j= 1;tcomm[j]="Tile window index within this night";
  j= 2;tcomm[j]="Tile start time";
  j= 3;tcomm[j]="Tile stop  time";
  j= 4;tcomm[j]="Tile start time";
  j= 5;tcomm[j]="Tile stop  time";
  j= 6;tcomm[j]="Tile mid   time";
  j= 7;tcomm[j]="Tile on-sky exposure (net)";
  j= 8;tcomm[j]="Tile duration (gross)";
  j= 9;tcomm[j]="True if this is a full tile";
  j=10;tcomm[j]="RA of zenith (at mid-time)";
  j=11;tcomm[j]="Dec of zenith";
  j=12;tcomm[j]="Seeing at zenith";
  j=13;tcomm[j]="Sky brightness at zenith";
  j=14;tcomm[j]="Sky brightness at zenith code";
  j=15;tcomm[j]="Moon illumination fraction";
  j=16;tcomm[j]="RA of Moon (at mid-time)";
  j=17;tcomm[j]="Dec of Moon (at mid-time)";
  j=18;tcomm[j]="Zenith distance of moon";
  j=19;tcomm[j]="Azimuth angle of moon";
  j=20;tcomm[j]="Cloud cover code";
  j=21;tcomm[j]="Code name of field";
  j=22;tcomm[j]="RA of field centre (at mid-time)";
  j=23;tcomm[j]="Dec of field centre";
  j=24;tcomm[j]="PA of field";
  j=25;tcomm[j]="Galactic long of field centre";
  j=26;tcomm[j]="Galactic lat of field centre";
  j=27;tcomm[j]="Zenith distance of field centre";
  j=28;tcomm[j]="Azimuth angle of field centre";
  j=29;tcomm[j]="Airmass of field centre";
  j=30;tcomm[j]="Sky brightness at field centre";
  j=31;tcomm[j]="IQ at field centre";
  j=32;tcomm[j]="Distance from moon to field centre";
  j=33;tcomm[j]="Distance slewed from previous tile";
  j=34;tcomm[j]="No. tiles requested, bright sky";
  j=35;tcomm[j]="No. tiles requested, dark/grey sky";
  j=36;tcomm[j]="No. tiles planned, bright sky";
  j=37;tcomm[j]="No. tiles planned, dark/grey sky";
  j=38;tcomm[j]="No. tiles done, bright sky";
  j=39;tcomm[j]="No. tiles done, dark/grey sky";
  j=40;tcomm[j]="No. tiles todo, bright sky";
  j=41;tcomm[j]="No. tiles todo, dark/grey sky";
  j=42;tcomm[j]="No. candidate hi-res targets";
  j=43;tcomm[j]="No. candidate lo-res targets";
  j=44;tcomm[j]="No. hi-res fibers available";
  j=45;tcomm[j]="No. lo-res fibers available";
  j=46;tcomm[j]="No. hi-res fibers reserved for sky";
  j=47;tcomm[j]="No. lo-res fibers reserved for sky";
  j=48;tcomm[j]="No. hi-res fibers reserved for PI targets";
  j=49;tcomm[j]="No. lo-res fibers reserved for PI targets";
  j=50;tcomm[j]="No. hi-res fibers assigned to targets";
  j=51;tcomm[j]="No. lo-res fibers assigned to targets";
  j=52;tcomm[j]="No. hi-res fibers unassigned";
  j=53;tcomm[j]="No. lo-res fibers unassigned";               

  //make the new table (allow 10% margin on predicted number of tiles)
  if (  fits_create_tbl ((fitsfile*) pSurvey->pTileLog, (int) BINARY_TBL,
                         (long) (pSurvey->num_tiles_poss_total*1.1), (int) ncols,
                         (char**) ttype, (char**) tform, (char**) tunit,
                         (char*) "TILELOG",
                         (int*) &status) )
  {
    fits_report_error(stderr, status);  
    return 1;
  }

  
  for(j=0;j<ncols;j++)
  {
    //now add some comments to the column name keywords
    char str_keyname[STR_MAX];
    snprintf(str_keyname, STR_MAX, "TTYPE%d", j+1);
    if (fits_modify_comment ((fitsfile*)pSurvey->pTileLog,
                             (char*) str_keyname, (char*) tcomm[j], (int*) &status))
    {
      fits_report_error(stderr, status);  
      return 1;
    }
  }

  return 0;

}
*/








//open up file and write header for logfile
int init_per_tile_log (survey_struct *pSurvey, const char *str_filename, FILE **ppFile)
{
  if ( *ppFile != NULL ) return 1;
  
  if (((*ppFile) = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the logfile : %s\n",
            ERROR_PREFIX, str_filename);
    
    return 1;
  }

  {
    time_t rawtime;
    struct tm *timeinfo;
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fflush(*ppFile);

    fprintf((FILE*) *ppFile,
            "#This is the per tile logfile for simulation: %s\n\
#This log was first written at: %s\
#Original filename: %s\n\
#This logfile contains one entry for each executed tile\n\
#Description of columns:\n\
#Index  Name      Format  Description\n\
#-----  --------- ------  -------------------------------------------------------------------------------- \n\
#1      DayOfSurv double  The 'day of survey' (decimal days from start of survey)\n\
#2      Field     string  The codename of the Field that was observed\n\
#3      F_ra0     double  The central RA of the Field (deg,J2000)\n\
#4      F_dec0    double  The central Dec of the Field (deg,J2000) \n\
#5      nTile     int     The number of Tiles executed so far for this Field (including this Tile)\n\
#6      nTodo     int     The number of Tiles that we planned to execute for this Field\n\
#7      Mn        string  The current moon phase conditions (dg=Dark/Grey, bb=Bright)\n\
#8      Texp(m)   float   The exposure time executed for this Tile (mins)\n\
#9      hiCand    int     The number of hi-res candidate targets in this Field\n\
#10     loCand    int     The number of lo-res candidate targets in this Field\n\
#11     hiNfib    int     The number of available hi-res fibers for this Tile (N_total-N_sky-N_PI)\n\
#12     loNfib    int     The number of available lo-res fibers for this Tile (N_total-N_sky-N_PI)\n\
#13     hiNass    int     The number of hi-res fibers assigned to science targets in this Tile\n\
#14     loNass    int     The number of lo-res fibers assigned to science targets in this Tile \n\
#15     hiBNbr    int     The number of observed hi-res targets with problematic neighbours (delta_mag>%g)\n\
#16     loBNbr    int     The number of observed lo-res targets with problematic neighbours (delta_mag>%g)\n\
#17     hiN_PI    int     The number of hi-res fibers reserved for PI targets in this Tile\n\
#18     loN_PI    int     The number of lo-res fibers reserved for PI targets in this Tile \n\
#19     hiNsky    int     The number of hi-res fibers reserved for sky observations in this Tile\n\
#20     loNsky    int     The number of lo-res fibers reserved for sky observations in this Tile \n\
#-----  --------- ------  -------------------------------------------------------------------------------- \n",
            (char*) pSurvey->sim_code_name,
            (char*) asctime(timeinfo),
            (char*) str_filename,
            (float) pSurvey->delta_mag_for_problem_neighbour,
            (float) pSurvey->delta_mag_for_problem_neighbour);
    fflush(*ppFile);
  }
  //deliberately leave the file open for further writing
  return 0;
}

//write a one line header
int print_tile_reports_header_line (FILE *pFile)
{
  if ( pFile == NULL ) return 1;
  fprintf(pFile,
          "%-12s %5s %4s %10s %10s %6s %4s %8s %8s %5s %2s %5s %7s %7s %8s %8s %5s %5s : %9s %8s %8s %8s %8s %5s %5s %5s %5s %3s %3s %3s %3s %3s %3s %6s\n",
          "#TileReport", "Night", "Tile", "StartTime", "StopTmeEst", "Texp", "flag", "RA@Zen", "Dec@Zen", "Sky@Z",
          "Mn", "MnIll", "MoonRA", "MoonDec", "MoonZ", "MoonAz", "IQ@Zn", "Cloud",
          "Fname", "F_ra0", "F_dec0", "F_Z", "F_Az", "AM@F", "Sky@F", "IQ@F", "MnDst",
          "rBB", "rDG", "pBB", "pDG", "xBB", "xDG", "Slew_D");
  fflush (pFile);
  //  (void) fflush(pFile);

  return 0;
}

// //write one data line
// int print_tile_reports_line (FILE *pFile, survey_struct *pSurvey)
// {
//   if ( pFile == NULL ) return 1;
//   if ( pSurvey == NULL ) return 1;
//   fprintf(pFile,
//             "%-12s %5d %4d %10.5f %10.5f %6.0f %4s %8.4f %+8.3f %5.2f %2s %5.3f %7.3f %+7.3f %8.3f %8.3f %5.3f %5s : ",
//             "TileReport", pSurvey->night_counter, pSurvey->tile_counter,
//             tile_start_sJD, tile_stop_sJD,
//             Texp_mins*MINS_TO_SECS, (tile_status_flag==TILE_STATUS_FULL_EXPOSURE?"Full":"Part"),
//             pTime->Rnow.ra_zen, pTime->Rnow.dec_zen, brightness_zen,
//             pMoon->codename, pTime->Rnow.moon.illum_frac,
//             pTime->Rnow.moon.ra, pTime->Rnow.moon.dec,pTime->Rnow.moon.Z, moon_Az,
//             seeing_zen, (char*) pSurvey->str_cloud_code);
// 
// 
// }


//open up file and write header for logfile
int init_tile_reports_logfile (survey_struct *pSurvey, const char *str_filename, FILE **ppFile)
{
  if ( *ppFile != NULL ) return 1;
  
  if ((*ppFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the logfile : %s\n",
            ERROR_PREFIX, str_filename);
    
    return 1;
  }

  {
    time_t rawtime;
    struct tm *timeinfo;
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = (struct tm*) localtime((time_t*) &rawtime);

    //    fprintf(stdout, "Writing header to tile_reports_logfile\n");
    //    fflush(stdout);

    fflush(*ppFile);
    
    fprintf((FILE*) *ppFile,
            "#This is the tile reports logfile for simulation: \"%s\"\n\
#This log was first written at: %s\
#Original filename: %s\n\
#This logfile contains one entry describing the observing for each observing window\n\
#This information is written before a tile is executed, so some info is estimated\n\
#Description of columns:\n\
#Index  Name        Format  Description\n\
#-----  ----------- ------  -------------------------------------------------------------------------------- \n\
#1      TileReport  string  Placeholder\n\
#2      Night       int     The night number (1=1st night of survey)\n\
#3      Tile        int     The Tile window number within this night (1=1st Tile in night)\n\
#4      StartTime   double  The Time at the start of the window (decimal days from start of survey)\n\
#5      StopTmeEst  double  The (estimated) time at the end of the window (decimal days from start of survey)\n\
#6      Texp        float   The on-sky exposure time estimated to be delivered for this tile (seconds)\n\
#7      flag        string  A flag describing if this is a 'Full' or 'Part' tile\n\
#8      RA@Zen      double  The RA coord at zenith (at mid-time during window,deg,J2000)\n\
#9      Dec@Zen     double  The Dec coord at zenith (at mid-time during window,deg,J2000)\n\
#10     Sky@Z       float   The sky brightness at zenith (AB mag/arcsec^2, V-band, at mid-time during window)\n\
#11     Mn          string  The moon brightness code (dg=Dark/Grey, bb=Bright, at mid-time during window)\n\
#12     MnIll       float   The fraction of the moon that is illuminated (at mid-time during window)\n\
#13     MoonRA      double  The RA coord of the moon (at mid-time during window,deg,J2000)\n\
#14     MoonDec     double  The Dec coord of the moon (at mid-time during window,deg,J2000)\n\
#15     MoonZ       float   The zenithal distance of the moon (at mid-time during window,deg) \n\
#16     MoonAz      float   The azimuth angle of the moon (at mid-time during window,deg)\n\
#17     IQzen       float   The seeing at zenith (FWHM,arcsec,V-band,averaged over window)\n\
#18     Cloud       string  Code describing cloud cover conditions (PHOTO,CLEAR,THIN,THICK,BAD)\n\
#19     :           char    Spacer\n\
#20     Fname       string  The codename of the Field that was observed in this window\n\
#21     F_ra0       double  The central RA coordinate of the Field (deg,J2000)\n\
#22     F_dec0      double  The central Dec coordinate of the Field (deg,J2000)\n\
#23     F_Z         float   The zenithal distance of the Field (at mid-time of Tile,deg)\n\
#24     F_Az        float   The azimuthal angle of the Field (at mid-time of Tile,deg)\n\
#25     AM@F        float   The airmass of the Field (at mid-time during Tile)\n\
#26     Sky@F       float   The sky brightness at location of Field (AB mag/arcsec^2, V-band,at mid-time of Tile)\n\
#27     IQ@F        float   The seeing at the field (FWHM,arcsec,V-band,averaged over Tile)\n\
#28     MnDst       float   The angular distance between the moon and the Field (at mid-time of Tile,deg)\n\
#29     rBB         int     The requested number of Tiles for this Field in Bright moon conditions\n\
#30     rDG         int     The requested number of Tiles for this Field in Dark/Grey moon conditions\n\
#31     pBB         int     The planned number of Tiles for this Field in Bright moon conditions\n\
#32     pDG         int     The planned number of Tiles for this Field in Dark/Grey moon conditions\n\
#33     xBB         int     The executed number of Tiles for this Field in Bright moon conditions\n\
#34     xDG         int     The executed number of Tiles for this Field in Dark/Grey moon conditions\n\
#35     Slew_D      float   The distance slewed to reach this Field from the last Tile (deg)\n\
#-----  ----------- ------  -------------------------------------------------------------------------------- \n",
            (char*) pSurvey->sim_code_name,
            (char*) asctime(timeinfo),
            (char*) str_filename);
    fflush(*ppFile);
  }

  
  //deliberately leave the file open for further writing
  return 0;
}



//open up file and write header for logfile
int init_night_reports_logfile (survey_struct *pSurvey, const char *str_filename, FILE **ppFile)
{
  time_t rawtime;
  struct tm *timeinfo;
  if ( *ppFile != NULL ) return 1;
  
  if ((*ppFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the logfile : %s\n",
            ERROR_PREFIX, str_filename);
    
    return 1;
  }

  if (time((time_t*) &rawtime) < 0 ) return 1;
  timeinfo = localtime(&rawtime);
    
  fprintf(*ppFile, "#This is the night reports logfile for simulation: \"%s\"\n\
#This log was first written at: %s\
#Original filename: %s\n\
#This logfile contains one entry per night of the survey giving breif info about night conditions\n\
#Description of columns:\n\
#Name       Description\n\
#-----      -----------------------------------------------------------------------------\n\
Night       Number of this night (1st night=1)\n\
JD_midnight Julian date at local midnight (decimal days of Julian epoch) \n\
JD_twi_eve  Julian date at the end of evening twilight (days) \n\
JD_twi_morn Julian date at the beginning of morning twilight (days) \n\
JD_start    Julian date at the beginning of survey (days) \n\
-           Additional reports of adverse night conditions are appended to entry where applicable\n\
#-----  ----------- ------  --------------------------------------------------------------\n",
          pSurvey->sim_code_name,
          asctime(timeinfo),
          str_filename);
  
  fflush(*ppFile);
  //deliberately leave the file open for further writing
  return 0;
}

//open up file and write header for logfile
int init_timeline_reports_logfile (survey_struct *pSurvey, const char *str_filename, FILE **ppFile)
{
  time_t rawtime;
  struct tm *timeinfo;
  if ( *ppFile != NULL ) return 1;
  
  if ((*ppFile = (FILE*) fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the logfile : %s\n",
            ERROR_PREFIX, str_filename);
    
    return 1;
  }

  if (time((time_t*) &rawtime) < 0 ) return 1;
  timeinfo = localtime(&rawtime);
    
  fprintf(*ppFile, "#This is the timeline reports logfile for simulation: %s\n\
#This log was first written at: %s\
#Original filename: %s\n\
#This logfile contains one entry per Tile window, and gives info about instantaneous observing conditionsn\n\
#Description of columns:\n\
#Index  Name        Format    Description\n\
#-----  ----------- --------- -------------------------------------------------------------------------------- \n\
#1      TimelineNow string    Placeholder\n\
#2      JD(days)    double    The current Julian date (days in Julian epoch)\n\
#3      InterpWind  flt:flt   The range from which the current interpolation has been made (hrs) \n\
#4      N/D         string    A flag telling us if it is currently Night or Day\n\
#5      E_twi-T     float     End of evening twilight minus current time (hours)\n\
#6      M_twi-T     float     Start of morning twilight minus timne now (hours)\n\
#7      Moon        float     Moon illumination fraction\n\
#8      U/D         string    Is moon Up or Down?\n\
#9      RA_Moon     double    Position of Moon, RA coordinate (deg,J2000)\n\
#10     Dec_Moon    double    Position of Moon, Dec coordinate (deg,J2000)\n\
#11     Z_Moon      float     Distance of Moon from Zenith (deg)\n\
#12     RA_Zen      double    Position of Zenith, RA coordinate (deg,J2000)  \n\
#13     Dec_Zen     double    Position of Zenith, Dec coordinate (deg,J2000) \n\
#14     SkyBrZen    float     Sky brightness at zenith (V-band,AB mag/arcsec^2)\n\
#15     LenNgt      float     Length of the current night (hours, end eve twi->start morn twi)\n\
#-----  ----------- --------- -------------------------------------------------------------------------------- \n",
          pSurvey->sim_code_name,
          asctime(timeinfo),
          str_filename);
  
  fflush(*ppFile);
  //deliberately leave the file open for further writing
  return 0;
}


//IQ(airmass) = seeing(zenith)*airmass^0.6
// -> airmass_max = [IQ_max / seeing(zenith)]^(1/0.6)
double calc_max_airmass ( double seeing_zen, double IQ_max, double airmass_limit)
{
  double result;
  if      ( seeing_zen <= (double) 0.0 )
  {
    result = (double) -1.0;
  }
  else if ( IQ_max <= seeing_zen ) 
  {
    result = (double) 0.0;
  }
  else
  {
    result = pow( IQ_max / seeing_zen,  (double) 1.6666666666666666666667); 
  }
  return  (double) MIN(result,airmass_limit);
}



////////////////////////////////////////////////////////////////////////////////////////////////
/////////// cluster_struct functions /////////////
int cluster_struct_init ( cluster_struct* pCluster)
{
  if ( pCluster == NULL ) return 1;
  pCluster->id = (int) -1;
  strncpy(pCluster->name, "", CLUSTERS_NAME_MAX_LENGTH);
  pCluster->redshift = (float) NAN;
  pCluster->num_gals = (int) 0;
  pCluster->num_observed = (int)0;
  pCluster->num_complete = (int) 0;
  pCluster->BCG_observed = (BOOL) FALSE;
  pCluster->BCG_complete = (BOOL) FALSE;
  pCluster->objid_BCG  = (int) -1;
  pCluster->deltaFoM = (float) NAN;
  pCluster->successfully_observed = (BOOL) FALSE;
  pCluster->redshift_bin_for_FoM = (int) -1;
  return 0;
}

int cluster_struct_copy( cluster_struct* pClusterOrig, cluster_struct* pClusterCopy)
{
  if ( pClusterOrig == NULL || pClusterCopy == NULL ) return 1;
  
  pClusterCopy->id            = (int)   pClusterOrig->id;
  strncpy(pClusterCopy->name,pClusterOrig->name, CLUSTERS_NAME_MAX_LENGTH);
  pClusterCopy->redshift      = (float) pClusterOrig->redshift;
  pClusterCopy->num_gals      = (int)   pClusterOrig->num_gals;
  pClusterCopy->num_observed  = (int)   pClusterOrig->num_observed;
  pClusterCopy->num_complete  = (int)   pClusterOrig->num_complete;
  pClusterCopy->BCG_observed  = (BOOL)  pClusterOrig->BCG_observed;
  pClusterCopy->BCG_complete  = (BOOL)  pClusterOrig->BCG_complete;
  pClusterCopy->objid_BCG     = (int)   pClusterOrig->objid_BCG;
  pClusterCopy->successfully_observed = (BOOL) pClusterOrig->successfully_observed;
  pClusterCopy->deltaFoM              = (float) pClusterOrig->deltaFoM;
  pClusterCopy->redshift_bin_for_FoM  = (int) pClusterOrig->redshift_bin_for_FoM;

  return 0;
}
/////////// cluster_struct functions /////////////
////////////////////////////////////////////////////////////////////////////////////////////////

/////////// clusterlist_struct functions /////////////
int clusterlist_struct_init (clusterlist_struct **ppClusterList, int total_num_clusters)
{
  if ( ppClusterList == NULL ) return 1;

  if ( *ppClusterList != NULL ) return 1;
  
  //make space for the struct
  if ( (*ppClusterList = (clusterlist_struct*) malloc(sizeof(clusterlist_struct))) == NULL )
  {
    fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;    
  }
  //set the contents to their initial values 
  (*ppClusterList)->array_length = (int) 0;
  (*ppClusterList)->pCluster = (cluster_struct*) NULL;     //assume that this doesn't already point to an array
  (*ppClusterList)->nClusters = (int) 0;

  //now, if needed, then must make space for the array of Cluster structures
  if ( total_num_clusters > 0 )
  {
    int i;
    if ( ((*ppClusterList)->pCluster = (cluster_struct*) malloc(sizeof(cluster_struct) * total_num_clusters)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
    (*ppClusterList)->array_length = (int) total_num_clusters ;
    (*ppClusterList)->nClusters = (int) 0;

    
    //now initialise the array of Clusters
    for ( i=0;i<(*ppClusterList)->array_length;i++)
    {
      cluster_struct *pCluster =  (cluster_struct*) &((*ppClusterList)->pCluster[i]);
      if ( cluster_struct_init ((cluster_struct*) pCluster))
      {
        fprintf(stderr, "%s Problem initialising cluster_struct %d\n", ERROR_PREFIX, i);
        return 1;
      }   
    }
  }
  return 0;
}

int clusterlist_struct_tidy (clusterlist_struct *pClusterList)
{
  if ( pClusterList == NULL ) return 1;
  if ( pClusterList->pCluster == NULL ) return 0;
  
  if ( pClusterList->nClusters <= 0 )
  {
    free(pClusterList->pCluster);
    pClusterList->pCluster = NULL;
    pClusterList->nClusters = 0;
    pClusterList->array_length = 0;
  }
  else if ( pClusterList->nClusters < pClusterList->array_length )
  {
    pClusterList->array_length = pClusterList->nClusters;
    if ( (pClusterList->pCluster = (cluster_struct*) realloc(pClusterList->pCluster,
                                                             sizeof(cluster_struct) * pClusterList->array_length)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
  }
  else
  {
    //nothing to do
  }
  return 0;  
}
/////////// clusterlist_struct functions /////////////
int clusterlist_struct_add_cluster (clusterlist_struct *pClusterList, cluster_struct *pCluster)
{
  if ( pClusterList == NULL ||
       pCluster == NULL )
  {
    fprintf(stderr, "%s Null pointer(s) supplied to function at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
    return 1;
  }
  if ( pClusterList->nClusters >= pClusterList->array_length )
  {
    int i;
    int old_array_length = pClusterList->array_length;
    pClusterList->array_length += CLUSTERS_PER_CHUNK;
    //need to allocate some more space for the array.
    if ( (pClusterList->pCluster = (cluster_struct*) realloc(pClusterList->pCluster,
                                                             sizeof(cluster_struct) * pClusterList->array_length)) == NULL )
    {
      fprintf(stderr, "%s Problem re-assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
   
    //now initialise the new Clusters
    for ( i=old_array_length;i<pClusterList->array_length;i++)
    {
      cluster_struct *pCluster =  (cluster_struct*) &(pClusterList->pCluster[i]);
      if ( cluster_struct_init ((cluster_struct*) pCluster))
      {
        fprintf(stderr, "%s Problem initialising cluster_struct %d\n", ERROR_PREFIX, i);
        return 1;
      }   
    }
  }

  //now copy the contents of the input cluster into the list 
  if ( cluster_struct_copy((cluster_struct*) pCluster, (cluster_struct*) &(pClusterList->pCluster[pClusterList->nClusters])))
  {
    fprintf(stderr, "%s Problem copying cluster struct\n", ERROR_PREFIX);
    return 1;
  }

  //and increment the nClusters counter
  pClusterList->nClusters ++;
  return 0;
}


int clusterlist_struct_free (clusterlist_struct **ppClusterList)
{
  if ( ppClusterList == NULL ) return 1; 
  if ( *ppClusterList )
  {
    //free the array of Cluster structures
    if ( (*ppClusterList)->pCluster )
    {
      free((*ppClusterList)->pCluster);
      (*ppClusterList)->pCluster = NULL;
    }
    //now free the Clusterlist struct itself
    free(*ppClusterList);
    *ppClusterList = NULL;
  }
  return 0; 
}

//go through the list of objects in pCat and calc stats of objects belonging to each cluster   
int clusterlist_struct_calc_summary (clusterlist_struct *pClusterList,  objectlist_struct *pObjList, catalogue_struct* pCat)
{
  int i;
  if ( pClusterList == NULL ||
       pObjList == NULL ||
       pCat == NULL ) return 1; 

  fprintf(stdout, "%s Calculating summary for Clusters catalogue\n", COMMENT_PREFIX);

  
  for (i=pCat->index_of_first_object;i<=pCat->index_of_last_object; i++)
  {
    object_struct *pObj = (object_struct*) &(pObjList->pObject[i]); 
    if ( pObj->sub_code >= 0 &&
         pObj->sub_code < pClusterList->nClusters )
    {
      cluster_struct *pCluster = (cluster_struct*) &(pClusterList->pCluster[pObj->sub_code]);
      pCluster->num_gals ++;
      if ( pObj->TexpFracDone > (float) 0.0)
      {
        pCluster->num_observed ++;
        if ( pObj->id == (uint) pCluster->objid_BCG )
          pCluster->BCG_observed = TRUE;

        if ( pObj->TexpFracDone >= (float) 1.0)
        {
          pCluster->num_complete ++;
          if ( pObj->id == (uint) pCluster->objid_BCG )
            pCluster->BCG_complete = TRUE;
        }
      }
    }
    
  }
 
  return 0; 
}

int clusterlist_struct_write_summary (clusterlist_struct *pClusterList, objectlist_struct *pObjList, const char *str_filename)
{
  int i;
  FILE* pFile = NULL;
  if ( pClusterList == NULL ||
       str_filename == NULL ||
       pObjList == NULL ) return 1;

  fprintf(stdout, "%s Writing summary for Clusters catalogue to %s\n", COMMENT_PREFIX, str_filename);
  fflush(stdout);
  
  if ((pFile = fopen(str_filename, "w")) == NULL )
  {
    fprintf(stderr, "%s  There were problems opening the Clusters summary file: %s\n",
            ERROR_PREFIX, str_filename);
    return 1;
  }

  //write the full header
  {
    time_t rawtime;
    struct tm *timeinfo;
    
    if (time((time_t*) &rawtime) < 0 ) return 1;
    timeinfo = localtime(&rawtime);
    fprintf(pFile, "#This is the Clusters results file\n\
#This file was first written at: %s\
#Original filename: %s\n\
#This file contains one entry per Cluster\n\
#The file gives summary info about the results for each Cluster at the end of the survey\n\
#Description of columns:\n\
#Index  Name        Format    Description\n\
#-----  ----------- --------- ------------------------------------------------------------------------ \n\
#1      ID          int       running index of this Cluster\n\
#2      ClusterName string    The code name of this Cluster (1st 11 characters of BCG name)\n\
#3      RA_BCG      double    The RA coordinate of the BCG in this Cluster (deg,J2000)\n\
#4      Dec_BCG     double    The Dec coordinate of the BCG in this Cluster (deg,J2000)\n\
#5      redshift    float     The redshift of the BCG\n\
#6      BCG         bool      BCG status: YES if BCG succesfully observed, OBS if observed, NO if not observed\n\
#7      n_gals      int       Total no. galaxies in the Cluster\n\
#8      n_obs       int       The no. galaxies in Cluster that were observed during the survey\n\
#9      n_comp      int       The no. galaxies in Cluster that were successfully observed (completed) during the survey\n\
#10     deltaFoM    float     The contribution this Cluster adds to the FoM\n\
#-----  ----------- --------- ------------------------------------------------------------------------ \n",
          asctime(timeinfo),
          str_filename);
  }


  
  //write a quick header line
  fprintf(pFile, "#%-5s %-12s %11s %11s %8s %3s %6s %6s %6s %9s\n",
          "ID", "ClusterName", "RA_BCG", "Dec_BCG", "redshift", "BCG", "N_gals", "N_obs", "N_comp", "deltaFoM");

  for (i=0;i<pClusterList->nClusters; i++)
  {
    cluster_struct *pCluster = (cluster_struct*) &(pClusterList->pCluster[i]);
    object_struct  *pBCG     = (object_struct*) NULL;
    if ( pCluster->objid_BCG >= 0 &&
         pCluster->objid_BCG < pObjList->nObjects)
      pBCG = (object_struct*) &(pObjList->pObject[pCluster->objid_BCG]);
      
    fprintf(pFile, "%6d %-12s %11.7f %11.7f %8g %3s %6d %6d %6d %9.7f\n",
            pCluster->id,
            pCluster->name,
            (pBCG!=NULL?pBCG->ra:NAN),
            (pBCG!=NULL?pBCG->dec:NAN),
            pCluster->redshift,
            (pCluster->BCG_complete?"YES":(pCluster->BCG_observed?"OBS":"NO")),
            pCluster->num_gals,
            pCluster->num_observed,
            pCluster->num_complete,
            pCluster->deltaFoM);
    
  }

  if (pFile) fclose(pFile);
  pFile = NULL;
 
  return 0; 
}

/////////// Clusterlist_struct functions /////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////// assign_struct functions /////////////
int assign_struct_init (assign_struct *pAssign)
{
  if ( pAssign == NULL ) return 1;
  pAssign->status = (int) ASSIGN_STATUS_FREE;
  pAssign->pFib = (fiber_struct*) NULL;
  pAssign->pObj = (object_struct*) NULL;
  pAssign->object_x = (float) NAN;
  pAssign->object_y = (float) NAN;
  return 0;
}

//write the info from an assign struct to file
int assign_struct_write (assign_struct *pAssign, FILE *pFile, const char *str_prefix )
{
  char *str_status = NULL;
  if ( pAssign == NULL || pFile == NULL ) return 1;

  if ( pAssign->pFib == NULL ) return 1;
  
  switch (pAssign->status )
  {
  case ASSIGN_STATUS_FREE    : str_status = (char*) ASSIGN_STATUS_STR_FREE; break;
  case ASSIGN_STATUS_SCIENCE : str_status = (char*) ASSIGN_STATUS_STR_SCIENCE; break;
  case ASSIGN_STATUS_SKY     : str_status = (char*) ASSIGN_STATUS_STR_SKY; break;
  case ASSIGN_STATUS_CALIB   : str_status = (char*) ASSIGN_STATUS_STR_CALIB; break;
  default : return 1; break;
  }
  fprintf(pFile, "%s%6s %8d  %10.4f %10.4f %8d %10.4f %10.4f\n",
          (const char*) (str_prefix?str_prefix:""),
          (char*)       str_status,
          (int)         pAssign->pFib->id,
          (float)       pAssign->pFib->Geom.x0,
          (float)       pAssign->pFib->Geom.y0,
          (int)         (pAssign->pObj ? pAssign->pObj->id : -1),
          (float)       pAssign->object_x,
          (float)       pAssign->object_y);
  return 0;
}
/////////// assign_struct functions /////////////


////////////////////////
/////////// tile_struct functions /////////////
int tile_struct_init (tile_struct *pTile, int assigns_per_tile)
{
  if ( pTile == NULL ) return 1;

  //set the contents to their initial values 
  pTile->array_length = (int) 0;
  pTile->pAssign = (assign_struct*) NULL;       
  
  //set the contents to their initial values 
  pTile->dither_index = (int) -1;      
  pTile->time_gross = (float) NAN;          
  pTile->time_net   = (float) NAN;            
  pTile->pFocal = (focalplane_struct*) NULL; 
  pTile->pField = (field_struct*) NULL;

  pTile->nAssign = (int) 0;         //we will fill this up later       

  //now, if needed, then must make space for the array of assign_struct structures
  if ( assigns_per_tile  > 0 )
  {
    int i;
    if ( (pTile->pAssign = (assign_struct*) malloc(sizeof(assign_struct) * assigns_per_tile)) == NULL )
    {
      fprintf(stderr, "%s Problem assigning memory at file %s line %d\n", ERROR_PREFIX, __FILE__, __LINE__);
      return 1;    
    }
    pTile->array_length = (int) assigns_per_tile;
  
    
    //now initialise the array of assign structs
    for ( i=0;i<pTile->array_length;i++)
    {
      assign_struct *pAssign = (assign_struct*) &(pTile->pAssign[i]);
      if ( assign_struct_init ((assign_struct*) pAssign))
      {
        fprintf(stderr, "%s Problem initialising assign_struct\n", ERROR_PREFIX);
        return 1;
      }
    }
  }
  return 0; 
}

////////////////////////
int tile_struct_free (tile_struct **ppTile)
{
  if ( ppTile == NULL ) return 1;
  if ( *ppTile )
  {
    //free the array of assign structures
    if ( (*ppTile)->pAssign )
    {
      free((*ppTile)->pAssign);
    }
    //now free the tile struct itself
    free(*ppTile);
    *ppTile = NULL;
  }
  return 0; 
}

//write the info from a tile struct to a file
int tile_struct_write (tile_struct *pTile, FILE *pFile, const char *str_prefix)
{
  int i;
  if ( pTile == NULL || pFile == NULL ) return 1;

  fprintf(pFile, "%s%6s %8d %11.7f %+11.7f %8.3f %3d %8.1f %8.1f %8d\n",
          (const char*) (str_prefix ? str_prefix : ""),
          (char*)  (pTile->pField ? pTile->pField->name :"-"),
          (int)    (pTile->pField ? pTile->pField->id   :  -1),
          (double) (pTile->pField ? pTile->pField->ra0  : NAN),
          (double) (pTile->pField ? pTile->pField->dec0 : NAN),
          (double) (pTile->pField ? pTile->pField->pa0 : NAN),
          (int)    pTile->dither_index,
          (float)  pTile->time_gross,
          (float)  pTile->time_net,
          (int)    pTile->nAssign);

  //now write a line for each target
  for(i=0;i<pTile->nAssign;i++)
  {
    char str_prefix2[STR_MAX];
    snprintf(str_prefix2, STR_MAX, "%sAssignSummary %5d ", (str_prefix?str_prefix:""), i);
    if ( assign_struct_write ((assign_struct*) &(pTile->pAssign[i]), (FILE*) pFile, (const char*) str_prefix2)) return 1;
  }  
  return 0;
}
/////////// tile_struct functions /////////////


//////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////
//carry out a sky survey in the ESO mode
int carry_out_ESO_OT_survey   (survey_struct *pSurvey,
                               objectlist_struct *pObjList,
                               fieldlist_struct *pFieldList,
                               focalplane_struct *pFocal,
                               telescope_struct *pTele,
                               timeline_struct *pTime,
                               cataloguelist_struct *pCatList)
{

  OBlist_struct *pOBlist = NULL;
  int total_num_nights;

  if ( pSurvey    == NULL ||
       pObjList   == NULL ||
       pFieldList == NULL ||
       pFocal      == NULL ||
       pTime      == NULL ||
       pCatList   == NULL ) return 1;

  
  total_num_nights = (int) ceil(pSurvey->JD_stop-pSurvey->JD_start);
  fprintf(stdout, "%s Now I will carry out the ESO OT mode survey (JD_start,stop= %8.1f %8.1f, total nights= %d)... \n",
          COMMENT_PREFIX, pSurvey->JD_start, pSurvey->JD_stop, total_num_nights);    

  // steps to take over course of a survey:
  // * Initialise everything
  // * Set JDnow to date at beginning of survey
  // * Loop over survey nights:
  //   * At beginning of each night:
  //   * Is it time to check the OB pool? (e.g. weekly)
  //     * Yes -> Retrieve current OB status report from the pseudo-P2PP database 
  //           -> Retrieve Tile throughput reports from the pseudo-DMS 
  //           -> Retrieve Focal Plane status report from the pseudo-DMS 
  //           -> Log the results of the observed OBs/Tiles to the target DB
  //           -> Remove completed OBs from OB pool
  //           -> Is it time to refresh the OB pool? (e.g. 6 monthly)
  //              * Yes -> Remove all OBs from the pseudo-P2PP database 
  //           -> Evaluate the survey progress
  //           -> Are there too few unobserved OBs remaining in the pool?
  //              * Yes -> Create some more OBs
  //                    -> Prefer OBs that should be observed in the next ~month
  //   * Now carry out observations for a night in ESO OT mode
  //  * At end of survey write logs and calculate statistics
  
  //set up the OB structure
  if ( OBlist_struct_init ((OBlist_struct**) &pOBlist, (int) MAX_OBS_IN_POOL, (int) MAX_TILES_PER_OB, (int) pFocal->num_fibers))
  {
    fprintf(stderr, "%s Could not initialise the OBlist structure\n", ERROR_PREFIX);
    return 1;
  }


  if ( OBlist_struct_free ((OBlist_struct**) &pOBlist))
  {
    fprintf(stderr, "%s Could not free the OBlist structure\n", ERROR_PREFIX);
    return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////


int survey_run_gnuplot_on_plotfile (survey_struct *pSurvey, const char* strPlotfile )
{
  if ( pSurvey->do_plots == TRUE )
  {
    if ( util_run_gnuplot_on_plotfile ( (const char*) strPlotfile,
                                        (BOOL) (pSurvey->max_threads > 1 ? TRUE : FALSE)))
      return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////
