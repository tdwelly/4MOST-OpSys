//----------------------------------------------------------------------------------
//--
//--    Filename: 4FS_OpSim.h
//--    Author: Tom Dwelly (dwelly@mpe.mpg.de)
//--    Use: This is a header for the 4FS_OpSim.c C code file
//--
//--    Notes:
//--


#ifndef FOURFS_OPSIM_H
#define FOURFS_OPSIM_H

//#define CVS_REVISION_4FS_OPSIM_H "$Revision: 1.199 $"
//#define CVS_DATE_4FS_OPSIM_H     "$Date: 2015/11/11 10:53:19 $"

//standard header files - need this for FILE* pointers
#include <stdio.h>


//my header files which include structure definitions come next
#include "define.h"
#include "utensils_lib.h"
#include "params_lib.h"
#include "quadtree_lib.h"
#include "geometry_lib.h"
#include "hpx_extras_lib.h"
#include "fits_helper_lib.h"
#include "OpSim_timeline_lib.h"
#include "OpSim_telescope_site_lib.h"

//-----------------Info for printing code revision-----------------//
//#define CVS_REVISION_H "$Revision: 1.199 $"
//#define CVS_DATE_H     "$Date: 2015/11/11 10:53:19 $"
inline static int FourFS_OpSim_print_version_header(FILE* pFile)
{
  if ( pFile == NULL ) return 1;
  //  GENERIC_PRINT_VERSION(pFile, __FILE__, CVS_REVISION_H, CVS_DATE_H, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  GENERIC_PRINT_GIT_VERSION(pFile, __FILE__, GIT_VERSION, GIT_DATE, __TIMESTAMP__, __TIME__, __DATE__ , __VERSION__);
  return 0;
}
//#undef CVS_REVISION_H
//#undef CVS_DATE_H
//-----------------Info for printing code revision-----------------//

////////////////////////////////////
#ifndef STR_MAX
#define STR_MAX DEF_STRLEN
#endif
#ifndef LSTR_MAX
#define LSTR_MAX DEF_LONGSTRLEN
#endif
////////////////////////////////////


#define OPSIM_BRANDING_STRING      "4MOST Facility Simulator - OpSim"
#define OPSIM_BRANDING_PLOTSTRING_GRAPH  "set label 9999 \"4MOST Facility Simulator - OpSim\" at graph  0.98,0.02 font \",14\" tc lt 3 right" 
#define OPSIM_BRANDING_PLOTSTRING_SCREEN "set label 9999 \"4MOST Facility Simulator - OpSim\" at screen 0.98,0.02 font \",14\" tc lt 3 right" 
#define OPSIM_BRANDING_PLOTSTRING_UNSET  "unset label 9999"
#define CONTACT_EMAIL_ADDRESS "4most@mpe.mpg.de"
#define OPSIM_OUTPUT_HTML_GUIDE "http://www.mpe.mpg.de/~4most/4FS/OpSim/sim/4FS_OpSim_outputs_help.html"
#define OPSIM_OUTPUT_HTML_SUMMARY_PAGE "results.html"

#define MAX_DITHERS_PER_FIELD          6     //!< Need to keep this small to reduce memory useage
//#define MAX_FIELD_NEIGHBOURS          10   //!< This is higher than we normally expect to use (usually max neighbours=6)
#define MAX_FIELD_NEIGHBOURS          256    //!< This allows for the densely tiled WAVES area
//#define MAX_VERTICES_PER_TILE         12   //!< To allow two points per corner for extended vertices of hexagon
#define MAX_VERTICES_PER_TILE         1024   //!< Large but allows for many points per corner for extended vertices of hexa-/polygon
//#define MAX_FIELDS_PER_OBJECT          3  //!< This should be enough if we have only sensible overlaps
#define MAX_FIELDS_PER_OBJECT         255   //!< This allows for the densely tiled WAVES area
//                                          //!< i.e. objects appearing in up to three fields
#define MAX_FIELDS_PER_OBJECT_TEMP    255  //!< This is overkill - max value that fits in a utiny

#define MAX_THREADS_LIMIT              8   //!< We don't want to go crazy and squash the system
                                           //!< TODO Multi-thread handling is not really implemented yet

#define MAX_OBJECTS_PER_FIBER       4096   //!< TODO - make dynamic 
#define MAX_FIBERS_PER_OBJECT         64   //!< maybe better to keep this non-dynamical.
#define MAX_SECTORS_PER_TILE          21   //!< TODO - this could be made more flexible.
#define MAX_RIVALS_PER_FIBER         100   //!< TODO - make dynamical 

#define MAX_VERTICES_PER_POSITIONER    4   //!< TODO - move fiber_struct and fibergeom_structs to positioner_lib - tricky  
#define MAX_CATALOGUES                20   //!< this can easily be larger.
#define MAX_SUBFOMS                    100 //!< ditto
#define MAX_SUBCATS                    MAX_SUBFOMS
//#define MAX_TILES_PER_FIELD           64   //!< Keep bitmask within the size of a 64bit unsigned long 
#define MAX_TILES_PER_FIELD          200   //!< can just treat this as a normal limit now

/////////////////////////////////////////////////////////////////////
//the following control the granularity with which we allocate memory
#define OBJECTS_PER_CHUNK         1048576  //!< Binary one million 2^20
#define OBJECTS_PER_PID_CHUNK        5000  //!< An array at least this long is allocated once per field, and then resized/grown
//#define FIELDS_PER_CHUNK           100000  //
#define FIBERS_PER_CHUNK             5000  //!<
#define CLUSTERS_PER_CHUNK         100000  //!< 
/////////////////////////////////////////////////////////////////////

#define FIBER_CCD_MAPPING_NUM_SPECTROS_LO  2
#define FIBER_CCD_MAPPING_NUM_SPECTROS_HI  2
#define FIBER_CCD_MAPPING_NUM_SPECTROS_SW  1
#define FIBER_CCD_MAPPING_NUM_DETECTORS_PER_SPECTRO_LO  2
#define FIBER_CCD_MAPPING_NUM_DETECTORS_PER_SPECTRO_HI  1
#define FIBER_CCD_MAPPING_NUM_DETECTORS_PER_SPECTRO_SW  1
#define FIBER_CCD_MAPPING_NUM_XDISP_PIX_LO  6144   //!< 6k pix
#define FIBER_CCD_MAPPING_NUM_XDISP_PIX_HI  6144
#define FIBER_CCD_MAPPING_NUM_XDISP_PIX_SW  6144



#define MAX_EXPOSURE_TIME           9999.0 // Minutes, in order to stay in 4 character limit


#define FOM_FOR_OK         0.5 //!< A survey is deemed a success if this threshold is reached
                               //!< See also pSurvey->FoM_success_threshold
#define FOM_TO_COLOUR(x) ((x)>=FOM_FOR_OK?"green":"red")

#define FIELD_RADIUS_TOLERANCE_FACTOR        1.01 //!< Only used in function calc_field_bounds()
#define FIELD_NEIGHBOUR_TOLERANCE_MULTIPLIER  3.0 //!< Only used in function calc_field_bounds()
#define FIELD_NEIGHBOUR_OVERLAP_RATIO         2.0 //!< Only used in function setup_field_neighbours

#define DEFAULT_SURVEY_DEC_LIMIT_MIN    -70.0  //!< Used to decide if targets lie in valid range
#define DEFAULT_SURVEY_DEC_LIMIT_MAX    +20.0  //!< Used to decide if targets lie in valid range
//#define DEFAULT_PLOT_DEC_MIN            (DEFAULT_SURVEY_DEC_LIMIT_MIN - 5.0)  //!< to allow a small boundary around sky plots
//#define DEFAULT_PLOT_DEC_MAX            (DEFAULT_SURVEY_DEC_LIMIT_MAX + 5.0)  //!< to allow a small boundary around sky plots

#define TILING_NRA_BINS  72  //!< to give 5 deg wide steps
#define TILING_RA_BIN_WIDTH (360.0/(double)TILING_NRA_BINS) //!< 5 deg wide steps
#define TILING_RA_BIN_SMOOTH_FACTOR 11     //!< make sure this is an odd number
//#define TILING_RA_BIN_ASSIGN_WINDOW (TILING_RA_BIN_WIDTH+0.01) // a smoothing kernel used when assigning tiles to fields
//#define TILING_RA_BIN_ASSIGN_WINDOW TILING_RA_BIN_WIDTH // a smoothing kernel used when assigning tiles to fields



#define SURVEY_MAIN_MODE_TIMELINE 1   //!< Default mode
//#define SURVEY_MAIN_MODE_POOLED   2
#define SURVEY_MAIN_MODE_P2PP     3 //!< ESO P2PP mode - TODO

#define CATALOGUE_FORMAT_ASCII     1 //tbd
#define CATALOGUE_FORMAT_FITS      2

/// Note. catalogue_code must fit into a utiny
enum catalogue_code {
  CATALOGUE_CODE_S1 =   1,
  CATALOGUE_CODE_S2      ,
  CATALOGUE_CODE_S3      ,
  CATALOGUE_CODE_S4      ,
  CATALOGUE_CODE_S5      ,
  CATALOGUE_CODE_S6      ,        
  CATALOGUE_CODE_S7      ,
  CATALOGUE_CODE_S8      ,
  CATALOGUE_CODE_S9      ,
  CATALOGUE_CODE_S3_SUBCAT,
  CATALOGUE_CODE_S4_SUBCAT,
  CATALOGUE_CODE_RANDOM         ,
  CATALOGUE_CODE_EXTRA     = 100,
  CATALOGUE_CODE_NULL      = 255};      


#define CATALOGUE_CODENAME_ALL       "All"        //<! Short catalogue names 
#define CATALOGUE_CODENAME_S1        "GalHaloLR"  //<! Short catalogue names 
#define CATALOGUE_CODENAME_S2        "GalHaloHR"  //<! Short catalogue names 
#define CATALOGUE_CODENAME_S3        "GalDiskLR"  //<! Short catalogue names 
#define CATALOGUE_CODENAME_S4        "GalDiskHR"  //<! Short catalogue names 
#define CATALOGUE_CODENAME_S5        "Clusters"   //<! Short catalogue names 
#define CATALOGUE_CODENAME_S6        "AGN"        //<! Short catalogue names 
#define CATALOGUE_CODENAME_S7        "WAVES"      //<! Short catalogue names 
#define CATALOGUE_CODENAME_S8        "Cosmology"  //<! Short catalogue names    
#define CATALOGUE_CODENAME_S9        "1001MC"     //<! Short catalogue names 
#define CATALOGUE_CODENAME_RANDOM    "RANDOM"     //<! Short catalogue names 
#define CATALOGUE_CODENAME_ALLHIRES  "AllHiRes"   //<! Short catalogue names 
#define CATALOGUE_CODENAME_ALLLORES  "AllLoRes"   //<! Short catalogue names 

#define CATALOGUE_DISPLAYNAME_ALL       "All targets"                  //<! Long names for merged cats 
#define CATALOGUE_DISPLAYNAME_ALLHIRES  "All high-resolution targets"  //<! Long names for merged cats 
#define CATALOGUE_DISPLAYNAME_ALLLORES  "All low-resolution targets"   //<! Long names for merged cats 

#define CATALOGUE_ALTNAME_S1 "HaloLR"           //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S2 "HaloHR"           //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S3 "DiskLR"           //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S4 "DiskHR"           //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S5 "X-rayClusters"    //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S6 "X-rayAGN"         //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S7 "GalaxyEvolution"  //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S8 "Cosmology"        //<! Alternative short catalogue names 
#define CATALOGUE_ALTNAME_S9 "1001MC"           //<! Alternative short catalogue names 

#define CATALOGUE_SNAME_S1 "S1"
#define CATALOGUE_SNAME_S2 "S2"
#define CATALOGUE_SNAME_S3 "S3"
#define CATALOGUE_SNAME_S4 "S4"
#define CATALOGUE_SNAME_S5 "S5"
#define CATALOGUE_SNAME_S6 "S6"
#define CATALOGUE_SNAME_S7 "S7"
#define CATALOGUE_SNAME_S8 "S8"
#define CATALOGUE_SNAME_S9 "S9"
#define CATALOGUE_SNAME_EXTRA "EXTRA"


//#define NUM_SUBFOMS_S3     5
//#define SUBFOM_CODE_S3_ESN 0
//#define SUBFOM_CODE_S3_DB1 1
//#define SUBFOM_CODE_S3_DB2 2
//#define SUBFOM_CODE_S3_DB3 3
//#define SUBFOM_CODE_S3_DB4 4
// 
// #define NUM_SUBFOMS_S4     3
// #define SUBFOM_CODE_S4_D   0
// #define SUBFOM_CODE_S4_B   1
// #define SUBFOM_CODE_S4_H   2
// 
//#define NUM_SUBFOMS_S6           3
#define SUBFOM_CODE_S6_MAG       0
#define SUBFOM_CODE_S6_Z         1
#define SUBFOM_CODE_S6_ALL       2

//#define NUM_SUBFOMS_S8           2
#define SUBFOM_CODE_S8_AREA      0
#define SUBFOM_CODE_S8_NUMBER    1
// 
//#define NUM_SUBFOMS_S7            3
#define SUBFOM_CODE_S7_WIDE       0
#define SUBFOM_CODE_S7_DEEP       1
// #define SUBFOM_CODE_S7_ULTRADEEP  2

#define SUBCATALOGUE_CODE_S3_UNKNOWN      0
#define SUBCATALOGUE_CODE_S3_ESN          1
#define SUBCATALOGUE_CODE_S3_DYNDISK1     2
#define SUBCATALOGUE_CODE_S3_DYNDISK2     3
#define SUBCATALOGUE_CODE_S3_CHEMDISK     4
#define SUBCATALOGUE_CODE_S3_BULGE        5
#define NUM_SUBCATS_S3                    6
#define SUBCATALOGUE_CODENAME_S3_ESN       "GalDiskLR_ESN"       // 
#define SUBCATALOGUE_CODENAME_S3_DYNDISK1  "GalDiskLR_DYNDISK1"  // 
#define SUBCATALOGUE_CODENAME_S3_DYNDISK2  "GalDiskLR_DYNDISK2"  // 
#define SUBCATALOGUE_CODENAME_S3_CHEMDISK  "GalDiskLR_CHEMDISK"  // 
#define SUBCATALOGUE_CODENAME_S3_BULGE     "GalDiskLR_BULGE"     // 
#define SUBCATALOGUE_CODENAME_S3_SUBCAT    "GalDiskLR_SUBCAT"     // 


// #define SUBCATALOGUE_CODE_S4_UNKNOWN      0
// #define SUBCATALOGUE_CODE_S4_OUTERDISK    1
// #define SUBCATALOGUE_CODE_S4_INNERDISK    2
// #define SUBCATALOGUE_CODE_S4_BULGE        3
// #define NUM_SUBCATS_S4                    4

#define SUBCATALOGUE_CODE_S4_UNKNOWN      0
#define SUBCATALOGUE_CODE_S4_BULGE        1
#define SUBCATALOGUE_CODE_S4_INNERDISK    2
#define SUBCATALOGUE_CODE_S4_OUTERDISK    3
#define SUBCATALOGUE_CODE_S4_UNBIASED     4
#define NUM_SUBCATS_S4                    5

#define SUBCATALOGUE_CODENAME_S4_BULGE        "GalDiskHR_BULGE"
#define SUBCATALOGUE_CODENAME_S4_INNERDISK    "GalDiskHR_INNERDISK"
#define SUBCATALOGUE_CODENAME_S4_OUTERDISK    "GalDiskHR_OUTERDISK"
#define SUBCATALOGUE_CODENAME_S4_UNBIASED     "GalDiskHR_ZUNBIASEDDISK"
#define SUBCATALOGUE_CODENAME_S4_SUBCAT       "GalDiskHR_SUBCAT"     // 

// #define SUBCATALOGUE_CODE_S4_UNKNOWN          255
// #define SUBCATALOGUE_CODE_S4_DB_HR_DISK       SUBFOM_CODE_S4_D
// #define SUBCATALOGUE_CODE_S4_DB_HR_BULGE      SUBFOM_CODE_S4_B
// #define SUBCATALOGUE_CODE_S4_DB_HR_HIGHLAT    SUBFOM_CODE_S4_H
// #define SUBCATALOGUE_CODE_S4_SUBSET1          1
// #define SUBCATALOGUE_CODE_S4_SUBSET2          2
// #define SUBCATALOGUE_CODE_S4_SUBSET3          3

#define SUBCATALOGUE_CODE_S6_UNKNOWN          0
#define SUBCATALOGUE_CODE_S6_SUBSET1          1

#define SUBCATALOGUE_CODE_S7_UNKNOWN          0
#define SUBCATALOGUE_CODE_S7_DEEP             1
#define SUBCATALOGUE_CODE_S7_WIDE             2
//#define SUBCATALOGUE_CODE_S7_ULTRADEEP        SUBFOM_CODE_S7_ULTRADEEP

#define SUBCATALOGUE_CODE_S8_UNKNOWN          255
#define SUBCATALOGUE_CODE_S8_SUBSET1          1

#define SUBCATALOGUE_CODE_S9_UNKNOWN             0
#define SUBCATALOGUE_CODE_S9_HR_SUBSET1          1
#define SUBCATALOGUE_CODE_S9_HR_SUBSET2          2
#define SUBCATALOGUE_CODE_S9_HR_SUBSET3          3
#define SUBCATALOGUE_CODE_S9_HR_SUBSET4          4
#define SUBCATALOGUE_CODE_S9_HR_SUBSET5          5
#define SUBCATALOGUE_CODE_S9_HR_SUBSET6          6
#define SUBCATALOGUE_CODE_S9_HR_SUBSET7          7
#define SUBCATALOGUE_CODE_S9_HR_SUBSET8          8
#define SUBCATALOGUE_CODE_S9_HR_SUBSET9          9
#define SUBCATALOGUE_CODE_S9_HR_SUBSET10        10
#define SUBCATALOGUE_CODE_S9_LR_SUBSET1          1
#define SUBCATALOGUE_CODE_S9_LR_SUBSET2          2
#define SUBCATALOGUE_CODE_S9_LR_SUBSET3          3
#define SUBCATALOGUE_CODE_S9_LR_SUBSET4          4
#define SUBCATALOGUE_CODE_S9_LR_SUBSET5          5
#define SUBCATALOGUE_CODE_S9_LR_SUBSET6          6
#define SUBCATALOGUE_CODE_S9_LR_SUBSET7          7
#define NUM_HR_SUBCATS_S9  11
#define NUM_LR_SUBCATS_S9  8



#define MOON_GROUP_BB 0
#define MOON_GROUP_DG 1
#define MOON_GROUP_NULL -1
#define MAX_NUM_MOON_GROUPS 2
#define MOON_GROUP_CODENAME_BRIGHT   "bb"
#define MOON_GROUP_CODENAME_DARKGREY "dg"
#define MOON_GROUP_LONGNAME_BRIGHT   "Bright"
#define MOON_GROUP_LONGNAME_DARKGREY "DarkGrey"



#define MOON_PHASE_NULL  -1
#define MOON_PHASE_BRIGHT 0
#define MOON_PHASE_GREY   1
#define MOON_PHASE_DARK   2
#define MAX_NUM_MOON_PHASES   3


#define MOON_PHASE_CODENAME_BRIGHT   "bb"
#define MOON_PHASE_CODENAME_GREY     "gg" 
#define MOON_PHASE_CODENAME_DARK     "dd"

#define MOON_PHASE_LONGNAME_BRIGHT   "Bright"
#define MOON_PHASE_LONGNAME_GREY     "Grey" 
#define MOON_PHASE_LONGNAME_DARK     "Dark"

//following is appox from here: http://www.eso.org/observing/etc/doc/ut1/fors/helpfors.html#sky
#define SKY_BRIGHTNESS_THRESH_GREYDARK   21.60  //the sky brightness (in magnitude/arcsec2) threshold
                                           //above which we are in dark time.
#define SKY_BRIGHTNESS_THRESH_BRIGHTGREY 20.80  //the sky brightness (in magnitude/arcsec2) threshold
                                           //below which we are in bright time.

#define RESOLUTION_CODE_NULL       0      //placeholder code
#define RESOLUTION_CODE_LOW        1      //these codes are used to describe the object/fiber resolution
#define RESOLUTION_CODE_HIGH       2
#define RESOLUTION_CODE_DUAL       3
#define NRESOLUTION_CODES 4      // manually increment when add more fibre res modes

#define RESOLUTION_CODE_STRING(x) \
((x)==RESOLUTION_CODE_NULL      ? "Null" : \
((x)==RESOLUTION_CODE_LOW       ? "LR"  : \
((x)==RESOLUTION_CODE_HIGH      ? "HR" : \
((x)==RESOLUTION_CODE_DUAL      ? "xR" : \
 "?R"))))


///////////////////Got to here/////////////////////////
//these codes/bitmasks are used to describe which objects/fields lie in which parts of the sky survey 
#define SURVEY_NONE            ((unsigned int) 0x00000000)
#define SURVEY_BRIGHT          ((unsigned int) 0x00000001)
#define SURVEY_GREY            ((unsigned int) 0x00000002)
#define SURVEY_DARK            ((unsigned int) 0x00000004)
#define SURVEY_DARKGREY        ((unsigned int) (SURVEY_DARK | SURVEY_GREY))
#define SURVEY_ANY             ((unsigned int) 0x0000ffff)

//these flags are useful to see which fields would have been observed if there had been enough time
#define SURVEY_IN_AREA_BRIGHT   ((unsigned int) 0x00010000)
#define SURVEY_IN_AREA_GREY     ((unsigned int) 0x00020000)
#define SURVEY_IN_AREA_DARK     ((unsigned int) 0x00040000)
#define SURVEY_IN_AREA_DARKGREY ((unsigned int) (SURVEY_IN_AREA_DARK | SURVEY_IN_AREA_GREY))
#define SURVEY_IN_AREA_ANY      ((unsigned int) 0xffff0000)

//------------------------------------------------------------------------------------
#define SURVEY_STRATEGY_FLAT  1   //these are all related to the survey strategy/goal 
#define SURVEY_STRATEGY_DBPDG 2
#define SURVEY_STRATEGY_MANUAL 3

//#define SURVEY_GOAL_AREA      1
//#define SURVEY_GOAL_EXPOSURE  2

#define SURVEY_MOON_SPLIT_BY_MAGNITUDE   1
#define SURVEY_MOON_SPLIT_BY_EXPOSURE    2
#define SURVEY_MOON_SPLIT_BY_SKY_AREA    3

#define TILING_METHOD_HARDIN_SLOANE 1
#define TILING_METHOD_GEODESIC      2
#define TILING_METHOD_FILE          3

#define MAX_PRIORITY_RESCALE_PARAMS      10    //super overkill!
#define PRIORITY_RESCALE_TYPE_NONE      1
#define PRIORITY_RESCALE_TYPE_LINEAR    2
#define PRIORITY_RESCALE_TYPE_QUADRATIC 3
//etc etc

#define TARGET_TO_FIELD_POLICY_FIRST    1      //object goes into only the first valid field we find
#define TARGET_TO_FIELD_POLICY_CLOSEST  2      //object goes into only the closest valid field
#define TARGET_TO_FIELD_POLICY_ALL      3      //object goes into all valid fields
#define TARGET_TO_FIELD_POLICY_SPARSEST 4      //object goes into the least well populated field 

#define FIELD_FLAG_OUTSIDE 0   //these are used to record if an object lies in the main part
#define FIELD_FLAG_INSIDE  1   //or at the edge area of a field
#define FIELD_FLAG_BORDER  2


#define MAX_FIELD_WEIGHTING_PARAMS 16   //TODO - implement this functionality

#define FIELD_WEIGHTING_METHOD_STANDARD   1
#define FIELD_WEIGHTING_METHOD_VISIBILITY 2
#define FIELD_WEIGHTING_METHOD_ESO_OT     3

#define FIELD_FOM_PLINDEX_AIRMASS    1.25 //TODO - make these adjustable input parameters
#define FIELD_FOM_PLINDEX_MOON_DIST  -1.0


enum tile_planning_method {
  TILE_PLANNING_METHOD_UNKNOWN               =-1, 
  TILE_PLANNING_METHOD_ALL                   = 1, 
  TILE_PLANNING_METHOD_RANDOM                , 
  TILE_PLANNING_METHOD_DEC                   , 
  TILE_PLANNING_METHOD_MIN_AIRMASS           ,
  TILE_PLANNING_METHOD_GAL_LAT               ,
  TILE_PLANNING_METHOD_VISIBILITY            ,
  TILE_PLANNING_METHOD_SUM_FIBERHOURS        ,
  TILE_PLANNING_METHOD_SUM_PRIORITY          ,
  TILE_PLANNING_METHOD_SUM_PRIORITY_WEIGHTED 
};
  
#define TILE_PLANNING_METHO_STRING(x) \
((x)==TILE_PLANNING_METHOD_ALL            ? "All"            : \
((x)==TILE_PLANNING_METHOD_RANDOM         ? "Random"         : \
((x)==TILE_PLANNING_METHOD_DEC            ? "Dec"            : \
((x)==TILE_PLANNING_METHOD_MIN_AIRMASS    ? "MinAirmass"     : \
((x)==TILE_PLANNING_METHOD_GAL_LAT        ? "|Gal Lat.|"     : \
((x)==TILE_PLANNING_METHOD_VISIBILITY     ? "Visibility"     : \
((x)==TILE_PLANNING_METHOD_SUM_FIBERHOURS ? "sumFiber-Hours" : \
((x)==TILE_PLANNING_METHOD_SUM_PRIORITY   ? "sumPriority"    : \
 "Unknown"))))))))



#define TILE_STATUS_FULL_EXPOSURE    0
#define TILE_STATUS_PARTIAL_EXPOSURE 1
#define TILE_STATUS_NO_EXPOSURE      2

#define SURVEY_MEAN_SLEW_DISTANCE 30.0  //degrees

#define SURVEY_OUTPUT_CATALOGUE_MODE_NONE   0
#define SURVEY_OUTPUT_CATALOGUE_MODE_MERGED 1
#define SURVEY_OUTPUT_CATALOGUE_MODE_INDIVIDUAL 2

#define FIBER_OFFSET_HISTO_NPIX 100

#define FIELDLIST_SELECT_ON_MOON_PHASE_NONE 0    //used by fieldlist_select_candidate_fields()
#define FIELDLIST_SELECT_ON_MOON_PHASE_STRICT 1
#define FIELDLIST_SELECT_ON_MOON_PHASE_STRICT_INVERSE 2

#define PLOTFILES_SUBDIRECTORY              "plotfiles"
#define FIELD_RANKING_SUBDIRECTORY          "field_ranking"
#define FIBER_ASSIGNMENT_SUBDIRECTORY       "fiber_assignment_files"
#define FIBER_ASSIGNMENT_STEM               "fiber_assignment"
#define TARGET_DETAILS_STEM                 "target_details"
#define FIBER_POSITIONS_STEM                "fiber_positions"
#define OUTPUT_CATALOGUE_STEM               "object_summary_file"
#define FIBER_OFFSET_SUBDIRECTORY           "fiber_offsets"
#define PROGRESS_REPORT_SUBDIRECTORY        "progress_reports"
#define OUTPUT_STATS_SUBDIRECTORY           "output_stats"
#define INPUT_STATS_SUBDIRECTORY            "input_stats"


#define VERSION_INFO_FILENAME               "OpSim_version_info.txt"
#define INTERPRETED_PARAMETER_FILENAME      "OpSim_interpreted_parameters.txt"
#define FIELD_VERTICES_FILENAME             "field_vertices.txt"
#define FIELD_VERTICES_EXTENDED_FILENAME    "field_extended_vertices.txt"
#define FIELD_DITHERED_VERTICES_FILENAME    "field_dithered_vertices.txt"
#define FIELD_DITHERED_EXTENDED_VERTICES_FILENAME    "field_dithered_extended_vertices.txt"
#define SKY_TILING_REQUESTED_STEM           "sky_tiling_requested"
#define SKY_TILING_PLANNED_STEM             "sky_tiling_planned"
#define SKY_TILING_EXECUTED_STEM            "sky_tiling_executed"
#define SKY_BRIGHTNESS_HISTO_FILENAME       "zenith_sky_brightness_histo.txt"
#define SKY_VISIBILITY_STEM                 "sky_visibility"
#define RA_PRESSURE_STEM                    "predicted_tiling_stats_vs_RA"
#define TILES_USED_WASTED_STEM              "used_and_wasted_tiles_vs_RA"
#define FIBER_EFFICIENCY_TRENDS_STEM        "fiber_efficiency_trends"
#define FIBER_EFFICIENCY_MAPS_STEM          "fiber_efficiency_maps"
#define FIBER_OFFSETS_STEM                  "fiber_offset_histo_XY"
#define FIBER_USAGE_STEM                    "fiber_usage"

#define PER_FIELD_LOG_FILENAME              "per_field_logfile.txt"
#define PER_FIELD_LOG_FILENAME_FITS         "field_logfile.fits"
#define PER_TILE_LOG_FILENAME               "per_tile_logfile.txt"
#define TILE_LOG_FILENAME_FITS              "tile_logfile.fits"
#define TILE_REPORTS_LOG_FILENAME           "tile_reports.txt"
#define NIGHT_REPORTS_LOG_FILENAME          "night_reports.txt"
#define TIMELINE_REPORTS_LOG_FILENAME       "timeline_reports.txt"
#define FIBERS_REPORT_LOG_FILENAME          "fibers_report.txt"
#define CLUSTERS_SUMMARY_FILENAME           "Clusters_stats_summary.txt"
#define TILES_PER_FIELD_STATS_FILENAME      "tiles_per_field_stats_delivered.txt"
#define TILING_STATS_VS_RA_FILENAME         "predicted_tiling_stats_vs_RA.txt"
#define WASTED_TILES_VS_RA_FILENAME         "used_and_wasted_tiles_vs_RA.txt"
#define FOM_PROGRESS_PLOT_STEM              "FoM_progress"
#define FOM_RESULTS_FILENAME                "FoM_results.txt"


#define TILING_PATTERNS_STR_FORMAT_HARDIN   "/home/4most/4most/sky_tiling_patterns/hardin-sloane/pointings_hardin%d_format.txt"
#define TILING_PATTERNS_STR_FORMAT_GEODESIC "/home/4most/4most/sky_tiling_patterns/geodesic/geodesic_tiling_pattern_npts%d.txt"

#define THUMBNAIL_HEIGHT_PIXELS             50

#define CLUSTERS_NAME_MAX_LENGTH            16 //only 11+1 characters are actually needed

#define SORTFIELDLIST_ESO_OT_PZ_EXPONENT       1.7
#define SORTFIELDLIST_ESO_OT_HZ_AIRMASS_NORM   1.7
#define SORTFIELDLIST_STANDARD_PASS_FINISHED_FACTOR 10.0    

#define OBJECT_WEIGHTING_DEFAULT_HIRES_BONUS               "20.0"
#define OBJECT_WEIGHTING_DEFAULT_CANNOT_COMPLETE_FACTOR    "0.5"
#define OBJECT_WEIGHTING_DEFAULT_STARTED_FACTOR            "1.01"
#define OBJECT_WEIGHTING_DEFAULT_COMPLETED_EXPONENT        "-2.0"
#define OBJECT_WEIGHTING_DEFAULT_HARD_IN_BRIGHT_DECREMENT  "-10.0"
#define OBJECT_WEIGHTING_DEFAULT_HARD_IN_BRIGHT_THRESHOLD  "2.0"

#define FIELD_VISIBILITY_AM_NUM_STEPS 6      //number of steps in airmass that are recorded 
#define FIELD_VISIBILITY_AM_STEP_SIZE 0.1    //step size in airmass:
                                             // step 0 is for AM < 1.0+FIELD_VISIBILITY_STEP_SIZE
                                             // step 1 is for AM < 1.0+2*FIELD_VISIBILITY_STEP_SIZE etc


#define HPX_NSIDE_FOR_ASSOCIATIONS 32 //TODO determine this dynamically from field of view radius
#define HPX_MEMBERS_PER_CHUNK 512
#define HPX_MAX_PIXELS_PER_FIELD 32




typedef struct {
  char   name[STR_MAX];
  uint   id;
  double unit_coords[3];  //location of the middle of the field in the linear x,y,z unit coords
  double ra0;             //in deg   //these are the ra,dec of the nominal field center
  double dec0;            //in deg

  uint   hpx_pix;                                //pixel containing field centre
  long   hpx_npix;                               //number of pixels overlapping field
  uint   hpx_pixlist[HPX_MAX_PIXELS_PER_FIELD];  //list of pixels overlapping field
  
  double pa0;             //pointing angle (degrees east of north).
                          //Note when sides are parallel with lines of latitude then pa0 = 30,90,150,210,270,330 deg
                          //                      when a point is to the north, then pa0 = 0,60,120,180,240,300 deg
  double b0;              //Galactic longitude, degrees   of the nominal field center          
  double l0;              //Galactic latitude, degrees  of the nominal field center
  double ra_min;          //in deg
  double ra_max;          //in deg
  double dec_min;         //in deg
  double dec_max;         //in deg
  double ra_min2;          //in deg //extra wide boundary - for finding neighbours
  double ra_max2;          //in deg
  double dec_min2;         //in deg
  double dec_max2;         //in deg
  float  radius_deg;      //in deg

  int num_dithers;
  double pra0[MAX_DITHERS_PER_FIELD]; //these are the dithered positions of the field center 
  double pdec0[MAX_DITHERS_PER_FIELD];
  double pb0[MAX_DITHERS_PER_FIELD]; //and in Galactic coords
  double pl0[MAX_DITHERS_PER_FIELD];
  
  uint   in_survey; //this is a bit field that gets set to true if we want to use this field for
                          //each element of the surveys
                          //we keep out-of-survey fields in order to calculate the orientation of their neighbours.
  
  //should be incremented every time a object is assigned to this pointing
  int n_object;      
  int n_object_lores;      
  int n_object_hires;      
 //this will contain a list of the id numbers of all the potential objects in the field 
  uint *pid;
  int pid_data_length; //the actual length of the pid array 
  //this will contain a list of the sub-index number of each object
  //(ie which of the fields_per_object is appropriate for this field)
  int *pobject_sub_index;

  
  //this is to hold info about the neighbouring pointings
  int    num_neighbours;
  int    id_neighbour[MAX_FIELD_NEIGHBOURS];
  double dist_neighbour[MAX_FIELD_NEIGHBOURS]; //in degrees
  double bearing_neighbour[MAX_FIELD_NEIGHBOURS]; //in degrees

  int    id_nearest_neighbour; 
  double dist_nearest_neighbour; 

  //parameters for the hexagon
  double cos_dec0;
  double sin_dec0;
  double cos_radius;
  double furthest_radius_deg;
  double cos_furthest_radius_deg;

  int    num_vertex;
  double vertex_ra[MAX_DITHERS_PER_FIELD][MAX_VERTICES_PER_TILE]; //in degrees
  double vertex_dec[MAX_DITHERS_PER_FIELD][MAX_VERTICES_PER_TILE]; //in degrees

  //the following approximate the area covered if we consider the reach of the positioners
  int    num_ext_vertex;
  double ext_vertex_ra[MAX_DITHERS_PER_FIELD][MAX_VERTICES_PER_TILE]; //in degrees
  double ext_vertex_dec[MAX_DITHERS_PER_FIELD][MAX_VERTICES_PER_TILE]; //in degrees

  float  airmass_min; //the minimum airmass that can be acheived for this field
  double ESO_OT_hz;   //the number of hours that the field is above the airmass limit
  double ESO_OT_Pz;   //as above by normalised by the hz for a field with Dec=ObservatoryLatitude

  //these are interim variable for setting up observations of the fields
  int ntiles_total; //the total number of tiles observed/planned for this field
  int ntiles_req[MAX_NUM_MOON_GROUPS];  //the number of tiles that we would like to observe in this field, per moon group
  int ntiles_todo[MAX_NUM_MOON_GROUPS]; //the number of tiles that we will actually observe in this field, per moon group
  int ntiles_done[MAX_NUM_MOON_PHASES];  //the number of tiles that have been observed in this field so far, per moon phase
  int ntiles_visible[MAX_NUM_MOON_GROUPS]; //the number of tiles where this field was visible (airmass,moon constraints) to observe, per moon group
  // The following is the relative number of tiles where this field is expected to be visible
  // (airmass,moon constraints) to observe , per moon group
  float ntiles_visible_predict[MAX_NUM_MOON_GROUPS]; 

  // The following is the relative number of tiles where this field is expected to
  // be visible (airmass,moon constraints) to observe, per moon group, weighted by
  // the inverse of the sum of the requested tiles for fields that are visible in each time slot
  float ntiles_visible_predict_weighted[MAX_NUM_MOON_GROUPS];

  int ntiles_visible_am[MAX_NUM_MOON_GROUPS][FIELD_VISIBILITY_AM_NUM_STEPS]; //the number of tiles where this field was visible (airmass,moon constraints) to observe, per moon group and per AM limit
  float ntiles_visible_am_predict[MAX_NUM_MOON_GROUPS][FIELD_VISIBILITY_AM_NUM_STEPS]; //the relative number of tiles where this field is expected to be visible (airmass,moon constraints) to observe , per moon group, per moon group and per AM limit

  
  int ntiles_per_dither[MAX_DITHERS_PER_FIELD];  //number of tiles for each dither position

  float texp_total_todo;  // sum of all the exposure time that is expected for this field - mins
  float texp_total_done;  // sum of all the exposure time that has been executed for this field - mins
  
  float object_priority_total;               //the summed object priority weight in this field
  float object_priority_completed;           //the summed object priority weight completed so far in this field
  float object_priority_remaining;           //the summed object priority weight still remaining in this field
  float object_priority_total_weighted;      //Same as above but weighted by number of fields object lies within
  float object_priority_completed_weighted;  //
  float object_priority_remaining_weighted;  //

  float object_dark_fhrs_lores; //the summed required fiber-hours (if done in dark/grey time) for lo-res objects in this field
  float object_dark_fhrs_hires; //             "                   "                              hi-res           "

  int last_dither_index;           //index of the last dither position observed on this field

} field_struct;
////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////
//wrapper
typedef struct {
  char str_filename[STR_MAX];
  int  nFields;
  int  num_allocated_fields;
  int  tiling_method;   //a code to control how fields are placed on sky
  int  tiling_num_points; //how many points in the pattern? (full sky)
  BOOL all_PAs_were_supplied_in_input_file;
  field_struct *pF;
  //  quad_struct Quad;    //This used to be a separate top level structure, but it is always used in
  //                       //relation to the fieldlist so now put inside this structure
  hpx_pixlist_struct Pixlist;
} fieldlist_struct;

typedef struct {
  char filename[STR_MAX];
  char codename[STR_MAX];
  char displayname[STR_MAX];
  char sname[STR_MAX];          //ie S1,S2....
  int  index;                   //index of this catalogue within the container struct
  utiny code;                   //catalogue code;
  int  num_lines;               //number of lines in the input file (including comments and blanks)
  int  num_objects;             //number of objects in the file (ie number of non blank, non comment lines)
  int  num_valid_objects;       //number of objects meeting basic criteria
  int  index_of_first_object;   //index of the first and last objects in this catalogue in the objectlist struct
  int  index_of_last_object;
  BOOL is_extragalactic; 
  BOOL is_in_updated_format;  //set to true in input params if we have extra texp columns inserted by the DQCT
  int  format_code;          //

  
  float FoM;                        //keep a log of the primary FoM for this catalogue
  int   num_subFoMs;                //number of sub-FoMs for this catalogue
  float subFoM[MAX_SUBFOMS];        //used for any sub-components of the FoM. 
  float completeness;               //used for display purposes only
  float completeness_hires;         //used for display purposes only
  float completeness_lores;         //used for display purposes only

  int num_valid_objects_subcat[MAX_SUBCATS];  //number of objects in each subcatalogue;
  
  int   priority_rescale_method;
  float priority_rescale_param1;
  float priority_rescale_param2;
  float exposure_scaling;
  float fiber_hours_moon[MAX_NUM_MOON_GROUPS];    //total number of fiber hours consumed by this catalogue in each moon group
  float fiber_hours_hires_moon[MAX_NUM_MOON_GROUPS];    //
  float fiber_hours_lores_moon[MAX_NUM_MOON_GROUPS];    //
  float fiber_hours_hires;
  float fiber_hours_lores;
  float fiber_hours;

  int num_objects_hires;    //number of objects of the given type
  int num_objects_lores;
  int num_objects_exgal;
  int num_objects_exgal_lores;
  int num_objects_exgal_hires;
  int num_objects_gal;
  int num_objects_gal_lores;
  int num_objects_gal_hires;
  int num_objects_possible;
  int num_objects_impossible;

  double req_fhrs[MAX_NUM_MOON_PHASES];
  double req_fhrs_lores[MAX_NUM_MOON_PHASES];
  double req_fhrs_hires[MAX_NUM_MOON_PHASES];

  int sub_code_min;  //range of sub_codes within this catalogue
  int sub_code_max;
  
} catalogue_struct;


typedef struct {
  int  num_cats;
  catalogue_struct *pCat;

  int num_cats_hires;    //number of catalogues comntaining at least one of the given target type
  int num_cats_lores;
  int num_cats_exgal;
  int num_cats_exgal_lores;
  int num_cats_exgal_hires;
  int num_cats_gal;
  int num_cats_gal_lores;
  int num_cats_gal_hires;

  double req_fhrs[MAX_NUM_MOON_PHASES];
  double req_fhrs_lores[MAX_NUM_MOON_PHASES];
  double req_fhrs_hires[MAX_NUM_MOON_PHASES];

  int longest_cat_filename;
  int longest_cat_codename;
  int longest_cat_displayname;
} cataloguelist_struct;

// This structure contains info about 
// the field in which an object lies 
typedef struct {
  uint   field_id;   // ID of field that this object lies within;
  utiny  perim_flag; // flag that shows if object was in inner part or was in perimeter of the field
  //                 // The perim flags show where the object was in the __first__ dither position.

  uint   nfibs;      //number of fibers that could reach this object 
  utiny  ntiles;     //number of tiles that this object was observed in 
  float  ptexp[MAX_NUM_MOON_PHASES]; //delivered exposure time in each moon phase (minutes)

} object_field_struct;


/////this needs to be as tightly defined as possible to avoid using up all the RAM
typedef struct {
  uint   id;            //internal index records position of this struct in the objectlist struct array 
  uint   line_number;   //position of this record within the input catalogue file
  double ra;            
  double dec;

  float  priority;       //to be renormalised internally
  catalogue_struct *pCat;
  int    sub_code;       //used only for GalDiskLR,GalDiskHR,Clusters,WAVES DRSs
                         //has to be int so that have enough ids for the tens of thousands of clusters
  utiny  res;         
  BOOL   to_observe;  //

  float  mag;  
  float  redshift;       //used only for extragalactic catalogues
  float  treq[MAX_NUM_MOON_PHASES]; //required/requested exposure time (minutes)  
  uint   in_survey;  //this is a bit field, with a bit that gets set to 1 if
                     //we want to use this object in that sub-survey (eg BRIGHT, DARKGREY)

  utiny  num_fields; //number of fields that this object lies within
                     // utiny is sufficient only when MAX_FIELDS_PER_OBJECT <= 255

  object_field_struct *pObjField; // see above

  float  TexpFracDone;                     //executed exposure time as a fraction of original request
  uint   healpix_pix;                      //the index of the healpix pixel in which this object lies - for mapping
  uint   hpx_pix;                          //the index of the healpix pixel in which this object lies - for association with fields

  // note that the following is primarily used for debug purposes
  // so consider removing it from the object_struct for production runs
  // can save MAX_FIELDS_PER_OBJECT*8 bytes = 96 bytes per object
  //ulong  pwas_observed[MAX_FIELDS_PER_OBJECT]; //this receives the results of the fiber assignments 
  ulong  was_observed_last_tile;                //this receives the results of the fiber assignments in the last tile 

} object_struct;

//wrapper
typedef struct {
  int nObjects;
  int max_objects;
  int max_objects_per_catalogue;  //this is an input param
  int max_num_fields_per_object;  //this is calculated internally

  int array_length;
  object_struct *pObject;
} objectlist_struct;


typedef struct {
  int  id;
  char name[CLUSTERS_NAME_MAX_LENGTH];
  float redshift;
  int num_gals;
  int num_observed;
  int num_complete;
  BOOL BCG_observed;
  BOOL BCG_complete;
  int objid_BCG;  //index in the objectlist of the BCG 
  float deltaFoM; //the contribution to the total FoM associated with this Cluster
  BOOL successfully_observed;
  int redshift_bin_for_FoM;
} cluster_struct;

typedef struct {
  int array_length;                    //actual length of the array pointed to by pCluster
  int nClusters;                       //number of Clusters currently in the pCluster array
  cluster_struct *pCluster;            //pointer to array of Cluster_structs
} clusterlist_struct;


typedef struct {
  float  x0;  //starting coords
  float  y0;
  float  z0;  //only needed for echidna so far
  float  x;   //end position coords
  float  y;
  float  z;   //only needed for echidna so far
  int    num_vertex;  //number of vertices for this positioner
  float  pos_vertices_x[MAX_VERTICES_PER_POSITIONER]; // list of positioner-family specific vertex locations for this fiber 
  float  pos_vertices_y[MAX_VERTICES_PER_POSITIONER]; // list of positioner-family specific vertex locations for this fiber 
  float  radial_dist; //the distance from x0,y0 to x,y 
  float  angle1;       //For potzPoz -> this is the bearing of x,y from x0,y0, in degrees;  for MuPoz this is theta
  float  angle2;       //For MuPoz this is phi; 
  float  axis2_x;     //this is for mupoz only, and is the location of the 2nd rotation axis
  float  axis2_y;

  int    number_of_movements_total;                 //keeps a log of all the movements made by this fiber during the survey
  float  distance_moved_dir1_total;                 
  float  distance_moved_dir2_total;
  int    number_of_movements;                       //keeps a log of the movements made by this fiber since last repair
  float  distance_moved_dir1;                 
  float  distance_moved_dir2;
} fibergeom_struct;

//contains all the params of a single fiber/positioner
typedef struct {
  int    index; ////the index of this fiber in the array 
  int    id;  //the id of this fiber from the input file
  utiny  res;  //1=low res, 2=high res, 3=dual res;
  utiny  assign_flag;  //set to FIBER_ASSIGN_CODE_TARGET when assigned to a target

  uint   status;  //the functionality status of this fiber+positioner see FIBER_STATUS_FLAG* codes

  BOOL   is_switchable;  //can this fibre be switched between hi and low resolution?
  BOOL   is_empty;       //is this a dummy positioner with no fiber?

  int    nobjects;
  float  nobjects_weighted;
  //TODO - make object_list, object_sq_dist dynamically assigned
  uint   object_list[MAX_OBJECTS_PER_FIBER];    //index number of each object in range;
  float  object_sq_dist[MAX_OBJECTS_PER_FIBER]; //square of distance from fiber rest position to object
  object_struct *pObj;                          //pointer to the targeted object 
  int    nrivals;
  int    rival_list[MAX_RIVALS_PER_FIBER];      //list of fiber indices for other fibers which which can collide with this fiber 
  float  rival_dist[MAX_RIVALS_PER_FIBER];      //list of distances to fibers which can collide with this fiber 

  int    nneighbours;                           //number of immediate neighbours

  int    edge_flag;                             //set to >0 if near the edge of the FOV

  int    fiber_ranking;                         //the ranking of this fiber in the opinion of the object that it observes  
  int    assignment_order;                      //in what order was the fiber assigned?

  fibergeom_struct Geom;
  
  int    spectrograph_lores;     //used for detecting bright things next to faint things
  int    spectrograph_hires;     //      - index of spectrograph
  int    spectrograph_swres;     //      - index of spectrograph
  int    detector_lores;         //ditto - index of chip in spectrograph
  int    detector_hires;         //
  int    detector_swres;         //
  int    streak_number_lores;    //ditto - index of streak in detector
  int    streak_number_hires;    //
  int    streak_number_swres;    //
  float  crossdisp_pixel_lores;  //ditto - coord of streak centre in detector
  float  crossdisp_pixel_hires;  //
  float  crossdisp_pixel_swres;  //

  int    total_assignments_to_target;
  int    total_assignments_to_sky;
  int    total_assignments_to_spare;
  int    total_assignments_to_PItarget;
  int    total_num_tiles_spent_in_broken_state;
  int    total_assignments_as_lores;
  int    total_assignments_as_hires;

  int    number_of_repairs;
  double time_of_last_repair;
  int    number_of_candidate_targets;
  float  number_of_candidate_targets_weighted; //

  int    sector;               //an index that identifies the location of the fiber within the focal plane
                               //will be used for even distribution of sky fibers over the FOV
} fiber_struct;


typedef struct {
  int id;
  int ring; 
  int num_fibers;
  int num_fibers_per_res[NRESOLUTION_CODES];
  int num_fibers_switchable;
  int num_fibers_empty;
  fiber_struct **ppFib;
} sector_struct;


/////////////////////////////////////////////////////////////////////////////
//this structure contains all the params of a single 'tile' ie an arrangement
//of fibers, and the associated details of the positioner
typedef struct {
  //the params which describe the positioner
  int   positioner_family;
  char  code_name[STR_MAX];
  char  positioner_layout_filename[STR_MAX];
  BOOL  layout_from_file;
  float plate_scale;   // in units of mm/arcsec
  float invplate_scale;   // in units of arcsec/mm

  char  str_layout_shape[STR_MAX];
  int   layout_shape_code;  //e.g. GEOM_SHAPE_HEXAGON, GEOM_SHAPE_CIRCLE, GEOM_SHAPE_SQUARE
  
  float coordinate_tweak_factor; //dimensionless
  float positioner_spacing;
  int   positioner_hilo_ratio;
  int   positioner_hilo_pattern; // a code that identifies a particular family of patterns for placing the hi res fibers
  char  str_positioner_hilo_pattern[STR_MAX]; //a copy of the original string

  int   num_fibers;
  int   num_fibers_lo;
  int   num_fibers_hi;
  int   num_fibers_really_lo;
  int   num_fibers_really_hi;
  int   num_fibers_dual;
  int   num_fibers_switchable;
  int   num_fibers_empty;

  fiber_struct *pFib;
  //positioner params in units of mm:
  float min_sep_mm;            // min dist to next positioner
  float sq_min_sep_mm;         // derived - square of min dist to next positioner
  float patrol_radius_max_mm;  // max radius of positioner from (x0,y0)
  float patrol_radius_min_mm;  // min radius of positioner from (x0,y0)
  float sq_patrol_radius_max_mm;  // square of max radius of positioner from (x0,y0)
  float sq_patrol_radius_min_mm;  // square of min radius of positioner from (x0,y0)

  //now in degrees
  float patrol_radius_max_deg;  // max radius of positioner from (x0,y0)

  float fiber_offaxis_max_mm;   //the max dist of any fiber starting position from the
  float fiber_offaxis_max_deg;  //telescope optical axis
  float sq_fiber_offaxis_max_mm;   //==SQR(fiber_offaxis_max_mm)
  
  //the vertices of the hexagon
  int    num_vertex; //should always be 6 for a hexagon
  double vertex_x[MAX_VERTICES_PER_TILE]; //in mm
  double vertex_y[MAX_VERTICES_PER_TILE]; //in mm
  //the following approximate the area covered if we consider the reach of the positioners
  int    num_ext_vertex; //should always be 6*2 for a hexagon
  double ext_vertex_x[MAX_VERTICES_PER_TILE]; //in arcsec
  double ext_vertex_y[MAX_VERTICES_PER_TILE]; //in arcsec

  //  //the midpoints of the faces of the hexagon - not currently used
  //  double midface_x[MAX_VERTICES_PER_TILE]; //in mm
  //  double midface_y[MAX_VERTICES_PER_TILE]; //in mm

  //plotting parameters
  float gp_ellipse_fs_lo;
  float gp_ellipse_fs_hi;

  ///the following gives the absolute minumum separation possible
  //between two objects that can both be targetted, given the knowledge of the tip radii, minumum separations etc
  float sq_min_object_sep_mm;
  
  //positioner-family specific parameters
  float potzpoz_width;
  float potzpoz_tip_radius;
  float potzpoz_pivot_offset;
  float potzpoz_rectangle_length;
  float potzpoz_half_width;

  float mupoz_tip_width;
  float mupoz_tip_radius;
  float mupoz_arm_width;
  float mupoz_arm_length;
  float mupoz_half_patrol_radius; //the following are all derived params
  float mupoz_half_tip_width;     //
  float mupoz_half_arm_width;     //
  float mupoz_arm_start;          //
  float mupoz_sq_min_dist_between_axis2; //

  float spine_diameter;   //used for all spine positioners (ECHIDNA,AESOP etc)
  float spine_length;
  float spine_radius; //derived param

  float old_mupoz_tip_width;
  float old_mupoz_tip_dist;

  float starbug_radius;

  quad_struct Quad; //quad struct for the fibers

  twoDhisto_struct fiberXY_histo_hires; //structures to record the 2d histo of fiber offsets 
  twoDhisto_struct fiberXY_histo_lores; //
  twoDhisto_struct fiberXY_histo_swres; //

  int number_of_spectrographs_hires;      //these are inputs
  int number_of_spectrographs_lores;
  int number_of_spectrographs_swres;
  int number_of_detectors_per_spectrograph_hires;      //these are inputs
  int number_of_detectors_per_spectrograph_lores;
  int number_of_detectors_per_spectrograph_swres;
  int crossdisp_pixels_per_spectrograph_hires; //these are inputs
  int crossdisp_pixels_per_spectrograph_lores;
  int crossdisp_pixels_per_spectrograph_swres;
  float crossdisp_pixels_per_fiber_hires;  //these are calculated from the above
  float crossdisp_pixels_per_fiber_lores;
  float crossdisp_pixels_per_fiber_swres;

  int num_sectors;
  int num_fibers_reserved_per_sector[NRESOLUTION_CODES];
  sector_struct Sector[MAX_SECTORS_PER_TILE];
  BOOL flag_sectors_supplied_in_input_file;
  BOOL flag_edge_flags_supplied_in_input_file;
} focalplane_struct;




typedef struct {
  //  int          phase;                       //this is a code identifying this moon phase
  int          group_code;                  //either set to MOON_GROUP_BB or MOON_GROUP_DG
  float        fraction;                    //this is fraction of the time that is spent in this moon group

  unsigned int mask;                        //bitmask for this moon phase. Usually = {SURVEY_BRIGHT, SURVEY_GREY | SURVEY_DARK} ;
  unsigned int in_area_mask;                //bitmask for this moon phase. Usually = {SURVEY_IN_AREA_BRIGHT, SURVEY_IN_AREA_GREY | SURVEY_IN_AREA_DARK} ;
  char*        codename;                    //a pointer to a string giving a short readable code for this moon phase
                                            //Usually one of {MOON_PHASE_CODENAME_BRIGHT, MOON_PHASE_CODENAME_GREY, MOON_PHASE_CODENAME_DARK} ;
  char*        longname;                    //a pointer to a string giving a longer readable code for this moon phase
                                            //Usually = {MOON_PHASE_CODENAME_BRIGHT, MOON_PHASE_CODENAME_GREY, MOON_PHASE_CODENAME_DARK} ;

  //the following apply to the baseline survey:
  //  int          num_passes;                  //num passes over the sky in each moon phase
  int          num_tiles_per_pass;          //number of tiles per pass  
  int          num_tiles_per_field;         //number of tiles per field =  num passes * number of tiles per pass
  int          num_tiles_poss;              //total number of tiles that we have time for in this moon phase
  int          num_tiles_req;               //total number of tiles that were requested in this moon phase
  int          num_tiles_todo;              //total number of tiles that we will carry out in this moon phase
  int          num_tiles_done;              //total number of tiles that we have carried out in this moon phase
  int          num_tiles_wasted;            //total number of tiles where we couldn't find anything to observe, in this moon phase


  
  int          histo_tiles_per_field_req[MAX_TILES_PER_FIELD+1];  //keep a count of how many Fields were requested to get how many tiles
  int          histo_tiles_per_field_todo[MAX_TILES_PER_FIELD+1]; //keep a count of how many Fields should get how many tiles
  int          histo_tiles_per_field_done[MAX_TILES_PER_FIELD+1]; //keep a count of how many Fields do actually get how many tiles
  int          max_num_tiles_per_field_req;
  int          max_num_tiles_per_field_todo;
  int          max_num_tiles_per_field_done;

  int          num_fields;                  //total number of complete fields that we have time for in this moon phase
  int          num_objects;                 //number of objects observed so far in this moon phase

  
  float        Hours_gross;                 //gross hours available for observing in this moon phase
  float        Minutes_gross_per_tile;      //gross minutes available for observing in this moon phase
  float        Minutes_net_per_tile;        //net minutes available per tile for observing in this moon phase
  float        Minutes_net_per_field;       //net minutes available per field for observing in this moon phase
  //  int          total_tiles_observed;
  int          total_pseudo_objects;

  //used in timeline survey
  double       minimum_gross_mins_per_partial_tile;

  float        average_overhead_per_first_tile_mins;
  float        average_overhead_per_subsequent_tile_mins;
  float        average_overhead_per_tile;

  float        overallocation_factor;

  char         str_tile_planning_method[STR_MAX];
  int          tile_planning_method;
  int          tile_planning_sign;

} moon_struct;


//this structure holds details of the survey strategy, and moon phase info
//expect a small number of defined moon phases e.g. bright and dark/grey
//survey_struct
typedef struct {
  char        sim_code_name[STR_MAX];             //code name of this simulation
  char        sim_comments[LSTR_MAX];

  int         main_mode;                           //code for the survey type (timeline, pooled etc)

  int         num_moon_groups;                //number of moon groups to consider
  moon_struct moon[MAX_NUM_MOON_GROUPS];      //array of moon structs - one for each possible moon group

  moon_struct *pmoon_bb;  //serve as pointers for when we want to specifically use bb or dg moon groups
  moon_struct *pmoon_dg;
  int         num_tiles_poss_total;       //total number of tiles that we have time for in any moon phase
  int         num_tiles_req_total;        //total number of tiles that were requested - in any moon phase
  int         num_tiles_todo_total;       //total number of tiles that will be done - in any moon phase
  float       duration;                   //number of years in survey
  double      JD_start;                   //start date of survey (Julian days)
  double      JD_stop;                    //endt date of survey (Julian days)
  int         total_num_nights;
  float       mean_dark_time_hours_per_night;
  float       Hours_gross_total;
  int         strategy_code;
  
  int         moon_split_code;
  float       moon_split_mag_high_res;
  float       moon_split_mag_low_res;
  float       moon_split_exp_tolerance;
  float       moon_split_by_exposure_texp_bright_threshold;  //the threshold bright exposure time for considering an object
                                                             //for the bright/darkgrey surveys in this moon phase
  
  //this is how to decide which tile to put a source into
  int         target_to_field_policy;

  int         minimum_sky_fibers_low_res;
  int         minimum_sky_fibers_high_res;
  int         minimum_PI_fibers_low_res;
  int         minimum_PI_fibers_high_res;
  
  float       moon_dist_min;      //absolute minimum moon distance(degrees) 
  float       moon_dist_frac_min; //product of distance(degrees) and illum_frac
  float       zenith_angle_min;   //degrees
  float       airmass_max;        //measured as sec(Z)
  float       zenith_angle_max;   //degrees, derived from airmass_max
  
  float       IQ_max;         //max _delivered_ V band seeing, FWHM, arcsec

  int         iNum_field_in_any_survey;
  int         iNum_field_in_bright_survey;
  int         iNum_field_in_darkgrey_survey;
  int         iNum_field_used_in_any_survey;

  double bounds_ra_min ;
  double bounds_ra_max ;
  double bounds_dec_min;
  double bounds_dec_max;
  double boundsA_dec_min;
  double boundsA_dec_max;
  double plot_dec_min;
  double plot_dec_max;

  double tiling_dec_min;
  double tiling_dec_max;
  char   str_tiling_description_file[STR_MAX];  //name of the tiling description file

  double dbpdg_gal_lat_max_bright;
  double dbpdg_gal_lat_min_darkgrey;
  
  int    tiling_num_dithers;
  double tiling_dither_offsets_x[MAX_DITHERS_PER_FIELD];  //in mm from field centre
  double tiling_dither_offsets_y[MAX_DITHERS_PER_FIELD];  //
  double max_dither_radius_mm;
  
  //some copies of the input overheads values
  float overheads_maintenance_nights_per_year;
  int   overheads_maintenance_cadence;             //time between scheduled maintenance breaks, nights
  int   overheads_maintenance_nights_per_shutdown; //time per scheduled maintenance break, nights 
  float overheads_fault_nights_per_year;

  float overheads_slew_speed;                  //deg s^-1
  float overheads_slew_accel;                  //deg s^-2
  float overheads_settling_mins_per_slew;      //mins/slew 
  float overheads_guide_star_acquisition_mins; //mins/slew 
  float overheads_positioning_mins_per_tile;   //mins/tile
  float overheads_readout_mins_per_tile;       //mins/tile
  float overheads_mins_per_dither;             //mins/dither
  float overheads_calibration_mins_per_tile;   //mins/tile

  float overheads_average_time_per_slew;       //mins/slew  - derived param
  float overhead_per_readout_reposition_calib_mins;

  int   map_projection;
  int   progress_report_cadence; //days
  int   FoM_report_cadence;      //days
  int   map_npix_ra;
  int   map_npix_dec;
  BOOL  map_left_is_east;   //if true then left corresponds to East on maps

  BOOL  do_stats;
  BOOL  do_plots;
  BOOL  suppress_output;
  BOOL  do_maps;
  BOOL  use_up_spare_tiles;

  BOOL  write_fiber_assignments;
  BOOL  write_fiber_positions;
  int   max_fiber_position_files;
  int   max_fiber_assignment_files;
  int   max_field_ranking_files;
  int   num_fiber_position_files_written;
  int   num_fiber_assignment_files_written;
  int   num_field_ranking_files_written;

  int   max_threads;
  int   debug_max_tiles;  //for debug use
  int   debug_max_nights;  //for debug use

  int   num_unique_clusters;

  float maximum_texpfracdone_to_consider;

  float object_weighting_completed_exponent;       // How to decrease priority for well completed objects
  float object_weighting_started_factor;           // How to increase priority of partially observed targets
  float object_weighting_hires_bonus;              // How to increase priority for high res objects
  float object_weighting_hard_in_bright_decrement; // How to decrease priority for objects that are difficult to complete in bright time
  float object_weighting_hard_in_bright_threshold; // Threshold for decreasing priority for objects that are difficult to complete in bright time
  float object_weighting_cannot_complete_factor;   // How to decrease priority for objects that are nominally impossible to complete
  
  int   field_weighting_method;  //method used to weight fields in timeline survey
  int   field_weighting_nparams;
  float field_weighting_params[MAX_FIELD_WEIGHTING_PARAMS];
  float field_weighting_last_field_bonus_factor;
  float field_weighting_blur_sigma;
  float field_weighting_complete_fields_exponent;
  float delta_mag_for_problem_neighbour;



  float discard_threshold_texp; //requested exposure time in minutes above which we consider a
                                //target impossible to observe.
  float max_exposure_scaling;
  float FoM_success_threshold;
  double debug_ra_min ;
  double debug_ra_max ;
  double debug_dec_min;
  double debug_dec_max;

  int total_objects_hires;
  int total_objects_lores;
  int total_objects_exgal;
  int total_objects_exgal_lores;
  int total_objects_exgal_hires;
  int total_objects_gal;
  int total_objects_gal_lores;
  int total_objects_gal_hires;
  int total_objects_impossible;
  int total_objects_impossible_lores;
  int total_objects_impossible_hires;

  clusterlist_struct *pClusterList;

  int num_nights;
  int *pSlots_each_night_dg;   //These are used to record the number of slots
  int *pSlots_each_night_bb;   //that are bb or dg in each night of the survey

  //variables that record the avilability of tiles as a function of RA
  float num_tiles_available_per_RA_bin[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
  float num_tiles_available_per_RA_bin_smooth[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
 
  int num_tiles_done_per_RA_bin[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
  int num_tiles_done_per_RAzen_bin[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];
  int num_tiles_wasted_per_RAzen_bin[TILING_NRA_BINS][MAX_NUM_MOON_GROUPS];

  //output logfiles 
  FILE *pNightReports;
  FILE *pTimelineReports;
  FILE *pTileReports;      // TODO eventually remove from outputs (superceded by TileLog)
  FILE *pPerTileLog;       // TODO eventually remove from outputs (superceded by TileLog)
  fits_table_helper_struct TileLog;

  BOOL gzip_output_catalogues;
  long healpix_nside;

  int nights_since_last_maintenance_shutdown;
  int nights_spent_in_maintenance_shutdown;

  int debug_num_tiles_observed;

  double last_ra;
  double last_dec;
  int night_counter;
  int tile_counter;
  BOOL   wind_constraint_flag;
  int    cloud_code;
  char  *str_cloud_code;
  BOOL   print_header_flag;
  BOOL   observe_tonight;

  BOOL  debug;
  BOOL  clobber;
  
} survey_struct;


//this is a struct to hold an array of lists of object-->fiber associations
//pFibers will be mallocced to hold an array (size nobjects*MAX_FIBERS_PER_OBJECT) of pointers to fiber structs 
//objects take the same index as the object list in the containing field.
typedef struct {
  int nobjects;
  int *pnumfibs;
  float *psqdist;
  //  fiber_struct **pFibers;
  int *pfib_ids;
} objectfiber_struct;


typedef struct {
  field_struct *pField;             //pointer to field structure
  float Z;                          //current zenith distance, deg
  float airmass;                    //current airmass
  float moon_dist;                  //current dist from moon, deg
  float sky_brightness;             //local sky brightness, mag/arcsec, can be relative
  float visibility;                 //observability of this field in this moon phase
  float fraction_of_tiles_done;     //fraction of requested tiles in this moon phase that
                                    //have been executed so far for this field 
  float priority_sum_remaining;     //sum of the object priorities of as-yet unfinished objects in this tile
  float weight;                     //eventual weight of this tile
} sortfield_struct;

typedef struct {
  int array_length;             //actual length of the array pointed to by pFieldList
  int nFields;                  //number of valid fields placed into the pFieldList array
  sortfield_struct *pFields;    //array of sortfield_structs
  sortfield_struct Mean;        //sortfield_struct containing the mean values 
  sortfield_struct Max;         //sortfield_struct containing the maximum values 
  sortfield_struct Min;         //sortfield_struct containing the minimum values 
  field_struct *pLastField;     //pointer to field structure of last observed field (can be NULL)
  int last_field_count;         //number of previous consecutive observations of this field
} sortfieldlist_struct;



#define ASSIGN_STATUS_FREE    0
#define ASSIGN_STATUS_SCIENCE 1
#define ASSIGN_STATUS_SKY     2
#define ASSIGN_STATUS_CALIB   3

#define ASSIGN_STATUS_STR_FREE    "FREE"
#define ASSIGN_STATUS_STR_SCIENCE "SCIENCE"
#define ASSIGN_STATUS_STR_SKY     "SKY"
#define ASSIGN_STATUS_STR_CALIB   "CALIB"


typedef struct {
  int status;
  fiber_struct  *pFib;
  object_struct *pObj;
  float object_x;
  float object_y;
} assign_struct;

typedef struct {
  focalplane_struct *pFocal; //pointer to the focal plane struct
  field_struct *pField;      //pointer to the Field which is being observed in this tile

  int   dither_index;        //so that can get correct ra0,dec0 for this Tile
  float time_gross;          //total time required to execute this tile (seconds)
  float time_net;            //net on-sky time for this tile (seconds)

  int array_length;          //actual length of the array pointed to by pAssign
  int nAssign;               //number of valid assign_struct structures pointed to by pAssign
  assign_struct *pAssign;    //pointer to array of assign_structs
} tile_struct;



///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///Here comes a list of function prototypes
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


int survey_calc_times_and_tiles            (survey_struct *pSurvey, fieldlist_struct* pFieldList, telescope_struct *pTele);
int survey_calc_total_time_available       (survey_struct *pSurvey, telescope_struct *pTele);
//int survey_calc_times_with_goal_area       (survey_struct *pSurvey);
//int survey_calc_times_with_goal_exposure   (survey_struct *pSurvey);
int survey_calc_tile_overheads (survey_struct *pSurvey);
int survey_calc_tiles_available (survey_struct *pSurvey);


int survey_choose_tiles_using_visibility (survey_struct *pSurvey, fieldlist_struct *pFieldList, telescope_struct *pTele);
int survey_choose_tiles_with_goal_exposure (survey_struct *pSurvey, fieldlist_struct *pFieldList, telescope_struct *pTele);

int survey_determine_sky_tiling (survey_struct *pSurvey,
                                 fieldlist_struct* pFieldList,
                                 telescope_struct *pTele,
                                 focalplane_struct *pFocal);

int read_and_process_input_catalogues      (objectlist_struct *pObjList, survey_struct *pSurvey, cataloguelist_struct* pCatList );
//int read_input_catalogue_ascii             (objectlist_struct *pObjList, survey_struct *pSurvey, catalogue_struct* pCat);
int read_input_catalogue_fits              (objectlist_struct *pObjList, survey_struct *pSurvey, catalogue_struct* pCat, cataloguelist_struct* pCatList);


int associate_object_with_fibers           (object_struct *pObj, field_struct *pField, focalplane_struct *pFocal, objectfiber_struct* pObjFib, int objfib_index, int dither_index);
int objectlist_process_objects             (objectlist_struct *pObjList, survey_struct *pSurvey);
int calc_modified_object_priority          (object_struct* pObj, int object_sub_index, float Texp, int moon_phase, float *pResult);
int associate_objects_with_fields          (objectlist_struct *pObjList, fieldlist_struct *pFieldList, telescope_struct *pTele, focalplane_struct *pFocal, survey_struct *pSurvey, const int target_to_field_policy);

int prepare_objects_for_observation        ( survey_struct *pSurvey, field_struct *pField, objectlist_struct *pObjList, moon_struct *pMoon, int *pnok );
int prepare_objects_for_observation_ignore_moon (survey_struct *pSurvey, field_struct *pField, objectlist_struct *pObjList, int *pnok );


//int carry_out_non_timeline_survey          (survey_struct *pSurvey, objectlist_struct *pObjList, fieldlist_struct *pFieldList, focalplane_struct *pFocal, telescope_struct *pTele);

int timeline_survey_prep_tasks     ( survey_struct *pSurvey,
                                     objectlist_struct *pObjList,
                                     fieldlist_struct *pFieldList,
                                     focalplane_struct *pFocal,
                                     telescope_struct *pTele,
                                     timeline_struct *pTime,
                                     cataloguelist_struct *pCatList,
                                     sortfieldlist_struct *pSortFieldList);

int timeline_survey_begin_night    ( survey_struct *pSurvey,
                                     objectlist_struct *pObjList,
                                     fieldlist_struct *pFieldList,
                                     focalplane_struct *pFocal,
                                     telescope_struct *pTele,
                                     timeline_struct *pTime,
                                     cataloguelist_struct *pCatList,
                                     sortfieldlist_struct *pSortFieldList);

int timeline_survey_observe_night  ( survey_struct *pSurvey,
                                     objectlist_struct *pObjList,
                                     fieldlist_struct *pFieldList,
                                     focalplane_struct *pFocal,
                                     telescope_struct *pTele,
                                     timeline_struct *pTime,
                                     cataloguelist_struct *pCatList,
                                     sortfieldlist_struct *pSortFieldList);
int timeline_survey_end_tasks      ( survey_struct *pSurvey,
                                     objectlist_struct *pObjList,
                                     fieldlist_struct *pFieldList,
                                     focalplane_struct *pFocal,
                                     telescope_struct *pTele,
                                     timeline_struct *pTime,
                                     cataloguelist_struct *pCatList,
                                     sortfieldlist_struct *pSortFieldList);

int timeline_survey_wrapper        ( survey_struct *pSurvey,
                                     objectlist_struct *pObjList,
                                     fieldlist_struct *pFieldList,
                                     focalplane_struct *pFocal,
                                     telescope_struct *pTele,
                                     timeline_struct *pTime,
                                     cataloguelist_struct *pCatList);


int add_object_to_fiber                    (focalplane_struct *pFocal, fiber_struct *pFib, object_struct *pObj, double x, double y);
int associate_objects_with_fibers          (objectlist_struct *pObjList, field_struct *pField, focalplane_struct *pFocal, objectfiber_struct* pObjFib, int dither_index);
int calc_object_weight                     (survey_struct *pSurvey,  object_struct *pObj, field_struct *pField, int moon_phase_code, float *presult);
int get_fibers_for_object                  (object_struct *pObj, focalplane_struct *pFocal, uint *pid_list, float *pdist_list, int *pnumber_of_fibers );
int assign_a_fiber_to_object               (focalplane_struct *pFocal, field_struct *pField, object_struct* pObj, objectfiber_struct *pObjFib, int objectfiber_index, fiber_struct **ppAssignedFib, int dither_index, int *pnfibers_remain_per_sector);
int add_fiber_to_objectfiber_struct        (objectfiber_struct *pObjFib, fiber_struct *pFib, int objfib_index );


int observe_tiles_on_a_field               (timeline_struct *pTime, moon_struct *pMoon, focalplane_struct *pFocal, fieldlist_struct *pFieldList, field_struct *pField, objectlist_struct *pObjList, survey_struct *pSurvey, int ntiles, float texp_per_tile_mins);
int observe_tile_on_a_field                (timeline_struct *pTime, moon_struct *pMoon, focalplane_struct *pFocal, fieldlist_struct *pFieldList, field_struct *pField, objectlist_struct *pObjList, survey_struct *pSurvey, objectfiber_struct* pObjFib, float texp_per_tile_mins, int dither_index);

int survey_calc_observability         (survey_struct *pSurvey, fieldlist_struct *pFieldList, telescope_struct *pTele, timeline_struct *pTime, sortfieldlist_struct *pSortFieldList);

int survey_calc_moon_fractions        (survey_struct *pSurvey, telescope_struct *pTele, timeline_struct *pTime);


//int write_assigned_objects_file            (objectlist_struct *pObjList, fieldlist_struct *pFieldList, survey_struct *pSurvey, cataloguelist_struct* pCatList, const char *str_filename );
//int write_assigned_objects_files           (objectlist_struct *pObjList, fieldlist_struct *pFieldList, survey_struct *pSurvey, cataloguelist_struct* pCatList, const char *str_prefix );
int write_unassigned_objects_file          (objectlist_struct *pObjList, cataloguelist_struct* pCatList, const char *str_filename );
int write_fiber_assignment_for_tile        (const char* str_filename, field_struct *pField, focalplane_struct *pFocal, objectlist_struct *pObjList, int tile_index, int dither_index);
int write_object_catalogue_for_tile (const char* str_filename, field_struct *pField, focalplane_struct *pFocal, objectlist_struct *pObjList, int tile_index, int dither_index);

int write_object_summary_header            (FILE* pFile, const char *str_filename, catalogue_struct* pCat);
int write_object_summary_line              (FILE* pFile, object_struct *pObj, int survey_main_mode, const char* str_suffix);
//int write_object_summary_file              (objectlist_struct *pObjList, survey_struct *pSurvey, cataloguelist_struct* pCatList, const char *str_filename );
//int write_object_summary_files             (objectlist_struct *pObjList, survey_struct *pSurvey, cataloguelist_struct* pCatList, const char *str_prefix );
int write_object_summary_files             (objectlist_struct *pObjList, survey_struct *pSurvey, cataloguelist_struct* pCatList, const char *str_prefix );
int write_object_summary_file_ascii        (objectlist_struct *pObjList, survey_struct *pSurvey, catalogue_struct* pCat, const char *str_prefix );
int write_object_summary_file_fits         (objectlist_struct *pObjList, survey_struct *pSurvey, catalogue_struct* pCat, const char *str_prefix );


//

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
int timeline_determine_moon_phase_code     (timeline_struct* pTime, float *pSkyBrZen, int *pPhase_code, int *pGroup_code, survey_struct *pSurvey, moon_struct **ppMoon);

//structure initilisation and space freeing functions
int objectlist_struct_init                 (objectlist_struct    **ppObjList);
int objectlist_struct_free                 (objectlist_struct    **ppObjList);
int focalplane_struct_init                 (focalplane_struct    **ppFocal);
int focalplane_struct_free                 (focalplane_struct    **ppFocal);
int survey_struct_init                     (survey_struct        **ppSurvey);
int survey_struct_free                     (survey_struct        **ppSurvey);
int cataloguelist_struct_init              (cataloguelist_struct **ppCatalogueList);
int cataloguelist_struct_free              (cataloguelist_struct **ppCatalogueList);


int FourFS_OpSim_print_version               (FILE *pFile);

void debug_print_type_sizes                  (FILE* pstream);
int print_version_and_inputs                 (FILE* pstream, int argc, char **argv);
int print_full_version_information_to_file   (const char* str_filename);
int print_full_version_information           (FILE *pFile);
int print_full_help_information              (FILE *pFile);

int write_per_field_logfile_fits             (fieldlist_struct *pFieldList, const char* str_filename, BOOL clobber);
int write_per_field_logfile_ascii            (fieldlist_struct *pFieldList, const char* str_filename);

int print_per_tile_log_header_line           (FILE *pFile);
int init_per_tile_log                        (survey_struct *pSurvey, const char *str_filename, FILE **ppFile);
int print_tile_reports_header_line           (FILE *pFile);
int init_tile_reports_logfile                (survey_struct *pSurvey, const char *str_filename, FILE **ppFile);
int init_night_reports_logfile               (survey_struct *pSurvey, const char *str_filename, FILE **ppFile);
int init_timeline_reports_logfile            (survey_struct *pSurvey, const char *str_filename, FILE **ppFile);
int init_tile_log_fits                       (survey_struct *pSurvey, const char *str_filename);
int write_nulls_to_tilelog                   (fits_table_helper_struct *pT, survey_struct* pSurvey, focalplane_struct* pFocal );

double calc_max_airmass ( double seeing_zen, double IQ_max, double airmass_limit);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int cluster_struct_init     (cluster_struct* pCluster);
int cluster_struct_copy     ( cluster_struct* pClusterOrig, cluster_struct* pClusterCopy);
int clusterlist_struct_add_cluster (clusterlist_struct *pClusterList, cluster_struct *pCluster);
int clusterlist_struct_init (clusterlist_struct **ppClusterList, int total_num_clusters);
int clusterlist_struct_tidy (clusterlist_struct *pClusterList);
int clusterlist_struct_free (clusterlist_struct **ppClusterList);
int clusterlist_struct_calc_summary (clusterlist_struct *pClusterList,  objectlist_struct *pObjList, catalogue_struct* pCat);
int clusterlist_struct_write_summary (clusterlist_struct *pClusterList, objectlist_struct *pObjList, const char *str_filename);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int assign_struct_init     (assign_struct *pAssign);
int assign_struct_write    (assign_struct *pAssign, FILE *pFile, const char *str_prefix);

int tile_struct_init       (tile_struct *pTile, int assigns_per_tile);
int tile_struct_free       (tile_struct **ppTile);
int tile_struct_write      (tile_struct *pTile, FILE *pFile, const char *str_prefix);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL test_if_file_is_fits        (const char* str_filename);
BOOL test_if_file_is_fits_table  (const char* str_filename);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int survey_run_gnuplot_on_plotfile         (survey_struct *pSurvey, const char* strPlotfile );
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
