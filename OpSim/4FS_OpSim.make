#------------------------------------
# This is the makefile for 4FS_OpSim
# CVS_REVISION "$Revision: 1.24 $"
# CVS_DATE     "$Date: 2015/11/16 17:07:35 $"
#------------------------------------

include ../arch.make

#the following allows you to have an include file 
#that lives outside the CVS tree:
#include ~/code/arch.make

# If for some reason you need to override the settings 
# in these arch.make files then do so below: 
#OPTIMISE =
#DEBUG =
#BINARY_DIR = 
#ARCH =
#CFLAGS = $(ARCH) -Wall -Wno-parentheses -Wl,--no-as-needed $(OPTIMISE) $(DEBUG)
#CXX = gcc

COMMON_DIR = ../common


LDIR = -L./ -L$(HEALPIX_PATH)/lib -L$(CFITSIO_PATH)/lib -L$(COMMON_DIR)/

IDIR = -I./ -I$(HEALPIX_PATH)/include -I$(CFITSIO_PATH)/include -I$(COMMON_DIR)/

LIBS =  -lm -lchealpix -lcfitsio 

OUTSTEM = 4FS_OpSim
OFILE   = $(addsuffix .o,$(OUTSTEM))
CFILE   = $(addsuffix .c,$(OUTSTEM))
HFILE   = $(addsuffix .h,$(OUTSTEM))
OUTFILE = $(OUTSTEM)

DEP_LIBS = $(COMMON_DIR)/utensils_lib $(COMMON_DIR)/params_lib $(COMMON_DIR)/quadtree_lib $(COMMON_DIR)/geometry_lib $(COMMON_DIR)/fits_helper_lib $(COMMON_DIR)/hpx_extras_lib OpSim_timeline_lib OpSim_input_params_lib OpSim_statistics_lib OpSim_FoMs_lib OpSim_positioner_lib OpSim_telescope_site_lib OpSim_html_lib OpSim_OB_lib OpSim_fields_lib


DEP_OFILES  = $(OFILE) $(addsuffix .o,$(DEP_LIBS)) 

DEP_HFILES  = $(HFILE) $(addsuffix .h,$(DEP_LIBS)) $(COMMON_DIR)/define.h

DEP_CFILES  = $(CFILE) $(addsuffix .c,$(DEP_LIBS)) 


$(OUTFILE)	:	$(DEP_OFILES)
	$(CXX) $(CFLAGS) $(LIBS) $(IDIR) $(LDIR) -o $(OUTFILE) $(DEP_OFILES)

$(OFILE)	:	$(DEP_HFILES) $(DEP_CFILES)
	$(CXX) $(CFLAGS) $(IDIR) -c -o $(OFILE) $(CFILE) 

#define the rules to make the library files
%_lib.o	:	%_lib.[ch]
	make -f $*_lib.make

install   : 
	cp -pv $(OUTFILE) $(BINARY_DIR)

clean   : 
	rm -f $(OUTFILE) ./*.o $(BINARY_DIR)/$(OUTFILE)
#	rm -f $(OFILE) $(OUTFILE) ./*.o

#clean_all   : 
#	rm -f $(DEP_OFILES) $(OUTFILE)

.PHONY: doc
doc	:	
	doxygen $(OUTSTEM).doxyfile

#end

