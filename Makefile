# Makefile to build all 4FS code
# 
# CVS_VERSION "$Revision: 1.6 $"
# CVS_DATE    "$Date: 2015/10/26 10:10:30 $"

SUBDIR1 = doc 
SUBDIR2 = common 
SUBDIR3 = OpSim
SUBDIR4 = ETC
SUBDIR5 = RST
SUBDIR6 = utils

# Determine which of these directories actually exist
SUBDIRS =
ifneq ("$(wildcard $(SUBDIR1))","")
  SUBDIRS += $(SUBDIR1)
endif
ifneq ("$(wildcard $(SUBDIR2))","")
  SUBDIRS += $(SUBDIR2)
endif
ifneq ("$(wildcard $(SUBDIR3))","")
  SUBDIRS += $(SUBDIR3)
endif
ifneq ("$(wildcard $(SUBDIR4))","")
  SUBDIRS += $(SUBDIR4)
endif
ifneq ("$(wildcard $(SUBDIR5))","")
  SUBDIRS += $(SUBDIR5)
endif
ifneq ("$(wildcard $(SUBDIR6))","")
  SUBDIRS += $(SUBDIR6)
endif


.PHONY:	$(SUBDIRS)

all:	
	for i in $(SUBDIRS); do \
	    $(MAKE) -C $$i; \
	done

clean:	
	for i in $(SUBDIRS); do \
	    $(MAKE) -C $$i clean; \
	done
#	rm -f ./*/git_version_date~


install:	
	for i in $(SUBDIRS); do \
	    $(MAKE) -C $$i install; \
	done

